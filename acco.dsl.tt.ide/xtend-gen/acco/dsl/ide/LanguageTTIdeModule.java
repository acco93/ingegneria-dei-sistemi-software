/**
 * generated by Xtext 2.11.0
 */
package acco.dsl.ide;

import acco.dsl.ide.AbstractLanguageTTIdeModule;

/**
 * Use this class to register ide components.
 */
@SuppressWarnings("all")
public class LanguageTTIdeModule extends AbstractLanguageTTIdeModule {
}
