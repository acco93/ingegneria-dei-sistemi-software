package it.unibo.userinterface;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import alice.tuprolog.Prolog;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.QActorContext;
import it.unibo.qactors.QActorUtils;
import it.unibo.qactors.akka.QActor;
import it.unibo.system.SituatedSysKb;

public class TestPCApplication {

	private IOutputEnvView stdOut = SituatedSysKb.standardOutEnvView;
	private QActorContext ctx;
	private QActor qa;
	private Prolog pengine;

	private String[] sonarData = { "p(100,20)", "p(100,0)", "p(100,40)", // normal
																			// behavior

			"p(100,0)", "p(100,20)", "p(20,40)", // obstacle in front of sonar40

			"p(45, 0)", "p(100,20)", "p(100,40)", // robot in front of sonar0

			"p(100,0)", "p(50,20)", "p(100,40)", // robot in front of sonar20

			"p(10,0)", "p(100,20)", "p(100,40)", // obstacle in front of sonar0
													// but no alarm sound
													// because
													// the robot is beyond
													// sonar20

			"p(100,0)", "p(100,20)", "p(10,40)", // obstacle in front of sonar40
													// too near to be the robot
													// + alarm sound

			"p(50,0)", "p(100,20)", "p(100,40)" // robot restar

	};

	@Before
	public void setUp() {
		try {

			ctx = QActorContext.initQActorSystem("pccontext", "./srcMore/it/unibo/pcContext/pc.pl",
					"./src-gen/it/unibo/pcContext/sysRules.pl", stdOut, null, true);
			assertTrue("setUp", ctx != null);
			qa = QActorUtils.getQActor("userinterface");
			assertTrue("setUp", qa != null);

			pengine = qa.getPrologEngine();
			assertTrue("setUp", pengine != null);
		} catch (Exception e) {
			System.out.println("setUp ERROR " + e.getMessage());
			fail("setUp " + e.getMessage());
		}
	}

	@Test
	public void testAll() throws Exception {

		// wait untill the gui is initialized
		int i = 0;
		while (!(Boolean) this.invoke("isSystemReady")) {
			System.out.println("Waiting for the ui to init ... [" + i + "]");
			i++;
			Thread.sleep(10000);
		}

		assertTrue("component ready", (Boolean) this.invoke("isSystemReady"));

		this.emitEvents(0, 3);

		assertTrue("check expression value", (Double) this.invoke("getExpressionValue") == 100.0);

		assertTrue("check sonar value", ((List<Sonar>) this.invoke("getSonarsList")).get(0).getLastValue() == 100);

		// the sonars are ordered
		assertTrue("check sonar position", ((List<Sonar>) this.invoke("getSonarsList")).get(1).getPosition() == 20);

		assertTrue("check restart, obstacle and alarm counts", (Integer) this.invoke("getRestartCounts")
				+ (Integer) this.invoke("getObstacleCounts") + (Integer) this.invoke("getAlarmCounts") == 0);

		assertTrue("robot position", (Integer) this.invoke("getRobotPosition") == 0);

		this.emitEvents(3, 6);

		assertTrue("obstacle in front of sonar40", (Integer) this.invoke("getObstacleCounts") == 1);

		this.emitEvents(6, 9);

		assertTrue("robot position 1",
				(Integer) this.invoke("getRobotPosition") == 1 && (Integer) this.invoke("getAlarmCounts") == 2);

		this.emitEvents(9, 12);

		assertTrue("robot position 2", (Integer) this.invoke("getRobotPosition") == 2);

		this.emitEvents(12, 15);

		assertTrue("check expression value",
				(Double) this.invoke("getExpressionValue") == 100.0 && (Integer) this.invoke("getObstacleCounts") == 2);

		this.emitEvents(15, 18);

		assertTrue("obstacle in front of sonar40 + alarm",
				(Integer) this.invoke("getObstacleCounts") == 3 && (Integer) this.invoke("getAlarmCounts") == 3);

		this.emitEvents(18, 21);

		assertTrue("restart",
				(Integer) this.invoke("getRestartCounts") == 1 && (Integer) this.invoke("getRobotPosition") == 1);
	}

	private void emitEvents(int start, int stop) throws InterruptedException {
		for (int i = start; i < stop; i++) {
			QActorUtils.emitEventAfterTime(ctx, "emitter", "sonar", sonarData[i], 0);
			// QActorUtils.raiseEvent(ctx, emitter, evId, evContent);
			Thread.sleep(1000);
		}
	}

	private Object invoke(String methodName) throws Exception {
		QActor qactrl = QActorUtils.getQActor("userinterface_ctrl");
		return qa.getByReflection(Userinterface.class, methodName).invoke(qactrl);
	}

}
