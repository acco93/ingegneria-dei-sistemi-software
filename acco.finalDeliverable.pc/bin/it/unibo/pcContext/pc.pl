%====================================================================================
% Context pcContext  SYSTEM-configuration: file it.unibo.pcContext.pc.pl 
%====================================================================================
context(pccontext, "localhost",  "TCP", "8033" ).  		 
%%% -------------------------------------------
qactor( userinterface , pccontext, "it.unibo.userinterface.MsgHandle_Userinterface"   ). %%store msgs 
qactor( userinterface_ctrl , pccontext, "it.unibo.userinterface.Userinterface"   ). %%control-driven 
qactor( mqttdaemon , pccontext, "it.unibo.mqttdaemon.MsgHandle_Mqttdaemon"   ). %%store msgs 
qactor( mqttdaemon_ctrl , pccontext, "it.unibo.mqttdaemon.Mqttdaemon"   ). %%control-driven 
%%% -------------------------------------------
eventhandler(evh,pccontext,"it.unibo.pcContext.Evh","sonar").  
%%% -------------------------------------------

