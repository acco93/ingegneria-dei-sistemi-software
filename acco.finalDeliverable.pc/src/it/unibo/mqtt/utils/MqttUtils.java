package it.unibo.mqtt.utils;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import it.unibo.qactors.akka.QActor;

public class MqttUtils {
	private static MqttUtils myself = null;
	protected String clientid = null;
	protected String eventId = "mqtt";
	protected String eventMsg = "";
	protected QActor actor = null;
	protected MqttClient client = null;
	private MqttCallback callback;

	public static MqttUtils getMqttSupport() {
		return myself;
	}

	public MqttUtils(MqttCallback callback) {
		myself = this;
		this.callback = callback;
	}

	public void connect(QActor actor, String brokerAddr, String topic) throws MqttException {
		System.out.println("			%%% MqttUtils connect/3 ");
		clientid = MqttClient.generateClientId();
		connect(actor, clientid, brokerAddr, topic);
	}

	public void connect(QActor actor, String clientid, String brokerAddr, String topic) throws MqttException {
		System.out.println("			%%% MqttUtils connect/4 " + clientid);
		this.actor = actor;
		client = new MqttClient(brokerAddr, clientid);
		MqttConnectOptions options = new MqttConnectOptions();
		options.setWill("unibo/clienterrors", "crashed".getBytes(), 2, true);
		client.connect(options);
	}

	public void disconnect() throws MqttException {
		System.out.println("			%%% MqttUtils disconnect " + client);
		if (client != null)
			client.disconnect();
	}

	public void publish(QActor actor, String clientid, String brokerAddr, String topic, String msg, int qos,
			boolean retain) throws MqttException {
		MqttMessage message = new MqttMessage();
		message.setRetained(retain);
		if (qos == 0 || qos == 1 || qos == 2) {// qos=0 fire and forget; qos=1
												// at least once(default);qos=2
												// exactly once
			message.setQos(0);
		}
		message.setPayload(msg.getBytes());
		System.out.println("			%%% MqttUtils publish " + message);
		client.publish(topic, message);
	}

	public void subscribe(QActor actor, String clientid, String brokerAddr, String topic) throws Exception {
		try {
			this.actor = actor;
			client.setCallback(callback);
			client.subscribe(topic);
		} catch (Exception e) {
			System.out.println("			%%% MqttUtils subscribe error " + e.getMessage());
			eventMsg = "mqtt(" + eventId + ", failure)";
			System.out.println("			%%% MqttUtils subscribe error " + eventMsg);
			if (actor != null)
				actor.sendMsg("mqttmsg", actor.getName(), "dispatch", "error");
			throw e;
		}
	}

}
