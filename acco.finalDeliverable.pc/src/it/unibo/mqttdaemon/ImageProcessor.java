package it.unibo.mqttdaemon;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.xml.bind.DatatypeConverter;

public class ImageProcessor implements MqttProcessor {

	public ImageProcessor() {

	}

	@Override
	public void process(String message) {

		byte[] decoded = DatatypeConverter.parseBase64Binary(message);

		BufferedImage img;

		try {

			img = ImageIO.read(new ByteArrayInputStream(decoded));

			JFrame f = new JFrame();
			f.setSize(500, 500);
			JLabel l = new JLabel(new ImageIcon(img));
			f.add(l);
			f.setVisible(true);

			// QActor qa = QActorUtils.getQActor("mqttdaemon");
			// qa.sendMsg("mqtt", "userinterface", "dispatch", "mqtt(done)");

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
