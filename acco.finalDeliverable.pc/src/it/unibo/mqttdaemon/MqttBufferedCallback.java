package it.unibo.mqttdaemon;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttMessage;

public class MqttBufferedCallback implements MqttCallback {

	private boolean receiving;
	private int chunksNum;
	private int currentChunk;
	private StringBuilder message;
	private MqttProcessor processor;

	public MqttBufferedCallback(MqttProcessor processor) {
		this.processor = processor;
		this.receiving = false;
		// in future they may become variable ...
		this.chunksNum = 1000;
		this.currentChunk = 0;
		this.message = new StringBuilder();
	}

	@Override
	public void messageArrived(String topic, MqttMessage chunk) throws Exception {

		System.out.println("Receiving [" + this.currentChunk + "/" + this.chunksNum + "]");

		if (!receiving) {
			this.receiving = true;
			this.currentChunk = 0;
			this.message = new StringBuilder();
		}

		this.message.append(chunk.toString());
		this.currentChunk++;

		if (this.currentChunk == this.chunksNum) {
			this.receiving = false;
			System.out.println(this.message.length());
			// deliver the message
			this.processor.process(message.toString());
		}

	}

	@Override
	public void connectionLost(Throwable cause) {
		System.out.println("Connection lost: " + cause);
	}

	@Override
	public void deliveryComplete(IMqttDeliveryToken token) {
		System.out.println("Delivery completed");

	}

}
