package it.unibo.mqttdaemon;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttMessage;

public class MqttOneShotCallback implements MqttCallback {

	private MqttProcessor processor;

	public MqttOneShotCallback(MqttProcessor processor) {
		this.processor = processor;
	}

	@Override
	public void connectionLost(Throwable cause) {
		System.out.println("Connection lost: " + cause);

	}

	@Override
	public void messageArrived(String topic, MqttMessage message) throws Exception {
		this.processor.process(message.toString());

	}

	@Override
	public void deliveryComplete(IMqttDeliveryToken token) {
		System.out.println("Delivery completed");

	}

}
