package it.unibo.mqttdaemon;

public interface MqttProcessor {

	void process(String message);

}
