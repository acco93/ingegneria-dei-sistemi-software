package it.unibo.visualinterface.visual;

import java.util.List;

import it.unibo.userinterface.Sonar;

/**
 * @author buzzo
 *
 */
public interface IGraphics {

	/**
	 * set a list of visualSonar from list of Sonar
	 * 
	 * @param sonars
	 */
	void setSonars(List<Sonar> sonars);

	/**
	 * set sonar distance if there is obstacle or robot
	 * 
	 * @param distance
	 * @param pos
	 */
	void setSonarData(int distance, int pos);

	/**
	 * set robot in front of sonar pos : pos = 0 for start position and pos =
	 * lastsonar+1 for area B
	 * 
	 * @param pos
	 */
	void setRobotPos(int pos);

	/**
	 * draw or delete obstacle
	 * 
	 * @param distance
	 * @param pos pos=1 means first sensor
	 * @param thereIs
	 *            for draw of delete obstacle
	 */
	void setObstacle(int distance, int pos, boolean thereIs);

	/**
	 * draw the expression
	 * 
	 * @param exp
	 */
	void printExpValue(String exp);
	
	/**
	 * take a photo in front of the sonar
	 * @param pos pos=1 means first sensor
	 */
	void takePhoto(int pos);
	
	void turnLeft(int pos);
	
	void turnForward(int pos);
}
