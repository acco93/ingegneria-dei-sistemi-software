package it.unibo.visualinterface.visual;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.geom.AffineTransform;
import java.awt.geom.RoundRectangle2D;

import javax.swing.ImageIcon;

public class VisualRobot {

	private int pos;
	private double x;
	private Graphics2D g2;
	private boolean thereIsSonar;
	private double width, height;
	private boolean camera;
	private Image img;

	public VisualRobot() {

		this.thereIsSonar = false;
		this.camera = false;
		this.pos = 0;

		img = new ImageIcon("src/camera.png").getImage();

	}

	public void addGraphics(Graphics2D g2) {
		this.g2 = g2;
	}

	public void setLocation(int pos) {

		this.pos = pos;

	}

	public void draw() {

		double newW = Surface.newW;

		double y = Surface.newH - 2 * Surface.newH / 4;

		double area = newW / (Surface.totSensor + 2);
		width = Surface.newW / 10;
		height = Surface.newH / 12;
		this.x = area + area * (this.pos - 1) + area / 2 - width / 2;

		g2.setColor(Color.ORANGE);
		RoundRectangle2D.Double rect = new RoundRectangle2D.Double(x, y, width, height, 10, 20);

		RoundRectangle2D.Double wheelR = new RoundRectangle2D.Double(x + width / 10, y - height / 4, width / 3,
				height / 2, 50, 5);
		RoundRectangle2D.Double wheelL = new RoundRectangle2D.Double(x + width / 10, y + height * 3 / 4, width / 3,
				height / 2, 50, 5);

		if (this.thereIsSonar) {
			AffineTransform at = new AffineTransform();
			at.translate(x + width / 2, y + height / 2);
			at.rotate(-Math.PI / 2);
			at.translate(-x - width / 2, -y - height / 2);

			g2.setColor(new Color(82, 82, 82));
			g2.fill(at.createTransformedShape(wheelL));
			g2.fill(at.createTransformedShape(wheelR));
			g2.setColor(Color.ORANGE);
			g2.fill(at.createTransformedShape(rect));

		} else {
			g2.setColor(new Color(82, 82, 82));
			g2.fill(wheelL);
			g2.fill(wheelR);
			g2.setColor(Color.ORANGE);
			g2.fill(rect);

		}

		if (this.camera) {
			int w = 106;
			int h = 87;
			double imgw = newW / 2000;
			double x = area + area * (this.pos - 1) + area / 2 - w * imgw / 2;

			g2.drawImage(img, (int) x, (int) (y - height * 2), (int) (w * imgw), (int) (h * imgw), null);
		}

	}

	public void turnLeft() {
		this.thereIsSonar = true;

	}

	public void turnForward() {
		this.thereIsSonar = false;
		this.camera = false;
	}

	public void returnPos() {
		this.thereIsSonar = false;
	}

	public boolean thereIsSonar() {
		return this.thereIsSonar;
	}

	public void camera() {
		this.camera = true;
	}

	public boolean getCamera() {
		return this.camera;
	}

}
