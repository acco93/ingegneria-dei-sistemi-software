package it.unibo.visualinterface.visual;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.geom.Line2D;


public class VisualObstacle {

	private int pos;
	private Graphics2D g2;
	private boolean thereIs;
	private double width;
	private double height;
	private double distance;
	private double x;

	public VisualObstacle() {
		this.thereIs = false;
		this.distance = 1;
	}

	public void addGraphics(Graphics2D g2) {
		this.g2 = g2;

	}

	public void setPosition(int distance, int pos, boolean thereIs) {

		this.thereIs = thereIs;
		if (!this.thereIs) {
			this.distance = 1;
		} else {
			this.distance = distance / 4;
		}
		this.pos = pos;

	}

	public void draw() {
		if (this.thereIs) {

			double newW = Surface.newW;

			double y = this.distance * Surface.newH / 90;
			double area = newW / (Surface.totSensor + 2);
			width = Surface.newW / 10;
			height = Surface.newH / 12;
			this.x = area + area * (this.pos - 1) + area / 2 - width / 2;

			g2.setColor(Color.RED);
			
			Stroke defaultStroke = g2.getStroke();
			g2.setStroke(new BasicStroke(4f));
			g2.draw(new Line2D.Double(x, y, x + width, y + height));
			g2.draw(new Line2D.Double(x, y + height, x + width, y));
			
			g2.setStroke(defaultStroke);
		}

	}

}
