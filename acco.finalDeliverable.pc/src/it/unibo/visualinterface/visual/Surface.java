package it.unibo.visualinterface.visual;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;

import java.awt.RenderingHints;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.geom.RoundRectangle2D;

import java.util.ArrayList;
import java.util.List;


import javax.swing.JComponent;

import it.unibo.userinterface.Sonar;


public class Surface extends JComponent implements IGraphics {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private VisualRobot robot;
	private VisualObstacle obstacle;
	private String exp;
	private List<VisualSensor> sensors = null;
	

	public final static int staticWidth = 800;
	public final static int staticHeight = 500;
	public static double newW, newH;

	public static int totSensor = 3;

	public Surface() {
		//super(true);
		this.setPreferredSize(new Dimension(staticWidth, staticHeight));

		
		this.addComponentListener(new ComponentAdapter() {
			@Override
			public void componentResized(ComponentEvent e) {
				// w = Surface.this.getSize().getWidth();
				Surface.this.setAspectRatio(Surface.this.getSize().getHeight());
				Surface.this.repaint();
			}
		});
		

		this.robot = new VisualRobot();
		this.obstacle = new VisualObstacle();

		
		this.exp = "";

		// test only
//		this.sensors = new ArrayList<VisualSensor>();
//
//		for (int i = 0; i < totSensor; i++) {
//			this.sensors.add(new VisualSensor(i + 1));
//		}
		
//		Sonar s1 = new Sonar(0);
//		Sonar s2 = new Sonar(1);
//		Sonar s3 = new Sonar(2);
//		List<Sonar> list  = new ArrayList<>();
//		
//		list.add(s1);
//		list.add(s2);
//		list.add(s3);
//		this.setSonars(list);

	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		Graphics2D g2D = (Graphics2D) g;

		this.robot.addGraphics(g2D);
		this.obstacle.addGraphics(g2D);

		if (this.sensors != null) {
			for (VisualSensor visualSensor : sensors) {
				visualSensor.addGraphics(g2D);
			}
		}

		g2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2D.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);

		// draw area B
		g2D.setColor(Color.LIGHT_GRAY);
		g2D.setColor(new Color(189,242,234));
		double areaH = Surface.newH - Surface.newH / 3;
		double areaW = Surface.newW / 8;
		g2D.fill(new RoundRectangle2D.Double(Surface.newW - Surface.newW / 9, Surface.newH / 10, areaW, areaH, 5, 5));
		g2D.setColor(Color.BLACK);
		Font font = new Font("Sans-Serif", Font.PLAIN, (int) Surface.newW / 30);
		g2D.setFont(font);

		int strW = g2D.getFontMetrics().stringWidth("B");
		g2D.drawString("B", (int) (Surface.newW - Surface.newW / 9 + strW / 2), (int) (Surface.newH / 10 + strW * 2));

		if (this.sensors != null) {
			for (VisualSensor visualSensor : sensors) {
				visualSensor.draw();
			}
		}

		this.robot.draw();
		
		
		
		this.obstacle.draw();

		g2D.setColor(new Color(46,152,136));
		g2D.fill(new RoundRectangle2D.Double(Surface.newW / 90, Surface.newH / 90, Surface.newW, Surface.newH / 20, 5,
				5));
		g2D.setColor(Color.BLACK);
		g2D.draw(new RoundRectangle2D.Double(Surface.newW / 90, Surface.newH / 90, Surface.newW, Surface.newH / 20, 5,
				5));

		int AW = g2D.getFontMetrics().stringWidth("A");
		g2D.drawString("A", (int) (Surface.newW / 9 + AW / 2), (int) (Surface.newH / 10 + AW * 2));

		// draw exp
		int expw = g2D.getFontMetrics().stringWidth(this.exp);
		g2D.drawString(exp, (int) (Surface.newW / 2 - expw / 2), (int) (Surface.newH - Surface.newH / 4 + +  Surface.newH / 6 ));

		g2D.dispose();
	}


	private void setAspectRatio(double h) {
		newW = Surface.staticWidth;
		newH = Surface.staticHeight;

		newH = h;

		newW = (newH * Surface.staticWidth) / Surface.staticHeight;
	}

	@Override
	public void setSonars(List<Sonar> sonars) {
		this.sensors = new ArrayList<VisualSensor>();
		int i=0;
		for (Sonar sonar : sonars) {
			i++;
			this.sensors.add(new VisualSensor(i));
			this.setSonarData(sonar.getLastValue(), i);
		}
		Surface.totSensor = this.sensors.size();
		this.repaint();

	}
	
	@Override
	public void setSonarData(int distance, int pos) {

		for (VisualSensor visualSensor : sensors) {
			visualSensor.resetDistance();
		}
		this.sensors.get(pos-1).setRectDistance(distance);
		this.repaint();
	}

	@Override
	public void setRobotPos(int pos) {

		this.robot.turnForward();
		this.robot.setLocation(pos);
		this.repaint();

	}

	@Override
	public void setObstacle(int distance, int pos, boolean thereIs) {
		this.obstacle.setPosition(distance, pos, thereIs);
		if (thereIs) {
			this.setSonarData(distance, pos);
		} else {
			this.setSonarData(1, pos);
		}
		this.repaint();

	}

	@Override
	public void printExpValue(String exp) {
		this.exp = exp;
		this.repaint();

	}

	@Override
	public void takePhoto(int pos) {
		this.robot.setLocation(pos);
		this.robot.camera();
		this.repaint();
	}

	@Override
	public void turnLeft(int pos) {
		this.robot.setLocation(pos);
		this.robot.turnLeft();
		this.repaint();
	}

	@Override
	public void turnForward(int pos) {
		this.robot.setLocation(pos);
		this.robot.turnForward();
		this.repaint();
		
	}

}