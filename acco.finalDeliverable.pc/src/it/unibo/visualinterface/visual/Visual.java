package it.unibo.visualinterface.visual;


import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import it.unibo.userinterface.Sonar;

public class Visual extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JButton btn,btn2,btn3;
	private JTextField text;
	private JButton btn4;
	private JButton btn6;
	private JButton btn5;
	private JTextField text2;
	private JButton btn7;

	public Visual() {

		this.setTitle("FrameDemo");

		// 2. Optional: What happens when the frame closes?
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		System.setProperty("sun.java2d.opengl", "True");

		this.setSize(new Dimension(800, 500));

		// Set the background to black.
	//	this.setBackground(Color.BLACK);

	
		setLocationRelativeTo(null);

		this.setVisible(true);

		Surface surface = new Surface();
		JPanel panel = new JPanel();
		this.btn = new JButton("setSensor");
		this.text = new JTextField("3");
		this.btn2 = new JButton("stepForward");
		this.text2 = new JTextField("1");
		this.btn3 = new JButton("turnLeft");
		this.btn4 = new JButton("setobstacle");
		this.btn7 = new JButton("deleteobstacle");
		this.btn5 = new JButton("setrobotpos");
		this.btn6= new JButton("takePhoto");
		this.btn.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent event) {

				int val = Integer.parseInt(text.getText());
				
				List<Sonar> list = new ArrayList<>();
				for(int i=0;i<val;i++){
					list.add(new Sonar(i));
				}
				surface.setSonars(list);
			}
		});
		
		this.btn5.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent event) {

				int val = Integer.parseInt(text2.getText());
				surface.setRobotPos(val);
				surface.setSonarData(150, val);
			}
		});
		
		this.btn3.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent event) {

				int val = Integer.parseInt(text2.getText());
				surface.turnLeft(val);
			}
		});
		
		this.btn2.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent event) {

				int val = Integer.parseInt(text2.getText());
				surface.turnForward(val);
			}
		});
		
		this.btn4.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent event) {

				int val = Integer.parseInt(text2.getText());
				surface.setObstacle(150, val, true);
			}
		});
		
		this.btn7.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent event) {

				int val = Integer.parseInt(text2.getText());
				surface.setObstacle(150, val, false);
			}
		});
		
		this.btn6.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent event) {

				int val = Integer.parseInt(text2.getText());
				surface.takePhoto(val);
			}
		});

		
		panel.add(btn);
		panel.add(text);
		panel.add(btn2);
		panel.add(btn3);
		panel.add(btn4);
		panel.add(btn5);
		panel.add(btn6);
		panel.add(btn7);
		panel.add(text2);
		
		panel.add(surface);
		this.add(panel);

		this.setMinimumSize(this.getPreferredSize());
		// Show the view window.
		setVisible(true);
	}
	

}
