package it.unibo.visualinterface.visual;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import java.awt.geom.RoundRectangle2D;

public class VisualSensor {

	private double x;
	private double width;
	private int number;
	private Graphics2D g2;
	private double distance;

	public VisualSensor(int num) {
		this.number = num;
		this.distance = 1;

	}

	public void addGraphics(Graphics2D g2) {
		this.g2 = g2;
	}

	public void draw() {

		// double misure = Surface.newW /8;

		double newW = Surface.newW; // - misure*2;

		double y = Surface.newH - Surface.newH / 4;
		double height = Surface.newH / 15;
		width = height;
		double area = newW / (Surface.totSensor + 2);
		x = area + area * (number - 1) + area / 2 - width / 2;
		// x += misure;

		this.drawRect(x, y, width, height);

		g2.setColor(new Color(65,194,48));
		g2.fill(new RoundRectangle2D.Double(x, y, width, height,10,10));
		Font font = new Font("Sans-Serif", Font.PLAIN, (int) newW / 30);
		g2.setFont(font);
		g2.setColor(Color.BLACK);
		int strW = g2.getFontMetrics().stringWidth("S" + number);

		g2.drawString("S" + number, (int) (x + width / 2 - strW / 2), (int) (y + height / 2 + Surface.newH / 80));

	}

	public double getPos() {
		return x + width / 2;
	}

	private void drawRect(double x, double y, double width, double height) {

		Stroke def = g2.getStroke();
		Stroke dotted = new BasicStroke(1, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 0, new float[] { 1, 2 }, 0);
		g2.setStroke(dotted);

		g2.setColor(Color.BLACK);

		g2.draw(new Line2D.Double(x + width / 2, y + height / 2, x + width / 2, this.distance * Surface.newH / 90));
		g2.setStroke(def);
	}

	public void setRectDistance(int distance) {
		this.distance = distance / 4;
	}

	public void resetDistance() {
		this.distance = 1;
	}

}
