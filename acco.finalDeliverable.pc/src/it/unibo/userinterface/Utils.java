package it.unibo.userinterface;

public class Utils {

	/*
	 * Distance measured by the sonars when there are no obstacles.
	 */
	private static final int SONAR_DEFAULT_DISTANCE = 95;

	/*
	 * The robot should run in a path located between 25 to 75 cm from the
	 * sonars
	 */
	private static final int ROBOT_MAX_DISTANCE = 65;
	private static final int ROBOT_MIN_DISTANCE = 20;

	
	public static int filterDistance(int distance) {
		int filteredDistance = distance;

		if (filteredDistance > SONAR_DEFAULT_DISTANCE) {
			filteredDistance = SONAR_DEFAULT_DISTANCE;
		}

		return filteredDistance;
	}

	public static double computeSimilarity(int distanceA, int distanceB) {
		return 1 - (Math.abs(distanceA - distanceB) / (double) SONAR_DEFAULT_DISTANCE);
	}
	
	/**
	 * The robot should run between ROBOT_MIN_DISTANCE and ROBOT_MAX_DISTANCE.
	 * 
	 * @param distance
	 * @return
	 */
	public static boolean isRobot(int distance) {

		return distance > ROBOT_MIN_DISTANCE && distance < ROBOT_MAX_DISTANCE;
	}

	/**
	 * Everything that is outside robot limits is an obstacle.
	 * 
	 * @param distance
	 * @return
	 */
	public static boolean isObstacle(int distance) {
		return distance < ROBOT_MIN_DISTANCE || 
				(distance > ROBOT_MAX_DISTANCE && distance < SONAR_DEFAULT_DISTANCE-10);

	}
	





}
