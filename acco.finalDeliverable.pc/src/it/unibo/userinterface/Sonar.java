package it.unibo.userinterface;

/**
 * Sonar representation.
 * @author acco
 *
 */
public class Sonar {

	private int position;

	private int lastValue;
	
	
	public Sonar(int position){
		this.position = position;
		this.lastValue = -1;
	}

	public int getPosition() {
		return position;
	}

	public int getLastValue() {
		return lastValue;
	}

	public void setLastValue(int lastValue) {
		this.lastValue = lastValue;
	}

	@Override
	public String toString() {
		return "Sonar [position=" + position + ", lastValue=" + lastValue + "]";
	}

	
	
}
