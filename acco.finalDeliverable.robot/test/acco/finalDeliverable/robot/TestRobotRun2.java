package acco.finalDeliverable.robot;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import alice.tuprolog.Prolog;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.QActorContext;
import it.unibo.qactors.QActorUtils;
import it.unibo.qactors.akka.QActor;
import it.unibo.qloop.Qloop;
import it.unibo.qloop.Qloop.RobotAction;
import it.unibo.system.SituatedSysKb;

public class TestRobotRun2 {

	private IOutputEnvView stdOut = SituatedSysKb.standardOutEnvView;
	private QActorContext ctx;
	private QActor qa;
	private Prolog pengine;

	@Before
	public void runBefore() {
		System.out.println("SETTING UP JUNIT");
		try {
			ctx = QActorContext.initQActorSystem("robotcontext", "./srcMore/it/unibo/robotcontext/robotsystem.pl",
					"./src-gen/it/unibo/robotcontext/sysRules.pl", stdOut, null, true);
			assertTrue("setUp", ctx != null);
			qa = QActorUtils.getQActor("qloop");
			assertTrue("setUp", qa != null);
			pengine = qa.getPrologEngine();
			assertTrue("setUp", pengine != null);
		} catch (Exception e) {
			System.out.println("setUp ERROR " + e.getMessage());
			fail("setUp " + e.getMessage());
		}
	}

	@After
	public void runAfter() throws InterruptedException {
		qa.terminate();
		ctx.terminateQActorSystem();
		pengine = null;
		Thread.sleep(5000);
	}

	@Test
	public void testRun2() throws Exception {

		Thread.sleep(1000);
		// handle stop command while going forward
		assertTrue("check robot action", (RobotAction) this.invoke("getCurrentAction") == RobotAction.FORWARD);
		this.emitEvent("inputcmd", "usercmd(executeInput(stop))");
		assertTrue("check robot action", (RobotAction) this.invoke("getCurrentAction") == RobotAction.STOP);

	}

	private void emitEvent(String id, String payload) throws InterruptedException {

		QActorUtils.emitEventAfterTime(ctx, "emitter", id, payload, 0);
		Thread.sleep(1000);

	}

	private Object invoke(String methodName) throws Exception {
		QActor qactrl = QActorUtils.getQActor("qloop_ctrl");
		return qa.getByReflection(Qloop.class, methodName).invoke(qactrl);
	}

}
