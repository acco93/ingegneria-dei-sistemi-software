package acco.finalDeliverable.robot;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.concurrent.TimeoutException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import alice.tuprolog.Prolog;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.QActorContext;
import it.unibo.qactors.QActorUtils;
import it.unibo.qactors.akka.QActor;
import it.unibo.qloop.Qloop;
import it.unibo.qloop.Qloop.RobotAction;
import it.unibo.system.SituatedSysKb;

public class TestRobotRunA {

	private IOutputEnvView stdOut = SituatedSysKb.standardOutEnvView;
	private QActorContext ctx;
	private QActor qa;
	private Prolog pengine;

	@Before
	public void runBefore() throws InterruptedException {
		Thread.sleep(5000);
		System.out.println("SETTING UP JUNIT");
		try {
			ctx = QActorContext.initQActorSystem("robotcontext", "./srcMore/it/unibo/robotcontext/robotsystem.pl",
					"./src-gen/it/unibo/robotcontext/sysRules.pl", stdOut, null, true);
			assertTrue("setUp", ctx != null);
			qa = QActorUtils.getQActor("qloop");
			assertTrue("setUp", qa != null);
			pengine = qa.getPrologEngine();
			assertTrue("setUp", pengine != null);
		} catch (Exception e) {
			System.out.println("setUp ERROR " + e.getMessage());
			fail("setUp " + e.getMessage());
		}		
	}

	@After
	public void runAfter() throws InterruptedException, TimeoutException{
		qa.terminate();
		ctx.terminateQActorSystem();
		pengine = null;		
	}
	

	@Test
	public void testRun1() throws Exception {

		Thread.sleep(2000);
		
		// at the beginning the robot should move forward
		this.emitEvent("sensordata", "sensordata(distance(100, null, null))");
		
		Thread.sleep(2000);
		assertTrue("check robot action", (RobotAction) this.invoke("getCurrentAction") == RobotAction.FORWARD);

		// the led should be off
		assertFalse("check led", (Boolean) this.invoke("isBlinking"));

		// no pictures acquired
		assertTrue("pictures acquired", (Integer) this.invoke("getPicturesAcquired") == 0);
		
		// test obstacle
		this.emitEvent("obstacle", "obstacle(5)");
		assertTrue("check obstacle", (RobotAction) this.invoke("getCurrentAction") == RobotAction.WAIT);

		// obstacle is still there
		this.emitEvent("sensordata", "sensordata(distance(9, null, null))");
		assertTrue("obstacle still here", (RobotAction) this.invoke("getCurrentAction") == RobotAction.WAIT);

		// obstacle vanished, it may be the cat tail
		this.emitEvent("sensordata", "sensordata(distance(88, null, null))");
		assertTrue("obstacle vanished", (RobotAction) this.invoke("getCurrentAction") == RobotAction.FORWARD);

		// sonar reached
		this.emitEvent("sonarReached", "sonarReached");
		// wait for the robot to turn
		Thread.sleep(1000);
		// the led should blink
		assertTrue("check led", (Boolean) this.invoke("isBlinking"));
		// wait some time for the robot to take the picture
		Thread.sleep(2000);
		assertFalse("check led", (Boolean) this.invoke("isBlinking"));
		assertTrue("pictures acquired", (Integer) this.invoke("getPicturesAcquired") == 1);

		// handle stop command while taking photo
		// sonar reached
		this.emitEvent("sonarReached", "sonarReached");
		this.emitEvent("inputcmd", "usercmd(executeInput(stop))");
		// wait some time for the robot to take the picture
		Thread.sleep(5000);
		assertTrue("pictures acquired", (Integer) this.invoke("getPicturesAcquired") == 2);
		assertTrue("check robot action", (RobotAction) this.invoke("getCurrentAction") == RobotAction.STOP);


	}
 


	private void emitEvent(String id, String payload) throws Exception {

		QActorUtils.raiseEvent(ctx, "emitter", id, payload);
		Thread.sleep(1000);

	}

	private Object invoke(String methodName) throws Exception {
		QActor qactrl = QActorUtils.getQActor("qloop_ctrl");
		return qa.getByReflection(Qloop.class, methodName).invoke(qactrl);
	}

}
