package acco.finalDeliverable.robot;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import alice.tuprolog.Prolog;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.QActorContext;
import it.unibo.qactors.QActorUtils;
import it.unibo.qactors.akka.QActor;
import it.unibo.qloop.Qloop;
import it.unibo.qloop.Qloop.RobotAction;
import it.unibo.system.SituatedSysKb;

public class TestRobotRun3 {
	private IOutputEnvView stdOut = SituatedSysKb.standardOutEnvView;
	private QActorContext ctx;
	private QActor qa;
	private Prolog pengine;

	@Before
	public void runBefore() {
		System.out.println("SETTING UP JUNIT");
		try {
			ctx = QActorContext.initQActorSystem("robotcontext", "./srcMore/it/unibo/robotcontext/robotsystem.pl",
					"./src-gen/it/unibo/robotcontext/sysRules.pl", stdOut, null, true);
			assertTrue("setUp", ctx != null);
			qa = QActorUtils.getQActor("qloop");
			assertTrue("setUp", qa != null);
			pengine = qa.getPrologEngine();
			assertTrue("setUp", pengine != null);
		} catch (Exception e) {
			System.out.println("setUp ERROR " + e.getMessage());
			fail("setUp " + e.getMessage());
		}
	}

	@After
	public void runAfter() throws InterruptedException {
		qa.terminate();
		ctx.terminateQActorSystem();
		pengine = null;

	}

	@Test
	public void testRun3() throws Exception {

		Thread.sleep(1000);
		// handle stop command while waiting for road to free
		assertTrue("check robot action", (RobotAction) this.invoke("getCurrentAction") == RobotAction.FORWARD);

		// test obstacle
		this.emitEvent("obstacle", "obstacle(5)");
		assertTrue("check obstacle", (RobotAction) this.invoke("getCurrentAction") == RobotAction.WAIT);

		// send stop command
		this.emitEvent("inputcmd", "usercmd(executeInput(stop))");
		System.out.println((RobotAction) this.invoke("getCurrentAction"));
		assertTrue("check robot action", (RobotAction) this.invoke("getCurrentAction") == RobotAction.STOP);

	}

	private void emitEvent(String id, String payload) throws InterruptedException {

		QActorUtils.emitEventAfterTime(ctx, "emitter", id, payload, 0);
		Thread.sleep(1000);

	}

	private Object invoke(String methodName) throws Exception {
		QActor qactrl = QActorUtils.getQActor("qloop_ctrl");
		return qa.getByReflection(Qloop.class, methodName).invoke(qactrl);
	}

}
