%====================================================================================
% Context robotcontext  SYSTEM-configuration: file it.unibo.robotcontext.robotsystem.pl 
%====================================================================================
context(robotcontext, "localhost",  "TCP", "7898" ).  		 
%%% -------------------------------------------
%%% -------------------------------------------
eventhandler(evh,robotcontext,"it.unibo.robotcontext.Evh","inputcmd").  
%%% -------------------------------------------
qactor( qloop , robotcontext, "it.unibo.qloop.MsgHandle_Qloop" ). 
qactor( qloop_ctrl , robotcontext, "it.unibo.qloop.Qloop" ). 

