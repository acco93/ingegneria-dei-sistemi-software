package it.unibo.action;

import java.util.Calendar;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.system.SituatedPlainObject;

public abstract class AsynchTimedAction extends SituatedPlainObject implements Callable<String>{
 	protected ActionTimerWatch timeWatch ;
 	protected String actionTimedResult = "unknown";
	protected String toutevId ="";
	protected int maxduration;
	protected boolean workingActionSynchFlag = true;
	private Thread localExecutorThread    ;
 	protected boolean suspended = false;
 	protected int timeRemained = 0;	
 	protected long durationMillis = -1;	
 	protected long tStart  = 0;
 	protected Thread myself;
  	protected String result;
 	
 	private ScheduledExecutorService executorService;
 	private Future<String> futureResult;
 	
	public AsynchTimedAction(String name, IOutputEnvView outEnvView, int maxduration)  {
 		super(name,  outEnvView );
    	this.maxduration   = maxduration;
// 		println("AsynchTimedAction CREATED " + this.getName() );
  	}	
	/* 1) ACTIVATE THE   ACTION	 */	
	public String execSynch()    { 
 		Future<String> fResult = execASynch();
		try {
	 	   //forces the caller to wait		
			return fResult.get();
		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
			return "ERROR:"+e.getMessage();
		}
	}
	public Future<String> execASynch()  {
//	    println("	%%% AsynchTimedAction " + getName() + " ACTIVATED with terminationEvId=" + terminationEvId);
		int numberOfCores      = Runtime.getRuntime().availableProcessors();
		executorService        = Executors.newScheduledThreadPool(numberOfCores);
		futureResult           = executorService.submit(this);	//should invoke call
		return futureResult;  
	}
	/* 2) Entry point for the executor  */
	@Override
	public String call() throws Exception {
// 		println("	%%% AsynchTimedAction call " + getName()   );
		/* START A TIMER that calls suspendAction() when terminates*/
		timeWatch = new ActionTimerWatch("", outEnvView,maxduration,this);
		startOfAction(); 
		execTheAction();
		/* GET THE RESULT OF THE ACTION */
		result = getResult();
  		println("	%%% AsynchTimedAction " + getName() +" DONE  "  +  result );
  		notifyTheObservers(result);
 		return result;
	}
	/* 3) starts the Action */
 	protected void startOfAction() throws Exception{
 		tStart = Calendar.getInstance().getTimeInMillis();
 		suspended = false;
 	}	
 	/* 4) execute the action (in a new thread) 
 	 * The current thread waits for the ends or is interrupted by a suspendAction
 	 */
	public void execTheAction() throws Exception {
 		myself             = Thread.currentThread();
		Callable<String> f = getActionBodyAsCallable();
 		localExecutorThread=createLocalThreadExecutor( f );
  		localExecutorThread.start();
 		waitForActionEnd();		
	}	
	
 	/* 5) to be written by the application designer */
	protected abstract Callable<String> getActionBodyAsCallable();	 	//action body
	protected abstract String getApplicationResult()  ;

	
	/* 6) wait for the end of the action localExecutorThread is finished
	 * or react to its suspension (suspendAction called) 
	 */
	public synchronized void waitForActionEnd(){
		try{
			while( workingActionSynchFlag ){
				wait();		
			}
			/* POINT CONTINUE */
 			evalDuration();
// 	  		println("	%%% AsynchTimedAction CONTINUE exectime=" + this.durationMillis);
		}catch(InterruptedException e){
			/* POINT INTERRUPTED */
			evalDuration();
//	  		println("	%%% AsynchTimedAction " + getName() +" suspendAction after:" + this.durationMillis   );
	 		timeWatch.stop();
  			localExecutorThread.interrupt();  //useful for action that sleep, like sound. Otherwise they do not stop
 			try {
				Thread.sleep(10);  //give time to the interrupt to run
			} catch (InterruptedException e1) { e1.printStackTrace();	}
 			localExecutorThread.stop();	//DEPRECATED but working
		}
	}
	/* CALLED BY EXTERNAL AGENTS (e.g. a timer)	*/
	public void suspendAction(){
 		evalDuration(); 
		/* WARNING: Interruption requires the cooperation of the task being interrupted.  */
		if( myself != null ) myself.interrupt(); //goto POINT INTERRUPTED of waitForActionEnd
 	}		

	/* GET TEH ACTION STATE/RESULTS */
	public boolean isSuspended(){ return suspended; 	} 
	
 	protected String getResult() {
 		if( ! this.suspended ){
 			return "action("+name+",working)";
 		}
 		timeWatch.stop();
		timeRemained = (int) (maxduration - durationMillis) ;
		if( timeRemained < 0 ) timeRemained = 0;
  		return getApplicationResult() ;
	}	
    protected void evalDuration(){
		suspended = true;
		if( durationMillis == -1 ){
			 long tEnd = Calendar.getInstance().getTimeInMillis();
			 durationMillis =  tEnd - tStart ;	
//    		 println("	%%% AsynchTimedAction " + getName() + " duration="  +  durationMillis);
		}    	
    }	 
	public int getMaxDuration() { return this.maxduration;	}	 
	public void setMaxDuration(int d) { maxduration =  d;	} 
	public String getResultRep() {
 		return getResult() ;
	}
/*
 * ===========================================================================
 * Support for action execution
 * ===========================================================================
 */
	protected Thread createLocalThreadExecutor( Callable<String> f ){
		workingActionSynchFlag = true;
		return new Thread(){
		public void run(){
			 try{	  
// 				 println("	%%% AsynchTimedAction local thread  f=" + f  );
				 String res = f.call();  //execute the application body
// 				 println("	%%% AsynchTimedAction local thread ENDS   "   );
				 resumeMyTimedAction(res);
 		   	 }catch(Exception e){
//				 println("	%%% AsynchTimedAction local thread " + e.getMessage()   );
 			 }				
		 }//run
	    };
 	}
	protected synchronized void resumeMyTimedAction(String res){
		workingActionSynchFlag = false;
//		println("	%%% AsynchTimedAction resumeMyTimedAction res=" + res  );
		this.notifyAll();	//goto POINT CONTINUE of waitForActionEnd
	}
 }