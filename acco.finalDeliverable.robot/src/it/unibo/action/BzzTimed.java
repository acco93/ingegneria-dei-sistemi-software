package it.unibo.action;

import java.util.concurrent.Callable;

import it.unibo.buzzer.high.IBuzzer;
import it.unibo.buzzer.high.INote;
import it.unibo.buzzer.high.IRhythm;
import it.unibo.is.interfaces.IOutputEnvView;

public class BzzTimed extends AsynchTimedAction {

	private IRhythm rhythm;
	private IBuzzer buzzer;

	public BzzTimed(String name, IOutputEnvView outEnvView, int maxduration, IBuzzer buzzer, IRhythm rhythm) {
		super(name, outEnvView, maxduration);

		this.buzzer = buzzer;
		this.rhythm = rhythm;
	}

	@Override
	protected Callable<String> getActionBodyAsCallable() {
		return new Callable<String>() {
			@Override
			public String call() throws Exception {
				while (rhythm.hasNext()) {
					INote note = rhythm.next();
					if (note.isPause()) {
						buzzer.turnOff();
					} else {
						buzzer.turnOn();
					}

					Thread.sleep(note.getTime());
				}
				return "done";
			}

		};
	}

	@Override
	protected String getApplicationResult() {
		return "actionDone(NAME,T)".replace("NAME", name).replace("T", "" + durationMillis);
	}

	@Override
	public void suspendAction() {
		super.suspendAction();
		this.buzzer.turnOff();
	}

}
