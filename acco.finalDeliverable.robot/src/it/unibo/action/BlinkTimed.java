package it.unibo.action;

import java.util.concurrent.Callable;

import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.led.high.IMultiColorLed;

/*
 * NEW CONCEPT: 
 */
public class BlinkTimed extends AsynchTimedAction  {
private IMultiColorLed led;

	public BlinkTimed(String name,IOutputEnvView outEnvView,int maxduration,IMultiColorLed led) {
		super(name, outEnvView, maxduration);
		this.led = led;
	}
 
  	@Override
	protected String getApplicationResult() { 		
 		return "actionDone(NAME,T)".replace("NAME", name).replace("T", ""+durationMillis);
	}
	@Override
	protected Callable<String> getActionBodyAsCallable() {
 		return new Callable<String>(){
			@Override
			public String call() throws Exception {
				for(int i=1; i<=1000000;i++){
					turnOnLed(i);
					Thread.sleep(500);
	 				led.turnOff();
	 				Thread.sleep(500);
				}
				return "done";
			}

			private void turnOnLed(int i) {
				switch(i%3){
				case 0:
					led.turnOn(true, false, false);
					break;
				case 1:
					led.turnOn(false, true, false);
					break;
				case 2:
					led.turnOn(false, false, true);
					break;
				}
				
			}		
		};
	}
	@Override
	public void suspendAction(){
		led.turnOff();
		super.suspendAction();
   	}		
}
