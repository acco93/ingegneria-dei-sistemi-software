package it.unibo.action;
import it.unibo.is.interfaces.IOutputEnvView;

/*
 *  
 */
public class ActionTimerWatch {
protected int execTime ;
protected String toutEventId;
protected IOutputEnvView outEnvView ;
protected String name;
protected AsynchTimedAction action;
protected Thread myself;

	public ActionTimerWatch (String name, IOutputEnvView outEnvView, int execTime , AsynchTimedAction action ){
		this.name        		= name;
		this.outEnvView  		= outEnvView;
 		this.execTime    		= execTime;
		this.action    			= action;
 		doJob();
 	}
	
  	protected void doJob()   {
 			myself = new Thread(){
 				public void run(){
 			 		try {
 			 			String m = "tout("+execTime+")";
//			 			outEnvView.addOutput("ActionTimerWatch " + name + " STARTS  " +   execTime   );
			 			Thread.sleep( execTime  ); 
 			  			if( ! action.isSuspended() ){			  				
// 	  			 			outEnvView.addOutput("ActionTimerWatch " + name + " ENDS  " +   execTime   );
			  				action.suspendAction();	 
 			  			}
 			 		} catch (Exception e) {
// 			 			outEnvView.addOutput( "	+++ ActionTimerWatch " + name + " interrupted " );
 			  		} 		
				}
 			};
 			myself.start();  			
	}

  	public void stop(){
//  		outEnvView.addOutput( "	+++ ActionTimerWatch " + name + " stop " );
  		myself.interrupt();
  	}
 
}
