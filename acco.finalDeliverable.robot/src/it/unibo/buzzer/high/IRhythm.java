package it.unibo.buzzer.high;

import java.util.Iterator;


/**
 * 
 * A song is thought as an iterable sequence of notes.
 * 
 * @author acco
 *
 */
public interface IRhythm extends Iterator<INote> {
	
	void reset();
	
}
