package it.unibo.buzzer.high;

import java.util.List;

/**
 * 
 * A classic rhythm implementation.
 * 
 * @author acco
 *
 */
public class Rhythm implements IRhythm {

	protected List<INote> notes;
	protected int index;

	public Rhythm(List<INote> notes) {
		this.notes = notes;
		this.index = 0;
	}

	@Override
	public boolean hasNext() {
		return this.index < this.notes.size();
	}

	@Override
	public INote next() {
		return this.notes.get(this.index++);
	}

	@Override
	public void reset() {
		this.index = 0;
	}

}
