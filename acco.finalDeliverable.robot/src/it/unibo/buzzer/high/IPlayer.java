package it.unibo.buzzer.high;

/**
 * 
 * A simple music player interface.
 * 
 * @author acco
 *
 */
public interface IPlayer {

	void play();

	void rewind();
	
}
