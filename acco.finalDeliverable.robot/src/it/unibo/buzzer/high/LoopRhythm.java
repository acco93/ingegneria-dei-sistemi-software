package it.unibo.buzzer.high;

import java.util.List;

/**
 * 
 * A rhythm implementation as an infinite loop.
 * 
 * @author acco
 *
 */
public class LoopRhythm extends Rhythm {

	public LoopRhythm(List<INote> notes) {
		super(notes);
	}

	@Override
	public boolean hasNext() {
		return true;
	}

	@Override
	public INote next() {
		INote currentNote = this.notes.get(this.index);

		this.index = (this.index + 1) % this.notes.size();

		return currentNote;
	}

}
