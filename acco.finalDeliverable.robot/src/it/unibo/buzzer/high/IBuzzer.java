package it.unibo.buzzer.high;

/**
 * 
 * Simple high level buzzer interface.
 * 
 * @author acco
 *
 */
public interface IBuzzer {

	void turnOn();

	void turnOff();
}
