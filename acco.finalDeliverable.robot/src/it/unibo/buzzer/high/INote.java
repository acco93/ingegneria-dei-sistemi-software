package it.unibo.buzzer.high;


/**
 * 
 * Just a simple musical note representation.
 * 
 * @author acco
 *
 */
public interface INote {

	public enum NoteType { SOUND, PAUSE };
	
	boolean isPause();

	boolean isSound();

	int getTime();

	String getName();

}
