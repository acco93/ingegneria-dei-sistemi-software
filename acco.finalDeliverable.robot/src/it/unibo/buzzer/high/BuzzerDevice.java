package it.unibo.buzzer.high;

public class BuzzerDevice implements IBuzzer {

	private IBuzzer buzzer;

	public BuzzerDevice() {

	}

	public BuzzerDevice(IBuzzer buzzer) {
		this.buzzer = buzzer;
	}

	public void setImplementation(IBuzzer buzzer) {
		this.buzzer = buzzer;
	}

	@Override
	public void turnOn() {
		this.buzzer.turnOn();
	}

	@Override
	public void turnOff() {
		this.buzzer.turnOff();
	}

}
