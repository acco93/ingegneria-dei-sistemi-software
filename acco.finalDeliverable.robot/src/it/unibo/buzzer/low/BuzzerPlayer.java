package it.unibo.buzzer.low;

import it.unibo.buzzer.high.IBuzzer;
import it.unibo.buzzer.high.IPlayer;
import it.unibo.buzzer.high.IRhythm;
import it.unibo.buzzer.high.INote;


/**
 * 
 * Player implementation suitable for a buzzer.
 * 
 * @author acco
 *
 */
public class BuzzerPlayer implements IPlayer {

	private IBuzzer buzzer;
	private IRhythm rhythm;

	public BuzzerPlayer(IBuzzer buzzer, IRhythm rhythm) {
		this.buzzer = buzzer;
		this.rhythm = rhythm;
	}

	@Override
	public void play() {

		while (this.rhythm.hasNext()) {
			INote note = this.rhythm.next();
			if (note.isPause()) {
				this.buzzer.turnOff();
			} else {
				this.buzzer.turnOn();
			}

			this.wasteTime(note.getTime());
		}

	}

	@Override
	public void rewind() {
		this.rhythm.reset();
	}

	private void wasteTime(int time) {

		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}

}
