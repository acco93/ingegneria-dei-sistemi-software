package it.unibo.buzzer.low;

import it.unibo.buzzer.high.IBuzzer;

public class VirtualBuzzer implements IBuzzer {

	public VirtualBuzzer(){
		this.turnOff();
	}
	
	@Override
	public void turnOn() {
		System.out.println("buzzer(on)");
	}

	@Override
	public void turnOff() {
		System.out.println("buzzer(off)");
	}

}
