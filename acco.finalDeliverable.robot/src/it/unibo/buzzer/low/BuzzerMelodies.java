package it.unibo.buzzer.low;

import java.util.ArrayList;
import java.util.List;

import it.unibo.buzzer.high.INote;
import it.unibo.buzzer.high.INote.NoteType;
import it.unibo.buzzer.high.IRhythm;
import it.unibo.buzzer.high.LoopRhythm;


/**
 * 
 * Utility class that contains some buzzer melodies.
 * 
 * @author acco
 *
 */
public class BuzzerMelodies {

	public static IRhythm obstacleMelody;
	public static IRhythm workingMelody;

	static {

		List<INote> obstacleNotes = new ArrayList<INote>();
		obstacleNotes.add(new BuzzerNote(NoteType.SOUND, 40));
		obstacleNotes.add(new BuzzerNote(NoteType.PAUSE, 40));
		obstacleMelody = new LoopRhythm(obstacleNotes);
		
		List<INote> workingNotes = new ArrayList<INote>();
		workingNotes.add(new BuzzerNote(NoteType.SOUND, 300));
		workingNotes.add(new BuzzerNote(NoteType.PAUSE, 100));
		workingNotes.add(new BuzzerNote(NoteType.SOUND, 240));
		workingNotes.add(new BuzzerNote(NoteType.PAUSE, 70));
		workingNotes.add(new BuzzerNote(NoteType.SOUND, 75));
		workingNotes.add(new BuzzerNote(NoteType.PAUSE, 50));
		workingMelody = new LoopRhythm(workingNotes);

	}

}
