package it.unibo.buzzer.low;

import it.unibo.buzzer.high.INote;

/**
 * 
 * Note implementation suitable for a buzzer.
 * 
 * @author acco
 *
 */
public class BuzzerNote implements INote {

	private boolean sound;
	private int time;

	public BuzzerNote(NoteType type, int time) {
		if (type == NoteType.SOUND) {
			this.sound = true;
		} else {
			this.sound = false;
		}
		this.time = time;
	}

	@Override
	public boolean isPause() {
		return !this.sound;
	}

	@Override
	public boolean isSound() {
		return this.sound;
	}

	@Override
	public int getTime() {
		return this.time;
	}

	@Override
	public String getName() {
		return "BuzzerNote";
	}

}
