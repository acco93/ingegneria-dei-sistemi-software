package it.unibo.buzzer.low;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.Pin;

import it.unibo.buzzer.high.IBuzzer;

/**
 * 
 * Physical buzzer handled using Pi4J.
 * 
 * @author acco
 *
 */
public class Pi4JPassiveBuzzer implements IBuzzer {

	private GpioPinDigitalOutput pin;

	public Pi4JPassiveBuzzer(Pin gpioPin) {
		final GpioController gpio = GpioFactory.getInstance();
		this.pin = gpio.provisionDigitalOutputPin(gpioPin);
		this.turnOff();
	}

	public void turnOn() {
		this.pin.low();
	}

	public void turnOff() {
		this.pin.high();
	}

}
