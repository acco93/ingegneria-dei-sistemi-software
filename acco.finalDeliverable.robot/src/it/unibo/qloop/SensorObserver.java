/* Generated by AN DISI Unibo */
package it.unibo.qloop;

import it.unibo.iot.models.sensorData.ISensorData;
import it.unibo.iot.sensors.ISensorObserver;
import it.unibo.is.interfaces.IOutputView;
import it.unibo.qactors.akka.QActor;
import it.unibo.system.SituatedPlainObject;

public class SensorObserver<T extends ISensorData> extends SituatedPlainObject implements ISensorObserver<T> {

	public SensorObserver(QActor actor, IOutputView outView) {
		super(outView);

	}

	@Override
	public void notify(T data) {

	}

	protected void handleData(T data) throws Exception {

	}

	protected void handleDataViaProlog(T data) {

	}
}
