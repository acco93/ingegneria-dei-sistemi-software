package it.unibo.camera.high;

import java.io.IOException;

import com.hopding.jrpicam.RPiCamera;

/**
 * 
 * High level camera implementation.
 * @author acco
 *
 */

public class CameraDevice implements ICamera {

	private RPiCamera camera;

	public CameraDevice() {
		this.camera = null;
	}

	public CameraDevice(RPiCamera camera) {
		this.camera = camera;
	}

	public void setImplementation(RPiCamera camera) {
		this.camera = camera;
	}

	@Override
	public void takeStill(String filePath) {

		System.out.println("Taking a picture");

		if (camera == null) {
			return;
		}

		try {
			this.camera.takeStill(filePath);
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}
	}

}
