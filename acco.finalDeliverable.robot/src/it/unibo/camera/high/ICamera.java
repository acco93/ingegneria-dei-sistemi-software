package it.unibo.camera.high;


/**
 * 
 * High level camera representation.
 * 
 * @author acco
 *
 */
public interface ICamera {

	public void takeStill(String filePath);
	
}
