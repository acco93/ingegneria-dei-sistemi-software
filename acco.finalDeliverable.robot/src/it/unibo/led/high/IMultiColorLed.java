package it.unibo.led.high;

/**
 * 
 * Multicolor led interface.
 * 
 * @author acco
 *
 */
public interface IMultiColorLed {

	public void turnOn(boolean r, boolean g, boolean b);
	
	public void turnOff();

	public boolean getState();
	
}
