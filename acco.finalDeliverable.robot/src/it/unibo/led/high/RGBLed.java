package it.unibo.led.high;

import it.unibo.led.low.IMultiColorLedImpl;

/**
 * 
 * A possible multicolor led, still technology independent.
 * 
 * @author acco
 *
 */
public class RGBLed implements IMultiColorLed {

	private IMultiColorLed concreteLed;

	public RGBLed() {
		this.concreteLed = null;
	}

	public RGBLed(IMultiColorLedImpl concreteLed) {
		this.concreteLed = concreteLed;
	}

	public void setImplementation(IMultiColorLed ledImpl) {
		this.concreteLed = ledImpl;
	}

	@Override
	public void turnOn(boolean r, boolean g, boolean b) {
		if (this.concreteLed != null) {
			this.concreteLed.turnOn(r, g, b);
		}
	}

	@Override
	public void turnOff() {
		if (this.concreteLed != null) {
			this.concreteLed.turnOff();
		}
	}

	@Override
	public boolean getState() {
		if (this.concreteLed != null) {
			return this.concreteLed.getState();
		} else {
			return false;
		}
	}

}
