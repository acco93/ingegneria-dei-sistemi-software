package it.unibo.led.low;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.Pin;


/**
 * 
 * Physical RGB led handled using Pi4J.
 * 
 * @author acco
 *
 */
public class Pi4JRGBLed implements IMultiColorLedImpl {

	private GpioPinDigitalOutput redPin;
	private GpioPinDigitalOutput greenPin;
	private GpioPinDigitalOutput bluePin;

	private boolean on = false;

	public Pi4JRGBLed(Pin gpioRedPin,Pin gpioGreenPin,Pin gpioBluePin ) {
		// create gpio controller
		final GpioController gpio = GpioFactory.getInstance();
		redPin = gpio.provisionDigitalOutputPin(gpioRedPin);
		greenPin = gpio.provisionDigitalOutputPin(gpioGreenPin);
		bluePin = gpio.provisionDigitalOutputPin(gpioBluePin);
		this.turnOff();
	}

	@Override
	public void turnOn(boolean r, boolean g, boolean b) {
		on = true;
		set(redPin, r);
		set(greenPin, g);
		set(bluePin, b);

	}

	private void set(GpioPinDigitalOutput pin, boolean state) {
		if (state) {
			pin.low();
		} else {
			pin.high();
		}
	}

	@Override
	public void turnOff() {
		on = false;
		redPin.high();
		greenPin.high();
		bluePin.high();
	}

	@Override
	public boolean getState() {
		return this.on;
	}

}
