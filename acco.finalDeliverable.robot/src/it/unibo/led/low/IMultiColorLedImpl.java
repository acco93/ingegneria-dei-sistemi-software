package it.unibo.led.low;

import it.unibo.led.high.IMultiColorLed;

/**
 * 
 * A multicolor led implementation interface (following the bridge pattern).
 * 
 * @author acco
 *
 */

public interface IMultiColorLedImpl extends IMultiColorLed {

}
