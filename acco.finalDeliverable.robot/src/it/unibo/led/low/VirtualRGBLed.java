package it.unibo.led.low;

import java.awt.Color;
import java.awt.Panel;

import it.unibo.led.high.IMultiColorLed;

public class VirtualRGBLed implements IMultiColorLed {

	private boolean on;

	public VirtualRGBLed() {
		this.turnOff();
	}

	@Override
	public void turnOn(boolean r, boolean g, boolean b) {

		this.on = true;

		System.out.println("Led(" + r + "," + g + "," + b + ")");

	}

	@Override
	public void turnOff() {
		this.on = false;
		System.out.println("Led(off)");
	}

	@Override
	public boolean getState() {
		return this.on;
	}

}
