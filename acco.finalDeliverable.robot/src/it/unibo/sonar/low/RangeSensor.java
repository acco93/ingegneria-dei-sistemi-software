package it.unibo.sonar.low;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.PinPullResistance;
import com.pi4j.io.gpio.PinState;

public class RangeSensor {

	GpioPinDigitalOutput firepulse;
	GpioPinDigitalInput result_pin;

	public RangeSensor(Pin trigger, Pin echo) {

		final GpioController gpio = GpioFactory.getInstance();

		// range sensor pins
		firepulse = gpio.provisionDigitalOutputPin(trigger, "Sensor Trigger", PinState.LOW);

		result_pin = gpio.provisionDigitalInputPin(echo, "Sensor Result", PinPullResistance.PULL_DOWN);

	}

	/**
	 * Trigger the Range Sensor and return the result
	 */
	public double getRange() {

		long start = 0;
		long diff = 0;

		try {
			firepulse.high();
			Thread.sleep(10);
			firepulse.low();

			while (result_pin.isLow()) {
				start = System.nanoTime();
			}

			while (result_pin.isHigh()) {
			}

			diff = (System.nanoTime() - start) / 58000;

			return diff;
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return -1;
	}
}
