package it.unibo.sonar.low;

import java.util.concurrent.Callable;

import it.unibo.action.AsynchTimedAction;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.QActorUtils;
import it.unibo.qactors.akka.QActor;

public class AsynchSonarData extends AsynchTimedAction {

	private RangeSensor sonar;
	private QActor robot;

	public AsynchSonarData(String name, IOutputEnvView outEnvView, int maxduration, RangeSensor sonar, QActor robot) {
		super(name, outEnvView, maxduration);
		this.sonar = sonar;
		this.robot = robot;
	}

	@Override
	protected Callable<String> getActionBodyAsCallable() {

		return new Callable<String>() {
			@Override
			public String call() throws Exception {
				while (true) {
					int distance = (int) sonar.getRange();

					if (distance < 10) {
						QActorUtils.raiseEvent(robot.getQActorContext(), "sensor", "obstacle",
								"obstacle(" + distance + ")");
					}

					QActorUtils.raiseEvent(robot.getQActorContext(), "sensor", "sensordata",
							"sensordata(distance(" + distance + ", dontcare, dontcare))");

					Thread.sleep(100);

				}

			}
		};

	}

	@Override
	protected String getApplicationResult() {
		return "actionDone(NAME,T)".replace("NAME", name).replace("T", "" + durationMillis);
	}

}
