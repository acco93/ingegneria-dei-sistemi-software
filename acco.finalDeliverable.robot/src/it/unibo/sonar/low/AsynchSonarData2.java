package it.unibo.sonar.low;

import it.unibo.qactors.QActorUtils;
import it.unibo.qactors.akka.QActor;

public class AsynchSonarData2 extends Thread {

	private RangeSensor sonar;
	private QActor robot;

	public AsynchSonarData2(RangeSensor sonar, QActor robot) {

		this.sonar = sonar;
		this.robot = robot;

	}

	@Override
	public void run() {
		while (true) {
			int distance = (int) sonar.getRange();

			// System.out.println(distance);

			try {
				if (distance < 10) {
					QActorUtils.raiseEvent(robot.getQActorContext(), "sensor", "obstacle",
							"obstacle(" + distance + ")");
				}

				QActorUtils.raiseEvent(robot.getQActorContext(), "sensor", "sensordata",
						"sensordata(distance(" + distance + ", dontcare, dontcare))");

				Thread.sleep(150);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

}
