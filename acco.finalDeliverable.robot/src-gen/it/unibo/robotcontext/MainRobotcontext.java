/* Generated by AN DISI Unibo */ 
package it.unibo.robotcontext;
import it.unibo.qactors.QActorContext;
import java.io.InputStream;
import alice.tuprolog.SolveInfo;
import alice.tuprolog.Term;
import alice.tuprolog.Var;
 
import it.unibo.is.interfaces.IBasicEnvAwt;
import it.unibo.is.interfaces.IIntent;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.system.SituatedSysKb;

public class MainRobotcontext   {
private IBasicEnvAwt env; 
private it.unibo.qactor.robot.RobotActor robot; 
 
 	
/*
* ----------------------------------------------
* MAIN
* ----------------------------------------------
*/
 
	public static void main(String[] args) throws Exception{
			IOutputEnvView outEnvView = SituatedSysKb.standardOutEnvView;
			it.unibo.qactors.QActorUtils.setRobotBase("loop" );  
		    String webDir = "./srcMore/it/unibo/robotcontext";
			QActorContext.initQActorSystem(
				"robotcontext", "./srcMore/it/unibo/robotcontext/robotsystem.pl", 
				"./srcMore/it/unibo/robotcontext/sysRules.pl", outEnvView,webDir, false);
 	}
 	
}
