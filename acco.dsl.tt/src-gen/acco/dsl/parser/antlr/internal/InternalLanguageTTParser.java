package acco.dsl.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import acco.dsl.services.LanguageTTGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalLanguageTTParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_INT", "RULE_BOOLEAN", "RULE_VARID", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'Testplan'", "'System'", "'Test'", "':'", "';'", "'Property'", "'Context'", "'QActor'", "'context'", "'Emit'", "'after'", "'ms'", "'Forward'", "'from'", "'to'", "'Delay'", "'Assert'", "'that'", "'for'", "'comment'", "'equal'", "'different'", "'lower'", "'higher'", "'lowerEqual'", "'higherEqual'", "'.'", "'Integer'", "'Boolean'", "'String'", "'True'", "'False'"
    };
    public static final int RULE_BOOLEAN=7;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int RULE_ID=4;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=6;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=9;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int RULE_VARID=8;
    public static final int RULE_STRING=5;
    public static final int RULE_SL_COMMENT=10;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_WS=11;
    public static final int RULE_ANY_OTHER=12;
    public static final int T__44=44;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;

    // delegates
    // delegators


        public InternalLanguageTTParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalLanguageTTParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalLanguageTTParser.tokenNames; }
    public String getGrammarFileName() { return "InternalLanguageTT.g"; }



     	private LanguageTTGrammarAccess grammarAccess;

        public InternalLanguageTTParser(TokenStream input, LanguageTTGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "Plan";
       	}

       	@Override
       	protected LanguageTTGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRulePlan"
    // InternalLanguageTT.g:65:1: entryRulePlan returns [EObject current=null] : iv_rulePlan= rulePlan EOF ;
    public final EObject entryRulePlan() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePlan = null;


        try {
            // InternalLanguageTT.g:65:45: (iv_rulePlan= rulePlan EOF )
            // InternalLanguageTT.g:66:2: iv_rulePlan= rulePlan EOF
            {
             newCompositeNode(grammarAccess.getPlanRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePlan=rulePlan();

            state._fsp--;

             current =iv_rulePlan; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePlan"


    // $ANTLR start "rulePlan"
    // InternalLanguageTT.g:72:1: rulePlan returns [EObject current=null] : (otherlv_0= 'Testplan' ( (lv_name_1_0= RULE_ID ) ) ( (lv_spec_2_0= rulePlanSpec ) ) ) ;
    public final EObject rulePlan() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        EObject lv_spec_2_0 = null;



        	enterRule();

        try {
            // InternalLanguageTT.g:78:2: ( (otherlv_0= 'Testplan' ( (lv_name_1_0= RULE_ID ) ) ( (lv_spec_2_0= rulePlanSpec ) ) ) )
            // InternalLanguageTT.g:79:2: (otherlv_0= 'Testplan' ( (lv_name_1_0= RULE_ID ) ) ( (lv_spec_2_0= rulePlanSpec ) ) )
            {
            // InternalLanguageTT.g:79:2: (otherlv_0= 'Testplan' ( (lv_name_1_0= RULE_ID ) ) ( (lv_spec_2_0= rulePlanSpec ) ) )
            // InternalLanguageTT.g:80:3: otherlv_0= 'Testplan' ( (lv_name_1_0= RULE_ID ) ) ( (lv_spec_2_0= rulePlanSpec ) )
            {
            otherlv_0=(Token)match(input,13,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getPlanAccess().getTestplanKeyword_0());
            		
            // InternalLanguageTT.g:84:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalLanguageTT.g:85:4: (lv_name_1_0= RULE_ID )
            {
            // InternalLanguageTT.g:85:4: (lv_name_1_0= RULE_ID )
            // InternalLanguageTT.g:86:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_4); 

            					newLeafNode(lv_name_1_0, grammarAccess.getPlanAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getPlanRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            // InternalLanguageTT.g:102:3: ( (lv_spec_2_0= rulePlanSpec ) )
            // InternalLanguageTT.g:103:4: (lv_spec_2_0= rulePlanSpec )
            {
            // InternalLanguageTT.g:103:4: (lv_spec_2_0= rulePlanSpec )
            // InternalLanguageTT.g:104:5: lv_spec_2_0= rulePlanSpec
            {

            					newCompositeNode(grammarAccess.getPlanAccess().getSpecPlanSpecParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_2);
            lv_spec_2_0=rulePlanSpec();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getPlanRule());
            					}
            					set(
            						current,
            						"spec",
            						lv_spec_2_0,
            						"acco.dsl.LanguageTT.PlanSpec");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePlan"


    // $ANTLR start "entryRulePlanSpec"
    // InternalLanguageTT.g:125:1: entryRulePlanSpec returns [EObject current=null] : iv_rulePlanSpec= rulePlanSpec EOF ;
    public final EObject entryRulePlanSpec() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePlanSpec = null;


        try {
            // InternalLanguageTT.g:125:49: (iv_rulePlanSpec= rulePlanSpec EOF )
            // InternalLanguageTT.g:126:2: iv_rulePlanSpec= rulePlanSpec EOF
            {
             newCompositeNode(grammarAccess.getPlanSpecRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePlanSpec=rulePlanSpec();

            state._fsp--;

             current =iv_rulePlanSpec; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePlanSpec"


    // $ANTLR start "rulePlanSpec"
    // InternalLanguageTT.g:132:1: rulePlanSpec returns [EObject current=null] : (otherlv_0= 'System' ( (otherlv_1= RULE_ID ) ) ( (lv_properties_2_0= ruleProperty ) )* ( (lv_contexts_3_0= ruleContext ) )* ( (lv_actors_4_0= ruleQActor ) )* ( (lv_tests_5_0= ruleTest ) )* ) ;
    public final EObject rulePlanSpec() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        EObject lv_properties_2_0 = null;

        EObject lv_contexts_3_0 = null;

        EObject lv_actors_4_0 = null;

        EObject lv_tests_5_0 = null;



        	enterRule();

        try {
            // InternalLanguageTT.g:138:2: ( (otherlv_0= 'System' ( (otherlv_1= RULE_ID ) ) ( (lv_properties_2_0= ruleProperty ) )* ( (lv_contexts_3_0= ruleContext ) )* ( (lv_actors_4_0= ruleQActor ) )* ( (lv_tests_5_0= ruleTest ) )* ) )
            // InternalLanguageTT.g:139:2: (otherlv_0= 'System' ( (otherlv_1= RULE_ID ) ) ( (lv_properties_2_0= ruleProperty ) )* ( (lv_contexts_3_0= ruleContext ) )* ( (lv_actors_4_0= ruleQActor ) )* ( (lv_tests_5_0= ruleTest ) )* )
            {
            // InternalLanguageTT.g:139:2: (otherlv_0= 'System' ( (otherlv_1= RULE_ID ) ) ( (lv_properties_2_0= ruleProperty ) )* ( (lv_contexts_3_0= ruleContext ) )* ( (lv_actors_4_0= ruleQActor ) )* ( (lv_tests_5_0= ruleTest ) )* )
            // InternalLanguageTT.g:140:3: otherlv_0= 'System' ( (otherlv_1= RULE_ID ) ) ( (lv_properties_2_0= ruleProperty ) )* ( (lv_contexts_3_0= ruleContext ) )* ( (lv_actors_4_0= ruleQActor ) )* ( (lv_tests_5_0= ruleTest ) )*
            {
            otherlv_0=(Token)match(input,14,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getPlanSpecAccess().getSystemKeyword_0());
            		
            // InternalLanguageTT.g:144:3: ( (otherlv_1= RULE_ID ) )
            // InternalLanguageTT.g:145:4: (otherlv_1= RULE_ID )
            {
            // InternalLanguageTT.g:145:4: (otherlv_1= RULE_ID )
            // InternalLanguageTT.g:146:5: otherlv_1= RULE_ID
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getPlanSpecRule());
            					}
            				
            otherlv_1=(Token)match(input,RULE_ID,FOLLOW_5); 

            					newLeafNode(otherlv_1, grammarAccess.getPlanSpecAccess().getSystemQActorSystemSpecCrossReference_1_0());
            				

            }


            }

            // InternalLanguageTT.g:157:3: ( (lv_properties_2_0= ruleProperty ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==18) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalLanguageTT.g:158:4: (lv_properties_2_0= ruleProperty )
            	    {
            	    // InternalLanguageTT.g:158:4: (lv_properties_2_0= ruleProperty )
            	    // InternalLanguageTT.g:159:5: lv_properties_2_0= ruleProperty
            	    {

            	    					newCompositeNode(grammarAccess.getPlanSpecAccess().getPropertiesPropertyParserRuleCall_2_0());
            	    				
            	    pushFollow(FOLLOW_5);
            	    lv_properties_2_0=ruleProperty();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getPlanSpecRule());
            	    					}
            	    					add(
            	    						current,
            	    						"properties",
            	    						lv_properties_2_0,
            	    						"acco.dsl.LanguageTT.Property");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

            // InternalLanguageTT.g:176:3: ( (lv_contexts_3_0= ruleContext ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==19) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalLanguageTT.g:177:4: (lv_contexts_3_0= ruleContext )
            	    {
            	    // InternalLanguageTT.g:177:4: (lv_contexts_3_0= ruleContext )
            	    // InternalLanguageTT.g:178:5: lv_contexts_3_0= ruleContext
            	    {

            	    					newCompositeNode(grammarAccess.getPlanSpecAccess().getContextsContextParserRuleCall_3_0());
            	    				
            	    pushFollow(FOLLOW_6);
            	    lv_contexts_3_0=ruleContext();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getPlanSpecRule());
            	    					}
            	    					add(
            	    						current,
            	    						"contexts",
            	    						lv_contexts_3_0,
            	    						"acco.dsl.LanguageTT.Context");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);

            // InternalLanguageTT.g:195:3: ( (lv_actors_4_0= ruleQActor ) )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==20) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // InternalLanguageTT.g:196:4: (lv_actors_4_0= ruleQActor )
            	    {
            	    // InternalLanguageTT.g:196:4: (lv_actors_4_0= ruleQActor )
            	    // InternalLanguageTT.g:197:5: lv_actors_4_0= ruleQActor
            	    {

            	    					newCompositeNode(grammarAccess.getPlanSpecAccess().getActorsQActorParserRuleCall_4_0());
            	    				
            	    pushFollow(FOLLOW_7);
            	    lv_actors_4_0=ruleQActor();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getPlanSpecRule());
            	    					}
            	    					add(
            	    						current,
            	    						"actors",
            	    						lv_actors_4_0,
            	    						"acco.dsl.LanguageTT.QActor");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);

            // InternalLanguageTT.g:214:3: ( (lv_tests_5_0= ruleTest ) )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==15) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // InternalLanguageTT.g:215:4: (lv_tests_5_0= ruleTest )
            	    {
            	    // InternalLanguageTT.g:215:4: (lv_tests_5_0= ruleTest )
            	    // InternalLanguageTT.g:216:5: lv_tests_5_0= ruleTest
            	    {

            	    					newCompositeNode(grammarAccess.getPlanSpecAccess().getTestsTestParserRuleCall_5_0());
            	    				
            	    pushFollow(FOLLOW_8);
            	    lv_tests_5_0=ruleTest();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getPlanSpecRule());
            	    					}
            	    					add(
            	    						current,
            	    						"tests",
            	    						lv_tests_5_0,
            	    						"acco.dsl.LanguageTT.Test");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePlanSpec"


    // $ANTLR start "entryRuleTest"
    // InternalLanguageTT.g:237:1: entryRuleTest returns [EObject current=null] : iv_ruleTest= ruleTest EOF ;
    public final EObject entryRuleTest() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTest = null;


        try {
            // InternalLanguageTT.g:237:45: (iv_ruleTest= ruleTest EOF )
            // InternalLanguageTT.g:238:2: iv_ruleTest= ruleTest EOF
            {
             newCompositeNode(grammarAccess.getTestRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleTest=ruleTest();

            state._fsp--;

             current =iv_ruleTest; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTest"


    // $ANTLR start "ruleTest"
    // InternalLanguageTT.g:244:1: ruleTest returns [EObject current=null] : (otherlv_0= 'Test' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (lv_actions_3_0= ruleAction ) ) (otherlv_4= ';' ( (lv_actions_5_0= ruleAction ) ) )* ) ;
    public final EObject ruleTest() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_actions_3_0 = null;

        EObject lv_actions_5_0 = null;



        	enterRule();

        try {
            // InternalLanguageTT.g:250:2: ( (otherlv_0= 'Test' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (lv_actions_3_0= ruleAction ) ) (otherlv_4= ';' ( (lv_actions_5_0= ruleAction ) ) )* ) )
            // InternalLanguageTT.g:251:2: (otherlv_0= 'Test' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (lv_actions_3_0= ruleAction ) ) (otherlv_4= ';' ( (lv_actions_5_0= ruleAction ) ) )* )
            {
            // InternalLanguageTT.g:251:2: (otherlv_0= 'Test' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (lv_actions_3_0= ruleAction ) ) (otherlv_4= ';' ( (lv_actions_5_0= ruleAction ) ) )* )
            // InternalLanguageTT.g:252:3: otherlv_0= 'Test' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (lv_actions_3_0= ruleAction ) ) (otherlv_4= ';' ( (lv_actions_5_0= ruleAction ) ) )*
            {
            otherlv_0=(Token)match(input,15,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getTestAccess().getTestKeyword_0());
            		
            // InternalLanguageTT.g:256:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalLanguageTT.g:257:4: (lv_name_1_0= RULE_ID )
            {
            // InternalLanguageTT.g:257:4: (lv_name_1_0= RULE_ID )
            // InternalLanguageTT.g:258:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_9); 

            					newLeafNode(lv_name_1_0, grammarAccess.getTestAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getTestRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_2=(Token)match(input,16,FOLLOW_10); 

            			newLeafNode(otherlv_2, grammarAccess.getTestAccess().getColonKeyword_2());
            		
            // InternalLanguageTT.g:278:3: ( (lv_actions_3_0= ruleAction ) )
            // InternalLanguageTT.g:279:4: (lv_actions_3_0= ruleAction )
            {
            // InternalLanguageTT.g:279:4: (lv_actions_3_0= ruleAction )
            // InternalLanguageTT.g:280:5: lv_actions_3_0= ruleAction
            {

            					newCompositeNode(grammarAccess.getTestAccess().getActionsActionParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_11);
            lv_actions_3_0=ruleAction();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getTestRule());
            					}
            					add(
            						current,
            						"actions",
            						lv_actions_3_0,
            						"acco.dsl.LanguageTT.Action");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalLanguageTT.g:297:3: (otherlv_4= ';' ( (lv_actions_5_0= ruleAction ) ) )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==17) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // InternalLanguageTT.g:298:4: otherlv_4= ';' ( (lv_actions_5_0= ruleAction ) )
            	    {
            	    otherlv_4=(Token)match(input,17,FOLLOW_10); 

            	    				newLeafNode(otherlv_4, grammarAccess.getTestAccess().getSemicolonKeyword_4_0());
            	    			
            	    // InternalLanguageTT.g:302:4: ( (lv_actions_5_0= ruleAction ) )
            	    // InternalLanguageTT.g:303:5: (lv_actions_5_0= ruleAction )
            	    {
            	    // InternalLanguageTT.g:303:5: (lv_actions_5_0= ruleAction )
            	    // InternalLanguageTT.g:304:6: lv_actions_5_0= ruleAction
            	    {

            	    						newCompositeNode(grammarAccess.getTestAccess().getActionsActionParserRuleCall_4_1_0());
            	    					
            	    pushFollow(FOLLOW_11);
            	    lv_actions_5_0=ruleAction();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getTestRule());
            	    						}
            	    						add(
            	    							current,
            	    							"actions",
            	    							lv_actions_5_0,
            	    							"acco.dsl.LanguageTT.Action");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTest"


    // $ANTLR start "entryRuleProperty"
    // InternalLanguageTT.g:326:1: entryRuleProperty returns [EObject current=null] : iv_ruleProperty= ruleProperty EOF ;
    public final EObject entryRuleProperty() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleProperty = null;


        try {
            // InternalLanguageTT.g:326:49: (iv_ruleProperty= ruleProperty EOF )
            // InternalLanguageTT.g:327:2: iv_ruleProperty= ruleProperty EOF
            {
             newCompositeNode(grammarAccess.getPropertyRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleProperty=ruleProperty();

            state._fsp--;

             current =iv_ruleProperty; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleProperty"


    // $ANTLR start "ruleProperty"
    // InternalLanguageTT.g:333:1: ruleProperty returns [EObject current=null] : (otherlv_0= 'Property' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (lv_type_3_0= rulePropertyType ) ) ) ;
    public final EObject ruleProperty() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Enumerator lv_type_3_0 = null;



        	enterRule();

        try {
            // InternalLanguageTT.g:339:2: ( (otherlv_0= 'Property' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (lv_type_3_0= rulePropertyType ) ) ) )
            // InternalLanguageTT.g:340:2: (otherlv_0= 'Property' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (lv_type_3_0= rulePropertyType ) ) )
            {
            // InternalLanguageTT.g:340:2: (otherlv_0= 'Property' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (lv_type_3_0= rulePropertyType ) ) )
            // InternalLanguageTT.g:341:3: otherlv_0= 'Property' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (lv_type_3_0= rulePropertyType ) )
            {
            otherlv_0=(Token)match(input,18,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getPropertyAccess().getPropertyKeyword_0());
            		
            // InternalLanguageTT.g:345:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalLanguageTT.g:346:4: (lv_name_1_0= RULE_ID )
            {
            // InternalLanguageTT.g:346:4: (lv_name_1_0= RULE_ID )
            // InternalLanguageTT.g:347:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_9); 

            					newLeafNode(lv_name_1_0, grammarAccess.getPropertyAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getPropertyRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_2=(Token)match(input,16,FOLLOW_12); 

            			newLeafNode(otherlv_2, grammarAccess.getPropertyAccess().getColonKeyword_2());
            		
            // InternalLanguageTT.g:367:3: ( (lv_type_3_0= rulePropertyType ) )
            // InternalLanguageTT.g:368:4: (lv_type_3_0= rulePropertyType )
            {
            // InternalLanguageTT.g:368:4: (lv_type_3_0= rulePropertyType )
            // InternalLanguageTT.g:369:5: lv_type_3_0= rulePropertyType
            {

            					newCompositeNode(grammarAccess.getPropertyAccess().getTypePropertyTypeEnumRuleCall_3_0());
            				
            pushFollow(FOLLOW_2);
            lv_type_3_0=rulePropertyType();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getPropertyRule());
            					}
            					set(
            						current,
            						"type",
            						lv_type_3_0,
            						"acco.dsl.LanguageTT.PropertyType");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleProperty"


    // $ANTLR start "entryRuleContext"
    // InternalLanguageTT.g:390:1: entryRuleContext returns [EObject current=null] : iv_ruleContext= ruleContext EOF ;
    public final EObject entryRuleContext() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleContext = null;


        try {
            // InternalLanguageTT.g:390:48: (iv_ruleContext= ruleContext EOF )
            // InternalLanguageTT.g:391:2: iv_ruleContext= ruleContext EOF
            {
             newCompositeNode(grammarAccess.getContextRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleContext=ruleContext();

            state._fsp--;

             current =iv_ruleContext; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleContext"


    // $ANTLR start "ruleContext"
    // InternalLanguageTT.g:397:1: ruleContext returns [EObject current=null] : (otherlv_0= 'Context' ( ( ruleQualifiedName ) ) ) ;
    public final EObject ruleContext() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;


        	enterRule();

        try {
            // InternalLanguageTT.g:403:2: ( (otherlv_0= 'Context' ( ( ruleQualifiedName ) ) ) )
            // InternalLanguageTT.g:404:2: (otherlv_0= 'Context' ( ( ruleQualifiedName ) ) )
            {
            // InternalLanguageTT.g:404:2: (otherlv_0= 'Context' ( ( ruleQualifiedName ) ) )
            // InternalLanguageTT.g:405:3: otherlv_0= 'Context' ( ( ruleQualifiedName ) )
            {
            otherlv_0=(Token)match(input,19,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getContextAccess().getContextKeyword_0());
            		
            // InternalLanguageTT.g:409:3: ( ( ruleQualifiedName ) )
            // InternalLanguageTT.g:410:4: ( ruleQualifiedName )
            {
            // InternalLanguageTT.g:410:4: ( ruleQualifiedName )
            // InternalLanguageTT.g:411:5: ruleQualifiedName
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getContextRule());
            					}
            				

            					newCompositeNode(grammarAccess.getContextAccess().getContextContextCrossReference_1_0());
            				
            pushFollow(FOLLOW_2);
            ruleQualifiedName();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleContext"


    // $ANTLR start "entryRuleQActor"
    // InternalLanguageTT.g:429:1: entryRuleQActor returns [EObject current=null] : iv_ruleQActor= ruleQActor EOF ;
    public final EObject entryRuleQActor() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleQActor = null;


        try {
            // InternalLanguageTT.g:429:47: (iv_ruleQActor= ruleQActor EOF )
            // InternalLanguageTT.g:430:2: iv_ruleQActor= ruleQActor EOF
            {
             newCompositeNode(grammarAccess.getQActorRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleQActor=ruleQActor();

            state._fsp--;

             current =iv_ruleQActor; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQActor"


    // $ANTLR start "ruleQActor"
    // InternalLanguageTT.g:436:1: ruleQActor returns [EObject current=null] : (otherlv_0= 'QActor' ( ( ruleQualifiedName ) ) otherlv_2= 'context' ( ( ruleQualifiedName ) ) ) ;
    public final EObject ruleQActor() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;


        	enterRule();

        try {
            // InternalLanguageTT.g:442:2: ( (otherlv_0= 'QActor' ( ( ruleQualifiedName ) ) otherlv_2= 'context' ( ( ruleQualifiedName ) ) ) )
            // InternalLanguageTT.g:443:2: (otherlv_0= 'QActor' ( ( ruleQualifiedName ) ) otherlv_2= 'context' ( ( ruleQualifiedName ) ) )
            {
            // InternalLanguageTT.g:443:2: (otherlv_0= 'QActor' ( ( ruleQualifiedName ) ) otherlv_2= 'context' ( ( ruleQualifiedName ) ) )
            // InternalLanguageTT.g:444:3: otherlv_0= 'QActor' ( ( ruleQualifiedName ) ) otherlv_2= 'context' ( ( ruleQualifiedName ) )
            {
            otherlv_0=(Token)match(input,20,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getQActorAccess().getQActorKeyword_0());
            		
            // InternalLanguageTT.g:448:3: ( ( ruleQualifiedName ) )
            // InternalLanguageTT.g:449:4: ( ruleQualifiedName )
            {
            // InternalLanguageTT.g:449:4: ( ruleQualifiedName )
            // InternalLanguageTT.g:450:5: ruleQualifiedName
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getQActorRule());
            					}
            				

            					newCompositeNode(grammarAccess.getQActorAccess().getQactorQActorCrossReference_1_0());
            				
            pushFollow(FOLLOW_13);
            ruleQualifiedName();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,21,FOLLOW_3); 

            			newLeafNode(otherlv_2, grammarAccess.getQActorAccess().getContextKeyword_2());
            		
            // InternalLanguageTT.g:468:3: ( ( ruleQualifiedName ) )
            // InternalLanguageTT.g:469:4: ( ruleQualifiedName )
            {
            // InternalLanguageTT.g:469:4: ( ruleQualifiedName )
            // InternalLanguageTT.g:470:5: ruleQualifiedName
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getQActorRule());
            					}
            				

            					newCompositeNode(grammarAccess.getQActorAccess().getContextContextCrossReference_3_0());
            				
            pushFollow(FOLLOW_2);
            ruleQualifiedName();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQActor"


    // $ANTLR start "entryRuleAction"
    // InternalLanguageTT.g:488:1: entryRuleAction returns [EObject current=null] : iv_ruleAction= ruleAction EOF ;
    public final EObject entryRuleAction() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAction = null;


        try {
            // InternalLanguageTT.g:488:47: (iv_ruleAction= ruleAction EOF )
            // InternalLanguageTT.g:489:2: iv_ruleAction= ruleAction EOF
            {
             newCompositeNode(grammarAccess.getActionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAction=ruleAction();

            state._fsp--;

             current =iv_ruleAction; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAction"


    // $ANTLR start "ruleAction"
    // InternalLanguageTT.g:495:1: ruleAction returns [EObject current=null] : (this_EmitEvent_0= ruleEmitEvent | this_ForwardDispatch_1= ruleForwardDispatch | this_Delay_2= ruleDelay | this_Assertion_3= ruleAssertion ) ;
    public final EObject ruleAction() throws RecognitionException {
        EObject current = null;

        EObject this_EmitEvent_0 = null;

        EObject this_ForwardDispatch_1 = null;

        EObject this_Delay_2 = null;

        EObject this_Assertion_3 = null;



        	enterRule();

        try {
            // InternalLanguageTT.g:501:2: ( (this_EmitEvent_0= ruleEmitEvent | this_ForwardDispatch_1= ruleForwardDispatch | this_Delay_2= ruleDelay | this_Assertion_3= ruleAssertion ) )
            // InternalLanguageTT.g:502:2: (this_EmitEvent_0= ruleEmitEvent | this_ForwardDispatch_1= ruleForwardDispatch | this_Delay_2= ruleDelay | this_Assertion_3= ruleAssertion )
            {
            // InternalLanguageTT.g:502:2: (this_EmitEvent_0= ruleEmitEvent | this_ForwardDispatch_1= ruleForwardDispatch | this_Delay_2= ruleDelay | this_Assertion_3= ruleAssertion )
            int alt6=4;
            switch ( input.LA(1) ) {
            case 22:
                {
                alt6=1;
                }
                break;
            case 25:
                {
                alt6=2;
                }
                break;
            case 28:
                {
                alt6=3;
                }
                break;
            case 29:
                {
                alt6=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }

            switch (alt6) {
                case 1 :
                    // InternalLanguageTT.g:503:3: this_EmitEvent_0= ruleEmitEvent
                    {

                    			newCompositeNode(grammarAccess.getActionAccess().getEmitEventParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_EmitEvent_0=ruleEmitEvent();

                    state._fsp--;


                    			current = this_EmitEvent_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalLanguageTT.g:512:3: this_ForwardDispatch_1= ruleForwardDispatch
                    {

                    			newCompositeNode(grammarAccess.getActionAccess().getForwardDispatchParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_ForwardDispatch_1=ruleForwardDispatch();

                    state._fsp--;


                    			current = this_ForwardDispatch_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 3 :
                    // InternalLanguageTT.g:521:3: this_Delay_2= ruleDelay
                    {

                    			newCompositeNode(grammarAccess.getActionAccess().getDelayParserRuleCall_2());
                    		
                    pushFollow(FOLLOW_2);
                    this_Delay_2=ruleDelay();

                    state._fsp--;


                    			current = this_Delay_2;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 4 :
                    // InternalLanguageTT.g:530:3: this_Assertion_3= ruleAssertion
                    {

                    			newCompositeNode(grammarAccess.getActionAccess().getAssertionParserRuleCall_3());
                    		
                    pushFollow(FOLLOW_2);
                    this_Assertion_3=ruleAssertion();

                    state._fsp--;


                    			current = this_Assertion_3;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAction"


    // $ANTLR start "entryRuleEmitEvent"
    // InternalLanguageTT.g:542:1: entryRuleEmitEvent returns [EObject current=null] : iv_ruleEmitEvent= ruleEmitEvent EOF ;
    public final EObject entryRuleEmitEvent() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEmitEvent = null;


        try {
            // InternalLanguageTT.g:542:50: (iv_ruleEmitEvent= ruleEmitEvent EOF )
            // InternalLanguageTT.g:543:2: iv_ruleEmitEvent= ruleEmitEvent EOF
            {
             newCompositeNode(grammarAccess.getEmitEventRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEmitEvent=ruleEmitEvent();

            state._fsp--;

             current =iv_ruleEmitEvent; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEmitEvent"


    // $ANTLR start "ruleEmitEvent"
    // InternalLanguageTT.g:549:1: ruleEmitEvent returns [EObject current=null] : (otherlv_0= 'Emit' ( (lv_ev_1_0= RULE_STRING ) ) otherlv_2= ':' ( (lv_content_3_0= RULE_STRING ) ) otherlv_4= 'context' ( ( ruleQualifiedName ) ) (otherlv_6= 'after' ( (lv_after_7_0= RULE_INT ) ) otherlv_8= 'ms' )? ) ;
    public final EObject ruleEmitEvent() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_ev_1_0=null;
        Token otherlv_2=null;
        Token lv_content_3_0=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token lv_after_7_0=null;
        Token otherlv_8=null;


        	enterRule();

        try {
            // InternalLanguageTT.g:555:2: ( (otherlv_0= 'Emit' ( (lv_ev_1_0= RULE_STRING ) ) otherlv_2= ':' ( (lv_content_3_0= RULE_STRING ) ) otherlv_4= 'context' ( ( ruleQualifiedName ) ) (otherlv_6= 'after' ( (lv_after_7_0= RULE_INT ) ) otherlv_8= 'ms' )? ) )
            // InternalLanguageTT.g:556:2: (otherlv_0= 'Emit' ( (lv_ev_1_0= RULE_STRING ) ) otherlv_2= ':' ( (lv_content_3_0= RULE_STRING ) ) otherlv_4= 'context' ( ( ruleQualifiedName ) ) (otherlv_6= 'after' ( (lv_after_7_0= RULE_INT ) ) otherlv_8= 'ms' )? )
            {
            // InternalLanguageTT.g:556:2: (otherlv_0= 'Emit' ( (lv_ev_1_0= RULE_STRING ) ) otherlv_2= ':' ( (lv_content_3_0= RULE_STRING ) ) otherlv_4= 'context' ( ( ruleQualifiedName ) ) (otherlv_6= 'after' ( (lv_after_7_0= RULE_INT ) ) otherlv_8= 'ms' )? )
            // InternalLanguageTT.g:557:3: otherlv_0= 'Emit' ( (lv_ev_1_0= RULE_STRING ) ) otherlv_2= ':' ( (lv_content_3_0= RULE_STRING ) ) otherlv_4= 'context' ( ( ruleQualifiedName ) ) (otherlv_6= 'after' ( (lv_after_7_0= RULE_INT ) ) otherlv_8= 'ms' )?
            {
            otherlv_0=(Token)match(input,22,FOLLOW_14); 

            			newLeafNode(otherlv_0, grammarAccess.getEmitEventAccess().getEmitKeyword_0());
            		
            // InternalLanguageTT.g:561:3: ( (lv_ev_1_0= RULE_STRING ) )
            // InternalLanguageTT.g:562:4: (lv_ev_1_0= RULE_STRING )
            {
            // InternalLanguageTT.g:562:4: (lv_ev_1_0= RULE_STRING )
            // InternalLanguageTT.g:563:5: lv_ev_1_0= RULE_STRING
            {
            lv_ev_1_0=(Token)match(input,RULE_STRING,FOLLOW_9); 

            					newLeafNode(lv_ev_1_0, grammarAccess.getEmitEventAccess().getEvSTRINGTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getEmitEventRule());
            					}
            					setWithLastConsumed(
            						current,
            						"ev",
            						lv_ev_1_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }

            otherlv_2=(Token)match(input,16,FOLLOW_14); 

            			newLeafNode(otherlv_2, grammarAccess.getEmitEventAccess().getColonKeyword_2());
            		
            // InternalLanguageTT.g:583:3: ( (lv_content_3_0= RULE_STRING ) )
            // InternalLanguageTT.g:584:4: (lv_content_3_0= RULE_STRING )
            {
            // InternalLanguageTT.g:584:4: (lv_content_3_0= RULE_STRING )
            // InternalLanguageTT.g:585:5: lv_content_3_0= RULE_STRING
            {
            lv_content_3_0=(Token)match(input,RULE_STRING,FOLLOW_13); 

            					newLeafNode(lv_content_3_0, grammarAccess.getEmitEventAccess().getContentSTRINGTerminalRuleCall_3_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getEmitEventRule());
            					}
            					setWithLastConsumed(
            						current,
            						"content",
            						lv_content_3_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }

            otherlv_4=(Token)match(input,21,FOLLOW_3); 

            			newLeafNode(otherlv_4, grammarAccess.getEmitEventAccess().getContextKeyword_4());
            		
            // InternalLanguageTT.g:605:3: ( ( ruleQualifiedName ) )
            // InternalLanguageTT.g:606:4: ( ruleQualifiedName )
            {
            // InternalLanguageTT.g:606:4: ( ruleQualifiedName )
            // InternalLanguageTT.g:607:5: ruleQualifiedName
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getEmitEventRule());
            					}
            				

            					newCompositeNode(grammarAccess.getEmitEventAccess().getContextContextCrossReference_5_0());
            				
            pushFollow(FOLLOW_15);
            ruleQualifiedName();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalLanguageTT.g:621:3: (otherlv_6= 'after' ( (lv_after_7_0= RULE_INT ) ) otherlv_8= 'ms' )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==23) ) {
                alt7=1;
            }
            switch (alt7) {
                case 1 :
                    // InternalLanguageTT.g:622:4: otherlv_6= 'after' ( (lv_after_7_0= RULE_INT ) ) otherlv_8= 'ms'
                    {
                    otherlv_6=(Token)match(input,23,FOLLOW_16); 

                    				newLeafNode(otherlv_6, grammarAccess.getEmitEventAccess().getAfterKeyword_6_0());
                    			
                    // InternalLanguageTT.g:626:4: ( (lv_after_7_0= RULE_INT ) )
                    // InternalLanguageTT.g:627:5: (lv_after_7_0= RULE_INT )
                    {
                    // InternalLanguageTT.g:627:5: (lv_after_7_0= RULE_INT )
                    // InternalLanguageTT.g:628:6: lv_after_7_0= RULE_INT
                    {
                    lv_after_7_0=(Token)match(input,RULE_INT,FOLLOW_17); 

                    						newLeafNode(lv_after_7_0, grammarAccess.getEmitEventAccess().getAfterINTTerminalRuleCall_6_1_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getEmitEventRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"after",
                    							lv_after_7_0,
                    							"org.eclipse.xtext.common.Terminals.INT");
                    					

                    }


                    }

                    otherlv_8=(Token)match(input,24,FOLLOW_2); 

                    				newLeafNode(otherlv_8, grammarAccess.getEmitEventAccess().getMsKeyword_6_2());
                    			

                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEmitEvent"


    // $ANTLR start "entryRuleForwardDispatch"
    // InternalLanguageTT.g:653:1: entryRuleForwardDispatch returns [EObject current=null] : iv_ruleForwardDispatch= ruleForwardDispatch EOF ;
    public final EObject entryRuleForwardDispatch() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleForwardDispatch = null;


        try {
            // InternalLanguageTT.g:653:56: (iv_ruleForwardDispatch= ruleForwardDispatch EOF )
            // InternalLanguageTT.g:654:2: iv_ruleForwardDispatch= ruleForwardDispatch EOF
            {
             newCompositeNode(grammarAccess.getForwardDispatchRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleForwardDispatch=ruleForwardDispatch();

            state._fsp--;

             current =iv_ruleForwardDispatch; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleForwardDispatch"


    // $ANTLR start "ruleForwardDispatch"
    // InternalLanguageTT.g:660:1: ruleForwardDispatch returns [EObject current=null] : (otherlv_0= 'Forward' ( (lv_id_1_0= RULE_STRING ) ) otherlv_2= ':' ( (lv_content_3_0= RULE_STRING ) ) otherlv_4= 'from' ( ( ruleQualifiedName ) ) otherlv_6= 'to' ( ( ruleQualifiedName ) ) ) ;
    public final EObject ruleForwardDispatch() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_id_1_0=null;
        Token otherlv_2=null;
        Token lv_content_3_0=null;
        Token otherlv_4=null;
        Token otherlv_6=null;


        	enterRule();

        try {
            // InternalLanguageTT.g:666:2: ( (otherlv_0= 'Forward' ( (lv_id_1_0= RULE_STRING ) ) otherlv_2= ':' ( (lv_content_3_0= RULE_STRING ) ) otherlv_4= 'from' ( ( ruleQualifiedName ) ) otherlv_6= 'to' ( ( ruleQualifiedName ) ) ) )
            // InternalLanguageTT.g:667:2: (otherlv_0= 'Forward' ( (lv_id_1_0= RULE_STRING ) ) otherlv_2= ':' ( (lv_content_3_0= RULE_STRING ) ) otherlv_4= 'from' ( ( ruleQualifiedName ) ) otherlv_6= 'to' ( ( ruleQualifiedName ) ) )
            {
            // InternalLanguageTT.g:667:2: (otherlv_0= 'Forward' ( (lv_id_1_0= RULE_STRING ) ) otherlv_2= ':' ( (lv_content_3_0= RULE_STRING ) ) otherlv_4= 'from' ( ( ruleQualifiedName ) ) otherlv_6= 'to' ( ( ruleQualifiedName ) ) )
            // InternalLanguageTT.g:668:3: otherlv_0= 'Forward' ( (lv_id_1_0= RULE_STRING ) ) otherlv_2= ':' ( (lv_content_3_0= RULE_STRING ) ) otherlv_4= 'from' ( ( ruleQualifiedName ) ) otherlv_6= 'to' ( ( ruleQualifiedName ) )
            {
            otherlv_0=(Token)match(input,25,FOLLOW_14); 

            			newLeafNode(otherlv_0, grammarAccess.getForwardDispatchAccess().getForwardKeyword_0());
            		
            // InternalLanguageTT.g:672:3: ( (lv_id_1_0= RULE_STRING ) )
            // InternalLanguageTT.g:673:4: (lv_id_1_0= RULE_STRING )
            {
            // InternalLanguageTT.g:673:4: (lv_id_1_0= RULE_STRING )
            // InternalLanguageTT.g:674:5: lv_id_1_0= RULE_STRING
            {
            lv_id_1_0=(Token)match(input,RULE_STRING,FOLLOW_9); 

            					newLeafNode(lv_id_1_0, grammarAccess.getForwardDispatchAccess().getIdSTRINGTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getForwardDispatchRule());
            					}
            					setWithLastConsumed(
            						current,
            						"id",
            						lv_id_1_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }

            otherlv_2=(Token)match(input,16,FOLLOW_14); 

            			newLeafNode(otherlv_2, grammarAccess.getForwardDispatchAccess().getColonKeyword_2());
            		
            // InternalLanguageTT.g:694:3: ( (lv_content_3_0= RULE_STRING ) )
            // InternalLanguageTT.g:695:4: (lv_content_3_0= RULE_STRING )
            {
            // InternalLanguageTT.g:695:4: (lv_content_3_0= RULE_STRING )
            // InternalLanguageTT.g:696:5: lv_content_3_0= RULE_STRING
            {
            lv_content_3_0=(Token)match(input,RULE_STRING,FOLLOW_18); 

            					newLeafNode(lv_content_3_0, grammarAccess.getForwardDispatchAccess().getContentSTRINGTerminalRuleCall_3_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getForwardDispatchRule());
            					}
            					setWithLastConsumed(
            						current,
            						"content",
            						lv_content_3_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }

            otherlv_4=(Token)match(input,26,FOLLOW_3); 

            			newLeafNode(otherlv_4, grammarAccess.getForwardDispatchAccess().getFromKeyword_4());
            		
            // InternalLanguageTT.g:716:3: ( ( ruleQualifiedName ) )
            // InternalLanguageTT.g:717:4: ( ruleQualifiedName )
            {
            // InternalLanguageTT.g:717:4: ( ruleQualifiedName )
            // InternalLanguageTT.g:718:5: ruleQualifiedName
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getForwardDispatchRule());
            					}
            				

            					newCompositeNode(grammarAccess.getForwardDispatchAccess().getSenderQActorCrossReference_5_0());
            				
            pushFollow(FOLLOW_19);
            ruleQualifiedName();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_6=(Token)match(input,27,FOLLOW_3); 

            			newLeafNode(otherlv_6, grammarAccess.getForwardDispatchAccess().getToKeyword_6());
            		
            // InternalLanguageTT.g:736:3: ( ( ruleQualifiedName ) )
            // InternalLanguageTT.g:737:4: ( ruleQualifiedName )
            {
            // InternalLanguageTT.g:737:4: ( ruleQualifiedName )
            // InternalLanguageTT.g:738:5: ruleQualifiedName
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getForwardDispatchRule());
            					}
            				

            					newCompositeNode(grammarAccess.getForwardDispatchAccess().getReceiverQActorCrossReference_7_0());
            				
            pushFollow(FOLLOW_2);
            ruleQualifiedName();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleForwardDispatch"


    // $ANTLR start "entryRuleDelay"
    // InternalLanguageTT.g:756:1: entryRuleDelay returns [EObject current=null] : iv_ruleDelay= ruleDelay EOF ;
    public final EObject entryRuleDelay() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDelay = null;


        try {
            // InternalLanguageTT.g:756:46: (iv_ruleDelay= ruleDelay EOF )
            // InternalLanguageTT.g:757:2: iv_ruleDelay= ruleDelay EOF
            {
             newCompositeNode(grammarAccess.getDelayRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDelay=ruleDelay();

            state._fsp--;

             current =iv_ruleDelay; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDelay"


    // $ANTLR start "ruleDelay"
    // InternalLanguageTT.g:763:1: ruleDelay returns [EObject current=null] : (otherlv_0= 'Delay' ( (lv_time_1_0= RULE_INT ) ) otherlv_2= 'ms' ) ;
    public final EObject ruleDelay() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_time_1_0=null;
        Token otherlv_2=null;


        	enterRule();

        try {
            // InternalLanguageTT.g:769:2: ( (otherlv_0= 'Delay' ( (lv_time_1_0= RULE_INT ) ) otherlv_2= 'ms' ) )
            // InternalLanguageTT.g:770:2: (otherlv_0= 'Delay' ( (lv_time_1_0= RULE_INT ) ) otherlv_2= 'ms' )
            {
            // InternalLanguageTT.g:770:2: (otherlv_0= 'Delay' ( (lv_time_1_0= RULE_INT ) ) otherlv_2= 'ms' )
            // InternalLanguageTT.g:771:3: otherlv_0= 'Delay' ( (lv_time_1_0= RULE_INT ) ) otherlv_2= 'ms'
            {
            otherlv_0=(Token)match(input,28,FOLLOW_16); 

            			newLeafNode(otherlv_0, grammarAccess.getDelayAccess().getDelayKeyword_0());
            		
            // InternalLanguageTT.g:775:3: ( (lv_time_1_0= RULE_INT ) )
            // InternalLanguageTT.g:776:4: (lv_time_1_0= RULE_INT )
            {
            // InternalLanguageTT.g:776:4: (lv_time_1_0= RULE_INT )
            // InternalLanguageTT.g:777:5: lv_time_1_0= RULE_INT
            {
            lv_time_1_0=(Token)match(input,RULE_INT,FOLLOW_17); 

            					newLeafNode(lv_time_1_0, grammarAccess.getDelayAccess().getTimeINTTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getDelayRule());
            					}
            					setWithLastConsumed(
            						current,
            						"time",
            						lv_time_1_0,
            						"org.eclipse.xtext.common.Terminals.INT");
            				

            }


            }

            otherlv_2=(Token)match(input,24,FOLLOW_2); 

            			newLeafNode(otherlv_2, grammarAccess.getDelayAccess().getMsKeyword_2());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDelay"


    // $ANTLR start "entryRuleAssertion"
    // InternalLanguageTT.g:801:1: entryRuleAssertion returns [EObject current=null] : iv_ruleAssertion= ruleAssertion EOF ;
    public final EObject entryRuleAssertion() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAssertion = null;


        try {
            // InternalLanguageTT.g:801:50: (iv_ruleAssertion= ruleAssertion EOF )
            // InternalLanguageTT.g:802:2: iv_ruleAssertion= ruleAssertion EOF
            {
             newCompositeNode(grammarAccess.getAssertionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAssertion=ruleAssertion();

            state._fsp--;

             current =iv_ruleAssertion; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAssertion"


    // $ANTLR start "ruleAssertion"
    // InternalLanguageTT.g:808:1: ruleAssertion returns [EObject current=null] : (otherlv_0= 'Assert' ( (lv_type_1_0= ruleBooleanEnum ) ) otherlv_2= 'that' ( (otherlv_3= RULE_ID ) ) ( (lv_op_4_0= rulePropertyOp ) ) otherlv_5= 'for' ( ( ruleQualifiedName ) ) otherlv_7= 'comment' ( (lv_comment_8_0= RULE_STRING ) ) ) ;
    public final EObject ruleAssertion() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token lv_comment_8_0=null;
        Enumerator lv_type_1_0 = null;

        EObject lv_op_4_0 = null;



        	enterRule();

        try {
            // InternalLanguageTT.g:814:2: ( (otherlv_0= 'Assert' ( (lv_type_1_0= ruleBooleanEnum ) ) otherlv_2= 'that' ( (otherlv_3= RULE_ID ) ) ( (lv_op_4_0= rulePropertyOp ) ) otherlv_5= 'for' ( ( ruleQualifiedName ) ) otherlv_7= 'comment' ( (lv_comment_8_0= RULE_STRING ) ) ) )
            // InternalLanguageTT.g:815:2: (otherlv_0= 'Assert' ( (lv_type_1_0= ruleBooleanEnum ) ) otherlv_2= 'that' ( (otherlv_3= RULE_ID ) ) ( (lv_op_4_0= rulePropertyOp ) ) otherlv_5= 'for' ( ( ruleQualifiedName ) ) otherlv_7= 'comment' ( (lv_comment_8_0= RULE_STRING ) ) )
            {
            // InternalLanguageTT.g:815:2: (otherlv_0= 'Assert' ( (lv_type_1_0= ruleBooleanEnum ) ) otherlv_2= 'that' ( (otherlv_3= RULE_ID ) ) ( (lv_op_4_0= rulePropertyOp ) ) otherlv_5= 'for' ( ( ruleQualifiedName ) ) otherlv_7= 'comment' ( (lv_comment_8_0= RULE_STRING ) ) )
            // InternalLanguageTT.g:816:3: otherlv_0= 'Assert' ( (lv_type_1_0= ruleBooleanEnum ) ) otherlv_2= 'that' ( (otherlv_3= RULE_ID ) ) ( (lv_op_4_0= rulePropertyOp ) ) otherlv_5= 'for' ( ( ruleQualifiedName ) ) otherlv_7= 'comment' ( (lv_comment_8_0= RULE_STRING ) )
            {
            otherlv_0=(Token)match(input,29,FOLLOW_20); 

            			newLeafNode(otherlv_0, grammarAccess.getAssertionAccess().getAssertKeyword_0());
            		
            // InternalLanguageTT.g:820:3: ( (lv_type_1_0= ruleBooleanEnum ) )
            // InternalLanguageTT.g:821:4: (lv_type_1_0= ruleBooleanEnum )
            {
            // InternalLanguageTT.g:821:4: (lv_type_1_0= ruleBooleanEnum )
            // InternalLanguageTT.g:822:5: lv_type_1_0= ruleBooleanEnum
            {

            					newCompositeNode(grammarAccess.getAssertionAccess().getTypeBooleanEnumEnumRuleCall_1_0());
            				
            pushFollow(FOLLOW_21);
            lv_type_1_0=ruleBooleanEnum();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getAssertionRule());
            					}
            					set(
            						current,
            						"type",
            						lv_type_1_0,
            						"acco.dsl.LanguageTT.BooleanEnum");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,30,FOLLOW_3); 

            			newLeafNode(otherlv_2, grammarAccess.getAssertionAccess().getThatKeyword_2());
            		
            // InternalLanguageTT.g:843:3: ( (otherlv_3= RULE_ID ) )
            // InternalLanguageTT.g:844:4: (otherlv_3= RULE_ID )
            {
            // InternalLanguageTT.g:844:4: (otherlv_3= RULE_ID )
            // InternalLanguageTT.g:845:5: otherlv_3= RULE_ID
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAssertionRule());
            					}
            				
            otherlv_3=(Token)match(input,RULE_ID,FOLLOW_22); 

            					newLeafNode(otherlv_3, grammarAccess.getAssertionAccess().getPropertyPropertyCrossReference_3_0());
            				

            }


            }

            // InternalLanguageTT.g:856:3: ( (lv_op_4_0= rulePropertyOp ) )
            // InternalLanguageTT.g:857:4: (lv_op_4_0= rulePropertyOp )
            {
            // InternalLanguageTT.g:857:4: (lv_op_4_0= rulePropertyOp )
            // InternalLanguageTT.g:858:5: lv_op_4_0= rulePropertyOp
            {

            					newCompositeNode(grammarAccess.getAssertionAccess().getOpPropertyOpParserRuleCall_4_0());
            				
            pushFollow(FOLLOW_23);
            lv_op_4_0=rulePropertyOp();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getAssertionRule());
            					}
            					set(
            						current,
            						"op",
            						lv_op_4_0,
            						"acco.dsl.LanguageTT.PropertyOp");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_5=(Token)match(input,31,FOLLOW_3); 

            			newLeafNode(otherlv_5, grammarAccess.getAssertionAccess().getForKeyword_5());
            		
            // InternalLanguageTT.g:879:3: ( ( ruleQualifiedName ) )
            // InternalLanguageTT.g:880:4: ( ruleQualifiedName )
            {
            // InternalLanguageTT.g:880:4: ( ruleQualifiedName )
            // InternalLanguageTT.g:881:5: ruleQualifiedName
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAssertionRule());
            					}
            				

            					newCompositeNode(grammarAccess.getAssertionAccess().getActorQActorCrossReference_6_0());
            				
            pushFollow(FOLLOW_24);
            ruleQualifiedName();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_7=(Token)match(input,32,FOLLOW_14); 

            			newLeafNode(otherlv_7, grammarAccess.getAssertionAccess().getCommentKeyword_7());
            		
            // InternalLanguageTT.g:899:3: ( (lv_comment_8_0= RULE_STRING ) )
            // InternalLanguageTT.g:900:4: (lv_comment_8_0= RULE_STRING )
            {
            // InternalLanguageTT.g:900:4: (lv_comment_8_0= RULE_STRING )
            // InternalLanguageTT.g:901:5: lv_comment_8_0= RULE_STRING
            {
            lv_comment_8_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

            					newLeafNode(lv_comment_8_0, grammarAccess.getAssertionAccess().getCommentSTRINGTerminalRuleCall_8_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAssertionRule());
            					}
            					setWithLastConsumed(
            						current,
            						"comment",
            						lv_comment_8_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAssertion"


    // $ANTLR start "entryRulePropertyOp"
    // InternalLanguageTT.g:921:1: entryRulePropertyOp returns [EObject current=null] : iv_rulePropertyOp= rulePropertyOp EOF ;
    public final EObject entryRulePropertyOp() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePropertyOp = null;


        try {
            // InternalLanguageTT.g:921:51: (iv_rulePropertyOp= rulePropertyOp EOF )
            // InternalLanguageTT.g:922:2: iv_rulePropertyOp= rulePropertyOp EOF
            {
             newCompositeNode(grammarAccess.getPropertyOpRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePropertyOp=rulePropertyOp();

            state._fsp--;

             current =iv_rulePropertyOp; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePropertyOp"


    // $ANTLR start "rulePropertyOp"
    // InternalLanguageTT.g:928:1: rulePropertyOp returns [EObject current=null] : ( ( ( (lv_type_0_0= 'equal' ) ) ( (lv_value_1_0= rulePropertyValue ) ) ) | ( ( (lv_type_2_0= 'different' ) ) ( (lv_value_3_0= rulePropertyValue ) ) ) | ( ( (lv_type_4_0= 'lower' ) ) ( (lv_value_5_0= rulePropertyValue ) ) ) | ( ( (lv_type_6_0= 'higher' ) ) ( (lv_value_7_0= rulePropertyValue ) ) ) | ( ( (lv_type_8_0= 'lowerEqual' ) ) ( (lv_value_9_0= rulePropertyValue ) ) ) | ( ( (lv_type_10_0= 'higherEqual' ) ) ( (lv_value_11_0= rulePropertyValue ) ) ) ) ;
    public final EObject rulePropertyOp() throws RecognitionException {
        EObject current = null;

        Token lv_type_0_0=null;
        Token lv_type_2_0=null;
        Token lv_type_4_0=null;
        Token lv_type_6_0=null;
        Token lv_type_8_0=null;
        Token lv_type_10_0=null;
        AntlrDatatypeRuleToken lv_value_1_0 = null;

        AntlrDatatypeRuleToken lv_value_3_0 = null;

        AntlrDatatypeRuleToken lv_value_5_0 = null;

        AntlrDatatypeRuleToken lv_value_7_0 = null;

        AntlrDatatypeRuleToken lv_value_9_0 = null;

        AntlrDatatypeRuleToken lv_value_11_0 = null;



        	enterRule();

        try {
            // InternalLanguageTT.g:934:2: ( ( ( ( (lv_type_0_0= 'equal' ) ) ( (lv_value_1_0= rulePropertyValue ) ) ) | ( ( (lv_type_2_0= 'different' ) ) ( (lv_value_3_0= rulePropertyValue ) ) ) | ( ( (lv_type_4_0= 'lower' ) ) ( (lv_value_5_0= rulePropertyValue ) ) ) | ( ( (lv_type_6_0= 'higher' ) ) ( (lv_value_7_0= rulePropertyValue ) ) ) | ( ( (lv_type_8_0= 'lowerEqual' ) ) ( (lv_value_9_0= rulePropertyValue ) ) ) | ( ( (lv_type_10_0= 'higherEqual' ) ) ( (lv_value_11_0= rulePropertyValue ) ) ) ) )
            // InternalLanguageTT.g:935:2: ( ( ( (lv_type_0_0= 'equal' ) ) ( (lv_value_1_0= rulePropertyValue ) ) ) | ( ( (lv_type_2_0= 'different' ) ) ( (lv_value_3_0= rulePropertyValue ) ) ) | ( ( (lv_type_4_0= 'lower' ) ) ( (lv_value_5_0= rulePropertyValue ) ) ) | ( ( (lv_type_6_0= 'higher' ) ) ( (lv_value_7_0= rulePropertyValue ) ) ) | ( ( (lv_type_8_0= 'lowerEqual' ) ) ( (lv_value_9_0= rulePropertyValue ) ) ) | ( ( (lv_type_10_0= 'higherEqual' ) ) ( (lv_value_11_0= rulePropertyValue ) ) ) )
            {
            // InternalLanguageTT.g:935:2: ( ( ( (lv_type_0_0= 'equal' ) ) ( (lv_value_1_0= rulePropertyValue ) ) ) | ( ( (lv_type_2_0= 'different' ) ) ( (lv_value_3_0= rulePropertyValue ) ) ) | ( ( (lv_type_4_0= 'lower' ) ) ( (lv_value_5_0= rulePropertyValue ) ) ) | ( ( (lv_type_6_0= 'higher' ) ) ( (lv_value_7_0= rulePropertyValue ) ) ) | ( ( (lv_type_8_0= 'lowerEqual' ) ) ( (lv_value_9_0= rulePropertyValue ) ) ) | ( ( (lv_type_10_0= 'higherEqual' ) ) ( (lv_value_11_0= rulePropertyValue ) ) ) )
            int alt8=6;
            switch ( input.LA(1) ) {
            case 33:
                {
                alt8=1;
                }
                break;
            case 34:
                {
                alt8=2;
                }
                break;
            case 35:
                {
                alt8=3;
                }
                break;
            case 36:
                {
                alt8=4;
                }
                break;
            case 37:
                {
                alt8=5;
                }
                break;
            case 38:
                {
                alt8=6;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;
            }

            switch (alt8) {
                case 1 :
                    // InternalLanguageTT.g:936:3: ( ( (lv_type_0_0= 'equal' ) ) ( (lv_value_1_0= rulePropertyValue ) ) )
                    {
                    // InternalLanguageTT.g:936:3: ( ( (lv_type_0_0= 'equal' ) ) ( (lv_value_1_0= rulePropertyValue ) ) )
                    // InternalLanguageTT.g:937:4: ( (lv_type_0_0= 'equal' ) ) ( (lv_value_1_0= rulePropertyValue ) )
                    {
                    // InternalLanguageTT.g:937:4: ( (lv_type_0_0= 'equal' ) )
                    // InternalLanguageTT.g:938:5: (lv_type_0_0= 'equal' )
                    {
                    // InternalLanguageTT.g:938:5: (lv_type_0_0= 'equal' )
                    // InternalLanguageTT.g:939:6: lv_type_0_0= 'equal'
                    {
                    lv_type_0_0=(Token)match(input,33,FOLLOW_25); 

                    						newLeafNode(lv_type_0_0, grammarAccess.getPropertyOpAccess().getTypeEqualKeyword_0_0_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getPropertyOpRule());
                    						}
                    						setWithLastConsumed(current, "type", lv_type_0_0, "equal");
                    					

                    }


                    }

                    // InternalLanguageTT.g:951:4: ( (lv_value_1_0= rulePropertyValue ) )
                    // InternalLanguageTT.g:952:5: (lv_value_1_0= rulePropertyValue )
                    {
                    // InternalLanguageTT.g:952:5: (lv_value_1_0= rulePropertyValue )
                    // InternalLanguageTT.g:953:6: lv_value_1_0= rulePropertyValue
                    {

                    						newCompositeNode(grammarAccess.getPropertyOpAccess().getValuePropertyValueParserRuleCall_0_1_0());
                    					
                    pushFollow(FOLLOW_2);
                    lv_value_1_0=rulePropertyValue();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getPropertyOpRule());
                    						}
                    						set(
                    							current,
                    							"value",
                    							lv_value_1_0,
                    							"acco.dsl.LanguageTT.PropertyValue");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalLanguageTT.g:972:3: ( ( (lv_type_2_0= 'different' ) ) ( (lv_value_3_0= rulePropertyValue ) ) )
                    {
                    // InternalLanguageTT.g:972:3: ( ( (lv_type_2_0= 'different' ) ) ( (lv_value_3_0= rulePropertyValue ) ) )
                    // InternalLanguageTT.g:973:4: ( (lv_type_2_0= 'different' ) ) ( (lv_value_3_0= rulePropertyValue ) )
                    {
                    // InternalLanguageTT.g:973:4: ( (lv_type_2_0= 'different' ) )
                    // InternalLanguageTT.g:974:5: (lv_type_2_0= 'different' )
                    {
                    // InternalLanguageTT.g:974:5: (lv_type_2_0= 'different' )
                    // InternalLanguageTT.g:975:6: lv_type_2_0= 'different'
                    {
                    lv_type_2_0=(Token)match(input,34,FOLLOW_25); 

                    						newLeafNode(lv_type_2_0, grammarAccess.getPropertyOpAccess().getTypeDifferentKeyword_1_0_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getPropertyOpRule());
                    						}
                    						setWithLastConsumed(current, "type", lv_type_2_0, "different");
                    					

                    }


                    }

                    // InternalLanguageTT.g:987:4: ( (lv_value_3_0= rulePropertyValue ) )
                    // InternalLanguageTT.g:988:5: (lv_value_3_0= rulePropertyValue )
                    {
                    // InternalLanguageTT.g:988:5: (lv_value_3_0= rulePropertyValue )
                    // InternalLanguageTT.g:989:6: lv_value_3_0= rulePropertyValue
                    {

                    						newCompositeNode(grammarAccess.getPropertyOpAccess().getValuePropertyValueParserRuleCall_1_1_0());
                    					
                    pushFollow(FOLLOW_2);
                    lv_value_3_0=rulePropertyValue();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getPropertyOpRule());
                    						}
                    						set(
                    							current,
                    							"value",
                    							lv_value_3_0,
                    							"acco.dsl.LanguageTT.PropertyValue");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }


                    }
                    break;
                case 3 :
                    // InternalLanguageTT.g:1008:3: ( ( (lv_type_4_0= 'lower' ) ) ( (lv_value_5_0= rulePropertyValue ) ) )
                    {
                    // InternalLanguageTT.g:1008:3: ( ( (lv_type_4_0= 'lower' ) ) ( (lv_value_5_0= rulePropertyValue ) ) )
                    // InternalLanguageTT.g:1009:4: ( (lv_type_4_0= 'lower' ) ) ( (lv_value_5_0= rulePropertyValue ) )
                    {
                    // InternalLanguageTT.g:1009:4: ( (lv_type_4_0= 'lower' ) )
                    // InternalLanguageTT.g:1010:5: (lv_type_4_0= 'lower' )
                    {
                    // InternalLanguageTT.g:1010:5: (lv_type_4_0= 'lower' )
                    // InternalLanguageTT.g:1011:6: lv_type_4_0= 'lower'
                    {
                    lv_type_4_0=(Token)match(input,35,FOLLOW_25); 

                    						newLeafNode(lv_type_4_0, grammarAccess.getPropertyOpAccess().getTypeLowerKeyword_2_0_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getPropertyOpRule());
                    						}
                    						setWithLastConsumed(current, "type", lv_type_4_0, "lower");
                    					

                    }


                    }

                    // InternalLanguageTT.g:1023:4: ( (lv_value_5_0= rulePropertyValue ) )
                    // InternalLanguageTT.g:1024:5: (lv_value_5_0= rulePropertyValue )
                    {
                    // InternalLanguageTT.g:1024:5: (lv_value_5_0= rulePropertyValue )
                    // InternalLanguageTT.g:1025:6: lv_value_5_0= rulePropertyValue
                    {

                    						newCompositeNode(grammarAccess.getPropertyOpAccess().getValuePropertyValueParserRuleCall_2_1_0());
                    					
                    pushFollow(FOLLOW_2);
                    lv_value_5_0=rulePropertyValue();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getPropertyOpRule());
                    						}
                    						set(
                    							current,
                    							"value",
                    							lv_value_5_0,
                    							"acco.dsl.LanguageTT.PropertyValue");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }


                    }
                    break;
                case 4 :
                    // InternalLanguageTT.g:1044:3: ( ( (lv_type_6_0= 'higher' ) ) ( (lv_value_7_0= rulePropertyValue ) ) )
                    {
                    // InternalLanguageTT.g:1044:3: ( ( (lv_type_6_0= 'higher' ) ) ( (lv_value_7_0= rulePropertyValue ) ) )
                    // InternalLanguageTT.g:1045:4: ( (lv_type_6_0= 'higher' ) ) ( (lv_value_7_0= rulePropertyValue ) )
                    {
                    // InternalLanguageTT.g:1045:4: ( (lv_type_6_0= 'higher' ) )
                    // InternalLanguageTT.g:1046:5: (lv_type_6_0= 'higher' )
                    {
                    // InternalLanguageTT.g:1046:5: (lv_type_6_0= 'higher' )
                    // InternalLanguageTT.g:1047:6: lv_type_6_0= 'higher'
                    {
                    lv_type_6_0=(Token)match(input,36,FOLLOW_25); 

                    						newLeafNode(lv_type_6_0, grammarAccess.getPropertyOpAccess().getTypeHigherKeyword_3_0_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getPropertyOpRule());
                    						}
                    						setWithLastConsumed(current, "type", lv_type_6_0, "higher");
                    					

                    }


                    }

                    // InternalLanguageTT.g:1059:4: ( (lv_value_7_0= rulePropertyValue ) )
                    // InternalLanguageTT.g:1060:5: (lv_value_7_0= rulePropertyValue )
                    {
                    // InternalLanguageTT.g:1060:5: (lv_value_7_0= rulePropertyValue )
                    // InternalLanguageTT.g:1061:6: lv_value_7_0= rulePropertyValue
                    {

                    						newCompositeNode(grammarAccess.getPropertyOpAccess().getValuePropertyValueParserRuleCall_3_1_0());
                    					
                    pushFollow(FOLLOW_2);
                    lv_value_7_0=rulePropertyValue();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getPropertyOpRule());
                    						}
                    						set(
                    							current,
                    							"value",
                    							lv_value_7_0,
                    							"acco.dsl.LanguageTT.PropertyValue");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }


                    }
                    break;
                case 5 :
                    // InternalLanguageTT.g:1080:3: ( ( (lv_type_8_0= 'lowerEqual' ) ) ( (lv_value_9_0= rulePropertyValue ) ) )
                    {
                    // InternalLanguageTT.g:1080:3: ( ( (lv_type_8_0= 'lowerEqual' ) ) ( (lv_value_9_0= rulePropertyValue ) ) )
                    // InternalLanguageTT.g:1081:4: ( (lv_type_8_0= 'lowerEqual' ) ) ( (lv_value_9_0= rulePropertyValue ) )
                    {
                    // InternalLanguageTT.g:1081:4: ( (lv_type_8_0= 'lowerEqual' ) )
                    // InternalLanguageTT.g:1082:5: (lv_type_8_0= 'lowerEqual' )
                    {
                    // InternalLanguageTT.g:1082:5: (lv_type_8_0= 'lowerEqual' )
                    // InternalLanguageTT.g:1083:6: lv_type_8_0= 'lowerEqual'
                    {
                    lv_type_8_0=(Token)match(input,37,FOLLOW_25); 

                    						newLeafNode(lv_type_8_0, grammarAccess.getPropertyOpAccess().getTypeLowerEqualKeyword_4_0_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getPropertyOpRule());
                    						}
                    						setWithLastConsumed(current, "type", lv_type_8_0, "lowerEqual");
                    					

                    }


                    }

                    // InternalLanguageTT.g:1095:4: ( (lv_value_9_0= rulePropertyValue ) )
                    // InternalLanguageTT.g:1096:5: (lv_value_9_0= rulePropertyValue )
                    {
                    // InternalLanguageTT.g:1096:5: (lv_value_9_0= rulePropertyValue )
                    // InternalLanguageTT.g:1097:6: lv_value_9_0= rulePropertyValue
                    {

                    						newCompositeNode(grammarAccess.getPropertyOpAccess().getValuePropertyValueParserRuleCall_4_1_0());
                    					
                    pushFollow(FOLLOW_2);
                    lv_value_9_0=rulePropertyValue();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getPropertyOpRule());
                    						}
                    						set(
                    							current,
                    							"value",
                    							lv_value_9_0,
                    							"acco.dsl.LanguageTT.PropertyValue");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }


                    }
                    break;
                case 6 :
                    // InternalLanguageTT.g:1116:3: ( ( (lv_type_10_0= 'higherEqual' ) ) ( (lv_value_11_0= rulePropertyValue ) ) )
                    {
                    // InternalLanguageTT.g:1116:3: ( ( (lv_type_10_0= 'higherEqual' ) ) ( (lv_value_11_0= rulePropertyValue ) ) )
                    // InternalLanguageTT.g:1117:4: ( (lv_type_10_0= 'higherEqual' ) ) ( (lv_value_11_0= rulePropertyValue ) )
                    {
                    // InternalLanguageTT.g:1117:4: ( (lv_type_10_0= 'higherEqual' ) )
                    // InternalLanguageTT.g:1118:5: (lv_type_10_0= 'higherEqual' )
                    {
                    // InternalLanguageTT.g:1118:5: (lv_type_10_0= 'higherEqual' )
                    // InternalLanguageTT.g:1119:6: lv_type_10_0= 'higherEqual'
                    {
                    lv_type_10_0=(Token)match(input,38,FOLLOW_25); 

                    						newLeafNode(lv_type_10_0, grammarAccess.getPropertyOpAccess().getTypeHigherEqualKeyword_5_0_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getPropertyOpRule());
                    						}
                    						setWithLastConsumed(current, "type", lv_type_10_0, "higherEqual");
                    					

                    }


                    }

                    // InternalLanguageTT.g:1131:4: ( (lv_value_11_0= rulePropertyValue ) )
                    // InternalLanguageTT.g:1132:5: (lv_value_11_0= rulePropertyValue )
                    {
                    // InternalLanguageTT.g:1132:5: (lv_value_11_0= rulePropertyValue )
                    // InternalLanguageTT.g:1133:6: lv_value_11_0= rulePropertyValue
                    {

                    						newCompositeNode(grammarAccess.getPropertyOpAccess().getValuePropertyValueParserRuleCall_5_1_0());
                    					
                    pushFollow(FOLLOW_2);
                    lv_value_11_0=rulePropertyValue();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getPropertyOpRule());
                    						}
                    						set(
                    							current,
                    							"value",
                    							lv_value_11_0,
                    							"acco.dsl.LanguageTT.PropertyValue");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePropertyOp"


    // $ANTLR start "entryRulePropertyValue"
    // InternalLanguageTT.g:1155:1: entryRulePropertyValue returns [String current=null] : iv_rulePropertyValue= rulePropertyValue EOF ;
    public final String entryRulePropertyValue() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_rulePropertyValue = null;


        try {
            // InternalLanguageTT.g:1155:53: (iv_rulePropertyValue= rulePropertyValue EOF )
            // InternalLanguageTT.g:1156:2: iv_rulePropertyValue= rulePropertyValue EOF
            {
             newCompositeNode(grammarAccess.getPropertyValueRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePropertyValue=rulePropertyValue();

            state._fsp--;

             current =iv_rulePropertyValue.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePropertyValue"


    // $ANTLR start "rulePropertyValue"
    // InternalLanguageTT.g:1162:1: rulePropertyValue returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_BOOLEAN_0= RULE_BOOLEAN | this_INT_1= RULE_INT | this_STRING_2= RULE_STRING ) ;
    public final AntlrDatatypeRuleToken rulePropertyValue() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_BOOLEAN_0=null;
        Token this_INT_1=null;
        Token this_STRING_2=null;


        	enterRule();

        try {
            // InternalLanguageTT.g:1168:2: ( (this_BOOLEAN_0= RULE_BOOLEAN | this_INT_1= RULE_INT | this_STRING_2= RULE_STRING ) )
            // InternalLanguageTT.g:1169:2: (this_BOOLEAN_0= RULE_BOOLEAN | this_INT_1= RULE_INT | this_STRING_2= RULE_STRING )
            {
            // InternalLanguageTT.g:1169:2: (this_BOOLEAN_0= RULE_BOOLEAN | this_INT_1= RULE_INT | this_STRING_2= RULE_STRING )
            int alt9=3;
            switch ( input.LA(1) ) {
            case RULE_BOOLEAN:
                {
                alt9=1;
                }
                break;
            case RULE_INT:
                {
                alt9=2;
                }
                break;
            case RULE_STRING:
                {
                alt9=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;
            }

            switch (alt9) {
                case 1 :
                    // InternalLanguageTT.g:1170:3: this_BOOLEAN_0= RULE_BOOLEAN
                    {
                    this_BOOLEAN_0=(Token)match(input,RULE_BOOLEAN,FOLLOW_2); 

                    			current.merge(this_BOOLEAN_0);
                    		

                    			newLeafNode(this_BOOLEAN_0, grammarAccess.getPropertyValueAccess().getBOOLEANTerminalRuleCall_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalLanguageTT.g:1178:3: this_INT_1= RULE_INT
                    {
                    this_INT_1=(Token)match(input,RULE_INT,FOLLOW_2); 

                    			current.merge(this_INT_1);
                    		

                    			newLeafNode(this_INT_1, grammarAccess.getPropertyValueAccess().getINTTerminalRuleCall_1());
                    		

                    }
                    break;
                case 3 :
                    // InternalLanguageTT.g:1186:3: this_STRING_2= RULE_STRING
                    {
                    this_STRING_2=(Token)match(input,RULE_STRING,FOLLOW_2); 

                    			current.merge(this_STRING_2);
                    		

                    			newLeafNode(this_STRING_2, grammarAccess.getPropertyValueAccess().getSTRINGTerminalRuleCall_2());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePropertyValue"


    // $ANTLR start "entryRuleQualifiedName"
    // InternalLanguageTT.g:1197:1: entryRuleQualifiedName returns [String current=null] : iv_ruleQualifiedName= ruleQualifiedName EOF ;
    public final String entryRuleQualifiedName() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleQualifiedName = null;


        try {
            // InternalLanguageTT.g:1197:53: (iv_ruleQualifiedName= ruleQualifiedName EOF )
            // InternalLanguageTT.g:1198:2: iv_ruleQualifiedName= ruleQualifiedName EOF
            {
             newCompositeNode(grammarAccess.getQualifiedNameRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleQualifiedName=ruleQualifiedName();

            state._fsp--;

             current =iv_ruleQualifiedName.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQualifiedName"


    // $ANTLR start "ruleQualifiedName"
    // InternalLanguageTT.g:1204:1: ruleQualifiedName returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* ) ;
    public final AntlrDatatypeRuleToken ruleQualifiedName() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_ID_0=null;
        Token kw=null;
        Token this_ID_2=null;


        	enterRule();

        try {
            // InternalLanguageTT.g:1210:2: ( (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* ) )
            // InternalLanguageTT.g:1211:2: (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* )
            {
            // InternalLanguageTT.g:1211:2: (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* )
            // InternalLanguageTT.g:1212:3: this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )*
            {
            this_ID_0=(Token)match(input,RULE_ID,FOLLOW_26); 

            			current.merge(this_ID_0);
            		

            			newLeafNode(this_ID_0, grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_0());
            		
            // InternalLanguageTT.g:1219:3: (kw= '.' this_ID_2= RULE_ID )*
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( (LA10_0==39) ) {
                    alt10=1;
                }


                switch (alt10) {
            	case 1 :
            	    // InternalLanguageTT.g:1220:4: kw= '.' this_ID_2= RULE_ID
            	    {
            	    kw=(Token)match(input,39,FOLLOW_3); 

            	    				current.merge(kw);
            	    				newLeafNode(kw, grammarAccess.getQualifiedNameAccess().getFullStopKeyword_1_0());
            	    			
            	    this_ID_2=(Token)match(input,RULE_ID,FOLLOW_26); 

            	    				current.merge(this_ID_2);
            	    			

            	    				newLeafNode(this_ID_2, grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_1_1());
            	    			

            	    }
            	    break;

            	default :
            	    break loop10;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQualifiedName"


    // $ANTLR start "rulePropertyType"
    // InternalLanguageTT.g:1237:1: rulePropertyType returns [Enumerator current=null] : ( (enumLiteral_0= 'Integer' ) | (enumLiteral_1= 'Boolean' ) | (enumLiteral_2= 'String' ) ) ;
    public final Enumerator rulePropertyType() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;


        	enterRule();

        try {
            // InternalLanguageTT.g:1243:2: ( ( (enumLiteral_0= 'Integer' ) | (enumLiteral_1= 'Boolean' ) | (enumLiteral_2= 'String' ) ) )
            // InternalLanguageTT.g:1244:2: ( (enumLiteral_0= 'Integer' ) | (enumLiteral_1= 'Boolean' ) | (enumLiteral_2= 'String' ) )
            {
            // InternalLanguageTT.g:1244:2: ( (enumLiteral_0= 'Integer' ) | (enumLiteral_1= 'Boolean' ) | (enumLiteral_2= 'String' ) )
            int alt11=3;
            switch ( input.LA(1) ) {
            case 40:
                {
                alt11=1;
                }
                break;
            case 41:
                {
                alt11=2;
                }
                break;
            case 42:
                {
                alt11=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 11, 0, input);

                throw nvae;
            }

            switch (alt11) {
                case 1 :
                    // InternalLanguageTT.g:1245:3: (enumLiteral_0= 'Integer' )
                    {
                    // InternalLanguageTT.g:1245:3: (enumLiteral_0= 'Integer' )
                    // InternalLanguageTT.g:1246:4: enumLiteral_0= 'Integer'
                    {
                    enumLiteral_0=(Token)match(input,40,FOLLOW_2); 

                    				current = grammarAccess.getPropertyTypeAccess().getIntegerEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_0, grammarAccess.getPropertyTypeAccess().getIntegerEnumLiteralDeclaration_0());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalLanguageTT.g:1253:3: (enumLiteral_1= 'Boolean' )
                    {
                    // InternalLanguageTT.g:1253:3: (enumLiteral_1= 'Boolean' )
                    // InternalLanguageTT.g:1254:4: enumLiteral_1= 'Boolean'
                    {
                    enumLiteral_1=(Token)match(input,41,FOLLOW_2); 

                    				current = grammarAccess.getPropertyTypeAccess().getBooleanEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_1, grammarAccess.getPropertyTypeAccess().getBooleanEnumLiteralDeclaration_1());
                    			

                    }


                    }
                    break;
                case 3 :
                    // InternalLanguageTT.g:1261:3: (enumLiteral_2= 'String' )
                    {
                    // InternalLanguageTT.g:1261:3: (enumLiteral_2= 'String' )
                    // InternalLanguageTT.g:1262:4: enumLiteral_2= 'String'
                    {
                    enumLiteral_2=(Token)match(input,42,FOLLOW_2); 

                    				current = grammarAccess.getPropertyTypeAccess().getStringEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_2, grammarAccess.getPropertyTypeAccess().getStringEnumLiteralDeclaration_2());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePropertyType"


    // $ANTLR start "ruleBooleanEnum"
    // InternalLanguageTT.g:1272:1: ruleBooleanEnum returns [Enumerator current=null] : ( (enumLiteral_0= 'True' ) | (enumLiteral_1= 'False' ) ) ;
    public final Enumerator ruleBooleanEnum() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;


        	enterRule();

        try {
            // InternalLanguageTT.g:1278:2: ( ( (enumLiteral_0= 'True' ) | (enumLiteral_1= 'False' ) ) )
            // InternalLanguageTT.g:1279:2: ( (enumLiteral_0= 'True' ) | (enumLiteral_1= 'False' ) )
            {
            // InternalLanguageTT.g:1279:2: ( (enumLiteral_0= 'True' ) | (enumLiteral_1= 'False' ) )
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==43) ) {
                alt12=1;
            }
            else if ( (LA12_0==44) ) {
                alt12=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 12, 0, input);

                throw nvae;
            }
            switch (alt12) {
                case 1 :
                    // InternalLanguageTT.g:1280:3: (enumLiteral_0= 'True' )
                    {
                    // InternalLanguageTT.g:1280:3: (enumLiteral_0= 'True' )
                    // InternalLanguageTT.g:1281:4: enumLiteral_0= 'True'
                    {
                    enumLiteral_0=(Token)match(input,43,FOLLOW_2); 

                    				current = grammarAccess.getBooleanEnumAccess().getTrueEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_0, grammarAccess.getBooleanEnumAccess().getTrueEnumLiteralDeclaration_0());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalLanguageTT.g:1288:3: (enumLiteral_1= 'False' )
                    {
                    // InternalLanguageTT.g:1288:3: (enumLiteral_1= 'False' )
                    // InternalLanguageTT.g:1289:4: enumLiteral_1= 'False'
                    {
                    enumLiteral_1=(Token)match(input,44,FOLLOW_2); 

                    				current = grammarAccess.getBooleanEnumAccess().getFalseEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_1, grammarAccess.getBooleanEnumAccess().getFalseEnumLiteralDeclaration_1());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBooleanEnum"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x00000000001C8002L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000188002L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000108002L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000008002L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000032400000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000020002L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000070000000000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000800002L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000004000000L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000008000000L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000180000000000L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000040000000L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000007E00000000L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000000100000000L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x00000000000000E0L});
    public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0000008000000002L});

}