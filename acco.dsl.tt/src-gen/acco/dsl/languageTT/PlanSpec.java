/**
 * generated by Xtext 2.11.0
 */
package acco.dsl.languageTT;

import it.unibo.xtext.qactor.QActorSystemSpec;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Plan Spec</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link acco.dsl.languageTT.PlanSpec#getSystem <em>System</em>}</li>
 *   <li>{@link acco.dsl.languageTT.PlanSpec#getProperties <em>Properties</em>}</li>
 *   <li>{@link acco.dsl.languageTT.PlanSpec#getContexts <em>Contexts</em>}</li>
 *   <li>{@link acco.dsl.languageTT.PlanSpec#getActors <em>Actors</em>}</li>
 *   <li>{@link acco.dsl.languageTT.PlanSpec#getTests <em>Tests</em>}</li>
 * </ul>
 *
 * @see acco.dsl.languageTT.LanguageTTPackage#getPlanSpec()
 * @model
 * @generated
 */
public interface PlanSpec extends EObject
{
  /**
   * Returns the value of the '<em><b>System</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>System</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>System</em>' reference.
   * @see #setSystem(QActorSystemSpec)
   * @see acco.dsl.languageTT.LanguageTTPackage#getPlanSpec_System()
   * @model
   * @generated
   */
  QActorSystemSpec getSystem();

  /**
   * Sets the value of the '{@link acco.dsl.languageTT.PlanSpec#getSystem <em>System</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>System</em>' reference.
   * @see #getSystem()
   * @generated
   */
  void setSystem(QActorSystemSpec value);

  /**
   * Returns the value of the '<em><b>Properties</b></em>' containment reference list.
   * The list contents are of type {@link acco.dsl.languageTT.Property}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Properties</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Properties</em>' containment reference list.
   * @see acco.dsl.languageTT.LanguageTTPackage#getPlanSpec_Properties()
   * @model containment="true"
   * @generated
   */
  EList<Property> getProperties();

  /**
   * Returns the value of the '<em><b>Contexts</b></em>' containment reference list.
   * The list contents are of type {@link acco.dsl.languageTT.Context}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Contexts</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Contexts</em>' containment reference list.
   * @see acco.dsl.languageTT.LanguageTTPackage#getPlanSpec_Contexts()
   * @model containment="true"
   * @generated
   */
  EList<Context> getContexts();

  /**
   * Returns the value of the '<em><b>Actors</b></em>' containment reference list.
   * The list contents are of type {@link acco.dsl.languageTT.QActor}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Actors</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Actors</em>' containment reference list.
   * @see acco.dsl.languageTT.LanguageTTPackage#getPlanSpec_Actors()
   * @model containment="true"
   * @generated
   */
  EList<QActor> getActors();

  /**
   * Returns the value of the '<em><b>Tests</b></em>' containment reference list.
   * The list contents are of type {@link acco.dsl.languageTT.Test}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Tests</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Tests</em>' containment reference list.
   * @see acco.dsl.languageTT.LanguageTTPackage#getPlanSpec_Tests()
   * @model containment="true"
   * @generated
   */
  EList<Test> getTests();

} // PlanSpec
