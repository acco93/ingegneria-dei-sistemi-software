/**
 * generated by Xtext 2.11.0
 */
package acco.dsl.languageTT.impl;

import acco.dsl.languageTT.Context;
import acco.dsl.languageTT.LanguageTTPackage;
import acco.dsl.languageTT.PlanSpec;
import acco.dsl.languageTT.Property;
import acco.dsl.languageTT.QActor;
import acco.dsl.languageTT.Test;

import it.unibo.xtext.qactor.QActorSystemSpec;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Plan Spec</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link acco.dsl.languageTT.impl.PlanSpecImpl#getSystem <em>System</em>}</li>
 *   <li>{@link acco.dsl.languageTT.impl.PlanSpecImpl#getProperties <em>Properties</em>}</li>
 *   <li>{@link acco.dsl.languageTT.impl.PlanSpecImpl#getContexts <em>Contexts</em>}</li>
 *   <li>{@link acco.dsl.languageTT.impl.PlanSpecImpl#getActors <em>Actors</em>}</li>
 *   <li>{@link acco.dsl.languageTT.impl.PlanSpecImpl#getTests <em>Tests</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PlanSpecImpl extends MinimalEObjectImpl.Container implements PlanSpec
{
  /**
   * The cached value of the '{@link #getSystem() <em>System</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSystem()
   * @generated
   * @ordered
   */
  protected QActorSystemSpec system;

  /**
   * The cached value of the '{@link #getProperties() <em>Properties</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getProperties()
   * @generated
   * @ordered
   */
  protected EList<Property> properties;

  /**
   * The cached value of the '{@link #getContexts() <em>Contexts</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getContexts()
   * @generated
   * @ordered
   */
  protected EList<Context> contexts;

  /**
   * The cached value of the '{@link #getActors() <em>Actors</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getActors()
   * @generated
   * @ordered
   */
  protected EList<QActor> actors;

  /**
   * The cached value of the '{@link #getTests() <em>Tests</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTests()
   * @generated
   * @ordered
   */
  protected EList<Test> tests;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected PlanSpecImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return LanguageTTPackage.Literals.PLAN_SPEC;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public QActorSystemSpec getSystem()
  {
    if (system != null && system.eIsProxy())
    {
      InternalEObject oldSystem = (InternalEObject)system;
      system = (QActorSystemSpec)eResolveProxy(oldSystem);
      if (system != oldSystem)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, LanguageTTPackage.PLAN_SPEC__SYSTEM, oldSystem, system));
      }
    }
    return system;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public QActorSystemSpec basicGetSystem()
  {
    return system;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSystem(QActorSystemSpec newSystem)
  {
    QActorSystemSpec oldSystem = system;
    system = newSystem;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, LanguageTTPackage.PLAN_SPEC__SYSTEM, oldSystem, system));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Property> getProperties()
  {
    if (properties == null)
    {
      properties = new EObjectContainmentEList<Property>(Property.class, this, LanguageTTPackage.PLAN_SPEC__PROPERTIES);
    }
    return properties;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Context> getContexts()
  {
    if (contexts == null)
    {
      contexts = new EObjectContainmentEList<Context>(Context.class, this, LanguageTTPackage.PLAN_SPEC__CONTEXTS);
    }
    return contexts;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<QActor> getActors()
  {
    if (actors == null)
    {
      actors = new EObjectContainmentEList<QActor>(QActor.class, this, LanguageTTPackage.PLAN_SPEC__ACTORS);
    }
    return actors;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Test> getTests()
  {
    if (tests == null)
    {
      tests = new EObjectContainmentEList<Test>(Test.class, this, LanguageTTPackage.PLAN_SPEC__TESTS);
    }
    return tests;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case LanguageTTPackage.PLAN_SPEC__PROPERTIES:
        return ((InternalEList<?>)getProperties()).basicRemove(otherEnd, msgs);
      case LanguageTTPackage.PLAN_SPEC__CONTEXTS:
        return ((InternalEList<?>)getContexts()).basicRemove(otherEnd, msgs);
      case LanguageTTPackage.PLAN_SPEC__ACTORS:
        return ((InternalEList<?>)getActors()).basicRemove(otherEnd, msgs);
      case LanguageTTPackage.PLAN_SPEC__TESTS:
        return ((InternalEList<?>)getTests()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case LanguageTTPackage.PLAN_SPEC__SYSTEM:
        if (resolve) return getSystem();
        return basicGetSystem();
      case LanguageTTPackage.PLAN_SPEC__PROPERTIES:
        return getProperties();
      case LanguageTTPackage.PLAN_SPEC__CONTEXTS:
        return getContexts();
      case LanguageTTPackage.PLAN_SPEC__ACTORS:
        return getActors();
      case LanguageTTPackage.PLAN_SPEC__TESTS:
        return getTests();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case LanguageTTPackage.PLAN_SPEC__SYSTEM:
        setSystem((QActorSystemSpec)newValue);
        return;
      case LanguageTTPackage.PLAN_SPEC__PROPERTIES:
        getProperties().clear();
        getProperties().addAll((Collection<? extends Property>)newValue);
        return;
      case LanguageTTPackage.PLAN_SPEC__CONTEXTS:
        getContexts().clear();
        getContexts().addAll((Collection<? extends Context>)newValue);
        return;
      case LanguageTTPackage.PLAN_SPEC__ACTORS:
        getActors().clear();
        getActors().addAll((Collection<? extends QActor>)newValue);
        return;
      case LanguageTTPackage.PLAN_SPEC__TESTS:
        getTests().clear();
        getTests().addAll((Collection<? extends Test>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case LanguageTTPackage.PLAN_SPEC__SYSTEM:
        setSystem((QActorSystemSpec)null);
        return;
      case LanguageTTPackage.PLAN_SPEC__PROPERTIES:
        getProperties().clear();
        return;
      case LanguageTTPackage.PLAN_SPEC__CONTEXTS:
        getContexts().clear();
        return;
      case LanguageTTPackage.PLAN_SPEC__ACTORS:
        getActors().clear();
        return;
      case LanguageTTPackage.PLAN_SPEC__TESTS:
        getTests().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case LanguageTTPackage.PLAN_SPEC__SYSTEM:
        return system != null;
      case LanguageTTPackage.PLAN_SPEC__PROPERTIES:
        return properties != null && !properties.isEmpty();
      case LanguageTTPackage.PLAN_SPEC__CONTEXTS:
        return contexts != null && !contexts.isEmpty();
      case LanguageTTPackage.PLAN_SPEC__ACTORS:
        return actors != null && !actors.isEmpty();
      case LanguageTTPackage.PLAN_SPEC__TESTS:
        return tests != null && !tests.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //PlanSpecImpl
