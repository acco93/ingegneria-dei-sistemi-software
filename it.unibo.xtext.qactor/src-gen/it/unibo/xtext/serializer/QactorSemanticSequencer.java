package it.unibo.xtext.serializer;

import com.google.inject.Inject;
import com.google.inject.Provider;
import it.unibo.xtext.qactor.Action;
import it.unibo.xtext.qactor.ActorOp;
import it.unibo.xtext.qactor.AddRule;
import it.unibo.xtext.qactor.AnswerEvent;
import it.unibo.xtext.qactor.BasicRobotMove;
import it.unibo.xtext.qactor.ComponentIP;
import it.unibo.xtext.qactor.Context;
import it.unibo.xtext.qactor.Continuation;
import it.unibo.xtext.qactor.ContinueEvent;
import it.unibo.xtext.qactor.Delay;
import it.unibo.xtext.qactor.Demo;
import it.unibo.xtext.qactor.Dispatch;
import it.unibo.xtext.qactor.EndActor;
import it.unibo.xtext.qactor.EndPlan;
import it.unibo.xtext.qactor.Event;
import it.unibo.xtext.qactor.EventHandler;
import it.unibo.xtext.qactor.EventHandlerBody;
import it.unibo.xtext.qactor.EventSwitch;
import it.unibo.xtext.qactor.ExecuteAction;
import it.unibo.xtext.qactor.GetActivationEvent;
import it.unibo.xtext.qactor.GetSensedEvent;
import it.unibo.xtext.qactor.Guard;
import it.unibo.xtext.qactor.GuardPredicateRemovable;
import it.unibo.xtext.qactor.GuardPredicateStable;
import it.unibo.xtext.qactor.IntegerData;
import it.unibo.xtext.qactor.Invitation;
import it.unibo.xtext.qactor.LoadPlan;
import it.unibo.xtext.qactor.MemoCurrentEvent;
import it.unibo.xtext.qactor.MemoCurrentMessage;
import it.unibo.xtext.qactor.MemoEvent;
import it.unibo.xtext.qactor.MemoOperation;
import it.unibo.xtext.qactor.MoveFile;
import it.unibo.xtext.qactor.MsgSelect;
import it.unibo.xtext.qactor.MsgSpec;
import it.unibo.xtext.qactor.MsgSwitch;
import it.unibo.xtext.qactor.NormalEvent;
import it.unibo.xtext.qactor.OnReceiveMsg;
import it.unibo.xtext.qactor.PActorCall;
import it.unibo.xtext.qactor.PAtomCut;
import it.unibo.xtext.qactor.PAtomNum;
import it.unibo.xtext.qactor.PAtomString;
import it.unibo.xtext.qactor.PAtomic;
import it.unibo.xtext.qactor.PIs;
import it.unibo.xtext.qactor.PStruct;
import it.unibo.xtext.qactor.Photo;
import it.unibo.xtext.qactor.Plan;
import it.unibo.xtext.qactor.PlanAction;
import it.unibo.xtext.qactor.Print;
import it.unibo.xtext.qactor.PrintCurrentEvent;
import it.unibo.xtext.qactor.PrintCurrentMessage;
import it.unibo.xtext.qactor.QActor;
import it.unibo.xtext.qactor.QActorSystem;
import it.unibo.xtext.qactor.QActorSystemSpec;
import it.unibo.xtext.qactor.QactorPackage;
import it.unibo.xtext.qactor.RaiseEvent;
import it.unibo.xtext.qactor.Reaction;
import it.unibo.xtext.qactor.ReceiveMsg;
import it.unibo.xtext.qactor.RemoveRule;
import it.unibo.xtext.qactor.RepeatPlan;
import it.unibo.xtext.qactor.ReplyToCaller;
import it.unibo.xtext.qactor.Request;
import it.unibo.xtext.qactor.ResumePlan;
import it.unibo.xtext.qactor.Robot;
import it.unibo.xtext.qactor.Rule;
import it.unibo.xtext.qactor.RunPlan;
import it.unibo.xtext.qactor.SendDispatch;
import it.unibo.xtext.qactor.SendEventAsDispatch;
import it.unibo.xtext.qactor.SendRequest;
import it.unibo.xtext.qactor.SenseEvent;
import it.unibo.xtext.qactor.Signal;
import it.unibo.xtext.qactor.SolveGoal;
import it.unibo.xtext.qactor.SolveOperation;
import it.unibo.xtext.qactor.Sound;
import it.unibo.xtext.qactor.StringData;
import it.unibo.xtext.qactor.SuspendPlan;
import it.unibo.xtext.qactor.SwitchPlan;
import it.unibo.xtext.qactor.TimeLimit;
import it.unibo.xtext.qactor.Token;
import it.unibo.xtext.qactor.VarOrAtomOrPStruct;
import it.unibo.xtext.qactor.VarOrAtomic;
import it.unibo.xtext.qactor.VarOrPStruct;
import it.unibo.xtext.qactor.VarOrPhead;
import it.unibo.xtext.qactor.VarOrQactor;
import it.unibo.xtext.qactor.VarOrString;
import it.unibo.xtext.qactor.Variable;
import it.unibo.xtext.qactor.Video;
import it.unibo.xtext.services.QactorGrammarAccess;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.serializer.acceptor.ISemanticSequenceAcceptor;
import org.eclipse.xtext.serializer.acceptor.SequenceFeeder;
import org.eclipse.xtext.serializer.diagnostic.ISemanticSequencerDiagnosticProvider;
import org.eclipse.xtext.serializer.diagnostic.ISerializationDiagnostic.Acceptor;
import org.eclipse.xtext.serializer.sequencer.AbstractDelegatingSemanticSequencer;
import org.eclipse.xtext.serializer.sequencer.GenericSequencer;
import org.eclipse.xtext.serializer.sequencer.ISemanticNodeProvider.INodesForEObjectProvider;
import org.eclipse.xtext.serializer.sequencer.ISemanticSequencer;
import org.eclipse.xtext.serializer.sequencer.ITransientValueService;
import org.eclipse.xtext.serializer.sequencer.ITransientValueService.ValueTransient;

@SuppressWarnings("all")
public class QactorSemanticSequencer extends AbstractDelegatingSemanticSequencer {

	@Inject
	private QactorGrammarAccess grammarAccess;
	
	public void createSequence(EObject context, EObject semanticObject) {
		if(semanticObject.eClass().getEPackage() == QactorPackage.eINSTANCE) switch(semanticObject.eClass().getClassifierID()) {
			case QactorPackage.ACTION:
				if(context == grammarAccess.getActionRule()) {
					sequence_Action(context, (Action) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.ACTOR_OP:
				if(context == grammarAccess.getActionMoveRule() ||
				   context == grammarAccess.getActorOpRule() ||
				   context == grammarAccess.getMoveRule()) {
					sequence_ActorOp(context, (ActorOp) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.ADD_RULE:
				if(context == grammarAccess.getAddRuleRule() ||
				   context == grammarAccess.getGuardMoveRule() ||
				   context == grammarAccess.getMoveRule()) {
					sequence_AddRule(context, (AddRule) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.ANSWER_EVENT:
				if(context == grammarAccess.getAnswerEventRule()) {
					sequence_AnswerEvent(context, (AnswerEvent) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.BASIC_ROBOT_MOVE:
				if(context == grammarAccess.getBasicRobotMoveRule() ||
				   context == grammarAccess.getMoveRule()) {
					sequence_BasicRobotMove(context, (BasicRobotMove) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.COMPONENT_IP:
				if(context == grammarAccess.getComponentIPRule()) {
					sequence_ComponentIP(context, (ComponentIP) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.CONTEXT:
				if(context == grammarAccess.getContextRule()) {
					sequence_Context(context, (Context) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.CONTINUATION:
				if(context == grammarAccess.getContinuationRule()) {
					sequence_Continuation(context, (Continuation) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.CONTINUE_EVENT:
				if(context == grammarAccess.getAlarmEventRule() ||
				   context == grammarAccess.getContinueEventRule()) {
					sequence_ContinueEvent(context, (ContinueEvent) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.DELAY:
				if(context == grammarAccess.getDelayRule() ||
				   context == grammarAccess.getExtensionMoveRule() ||
				   context == grammarAccess.getMoveRule()) {
					sequence_Delay(context, (Delay) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.DEMO:
				if(context == grammarAccess.getActionMoveRule() ||
				   context == grammarAccess.getDemoRule() ||
				   context == grammarAccess.getMoveRule()) {
					sequence_Demo(context, (Demo) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.DISPATCH:
				if(context == grammarAccess.getDispatchRule() ||
				   context == grammarAccess.getMessageRule() ||
				   context == grammarAccess.getOutOnlyMessageRule()) {
					sequence_Dispatch(context, (Dispatch) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.END_ACTOR:
				if(context == grammarAccess.getEndActorRule() ||
				   context == grammarAccess.getMoveRule() ||
				   context == grammarAccess.getPlanMoveRule()) {
					sequence_EndActor(context, (EndActor) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.END_PLAN:
				if(context == grammarAccess.getEndPlanRule() ||
				   context == grammarAccess.getMoveRule() ||
				   context == grammarAccess.getPlanMoveRule()) {
					sequence_EndPlan(context, (EndPlan) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.EVENT:
				if(context == grammarAccess.getEventRule() ||
				   context == grammarAccess.getMessageRule() ||
				   context == grammarAccess.getOutOnlyMessageRule()) {
					sequence_Event(context, (Event) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.EVENT_HANDLER:
				if(context == grammarAccess.getEventHandlerRule()) {
					sequence_EventHandler(context, (EventHandler) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.EVENT_HANDLER_BODY:
				if(context == grammarAccess.getEventHandlerBodyRule()) {
					sequence_EventHandlerBody(context, (EventHandlerBody) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.EVENT_SWITCH:
				if(context == grammarAccess.getEventSwitchRule() ||
				   context == grammarAccess.getMessageMoveRule() ||
				   context == grammarAccess.getMoveRule()) {
					sequence_EventSwitch(context, (EventSwitch) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.EXECUTE_ACTION:
				if(context == grammarAccess.getActionMoveRule() ||
				   context == grammarAccess.getExecuteActionRule() ||
				   context == grammarAccess.getMoveRule()) {
					sequence_ExecuteAction(context, (ExecuteAction) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.GET_ACTIVATION_EVENT:
				if(context == grammarAccess.getGetActivationEventRule() ||
				   context == grammarAccess.getMoveRule() ||
				   context == grammarAccess.getPlanMoveRule()) {
					sequence_GetActivationEvent(context, (GetActivationEvent) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.GET_SENSED_EVENT:
				if(context == grammarAccess.getGetSensedEventRule() ||
				   context == grammarAccess.getMoveRule() ||
				   context == grammarAccess.getPlanMoveRule()) {
					sequence_GetSensedEvent(context, (GetSensedEvent) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.GUARD:
				if(context == grammarAccess.getGuardRule()) {
					sequence_Guard(context, (Guard) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.GUARD_PREDICATE_REMOVABLE:
				if(context == grammarAccess.getGuardPredicateRule() ||
				   context == grammarAccess.getGuardPredicateRemovableRule()) {
					sequence_GuardPredicateRemovable(context, (GuardPredicateRemovable) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.GUARD_PREDICATE_STABLE:
				if(context == grammarAccess.getGuardPredicateRule() ||
				   context == grammarAccess.getGuardPredicateStableRule()) {
					sequence_GuardPredicateStable(context, (GuardPredicateStable) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.INTEGER_DATA:
				if(context == grammarAccess.getDataRule() ||
				   context == grammarAccess.getIntegerDataRule()) {
					sequence_IntegerData(context, (IntegerData) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.INVITATION:
				if(context == grammarAccess.getInvitationRule() ||
				   context == grammarAccess.getMessageRule() ||
				   context == grammarAccess.getOutInMessageRule()) {
					sequence_Invitation(context, (Invitation) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.LOAD_PLAN:
				if(context == grammarAccess.getLoadPlanRule() ||
				   context == grammarAccess.getMoveRule() ||
				   context == grammarAccess.getPlanMoveRule()) {
					sequence_LoadPlan(context, (LoadPlan) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.MEMO_CURRENT_EVENT:
				if(context == grammarAccess.getBasicMoveRule() ||
				   context == grammarAccess.getMemoCurrentEventRule() ||
				   context == grammarAccess.getMoveRule()) {
					sequence_MemoCurrentEvent(context, (MemoCurrentEvent) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.MEMO_CURRENT_MESSAGE:
				if(context == grammarAccess.getBasicMoveRule() ||
				   context == grammarAccess.getMemoCurrentMessageRule() ||
				   context == grammarAccess.getMoveRule()) {
					sequence_MemoCurrentMessage(context, (MemoCurrentMessage) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.MEMO_EVENT:
				if(context == grammarAccess.getMemoEventRule() ||
				   context == grammarAccess.getMemoRuleRule()) {
					sequence_MemoEvent(context, (MemoEvent) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.MEMO_OPERATION:
				if(context == grammarAccess.getEventHandlerOperationRule() ||
				   context == grammarAccess.getMemoOperationRule()) {
					sequence_MemoOperation(context, (MemoOperation) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.MOVE_FILE:
				if(context == grammarAccess.getMoveFileRule()) {
					sequence_MoveFile(context, (MoveFile) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.MSG_SELECT:
				if(context == grammarAccess.getMessageMoveRule() ||
				   context == grammarAccess.getMoveRule() ||
				   context == grammarAccess.getMsgSelectRule()) {
					sequence_MsgSelect(context, (MsgSelect) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.MSG_SPEC:
				if(context == grammarAccess.getMsgSpecRule()) {
					sequence_MsgSpec(context, (MsgSpec) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.MSG_SWITCH:
				if(context == grammarAccess.getMessageMoveRule() ||
				   context == grammarAccess.getMoveRule() ||
				   context == grammarAccess.getMsgSwitchRule()) {
					sequence_MsgSwitch(context, (MsgSwitch) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.NORMAL_EVENT:
				if(context == grammarAccess.getAlarmEventRule() ||
				   context == grammarAccess.getNormalEventRule()) {
					sequence_NormalEvent(context, (NormalEvent) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.ON_RECEIVE_MSG:
				if(context == grammarAccess.getMessageMoveRule() ||
				   context == grammarAccess.getMoveRule() ||
				   context == grammarAccess.getOnReceiveMsgRule()) {
					sequence_OnReceiveMsg(context, (OnReceiveMsg) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.PACTOR_CALL:
				if(context == grammarAccess.getPActorCallRule() ||
				   context == grammarAccess.getPTermRule()) {
					sequence_PActorCall(context, (PActorCall) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.PATOM_CUT:
				if(context == grammarAccess.getPAtomCutRule() ||
				   context == grammarAccess.getPPredefRule() ||
				   context == grammarAccess.getPTermRule()) {
					sequence_PAtomCut(context, (PAtomCut) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.PATOM_NUM:
				if(context == grammarAccess.getPAtomRule() ||
				   context == grammarAccess.getPAtomNumRule() ||
				   context == grammarAccess.getPHeadRule() ||
				   context == grammarAccess.getPTermRule()) {
					sequence_PAtomNum(context, (PAtomNum) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.PATOM_STRING:
				if(context == grammarAccess.getPAtomRule() ||
				   context == grammarAccess.getPAtomStringRule() ||
				   context == grammarAccess.getPHeadRule() ||
				   context == grammarAccess.getPTermRule()) {
					sequence_PAtomString(context, (PAtomString) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.PATOMIC:
				if(context == grammarAccess.getPAtomRule() ||
				   context == grammarAccess.getPAtomicRule() ||
				   context == grammarAccess.getPHeadRule() ||
				   context == grammarAccess.getPTermRule()) {
					sequence_PAtomic(context, (PAtomic) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.PIS:
				if(context == grammarAccess.getPIsRule() ||
				   context == grammarAccess.getPPredefRule() ||
				   context == grammarAccess.getPTermRule()) {
					sequence_PIs(context, (PIs) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.PSTRUCT:
				if(context == grammarAccess.getPHeadRule() ||
				   context == grammarAccess.getPStructRule() ||
				   context == grammarAccess.getPTermRule()) {
					sequence_PStruct(context, (PStruct) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.PHOTO:
				if(context == grammarAccess.getExtensionMoveRule() ||
				   context == grammarAccess.getMoveRule() ||
				   context == grammarAccess.getPhotoRule()) {
					sequence_Photo(context, (Photo) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.PLAN:
				if(context == grammarAccess.getPlanRule()) {
					sequence_Plan(context, (Plan) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.PLAN_ACTION:
				if(context == grammarAccess.getPlanActionRule()) {
					sequence_PlanAction(context, (PlanAction) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.PRINT:
				if(context == grammarAccess.getBasicMoveRule() ||
				   context == grammarAccess.getMoveRule() ||
				   context == grammarAccess.getPrintRule()) {
					sequence_Print(context, (Print) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.PRINT_CURRENT_EVENT:
				if(context == grammarAccess.getBasicMoveRule() ||
				   context == grammarAccess.getMoveRule() ||
				   context == grammarAccess.getPrintCurrentEventRule()) {
					sequence_PrintCurrentEvent(context, (PrintCurrentEvent) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.PRINT_CURRENT_MESSAGE:
				if(context == grammarAccess.getBasicMoveRule() ||
				   context == grammarAccess.getMoveRule() ||
				   context == grammarAccess.getPrintCurrentMessageRule()) {
					sequence_PrintCurrentMessage(context, (PrintCurrentMessage) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.QACTOR:
				if(context == grammarAccess.getQActorRule()) {
					sequence_QActor(context, (QActor) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.QACTOR_SYSTEM:
				if(context == grammarAccess.getQActorSystemRule()) {
					sequence_QActorSystem(context, (QActorSystem) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.QACTOR_SYSTEM_SPEC:
				if(context == grammarAccess.getQActorSystemSpecRule()) {
					sequence_QActorSystemSpec(context, (QActorSystemSpec) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.RAISE_EVENT:
				if(context == grammarAccess.getEventHandlerOperationRule() ||
				   context == grammarAccess.getMessageMoveRule() ||
				   context == grammarAccess.getMoveRule() ||
				   context == grammarAccess.getRaiseEventRule()) {
					sequence_RaiseEvent(context, (RaiseEvent) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.REACTION:
				if(context == grammarAccess.getReactionRule()) {
					sequence_Reaction(context, (Reaction) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.RECEIVE_MSG:
				if(context == grammarAccess.getMessageMoveRule() ||
				   context == grammarAccess.getMoveRule() ||
				   context == grammarAccess.getReceiveMsgRule()) {
					sequence_ReceiveMsg(context, (ReceiveMsg) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.REMOVE_RULE:
				if(context == grammarAccess.getGuardMoveRule() ||
				   context == grammarAccess.getMoveRule() ||
				   context == grammarAccess.getRemoveRuleRule()) {
					sequence_RemoveRule(context, (RemoveRule) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.REPEAT_PLAN:
				if(context == grammarAccess.getMoveRule() ||
				   context == grammarAccess.getPlanMoveRule() ||
				   context == grammarAccess.getRepeatPlanRule()) {
					sequence_RepeatPlan(context, (RepeatPlan) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.REPLY_TO_CALLER:
				if(context == grammarAccess.getMessageMoveRule() ||
				   context == grammarAccess.getMoveRule() ||
				   context == grammarAccess.getReplyToCallerRule()) {
					sequence_ReplyToCaller(context, (ReplyToCaller) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.REQUEST:
				if(context == grammarAccess.getMessageRule() ||
				   context == grammarAccess.getOutInMessageRule() ||
				   context == grammarAccess.getRequestRule()) {
					sequence_Request(context, (Request) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.RESUME_PLAN:
				if(context == grammarAccess.getMoveRule() ||
				   context == grammarAccess.getPlanMoveRule() ||
				   context == grammarAccess.getResumePlanRule()) {
					sequence_ResumePlan(context, (ResumePlan) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.ROBOT:
				if(context == grammarAccess.getRobotRule()) {
					sequence_Robot(context, (Robot) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.RULE:
				if(context == grammarAccess.getRuleRule()) {
					sequence_Rule(context, (Rule) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.RUN_PLAN:
				if(context == grammarAccess.getMoveRule() ||
				   context == grammarAccess.getPlanMoveRule() ||
				   context == grammarAccess.getRunPlanRule()) {
					sequence_RunPlan(context, (RunPlan) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.SEND_DISPATCH:
				if(context == grammarAccess.getMessageMoveRule() ||
				   context == grammarAccess.getMoveRule() ||
				   context == grammarAccess.getSendDispatchRule()) {
					sequence_SendDispatch(context, (SendDispatch) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.SEND_EVENT_AS_DISPATCH:
				if(context == grammarAccess.getEventHandlerOperationRule() ||
				   context == grammarAccess.getSendEventAsDispatchRule()) {
					sequence_SendEventAsDispatch(context, (SendEventAsDispatch) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.SEND_REQUEST:
				if(context == grammarAccess.getMessageMoveRule() ||
				   context == grammarAccess.getMoveRule() ||
				   context == grammarAccess.getSendRequestRule()) {
					sequence_SendRequest(context, (SendRequest) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.SENSE_EVENT:
				if(context == grammarAccess.getMessageMoveRule() ||
				   context == grammarAccess.getMoveRule() ||
				   context == grammarAccess.getSenseEventRule()) {
					sequence_SenseEvent(context, (SenseEvent) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.SIGNAL:
				if(context == grammarAccess.getMessageRule() ||
				   context == grammarAccess.getOutOnlyMessageRule() ||
				   context == grammarAccess.getSignalRule()) {
					sequence_Signal(context, (Signal) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.SOLVE_GOAL:
				if(context == grammarAccess.getActionMoveRule() ||
				   context == grammarAccess.getMoveRule() ||
				   context == grammarAccess.getSolveGoalRule()) {
					sequence_SolveGoal(context, (SolveGoal) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.SOLVE_OPERATION:
				if(context == grammarAccess.getEventHandlerOperationRule() ||
				   context == grammarAccess.getSolveOperationRule()) {
					sequence_SolveOperation(context, (SolveOperation) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.SOUND:
				if(context == grammarAccess.getExtensionMoveRule() ||
				   context == grammarAccess.getMoveRule() ||
				   context == grammarAccess.getSoundRule()) {
					sequence_Sound(context, (Sound) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.STRING_DATA:
				if(context == grammarAccess.getDataRule() ||
				   context == grammarAccess.getStringDataRule()) {
					sequence_StringData(context, (StringData) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.SUSPEND_PLAN:
				if(context == grammarAccess.getMoveRule() ||
				   context == grammarAccess.getPlanMoveRule() ||
				   context == grammarAccess.getSuspendPlanRule()) {
					sequence_SuspendPlan(context, (SuspendPlan) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.SWITCH_PLAN:
				if(context == grammarAccess.getMoveRule() ||
				   context == grammarAccess.getPlanMoveRule() ||
				   context == grammarAccess.getSwitchPlanRule()) {
					sequence_SwitchPlan(context, (SwitchPlan) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.TIME_LIMIT:
				if(context == grammarAccess.getTimeLimitRule()) {
					sequence_TimeLimit(context, (TimeLimit) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.TOKEN:
				if(context == grammarAccess.getMessageRule() ||
				   context == grammarAccess.getOutOnlyMessageRule() ||
				   context == grammarAccess.getTokenRule()) {
					sequence_Token(context, (Token) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.VAR_OR_ATOM_OR_PSTRUCT:
				if(context == grammarAccess.getVarOrAtomOrPStructRule()) {
					sequence_VarOrAtomOrPStruct(context, (VarOrAtomOrPStruct) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.VAR_OR_ATOMIC:
				if(context == grammarAccess.getVarOrAtomicRule()) {
					sequence_VarOrAtomic(context, (VarOrAtomic) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.VAR_OR_PSTRUCT:
				if(context == grammarAccess.getVarOrPStructRule()) {
					sequence_VarOrPStruct(context, (VarOrPStruct) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.VAR_OR_PHEAD:
				if(context == grammarAccess.getVarOrPheadRule()) {
					sequence_VarOrPhead(context, (VarOrPhead) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.VAR_OR_QACTOR:
				if(context == grammarAccess.getVarOrQactorRule()) {
					sequence_VarOrQactor(context, (VarOrQactor) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.VAR_OR_STRING:
				if(context == grammarAccess.getVarOrStringRule()) {
					sequence_VarOrString(context, (VarOrString) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.VARIABLE:
				if(context == grammarAccess.getPAtomRule() ||
				   context == grammarAccess.getPHeadRule() ||
				   context == grammarAccess.getPTermRule() ||
				   context == grammarAccess.getVariableRule()) {
					sequence_Variable(context, (Variable) semanticObject); 
					return; 
				}
				else break;
			case QactorPackage.VIDEO:
				if(context == grammarAccess.getExtensionMoveRule() ||
				   context == grammarAccess.getMoveRule() ||
				   context == grammarAccess.getVideoRule()) {
					sequence_Video(context, (Video) semanticObject); 
					return; 
				}
				else break;
			}
		if (errorAcceptor != null) errorAcceptor.accept(diagnosticProvider.createInvalidContextOrTypeDiagnostic(semanticObject, context));
	}
	
	/**
	 * Constraint:
	 *     (name=ID undoable?='undoable'? msec=INT arg=PStruct?)
	 */
	protected void sequence_Action(EObject context, Action semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name='actorOp' goal=PHead plan=[Plan|ID]?)
	 */
	protected void sequence_ActorOp(EObject context, ActorOp semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name='addRule' rule=PHead)
	 */
	protected void sequence_AddRule(EObject context, AddRule semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.GUARD_MOVE__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.GUARD_MOVE__NAME));
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.GUARD_MOVE__RULE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.GUARD_MOVE__RULE));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getAddRuleAccess().getNameAddRuleKeyword_0_0(), semanticObject.getName());
		feeder.accept(grammarAccess.getAddRuleAccess().getRulePHeadParserRuleCall_1_0(), semanticObject.getRule());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     name=ID
	 */
	protected void sequence_AnswerEvent(EObject context, AnswerEvent semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.ANSWER_EVENT__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.ANSWER_EVENT__NAME));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getAnswerEventAccess().getNameIDTerminalRuleCall_1_0(), semanticObject.getName());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     name='dummyRobotMove'
	 */
	protected void sequence_BasicRobotMove(EObject context, BasicRobotMove semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.BASIC_ROBOT_MOVE__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.BASIC_ROBOT_MOVE__NAME));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getBasicRobotMoveAccess().getNameDummyRobotMoveKeyword_0(), semanticObject.getName());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (host=STRING port=INT)
	 */
	protected void sequence_ComponentIP(EObject context, ComponentIP semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.COMPONENT_IP__HOST) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.COMPONENT_IP__HOST));
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.COMPONENT_IP__PORT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.COMPONENT_IP__PORT));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getComponentIPAccess().getHostSTRINGTerminalRuleCall_2_0(), semanticObject.getHost());
		feeder.accept(grammarAccess.getComponentIPAccess().getPortINTTerminalRuleCall_4_0(), semanticObject.getPort());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (
	 *         name=ID 
	 *         ip=ComponentIP 
	 *         (env?='-g' color=WindowColor)? 
	 *         standalone?='-standalone '? 
	 *         httpserver?='-httpserver'? 
	 *         handler+=EventHandler*
	 *     )
	 */
	protected void sequence_Context(EObject context, Context semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (plan=[Plan|ID] | nane='continue')
	 */
	protected void sequence_Continuation(EObject context, Continuation semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name='when' evOccur=[Event|ID])
	 */
	protected void sequence_ContinueEvent(EObject context, ContinueEvent semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.ALARM_EVENT__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.ALARM_EVENT__NAME));
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.CONTINUE_EVENT__EV_OCCUR) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.CONTINUE_EVENT__EV_OCCUR));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getContinueEventAccess().getNameWhenKeyword_0_0(), semanticObject.getName());
		feeder.accept(grammarAccess.getContinueEventAccess().getEvOccurEventIDTerminalRuleCall_1_0_1(), semanticObject.getEvOccur());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (name='delay' duration=TimeLimit)
	 */
	protected void sequence_Delay(EObject context, Delay semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.EXTENSION_MOVE__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.EXTENSION_MOVE__NAME));
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.EXTENSION_MOVE__DURATION) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.EXTENSION_MOVE__DURATION));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getDelayAccess().getNameDelayKeyword_0_0(), semanticObject.getName());
		feeder.accept(grammarAccess.getDelayAccess().getDurationTimeLimitParserRuleCall_1_0(), semanticObject.getDuration());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (name='demo' goal=PHead plan=[Plan|ID]?)
	 */
	protected void sequence_Demo(EObject context, Demo semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=ID msg=PHead)
	 */
	protected void sequence_Dispatch(EObject context, Dispatch semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.MESSAGE__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.MESSAGE__NAME));
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.MESSAGE__MSG) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.MESSAGE__MSG));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getDispatchAccess().getNameIDTerminalRuleCall_1_0(), semanticObject.getName());
		feeder.accept(grammarAccess.getDispatchAccess().getMsgPHeadParserRuleCall_3_0(), semanticObject.getMsg());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (name='endQActor' msg=STRING)
	 */
	protected void sequence_EndActor(EObject context, EndActor semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.PLAN_MOVE__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.PLAN_MOVE__NAME));
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.END_ACTOR__MSG) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.END_ACTOR__MSG));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getEndActorAccess().getNameEndQActorKeyword_0_0(), semanticObject.getName());
		feeder.accept(grammarAccess.getEndActorAccess().getMsgSTRINGTerminalRuleCall_1_0(), semanticObject.getMsg());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (name='endPlan' msg=STRING)
	 */
	protected void sequence_EndPlan(EObject context, EndPlan semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.PLAN_MOVE__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.PLAN_MOVE__NAME));
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.END_PLAN__MSG) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.END_PLAN__MSG));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getEndPlanAccess().getNameEndPlanKeyword_0_0(), semanticObject.getName());
		feeder.accept(grammarAccess.getEndPlanAccess().getMsgSTRINGTerminalRuleCall_1_0(), semanticObject.getMsg());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (op+=EventHandlerOperation op+=EventHandlerOperation*)
	 */
	protected void sequence_EventHandlerBody(EObject context, EventHandlerBody semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=ID (events+=[Event|ID] events+=[Event|ID]*)? print?='-print'? body=EventHandlerBody?)
	 */
	protected void sequence_EventHandler(EObject context, EventHandler semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (event=[Event|ID] msg=PHead move=Move)
	 */
	protected void sequence_EventSwitch(EObject context, EventSwitch semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.EVENT_SWITCH__EVENT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.EVENT_SWITCH__EVENT));
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.EVENT_SWITCH__MSG) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.EVENT_SWITCH__MSG));
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.EVENT_SWITCH__MOVE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.EVENT_SWITCH__MOVE));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getEventSwitchAccess().getEventEventIDTerminalRuleCall_1_0_1(), semanticObject.getEvent());
		feeder.accept(grammarAccess.getEventSwitchAccess().getMsgPHeadParserRuleCall_3_0(), semanticObject.getMsg());
		feeder.accept(grammarAccess.getEventSwitchAccess().getMoveMoveParserRuleCall_5_0(), semanticObject.getMove());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (name=ID msg=PHead)
	 */
	protected void sequence_Event(EObject context, Event semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.MESSAGE__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.MESSAGE__NAME));
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.MESSAGE__MSG) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.MESSAGE__MSG));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getEventAccess().getNameIDTerminalRuleCall_1_0(), semanticObject.getName());
		feeder.accept(grammarAccess.getEventAccess().getMsgPHeadParserRuleCall_3_0(), semanticObject.getMsg());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     ((action=[Action|ID] arg=PHead?) | sentence=PHead)
	 */
	protected void sequence_ExecuteAction(EObject context, ExecuteAction semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name='getActivationEvent ' var=Variable)
	 */
	protected void sequence_GetActivationEvent(EObject context, GetActivationEvent semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.PLAN_MOVE__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.PLAN_MOVE__NAME));
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.GET_ACTIVATION_EVENT__VAR) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.GET_ACTIVATION_EVENT__VAR));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getGetActivationEventAccess().getNameGetActivationEventKeyword_0_0(), semanticObject.getName());
		feeder.accept(grammarAccess.getGetActivationEventAccess().getVarVariableParserRuleCall_1_0(), semanticObject.getVar());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (name='getSensedEvent ' var=Variable)
	 */
	protected void sequence_GetSensedEvent(EObject context, GetSensedEvent semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.PLAN_MOVE__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.PLAN_MOVE__NAME));
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.GET_SENSED_EVENT__VAR) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.GET_SENSED_EVENT__VAR));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getGetSensedEventAccess().getNameGetSensedEventKeyword_0_0(), semanticObject.getName());
		feeder.accept(grammarAccess.getGetSensedEventAccess().getVarVariableParserRuleCall_1_0(), semanticObject.getVar());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (name='??' pred=PTerm)
	 */
	protected void sequence_GuardPredicateRemovable(EObject context, GuardPredicateRemovable semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.GUARD_PREDICATE__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.GUARD_PREDICATE__NAME));
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.GUARD_PREDICATE__PRED) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.GUARD_PREDICATE__PRED));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getGuardPredicateRemovableAccess().getNameQuestionMarkQuestionMarkKeyword_0_0(), semanticObject.getName());
		feeder.accept(grammarAccess.getGuardPredicateRemovableAccess().getPredPTermParserRuleCall_1_0(), semanticObject.getPred());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (name='!?' pred=PTerm)
	 */
	protected void sequence_GuardPredicateStable(EObject context, GuardPredicateStable semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.GUARD_PREDICATE__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.GUARD_PREDICATE__NAME));
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.GUARD_PREDICATE__PRED) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.GUARD_PREDICATE__PRED));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getGuardPredicateStableAccess().getNameExclamationMarkQuestionMarkKeyword_0_0(), semanticObject.getName());
		feeder.accept(grammarAccess.getGuardPredicateStableAccess().getPredPTermParserRuleCall_1_0(), semanticObject.getPred());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (name='[' not?='not'? guardspec=GuardPredicate)
	 */
	protected void sequence_Guard(EObject context, Guard semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=ID value=INT)
	 */
	protected void sequence_IntegerData(EObject context, IntegerData semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.DATA__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.DATA__NAME));
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.INTEGER_DATA__VALUE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.INTEGER_DATA__VALUE));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getIntegerDataAccess().getNameIDTerminalRuleCall_1_0(), semanticObject.getName());
		feeder.accept(grammarAccess.getIntegerDataAccess().getValueINTTerminalRuleCall_3_0(), semanticObject.getValue());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (name=ID msg=PHead)
	 */
	protected void sequence_Invitation(EObject context, Invitation semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.MESSAGE__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.MESSAGE__NAME));
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.MESSAGE__MSG) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.MESSAGE__MSG));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getInvitationAccess().getNameIDTerminalRuleCall_1_0(), semanticObject.getName());
		feeder.accept(grammarAccess.getInvitationAccess().getMsgPHeadParserRuleCall_3_0(), semanticObject.getMsg());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (name='loadPlan' fname=VarOrString)
	 */
	protected void sequence_LoadPlan(EObject context, LoadPlan semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.PLAN_MOVE__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.PLAN_MOVE__NAME));
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.LOAD_PLAN__FNAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.LOAD_PLAN__FNAME));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getLoadPlanAccess().getNameLoadPlanKeyword_0_0(), semanticObject.getName());
		feeder.accept(grammarAccess.getLoadPlanAccess().getFnameVarOrStringParserRuleCall_1_0(), semanticObject.getFname());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (name='memoCurrentEvent' lastonly?='-lastonly'?)
	 */
	protected void sequence_MemoCurrentEvent(EObject context, MemoCurrentEvent semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name='memoCurrentMessage' lastonly?='-lastonly'?)
	 */
	protected void sequence_MemoCurrentMessage(EObject context, MemoCurrentMessage semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     name='currentEvent'
	 */
	protected void sequence_MemoEvent(EObject context, MemoEvent semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.MEMO_EVENT__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.MEMO_EVENT__NAME));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getMemoEventAccess().getNameCurrentEventKeyword_0(), semanticObject.getName());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     ((rule=MemoRule actor=[QActor|ID]) | (doMemo=MemoCurrentEvent actor=[QActor|ID]))
	 */
	protected void sequence_MemoOperation(EObject context, MemoOperation semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name='file' fname=VarOrString)
	 */
	protected void sequence_MoveFile(EObject context, MoveFile semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.MOVE_FILE__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.MOVE_FILE__NAME));
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.MOVE_FILE__FNAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.MOVE_FILE__FNAME));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getMoveFileAccess().getNameFileKeyword_0_0(), semanticObject.getName());
		feeder.accept(grammarAccess.getMoveFileAccess().getFnameVarOrStringParserRuleCall_2_0(), semanticObject.getFname());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (
	 *         name='receiveAndSwitch' 
	 *         duration=TimeLimit 
	 *         messages+=[Message|ID] 
	 *         messages+=[Message|ID]* 
	 *         plans+=[Plan|ID] 
	 *         plans+=[Plan|ID]*
	 *     )
	 */
	protected void sequence_MsgSelect(EObject context, MsgSelect semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (msg=[Message|ID] sender=VarOrAtomic content=PHead)
	 */
	protected void sequence_MsgSpec(EObject context, MsgSpec semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.MSG_SPEC__MSG) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.MSG_SPEC__MSG));
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.MSG_SPEC__SENDER) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.MSG_SPEC__SENDER));
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.MSG_SPEC__CONTENT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.MSG_SPEC__CONTENT));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getMsgSpecAccess().getMsgMessageIDTerminalRuleCall_1_0_1(), semanticObject.getMsg());
		feeder.accept(grammarAccess.getMsgSpecAccess().getSenderVarOrAtomicParserRuleCall_3_0(), semanticObject.getSender());
		feeder.accept(grammarAccess.getMsgSpecAccess().getContentPHeadParserRuleCall_5_0(), semanticObject.getContent());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (message=[Message|ID] msg=PHead move=Move)
	 */
	protected void sequence_MsgSwitch(EObject context, MsgSwitch semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.MSG_SWITCH__MESSAGE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.MSG_SWITCH__MESSAGE));
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.MSG_SWITCH__MSG) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.MSG_SWITCH__MSG));
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.MSG_SWITCH__MOVE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.MSG_SWITCH__MOVE));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getMsgSwitchAccess().getMessageMessageIDTerminalRuleCall_1_0_1(), semanticObject.getMessage());
		feeder.accept(grammarAccess.getMsgSwitchAccess().getMsgPHeadParserRuleCall_3_0(), semanticObject.getMsg());
		feeder.accept(grammarAccess.getMsgSwitchAccess().getMoveMoveParserRuleCall_5_0(), semanticObject.getMove());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (name='event' ev=[Event|ID] planRef=[Plan|ID])
	 */
	protected void sequence_NormalEvent(EObject context, NormalEvent semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.ALARM_EVENT__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.ALARM_EVENT__NAME));
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.NORMAL_EVENT__EV) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.NORMAL_EVENT__EV));
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.NORMAL_EVENT__PLAN_REF) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.NORMAL_EVENT__PLAN_REF));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getNormalEventAccess().getNameEventKeyword_0_0(), semanticObject.getName());
		feeder.accept(grammarAccess.getNormalEventAccess().getEvEventIDTerminalRuleCall_1_0_1(), semanticObject.getEv());
		feeder.accept(grammarAccess.getNormalEventAccess().getPlanRefPlanIDTerminalRuleCall_3_0_1(), semanticObject.getPlanRef());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (
	 *         name='receiveTheMsg' 
	 *         msgid=PHead 
	 *         msgtype=PHead 
	 *         msgsender=PHead 
	 *         msgreceiver=PHead 
	 *         msgcontent=PHead 
	 *         msgseqnum=PHead 
	 *         duration=TimeLimit
	 *     )
	 */
	protected void sequence_OnReceiveMsg(EObject context, OnReceiveMsg semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.ON_RECEIVE_MSG__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.ON_RECEIVE_MSG__NAME));
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.ON_RECEIVE_MSG__MSGID) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.ON_RECEIVE_MSG__MSGID));
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.ON_RECEIVE_MSG__MSGTYPE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.ON_RECEIVE_MSG__MSGTYPE));
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.ON_RECEIVE_MSG__MSGSENDER) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.ON_RECEIVE_MSG__MSGSENDER));
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.ON_RECEIVE_MSG__MSGRECEIVER) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.ON_RECEIVE_MSG__MSGRECEIVER));
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.ON_RECEIVE_MSG__MSGCONTENT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.ON_RECEIVE_MSG__MSGCONTENT));
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.ON_RECEIVE_MSG__MSGSEQNUM) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.ON_RECEIVE_MSG__MSGSEQNUM));
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.ON_RECEIVE_MSG__DURATION) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.ON_RECEIVE_MSG__DURATION));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getOnReceiveMsgAccess().getNameReceiveTheMsgKeyword_0_0(), semanticObject.getName());
		feeder.accept(grammarAccess.getOnReceiveMsgAccess().getMsgidPHeadParserRuleCall_3_0(), semanticObject.getMsgid());
		feeder.accept(grammarAccess.getOnReceiveMsgAccess().getMsgtypePHeadParserRuleCall_5_0(), semanticObject.getMsgtype());
		feeder.accept(grammarAccess.getOnReceiveMsgAccess().getMsgsenderPHeadParserRuleCall_7_0(), semanticObject.getMsgsender());
		feeder.accept(grammarAccess.getOnReceiveMsgAccess().getMsgreceiverPHeadParserRuleCall_9_0(), semanticObject.getMsgreceiver());
		feeder.accept(grammarAccess.getOnReceiveMsgAccess().getMsgcontentPHeadParserRuleCall_11_0(), semanticObject.getMsgcontent());
		feeder.accept(grammarAccess.getOnReceiveMsgAccess().getMsgseqnumPHeadParserRuleCall_13_0(), semanticObject.getMsgseqnum());
		feeder.accept(grammarAccess.getOnReceiveMsgAccess().getDurationTimeLimitParserRuleCall_15_0(), semanticObject.getDuration());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     body=PStruct
	 */
	protected void sequence_PActorCall(EObject context, PActorCall semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.PACTOR_CALL__BODY) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.PACTOR_CALL__BODY));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getPActorCallAccess().getBodyPStructParserRuleCall_2_0(), semanticObject.getBody());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     name='!'
	 */
	protected void sequence_PAtomCut(EObject context, PAtomCut semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.PATOM_CUT__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.PATOM_CUT__NAME));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getPAtomCutAccess().getNameExclamationMarkKeyword_0(), semanticObject.getName());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     val=INT
	 */
	protected void sequence_PAtomNum(EObject context, PAtomNum semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.PATOM_NUM__VAL) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.PATOM_NUM__VAL));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getPAtomNumAccess().getValINTTerminalRuleCall_0(), semanticObject.getVal());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     val=STRING
	 */
	protected void sequence_PAtomString(EObject context, PAtomString semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.PATOM_STRING__VAL) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.PATOM_STRING__VAL));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getPAtomStringAccess().getValSTRINGTerminalRuleCall_0(), semanticObject.getVal());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     val=ID
	 */
	protected void sequence_PAtomic(EObject context, PAtomic semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.PATOMIC__VAL) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.PATOMIC__VAL));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getPAtomicAccess().getValIDTerminalRuleCall_0(), semanticObject.getVal());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (varout=Variable varin=Variable num=PAtomNum)
	 */
	protected void sequence_PIs(EObject context, PIs semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.PIS__VAROUT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.PIS__VAROUT));
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.PIS__VARIN) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.PIS__VARIN));
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.PIS__NUM) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.PIS__NUM));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getPIsAccess().getVaroutVariableParserRuleCall_0_0(), semanticObject.getVarout());
		feeder.accept(grammarAccess.getPIsAccess().getVarinVariableParserRuleCall_2_0(), semanticObject.getVarin());
		feeder.accept(grammarAccess.getPIsAccess().getNumPAtomNumParserRuleCall_4_0(), semanticObject.getNum());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (name=ID msgArg+=PTerm? msgArg+=PTerm*)
	 */
	protected void sequence_PStruct(EObject context, PStruct semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name='photo' duration=TimeLimit destfile=MoveFile answerEvent=AnswerEvent?)
	 */
	protected void sequence_Photo(EObject context, Photo semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (guard=Guard? move=Move react=Reaction? (elsemove=Move elsereact=Reaction?)?)
	 */
	protected void sequence_PlanAction(EObject context, PlanAction semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=ID normal?='normal'? resume?='resumeLastPlan'? action+=PlanAction action+=PlanAction*)
	 */
	protected void sequence_Plan(EObject context, Plan semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name='printCurrentEvent' memo?='-memo'?)
	 */
	protected void sequence_PrintCurrentEvent(EObject context, PrintCurrentEvent semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name='printCurrentMessage' memo?='-memo'?)
	 */
	protected void sequence_PrintCurrentMessage(EObject context, PrintCurrentMessage semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name='println' args=PHead)
	 */
	protected void sequence_Print(EObject context, Print semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.BASIC_MOVE__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.BASIC_MOVE__NAME));
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.PRINT__ARGS) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.PRINT__ARGS));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getPrintAccess().getNamePrintlnKeyword_0_0(), semanticObject.getName());
		feeder.accept(grammarAccess.getPrintAccess().getArgsPHeadParserRuleCall_2_0(), semanticObject.getArgs());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (
	 *         name=ID 
	 *         testing?='-testing'? 
	 *         message+=Message* 
	 *         context+=Context* 
	 *         actor+=QActor* 
	 *         robot=Robot?
	 *     )
	 */
	protected void sequence_QActorSystemSpec(EObject context, QActorSystemSpec semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     spec=QActorSystemSpec
	 */
	protected void sequence_QActorSystem(EObject context, QActorSystem semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.QACTOR_SYSTEM__SPEC) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.QACTOR_SYSTEM__SPEC));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getQActorSystemAccess().getSpecQActorSystemSpecParserRuleCall_1_0(), semanticObject.getSpec());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (
	 *         name=ID 
	 *         context=[Context|ID] 
	 *         (env?='-g' color=WindowColor)? 
	 *         rules+=Rule* 
	 *         data+=Data* 
	 *         action+=Action* 
	 *         plans+=Plan*
	 *     )
	 */
	protected void sequence_QActor(EObject context, QActor semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name='emit' ev=[Event|ID] content=PHead)
	 */
	protected void sequence_RaiseEvent(EObject context, RaiseEvent semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.RAISE_EVENT__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.RAISE_EVENT__NAME));
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.RAISE_EVENT__EV) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.RAISE_EVENT__EV));
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.RAISE_EVENT__CONTENT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.RAISE_EVENT__CONTENT));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getRaiseEventAccess().getNameEmitKeyword_0_0(), semanticObject.getName());
		feeder.accept(grammarAccess.getRaiseEventAccess().getEvEventIDTerminalRuleCall_1_0_1(), semanticObject.getEv());
		feeder.accept(grammarAccess.getRaiseEventAccess().getContentPHeadParserRuleCall_3_0(), semanticObject.getContent());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (name='react' alarms+=AlarmEvent alarms+=AlarmEvent*)
	 */
	protected void sequence_Reaction(EObject context, Reaction semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name='receiveMsg' duration=TimeLimit spec=MsgSpec?)
	 */
	protected void sequence_ReceiveMsg(EObject context, ReceiveMsg semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name='removeRule' rule=PHead)
	 */
	protected void sequence_RemoveRule(EObject context, RemoveRule semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.GUARD_MOVE__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.GUARD_MOVE__NAME));
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.GUARD_MOVE__RULE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.GUARD_MOVE__RULE));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getRemoveRuleAccess().getNameRemoveRuleKeyword_0_0(), semanticObject.getName());
		feeder.accept(grammarAccess.getRemoveRuleAccess().getRulePHeadParserRuleCall_1_0(), semanticObject.getRule());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (name='repeatPlan' niter=INT?)
	 */
	protected void sequence_RepeatPlan(EObject context, RepeatPlan semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name='replyToCaller' msgref=[Message|ID] val=PHead)
	 */
	protected void sequence_ReplyToCaller(EObject context, ReplyToCaller semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.REPLY_TO_CALLER__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.REPLY_TO_CALLER__NAME));
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.REPLY_TO_CALLER__MSGREF) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.REPLY_TO_CALLER__MSGREF));
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.REPLY_TO_CALLER__VAL) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.REPLY_TO_CALLER__VAL));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getReplyToCallerAccess().getNameReplyToCallerKeyword_0_0(), semanticObject.getName());
		feeder.accept(grammarAccess.getReplyToCallerAccess().getMsgrefMessageIDTerminalRuleCall_2_0_1(), semanticObject.getMsgref());
		feeder.accept(grammarAccess.getReplyToCallerAccess().getValPHeadParserRuleCall_4_0(), semanticObject.getVal());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (name=ID msg=PHead)
	 */
	protected void sequence_Request(EObject context, Request semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.MESSAGE__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.MESSAGE__NAME));
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.MESSAGE__MSG) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.MESSAGE__MSG));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getRequestAccess().getNameIDTerminalRuleCall_1_0(), semanticObject.getName());
		feeder.accept(grammarAccess.getRequestAccess().getMsgPHeadParserRuleCall_3_0(), semanticObject.getMsg());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     name='resumeLastPlan'
	 */
	protected void sequence_ResumePlan(EObject context, ResumePlan semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.PLAN_MOVE__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.PLAN_MOVE__NAME));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getResumePlanAccess().getNameResumeLastPlanKeyword_0(), semanticObject.getName());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (name=ID actor=QActor)
	 */
	protected void sequence_Robot(EObject context, Robot semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.ROBOT__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.ROBOT__NAME));
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.ROBOT__ACTOR) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.ROBOT__ACTOR));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getRobotAccess().getNameIDTerminalRuleCall_0_0(), semanticObject.getName());
		feeder.accept(grammarAccess.getRobotAccess().getActorQActorParserRuleCall_1_0(), semanticObject.getActor());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (head=PHead (body+=PTerm body+=PTerm*)?)
	 */
	protected void sequence_Rule(EObject context, Rule semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name='runPlan' plainid=VarOrAtomic)
	 */
	protected void sequence_RunPlan(EObject context, RunPlan semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.PLAN_MOVE__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.PLAN_MOVE__NAME));
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.RUN_PLAN__PLAINID) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.RUN_PLAN__PLAINID));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getRunPlanAccess().getNameRunPlanKeyword_0_0(), semanticObject.getName());
		feeder.accept(grammarAccess.getRunPlanAccess().getPlainidVarOrAtomicParserRuleCall_1_0(), semanticObject.getPlainid());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (name='forward' dest=VarOrQactor msgref=[Message|ID] val=PHead)
	 */
	protected void sequence_SendDispatch(EObject context, SendDispatch semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.SEND_DISPATCH__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.SEND_DISPATCH__NAME));
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.SEND_DISPATCH__DEST) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.SEND_DISPATCH__DEST));
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.SEND_DISPATCH__MSGREF) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.SEND_DISPATCH__MSGREF));
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.SEND_DISPATCH__VAL) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.SEND_DISPATCH__VAL));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getSendDispatchAccess().getNameForwardKeyword_0_0(), semanticObject.getName());
		feeder.accept(grammarAccess.getSendDispatchAccess().getDestVarOrQactorParserRuleCall_1_0(), semanticObject.getDest());
		feeder.accept(grammarAccess.getSendDispatchAccess().getMsgrefMessageIDTerminalRuleCall_3_0_1(), semanticObject.getMsgref());
		feeder.accept(grammarAccess.getSendDispatchAccess().getValPHeadParserRuleCall_5_0(), semanticObject.getVal());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (actor=[QActor|ID] msgref=[Message|ID])
	 */
	protected void sequence_SendEventAsDispatch(EObject context, SendEventAsDispatch semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.SEND_EVENT_AS_DISPATCH__ACTOR) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.SEND_EVENT_AS_DISPATCH__ACTOR));
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.SEND_EVENT_AS_DISPATCH__MSGREF) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.SEND_EVENT_AS_DISPATCH__MSGREF));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getSendEventAsDispatchAccess().getActorQActorIDTerminalRuleCall_1_0_1(), semanticObject.getActor());
		feeder.accept(grammarAccess.getSendEventAsDispatchAccess().getMsgrefMessageIDTerminalRuleCall_3_0_1(), semanticObject.getMsgref());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (name='demand' dest=VarOrQactor msgref=[Message|ID] val=PHead)
	 */
	protected void sequence_SendRequest(EObject context, SendRequest semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.SEND_REQUEST__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.SEND_REQUEST__NAME));
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.SEND_REQUEST__DEST) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.SEND_REQUEST__DEST));
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.SEND_REQUEST__MSGREF) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.SEND_REQUEST__MSGREF));
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.SEND_REQUEST__VAL) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.SEND_REQUEST__VAL));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getSendRequestAccess().getNameDemandKeyword_0_0(), semanticObject.getName());
		feeder.accept(grammarAccess.getSendRequestAccess().getDestVarOrQactorParserRuleCall_1_0(), semanticObject.getDest());
		feeder.accept(grammarAccess.getSendRequestAccess().getMsgrefMessageIDTerminalRuleCall_3_0_1(), semanticObject.getMsgref());
		feeder.accept(grammarAccess.getSendRequestAccess().getValPHeadParserRuleCall_5_0(), semanticObject.getVal());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (duration=TimeLimit events+=[Event|ID] events+=[Event|ID]* plans+=Continuation plans+=Continuation*)
	 */
	protected void sequence_SenseEvent(EObject context, SenseEvent semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=ID msg=PHead)
	 */
	protected void sequence_Signal(EObject context, Signal semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.MESSAGE__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.MESSAGE__NAME));
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.MESSAGE__MSG) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.MESSAGE__MSG));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getSignalAccess().getNameIDTerminalRuleCall_1_0(), semanticObject.getName());
		feeder.accept(grammarAccess.getSignalAccess().getMsgPHeadParserRuleCall_3_0(), semanticObject.getMsg());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (name='solve' goal=PHead duration=TimeLimit plan=[Plan|ID]?)
	 */
	protected void sequence_SolveGoal(EObject context, SolveGoal semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (goal=PTerm actor=[QActor|ID])
	 */
	protected void sequence_SolveOperation(EObject context, SolveOperation semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.SOLVE_OPERATION__GOAL) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.SOLVE_OPERATION__GOAL));
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.SOLVE_OPERATION__ACTOR) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.SOLVE_OPERATION__ACTOR));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getSolveOperationAccess().getGoalPTermParserRuleCall_1_0(), semanticObject.getGoal());
		feeder.accept(grammarAccess.getSolveOperationAccess().getActorQActorIDTerminalRuleCall_3_0_1(), semanticObject.getActor());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (name='sound' duration=TimeLimit srcfile=MoveFile answerEvent=AnswerEvent?)
	 */
	protected void sequence_Sound(EObject context, Sound semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=ID value=STRING)
	 */
	protected void sequence_StringData(EObject context, StringData semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.DATA__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.DATA__NAME));
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.STRING_DATA__VALUE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.STRING_DATA__VALUE));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getStringDataAccess().getNameIDTerminalRuleCall_1_0(), semanticObject.getName());
		feeder.accept(grammarAccess.getStringDataAccess().getValueSTRINGTerminalRuleCall_3_0(), semanticObject.getValue());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     name='suspendLastPlan'
	 */
	protected void sequence_SuspendPlan(EObject context, SuspendPlan semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.PLAN_MOVE__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.PLAN_MOVE__NAME));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getSuspendPlanAccess().getNameSuspendLastPlanKeyword_0(), semanticObject.getName());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (name='switchToPlan' plan=[Plan|ID])
	 */
	protected void sequence_SwitchPlan(EObject context, SwitchPlan semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.PLAN_MOVE__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.PLAN_MOVE__NAME));
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.SWITCH_PLAN__PLAN) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.SWITCH_PLAN__PLAN));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getSwitchPlanAccess().getNameSwitchToPlanKeyword_0_0(), semanticObject.getName());
		feeder.accept(grammarAccess.getSwitchPlanAccess().getPlanPlanIDTerminalRuleCall_1_0_1(), semanticObject.getPlan());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (name='time' (msec=INT | var=Variable))
	 */
	protected void sequence_TimeLimit(EObject context, TimeLimit semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=ID msg=PHead)
	 */
	protected void sequence_Token(EObject context, Token semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.MESSAGE__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.MESSAGE__NAME));
			if(transientValues.isValueTransient(semanticObject, QactorPackage.Literals.MESSAGE__MSG) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QactorPackage.Literals.MESSAGE__MSG));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getTokenAccess().getNameIDTerminalRuleCall_1_0(), semanticObject.getName());
		feeder.accept(grammarAccess.getTokenAccess().getMsgPHeadParserRuleCall_3_0(), semanticObject.getMsg());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (var=Variable | psrtuct=PStruct | atom=PAtomic)
	 */
	protected void sequence_VarOrAtomOrPStruct(EObject context, VarOrAtomOrPStruct semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (var=Variable | const=PAtomic)
	 */
	protected void sequence_VarOrAtomic(EObject context, VarOrAtomic semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (var=Variable | psrtuct=PStruct)
	 */
	protected void sequence_VarOrPStruct(EObject context, VarOrPStruct semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (var=Variable | phead=PHead)
	 */
	protected void sequence_VarOrPhead(EObject context, VarOrPhead semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (var=Variable | dest=[QActor|ID])
	 */
	protected void sequence_VarOrQactor(EObject context, VarOrQactor semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (var=Variable | const=STRING)
	 */
	protected void sequence_VarOrString(EObject context, VarOrString semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (guardvar?='$'? name=VARID)
	 */
	protected void sequence_Variable(EObject context, Variable semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name='video' duration=TimeLimit destfile=MoveFile answerEvent=AnswerEvent?)
	 */
	protected void sequence_Video(EObject context, Video semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
}
