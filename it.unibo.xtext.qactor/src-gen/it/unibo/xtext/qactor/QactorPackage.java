/**
 */
package it.unibo.xtext.qactor;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see it.unibo.xtext.qactor.QactorFactory
 * @model kind="package"
 * @generated
 */
public interface QactorPackage extends EPackage
{
  /**
   * The package name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNAME = "qactor";

  /**
   * The package namespace URI.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_URI = "http://www.unibo.it/xtext/Qactor";

  /**
   * The package namespace name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_PREFIX = "qactor";

  /**
   * The singleton instance of the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  QactorPackage eINSTANCE = it.unibo.xtext.qactor.impl.QactorPackageImpl.init();

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.QActorSystemImpl <em>QActor System</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.QActorSystemImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getQActorSystem()
   * @generated
   */
  int QACTOR_SYSTEM = 0;

  /**
   * The feature id for the '<em><b>Spec</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int QACTOR_SYSTEM__SPEC = 0;

  /**
   * The number of structural features of the '<em>QActor System</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int QACTOR_SYSTEM_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.QActorSystemSpecImpl <em>QActor System Spec</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.QActorSystemSpecImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getQActorSystemSpec()
   * @generated
   */
  int QACTOR_SYSTEM_SPEC = 1;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int QACTOR_SYSTEM_SPEC__NAME = 0;

  /**
   * The feature id for the '<em><b>Testing</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int QACTOR_SYSTEM_SPEC__TESTING = 1;

  /**
   * The feature id for the '<em><b>Message</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int QACTOR_SYSTEM_SPEC__MESSAGE = 2;

  /**
   * The feature id for the '<em><b>Context</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int QACTOR_SYSTEM_SPEC__CONTEXT = 3;

  /**
   * The feature id for the '<em><b>Actor</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int QACTOR_SYSTEM_SPEC__ACTOR = 4;

  /**
   * The feature id for the '<em><b>Robot</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int QACTOR_SYSTEM_SPEC__ROBOT = 5;

  /**
   * The number of structural features of the '<em>QActor System Spec</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int QACTOR_SYSTEM_SPEC_FEATURE_COUNT = 6;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.RobotImpl <em>Robot</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.RobotImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getRobot()
   * @generated
   */
  int ROBOT = 2;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ROBOT__NAME = 0;

  /**
   * The feature id for the '<em><b>Actor</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ROBOT__ACTOR = 1;

  /**
   * The number of structural features of the '<em>Robot</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ROBOT_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.MessageImpl <em>Message</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.MessageImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getMessage()
   * @generated
   */
  int MESSAGE = 3;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MESSAGE__NAME = 0;

  /**
   * The feature id for the '<em><b>Msg</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MESSAGE__MSG = 1;

  /**
   * The number of structural features of the '<em>Message</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MESSAGE_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.OutOnlyMessageImpl <em>Out Only Message</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.OutOnlyMessageImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getOutOnlyMessage()
   * @generated
   */
  int OUT_ONLY_MESSAGE = 4;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OUT_ONLY_MESSAGE__NAME = MESSAGE__NAME;

  /**
   * The feature id for the '<em><b>Msg</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OUT_ONLY_MESSAGE__MSG = MESSAGE__MSG;

  /**
   * The number of structural features of the '<em>Out Only Message</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OUT_ONLY_MESSAGE_FEATURE_COUNT = MESSAGE_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.OutInMessageImpl <em>Out In Message</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.OutInMessageImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getOutInMessage()
   * @generated
   */
  int OUT_IN_MESSAGE = 5;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OUT_IN_MESSAGE__NAME = MESSAGE__NAME;

  /**
   * The feature id for the '<em><b>Msg</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OUT_IN_MESSAGE__MSG = MESSAGE__MSG;

  /**
   * The number of structural features of the '<em>Out In Message</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OUT_IN_MESSAGE_FEATURE_COUNT = MESSAGE_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.EventImpl <em>Event</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.EventImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getEvent()
   * @generated
   */
  int EVENT = 6;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EVENT__NAME = OUT_ONLY_MESSAGE__NAME;

  /**
   * The feature id for the '<em><b>Msg</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EVENT__MSG = OUT_ONLY_MESSAGE__MSG;

  /**
   * The number of structural features of the '<em>Event</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EVENT_FEATURE_COUNT = OUT_ONLY_MESSAGE_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.SignalImpl <em>Signal</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.SignalImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getSignal()
   * @generated
   */
  int SIGNAL = 7;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIGNAL__NAME = OUT_ONLY_MESSAGE__NAME;

  /**
   * The feature id for the '<em><b>Msg</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIGNAL__MSG = OUT_ONLY_MESSAGE__MSG;

  /**
   * The number of structural features of the '<em>Signal</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIGNAL_FEATURE_COUNT = OUT_ONLY_MESSAGE_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.TokenImpl <em>Token</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.TokenImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getToken()
   * @generated
   */
  int TOKEN = 8;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TOKEN__NAME = OUT_ONLY_MESSAGE__NAME;

  /**
   * The feature id for the '<em><b>Msg</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TOKEN__MSG = OUT_ONLY_MESSAGE__MSG;

  /**
   * The number of structural features of the '<em>Token</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TOKEN_FEATURE_COUNT = OUT_ONLY_MESSAGE_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.DispatchImpl <em>Dispatch</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.DispatchImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getDispatch()
   * @generated
   */
  int DISPATCH = 9;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DISPATCH__NAME = OUT_ONLY_MESSAGE__NAME;

  /**
   * The feature id for the '<em><b>Msg</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DISPATCH__MSG = OUT_ONLY_MESSAGE__MSG;

  /**
   * The number of structural features of the '<em>Dispatch</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DISPATCH_FEATURE_COUNT = OUT_ONLY_MESSAGE_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.RequestImpl <em>Request</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.RequestImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getRequest()
   * @generated
   */
  int REQUEST = 10;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REQUEST__NAME = OUT_IN_MESSAGE__NAME;

  /**
   * The feature id for the '<em><b>Msg</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REQUEST__MSG = OUT_IN_MESSAGE__MSG;

  /**
   * The number of structural features of the '<em>Request</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REQUEST_FEATURE_COUNT = OUT_IN_MESSAGE_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.InvitationImpl <em>Invitation</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.InvitationImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getInvitation()
   * @generated
   */
  int INVITATION = 11;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INVITATION__NAME = OUT_IN_MESSAGE__NAME;

  /**
   * The feature id for the '<em><b>Msg</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INVITATION__MSG = OUT_IN_MESSAGE__MSG;

  /**
   * The number of structural features of the '<em>Invitation</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INVITATION_FEATURE_COUNT = OUT_IN_MESSAGE_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.ContextImpl <em>Context</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.ContextImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getContext()
   * @generated
   */
  int CONTEXT = 12;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONTEXT__NAME = 0;

  /**
   * The feature id for the '<em><b>Ip</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONTEXT__IP = 1;

  /**
   * The feature id for the '<em><b>Env</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONTEXT__ENV = 2;

  /**
   * The feature id for the '<em><b>Color</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONTEXT__COLOR = 3;

  /**
   * The feature id for the '<em><b>Standalone</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONTEXT__STANDALONE = 4;

  /**
   * The feature id for the '<em><b>Httpserver</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONTEXT__HTTPSERVER = 5;

  /**
   * The feature id for the '<em><b>Handler</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONTEXT__HANDLER = 6;

  /**
   * The number of structural features of the '<em>Context</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONTEXT_FEATURE_COUNT = 7;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.QActorImpl <em>QActor</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.QActorImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getQActor()
   * @generated
   */
  int QACTOR = 13;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int QACTOR__NAME = 0;

  /**
   * The feature id for the '<em><b>Context</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int QACTOR__CONTEXT = 1;

  /**
   * The feature id for the '<em><b>Env</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int QACTOR__ENV = 2;

  /**
   * The feature id for the '<em><b>Color</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int QACTOR__COLOR = 3;

  /**
   * The feature id for the '<em><b>Rules</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int QACTOR__RULES = 4;

  /**
   * The feature id for the '<em><b>Data</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int QACTOR__DATA = 5;

  /**
   * The feature id for the '<em><b>Action</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int QACTOR__ACTION = 6;

  /**
   * The feature id for the '<em><b>Plans</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int QACTOR__PLANS = 7;

  /**
   * The number of structural features of the '<em>QActor</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int QACTOR_FEATURE_COUNT = 8;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.RuleImpl <em>Rule</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.RuleImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getRule()
   * @generated
   */
  int RULE = 14;

  /**
   * The feature id for the '<em><b>Head</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RULE__HEAD = 0;

  /**
   * The feature id for the '<em><b>Body</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RULE__BODY = 1;

  /**
   * The number of structural features of the '<em>Rule</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RULE_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.PHeadImpl <em>PHead</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.PHeadImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getPHead()
   * @generated
   */
  int PHEAD = 15;

  /**
   * The number of structural features of the '<em>PHead</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PHEAD_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.PTermImpl <em>PTerm</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.PTermImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getPTerm()
   * @generated
   */
  int PTERM = 16;

  /**
   * The number of structural features of the '<em>PTerm</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PTERM_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.PAtomImpl <em>PAtom</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.PAtomImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getPAtom()
   * @generated
   */
  int PATOM = 17;

  /**
   * The number of structural features of the '<em>PAtom</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PATOM_FEATURE_COUNT = PHEAD_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.PAtomStringImpl <em>PAtom String</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.PAtomStringImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getPAtomString()
   * @generated
   */
  int PATOM_STRING = 18;

  /**
   * The feature id for the '<em><b>Val</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PATOM_STRING__VAL = PATOM_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>PAtom String</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PATOM_STRING_FEATURE_COUNT = PATOM_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.PAtomicImpl <em>PAtomic</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.PAtomicImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getPAtomic()
   * @generated
   */
  int PATOMIC = 19;

  /**
   * The feature id for the '<em><b>Val</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PATOMIC__VAL = PATOM_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>PAtomic</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PATOMIC_FEATURE_COUNT = PATOM_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.PAtomNumImpl <em>PAtom Num</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.PAtomNumImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getPAtomNum()
   * @generated
   */
  int PATOM_NUM = 20;

  /**
   * The feature id for the '<em><b>Val</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PATOM_NUM__VAL = PATOM_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>PAtom Num</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PATOM_NUM_FEATURE_COUNT = PATOM_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.PStructImpl <em>PStruct</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.PStructImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getPStruct()
   * @generated
   */
  int PSTRUCT = 21;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PSTRUCT__NAME = PHEAD_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Msg Arg</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PSTRUCT__MSG_ARG = PHEAD_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>PStruct</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PSTRUCT_FEATURE_COUNT = PHEAD_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.PActorCallImpl <em>PActor Call</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.PActorCallImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getPActorCall()
   * @generated
   */
  int PACTOR_CALL = 22;

  /**
   * The feature id for the '<em><b>Body</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PACTOR_CALL__BODY = PTERM_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>PActor Call</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PACTOR_CALL_FEATURE_COUNT = PTERM_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.PPredefImpl <em>PPredef</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.PPredefImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getPPredef()
   * @generated
   */
  int PPREDEF = 23;

  /**
   * The number of structural features of the '<em>PPredef</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PPREDEF_FEATURE_COUNT = PTERM_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.PIsImpl <em>PIs</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.PIsImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getPIs()
   * @generated
   */
  int PIS = 24;

  /**
   * The feature id for the '<em><b>Varout</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PIS__VAROUT = PPREDEF_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Varin</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PIS__VARIN = PPREDEF_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Num</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PIS__NUM = PPREDEF_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>PIs</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PIS_FEATURE_COUNT = PPREDEF_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.PAtomCutImpl <em>PAtom Cut</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.PAtomCutImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getPAtomCut()
   * @generated
   */
  int PATOM_CUT = 25;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PATOM_CUT__NAME = PPREDEF_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>PAtom Cut</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PATOM_CUT_FEATURE_COUNT = PPREDEF_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.DataImpl <em>Data</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.DataImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getData()
   * @generated
   */
  int DATA = 26;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATA__NAME = 0;

  /**
   * The number of structural features of the '<em>Data</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATA_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.IntegerDataImpl <em>Integer Data</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.IntegerDataImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getIntegerData()
   * @generated
   */
  int INTEGER_DATA = 27;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTEGER_DATA__NAME = DATA__NAME;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTEGER_DATA__VALUE = DATA_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Integer Data</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTEGER_DATA_FEATURE_COUNT = DATA_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.StringDataImpl <em>String Data</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.StringDataImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getStringData()
   * @generated
   */
  int STRING_DATA = 28;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STRING_DATA__NAME = DATA__NAME;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STRING_DATA__VALUE = DATA_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>String Data</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STRING_DATA_FEATURE_COUNT = DATA_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.ActionImpl <em>Action</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.ActionImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getAction()
   * @generated
   */
  int ACTION = 29;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ACTION__NAME = 0;

  /**
   * The feature id for the '<em><b>Undoable</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ACTION__UNDOABLE = 1;

  /**
   * The feature id for the '<em><b>Msec</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ACTION__MSEC = 2;

  /**
   * The feature id for the '<em><b>Arg</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ACTION__ARG = 3;

  /**
   * The number of structural features of the '<em>Action</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ACTION_FEATURE_COUNT = 4;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.PlanImpl <em>Plan</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.PlanImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getPlan()
   * @generated
   */
  int PLAN = 30;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PLAN__NAME = 0;

  /**
   * The feature id for the '<em><b>Normal</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PLAN__NORMAL = 1;

  /**
   * The feature id for the '<em><b>Resume</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PLAN__RESUME = 2;

  /**
   * The feature id for the '<em><b>Action</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PLAN__ACTION = 3;

  /**
   * The number of structural features of the '<em>Plan</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PLAN_FEATURE_COUNT = 4;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.PlanActionImpl <em>Plan Action</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.PlanActionImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getPlanAction()
   * @generated
   */
  int PLAN_ACTION = 31;

  /**
   * The feature id for the '<em><b>Guard</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PLAN_ACTION__GUARD = 0;

  /**
   * The feature id for the '<em><b>Move</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PLAN_ACTION__MOVE = 1;

  /**
   * The feature id for the '<em><b>React</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PLAN_ACTION__REACT = 2;

  /**
   * The feature id for the '<em><b>Elsemove</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PLAN_ACTION__ELSEMOVE = 3;

  /**
   * The feature id for the '<em><b>Elsereact</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PLAN_ACTION__ELSEREACT = 4;

  /**
   * The number of structural features of the '<em>Plan Action</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PLAN_ACTION_FEATURE_COUNT = 5;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.GuardImpl <em>Guard</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.GuardImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getGuard()
   * @generated
   */
  int GUARD = 32;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GUARD__NAME = 0;

  /**
   * The feature id for the '<em><b>Not</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GUARD__NOT = 1;

  /**
   * The feature id for the '<em><b>Guardspec</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GUARD__GUARDSPEC = 2;

  /**
   * The number of structural features of the '<em>Guard</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GUARD_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.GuardPredicateImpl <em>Guard Predicate</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.GuardPredicateImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getGuardPredicate()
   * @generated
   */
  int GUARD_PREDICATE = 33;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GUARD_PREDICATE__NAME = 0;

  /**
   * The feature id for the '<em><b>Pred</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GUARD_PREDICATE__PRED = 1;

  /**
   * The number of structural features of the '<em>Guard Predicate</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GUARD_PREDICATE_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.GuardPredicateRemovableImpl <em>Guard Predicate Removable</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.GuardPredicateRemovableImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getGuardPredicateRemovable()
   * @generated
   */
  int GUARD_PREDICATE_REMOVABLE = 34;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GUARD_PREDICATE_REMOVABLE__NAME = GUARD_PREDICATE__NAME;

  /**
   * The feature id for the '<em><b>Pred</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GUARD_PREDICATE_REMOVABLE__PRED = GUARD_PREDICATE__PRED;

  /**
   * The number of structural features of the '<em>Guard Predicate Removable</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GUARD_PREDICATE_REMOVABLE_FEATURE_COUNT = GUARD_PREDICATE_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.GuardPredicateStableImpl <em>Guard Predicate Stable</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.GuardPredicateStableImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getGuardPredicateStable()
   * @generated
   */
  int GUARD_PREDICATE_STABLE = 35;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GUARD_PREDICATE_STABLE__NAME = GUARD_PREDICATE__NAME;

  /**
   * The feature id for the '<em><b>Pred</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GUARD_PREDICATE_STABLE__PRED = GUARD_PREDICATE__PRED;

  /**
   * The number of structural features of the '<em>Guard Predicate Stable</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GUARD_PREDICATE_STABLE_FEATURE_COUNT = GUARD_PREDICATE_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.MoveImpl <em>Move</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.MoveImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getMove()
   * @generated
   */
  int MOVE = 36;

  /**
   * The number of structural features of the '<em>Move</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MOVE_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.ActionMoveImpl <em>Action Move</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.ActionMoveImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getActionMove()
   * @generated
   */
  int ACTION_MOVE = 37;

  /**
   * The number of structural features of the '<em>Action Move</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ACTION_MOVE_FEATURE_COUNT = MOVE_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.ExecuteActionImpl <em>Execute Action</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.ExecuteActionImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getExecuteAction()
   * @generated
   */
  int EXECUTE_ACTION = 38;

  /**
   * The feature id for the '<em><b>Action</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXECUTE_ACTION__ACTION = ACTION_MOVE_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Arg</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXECUTE_ACTION__ARG = ACTION_MOVE_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Sentence</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXECUTE_ACTION__SENTENCE = ACTION_MOVE_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Execute Action</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXECUTE_ACTION_FEATURE_COUNT = ACTION_MOVE_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.SolveGoalImpl <em>Solve Goal</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.SolveGoalImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getSolveGoal()
   * @generated
   */
  int SOLVE_GOAL = 39;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SOLVE_GOAL__NAME = ACTION_MOVE_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Goal</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SOLVE_GOAL__GOAL = ACTION_MOVE_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Duration</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SOLVE_GOAL__DURATION = ACTION_MOVE_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Plan</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SOLVE_GOAL__PLAN = ACTION_MOVE_FEATURE_COUNT + 3;

  /**
   * The number of structural features of the '<em>Solve Goal</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SOLVE_GOAL_FEATURE_COUNT = ACTION_MOVE_FEATURE_COUNT + 4;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.DemoImpl <em>Demo</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.DemoImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getDemo()
   * @generated
   */
  int DEMO = 40;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DEMO__NAME = ACTION_MOVE_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Goal</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DEMO__GOAL = ACTION_MOVE_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Plan</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DEMO__PLAN = ACTION_MOVE_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Demo</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DEMO_FEATURE_COUNT = ACTION_MOVE_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.ActorOpImpl <em>Actor Op</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.ActorOpImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getActorOp()
   * @generated
   */
  int ACTOR_OP = 41;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ACTOR_OP__NAME = ACTION_MOVE_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Goal</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ACTOR_OP__GOAL = ACTION_MOVE_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Plan</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ACTOR_OP__PLAN = ACTION_MOVE_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Actor Op</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ACTOR_OP_FEATURE_COUNT = ACTION_MOVE_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.BasicRobotMoveImpl <em>Basic Robot Move</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.BasicRobotMoveImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getBasicRobotMove()
   * @generated
   */
  int BASIC_ROBOT_MOVE = 42;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BASIC_ROBOT_MOVE__NAME = MOVE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Basic Robot Move</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BASIC_ROBOT_MOVE_FEATURE_COUNT = MOVE_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.BasicMoveImpl <em>Basic Move</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.BasicMoveImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getBasicMove()
   * @generated
   */
  int BASIC_MOVE = 43;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BASIC_MOVE__NAME = MOVE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Basic Move</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BASIC_MOVE_FEATURE_COUNT = MOVE_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.PrintImpl <em>Print</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.PrintImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getPrint()
   * @generated
   */
  int PRINT = 44;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PRINT__NAME = BASIC_MOVE__NAME;

  /**
   * The feature id for the '<em><b>Args</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PRINT__ARGS = BASIC_MOVE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Print</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PRINT_FEATURE_COUNT = BASIC_MOVE_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.PrintCurrentEventImpl <em>Print Current Event</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.PrintCurrentEventImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getPrintCurrentEvent()
   * @generated
   */
  int PRINT_CURRENT_EVENT = 45;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PRINT_CURRENT_EVENT__NAME = BASIC_MOVE__NAME;

  /**
   * The feature id for the '<em><b>Memo</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PRINT_CURRENT_EVENT__MEMO = BASIC_MOVE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Print Current Event</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PRINT_CURRENT_EVENT_FEATURE_COUNT = BASIC_MOVE_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.PrintCurrentMessageImpl <em>Print Current Message</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.PrintCurrentMessageImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getPrintCurrentMessage()
   * @generated
   */
  int PRINT_CURRENT_MESSAGE = 46;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PRINT_CURRENT_MESSAGE__NAME = BASIC_MOVE__NAME;

  /**
   * The feature id for the '<em><b>Memo</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PRINT_CURRENT_MESSAGE__MEMO = BASIC_MOVE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Print Current Message</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PRINT_CURRENT_MESSAGE_FEATURE_COUNT = BASIC_MOVE_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.MemoCurrentEventImpl <em>Memo Current Event</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.MemoCurrentEventImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getMemoCurrentEvent()
   * @generated
   */
  int MEMO_CURRENT_EVENT = 47;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MEMO_CURRENT_EVENT__NAME = BASIC_MOVE__NAME;

  /**
   * The feature id for the '<em><b>Lastonly</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MEMO_CURRENT_EVENT__LASTONLY = BASIC_MOVE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Memo Current Event</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MEMO_CURRENT_EVENT_FEATURE_COUNT = BASIC_MOVE_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.MemoCurrentMessageImpl <em>Memo Current Message</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.MemoCurrentMessageImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getMemoCurrentMessage()
   * @generated
   */
  int MEMO_CURRENT_MESSAGE = 48;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MEMO_CURRENT_MESSAGE__NAME = BASIC_MOVE__NAME;

  /**
   * The feature id for the '<em><b>Lastonly</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MEMO_CURRENT_MESSAGE__LASTONLY = BASIC_MOVE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Memo Current Message</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MEMO_CURRENT_MESSAGE_FEATURE_COUNT = BASIC_MOVE_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.PlanMoveImpl <em>Plan Move</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.PlanMoveImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getPlanMove()
   * @generated
   */
  int PLAN_MOVE = 49;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PLAN_MOVE__NAME = MOVE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Plan Move</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PLAN_MOVE_FEATURE_COUNT = MOVE_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.GetActivationEventImpl <em>Get Activation Event</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.GetActivationEventImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getGetActivationEvent()
   * @generated
   */
  int GET_ACTIVATION_EVENT = 50;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GET_ACTIVATION_EVENT__NAME = PLAN_MOVE__NAME;

  /**
   * The feature id for the '<em><b>Var</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GET_ACTIVATION_EVENT__VAR = PLAN_MOVE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Get Activation Event</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GET_ACTIVATION_EVENT_FEATURE_COUNT = PLAN_MOVE_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.GetSensedEventImpl <em>Get Sensed Event</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.GetSensedEventImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getGetSensedEvent()
   * @generated
   */
  int GET_SENSED_EVENT = 51;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GET_SENSED_EVENT__NAME = PLAN_MOVE__NAME;

  /**
   * The feature id for the '<em><b>Var</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GET_SENSED_EVENT__VAR = PLAN_MOVE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Get Sensed Event</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GET_SENSED_EVENT_FEATURE_COUNT = PLAN_MOVE_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.LoadPlanImpl <em>Load Plan</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.LoadPlanImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getLoadPlan()
   * @generated
   */
  int LOAD_PLAN = 52;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LOAD_PLAN__NAME = PLAN_MOVE__NAME;

  /**
   * The feature id for the '<em><b>Fname</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LOAD_PLAN__FNAME = PLAN_MOVE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Load Plan</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LOAD_PLAN_FEATURE_COUNT = PLAN_MOVE_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.RunPlanImpl <em>Run Plan</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.RunPlanImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getRunPlan()
   * @generated
   */
  int RUN_PLAN = 53;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RUN_PLAN__NAME = PLAN_MOVE__NAME;

  /**
   * The feature id for the '<em><b>Plainid</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RUN_PLAN__PLAINID = PLAN_MOVE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Run Plan</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RUN_PLAN_FEATURE_COUNT = PLAN_MOVE_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.ResumePlanImpl <em>Resume Plan</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.ResumePlanImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getResumePlan()
   * @generated
   */
  int RESUME_PLAN = 54;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RESUME_PLAN__NAME = PLAN_MOVE__NAME;

  /**
   * The number of structural features of the '<em>Resume Plan</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RESUME_PLAN_FEATURE_COUNT = PLAN_MOVE_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.SuspendPlanImpl <em>Suspend Plan</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.SuspendPlanImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getSuspendPlan()
   * @generated
   */
  int SUSPEND_PLAN = 55;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SUSPEND_PLAN__NAME = PLAN_MOVE__NAME;

  /**
   * The number of structural features of the '<em>Suspend Plan</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SUSPEND_PLAN_FEATURE_COUNT = PLAN_MOVE_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.RepeatPlanImpl <em>Repeat Plan</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.RepeatPlanImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getRepeatPlan()
   * @generated
   */
  int REPEAT_PLAN = 56;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REPEAT_PLAN__NAME = PLAN_MOVE__NAME;

  /**
   * The feature id for the '<em><b>Niter</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REPEAT_PLAN__NITER = PLAN_MOVE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Repeat Plan</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REPEAT_PLAN_FEATURE_COUNT = PLAN_MOVE_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.SwitchPlanImpl <em>Switch Plan</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.SwitchPlanImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getSwitchPlan()
   * @generated
   */
  int SWITCH_PLAN = 57;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SWITCH_PLAN__NAME = PLAN_MOVE__NAME;

  /**
   * The feature id for the '<em><b>Plan</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SWITCH_PLAN__PLAN = PLAN_MOVE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Switch Plan</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SWITCH_PLAN_FEATURE_COUNT = PLAN_MOVE_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.EndPlanImpl <em>End Plan</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.EndPlanImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getEndPlan()
   * @generated
   */
  int END_PLAN = 58;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int END_PLAN__NAME = PLAN_MOVE__NAME;

  /**
   * The feature id for the '<em><b>Msg</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int END_PLAN__MSG = PLAN_MOVE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>End Plan</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int END_PLAN_FEATURE_COUNT = PLAN_MOVE_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.EndActorImpl <em>End Actor</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.EndActorImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getEndActor()
   * @generated
   */
  int END_ACTOR = 59;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int END_ACTOR__NAME = PLAN_MOVE__NAME;

  /**
   * The feature id for the '<em><b>Msg</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int END_ACTOR__MSG = PLAN_MOVE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>End Actor</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int END_ACTOR_FEATURE_COUNT = PLAN_MOVE_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.GuardMoveImpl <em>Guard Move</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.GuardMoveImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getGuardMove()
   * @generated
   */
  int GUARD_MOVE = 60;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GUARD_MOVE__NAME = MOVE_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Rule</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GUARD_MOVE__RULE = MOVE_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Guard Move</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GUARD_MOVE_FEATURE_COUNT = MOVE_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.AddRuleImpl <em>Add Rule</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.AddRuleImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getAddRule()
   * @generated
   */
  int ADD_RULE = 61;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ADD_RULE__NAME = GUARD_MOVE__NAME;

  /**
   * The feature id for the '<em><b>Rule</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ADD_RULE__RULE = GUARD_MOVE__RULE;

  /**
   * The number of structural features of the '<em>Add Rule</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ADD_RULE_FEATURE_COUNT = GUARD_MOVE_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.RemoveRuleImpl <em>Remove Rule</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.RemoveRuleImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getRemoveRule()
   * @generated
   */
  int REMOVE_RULE = 62;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REMOVE_RULE__NAME = GUARD_MOVE__NAME;

  /**
   * The feature id for the '<em><b>Rule</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REMOVE_RULE__RULE = GUARD_MOVE__RULE;

  /**
   * The number of structural features of the '<em>Remove Rule</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REMOVE_RULE_FEATURE_COUNT = GUARD_MOVE_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.MessageMoveImpl <em>Message Move</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.MessageMoveImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getMessageMove()
   * @generated
   */
  int MESSAGE_MOVE = 63;

  /**
   * The number of structural features of the '<em>Message Move</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MESSAGE_MOVE_FEATURE_COUNT = MOVE_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.SendDispatchImpl <em>Send Dispatch</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.SendDispatchImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getSendDispatch()
   * @generated
   */
  int SEND_DISPATCH = 64;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SEND_DISPATCH__NAME = MESSAGE_MOVE_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Dest</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SEND_DISPATCH__DEST = MESSAGE_MOVE_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Msgref</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SEND_DISPATCH__MSGREF = MESSAGE_MOVE_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Val</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SEND_DISPATCH__VAL = MESSAGE_MOVE_FEATURE_COUNT + 3;

  /**
   * The number of structural features of the '<em>Send Dispatch</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SEND_DISPATCH_FEATURE_COUNT = MESSAGE_MOVE_FEATURE_COUNT + 4;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.SendRequestImpl <em>Send Request</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.SendRequestImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getSendRequest()
   * @generated
   */
  int SEND_REQUEST = 65;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SEND_REQUEST__NAME = MESSAGE_MOVE_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Dest</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SEND_REQUEST__DEST = MESSAGE_MOVE_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Msgref</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SEND_REQUEST__MSGREF = MESSAGE_MOVE_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Val</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SEND_REQUEST__VAL = MESSAGE_MOVE_FEATURE_COUNT + 3;

  /**
   * The number of structural features of the '<em>Send Request</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SEND_REQUEST_FEATURE_COUNT = MESSAGE_MOVE_FEATURE_COUNT + 4;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.ReplyToCallerImpl <em>Reply To Caller</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.ReplyToCallerImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getReplyToCaller()
   * @generated
   */
  int REPLY_TO_CALLER = 66;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REPLY_TO_CALLER__NAME = MESSAGE_MOVE_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Msgref</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REPLY_TO_CALLER__MSGREF = MESSAGE_MOVE_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Val</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REPLY_TO_CALLER__VAL = MESSAGE_MOVE_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Reply To Caller</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REPLY_TO_CALLER_FEATURE_COUNT = MESSAGE_MOVE_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.ReceiveMsgImpl <em>Receive Msg</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.ReceiveMsgImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getReceiveMsg()
   * @generated
   */
  int RECEIVE_MSG = 67;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RECEIVE_MSG__NAME = MESSAGE_MOVE_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Duration</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RECEIVE_MSG__DURATION = MESSAGE_MOVE_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Spec</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RECEIVE_MSG__SPEC = MESSAGE_MOVE_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Receive Msg</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RECEIVE_MSG_FEATURE_COUNT = MESSAGE_MOVE_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.MsgSpecImpl <em>Msg Spec</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.MsgSpecImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getMsgSpec()
   * @generated
   */
  int MSG_SPEC = 68;

  /**
   * The feature id for the '<em><b>Msg</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MSG_SPEC__MSG = 0;

  /**
   * The feature id for the '<em><b>Sender</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MSG_SPEC__SENDER = 1;

  /**
   * The feature id for the '<em><b>Content</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MSG_SPEC__CONTENT = 2;

  /**
   * The number of structural features of the '<em>Msg Spec</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MSG_SPEC_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.OnReceiveMsgImpl <em>On Receive Msg</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.OnReceiveMsgImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getOnReceiveMsg()
   * @generated
   */
  int ON_RECEIVE_MSG = 69;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ON_RECEIVE_MSG__NAME = MESSAGE_MOVE_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Msgid</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ON_RECEIVE_MSG__MSGID = MESSAGE_MOVE_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Msgtype</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ON_RECEIVE_MSG__MSGTYPE = MESSAGE_MOVE_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Msgsender</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ON_RECEIVE_MSG__MSGSENDER = MESSAGE_MOVE_FEATURE_COUNT + 3;

  /**
   * The feature id for the '<em><b>Msgreceiver</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ON_RECEIVE_MSG__MSGRECEIVER = MESSAGE_MOVE_FEATURE_COUNT + 4;

  /**
   * The feature id for the '<em><b>Msgcontent</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ON_RECEIVE_MSG__MSGCONTENT = MESSAGE_MOVE_FEATURE_COUNT + 5;

  /**
   * The feature id for the '<em><b>Msgseqnum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ON_RECEIVE_MSG__MSGSEQNUM = MESSAGE_MOVE_FEATURE_COUNT + 6;

  /**
   * The feature id for the '<em><b>Duration</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ON_RECEIVE_MSG__DURATION = MESSAGE_MOVE_FEATURE_COUNT + 7;

  /**
   * The number of structural features of the '<em>On Receive Msg</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ON_RECEIVE_MSG_FEATURE_COUNT = MESSAGE_MOVE_FEATURE_COUNT + 8;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.MsgSelectImpl <em>Msg Select</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.MsgSelectImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getMsgSelect()
   * @generated
   */
  int MSG_SELECT = 70;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MSG_SELECT__NAME = MESSAGE_MOVE_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Duration</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MSG_SELECT__DURATION = MESSAGE_MOVE_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Messages</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MSG_SELECT__MESSAGES = MESSAGE_MOVE_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Plans</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MSG_SELECT__PLANS = MESSAGE_MOVE_FEATURE_COUNT + 3;

  /**
   * The number of structural features of the '<em>Msg Select</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MSG_SELECT_FEATURE_COUNT = MESSAGE_MOVE_FEATURE_COUNT + 4;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.RaiseEventImpl <em>Raise Event</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.RaiseEventImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getRaiseEvent()
   * @generated
   */
  int RAISE_EVENT = 71;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RAISE_EVENT__NAME = MESSAGE_MOVE_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Ev</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RAISE_EVENT__EV = MESSAGE_MOVE_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Content</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RAISE_EVENT__CONTENT = MESSAGE_MOVE_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Raise Event</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RAISE_EVENT_FEATURE_COUNT = MESSAGE_MOVE_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.SenseEventImpl <em>Sense Event</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.SenseEventImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getSenseEvent()
   * @generated
   */
  int SENSE_EVENT = 72;

  /**
   * The feature id for the '<em><b>Duration</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SENSE_EVENT__DURATION = MESSAGE_MOVE_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Events</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SENSE_EVENT__EVENTS = MESSAGE_MOVE_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Plans</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SENSE_EVENT__PLANS = MESSAGE_MOVE_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Sense Event</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SENSE_EVENT_FEATURE_COUNT = MESSAGE_MOVE_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.MsgSwitchImpl <em>Msg Switch</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.MsgSwitchImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getMsgSwitch()
   * @generated
   */
  int MSG_SWITCH = 73;

  /**
   * The feature id for the '<em><b>Message</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MSG_SWITCH__MESSAGE = MESSAGE_MOVE_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Msg</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MSG_SWITCH__MSG = MESSAGE_MOVE_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Move</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MSG_SWITCH__MOVE = MESSAGE_MOVE_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Msg Switch</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MSG_SWITCH_FEATURE_COUNT = MESSAGE_MOVE_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.EventSwitchImpl <em>Event Switch</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.EventSwitchImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getEventSwitch()
   * @generated
   */
  int EVENT_SWITCH = 74;

  /**
   * The feature id for the '<em><b>Event</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EVENT_SWITCH__EVENT = MESSAGE_MOVE_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Msg</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EVENT_SWITCH__MSG = MESSAGE_MOVE_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Move</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EVENT_SWITCH__MOVE = MESSAGE_MOVE_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Event Switch</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EVENT_SWITCH_FEATURE_COUNT = MESSAGE_MOVE_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.ContinuationImpl <em>Continuation</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.ContinuationImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getContinuation()
   * @generated
   */
  int CONTINUATION = 75;

  /**
   * The feature id for the '<em><b>Plan</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONTINUATION__PLAN = 0;

  /**
   * The feature id for the '<em><b>Nane</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONTINUATION__NANE = 1;

  /**
   * The number of structural features of the '<em>Continuation</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONTINUATION_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.ExtensionMoveImpl <em>Extension Move</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.ExtensionMoveImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getExtensionMove()
   * @generated
   */
  int EXTENSION_MOVE = 76;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXTENSION_MOVE__NAME = MOVE_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Duration</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXTENSION_MOVE__DURATION = MOVE_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Extension Move</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXTENSION_MOVE_FEATURE_COUNT = MOVE_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.PhotoImpl <em>Photo</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.PhotoImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getPhoto()
   * @generated
   */
  int PHOTO = 77;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PHOTO__NAME = EXTENSION_MOVE__NAME;

  /**
   * The feature id for the '<em><b>Duration</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PHOTO__DURATION = EXTENSION_MOVE__DURATION;

  /**
   * The feature id for the '<em><b>Destfile</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PHOTO__DESTFILE = EXTENSION_MOVE_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Answer Event</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PHOTO__ANSWER_EVENT = EXTENSION_MOVE_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Photo</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PHOTO_FEATURE_COUNT = EXTENSION_MOVE_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.SoundImpl <em>Sound</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.SoundImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getSound()
   * @generated
   */
  int SOUND = 78;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SOUND__NAME = EXTENSION_MOVE__NAME;

  /**
   * The feature id for the '<em><b>Duration</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SOUND__DURATION = EXTENSION_MOVE__DURATION;

  /**
   * The feature id for the '<em><b>Srcfile</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SOUND__SRCFILE = EXTENSION_MOVE_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Answer Event</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SOUND__ANSWER_EVENT = EXTENSION_MOVE_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Sound</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SOUND_FEATURE_COUNT = EXTENSION_MOVE_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.VideoImpl <em>Video</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.VideoImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getVideo()
   * @generated
   */
  int VIDEO = 79;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VIDEO__NAME = EXTENSION_MOVE__NAME;

  /**
   * The feature id for the '<em><b>Duration</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VIDEO__DURATION = EXTENSION_MOVE__DURATION;

  /**
   * The feature id for the '<em><b>Destfile</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VIDEO__DESTFILE = EXTENSION_MOVE_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Answer Event</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VIDEO__ANSWER_EVENT = EXTENSION_MOVE_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Video</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VIDEO_FEATURE_COUNT = EXTENSION_MOVE_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.DelayImpl <em>Delay</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.DelayImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getDelay()
   * @generated
   */
  int DELAY = 80;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DELAY__NAME = EXTENSION_MOVE__NAME;

  /**
   * The feature id for the '<em><b>Duration</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DELAY__DURATION = EXTENSION_MOVE__DURATION;

  /**
   * The number of structural features of the '<em>Delay</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DELAY_FEATURE_COUNT = EXTENSION_MOVE_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.AnswerEventImpl <em>Answer Event</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.AnswerEventImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getAnswerEvent()
   * @generated
   */
  int ANSWER_EVENT = 81;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ANSWER_EVENT__NAME = 0;

  /**
   * The number of structural features of the '<em>Answer Event</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ANSWER_EVENT_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.EventHandlerImpl <em>Event Handler</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.EventHandlerImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getEventHandler()
   * @generated
   */
  int EVENT_HANDLER = 82;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EVENT_HANDLER__NAME = 0;

  /**
   * The feature id for the '<em><b>Events</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EVENT_HANDLER__EVENTS = 1;

  /**
   * The feature id for the '<em><b>Print</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EVENT_HANDLER__PRINT = 2;

  /**
   * The feature id for the '<em><b>Body</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EVENT_HANDLER__BODY = 3;

  /**
   * The number of structural features of the '<em>Event Handler</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EVENT_HANDLER_FEATURE_COUNT = 4;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.EventHandlerBodyImpl <em>Event Handler Body</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.EventHandlerBodyImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getEventHandlerBody()
   * @generated
   */
  int EVENT_HANDLER_BODY = 83;

  /**
   * The feature id for the '<em><b>Op</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EVENT_HANDLER_BODY__OP = 0;

  /**
   * The number of structural features of the '<em>Event Handler Body</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EVENT_HANDLER_BODY_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.EventHandlerOperationImpl <em>Event Handler Operation</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.EventHandlerOperationImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getEventHandlerOperation()
   * @generated
   */
  int EVENT_HANDLER_OPERATION = 84;

  /**
   * The number of structural features of the '<em>Event Handler Operation</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EVENT_HANDLER_OPERATION_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.MemoOperationImpl <em>Memo Operation</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.MemoOperationImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getMemoOperation()
   * @generated
   */
  int MEMO_OPERATION = 85;

  /**
   * The feature id for the '<em><b>Rule</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MEMO_OPERATION__RULE = EVENT_HANDLER_OPERATION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Actor</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MEMO_OPERATION__ACTOR = EVENT_HANDLER_OPERATION_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Do Memo</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MEMO_OPERATION__DO_MEMO = EVENT_HANDLER_OPERATION_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Memo Operation</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MEMO_OPERATION_FEATURE_COUNT = EVENT_HANDLER_OPERATION_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.SolveOperationImpl <em>Solve Operation</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.SolveOperationImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getSolveOperation()
   * @generated
   */
  int SOLVE_OPERATION = 86;

  /**
   * The feature id for the '<em><b>Goal</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SOLVE_OPERATION__GOAL = EVENT_HANDLER_OPERATION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Actor</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SOLVE_OPERATION__ACTOR = EVENT_HANDLER_OPERATION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Solve Operation</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SOLVE_OPERATION_FEATURE_COUNT = EVENT_HANDLER_OPERATION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.SendEventAsDispatchImpl <em>Send Event As Dispatch</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.SendEventAsDispatchImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getSendEventAsDispatch()
   * @generated
   */
  int SEND_EVENT_AS_DISPATCH = 87;

  /**
   * The feature id for the '<em><b>Actor</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SEND_EVENT_AS_DISPATCH__ACTOR = EVENT_HANDLER_OPERATION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Msgref</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SEND_EVENT_AS_DISPATCH__MSGREF = EVENT_HANDLER_OPERATION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Send Event As Dispatch</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SEND_EVENT_AS_DISPATCH_FEATURE_COUNT = EVENT_HANDLER_OPERATION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.MemoRuleImpl <em>Memo Rule</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.MemoRuleImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getMemoRule()
   * @generated
   */
  int MEMO_RULE = 88;

  /**
   * The number of structural features of the '<em>Memo Rule</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MEMO_RULE_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.MemoEventImpl <em>Memo Event</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.MemoEventImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getMemoEvent()
   * @generated
   */
  int MEMO_EVENT = 89;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MEMO_EVENT__NAME = MEMO_RULE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Memo Event</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MEMO_EVENT_FEATURE_COUNT = MEMO_RULE_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.ReactionImpl <em>Reaction</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.ReactionImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getReaction()
   * @generated
   */
  int REACTION = 90;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REACTION__NAME = 0;

  /**
   * The feature id for the '<em><b>Alarms</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REACTION__ALARMS = 1;

  /**
   * The number of structural features of the '<em>Reaction</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REACTION_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.AlarmEventImpl <em>Alarm Event</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.AlarmEventImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getAlarmEvent()
   * @generated
   */
  int ALARM_EVENT = 91;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALARM_EVENT__NAME = 0;

  /**
   * The number of structural features of the '<em>Alarm Event</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALARM_EVENT_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.NormalEventImpl <em>Normal Event</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.NormalEventImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getNormalEvent()
   * @generated
   */
  int NORMAL_EVENT = 92;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NORMAL_EVENT__NAME = ALARM_EVENT__NAME;

  /**
   * The feature id for the '<em><b>Ev</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NORMAL_EVENT__EV = ALARM_EVENT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Plan Ref</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NORMAL_EVENT__PLAN_REF = ALARM_EVENT_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Normal Event</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NORMAL_EVENT_FEATURE_COUNT = ALARM_EVENT_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.ContinueEventImpl <em>Continue Event</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.ContinueEventImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getContinueEvent()
   * @generated
   */
  int CONTINUE_EVENT = 93;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONTINUE_EVENT__NAME = ALARM_EVENT__NAME;

  /**
   * The feature id for the '<em><b>Ev Occur</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONTINUE_EVENT__EV_OCCUR = ALARM_EVENT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Continue Event</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONTINUE_EVENT_FEATURE_COUNT = ALARM_EVENT_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.VarOrQactorImpl <em>Var Or Qactor</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.VarOrQactorImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getVarOrQactor()
   * @generated
   */
  int VAR_OR_QACTOR = 94;

  /**
   * The feature id for the '<em><b>Var</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VAR_OR_QACTOR__VAR = 0;

  /**
   * The feature id for the '<em><b>Dest</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VAR_OR_QACTOR__DEST = 1;

  /**
   * The number of structural features of the '<em>Var Or Qactor</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VAR_OR_QACTOR_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.VarOrAtomicImpl <em>Var Or Atomic</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.VarOrAtomicImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getVarOrAtomic()
   * @generated
   */
  int VAR_OR_ATOMIC = 95;

  /**
   * The feature id for the '<em><b>Var</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VAR_OR_ATOMIC__VAR = 0;

  /**
   * The feature id for the '<em><b>Const</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VAR_OR_ATOMIC__CONST = 1;

  /**
   * The number of structural features of the '<em>Var Or Atomic</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VAR_OR_ATOMIC_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.VarOrStringImpl <em>Var Or String</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.VarOrStringImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getVarOrString()
   * @generated
   */
  int VAR_OR_STRING = 96;

  /**
   * The feature id for the '<em><b>Var</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VAR_OR_STRING__VAR = 0;

  /**
   * The feature id for the '<em><b>Const</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VAR_OR_STRING__CONST = 1;

  /**
   * The number of structural features of the '<em>Var Or String</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VAR_OR_STRING_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.VarOrPStructImpl <em>Var Or PStruct</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.VarOrPStructImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getVarOrPStruct()
   * @generated
   */
  int VAR_OR_PSTRUCT = 97;

  /**
   * The feature id for the '<em><b>Var</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VAR_OR_PSTRUCT__VAR = 0;

  /**
   * The feature id for the '<em><b>Psrtuct</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VAR_OR_PSTRUCT__PSRTUCT = 1;

  /**
   * The number of structural features of the '<em>Var Or PStruct</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VAR_OR_PSTRUCT_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.VarOrPheadImpl <em>Var Or Phead</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.VarOrPheadImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getVarOrPhead()
   * @generated
   */
  int VAR_OR_PHEAD = 98;

  /**
   * The feature id for the '<em><b>Var</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VAR_OR_PHEAD__VAR = 0;

  /**
   * The feature id for the '<em><b>Phead</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VAR_OR_PHEAD__PHEAD = 1;

  /**
   * The number of structural features of the '<em>Var Or Phead</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VAR_OR_PHEAD_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.VarOrAtomOrPStructImpl <em>Var Or Atom Or PStruct</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.VarOrAtomOrPStructImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getVarOrAtomOrPStruct()
   * @generated
   */
  int VAR_OR_ATOM_OR_PSTRUCT = 99;

  /**
   * The feature id for the '<em><b>Var</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VAR_OR_ATOM_OR_PSTRUCT__VAR = 0;

  /**
   * The feature id for the '<em><b>Psrtuct</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VAR_OR_ATOM_OR_PSTRUCT__PSRTUCT = 1;

  /**
   * The feature id for the '<em><b>Atom</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VAR_OR_ATOM_OR_PSTRUCT__ATOM = 2;

  /**
   * The number of structural features of the '<em>Var Or Atom Or PStruct</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VAR_OR_ATOM_OR_PSTRUCT_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.VariableImpl <em>Variable</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.VariableImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getVariable()
   * @generated
   */
  int VARIABLE = 100;

  /**
   * The feature id for the '<em><b>Guardvar</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VARIABLE__GUARDVAR = PATOM_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VARIABLE__NAME = PATOM_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Variable</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VARIABLE_FEATURE_COUNT = PATOM_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.TimeLimitImpl <em>Time Limit</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.TimeLimitImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getTimeLimit()
   * @generated
   */
  int TIME_LIMIT = 101;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TIME_LIMIT__NAME = 0;

  /**
   * The feature id for the '<em><b>Msec</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TIME_LIMIT__MSEC = 1;

  /**
   * The feature id for the '<em><b>Var</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TIME_LIMIT__VAR = 2;

  /**
   * The number of structural features of the '<em>Time Limit</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TIME_LIMIT_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.ComponentIPImpl <em>Component IP</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.ComponentIPImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getComponentIP()
   * @generated
   */
  int COMPONENT_IP = 102;

  /**
   * The feature id for the '<em><b>Host</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPONENT_IP__HOST = 0;

  /**
   * The feature id for the '<em><b>Port</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPONENT_IP__PORT = 1;

  /**
   * The number of structural features of the '<em>Component IP</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPONENT_IP_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.impl.MoveFileImpl <em>Move File</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.impl.MoveFileImpl
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getMoveFile()
   * @generated
   */
  int MOVE_FILE = 103;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MOVE_FILE__NAME = 0;

  /**
   * The feature id for the '<em><b>Fname</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MOVE_FILE__FNAME = 1;

  /**
   * The number of structural features of the '<em>Move File</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MOVE_FILE_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link it.unibo.xtext.qactor.WindowColor <em>Window Color</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see it.unibo.xtext.qactor.WindowColor
   * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getWindowColor()
   * @generated
   */
  int WINDOW_COLOR = 104;


  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.QActorSystem <em>QActor System</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>QActor System</em>'.
   * @see it.unibo.xtext.qactor.QActorSystem
   * @generated
   */
  EClass getQActorSystem();

  /**
   * Returns the meta object for the containment reference '{@link it.unibo.xtext.qactor.QActorSystem#getSpec <em>Spec</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Spec</em>'.
   * @see it.unibo.xtext.qactor.QActorSystem#getSpec()
   * @see #getQActorSystem()
   * @generated
   */
  EReference getQActorSystem_Spec();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.QActorSystemSpec <em>QActor System Spec</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>QActor System Spec</em>'.
   * @see it.unibo.xtext.qactor.QActorSystemSpec
   * @generated
   */
  EClass getQActorSystemSpec();

  /**
   * Returns the meta object for the attribute '{@link it.unibo.xtext.qactor.QActorSystemSpec#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see it.unibo.xtext.qactor.QActorSystemSpec#getName()
   * @see #getQActorSystemSpec()
   * @generated
   */
  EAttribute getQActorSystemSpec_Name();

  /**
   * Returns the meta object for the attribute '{@link it.unibo.xtext.qactor.QActorSystemSpec#isTesting <em>Testing</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Testing</em>'.
   * @see it.unibo.xtext.qactor.QActorSystemSpec#isTesting()
   * @see #getQActorSystemSpec()
   * @generated
   */
  EAttribute getQActorSystemSpec_Testing();

  /**
   * Returns the meta object for the containment reference list '{@link it.unibo.xtext.qactor.QActorSystemSpec#getMessage <em>Message</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Message</em>'.
   * @see it.unibo.xtext.qactor.QActorSystemSpec#getMessage()
   * @see #getQActorSystemSpec()
   * @generated
   */
  EReference getQActorSystemSpec_Message();

  /**
   * Returns the meta object for the containment reference list '{@link it.unibo.xtext.qactor.QActorSystemSpec#getContext <em>Context</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Context</em>'.
   * @see it.unibo.xtext.qactor.QActorSystemSpec#getContext()
   * @see #getQActorSystemSpec()
   * @generated
   */
  EReference getQActorSystemSpec_Context();

  /**
   * Returns the meta object for the containment reference list '{@link it.unibo.xtext.qactor.QActorSystemSpec#getActor <em>Actor</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Actor</em>'.
   * @see it.unibo.xtext.qactor.QActorSystemSpec#getActor()
   * @see #getQActorSystemSpec()
   * @generated
   */
  EReference getQActorSystemSpec_Actor();

  /**
   * Returns the meta object for the containment reference '{@link it.unibo.xtext.qactor.QActorSystemSpec#getRobot <em>Robot</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Robot</em>'.
   * @see it.unibo.xtext.qactor.QActorSystemSpec#getRobot()
   * @see #getQActorSystemSpec()
   * @generated
   */
  EReference getQActorSystemSpec_Robot();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.Robot <em>Robot</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Robot</em>'.
   * @see it.unibo.xtext.qactor.Robot
   * @generated
   */
  EClass getRobot();

  /**
   * Returns the meta object for the attribute '{@link it.unibo.xtext.qactor.Robot#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see it.unibo.xtext.qactor.Robot#getName()
   * @see #getRobot()
   * @generated
   */
  EAttribute getRobot_Name();

  /**
   * Returns the meta object for the containment reference '{@link it.unibo.xtext.qactor.Robot#getActor <em>Actor</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Actor</em>'.
   * @see it.unibo.xtext.qactor.Robot#getActor()
   * @see #getRobot()
   * @generated
   */
  EReference getRobot_Actor();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.Message <em>Message</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Message</em>'.
   * @see it.unibo.xtext.qactor.Message
   * @generated
   */
  EClass getMessage();

  /**
   * Returns the meta object for the attribute '{@link it.unibo.xtext.qactor.Message#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see it.unibo.xtext.qactor.Message#getName()
   * @see #getMessage()
   * @generated
   */
  EAttribute getMessage_Name();

  /**
   * Returns the meta object for the containment reference '{@link it.unibo.xtext.qactor.Message#getMsg <em>Msg</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Msg</em>'.
   * @see it.unibo.xtext.qactor.Message#getMsg()
   * @see #getMessage()
   * @generated
   */
  EReference getMessage_Msg();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.OutOnlyMessage <em>Out Only Message</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Out Only Message</em>'.
   * @see it.unibo.xtext.qactor.OutOnlyMessage
   * @generated
   */
  EClass getOutOnlyMessage();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.OutInMessage <em>Out In Message</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Out In Message</em>'.
   * @see it.unibo.xtext.qactor.OutInMessage
   * @generated
   */
  EClass getOutInMessage();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.Event <em>Event</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Event</em>'.
   * @see it.unibo.xtext.qactor.Event
   * @generated
   */
  EClass getEvent();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.Signal <em>Signal</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Signal</em>'.
   * @see it.unibo.xtext.qactor.Signal
   * @generated
   */
  EClass getSignal();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.Token <em>Token</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Token</em>'.
   * @see it.unibo.xtext.qactor.Token
   * @generated
   */
  EClass getToken();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.Dispatch <em>Dispatch</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Dispatch</em>'.
   * @see it.unibo.xtext.qactor.Dispatch
   * @generated
   */
  EClass getDispatch();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.Request <em>Request</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Request</em>'.
   * @see it.unibo.xtext.qactor.Request
   * @generated
   */
  EClass getRequest();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.Invitation <em>Invitation</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Invitation</em>'.
   * @see it.unibo.xtext.qactor.Invitation
   * @generated
   */
  EClass getInvitation();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.Context <em>Context</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Context</em>'.
   * @see it.unibo.xtext.qactor.Context
   * @generated
   */
  EClass getContext();

  /**
   * Returns the meta object for the attribute '{@link it.unibo.xtext.qactor.Context#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see it.unibo.xtext.qactor.Context#getName()
   * @see #getContext()
   * @generated
   */
  EAttribute getContext_Name();

  /**
   * Returns the meta object for the containment reference '{@link it.unibo.xtext.qactor.Context#getIp <em>Ip</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ip</em>'.
   * @see it.unibo.xtext.qactor.Context#getIp()
   * @see #getContext()
   * @generated
   */
  EReference getContext_Ip();

  /**
   * Returns the meta object for the attribute '{@link it.unibo.xtext.qactor.Context#isEnv <em>Env</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Env</em>'.
   * @see it.unibo.xtext.qactor.Context#isEnv()
   * @see #getContext()
   * @generated
   */
  EAttribute getContext_Env();

  /**
   * Returns the meta object for the attribute '{@link it.unibo.xtext.qactor.Context#getColor <em>Color</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Color</em>'.
   * @see it.unibo.xtext.qactor.Context#getColor()
   * @see #getContext()
   * @generated
   */
  EAttribute getContext_Color();

  /**
   * Returns the meta object for the attribute '{@link it.unibo.xtext.qactor.Context#isStandalone <em>Standalone</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Standalone</em>'.
   * @see it.unibo.xtext.qactor.Context#isStandalone()
   * @see #getContext()
   * @generated
   */
  EAttribute getContext_Standalone();

  /**
   * Returns the meta object for the attribute '{@link it.unibo.xtext.qactor.Context#isHttpserver <em>Httpserver</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Httpserver</em>'.
   * @see it.unibo.xtext.qactor.Context#isHttpserver()
   * @see #getContext()
   * @generated
   */
  EAttribute getContext_Httpserver();

  /**
   * Returns the meta object for the containment reference list '{@link it.unibo.xtext.qactor.Context#getHandler <em>Handler</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Handler</em>'.
   * @see it.unibo.xtext.qactor.Context#getHandler()
   * @see #getContext()
   * @generated
   */
  EReference getContext_Handler();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.QActor <em>QActor</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>QActor</em>'.
   * @see it.unibo.xtext.qactor.QActor
   * @generated
   */
  EClass getQActor();

  /**
   * Returns the meta object for the attribute '{@link it.unibo.xtext.qactor.QActor#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see it.unibo.xtext.qactor.QActor#getName()
   * @see #getQActor()
   * @generated
   */
  EAttribute getQActor_Name();

  /**
   * Returns the meta object for the reference '{@link it.unibo.xtext.qactor.QActor#getContext <em>Context</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Context</em>'.
   * @see it.unibo.xtext.qactor.QActor#getContext()
   * @see #getQActor()
   * @generated
   */
  EReference getQActor_Context();

  /**
   * Returns the meta object for the attribute '{@link it.unibo.xtext.qactor.QActor#isEnv <em>Env</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Env</em>'.
   * @see it.unibo.xtext.qactor.QActor#isEnv()
   * @see #getQActor()
   * @generated
   */
  EAttribute getQActor_Env();

  /**
   * Returns the meta object for the attribute '{@link it.unibo.xtext.qactor.QActor#getColor <em>Color</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Color</em>'.
   * @see it.unibo.xtext.qactor.QActor#getColor()
   * @see #getQActor()
   * @generated
   */
  EAttribute getQActor_Color();

  /**
   * Returns the meta object for the containment reference list '{@link it.unibo.xtext.qactor.QActor#getRules <em>Rules</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Rules</em>'.
   * @see it.unibo.xtext.qactor.QActor#getRules()
   * @see #getQActor()
   * @generated
   */
  EReference getQActor_Rules();

  /**
   * Returns the meta object for the containment reference list '{@link it.unibo.xtext.qactor.QActor#getData <em>Data</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Data</em>'.
   * @see it.unibo.xtext.qactor.QActor#getData()
   * @see #getQActor()
   * @generated
   */
  EReference getQActor_Data();

  /**
   * Returns the meta object for the containment reference list '{@link it.unibo.xtext.qactor.QActor#getAction <em>Action</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Action</em>'.
   * @see it.unibo.xtext.qactor.QActor#getAction()
   * @see #getQActor()
   * @generated
   */
  EReference getQActor_Action();

  /**
   * Returns the meta object for the containment reference list '{@link it.unibo.xtext.qactor.QActor#getPlans <em>Plans</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Plans</em>'.
   * @see it.unibo.xtext.qactor.QActor#getPlans()
   * @see #getQActor()
   * @generated
   */
  EReference getQActor_Plans();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.Rule <em>Rule</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Rule</em>'.
   * @see it.unibo.xtext.qactor.Rule
   * @generated
   */
  EClass getRule();

  /**
   * Returns the meta object for the containment reference '{@link it.unibo.xtext.qactor.Rule#getHead <em>Head</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Head</em>'.
   * @see it.unibo.xtext.qactor.Rule#getHead()
   * @see #getRule()
   * @generated
   */
  EReference getRule_Head();

  /**
   * Returns the meta object for the containment reference list '{@link it.unibo.xtext.qactor.Rule#getBody <em>Body</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Body</em>'.
   * @see it.unibo.xtext.qactor.Rule#getBody()
   * @see #getRule()
   * @generated
   */
  EReference getRule_Body();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.PHead <em>PHead</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>PHead</em>'.
   * @see it.unibo.xtext.qactor.PHead
   * @generated
   */
  EClass getPHead();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.PTerm <em>PTerm</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>PTerm</em>'.
   * @see it.unibo.xtext.qactor.PTerm
   * @generated
   */
  EClass getPTerm();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.PAtom <em>PAtom</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>PAtom</em>'.
   * @see it.unibo.xtext.qactor.PAtom
   * @generated
   */
  EClass getPAtom();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.PAtomString <em>PAtom String</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>PAtom String</em>'.
   * @see it.unibo.xtext.qactor.PAtomString
   * @generated
   */
  EClass getPAtomString();

  /**
   * Returns the meta object for the attribute '{@link it.unibo.xtext.qactor.PAtomString#getVal <em>Val</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Val</em>'.
   * @see it.unibo.xtext.qactor.PAtomString#getVal()
   * @see #getPAtomString()
   * @generated
   */
  EAttribute getPAtomString_Val();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.PAtomic <em>PAtomic</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>PAtomic</em>'.
   * @see it.unibo.xtext.qactor.PAtomic
   * @generated
   */
  EClass getPAtomic();

  /**
   * Returns the meta object for the attribute '{@link it.unibo.xtext.qactor.PAtomic#getVal <em>Val</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Val</em>'.
   * @see it.unibo.xtext.qactor.PAtomic#getVal()
   * @see #getPAtomic()
   * @generated
   */
  EAttribute getPAtomic_Val();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.PAtomNum <em>PAtom Num</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>PAtom Num</em>'.
   * @see it.unibo.xtext.qactor.PAtomNum
   * @generated
   */
  EClass getPAtomNum();

  /**
   * Returns the meta object for the attribute '{@link it.unibo.xtext.qactor.PAtomNum#getVal <em>Val</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Val</em>'.
   * @see it.unibo.xtext.qactor.PAtomNum#getVal()
   * @see #getPAtomNum()
   * @generated
   */
  EAttribute getPAtomNum_Val();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.PStruct <em>PStruct</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>PStruct</em>'.
   * @see it.unibo.xtext.qactor.PStruct
   * @generated
   */
  EClass getPStruct();

  /**
   * Returns the meta object for the attribute '{@link it.unibo.xtext.qactor.PStruct#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see it.unibo.xtext.qactor.PStruct#getName()
   * @see #getPStruct()
   * @generated
   */
  EAttribute getPStruct_Name();

  /**
   * Returns the meta object for the containment reference list '{@link it.unibo.xtext.qactor.PStruct#getMsgArg <em>Msg Arg</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Msg Arg</em>'.
   * @see it.unibo.xtext.qactor.PStruct#getMsgArg()
   * @see #getPStruct()
   * @generated
   */
  EReference getPStruct_MsgArg();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.PActorCall <em>PActor Call</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>PActor Call</em>'.
   * @see it.unibo.xtext.qactor.PActorCall
   * @generated
   */
  EClass getPActorCall();

  /**
   * Returns the meta object for the containment reference '{@link it.unibo.xtext.qactor.PActorCall#getBody <em>Body</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Body</em>'.
   * @see it.unibo.xtext.qactor.PActorCall#getBody()
   * @see #getPActorCall()
   * @generated
   */
  EReference getPActorCall_Body();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.PPredef <em>PPredef</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>PPredef</em>'.
   * @see it.unibo.xtext.qactor.PPredef
   * @generated
   */
  EClass getPPredef();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.PIs <em>PIs</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>PIs</em>'.
   * @see it.unibo.xtext.qactor.PIs
   * @generated
   */
  EClass getPIs();

  /**
   * Returns the meta object for the containment reference '{@link it.unibo.xtext.qactor.PIs#getVarout <em>Varout</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Varout</em>'.
   * @see it.unibo.xtext.qactor.PIs#getVarout()
   * @see #getPIs()
   * @generated
   */
  EReference getPIs_Varout();

  /**
   * Returns the meta object for the containment reference '{@link it.unibo.xtext.qactor.PIs#getVarin <em>Varin</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Varin</em>'.
   * @see it.unibo.xtext.qactor.PIs#getVarin()
   * @see #getPIs()
   * @generated
   */
  EReference getPIs_Varin();

  /**
   * Returns the meta object for the containment reference '{@link it.unibo.xtext.qactor.PIs#getNum <em>Num</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Num</em>'.
   * @see it.unibo.xtext.qactor.PIs#getNum()
   * @see #getPIs()
   * @generated
   */
  EReference getPIs_Num();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.PAtomCut <em>PAtom Cut</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>PAtom Cut</em>'.
   * @see it.unibo.xtext.qactor.PAtomCut
   * @generated
   */
  EClass getPAtomCut();

  /**
   * Returns the meta object for the attribute '{@link it.unibo.xtext.qactor.PAtomCut#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see it.unibo.xtext.qactor.PAtomCut#getName()
   * @see #getPAtomCut()
   * @generated
   */
  EAttribute getPAtomCut_Name();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.Data <em>Data</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Data</em>'.
   * @see it.unibo.xtext.qactor.Data
   * @generated
   */
  EClass getData();

  /**
   * Returns the meta object for the attribute '{@link it.unibo.xtext.qactor.Data#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see it.unibo.xtext.qactor.Data#getName()
   * @see #getData()
   * @generated
   */
  EAttribute getData_Name();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.IntegerData <em>Integer Data</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Integer Data</em>'.
   * @see it.unibo.xtext.qactor.IntegerData
   * @generated
   */
  EClass getIntegerData();

  /**
   * Returns the meta object for the attribute '{@link it.unibo.xtext.qactor.IntegerData#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see it.unibo.xtext.qactor.IntegerData#getValue()
   * @see #getIntegerData()
   * @generated
   */
  EAttribute getIntegerData_Value();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.StringData <em>String Data</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>String Data</em>'.
   * @see it.unibo.xtext.qactor.StringData
   * @generated
   */
  EClass getStringData();

  /**
   * Returns the meta object for the attribute '{@link it.unibo.xtext.qactor.StringData#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see it.unibo.xtext.qactor.StringData#getValue()
   * @see #getStringData()
   * @generated
   */
  EAttribute getStringData_Value();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.Action <em>Action</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Action</em>'.
   * @see it.unibo.xtext.qactor.Action
   * @generated
   */
  EClass getAction();

  /**
   * Returns the meta object for the attribute '{@link it.unibo.xtext.qactor.Action#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see it.unibo.xtext.qactor.Action#getName()
   * @see #getAction()
   * @generated
   */
  EAttribute getAction_Name();

  /**
   * Returns the meta object for the attribute '{@link it.unibo.xtext.qactor.Action#isUndoable <em>Undoable</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Undoable</em>'.
   * @see it.unibo.xtext.qactor.Action#isUndoable()
   * @see #getAction()
   * @generated
   */
  EAttribute getAction_Undoable();

  /**
   * Returns the meta object for the attribute '{@link it.unibo.xtext.qactor.Action#getMsec <em>Msec</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Msec</em>'.
   * @see it.unibo.xtext.qactor.Action#getMsec()
   * @see #getAction()
   * @generated
   */
  EAttribute getAction_Msec();

  /**
   * Returns the meta object for the containment reference '{@link it.unibo.xtext.qactor.Action#getArg <em>Arg</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Arg</em>'.
   * @see it.unibo.xtext.qactor.Action#getArg()
   * @see #getAction()
   * @generated
   */
  EReference getAction_Arg();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.Plan <em>Plan</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Plan</em>'.
   * @see it.unibo.xtext.qactor.Plan
   * @generated
   */
  EClass getPlan();

  /**
   * Returns the meta object for the attribute '{@link it.unibo.xtext.qactor.Plan#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see it.unibo.xtext.qactor.Plan#getName()
   * @see #getPlan()
   * @generated
   */
  EAttribute getPlan_Name();

  /**
   * Returns the meta object for the attribute '{@link it.unibo.xtext.qactor.Plan#isNormal <em>Normal</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Normal</em>'.
   * @see it.unibo.xtext.qactor.Plan#isNormal()
   * @see #getPlan()
   * @generated
   */
  EAttribute getPlan_Normal();

  /**
   * Returns the meta object for the attribute '{@link it.unibo.xtext.qactor.Plan#isResume <em>Resume</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Resume</em>'.
   * @see it.unibo.xtext.qactor.Plan#isResume()
   * @see #getPlan()
   * @generated
   */
  EAttribute getPlan_Resume();

  /**
   * Returns the meta object for the containment reference list '{@link it.unibo.xtext.qactor.Plan#getAction <em>Action</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Action</em>'.
   * @see it.unibo.xtext.qactor.Plan#getAction()
   * @see #getPlan()
   * @generated
   */
  EReference getPlan_Action();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.PlanAction <em>Plan Action</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Plan Action</em>'.
   * @see it.unibo.xtext.qactor.PlanAction
   * @generated
   */
  EClass getPlanAction();

  /**
   * Returns the meta object for the containment reference '{@link it.unibo.xtext.qactor.PlanAction#getGuard <em>Guard</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Guard</em>'.
   * @see it.unibo.xtext.qactor.PlanAction#getGuard()
   * @see #getPlanAction()
   * @generated
   */
  EReference getPlanAction_Guard();

  /**
   * Returns the meta object for the containment reference '{@link it.unibo.xtext.qactor.PlanAction#getMove <em>Move</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Move</em>'.
   * @see it.unibo.xtext.qactor.PlanAction#getMove()
   * @see #getPlanAction()
   * @generated
   */
  EReference getPlanAction_Move();

  /**
   * Returns the meta object for the containment reference '{@link it.unibo.xtext.qactor.PlanAction#getReact <em>React</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>React</em>'.
   * @see it.unibo.xtext.qactor.PlanAction#getReact()
   * @see #getPlanAction()
   * @generated
   */
  EReference getPlanAction_React();

  /**
   * Returns the meta object for the containment reference '{@link it.unibo.xtext.qactor.PlanAction#getElsemove <em>Elsemove</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Elsemove</em>'.
   * @see it.unibo.xtext.qactor.PlanAction#getElsemove()
   * @see #getPlanAction()
   * @generated
   */
  EReference getPlanAction_Elsemove();

  /**
   * Returns the meta object for the containment reference '{@link it.unibo.xtext.qactor.PlanAction#getElsereact <em>Elsereact</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Elsereact</em>'.
   * @see it.unibo.xtext.qactor.PlanAction#getElsereact()
   * @see #getPlanAction()
   * @generated
   */
  EReference getPlanAction_Elsereact();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.Guard <em>Guard</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Guard</em>'.
   * @see it.unibo.xtext.qactor.Guard
   * @generated
   */
  EClass getGuard();

  /**
   * Returns the meta object for the attribute '{@link it.unibo.xtext.qactor.Guard#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see it.unibo.xtext.qactor.Guard#getName()
   * @see #getGuard()
   * @generated
   */
  EAttribute getGuard_Name();

  /**
   * Returns the meta object for the attribute '{@link it.unibo.xtext.qactor.Guard#isNot <em>Not</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Not</em>'.
   * @see it.unibo.xtext.qactor.Guard#isNot()
   * @see #getGuard()
   * @generated
   */
  EAttribute getGuard_Not();

  /**
   * Returns the meta object for the containment reference '{@link it.unibo.xtext.qactor.Guard#getGuardspec <em>Guardspec</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Guardspec</em>'.
   * @see it.unibo.xtext.qactor.Guard#getGuardspec()
   * @see #getGuard()
   * @generated
   */
  EReference getGuard_Guardspec();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.GuardPredicate <em>Guard Predicate</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Guard Predicate</em>'.
   * @see it.unibo.xtext.qactor.GuardPredicate
   * @generated
   */
  EClass getGuardPredicate();

  /**
   * Returns the meta object for the attribute '{@link it.unibo.xtext.qactor.GuardPredicate#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see it.unibo.xtext.qactor.GuardPredicate#getName()
   * @see #getGuardPredicate()
   * @generated
   */
  EAttribute getGuardPredicate_Name();

  /**
   * Returns the meta object for the containment reference '{@link it.unibo.xtext.qactor.GuardPredicate#getPred <em>Pred</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Pred</em>'.
   * @see it.unibo.xtext.qactor.GuardPredicate#getPred()
   * @see #getGuardPredicate()
   * @generated
   */
  EReference getGuardPredicate_Pred();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.GuardPredicateRemovable <em>Guard Predicate Removable</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Guard Predicate Removable</em>'.
   * @see it.unibo.xtext.qactor.GuardPredicateRemovable
   * @generated
   */
  EClass getGuardPredicateRemovable();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.GuardPredicateStable <em>Guard Predicate Stable</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Guard Predicate Stable</em>'.
   * @see it.unibo.xtext.qactor.GuardPredicateStable
   * @generated
   */
  EClass getGuardPredicateStable();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.Move <em>Move</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Move</em>'.
   * @see it.unibo.xtext.qactor.Move
   * @generated
   */
  EClass getMove();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.ActionMove <em>Action Move</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Action Move</em>'.
   * @see it.unibo.xtext.qactor.ActionMove
   * @generated
   */
  EClass getActionMove();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.ExecuteAction <em>Execute Action</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Execute Action</em>'.
   * @see it.unibo.xtext.qactor.ExecuteAction
   * @generated
   */
  EClass getExecuteAction();

  /**
   * Returns the meta object for the reference '{@link it.unibo.xtext.qactor.ExecuteAction#getAction <em>Action</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Action</em>'.
   * @see it.unibo.xtext.qactor.ExecuteAction#getAction()
   * @see #getExecuteAction()
   * @generated
   */
  EReference getExecuteAction_Action();

  /**
   * Returns the meta object for the containment reference '{@link it.unibo.xtext.qactor.ExecuteAction#getArg <em>Arg</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Arg</em>'.
   * @see it.unibo.xtext.qactor.ExecuteAction#getArg()
   * @see #getExecuteAction()
   * @generated
   */
  EReference getExecuteAction_Arg();

  /**
   * Returns the meta object for the containment reference '{@link it.unibo.xtext.qactor.ExecuteAction#getSentence <em>Sentence</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Sentence</em>'.
   * @see it.unibo.xtext.qactor.ExecuteAction#getSentence()
   * @see #getExecuteAction()
   * @generated
   */
  EReference getExecuteAction_Sentence();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.SolveGoal <em>Solve Goal</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Solve Goal</em>'.
   * @see it.unibo.xtext.qactor.SolveGoal
   * @generated
   */
  EClass getSolveGoal();

  /**
   * Returns the meta object for the attribute '{@link it.unibo.xtext.qactor.SolveGoal#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see it.unibo.xtext.qactor.SolveGoal#getName()
   * @see #getSolveGoal()
   * @generated
   */
  EAttribute getSolveGoal_Name();

  /**
   * Returns the meta object for the containment reference '{@link it.unibo.xtext.qactor.SolveGoal#getGoal <em>Goal</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Goal</em>'.
   * @see it.unibo.xtext.qactor.SolveGoal#getGoal()
   * @see #getSolveGoal()
   * @generated
   */
  EReference getSolveGoal_Goal();

  /**
   * Returns the meta object for the containment reference '{@link it.unibo.xtext.qactor.SolveGoal#getDuration <em>Duration</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Duration</em>'.
   * @see it.unibo.xtext.qactor.SolveGoal#getDuration()
   * @see #getSolveGoal()
   * @generated
   */
  EReference getSolveGoal_Duration();

  /**
   * Returns the meta object for the reference '{@link it.unibo.xtext.qactor.SolveGoal#getPlan <em>Plan</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Plan</em>'.
   * @see it.unibo.xtext.qactor.SolveGoal#getPlan()
   * @see #getSolveGoal()
   * @generated
   */
  EReference getSolveGoal_Plan();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.Demo <em>Demo</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Demo</em>'.
   * @see it.unibo.xtext.qactor.Demo
   * @generated
   */
  EClass getDemo();

  /**
   * Returns the meta object for the attribute '{@link it.unibo.xtext.qactor.Demo#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see it.unibo.xtext.qactor.Demo#getName()
   * @see #getDemo()
   * @generated
   */
  EAttribute getDemo_Name();

  /**
   * Returns the meta object for the containment reference '{@link it.unibo.xtext.qactor.Demo#getGoal <em>Goal</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Goal</em>'.
   * @see it.unibo.xtext.qactor.Demo#getGoal()
   * @see #getDemo()
   * @generated
   */
  EReference getDemo_Goal();

  /**
   * Returns the meta object for the reference '{@link it.unibo.xtext.qactor.Demo#getPlan <em>Plan</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Plan</em>'.
   * @see it.unibo.xtext.qactor.Demo#getPlan()
   * @see #getDemo()
   * @generated
   */
  EReference getDemo_Plan();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.ActorOp <em>Actor Op</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Actor Op</em>'.
   * @see it.unibo.xtext.qactor.ActorOp
   * @generated
   */
  EClass getActorOp();

  /**
   * Returns the meta object for the attribute '{@link it.unibo.xtext.qactor.ActorOp#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see it.unibo.xtext.qactor.ActorOp#getName()
   * @see #getActorOp()
   * @generated
   */
  EAttribute getActorOp_Name();

  /**
   * Returns the meta object for the containment reference '{@link it.unibo.xtext.qactor.ActorOp#getGoal <em>Goal</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Goal</em>'.
   * @see it.unibo.xtext.qactor.ActorOp#getGoal()
   * @see #getActorOp()
   * @generated
   */
  EReference getActorOp_Goal();

  /**
   * Returns the meta object for the reference '{@link it.unibo.xtext.qactor.ActorOp#getPlan <em>Plan</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Plan</em>'.
   * @see it.unibo.xtext.qactor.ActorOp#getPlan()
   * @see #getActorOp()
   * @generated
   */
  EReference getActorOp_Plan();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.BasicRobotMove <em>Basic Robot Move</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Basic Robot Move</em>'.
   * @see it.unibo.xtext.qactor.BasicRobotMove
   * @generated
   */
  EClass getBasicRobotMove();

  /**
   * Returns the meta object for the attribute '{@link it.unibo.xtext.qactor.BasicRobotMove#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see it.unibo.xtext.qactor.BasicRobotMove#getName()
   * @see #getBasicRobotMove()
   * @generated
   */
  EAttribute getBasicRobotMove_Name();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.BasicMove <em>Basic Move</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Basic Move</em>'.
   * @see it.unibo.xtext.qactor.BasicMove
   * @generated
   */
  EClass getBasicMove();

  /**
   * Returns the meta object for the attribute '{@link it.unibo.xtext.qactor.BasicMove#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see it.unibo.xtext.qactor.BasicMove#getName()
   * @see #getBasicMove()
   * @generated
   */
  EAttribute getBasicMove_Name();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.Print <em>Print</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Print</em>'.
   * @see it.unibo.xtext.qactor.Print
   * @generated
   */
  EClass getPrint();

  /**
   * Returns the meta object for the containment reference '{@link it.unibo.xtext.qactor.Print#getArgs <em>Args</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Args</em>'.
   * @see it.unibo.xtext.qactor.Print#getArgs()
   * @see #getPrint()
   * @generated
   */
  EReference getPrint_Args();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.PrintCurrentEvent <em>Print Current Event</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Print Current Event</em>'.
   * @see it.unibo.xtext.qactor.PrintCurrentEvent
   * @generated
   */
  EClass getPrintCurrentEvent();

  /**
   * Returns the meta object for the attribute '{@link it.unibo.xtext.qactor.PrintCurrentEvent#isMemo <em>Memo</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Memo</em>'.
   * @see it.unibo.xtext.qactor.PrintCurrentEvent#isMemo()
   * @see #getPrintCurrentEvent()
   * @generated
   */
  EAttribute getPrintCurrentEvent_Memo();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.PrintCurrentMessage <em>Print Current Message</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Print Current Message</em>'.
   * @see it.unibo.xtext.qactor.PrintCurrentMessage
   * @generated
   */
  EClass getPrintCurrentMessage();

  /**
   * Returns the meta object for the attribute '{@link it.unibo.xtext.qactor.PrintCurrentMessage#isMemo <em>Memo</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Memo</em>'.
   * @see it.unibo.xtext.qactor.PrintCurrentMessage#isMemo()
   * @see #getPrintCurrentMessage()
   * @generated
   */
  EAttribute getPrintCurrentMessage_Memo();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.MemoCurrentEvent <em>Memo Current Event</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Memo Current Event</em>'.
   * @see it.unibo.xtext.qactor.MemoCurrentEvent
   * @generated
   */
  EClass getMemoCurrentEvent();

  /**
   * Returns the meta object for the attribute '{@link it.unibo.xtext.qactor.MemoCurrentEvent#isLastonly <em>Lastonly</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Lastonly</em>'.
   * @see it.unibo.xtext.qactor.MemoCurrentEvent#isLastonly()
   * @see #getMemoCurrentEvent()
   * @generated
   */
  EAttribute getMemoCurrentEvent_Lastonly();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.MemoCurrentMessage <em>Memo Current Message</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Memo Current Message</em>'.
   * @see it.unibo.xtext.qactor.MemoCurrentMessage
   * @generated
   */
  EClass getMemoCurrentMessage();

  /**
   * Returns the meta object for the attribute '{@link it.unibo.xtext.qactor.MemoCurrentMessage#isLastonly <em>Lastonly</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Lastonly</em>'.
   * @see it.unibo.xtext.qactor.MemoCurrentMessage#isLastonly()
   * @see #getMemoCurrentMessage()
   * @generated
   */
  EAttribute getMemoCurrentMessage_Lastonly();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.PlanMove <em>Plan Move</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Plan Move</em>'.
   * @see it.unibo.xtext.qactor.PlanMove
   * @generated
   */
  EClass getPlanMove();

  /**
   * Returns the meta object for the attribute '{@link it.unibo.xtext.qactor.PlanMove#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see it.unibo.xtext.qactor.PlanMove#getName()
   * @see #getPlanMove()
   * @generated
   */
  EAttribute getPlanMove_Name();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.GetActivationEvent <em>Get Activation Event</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Get Activation Event</em>'.
   * @see it.unibo.xtext.qactor.GetActivationEvent
   * @generated
   */
  EClass getGetActivationEvent();

  /**
   * Returns the meta object for the containment reference '{@link it.unibo.xtext.qactor.GetActivationEvent#getVar <em>Var</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Var</em>'.
   * @see it.unibo.xtext.qactor.GetActivationEvent#getVar()
   * @see #getGetActivationEvent()
   * @generated
   */
  EReference getGetActivationEvent_Var();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.GetSensedEvent <em>Get Sensed Event</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Get Sensed Event</em>'.
   * @see it.unibo.xtext.qactor.GetSensedEvent
   * @generated
   */
  EClass getGetSensedEvent();

  /**
   * Returns the meta object for the containment reference '{@link it.unibo.xtext.qactor.GetSensedEvent#getVar <em>Var</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Var</em>'.
   * @see it.unibo.xtext.qactor.GetSensedEvent#getVar()
   * @see #getGetSensedEvent()
   * @generated
   */
  EReference getGetSensedEvent_Var();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.LoadPlan <em>Load Plan</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Load Plan</em>'.
   * @see it.unibo.xtext.qactor.LoadPlan
   * @generated
   */
  EClass getLoadPlan();

  /**
   * Returns the meta object for the containment reference '{@link it.unibo.xtext.qactor.LoadPlan#getFname <em>Fname</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Fname</em>'.
   * @see it.unibo.xtext.qactor.LoadPlan#getFname()
   * @see #getLoadPlan()
   * @generated
   */
  EReference getLoadPlan_Fname();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.RunPlan <em>Run Plan</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Run Plan</em>'.
   * @see it.unibo.xtext.qactor.RunPlan
   * @generated
   */
  EClass getRunPlan();

  /**
   * Returns the meta object for the containment reference '{@link it.unibo.xtext.qactor.RunPlan#getPlainid <em>Plainid</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Plainid</em>'.
   * @see it.unibo.xtext.qactor.RunPlan#getPlainid()
   * @see #getRunPlan()
   * @generated
   */
  EReference getRunPlan_Plainid();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.ResumePlan <em>Resume Plan</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Resume Plan</em>'.
   * @see it.unibo.xtext.qactor.ResumePlan
   * @generated
   */
  EClass getResumePlan();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.SuspendPlan <em>Suspend Plan</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Suspend Plan</em>'.
   * @see it.unibo.xtext.qactor.SuspendPlan
   * @generated
   */
  EClass getSuspendPlan();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.RepeatPlan <em>Repeat Plan</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Repeat Plan</em>'.
   * @see it.unibo.xtext.qactor.RepeatPlan
   * @generated
   */
  EClass getRepeatPlan();

  /**
   * Returns the meta object for the attribute '{@link it.unibo.xtext.qactor.RepeatPlan#getNiter <em>Niter</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Niter</em>'.
   * @see it.unibo.xtext.qactor.RepeatPlan#getNiter()
   * @see #getRepeatPlan()
   * @generated
   */
  EAttribute getRepeatPlan_Niter();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.SwitchPlan <em>Switch Plan</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Switch Plan</em>'.
   * @see it.unibo.xtext.qactor.SwitchPlan
   * @generated
   */
  EClass getSwitchPlan();

  /**
   * Returns the meta object for the reference '{@link it.unibo.xtext.qactor.SwitchPlan#getPlan <em>Plan</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Plan</em>'.
   * @see it.unibo.xtext.qactor.SwitchPlan#getPlan()
   * @see #getSwitchPlan()
   * @generated
   */
  EReference getSwitchPlan_Plan();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.EndPlan <em>End Plan</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>End Plan</em>'.
   * @see it.unibo.xtext.qactor.EndPlan
   * @generated
   */
  EClass getEndPlan();

  /**
   * Returns the meta object for the attribute '{@link it.unibo.xtext.qactor.EndPlan#getMsg <em>Msg</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Msg</em>'.
   * @see it.unibo.xtext.qactor.EndPlan#getMsg()
   * @see #getEndPlan()
   * @generated
   */
  EAttribute getEndPlan_Msg();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.EndActor <em>End Actor</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>End Actor</em>'.
   * @see it.unibo.xtext.qactor.EndActor
   * @generated
   */
  EClass getEndActor();

  /**
   * Returns the meta object for the attribute '{@link it.unibo.xtext.qactor.EndActor#getMsg <em>Msg</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Msg</em>'.
   * @see it.unibo.xtext.qactor.EndActor#getMsg()
   * @see #getEndActor()
   * @generated
   */
  EAttribute getEndActor_Msg();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.GuardMove <em>Guard Move</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Guard Move</em>'.
   * @see it.unibo.xtext.qactor.GuardMove
   * @generated
   */
  EClass getGuardMove();

  /**
   * Returns the meta object for the attribute '{@link it.unibo.xtext.qactor.GuardMove#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see it.unibo.xtext.qactor.GuardMove#getName()
   * @see #getGuardMove()
   * @generated
   */
  EAttribute getGuardMove_Name();

  /**
   * Returns the meta object for the containment reference '{@link it.unibo.xtext.qactor.GuardMove#getRule <em>Rule</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Rule</em>'.
   * @see it.unibo.xtext.qactor.GuardMove#getRule()
   * @see #getGuardMove()
   * @generated
   */
  EReference getGuardMove_Rule();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.AddRule <em>Add Rule</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Add Rule</em>'.
   * @see it.unibo.xtext.qactor.AddRule
   * @generated
   */
  EClass getAddRule();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.RemoveRule <em>Remove Rule</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Remove Rule</em>'.
   * @see it.unibo.xtext.qactor.RemoveRule
   * @generated
   */
  EClass getRemoveRule();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.MessageMove <em>Message Move</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Message Move</em>'.
   * @see it.unibo.xtext.qactor.MessageMove
   * @generated
   */
  EClass getMessageMove();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.SendDispatch <em>Send Dispatch</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Send Dispatch</em>'.
   * @see it.unibo.xtext.qactor.SendDispatch
   * @generated
   */
  EClass getSendDispatch();

  /**
   * Returns the meta object for the attribute '{@link it.unibo.xtext.qactor.SendDispatch#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see it.unibo.xtext.qactor.SendDispatch#getName()
   * @see #getSendDispatch()
   * @generated
   */
  EAttribute getSendDispatch_Name();

  /**
   * Returns the meta object for the containment reference '{@link it.unibo.xtext.qactor.SendDispatch#getDest <em>Dest</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Dest</em>'.
   * @see it.unibo.xtext.qactor.SendDispatch#getDest()
   * @see #getSendDispatch()
   * @generated
   */
  EReference getSendDispatch_Dest();

  /**
   * Returns the meta object for the reference '{@link it.unibo.xtext.qactor.SendDispatch#getMsgref <em>Msgref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Msgref</em>'.
   * @see it.unibo.xtext.qactor.SendDispatch#getMsgref()
   * @see #getSendDispatch()
   * @generated
   */
  EReference getSendDispatch_Msgref();

  /**
   * Returns the meta object for the containment reference '{@link it.unibo.xtext.qactor.SendDispatch#getVal <em>Val</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Val</em>'.
   * @see it.unibo.xtext.qactor.SendDispatch#getVal()
   * @see #getSendDispatch()
   * @generated
   */
  EReference getSendDispatch_Val();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.SendRequest <em>Send Request</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Send Request</em>'.
   * @see it.unibo.xtext.qactor.SendRequest
   * @generated
   */
  EClass getSendRequest();

  /**
   * Returns the meta object for the attribute '{@link it.unibo.xtext.qactor.SendRequest#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see it.unibo.xtext.qactor.SendRequest#getName()
   * @see #getSendRequest()
   * @generated
   */
  EAttribute getSendRequest_Name();

  /**
   * Returns the meta object for the containment reference '{@link it.unibo.xtext.qactor.SendRequest#getDest <em>Dest</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Dest</em>'.
   * @see it.unibo.xtext.qactor.SendRequest#getDest()
   * @see #getSendRequest()
   * @generated
   */
  EReference getSendRequest_Dest();

  /**
   * Returns the meta object for the reference '{@link it.unibo.xtext.qactor.SendRequest#getMsgref <em>Msgref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Msgref</em>'.
   * @see it.unibo.xtext.qactor.SendRequest#getMsgref()
   * @see #getSendRequest()
   * @generated
   */
  EReference getSendRequest_Msgref();

  /**
   * Returns the meta object for the containment reference '{@link it.unibo.xtext.qactor.SendRequest#getVal <em>Val</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Val</em>'.
   * @see it.unibo.xtext.qactor.SendRequest#getVal()
   * @see #getSendRequest()
   * @generated
   */
  EReference getSendRequest_Val();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.ReplyToCaller <em>Reply To Caller</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Reply To Caller</em>'.
   * @see it.unibo.xtext.qactor.ReplyToCaller
   * @generated
   */
  EClass getReplyToCaller();

  /**
   * Returns the meta object for the attribute '{@link it.unibo.xtext.qactor.ReplyToCaller#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see it.unibo.xtext.qactor.ReplyToCaller#getName()
   * @see #getReplyToCaller()
   * @generated
   */
  EAttribute getReplyToCaller_Name();

  /**
   * Returns the meta object for the reference '{@link it.unibo.xtext.qactor.ReplyToCaller#getMsgref <em>Msgref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Msgref</em>'.
   * @see it.unibo.xtext.qactor.ReplyToCaller#getMsgref()
   * @see #getReplyToCaller()
   * @generated
   */
  EReference getReplyToCaller_Msgref();

  /**
   * Returns the meta object for the containment reference '{@link it.unibo.xtext.qactor.ReplyToCaller#getVal <em>Val</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Val</em>'.
   * @see it.unibo.xtext.qactor.ReplyToCaller#getVal()
   * @see #getReplyToCaller()
   * @generated
   */
  EReference getReplyToCaller_Val();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.ReceiveMsg <em>Receive Msg</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Receive Msg</em>'.
   * @see it.unibo.xtext.qactor.ReceiveMsg
   * @generated
   */
  EClass getReceiveMsg();

  /**
   * Returns the meta object for the attribute '{@link it.unibo.xtext.qactor.ReceiveMsg#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see it.unibo.xtext.qactor.ReceiveMsg#getName()
   * @see #getReceiveMsg()
   * @generated
   */
  EAttribute getReceiveMsg_Name();

  /**
   * Returns the meta object for the containment reference '{@link it.unibo.xtext.qactor.ReceiveMsg#getDuration <em>Duration</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Duration</em>'.
   * @see it.unibo.xtext.qactor.ReceiveMsg#getDuration()
   * @see #getReceiveMsg()
   * @generated
   */
  EReference getReceiveMsg_Duration();

  /**
   * Returns the meta object for the containment reference '{@link it.unibo.xtext.qactor.ReceiveMsg#getSpec <em>Spec</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Spec</em>'.
   * @see it.unibo.xtext.qactor.ReceiveMsg#getSpec()
   * @see #getReceiveMsg()
   * @generated
   */
  EReference getReceiveMsg_Spec();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.MsgSpec <em>Msg Spec</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Msg Spec</em>'.
   * @see it.unibo.xtext.qactor.MsgSpec
   * @generated
   */
  EClass getMsgSpec();

  /**
   * Returns the meta object for the reference '{@link it.unibo.xtext.qactor.MsgSpec#getMsg <em>Msg</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Msg</em>'.
   * @see it.unibo.xtext.qactor.MsgSpec#getMsg()
   * @see #getMsgSpec()
   * @generated
   */
  EReference getMsgSpec_Msg();

  /**
   * Returns the meta object for the containment reference '{@link it.unibo.xtext.qactor.MsgSpec#getSender <em>Sender</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Sender</em>'.
   * @see it.unibo.xtext.qactor.MsgSpec#getSender()
   * @see #getMsgSpec()
   * @generated
   */
  EReference getMsgSpec_Sender();

  /**
   * Returns the meta object for the containment reference '{@link it.unibo.xtext.qactor.MsgSpec#getContent <em>Content</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Content</em>'.
   * @see it.unibo.xtext.qactor.MsgSpec#getContent()
   * @see #getMsgSpec()
   * @generated
   */
  EReference getMsgSpec_Content();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.OnReceiveMsg <em>On Receive Msg</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>On Receive Msg</em>'.
   * @see it.unibo.xtext.qactor.OnReceiveMsg
   * @generated
   */
  EClass getOnReceiveMsg();

  /**
   * Returns the meta object for the attribute '{@link it.unibo.xtext.qactor.OnReceiveMsg#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see it.unibo.xtext.qactor.OnReceiveMsg#getName()
   * @see #getOnReceiveMsg()
   * @generated
   */
  EAttribute getOnReceiveMsg_Name();

  /**
   * Returns the meta object for the containment reference '{@link it.unibo.xtext.qactor.OnReceiveMsg#getMsgid <em>Msgid</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Msgid</em>'.
   * @see it.unibo.xtext.qactor.OnReceiveMsg#getMsgid()
   * @see #getOnReceiveMsg()
   * @generated
   */
  EReference getOnReceiveMsg_Msgid();

  /**
   * Returns the meta object for the containment reference '{@link it.unibo.xtext.qactor.OnReceiveMsg#getMsgtype <em>Msgtype</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Msgtype</em>'.
   * @see it.unibo.xtext.qactor.OnReceiveMsg#getMsgtype()
   * @see #getOnReceiveMsg()
   * @generated
   */
  EReference getOnReceiveMsg_Msgtype();

  /**
   * Returns the meta object for the containment reference '{@link it.unibo.xtext.qactor.OnReceiveMsg#getMsgsender <em>Msgsender</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Msgsender</em>'.
   * @see it.unibo.xtext.qactor.OnReceiveMsg#getMsgsender()
   * @see #getOnReceiveMsg()
   * @generated
   */
  EReference getOnReceiveMsg_Msgsender();

  /**
   * Returns the meta object for the containment reference '{@link it.unibo.xtext.qactor.OnReceiveMsg#getMsgreceiver <em>Msgreceiver</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Msgreceiver</em>'.
   * @see it.unibo.xtext.qactor.OnReceiveMsg#getMsgreceiver()
   * @see #getOnReceiveMsg()
   * @generated
   */
  EReference getOnReceiveMsg_Msgreceiver();

  /**
   * Returns the meta object for the containment reference '{@link it.unibo.xtext.qactor.OnReceiveMsg#getMsgcontent <em>Msgcontent</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Msgcontent</em>'.
   * @see it.unibo.xtext.qactor.OnReceiveMsg#getMsgcontent()
   * @see #getOnReceiveMsg()
   * @generated
   */
  EReference getOnReceiveMsg_Msgcontent();

  /**
   * Returns the meta object for the containment reference '{@link it.unibo.xtext.qactor.OnReceiveMsg#getMsgseqnum <em>Msgseqnum</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Msgseqnum</em>'.
   * @see it.unibo.xtext.qactor.OnReceiveMsg#getMsgseqnum()
   * @see #getOnReceiveMsg()
   * @generated
   */
  EReference getOnReceiveMsg_Msgseqnum();

  /**
   * Returns the meta object for the containment reference '{@link it.unibo.xtext.qactor.OnReceiveMsg#getDuration <em>Duration</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Duration</em>'.
   * @see it.unibo.xtext.qactor.OnReceiveMsg#getDuration()
   * @see #getOnReceiveMsg()
   * @generated
   */
  EReference getOnReceiveMsg_Duration();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.MsgSelect <em>Msg Select</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Msg Select</em>'.
   * @see it.unibo.xtext.qactor.MsgSelect
   * @generated
   */
  EClass getMsgSelect();

  /**
   * Returns the meta object for the attribute '{@link it.unibo.xtext.qactor.MsgSelect#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see it.unibo.xtext.qactor.MsgSelect#getName()
   * @see #getMsgSelect()
   * @generated
   */
  EAttribute getMsgSelect_Name();

  /**
   * Returns the meta object for the containment reference '{@link it.unibo.xtext.qactor.MsgSelect#getDuration <em>Duration</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Duration</em>'.
   * @see it.unibo.xtext.qactor.MsgSelect#getDuration()
   * @see #getMsgSelect()
   * @generated
   */
  EReference getMsgSelect_Duration();

  /**
   * Returns the meta object for the reference list '{@link it.unibo.xtext.qactor.MsgSelect#getMessages <em>Messages</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference list '<em>Messages</em>'.
   * @see it.unibo.xtext.qactor.MsgSelect#getMessages()
   * @see #getMsgSelect()
   * @generated
   */
  EReference getMsgSelect_Messages();

  /**
   * Returns the meta object for the reference list '{@link it.unibo.xtext.qactor.MsgSelect#getPlans <em>Plans</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference list '<em>Plans</em>'.
   * @see it.unibo.xtext.qactor.MsgSelect#getPlans()
   * @see #getMsgSelect()
   * @generated
   */
  EReference getMsgSelect_Plans();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.RaiseEvent <em>Raise Event</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Raise Event</em>'.
   * @see it.unibo.xtext.qactor.RaiseEvent
   * @generated
   */
  EClass getRaiseEvent();

  /**
   * Returns the meta object for the attribute '{@link it.unibo.xtext.qactor.RaiseEvent#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see it.unibo.xtext.qactor.RaiseEvent#getName()
   * @see #getRaiseEvent()
   * @generated
   */
  EAttribute getRaiseEvent_Name();

  /**
   * Returns the meta object for the reference '{@link it.unibo.xtext.qactor.RaiseEvent#getEv <em>Ev</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Ev</em>'.
   * @see it.unibo.xtext.qactor.RaiseEvent#getEv()
   * @see #getRaiseEvent()
   * @generated
   */
  EReference getRaiseEvent_Ev();

  /**
   * Returns the meta object for the containment reference '{@link it.unibo.xtext.qactor.RaiseEvent#getContent <em>Content</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Content</em>'.
   * @see it.unibo.xtext.qactor.RaiseEvent#getContent()
   * @see #getRaiseEvent()
   * @generated
   */
  EReference getRaiseEvent_Content();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.SenseEvent <em>Sense Event</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Sense Event</em>'.
   * @see it.unibo.xtext.qactor.SenseEvent
   * @generated
   */
  EClass getSenseEvent();

  /**
   * Returns the meta object for the containment reference '{@link it.unibo.xtext.qactor.SenseEvent#getDuration <em>Duration</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Duration</em>'.
   * @see it.unibo.xtext.qactor.SenseEvent#getDuration()
   * @see #getSenseEvent()
   * @generated
   */
  EReference getSenseEvent_Duration();

  /**
   * Returns the meta object for the reference list '{@link it.unibo.xtext.qactor.SenseEvent#getEvents <em>Events</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference list '<em>Events</em>'.
   * @see it.unibo.xtext.qactor.SenseEvent#getEvents()
   * @see #getSenseEvent()
   * @generated
   */
  EReference getSenseEvent_Events();

  /**
   * Returns the meta object for the containment reference list '{@link it.unibo.xtext.qactor.SenseEvent#getPlans <em>Plans</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Plans</em>'.
   * @see it.unibo.xtext.qactor.SenseEvent#getPlans()
   * @see #getSenseEvent()
   * @generated
   */
  EReference getSenseEvent_Plans();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.MsgSwitch <em>Msg Switch</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Msg Switch</em>'.
   * @see it.unibo.xtext.qactor.MsgSwitch
   * @generated
   */
  EClass getMsgSwitch();

  /**
   * Returns the meta object for the reference '{@link it.unibo.xtext.qactor.MsgSwitch#getMessage <em>Message</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Message</em>'.
   * @see it.unibo.xtext.qactor.MsgSwitch#getMessage()
   * @see #getMsgSwitch()
   * @generated
   */
  EReference getMsgSwitch_Message();

  /**
   * Returns the meta object for the containment reference '{@link it.unibo.xtext.qactor.MsgSwitch#getMsg <em>Msg</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Msg</em>'.
   * @see it.unibo.xtext.qactor.MsgSwitch#getMsg()
   * @see #getMsgSwitch()
   * @generated
   */
  EReference getMsgSwitch_Msg();

  /**
   * Returns the meta object for the containment reference '{@link it.unibo.xtext.qactor.MsgSwitch#getMove <em>Move</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Move</em>'.
   * @see it.unibo.xtext.qactor.MsgSwitch#getMove()
   * @see #getMsgSwitch()
   * @generated
   */
  EReference getMsgSwitch_Move();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.EventSwitch <em>Event Switch</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Event Switch</em>'.
   * @see it.unibo.xtext.qactor.EventSwitch
   * @generated
   */
  EClass getEventSwitch();

  /**
   * Returns the meta object for the reference '{@link it.unibo.xtext.qactor.EventSwitch#getEvent <em>Event</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Event</em>'.
   * @see it.unibo.xtext.qactor.EventSwitch#getEvent()
   * @see #getEventSwitch()
   * @generated
   */
  EReference getEventSwitch_Event();

  /**
   * Returns the meta object for the containment reference '{@link it.unibo.xtext.qactor.EventSwitch#getMsg <em>Msg</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Msg</em>'.
   * @see it.unibo.xtext.qactor.EventSwitch#getMsg()
   * @see #getEventSwitch()
   * @generated
   */
  EReference getEventSwitch_Msg();

  /**
   * Returns the meta object for the containment reference '{@link it.unibo.xtext.qactor.EventSwitch#getMove <em>Move</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Move</em>'.
   * @see it.unibo.xtext.qactor.EventSwitch#getMove()
   * @see #getEventSwitch()
   * @generated
   */
  EReference getEventSwitch_Move();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.Continuation <em>Continuation</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Continuation</em>'.
   * @see it.unibo.xtext.qactor.Continuation
   * @generated
   */
  EClass getContinuation();

  /**
   * Returns the meta object for the reference '{@link it.unibo.xtext.qactor.Continuation#getPlan <em>Plan</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Plan</em>'.
   * @see it.unibo.xtext.qactor.Continuation#getPlan()
   * @see #getContinuation()
   * @generated
   */
  EReference getContinuation_Plan();

  /**
   * Returns the meta object for the attribute '{@link it.unibo.xtext.qactor.Continuation#getNane <em>Nane</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Nane</em>'.
   * @see it.unibo.xtext.qactor.Continuation#getNane()
   * @see #getContinuation()
   * @generated
   */
  EAttribute getContinuation_Nane();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.ExtensionMove <em>Extension Move</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Extension Move</em>'.
   * @see it.unibo.xtext.qactor.ExtensionMove
   * @generated
   */
  EClass getExtensionMove();

  /**
   * Returns the meta object for the attribute '{@link it.unibo.xtext.qactor.ExtensionMove#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see it.unibo.xtext.qactor.ExtensionMove#getName()
   * @see #getExtensionMove()
   * @generated
   */
  EAttribute getExtensionMove_Name();

  /**
   * Returns the meta object for the containment reference '{@link it.unibo.xtext.qactor.ExtensionMove#getDuration <em>Duration</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Duration</em>'.
   * @see it.unibo.xtext.qactor.ExtensionMove#getDuration()
   * @see #getExtensionMove()
   * @generated
   */
  EReference getExtensionMove_Duration();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.Photo <em>Photo</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Photo</em>'.
   * @see it.unibo.xtext.qactor.Photo
   * @generated
   */
  EClass getPhoto();

  /**
   * Returns the meta object for the containment reference '{@link it.unibo.xtext.qactor.Photo#getDestfile <em>Destfile</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Destfile</em>'.
   * @see it.unibo.xtext.qactor.Photo#getDestfile()
   * @see #getPhoto()
   * @generated
   */
  EReference getPhoto_Destfile();

  /**
   * Returns the meta object for the containment reference '{@link it.unibo.xtext.qactor.Photo#getAnswerEvent <em>Answer Event</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Answer Event</em>'.
   * @see it.unibo.xtext.qactor.Photo#getAnswerEvent()
   * @see #getPhoto()
   * @generated
   */
  EReference getPhoto_AnswerEvent();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.Sound <em>Sound</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Sound</em>'.
   * @see it.unibo.xtext.qactor.Sound
   * @generated
   */
  EClass getSound();

  /**
   * Returns the meta object for the containment reference '{@link it.unibo.xtext.qactor.Sound#getSrcfile <em>Srcfile</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Srcfile</em>'.
   * @see it.unibo.xtext.qactor.Sound#getSrcfile()
   * @see #getSound()
   * @generated
   */
  EReference getSound_Srcfile();

  /**
   * Returns the meta object for the containment reference '{@link it.unibo.xtext.qactor.Sound#getAnswerEvent <em>Answer Event</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Answer Event</em>'.
   * @see it.unibo.xtext.qactor.Sound#getAnswerEvent()
   * @see #getSound()
   * @generated
   */
  EReference getSound_AnswerEvent();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.Video <em>Video</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Video</em>'.
   * @see it.unibo.xtext.qactor.Video
   * @generated
   */
  EClass getVideo();

  /**
   * Returns the meta object for the containment reference '{@link it.unibo.xtext.qactor.Video#getDestfile <em>Destfile</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Destfile</em>'.
   * @see it.unibo.xtext.qactor.Video#getDestfile()
   * @see #getVideo()
   * @generated
   */
  EReference getVideo_Destfile();

  /**
   * Returns the meta object for the containment reference '{@link it.unibo.xtext.qactor.Video#getAnswerEvent <em>Answer Event</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Answer Event</em>'.
   * @see it.unibo.xtext.qactor.Video#getAnswerEvent()
   * @see #getVideo()
   * @generated
   */
  EReference getVideo_AnswerEvent();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.Delay <em>Delay</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Delay</em>'.
   * @see it.unibo.xtext.qactor.Delay
   * @generated
   */
  EClass getDelay();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.AnswerEvent <em>Answer Event</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Answer Event</em>'.
   * @see it.unibo.xtext.qactor.AnswerEvent
   * @generated
   */
  EClass getAnswerEvent();

  /**
   * Returns the meta object for the attribute '{@link it.unibo.xtext.qactor.AnswerEvent#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see it.unibo.xtext.qactor.AnswerEvent#getName()
   * @see #getAnswerEvent()
   * @generated
   */
  EAttribute getAnswerEvent_Name();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.EventHandler <em>Event Handler</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Event Handler</em>'.
   * @see it.unibo.xtext.qactor.EventHandler
   * @generated
   */
  EClass getEventHandler();

  /**
   * Returns the meta object for the attribute '{@link it.unibo.xtext.qactor.EventHandler#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see it.unibo.xtext.qactor.EventHandler#getName()
   * @see #getEventHandler()
   * @generated
   */
  EAttribute getEventHandler_Name();

  /**
   * Returns the meta object for the reference list '{@link it.unibo.xtext.qactor.EventHandler#getEvents <em>Events</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference list '<em>Events</em>'.
   * @see it.unibo.xtext.qactor.EventHandler#getEvents()
   * @see #getEventHandler()
   * @generated
   */
  EReference getEventHandler_Events();

  /**
   * Returns the meta object for the attribute '{@link it.unibo.xtext.qactor.EventHandler#isPrint <em>Print</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Print</em>'.
   * @see it.unibo.xtext.qactor.EventHandler#isPrint()
   * @see #getEventHandler()
   * @generated
   */
  EAttribute getEventHandler_Print();

  /**
   * Returns the meta object for the containment reference '{@link it.unibo.xtext.qactor.EventHandler#getBody <em>Body</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Body</em>'.
   * @see it.unibo.xtext.qactor.EventHandler#getBody()
   * @see #getEventHandler()
   * @generated
   */
  EReference getEventHandler_Body();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.EventHandlerBody <em>Event Handler Body</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Event Handler Body</em>'.
   * @see it.unibo.xtext.qactor.EventHandlerBody
   * @generated
   */
  EClass getEventHandlerBody();

  /**
   * Returns the meta object for the containment reference list '{@link it.unibo.xtext.qactor.EventHandlerBody#getOp <em>Op</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Op</em>'.
   * @see it.unibo.xtext.qactor.EventHandlerBody#getOp()
   * @see #getEventHandlerBody()
   * @generated
   */
  EReference getEventHandlerBody_Op();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.EventHandlerOperation <em>Event Handler Operation</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Event Handler Operation</em>'.
   * @see it.unibo.xtext.qactor.EventHandlerOperation
   * @generated
   */
  EClass getEventHandlerOperation();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.MemoOperation <em>Memo Operation</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Memo Operation</em>'.
   * @see it.unibo.xtext.qactor.MemoOperation
   * @generated
   */
  EClass getMemoOperation();

  /**
   * Returns the meta object for the containment reference '{@link it.unibo.xtext.qactor.MemoOperation#getRule <em>Rule</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Rule</em>'.
   * @see it.unibo.xtext.qactor.MemoOperation#getRule()
   * @see #getMemoOperation()
   * @generated
   */
  EReference getMemoOperation_Rule();

  /**
   * Returns the meta object for the reference '{@link it.unibo.xtext.qactor.MemoOperation#getActor <em>Actor</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Actor</em>'.
   * @see it.unibo.xtext.qactor.MemoOperation#getActor()
   * @see #getMemoOperation()
   * @generated
   */
  EReference getMemoOperation_Actor();

  /**
   * Returns the meta object for the containment reference '{@link it.unibo.xtext.qactor.MemoOperation#getDoMemo <em>Do Memo</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Do Memo</em>'.
   * @see it.unibo.xtext.qactor.MemoOperation#getDoMemo()
   * @see #getMemoOperation()
   * @generated
   */
  EReference getMemoOperation_DoMemo();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.SolveOperation <em>Solve Operation</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Solve Operation</em>'.
   * @see it.unibo.xtext.qactor.SolveOperation
   * @generated
   */
  EClass getSolveOperation();

  /**
   * Returns the meta object for the containment reference '{@link it.unibo.xtext.qactor.SolveOperation#getGoal <em>Goal</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Goal</em>'.
   * @see it.unibo.xtext.qactor.SolveOperation#getGoal()
   * @see #getSolveOperation()
   * @generated
   */
  EReference getSolveOperation_Goal();

  /**
   * Returns the meta object for the reference '{@link it.unibo.xtext.qactor.SolveOperation#getActor <em>Actor</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Actor</em>'.
   * @see it.unibo.xtext.qactor.SolveOperation#getActor()
   * @see #getSolveOperation()
   * @generated
   */
  EReference getSolveOperation_Actor();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.SendEventAsDispatch <em>Send Event As Dispatch</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Send Event As Dispatch</em>'.
   * @see it.unibo.xtext.qactor.SendEventAsDispatch
   * @generated
   */
  EClass getSendEventAsDispatch();

  /**
   * Returns the meta object for the reference '{@link it.unibo.xtext.qactor.SendEventAsDispatch#getActor <em>Actor</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Actor</em>'.
   * @see it.unibo.xtext.qactor.SendEventAsDispatch#getActor()
   * @see #getSendEventAsDispatch()
   * @generated
   */
  EReference getSendEventAsDispatch_Actor();

  /**
   * Returns the meta object for the reference '{@link it.unibo.xtext.qactor.SendEventAsDispatch#getMsgref <em>Msgref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Msgref</em>'.
   * @see it.unibo.xtext.qactor.SendEventAsDispatch#getMsgref()
   * @see #getSendEventAsDispatch()
   * @generated
   */
  EReference getSendEventAsDispatch_Msgref();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.MemoRule <em>Memo Rule</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Memo Rule</em>'.
   * @see it.unibo.xtext.qactor.MemoRule
   * @generated
   */
  EClass getMemoRule();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.MemoEvent <em>Memo Event</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Memo Event</em>'.
   * @see it.unibo.xtext.qactor.MemoEvent
   * @generated
   */
  EClass getMemoEvent();

  /**
   * Returns the meta object for the attribute '{@link it.unibo.xtext.qactor.MemoEvent#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see it.unibo.xtext.qactor.MemoEvent#getName()
   * @see #getMemoEvent()
   * @generated
   */
  EAttribute getMemoEvent_Name();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.Reaction <em>Reaction</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Reaction</em>'.
   * @see it.unibo.xtext.qactor.Reaction
   * @generated
   */
  EClass getReaction();

  /**
   * Returns the meta object for the attribute '{@link it.unibo.xtext.qactor.Reaction#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see it.unibo.xtext.qactor.Reaction#getName()
   * @see #getReaction()
   * @generated
   */
  EAttribute getReaction_Name();

  /**
   * Returns the meta object for the containment reference list '{@link it.unibo.xtext.qactor.Reaction#getAlarms <em>Alarms</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Alarms</em>'.
   * @see it.unibo.xtext.qactor.Reaction#getAlarms()
   * @see #getReaction()
   * @generated
   */
  EReference getReaction_Alarms();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.AlarmEvent <em>Alarm Event</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Alarm Event</em>'.
   * @see it.unibo.xtext.qactor.AlarmEvent
   * @generated
   */
  EClass getAlarmEvent();

  /**
   * Returns the meta object for the attribute '{@link it.unibo.xtext.qactor.AlarmEvent#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see it.unibo.xtext.qactor.AlarmEvent#getName()
   * @see #getAlarmEvent()
   * @generated
   */
  EAttribute getAlarmEvent_Name();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.NormalEvent <em>Normal Event</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Normal Event</em>'.
   * @see it.unibo.xtext.qactor.NormalEvent
   * @generated
   */
  EClass getNormalEvent();

  /**
   * Returns the meta object for the reference '{@link it.unibo.xtext.qactor.NormalEvent#getEv <em>Ev</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Ev</em>'.
   * @see it.unibo.xtext.qactor.NormalEvent#getEv()
   * @see #getNormalEvent()
   * @generated
   */
  EReference getNormalEvent_Ev();

  /**
   * Returns the meta object for the reference '{@link it.unibo.xtext.qactor.NormalEvent#getPlanRef <em>Plan Ref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Plan Ref</em>'.
   * @see it.unibo.xtext.qactor.NormalEvent#getPlanRef()
   * @see #getNormalEvent()
   * @generated
   */
  EReference getNormalEvent_PlanRef();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.ContinueEvent <em>Continue Event</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Continue Event</em>'.
   * @see it.unibo.xtext.qactor.ContinueEvent
   * @generated
   */
  EClass getContinueEvent();

  /**
   * Returns the meta object for the reference '{@link it.unibo.xtext.qactor.ContinueEvent#getEvOccur <em>Ev Occur</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Ev Occur</em>'.
   * @see it.unibo.xtext.qactor.ContinueEvent#getEvOccur()
   * @see #getContinueEvent()
   * @generated
   */
  EReference getContinueEvent_EvOccur();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.VarOrQactor <em>Var Or Qactor</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Var Or Qactor</em>'.
   * @see it.unibo.xtext.qactor.VarOrQactor
   * @generated
   */
  EClass getVarOrQactor();

  /**
   * Returns the meta object for the containment reference '{@link it.unibo.xtext.qactor.VarOrQactor#getVar <em>Var</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Var</em>'.
   * @see it.unibo.xtext.qactor.VarOrQactor#getVar()
   * @see #getVarOrQactor()
   * @generated
   */
  EReference getVarOrQactor_Var();

  /**
   * Returns the meta object for the reference '{@link it.unibo.xtext.qactor.VarOrQactor#getDest <em>Dest</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Dest</em>'.
   * @see it.unibo.xtext.qactor.VarOrQactor#getDest()
   * @see #getVarOrQactor()
   * @generated
   */
  EReference getVarOrQactor_Dest();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.VarOrAtomic <em>Var Or Atomic</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Var Or Atomic</em>'.
   * @see it.unibo.xtext.qactor.VarOrAtomic
   * @generated
   */
  EClass getVarOrAtomic();

  /**
   * Returns the meta object for the containment reference '{@link it.unibo.xtext.qactor.VarOrAtomic#getVar <em>Var</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Var</em>'.
   * @see it.unibo.xtext.qactor.VarOrAtomic#getVar()
   * @see #getVarOrAtomic()
   * @generated
   */
  EReference getVarOrAtomic_Var();

  /**
   * Returns the meta object for the containment reference '{@link it.unibo.xtext.qactor.VarOrAtomic#getConst <em>Const</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Const</em>'.
   * @see it.unibo.xtext.qactor.VarOrAtomic#getConst()
   * @see #getVarOrAtomic()
   * @generated
   */
  EReference getVarOrAtomic_Const();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.VarOrString <em>Var Or String</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Var Or String</em>'.
   * @see it.unibo.xtext.qactor.VarOrString
   * @generated
   */
  EClass getVarOrString();

  /**
   * Returns the meta object for the containment reference '{@link it.unibo.xtext.qactor.VarOrString#getVar <em>Var</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Var</em>'.
   * @see it.unibo.xtext.qactor.VarOrString#getVar()
   * @see #getVarOrString()
   * @generated
   */
  EReference getVarOrString_Var();

  /**
   * Returns the meta object for the attribute '{@link it.unibo.xtext.qactor.VarOrString#getConst <em>Const</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Const</em>'.
   * @see it.unibo.xtext.qactor.VarOrString#getConst()
   * @see #getVarOrString()
   * @generated
   */
  EAttribute getVarOrString_Const();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.VarOrPStruct <em>Var Or PStruct</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Var Or PStruct</em>'.
   * @see it.unibo.xtext.qactor.VarOrPStruct
   * @generated
   */
  EClass getVarOrPStruct();

  /**
   * Returns the meta object for the containment reference '{@link it.unibo.xtext.qactor.VarOrPStruct#getVar <em>Var</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Var</em>'.
   * @see it.unibo.xtext.qactor.VarOrPStruct#getVar()
   * @see #getVarOrPStruct()
   * @generated
   */
  EReference getVarOrPStruct_Var();

  /**
   * Returns the meta object for the containment reference '{@link it.unibo.xtext.qactor.VarOrPStruct#getPsrtuct <em>Psrtuct</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Psrtuct</em>'.
   * @see it.unibo.xtext.qactor.VarOrPStruct#getPsrtuct()
   * @see #getVarOrPStruct()
   * @generated
   */
  EReference getVarOrPStruct_Psrtuct();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.VarOrPhead <em>Var Or Phead</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Var Or Phead</em>'.
   * @see it.unibo.xtext.qactor.VarOrPhead
   * @generated
   */
  EClass getVarOrPhead();

  /**
   * Returns the meta object for the containment reference '{@link it.unibo.xtext.qactor.VarOrPhead#getVar <em>Var</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Var</em>'.
   * @see it.unibo.xtext.qactor.VarOrPhead#getVar()
   * @see #getVarOrPhead()
   * @generated
   */
  EReference getVarOrPhead_Var();

  /**
   * Returns the meta object for the containment reference '{@link it.unibo.xtext.qactor.VarOrPhead#getPhead <em>Phead</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Phead</em>'.
   * @see it.unibo.xtext.qactor.VarOrPhead#getPhead()
   * @see #getVarOrPhead()
   * @generated
   */
  EReference getVarOrPhead_Phead();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.VarOrAtomOrPStruct <em>Var Or Atom Or PStruct</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Var Or Atom Or PStruct</em>'.
   * @see it.unibo.xtext.qactor.VarOrAtomOrPStruct
   * @generated
   */
  EClass getVarOrAtomOrPStruct();

  /**
   * Returns the meta object for the containment reference '{@link it.unibo.xtext.qactor.VarOrAtomOrPStruct#getVar <em>Var</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Var</em>'.
   * @see it.unibo.xtext.qactor.VarOrAtomOrPStruct#getVar()
   * @see #getVarOrAtomOrPStruct()
   * @generated
   */
  EReference getVarOrAtomOrPStruct_Var();

  /**
   * Returns the meta object for the containment reference '{@link it.unibo.xtext.qactor.VarOrAtomOrPStruct#getPsrtuct <em>Psrtuct</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Psrtuct</em>'.
   * @see it.unibo.xtext.qactor.VarOrAtomOrPStruct#getPsrtuct()
   * @see #getVarOrAtomOrPStruct()
   * @generated
   */
  EReference getVarOrAtomOrPStruct_Psrtuct();

  /**
   * Returns the meta object for the containment reference '{@link it.unibo.xtext.qactor.VarOrAtomOrPStruct#getAtom <em>Atom</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Atom</em>'.
   * @see it.unibo.xtext.qactor.VarOrAtomOrPStruct#getAtom()
   * @see #getVarOrAtomOrPStruct()
   * @generated
   */
  EReference getVarOrAtomOrPStruct_Atom();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.Variable <em>Variable</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Variable</em>'.
   * @see it.unibo.xtext.qactor.Variable
   * @generated
   */
  EClass getVariable();

  /**
   * Returns the meta object for the attribute '{@link it.unibo.xtext.qactor.Variable#isGuardvar <em>Guardvar</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Guardvar</em>'.
   * @see it.unibo.xtext.qactor.Variable#isGuardvar()
   * @see #getVariable()
   * @generated
   */
  EAttribute getVariable_Guardvar();

  /**
   * Returns the meta object for the attribute '{@link it.unibo.xtext.qactor.Variable#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see it.unibo.xtext.qactor.Variable#getName()
   * @see #getVariable()
   * @generated
   */
  EAttribute getVariable_Name();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.TimeLimit <em>Time Limit</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Time Limit</em>'.
   * @see it.unibo.xtext.qactor.TimeLimit
   * @generated
   */
  EClass getTimeLimit();

  /**
   * Returns the meta object for the attribute '{@link it.unibo.xtext.qactor.TimeLimit#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see it.unibo.xtext.qactor.TimeLimit#getName()
   * @see #getTimeLimit()
   * @generated
   */
  EAttribute getTimeLimit_Name();

  /**
   * Returns the meta object for the attribute '{@link it.unibo.xtext.qactor.TimeLimit#getMsec <em>Msec</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Msec</em>'.
   * @see it.unibo.xtext.qactor.TimeLimit#getMsec()
   * @see #getTimeLimit()
   * @generated
   */
  EAttribute getTimeLimit_Msec();

  /**
   * Returns the meta object for the containment reference '{@link it.unibo.xtext.qactor.TimeLimit#getVar <em>Var</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Var</em>'.
   * @see it.unibo.xtext.qactor.TimeLimit#getVar()
   * @see #getTimeLimit()
   * @generated
   */
  EReference getTimeLimit_Var();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.ComponentIP <em>Component IP</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Component IP</em>'.
   * @see it.unibo.xtext.qactor.ComponentIP
   * @generated
   */
  EClass getComponentIP();

  /**
   * Returns the meta object for the attribute '{@link it.unibo.xtext.qactor.ComponentIP#getHost <em>Host</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Host</em>'.
   * @see it.unibo.xtext.qactor.ComponentIP#getHost()
   * @see #getComponentIP()
   * @generated
   */
  EAttribute getComponentIP_Host();

  /**
   * Returns the meta object for the attribute '{@link it.unibo.xtext.qactor.ComponentIP#getPort <em>Port</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Port</em>'.
   * @see it.unibo.xtext.qactor.ComponentIP#getPort()
   * @see #getComponentIP()
   * @generated
   */
  EAttribute getComponentIP_Port();

  /**
   * Returns the meta object for class '{@link it.unibo.xtext.qactor.MoveFile <em>Move File</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Move File</em>'.
   * @see it.unibo.xtext.qactor.MoveFile
   * @generated
   */
  EClass getMoveFile();

  /**
   * Returns the meta object for the attribute '{@link it.unibo.xtext.qactor.MoveFile#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see it.unibo.xtext.qactor.MoveFile#getName()
   * @see #getMoveFile()
   * @generated
   */
  EAttribute getMoveFile_Name();

  /**
   * Returns the meta object for the containment reference '{@link it.unibo.xtext.qactor.MoveFile#getFname <em>Fname</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Fname</em>'.
   * @see it.unibo.xtext.qactor.MoveFile#getFname()
   * @see #getMoveFile()
   * @generated
   */
  EReference getMoveFile_Fname();

  /**
   * Returns the meta object for enum '{@link it.unibo.xtext.qactor.WindowColor <em>Window Color</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>Window Color</em>'.
   * @see it.unibo.xtext.qactor.WindowColor
   * @generated
   */
  EEnum getWindowColor();

  /**
   * Returns the factory that creates the instances of the model.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the factory that creates the instances of the model.
   * @generated
   */
  QactorFactory getQactorFactory();

  /**
   * <!-- begin-user-doc -->
   * Defines literals for the meta objects that represent
   * <ul>
   *   <li>each class,</li>
   *   <li>each feature of each class,</li>
   *   <li>each enum,</li>
   *   <li>and each data type</li>
   * </ul>
   * <!-- end-user-doc -->
   * @generated
   */
  interface Literals
  {
    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.QActorSystemImpl <em>QActor System</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.QActorSystemImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getQActorSystem()
     * @generated
     */
    EClass QACTOR_SYSTEM = eINSTANCE.getQActorSystem();

    /**
     * The meta object literal for the '<em><b>Spec</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference QACTOR_SYSTEM__SPEC = eINSTANCE.getQActorSystem_Spec();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.QActorSystemSpecImpl <em>QActor System Spec</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.QActorSystemSpecImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getQActorSystemSpec()
     * @generated
     */
    EClass QACTOR_SYSTEM_SPEC = eINSTANCE.getQActorSystemSpec();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute QACTOR_SYSTEM_SPEC__NAME = eINSTANCE.getQActorSystemSpec_Name();

    /**
     * The meta object literal for the '<em><b>Testing</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute QACTOR_SYSTEM_SPEC__TESTING = eINSTANCE.getQActorSystemSpec_Testing();

    /**
     * The meta object literal for the '<em><b>Message</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference QACTOR_SYSTEM_SPEC__MESSAGE = eINSTANCE.getQActorSystemSpec_Message();

    /**
     * The meta object literal for the '<em><b>Context</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference QACTOR_SYSTEM_SPEC__CONTEXT = eINSTANCE.getQActorSystemSpec_Context();

    /**
     * The meta object literal for the '<em><b>Actor</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference QACTOR_SYSTEM_SPEC__ACTOR = eINSTANCE.getQActorSystemSpec_Actor();

    /**
     * The meta object literal for the '<em><b>Robot</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference QACTOR_SYSTEM_SPEC__ROBOT = eINSTANCE.getQActorSystemSpec_Robot();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.RobotImpl <em>Robot</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.RobotImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getRobot()
     * @generated
     */
    EClass ROBOT = eINSTANCE.getRobot();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ROBOT__NAME = eINSTANCE.getRobot_Name();

    /**
     * The meta object literal for the '<em><b>Actor</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ROBOT__ACTOR = eINSTANCE.getRobot_Actor();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.MessageImpl <em>Message</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.MessageImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getMessage()
     * @generated
     */
    EClass MESSAGE = eINSTANCE.getMessage();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute MESSAGE__NAME = eINSTANCE.getMessage_Name();

    /**
     * The meta object literal for the '<em><b>Msg</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MESSAGE__MSG = eINSTANCE.getMessage_Msg();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.OutOnlyMessageImpl <em>Out Only Message</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.OutOnlyMessageImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getOutOnlyMessage()
     * @generated
     */
    EClass OUT_ONLY_MESSAGE = eINSTANCE.getOutOnlyMessage();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.OutInMessageImpl <em>Out In Message</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.OutInMessageImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getOutInMessage()
     * @generated
     */
    EClass OUT_IN_MESSAGE = eINSTANCE.getOutInMessage();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.EventImpl <em>Event</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.EventImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getEvent()
     * @generated
     */
    EClass EVENT = eINSTANCE.getEvent();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.SignalImpl <em>Signal</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.SignalImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getSignal()
     * @generated
     */
    EClass SIGNAL = eINSTANCE.getSignal();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.TokenImpl <em>Token</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.TokenImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getToken()
     * @generated
     */
    EClass TOKEN = eINSTANCE.getToken();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.DispatchImpl <em>Dispatch</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.DispatchImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getDispatch()
     * @generated
     */
    EClass DISPATCH = eINSTANCE.getDispatch();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.RequestImpl <em>Request</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.RequestImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getRequest()
     * @generated
     */
    EClass REQUEST = eINSTANCE.getRequest();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.InvitationImpl <em>Invitation</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.InvitationImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getInvitation()
     * @generated
     */
    EClass INVITATION = eINSTANCE.getInvitation();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.ContextImpl <em>Context</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.ContextImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getContext()
     * @generated
     */
    EClass CONTEXT = eINSTANCE.getContext();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute CONTEXT__NAME = eINSTANCE.getContext_Name();

    /**
     * The meta object literal for the '<em><b>Ip</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference CONTEXT__IP = eINSTANCE.getContext_Ip();

    /**
     * The meta object literal for the '<em><b>Env</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute CONTEXT__ENV = eINSTANCE.getContext_Env();

    /**
     * The meta object literal for the '<em><b>Color</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute CONTEXT__COLOR = eINSTANCE.getContext_Color();

    /**
     * The meta object literal for the '<em><b>Standalone</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute CONTEXT__STANDALONE = eINSTANCE.getContext_Standalone();

    /**
     * The meta object literal for the '<em><b>Httpserver</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute CONTEXT__HTTPSERVER = eINSTANCE.getContext_Httpserver();

    /**
     * The meta object literal for the '<em><b>Handler</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference CONTEXT__HANDLER = eINSTANCE.getContext_Handler();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.QActorImpl <em>QActor</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.QActorImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getQActor()
     * @generated
     */
    EClass QACTOR = eINSTANCE.getQActor();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute QACTOR__NAME = eINSTANCE.getQActor_Name();

    /**
     * The meta object literal for the '<em><b>Context</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference QACTOR__CONTEXT = eINSTANCE.getQActor_Context();

    /**
     * The meta object literal for the '<em><b>Env</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute QACTOR__ENV = eINSTANCE.getQActor_Env();

    /**
     * The meta object literal for the '<em><b>Color</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute QACTOR__COLOR = eINSTANCE.getQActor_Color();

    /**
     * The meta object literal for the '<em><b>Rules</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference QACTOR__RULES = eINSTANCE.getQActor_Rules();

    /**
     * The meta object literal for the '<em><b>Data</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference QACTOR__DATA = eINSTANCE.getQActor_Data();

    /**
     * The meta object literal for the '<em><b>Action</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference QACTOR__ACTION = eINSTANCE.getQActor_Action();

    /**
     * The meta object literal for the '<em><b>Plans</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference QACTOR__PLANS = eINSTANCE.getQActor_Plans();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.RuleImpl <em>Rule</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.RuleImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getRule()
     * @generated
     */
    EClass RULE = eINSTANCE.getRule();

    /**
     * The meta object literal for the '<em><b>Head</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference RULE__HEAD = eINSTANCE.getRule_Head();

    /**
     * The meta object literal for the '<em><b>Body</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference RULE__BODY = eINSTANCE.getRule_Body();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.PHeadImpl <em>PHead</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.PHeadImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getPHead()
     * @generated
     */
    EClass PHEAD = eINSTANCE.getPHead();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.PTermImpl <em>PTerm</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.PTermImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getPTerm()
     * @generated
     */
    EClass PTERM = eINSTANCE.getPTerm();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.PAtomImpl <em>PAtom</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.PAtomImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getPAtom()
     * @generated
     */
    EClass PATOM = eINSTANCE.getPAtom();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.PAtomStringImpl <em>PAtom String</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.PAtomStringImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getPAtomString()
     * @generated
     */
    EClass PATOM_STRING = eINSTANCE.getPAtomString();

    /**
     * The meta object literal for the '<em><b>Val</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute PATOM_STRING__VAL = eINSTANCE.getPAtomString_Val();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.PAtomicImpl <em>PAtomic</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.PAtomicImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getPAtomic()
     * @generated
     */
    EClass PATOMIC = eINSTANCE.getPAtomic();

    /**
     * The meta object literal for the '<em><b>Val</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute PATOMIC__VAL = eINSTANCE.getPAtomic_Val();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.PAtomNumImpl <em>PAtom Num</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.PAtomNumImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getPAtomNum()
     * @generated
     */
    EClass PATOM_NUM = eINSTANCE.getPAtomNum();

    /**
     * The meta object literal for the '<em><b>Val</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute PATOM_NUM__VAL = eINSTANCE.getPAtomNum_Val();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.PStructImpl <em>PStruct</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.PStructImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getPStruct()
     * @generated
     */
    EClass PSTRUCT = eINSTANCE.getPStruct();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute PSTRUCT__NAME = eINSTANCE.getPStruct_Name();

    /**
     * The meta object literal for the '<em><b>Msg Arg</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PSTRUCT__MSG_ARG = eINSTANCE.getPStruct_MsgArg();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.PActorCallImpl <em>PActor Call</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.PActorCallImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getPActorCall()
     * @generated
     */
    EClass PACTOR_CALL = eINSTANCE.getPActorCall();

    /**
     * The meta object literal for the '<em><b>Body</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PACTOR_CALL__BODY = eINSTANCE.getPActorCall_Body();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.PPredefImpl <em>PPredef</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.PPredefImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getPPredef()
     * @generated
     */
    EClass PPREDEF = eINSTANCE.getPPredef();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.PIsImpl <em>PIs</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.PIsImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getPIs()
     * @generated
     */
    EClass PIS = eINSTANCE.getPIs();

    /**
     * The meta object literal for the '<em><b>Varout</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PIS__VAROUT = eINSTANCE.getPIs_Varout();

    /**
     * The meta object literal for the '<em><b>Varin</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PIS__VARIN = eINSTANCE.getPIs_Varin();

    /**
     * The meta object literal for the '<em><b>Num</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PIS__NUM = eINSTANCE.getPIs_Num();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.PAtomCutImpl <em>PAtom Cut</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.PAtomCutImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getPAtomCut()
     * @generated
     */
    EClass PATOM_CUT = eINSTANCE.getPAtomCut();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute PATOM_CUT__NAME = eINSTANCE.getPAtomCut_Name();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.DataImpl <em>Data</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.DataImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getData()
     * @generated
     */
    EClass DATA = eINSTANCE.getData();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute DATA__NAME = eINSTANCE.getData_Name();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.IntegerDataImpl <em>Integer Data</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.IntegerDataImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getIntegerData()
     * @generated
     */
    EClass INTEGER_DATA = eINSTANCE.getIntegerData();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute INTEGER_DATA__VALUE = eINSTANCE.getIntegerData_Value();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.StringDataImpl <em>String Data</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.StringDataImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getStringData()
     * @generated
     */
    EClass STRING_DATA = eINSTANCE.getStringData();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute STRING_DATA__VALUE = eINSTANCE.getStringData_Value();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.ActionImpl <em>Action</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.ActionImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getAction()
     * @generated
     */
    EClass ACTION = eINSTANCE.getAction();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ACTION__NAME = eINSTANCE.getAction_Name();

    /**
     * The meta object literal for the '<em><b>Undoable</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ACTION__UNDOABLE = eINSTANCE.getAction_Undoable();

    /**
     * The meta object literal for the '<em><b>Msec</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ACTION__MSEC = eINSTANCE.getAction_Msec();

    /**
     * The meta object literal for the '<em><b>Arg</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ACTION__ARG = eINSTANCE.getAction_Arg();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.PlanImpl <em>Plan</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.PlanImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getPlan()
     * @generated
     */
    EClass PLAN = eINSTANCE.getPlan();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute PLAN__NAME = eINSTANCE.getPlan_Name();

    /**
     * The meta object literal for the '<em><b>Normal</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute PLAN__NORMAL = eINSTANCE.getPlan_Normal();

    /**
     * The meta object literal for the '<em><b>Resume</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute PLAN__RESUME = eINSTANCE.getPlan_Resume();

    /**
     * The meta object literal for the '<em><b>Action</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PLAN__ACTION = eINSTANCE.getPlan_Action();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.PlanActionImpl <em>Plan Action</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.PlanActionImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getPlanAction()
     * @generated
     */
    EClass PLAN_ACTION = eINSTANCE.getPlanAction();

    /**
     * The meta object literal for the '<em><b>Guard</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PLAN_ACTION__GUARD = eINSTANCE.getPlanAction_Guard();

    /**
     * The meta object literal for the '<em><b>Move</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PLAN_ACTION__MOVE = eINSTANCE.getPlanAction_Move();

    /**
     * The meta object literal for the '<em><b>React</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PLAN_ACTION__REACT = eINSTANCE.getPlanAction_React();

    /**
     * The meta object literal for the '<em><b>Elsemove</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PLAN_ACTION__ELSEMOVE = eINSTANCE.getPlanAction_Elsemove();

    /**
     * The meta object literal for the '<em><b>Elsereact</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PLAN_ACTION__ELSEREACT = eINSTANCE.getPlanAction_Elsereact();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.GuardImpl <em>Guard</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.GuardImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getGuard()
     * @generated
     */
    EClass GUARD = eINSTANCE.getGuard();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute GUARD__NAME = eINSTANCE.getGuard_Name();

    /**
     * The meta object literal for the '<em><b>Not</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute GUARD__NOT = eINSTANCE.getGuard_Not();

    /**
     * The meta object literal for the '<em><b>Guardspec</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference GUARD__GUARDSPEC = eINSTANCE.getGuard_Guardspec();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.GuardPredicateImpl <em>Guard Predicate</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.GuardPredicateImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getGuardPredicate()
     * @generated
     */
    EClass GUARD_PREDICATE = eINSTANCE.getGuardPredicate();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute GUARD_PREDICATE__NAME = eINSTANCE.getGuardPredicate_Name();

    /**
     * The meta object literal for the '<em><b>Pred</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference GUARD_PREDICATE__PRED = eINSTANCE.getGuardPredicate_Pred();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.GuardPredicateRemovableImpl <em>Guard Predicate Removable</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.GuardPredicateRemovableImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getGuardPredicateRemovable()
     * @generated
     */
    EClass GUARD_PREDICATE_REMOVABLE = eINSTANCE.getGuardPredicateRemovable();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.GuardPredicateStableImpl <em>Guard Predicate Stable</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.GuardPredicateStableImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getGuardPredicateStable()
     * @generated
     */
    EClass GUARD_PREDICATE_STABLE = eINSTANCE.getGuardPredicateStable();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.MoveImpl <em>Move</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.MoveImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getMove()
     * @generated
     */
    EClass MOVE = eINSTANCE.getMove();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.ActionMoveImpl <em>Action Move</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.ActionMoveImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getActionMove()
     * @generated
     */
    EClass ACTION_MOVE = eINSTANCE.getActionMove();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.ExecuteActionImpl <em>Execute Action</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.ExecuteActionImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getExecuteAction()
     * @generated
     */
    EClass EXECUTE_ACTION = eINSTANCE.getExecuteAction();

    /**
     * The meta object literal for the '<em><b>Action</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EXECUTE_ACTION__ACTION = eINSTANCE.getExecuteAction_Action();

    /**
     * The meta object literal for the '<em><b>Arg</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EXECUTE_ACTION__ARG = eINSTANCE.getExecuteAction_Arg();

    /**
     * The meta object literal for the '<em><b>Sentence</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EXECUTE_ACTION__SENTENCE = eINSTANCE.getExecuteAction_Sentence();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.SolveGoalImpl <em>Solve Goal</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.SolveGoalImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getSolveGoal()
     * @generated
     */
    EClass SOLVE_GOAL = eINSTANCE.getSolveGoal();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute SOLVE_GOAL__NAME = eINSTANCE.getSolveGoal_Name();

    /**
     * The meta object literal for the '<em><b>Goal</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SOLVE_GOAL__GOAL = eINSTANCE.getSolveGoal_Goal();

    /**
     * The meta object literal for the '<em><b>Duration</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SOLVE_GOAL__DURATION = eINSTANCE.getSolveGoal_Duration();

    /**
     * The meta object literal for the '<em><b>Plan</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SOLVE_GOAL__PLAN = eINSTANCE.getSolveGoal_Plan();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.DemoImpl <em>Demo</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.DemoImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getDemo()
     * @generated
     */
    EClass DEMO = eINSTANCE.getDemo();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute DEMO__NAME = eINSTANCE.getDemo_Name();

    /**
     * The meta object literal for the '<em><b>Goal</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DEMO__GOAL = eINSTANCE.getDemo_Goal();

    /**
     * The meta object literal for the '<em><b>Plan</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DEMO__PLAN = eINSTANCE.getDemo_Plan();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.ActorOpImpl <em>Actor Op</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.ActorOpImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getActorOp()
     * @generated
     */
    EClass ACTOR_OP = eINSTANCE.getActorOp();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ACTOR_OP__NAME = eINSTANCE.getActorOp_Name();

    /**
     * The meta object literal for the '<em><b>Goal</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ACTOR_OP__GOAL = eINSTANCE.getActorOp_Goal();

    /**
     * The meta object literal for the '<em><b>Plan</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ACTOR_OP__PLAN = eINSTANCE.getActorOp_Plan();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.BasicRobotMoveImpl <em>Basic Robot Move</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.BasicRobotMoveImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getBasicRobotMove()
     * @generated
     */
    EClass BASIC_ROBOT_MOVE = eINSTANCE.getBasicRobotMove();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BASIC_ROBOT_MOVE__NAME = eINSTANCE.getBasicRobotMove_Name();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.BasicMoveImpl <em>Basic Move</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.BasicMoveImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getBasicMove()
     * @generated
     */
    EClass BASIC_MOVE = eINSTANCE.getBasicMove();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BASIC_MOVE__NAME = eINSTANCE.getBasicMove_Name();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.PrintImpl <em>Print</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.PrintImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getPrint()
     * @generated
     */
    EClass PRINT = eINSTANCE.getPrint();

    /**
     * The meta object literal for the '<em><b>Args</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PRINT__ARGS = eINSTANCE.getPrint_Args();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.PrintCurrentEventImpl <em>Print Current Event</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.PrintCurrentEventImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getPrintCurrentEvent()
     * @generated
     */
    EClass PRINT_CURRENT_EVENT = eINSTANCE.getPrintCurrentEvent();

    /**
     * The meta object literal for the '<em><b>Memo</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute PRINT_CURRENT_EVENT__MEMO = eINSTANCE.getPrintCurrentEvent_Memo();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.PrintCurrentMessageImpl <em>Print Current Message</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.PrintCurrentMessageImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getPrintCurrentMessage()
     * @generated
     */
    EClass PRINT_CURRENT_MESSAGE = eINSTANCE.getPrintCurrentMessage();

    /**
     * The meta object literal for the '<em><b>Memo</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute PRINT_CURRENT_MESSAGE__MEMO = eINSTANCE.getPrintCurrentMessage_Memo();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.MemoCurrentEventImpl <em>Memo Current Event</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.MemoCurrentEventImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getMemoCurrentEvent()
     * @generated
     */
    EClass MEMO_CURRENT_EVENT = eINSTANCE.getMemoCurrentEvent();

    /**
     * The meta object literal for the '<em><b>Lastonly</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute MEMO_CURRENT_EVENT__LASTONLY = eINSTANCE.getMemoCurrentEvent_Lastonly();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.MemoCurrentMessageImpl <em>Memo Current Message</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.MemoCurrentMessageImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getMemoCurrentMessage()
     * @generated
     */
    EClass MEMO_CURRENT_MESSAGE = eINSTANCE.getMemoCurrentMessage();

    /**
     * The meta object literal for the '<em><b>Lastonly</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute MEMO_CURRENT_MESSAGE__LASTONLY = eINSTANCE.getMemoCurrentMessage_Lastonly();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.PlanMoveImpl <em>Plan Move</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.PlanMoveImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getPlanMove()
     * @generated
     */
    EClass PLAN_MOVE = eINSTANCE.getPlanMove();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute PLAN_MOVE__NAME = eINSTANCE.getPlanMove_Name();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.GetActivationEventImpl <em>Get Activation Event</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.GetActivationEventImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getGetActivationEvent()
     * @generated
     */
    EClass GET_ACTIVATION_EVENT = eINSTANCE.getGetActivationEvent();

    /**
     * The meta object literal for the '<em><b>Var</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference GET_ACTIVATION_EVENT__VAR = eINSTANCE.getGetActivationEvent_Var();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.GetSensedEventImpl <em>Get Sensed Event</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.GetSensedEventImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getGetSensedEvent()
     * @generated
     */
    EClass GET_SENSED_EVENT = eINSTANCE.getGetSensedEvent();

    /**
     * The meta object literal for the '<em><b>Var</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference GET_SENSED_EVENT__VAR = eINSTANCE.getGetSensedEvent_Var();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.LoadPlanImpl <em>Load Plan</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.LoadPlanImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getLoadPlan()
     * @generated
     */
    EClass LOAD_PLAN = eINSTANCE.getLoadPlan();

    /**
     * The meta object literal for the '<em><b>Fname</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference LOAD_PLAN__FNAME = eINSTANCE.getLoadPlan_Fname();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.RunPlanImpl <em>Run Plan</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.RunPlanImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getRunPlan()
     * @generated
     */
    EClass RUN_PLAN = eINSTANCE.getRunPlan();

    /**
     * The meta object literal for the '<em><b>Plainid</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference RUN_PLAN__PLAINID = eINSTANCE.getRunPlan_Plainid();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.ResumePlanImpl <em>Resume Plan</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.ResumePlanImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getResumePlan()
     * @generated
     */
    EClass RESUME_PLAN = eINSTANCE.getResumePlan();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.SuspendPlanImpl <em>Suspend Plan</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.SuspendPlanImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getSuspendPlan()
     * @generated
     */
    EClass SUSPEND_PLAN = eINSTANCE.getSuspendPlan();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.RepeatPlanImpl <em>Repeat Plan</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.RepeatPlanImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getRepeatPlan()
     * @generated
     */
    EClass REPEAT_PLAN = eINSTANCE.getRepeatPlan();

    /**
     * The meta object literal for the '<em><b>Niter</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute REPEAT_PLAN__NITER = eINSTANCE.getRepeatPlan_Niter();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.SwitchPlanImpl <em>Switch Plan</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.SwitchPlanImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getSwitchPlan()
     * @generated
     */
    EClass SWITCH_PLAN = eINSTANCE.getSwitchPlan();

    /**
     * The meta object literal for the '<em><b>Plan</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SWITCH_PLAN__PLAN = eINSTANCE.getSwitchPlan_Plan();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.EndPlanImpl <em>End Plan</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.EndPlanImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getEndPlan()
     * @generated
     */
    EClass END_PLAN = eINSTANCE.getEndPlan();

    /**
     * The meta object literal for the '<em><b>Msg</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute END_PLAN__MSG = eINSTANCE.getEndPlan_Msg();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.EndActorImpl <em>End Actor</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.EndActorImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getEndActor()
     * @generated
     */
    EClass END_ACTOR = eINSTANCE.getEndActor();

    /**
     * The meta object literal for the '<em><b>Msg</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute END_ACTOR__MSG = eINSTANCE.getEndActor_Msg();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.GuardMoveImpl <em>Guard Move</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.GuardMoveImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getGuardMove()
     * @generated
     */
    EClass GUARD_MOVE = eINSTANCE.getGuardMove();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute GUARD_MOVE__NAME = eINSTANCE.getGuardMove_Name();

    /**
     * The meta object literal for the '<em><b>Rule</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference GUARD_MOVE__RULE = eINSTANCE.getGuardMove_Rule();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.AddRuleImpl <em>Add Rule</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.AddRuleImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getAddRule()
     * @generated
     */
    EClass ADD_RULE = eINSTANCE.getAddRule();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.RemoveRuleImpl <em>Remove Rule</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.RemoveRuleImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getRemoveRule()
     * @generated
     */
    EClass REMOVE_RULE = eINSTANCE.getRemoveRule();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.MessageMoveImpl <em>Message Move</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.MessageMoveImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getMessageMove()
     * @generated
     */
    EClass MESSAGE_MOVE = eINSTANCE.getMessageMove();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.SendDispatchImpl <em>Send Dispatch</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.SendDispatchImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getSendDispatch()
     * @generated
     */
    EClass SEND_DISPATCH = eINSTANCE.getSendDispatch();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute SEND_DISPATCH__NAME = eINSTANCE.getSendDispatch_Name();

    /**
     * The meta object literal for the '<em><b>Dest</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SEND_DISPATCH__DEST = eINSTANCE.getSendDispatch_Dest();

    /**
     * The meta object literal for the '<em><b>Msgref</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SEND_DISPATCH__MSGREF = eINSTANCE.getSendDispatch_Msgref();

    /**
     * The meta object literal for the '<em><b>Val</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SEND_DISPATCH__VAL = eINSTANCE.getSendDispatch_Val();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.SendRequestImpl <em>Send Request</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.SendRequestImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getSendRequest()
     * @generated
     */
    EClass SEND_REQUEST = eINSTANCE.getSendRequest();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute SEND_REQUEST__NAME = eINSTANCE.getSendRequest_Name();

    /**
     * The meta object literal for the '<em><b>Dest</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SEND_REQUEST__DEST = eINSTANCE.getSendRequest_Dest();

    /**
     * The meta object literal for the '<em><b>Msgref</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SEND_REQUEST__MSGREF = eINSTANCE.getSendRequest_Msgref();

    /**
     * The meta object literal for the '<em><b>Val</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SEND_REQUEST__VAL = eINSTANCE.getSendRequest_Val();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.ReplyToCallerImpl <em>Reply To Caller</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.ReplyToCallerImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getReplyToCaller()
     * @generated
     */
    EClass REPLY_TO_CALLER = eINSTANCE.getReplyToCaller();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute REPLY_TO_CALLER__NAME = eINSTANCE.getReplyToCaller_Name();

    /**
     * The meta object literal for the '<em><b>Msgref</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference REPLY_TO_CALLER__MSGREF = eINSTANCE.getReplyToCaller_Msgref();

    /**
     * The meta object literal for the '<em><b>Val</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference REPLY_TO_CALLER__VAL = eINSTANCE.getReplyToCaller_Val();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.ReceiveMsgImpl <em>Receive Msg</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.ReceiveMsgImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getReceiveMsg()
     * @generated
     */
    EClass RECEIVE_MSG = eINSTANCE.getReceiveMsg();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute RECEIVE_MSG__NAME = eINSTANCE.getReceiveMsg_Name();

    /**
     * The meta object literal for the '<em><b>Duration</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference RECEIVE_MSG__DURATION = eINSTANCE.getReceiveMsg_Duration();

    /**
     * The meta object literal for the '<em><b>Spec</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference RECEIVE_MSG__SPEC = eINSTANCE.getReceiveMsg_Spec();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.MsgSpecImpl <em>Msg Spec</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.MsgSpecImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getMsgSpec()
     * @generated
     */
    EClass MSG_SPEC = eINSTANCE.getMsgSpec();

    /**
     * The meta object literal for the '<em><b>Msg</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MSG_SPEC__MSG = eINSTANCE.getMsgSpec_Msg();

    /**
     * The meta object literal for the '<em><b>Sender</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MSG_SPEC__SENDER = eINSTANCE.getMsgSpec_Sender();

    /**
     * The meta object literal for the '<em><b>Content</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MSG_SPEC__CONTENT = eINSTANCE.getMsgSpec_Content();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.OnReceiveMsgImpl <em>On Receive Msg</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.OnReceiveMsgImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getOnReceiveMsg()
     * @generated
     */
    EClass ON_RECEIVE_MSG = eINSTANCE.getOnReceiveMsg();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ON_RECEIVE_MSG__NAME = eINSTANCE.getOnReceiveMsg_Name();

    /**
     * The meta object literal for the '<em><b>Msgid</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ON_RECEIVE_MSG__MSGID = eINSTANCE.getOnReceiveMsg_Msgid();

    /**
     * The meta object literal for the '<em><b>Msgtype</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ON_RECEIVE_MSG__MSGTYPE = eINSTANCE.getOnReceiveMsg_Msgtype();

    /**
     * The meta object literal for the '<em><b>Msgsender</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ON_RECEIVE_MSG__MSGSENDER = eINSTANCE.getOnReceiveMsg_Msgsender();

    /**
     * The meta object literal for the '<em><b>Msgreceiver</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ON_RECEIVE_MSG__MSGRECEIVER = eINSTANCE.getOnReceiveMsg_Msgreceiver();

    /**
     * The meta object literal for the '<em><b>Msgcontent</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ON_RECEIVE_MSG__MSGCONTENT = eINSTANCE.getOnReceiveMsg_Msgcontent();

    /**
     * The meta object literal for the '<em><b>Msgseqnum</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ON_RECEIVE_MSG__MSGSEQNUM = eINSTANCE.getOnReceiveMsg_Msgseqnum();

    /**
     * The meta object literal for the '<em><b>Duration</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ON_RECEIVE_MSG__DURATION = eINSTANCE.getOnReceiveMsg_Duration();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.MsgSelectImpl <em>Msg Select</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.MsgSelectImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getMsgSelect()
     * @generated
     */
    EClass MSG_SELECT = eINSTANCE.getMsgSelect();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute MSG_SELECT__NAME = eINSTANCE.getMsgSelect_Name();

    /**
     * The meta object literal for the '<em><b>Duration</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MSG_SELECT__DURATION = eINSTANCE.getMsgSelect_Duration();

    /**
     * The meta object literal for the '<em><b>Messages</b></em>' reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MSG_SELECT__MESSAGES = eINSTANCE.getMsgSelect_Messages();

    /**
     * The meta object literal for the '<em><b>Plans</b></em>' reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MSG_SELECT__PLANS = eINSTANCE.getMsgSelect_Plans();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.RaiseEventImpl <em>Raise Event</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.RaiseEventImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getRaiseEvent()
     * @generated
     */
    EClass RAISE_EVENT = eINSTANCE.getRaiseEvent();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute RAISE_EVENT__NAME = eINSTANCE.getRaiseEvent_Name();

    /**
     * The meta object literal for the '<em><b>Ev</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference RAISE_EVENT__EV = eINSTANCE.getRaiseEvent_Ev();

    /**
     * The meta object literal for the '<em><b>Content</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference RAISE_EVENT__CONTENT = eINSTANCE.getRaiseEvent_Content();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.SenseEventImpl <em>Sense Event</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.SenseEventImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getSenseEvent()
     * @generated
     */
    EClass SENSE_EVENT = eINSTANCE.getSenseEvent();

    /**
     * The meta object literal for the '<em><b>Duration</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SENSE_EVENT__DURATION = eINSTANCE.getSenseEvent_Duration();

    /**
     * The meta object literal for the '<em><b>Events</b></em>' reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SENSE_EVENT__EVENTS = eINSTANCE.getSenseEvent_Events();

    /**
     * The meta object literal for the '<em><b>Plans</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SENSE_EVENT__PLANS = eINSTANCE.getSenseEvent_Plans();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.MsgSwitchImpl <em>Msg Switch</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.MsgSwitchImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getMsgSwitch()
     * @generated
     */
    EClass MSG_SWITCH = eINSTANCE.getMsgSwitch();

    /**
     * The meta object literal for the '<em><b>Message</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MSG_SWITCH__MESSAGE = eINSTANCE.getMsgSwitch_Message();

    /**
     * The meta object literal for the '<em><b>Msg</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MSG_SWITCH__MSG = eINSTANCE.getMsgSwitch_Msg();

    /**
     * The meta object literal for the '<em><b>Move</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MSG_SWITCH__MOVE = eINSTANCE.getMsgSwitch_Move();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.EventSwitchImpl <em>Event Switch</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.EventSwitchImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getEventSwitch()
     * @generated
     */
    EClass EVENT_SWITCH = eINSTANCE.getEventSwitch();

    /**
     * The meta object literal for the '<em><b>Event</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EVENT_SWITCH__EVENT = eINSTANCE.getEventSwitch_Event();

    /**
     * The meta object literal for the '<em><b>Msg</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EVENT_SWITCH__MSG = eINSTANCE.getEventSwitch_Msg();

    /**
     * The meta object literal for the '<em><b>Move</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EVENT_SWITCH__MOVE = eINSTANCE.getEventSwitch_Move();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.ContinuationImpl <em>Continuation</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.ContinuationImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getContinuation()
     * @generated
     */
    EClass CONTINUATION = eINSTANCE.getContinuation();

    /**
     * The meta object literal for the '<em><b>Plan</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference CONTINUATION__PLAN = eINSTANCE.getContinuation_Plan();

    /**
     * The meta object literal for the '<em><b>Nane</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute CONTINUATION__NANE = eINSTANCE.getContinuation_Nane();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.ExtensionMoveImpl <em>Extension Move</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.ExtensionMoveImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getExtensionMove()
     * @generated
     */
    EClass EXTENSION_MOVE = eINSTANCE.getExtensionMove();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute EXTENSION_MOVE__NAME = eINSTANCE.getExtensionMove_Name();

    /**
     * The meta object literal for the '<em><b>Duration</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EXTENSION_MOVE__DURATION = eINSTANCE.getExtensionMove_Duration();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.PhotoImpl <em>Photo</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.PhotoImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getPhoto()
     * @generated
     */
    EClass PHOTO = eINSTANCE.getPhoto();

    /**
     * The meta object literal for the '<em><b>Destfile</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PHOTO__DESTFILE = eINSTANCE.getPhoto_Destfile();

    /**
     * The meta object literal for the '<em><b>Answer Event</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PHOTO__ANSWER_EVENT = eINSTANCE.getPhoto_AnswerEvent();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.SoundImpl <em>Sound</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.SoundImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getSound()
     * @generated
     */
    EClass SOUND = eINSTANCE.getSound();

    /**
     * The meta object literal for the '<em><b>Srcfile</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SOUND__SRCFILE = eINSTANCE.getSound_Srcfile();

    /**
     * The meta object literal for the '<em><b>Answer Event</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SOUND__ANSWER_EVENT = eINSTANCE.getSound_AnswerEvent();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.VideoImpl <em>Video</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.VideoImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getVideo()
     * @generated
     */
    EClass VIDEO = eINSTANCE.getVideo();

    /**
     * The meta object literal for the '<em><b>Destfile</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference VIDEO__DESTFILE = eINSTANCE.getVideo_Destfile();

    /**
     * The meta object literal for the '<em><b>Answer Event</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference VIDEO__ANSWER_EVENT = eINSTANCE.getVideo_AnswerEvent();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.DelayImpl <em>Delay</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.DelayImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getDelay()
     * @generated
     */
    EClass DELAY = eINSTANCE.getDelay();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.AnswerEventImpl <em>Answer Event</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.AnswerEventImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getAnswerEvent()
     * @generated
     */
    EClass ANSWER_EVENT = eINSTANCE.getAnswerEvent();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ANSWER_EVENT__NAME = eINSTANCE.getAnswerEvent_Name();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.EventHandlerImpl <em>Event Handler</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.EventHandlerImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getEventHandler()
     * @generated
     */
    EClass EVENT_HANDLER = eINSTANCE.getEventHandler();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute EVENT_HANDLER__NAME = eINSTANCE.getEventHandler_Name();

    /**
     * The meta object literal for the '<em><b>Events</b></em>' reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EVENT_HANDLER__EVENTS = eINSTANCE.getEventHandler_Events();

    /**
     * The meta object literal for the '<em><b>Print</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute EVENT_HANDLER__PRINT = eINSTANCE.getEventHandler_Print();

    /**
     * The meta object literal for the '<em><b>Body</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EVENT_HANDLER__BODY = eINSTANCE.getEventHandler_Body();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.EventHandlerBodyImpl <em>Event Handler Body</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.EventHandlerBodyImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getEventHandlerBody()
     * @generated
     */
    EClass EVENT_HANDLER_BODY = eINSTANCE.getEventHandlerBody();

    /**
     * The meta object literal for the '<em><b>Op</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EVENT_HANDLER_BODY__OP = eINSTANCE.getEventHandlerBody_Op();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.EventHandlerOperationImpl <em>Event Handler Operation</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.EventHandlerOperationImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getEventHandlerOperation()
     * @generated
     */
    EClass EVENT_HANDLER_OPERATION = eINSTANCE.getEventHandlerOperation();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.MemoOperationImpl <em>Memo Operation</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.MemoOperationImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getMemoOperation()
     * @generated
     */
    EClass MEMO_OPERATION = eINSTANCE.getMemoOperation();

    /**
     * The meta object literal for the '<em><b>Rule</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MEMO_OPERATION__RULE = eINSTANCE.getMemoOperation_Rule();

    /**
     * The meta object literal for the '<em><b>Actor</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MEMO_OPERATION__ACTOR = eINSTANCE.getMemoOperation_Actor();

    /**
     * The meta object literal for the '<em><b>Do Memo</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MEMO_OPERATION__DO_MEMO = eINSTANCE.getMemoOperation_DoMemo();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.SolveOperationImpl <em>Solve Operation</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.SolveOperationImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getSolveOperation()
     * @generated
     */
    EClass SOLVE_OPERATION = eINSTANCE.getSolveOperation();

    /**
     * The meta object literal for the '<em><b>Goal</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SOLVE_OPERATION__GOAL = eINSTANCE.getSolveOperation_Goal();

    /**
     * The meta object literal for the '<em><b>Actor</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SOLVE_OPERATION__ACTOR = eINSTANCE.getSolveOperation_Actor();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.SendEventAsDispatchImpl <em>Send Event As Dispatch</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.SendEventAsDispatchImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getSendEventAsDispatch()
     * @generated
     */
    EClass SEND_EVENT_AS_DISPATCH = eINSTANCE.getSendEventAsDispatch();

    /**
     * The meta object literal for the '<em><b>Actor</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SEND_EVENT_AS_DISPATCH__ACTOR = eINSTANCE.getSendEventAsDispatch_Actor();

    /**
     * The meta object literal for the '<em><b>Msgref</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SEND_EVENT_AS_DISPATCH__MSGREF = eINSTANCE.getSendEventAsDispatch_Msgref();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.MemoRuleImpl <em>Memo Rule</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.MemoRuleImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getMemoRule()
     * @generated
     */
    EClass MEMO_RULE = eINSTANCE.getMemoRule();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.MemoEventImpl <em>Memo Event</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.MemoEventImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getMemoEvent()
     * @generated
     */
    EClass MEMO_EVENT = eINSTANCE.getMemoEvent();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute MEMO_EVENT__NAME = eINSTANCE.getMemoEvent_Name();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.ReactionImpl <em>Reaction</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.ReactionImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getReaction()
     * @generated
     */
    EClass REACTION = eINSTANCE.getReaction();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute REACTION__NAME = eINSTANCE.getReaction_Name();

    /**
     * The meta object literal for the '<em><b>Alarms</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference REACTION__ALARMS = eINSTANCE.getReaction_Alarms();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.AlarmEventImpl <em>Alarm Event</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.AlarmEventImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getAlarmEvent()
     * @generated
     */
    EClass ALARM_EVENT = eINSTANCE.getAlarmEvent();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ALARM_EVENT__NAME = eINSTANCE.getAlarmEvent_Name();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.NormalEventImpl <em>Normal Event</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.NormalEventImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getNormalEvent()
     * @generated
     */
    EClass NORMAL_EVENT = eINSTANCE.getNormalEvent();

    /**
     * The meta object literal for the '<em><b>Ev</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference NORMAL_EVENT__EV = eINSTANCE.getNormalEvent_Ev();

    /**
     * The meta object literal for the '<em><b>Plan Ref</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference NORMAL_EVENT__PLAN_REF = eINSTANCE.getNormalEvent_PlanRef();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.ContinueEventImpl <em>Continue Event</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.ContinueEventImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getContinueEvent()
     * @generated
     */
    EClass CONTINUE_EVENT = eINSTANCE.getContinueEvent();

    /**
     * The meta object literal for the '<em><b>Ev Occur</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference CONTINUE_EVENT__EV_OCCUR = eINSTANCE.getContinueEvent_EvOccur();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.VarOrQactorImpl <em>Var Or Qactor</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.VarOrQactorImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getVarOrQactor()
     * @generated
     */
    EClass VAR_OR_QACTOR = eINSTANCE.getVarOrQactor();

    /**
     * The meta object literal for the '<em><b>Var</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference VAR_OR_QACTOR__VAR = eINSTANCE.getVarOrQactor_Var();

    /**
     * The meta object literal for the '<em><b>Dest</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference VAR_OR_QACTOR__DEST = eINSTANCE.getVarOrQactor_Dest();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.VarOrAtomicImpl <em>Var Or Atomic</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.VarOrAtomicImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getVarOrAtomic()
     * @generated
     */
    EClass VAR_OR_ATOMIC = eINSTANCE.getVarOrAtomic();

    /**
     * The meta object literal for the '<em><b>Var</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference VAR_OR_ATOMIC__VAR = eINSTANCE.getVarOrAtomic_Var();

    /**
     * The meta object literal for the '<em><b>Const</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference VAR_OR_ATOMIC__CONST = eINSTANCE.getVarOrAtomic_Const();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.VarOrStringImpl <em>Var Or String</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.VarOrStringImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getVarOrString()
     * @generated
     */
    EClass VAR_OR_STRING = eINSTANCE.getVarOrString();

    /**
     * The meta object literal for the '<em><b>Var</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference VAR_OR_STRING__VAR = eINSTANCE.getVarOrString_Var();

    /**
     * The meta object literal for the '<em><b>Const</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute VAR_OR_STRING__CONST = eINSTANCE.getVarOrString_Const();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.VarOrPStructImpl <em>Var Or PStruct</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.VarOrPStructImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getVarOrPStruct()
     * @generated
     */
    EClass VAR_OR_PSTRUCT = eINSTANCE.getVarOrPStruct();

    /**
     * The meta object literal for the '<em><b>Var</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference VAR_OR_PSTRUCT__VAR = eINSTANCE.getVarOrPStruct_Var();

    /**
     * The meta object literal for the '<em><b>Psrtuct</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference VAR_OR_PSTRUCT__PSRTUCT = eINSTANCE.getVarOrPStruct_Psrtuct();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.VarOrPheadImpl <em>Var Or Phead</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.VarOrPheadImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getVarOrPhead()
     * @generated
     */
    EClass VAR_OR_PHEAD = eINSTANCE.getVarOrPhead();

    /**
     * The meta object literal for the '<em><b>Var</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference VAR_OR_PHEAD__VAR = eINSTANCE.getVarOrPhead_Var();

    /**
     * The meta object literal for the '<em><b>Phead</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference VAR_OR_PHEAD__PHEAD = eINSTANCE.getVarOrPhead_Phead();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.VarOrAtomOrPStructImpl <em>Var Or Atom Or PStruct</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.VarOrAtomOrPStructImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getVarOrAtomOrPStruct()
     * @generated
     */
    EClass VAR_OR_ATOM_OR_PSTRUCT = eINSTANCE.getVarOrAtomOrPStruct();

    /**
     * The meta object literal for the '<em><b>Var</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference VAR_OR_ATOM_OR_PSTRUCT__VAR = eINSTANCE.getVarOrAtomOrPStruct_Var();

    /**
     * The meta object literal for the '<em><b>Psrtuct</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference VAR_OR_ATOM_OR_PSTRUCT__PSRTUCT = eINSTANCE.getVarOrAtomOrPStruct_Psrtuct();

    /**
     * The meta object literal for the '<em><b>Atom</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference VAR_OR_ATOM_OR_PSTRUCT__ATOM = eINSTANCE.getVarOrAtomOrPStruct_Atom();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.VariableImpl <em>Variable</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.VariableImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getVariable()
     * @generated
     */
    EClass VARIABLE = eINSTANCE.getVariable();

    /**
     * The meta object literal for the '<em><b>Guardvar</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute VARIABLE__GUARDVAR = eINSTANCE.getVariable_Guardvar();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute VARIABLE__NAME = eINSTANCE.getVariable_Name();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.TimeLimitImpl <em>Time Limit</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.TimeLimitImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getTimeLimit()
     * @generated
     */
    EClass TIME_LIMIT = eINSTANCE.getTimeLimit();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute TIME_LIMIT__NAME = eINSTANCE.getTimeLimit_Name();

    /**
     * The meta object literal for the '<em><b>Msec</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute TIME_LIMIT__MSEC = eINSTANCE.getTimeLimit_Msec();

    /**
     * The meta object literal for the '<em><b>Var</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference TIME_LIMIT__VAR = eINSTANCE.getTimeLimit_Var();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.ComponentIPImpl <em>Component IP</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.ComponentIPImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getComponentIP()
     * @generated
     */
    EClass COMPONENT_IP = eINSTANCE.getComponentIP();

    /**
     * The meta object literal for the '<em><b>Host</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute COMPONENT_IP__HOST = eINSTANCE.getComponentIP_Host();

    /**
     * The meta object literal for the '<em><b>Port</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute COMPONENT_IP__PORT = eINSTANCE.getComponentIP_Port();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.impl.MoveFileImpl <em>Move File</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.impl.MoveFileImpl
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getMoveFile()
     * @generated
     */
    EClass MOVE_FILE = eINSTANCE.getMoveFile();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute MOVE_FILE__NAME = eINSTANCE.getMoveFile_Name();

    /**
     * The meta object literal for the '<em><b>Fname</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MOVE_FILE__FNAME = eINSTANCE.getMoveFile_Fname();

    /**
     * The meta object literal for the '{@link it.unibo.xtext.qactor.WindowColor <em>Window Color</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see it.unibo.xtext.qactor.WindowColor
     * @see it.unibo.xtext.qactor.impl.QactorPackageImpl#getWindowColor()
     * @generated
     */
    EEnum WINDOW_COLOR = eINSTANCE.getWindowColor();

  }

} //QactorPackage
