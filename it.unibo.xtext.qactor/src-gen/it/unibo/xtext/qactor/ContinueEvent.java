/**
 */
package it.unibo.xtext.qactor;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Continue Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link it.unibo.xtext.qactor.ContinueEvent#getEvOccur <em>Ev Occur</em>}</li>
 * </ul>
 * </p>
 *
 * @see it.unibo.xtext.qactor.QactorPackage#getContinueEvent()
 * @model
 * @generated
 */
public interface ContinueEvent extends AlarmEvent
{
  /**
   * Returns the value of the '<em><b>Ev Occur</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ev Occur</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ev Occur</em>' reference.
   * @see #setEvOccur(Event)
   * @see it.unibo.xtext.qactor.QactorPackage#getContinueEvent_EvOccur()
   * @model
   * @generated
   */
  Event getEvOccur();

  /**
   * Sets the value of the '{@link it.unibo.xtext.qactor.ContinueEvent#getEvOccur <em>Ev Occur</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ev Occur</em>' reference.
   * @see #getEvOccur()
   * @generated
   */
  void setEvOccur(Event value);

} // ContinueEvent
