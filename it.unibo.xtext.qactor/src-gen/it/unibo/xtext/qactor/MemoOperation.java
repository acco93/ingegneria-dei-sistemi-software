/**
 */
package it.unibo.xtext.qactor;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Memo Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link it.unibo.xtext.qactor.MemoOperation#getRule <em>Rule</em>}</li>
 *   <li>{@link it.unibo.xtext.qactor.MemoOperation#getActor <em>Actor</em>}</li>
 *   <li>{@link it.unibo.xtext.qactor.MemoOperation#getDoMemo <em>Do Memo</em>}</li>
 * </ul>
 * </p>
 *
 * @see it.unibo.xtext.qactor.QactorPackage#getMemoOperation()
 * @model
 * @generated
 */
public interface MemoOperation extends EventHandlerOperation
{
  /**
   * Returns the value of the '<em><b>Rule</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Rule</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Rule</em>' containment reference.
   * @see #setRule(MemoRule)
   * @see it.unibo.xtext.qactor.QactorPackage#getMemoOperation_Rule()
   * @model containment="true"
   * @generated
   */
  MemoRule getRule();

  /**
   * Sets the value of the '{@link it.unibo.xtext.qactor.MemoOperation#getRule <em>Rule</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Rule</em>' containment reference.
   * @see #getRule()
   * @generated
   */
  void setRule(MemoRule value);

  /**
   * Returns the value of the '<em><b>Actor</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Actor</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Actor</em>' reference.
   * @see #setActor(QActor)
   * @see it.unibo.xtext.qactor.QactorPackage#getMemoOperation_Actor()
   * @model
   * @generated
   */
  QActor getActor();

  /**
   * Sets the value of the '{@link it.unibo.xtext.qactor.MemoOperation#getActor <em>Actor</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Actor</em>' reference.
   * @see #getActor()
   * @generated
   */
  void setActor(QActor value);

  /**
   * Returns the value of the '<em><b>Do Memo</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Do Memo</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Do Memo</em>' containment reference.
   * @see #setDoMemo(MemoCurrentEvent)
   * @see it.unibo.xtext.qactor.QactorPackage#getMemoOperation_DoMemo()
   * @model containment="true"
   * @generated
   */
  MemoCurrentEvent getDoMemo();

  /**
   * Sets the value of the '{@link it.unibo.xtext.qactor.MemoOperation#getDoMemo <em>Do Memo</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Do Memo</em>' containment reference.
   * @see #getDoMemo()
   * @generated
   */
  void setDoMemo(MemoCurrentEvent value);

} // MemoOperation
