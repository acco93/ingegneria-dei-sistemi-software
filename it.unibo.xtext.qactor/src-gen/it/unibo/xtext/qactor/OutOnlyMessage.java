/**
 */
package it.unibo.xtext.qactor;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Out Only Message</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see it.unibo.xtext.qactor.QactorPackage#getOutOnlyMessage()
 * @model
 * @generated
 */
public interface OutOnlyMessage extends Message
{
} // OutOnlyMessage
