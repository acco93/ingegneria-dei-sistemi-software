/**
 */
package it.unibo.xtext.qactor;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>PTerm</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see it.unibo.xtext.qactor.QactorPackage#getPTerm()
 * @model
 * @generated
 */
public interface PTerm extends EObject
{
} // PTerm
