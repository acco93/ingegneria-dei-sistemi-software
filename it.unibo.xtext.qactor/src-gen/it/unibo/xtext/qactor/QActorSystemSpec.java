/**
 */
package it.unibo.xtext.qactor;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>QActor System Spec</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link it.unibo.xtext.qactor.QActorSystemSpec#getName <em>Name</em>}</li>
 *   <li>{@link it.unibo.xtext.qactor.QActorSystemSpec#isTesting <em>Testing</em>}</li>
 *   <li>{@link it.unibo.xtext.qactor.QActorSystemSpec#getMessage <em>Message</em>}</li>
 *   <li>{@link it.unibo.xtext.qactor.QActorSystemSpec#getContext <em>Context</em>}</li>
 *   <li>{@link it.unibo.xtext.qactor.QActorSystemSpec#getActor <em>Actor</em>}</li>
 *   <li>{@link it.unibo.xtext.qactor.QActorSystemSpec#getRobot <em>Robot</em>}</li>
 * </ul>
 * </p>
 *
 * @see it.unibo.xtext.qactor.QactorPackage#getQActorSystemSpec()
 * @model
 * @generated
 */
public interface QActorSystemSpec extends EObject
{
  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see it.unibo.xtext.qactor.QactorPackage#getQActorSystemSpec_Name()
   * @model
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link it.unibo.xtext.qactor.QActorSystemSpec#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

  /**
   * Returns the value of the '<em><b>Testing</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Testing</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Testing</em>' attribute.
   * @see #setTesting(boolean)
   * @see it.unibo.xtext.qactor.QactorPackage#getQActorSystemSpec_Testing()
   * @model
   * @generated
   */
  boolean isTesting();

  /**
   * Sets the value of the '{@link it.unibo.xtext.qactor.QActorSystemSpec#isTesting <em>Testing</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Testing</em>' attribute.
   * @see #isTesting()
   * @generated
   */
  void setTesting(boolean value);

  /**
   * Returns the value of the '<em><b>Message</b></em>' containment reference list.
   * The list contents are of type {@link it.unibo.xtext.qactor.Message}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Message</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Message</em>' containment reference list.
   * @see it.unibo.xtext.qactor.QactorPackage#getQActorSystemSpec_Message()
   * @model containment="true"
   * @generated
   */
  EList<Message> getMessage();

  /**
   * Returns the value of the '<em><b>Context</b></em>' containment reference list.
   * The list contents are of type {@link it.unibo.xtext.qactor.Context}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Context</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Context</em>' containment reference list.
   * @see it.unibo.xtext.qactor.QactorPackage#getQActorSystemSpec_Context()
   * @model containment="true"
   * @generated
   */
  EList<Context> getContext();

  /**
   * Returns the value of the '<em><b>Actor</b></em>' containment reference list.
   * The list contents are of type {@link it.unibo.xtext.qactor.QActor}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Actor</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Actor</em>' containment reference list.
   * @see it.unibo.xtext.qactor.QactorPackage#getQActorSystemSpec_Actor()
   * @model containment="true"
   * @generated
   */
  EList<QActor> getActor();

  /**
   * Returns the value of the '<em><b>Robot</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Robot</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Robot</em>' containment reference.
   * @see #setRobot(Robot)
   * @see it.unibo.xtext.qactor.QactorPackage#getQActorSystemSpec_Robot()
   * @model containment="true"
   * @generated
   */
  Robot getRobot();

  /**
   * Sets the value of the '{@link it.unibo.xtext.qactor.QActorSystemSpec#getRobot <em>Robot</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Robot</em>' containment reference.
   * @see #getRobot()
   * @generated
   */
  void setRobot(Robot value);

} // QActorSystemSpec
