/**
 */
package it.unibo.xtext.qactor;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>PIs</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link it.unibo.xtext.qactor.PIs#getVarout <em>Varout</em>}</li>
 *   <li>{@link it.unibo.xtext.qactor.PIs#getVarin <em>Varin</em>}</li>
 *   <li>{@link it.unibo.xtext.qactor.PIs#getNum <em>Num</em>}</li>
 * </ul>
 * </p>
 *
 * @see it.unibo.xtext.qactor.QactorPackage#getPIs()
 * @model
 * @generated
 */
public interface PIs extends PPredef
{
  /**
   * Returns the value of the '<em><b>Varout</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Varout</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Varout</em>' containment reference.
   * @see #setVarout(Variable)
   * @see it.unibo.xtext.qactor.QactorPackage#getPIs_Varout()
   * @model containment="true"
   * @generated
   */
  Variable getVarout();

  /**
   * Sets the value of the '{@link it.unibo.xtext.qactor.PIs#getVarout <em>Varout</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Varout</em>' containment reference.
   * @see #getVarout()
   * @generated
   */
  void setVarout(Variable value);

  /**
   * Returns the value of the '<em><b>Varin</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Varin</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Varin</em>' containment reference.
   * @see #setVarin(Variable)
   * @see it.unibo.xtext.qactor.QactorPackage#getPIs_Varin()
   * @model containment="true"
   * @generated
   */
  Variable getVarin();

  /**
   * Sets the value of the '{@link it.unibo.xtext.qactor.PIs#getVarin <em>Varin</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Varin</em>' containment reference.
   * @see #getVarin()
   * @generated
   */
  void setVarin(Variable value);

  /**
   * Returns the value of the '<em><b>Num</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Num</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Num</em>' containment reference.
   * @see #setNum(PAtomNum)
   * @see it.unibo.xtext.qactor.QactorPackage#getPIs_Num()
   * @model containment="true"
   * @generated
   */
  PAtomNum getNum();

  /**
   * Sets the value of the '{@link it.unibo.xtext.qactor.PIs#getNum <em>Num</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Num</em>' containment reference.
   * @see #getNum()
   * @generated
   */
  void setNum(PAtomNum value);

} // PIs
