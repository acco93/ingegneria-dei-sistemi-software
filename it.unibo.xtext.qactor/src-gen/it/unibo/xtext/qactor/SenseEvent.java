/**
 */
package it.unibo.xtext.qactor;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Sense Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link it.unibo.xtext.qactor.SenseEvent#getDuration <em>Duration</em>}</li>
 *   <li>{@link it.unibo.xtext.qactor.SenseEvent#getEvents <em>Events</em>}</li>
 *   <li>{@link it.unibo.xtext.qactor.SenseEvent#getPlans <em>Plans</em>}</li>
 * </ul>
 * </p>
 *
 * @see it.unibo.xtext.qactor.QactorPackage#getSenseEvent()
 * @model
 * @generated
 */
public interface SenseEvent extends MessageMove
{
  /**
   * Returns the value of the '<em><b>Duration</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Duration</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Duration</em>' containment reference.
   * @see #setDuration(TimeLimit)
   * @see it.unibo.xtext.qactor.QactorPackage#getSenseEvent_Duration()
   * @model containment="true"
   * @generated
   */
  TimeLimit getDuration();

  /**
   * Sets the value of the '{@link it.unibo.xtext.qactor.SenseEvent#getDuration <em>Duration</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Duration</em>' containment reference.
   * @see #getDuration()
   * @generated
   */
  void setDuration(TimeLimit value);

  /**
   * Returns the value of the '<em><b>Events</b></em>' reference list.
   * The list contents are of type {@link it.unibo.xtext.qactor.Event}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Events</em>' reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Events</em>' reference list.
   * @see it.unibo.xtext.qactor.QactorPackage#getSenseEvent_Events()
   * @model
   * @generated
   */
  EList<Event> getEvents();

  /**
   * Returns the value of the '<em><b>Plans</b></em>' containment reference list.
   * The list contents are of type {@link it.unibo.xtext.qactor.Continuation}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Plans</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Plans</em>' containment reference list.
   * @see it.unibo.xtext.qactor.QactorPackage#getSenseEvent_Plans()
   * @model containment="true"
   * @generated
   */
  EList<Continuation> getPlans();

} // SenseEvent
