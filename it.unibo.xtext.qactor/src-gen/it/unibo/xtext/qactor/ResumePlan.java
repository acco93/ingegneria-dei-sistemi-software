/**
 */
package it.unibo.xtext.qactor;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Resume Plan</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see it.unibo.xtext.qactor.QactorPackage#getResumePlan()
 * @model
 * @generated
 */
public interface ResumePlan extends PlanMove
{
} // ResumePlan
