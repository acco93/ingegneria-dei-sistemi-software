/**
 */
package it.unibo.xtext.qactor;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Suspend Plan</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see it.unibo.xtext.qactor.QactorPackage#getSuspendPlan()
 * @model
 * @generated
 */
public interface SuspendPlan extends PlanMove
{
} // SuspendPlan
