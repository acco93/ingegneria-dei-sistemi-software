/**
 */
package it.unibo.xtext.qactor;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Remove Rule</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see it.unibo.xtext.qactor.QactorPackage#getRemoveRule()
 * @model
 * @generated
 */
public interface RemoveRule extends GuardMove
{
} // RemoveRule
