/**
 */
package it.unibo.xtext.qactor;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Var Or String</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link it.unibo.xtext.qactor.VarOrString#getVar <em>Var</em>}</li>
 *   <li>{@link it.unibo.xtext.qactor.VarOrString#getConst <em>Const</em>}</li>
 * </ul>
 * </p>
 *
 * @see it.unibo.xtext.qactor.QactorPackage#getVarOrString()
 * @model
 * @generated
 */
public interface VarOrString extends EObject
{
  /**
   * Returns the value of the '<em><b>Var</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Var</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Var</em>' containment reference.
   * @see #setVar(Variable)
   * @see it.unibo.xtext.qactor.QactorPackage#getVarOrString_Var()
   * @model containment="true"
   * @generated
   */
  Variable getVar();

  /**
   * Sets the value of the '{@link it.unibo.xtext.qactor.VarOrString#getVar <em>Var</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Var</em>' containment reference.
   * @see #getVar()
   * @generated
   */
  void setVar(Variable value);

  /**
   * Returns the value of the '<em><b>Const</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Const</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Const</em>' attribute.
   * @see #setConst(String)
   * @see it.unibo.xtext.qactor.QactorPackage#getVarOrString_Const()
   * @model
   * @generated
   */
  String getConst();

  /**
   * Sets the value of the '{@link it.unibo.xtext.qactor.VarOrString#getConst <em>Const</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Const</em>' attribute.
   * @see #getConst()
   * @generated
   */
  void setConst(String value);

} // VarOrString
