/**
 */
package it.unibo.xtext.qactor;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Msg Spec</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link it.unibo.xtext.qactor.MsgSpec#getMsg <em>Msg</em>}</li>
 *   <li>{@link it.unibo.xtext.qactor.MsgSpec#getSender <em>Sender</em>}</li>
 *   <li>{@link it.unibo.xtext.qactor.MsgSpec#getContent <em>Content</em>}</li>
 * </ul>
 * </p>
 *
 * @see it.unibo.xtext.qactor.QactorPackage#getMsgSpec()
 * @model
 * @generated
 */
public interface MsgSpec extends EObject
{
  /**
   * Returns the value of the '<em><b>Msg</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Msg</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Msg</em>' reference.
   * @see #setMsg(Message)
   * @see it.unibo.xtext.qactor.QactorPackage#getMsgSpec_Msg()
   * @model
   * @generated
   */
  Message getMsg();

  /**
   * Sets the value of the '{@link it.unibo.xtext.qactor.MsgSpec#getMsg <em>Msg</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Msg</em>' reference.
   * @see #getMsg()
   * @generated
   */
  void setMsg(Message value);

  /**
   * Returns the value of the '<em><b>Sender</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Sender</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Sender</em>' containment reference.
   * @see #setSender(VarOrAtomic)
   * @see it.unibo.xtext.qactor.QactorPackage#getMsgSpec_Sender()
   * @model containment="true"
   * @generated
   */
  VarOrAtomic getSender();

  /**
   * Sets the value of the '{@link it.unibo.xtext.qactor.MsgSpec#getSender <em>Sender</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Sender</em>' containment reference.
   * @see #getSender()
   * @generated
   */
  void setSender(VarOrAtomic value);

  /**
   * Returns the value of the '<em><b>Content</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Content</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Content</em>' containment reference.
   * @see #setContent(PHead)
   * @see it.unibo.xtext.qactor.QactorPackage#getMsgSpec_Content()
   * @model containment="true"
   * @generated
   */
  PHead getContent();

  /**
   * Sets the value of the '{@link it.unibo.xtext.qactor.MsgSpec#getContent <em>Content</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Content</em>' containment reference.
   * @see #getContent()
   * @generated
   */
  void setContent(PHead value);

} // MsgSpec
