/**
 */
package it.unibo.xtext.qactor;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Raise Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link it.unibo.xtext.qactor.RaiseEvent#getName <em>Name</em>}</li>
 *   <li>{@link it.unibo.xtext.qactor.RaiseEvent#getEv <em>Ev</em>}</li>
 *   <li>{@link it.unibo.xtext.qactor.RaiseEvent#getContent <em>Content</em>}</li>
 * </ul>
 * </p>
 *
 * @see it.unibo.xtext.qactor.QactorPackage#getRaiseEvent()
 * @model
 * @generated
 */
public interface RaiseEvent extends MessageMove, EventHandlerOperation
{
  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see it.unibo.xtext.qactor.QactorPackage#getRaiseEvent_Name()
   * @model
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link it.unibo.xtext.qactor.RaiseEvent#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

  /**
   * Returns the value of the '<em><b>Ev</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ev</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ev</em>' reference.
   * @see #setEv(Event)
   * @see it.unibo.xtext.qactor.QactorPackage#getRaiseEvent_Ev()
   * @model
   * @generated
   */
  Event getEv();

  /**
   * Sets the value of the '{@link it.unibo.xtext.qactor.RaiseEvent#getEv <em>Ev</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ev</em>' reference.
   * @see #getEv()
   * @generated
   */
  void setEv(Event value);

  /**
   * Returns the value of the '<em><b>Content</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Content</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Content</em>' containment reference.
   * @see #setContent(PHead)
   * @see it.unibo.xtext.qactor.QactorPackage#getRaiseEvent_Content()
   * @model containment="true"
   * @generated
   */
  PHead getContent();

  /**
   * Sets the value of the '{@link it.unibo.xtext.qactor.RaiseEvent#getContent <em>Content</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Content</em>' containment reference.
   * @see #getContent()
   * @generated
   */
  void setContent(PHead value);

} // RaiseEvent
