/**
 */
package it.unibo.xtext.qactor;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Msg Select</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link it.unibo.xtext.qactor.MsgSelect#getName <em>Name</em>}</li>
 *   <li>{@link it.unibo.xtext.qactor.MsgSelect#getDuration <em>Duration</em>}</li>
 *   <li>{@link it.unibo.xtext.qactor.MsgSelect#getMessages <em>Messages</em>}</li>
 *   <li>{@link it.unibo.xtext.qactor.MsgSelect#getPlans <em>Plans</em>}</li>
 * </ul>
 * </p>
 *
 * @see it.unibo.xtext.qactor.QactorPackage#getMsgSelect()
 * @model
 * @generated
 */
public interface MsgSelect extends MessageMove
{
  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see it.unibo.xtext.qactor.QactorPackage#getMsgSelect_Name()
   * @model
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link it.unibo.xtext.qactor.MsgSelect#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

  /**
   * Returns the value of the '<em><b>Duration</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Duration</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Duration</em>' containment reference.
   * @see #setDuration(TimeLimit)
   * @see it.unibo.xtext.qactor.QactorPackage#getMsgSelect_Duration()
   * @model containment="true"
   * @generated
   */
  TimeLimit getDuration();

  /**
   * Sets the value of the '{@link it.unibo.xtext.qactor.MsgSelect#getDuration <em>Duration</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Duration</em>' containment reference.
   * @see #getDuration()
   * @generated
   */
  void setDuration(TimeLimit value);

  /**
   * Returns the value of the '<em><b>Messages</b></em>' reference list.
   * The list contents are of type {@link it.unibo.xtext.qactor.Message}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Messages</em>' reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Messages</em>' reference list.
   * @see it.unibo.xtext.qactor.QactorPackage#getMsgSelect_Messages()
   * @model
   * @generated
   */
  EList<Message> getMessages();

  /**
   * Returns the value of the '<em><b>Plans</b></em>' reference list.
   * The list contents are of type {@link it.unibo.xtext.qactor.Plan}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Plans</em>' reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Plans</em>' reference list.
   * @see it.unibo.xtext.qactor.QactorPackage#getMsgSelect_Plans()
   * @model
   * @generated
   */
  EList<Plan> getPlans();

} // MsgSelect
