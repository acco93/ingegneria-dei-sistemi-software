/**
 */
package it.unibo.xtext.qactor;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Event Handler Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see it.unibo.xtext.qactor.QactorPackage#getEventHandlerOperation()
 * @model
 * @generated
 */
public interface EventHandlerOperation extends EObject
{
} // EventHandlerOperation
