/**
 */
package it.unibo.xtext.qactor;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Continuation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link it.unibo.xtext.qactor.Continuation#getPlan <em>Plan</em>}</li>
 *   <li>{@link it.unibo.xtext.qactor.Continuation#getNane <em>Nane</em>}</li>
 * </ul>
 * </p>
 *
 * @see it.unibo.xtext.qactor.QactorPackage#getContinuation()
 * @model
 * @generated
 */
public interface Continuation extends EObject
{
  /**
   * Returns the value of the '<em><b>Plan</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Plan</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Plan</em>' reference.
   * @see #setPlan(Plan)
   * @see it.unibo.xtext.qactor.QactorPackage#getContinuation_Plan()
   * @model
   * @generated
   */
  Plan getPlan();

  /**
   * Sets the value of the '{@link it.unibo.xtext.qactor.Continuation#getPlan <em>Plan</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Plan</em>' reference.
   * @see #getPlan()
   * @generated
   */
  void setPlan(Plan value);

  /**
   * Returns the value of the '<em><b>Nane</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Nane</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Nane</em>' attribute.
   * @see #setNane(String)
   * @see it.unibo.xtext.qactor.QactorPackage#getContinuation_Nane()
   * @model
   * @generated
   */
  String getNane();

  /**
   * Sets the value of the '{@link it.unibo.xtext.qactor.Continuation#getNane <em>Nane</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Nane</em>' attribute.
   * @see #getNane()
   * @generated
   */
  void setNane(String value);

} // Continuation
