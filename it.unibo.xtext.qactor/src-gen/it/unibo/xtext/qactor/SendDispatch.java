/**
 */
package it.unibo.xtext.qactor;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Send Dispatch</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link it.unibo.xtext.qactor.SendDispatch#getName <em>Name</em>}</li>
 *   <li>{@link it.unibo.xtext.qactor.SendDispatch#getDest <em>Dest</em>}</li>
 *   <li>{@link it.unibo.xtext.qactor.SendDispatch#getMsgref <em>Msgref</em>}</li>
 *   <li>{@link it.unibo.xtext.qactor.SendDispatch#getVal <em>Val</em>}</li>
 * </ul>
 * </p>
 *
 * @see it.unibo.xtext.qactor.QactorPackage#getSendDispatch()
 * @model
 * @generated
 */
public interface SendDispatch extends MessageMove
{
  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see it.unibo.xtext.qactor.QactorPackage#getSendDispatch_Name()
   * @model
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link it.unibo.xtext.qactor.SendDispatch#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

  /**
   * Returns the value of the '<em><b>Dest</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Dest</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Dest</em>' containment reference.
   * @see #setDest(VarOrQactor)
   * @see it.unibo.xtext.qactor.QactorPackage#getSendDispatch_Dest()
   * @model containment="true"
   * @generated
   */
  VarOrQactor getDest();

  /**
   * Sets the value of the '{@link it.unibo.xtext.qactor.SendDispatch#getDest <em>Dest</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Dest</em>' containment reference.
   * @see #getDest()
   * @generated
   */
  void setDest(VarOrQactor value);

  /**
   * Returns the value of the '<em><b>Msgref</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Msgref</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Msgref</em>' reference.
   * @see #setMsgref(Message)
   * @see it.unibo.xtext.qactor.QactorPackage#getSendDispatch_Msgref()
   * @model
   * @generated
   */
  Message getMsgref();

  /**
   * Sets the value of the '{@link it.unibo.xtext.qactor.SendDispatch#getMsgref <em>Msgref</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Msgref</em>' reference.
   * @see #getMsgref()
   * @generated
   */
  void setMsgref(Message value);

  /**
   * Returns the value of the '<em><b>Val</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Val</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Val</em>' containment reference.
   * @see #setVal(PHead)
   * @see it.unibo.xtext.qactor.QactorPackage#getSendDispatch_Val()
   * @model containment="true"
   * @generated
   */
  PHead getVal();

  /**
   * Sets the value of the '{@link it.unibo.xtext.qactor.SendDispatch#getVal <em>Val</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Val</em>' containment reference.
   * @see #getVal()
   * @generated
   */
  void setVal(PHead value);

} // SendDispatch
