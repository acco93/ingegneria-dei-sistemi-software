/**
 */
package it.unibo.xtext.qactor;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>On Receive Msg</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link it.unibo.xtext.qactor.OnReceiveMsg#getName <em>Name</em>}</li>
 *   <li>{@link it.unibo.xtext.qactor.OnReceiveMsg#getMsgid <em>Msgid</em>}</li>
 *   <li>{@link it.unibo.xtext.qactor.OnReceiveMsg#getMsgtype <em>Msgtype</em>}</li>
 *   <li>{@link it.unibo.xtext.qactor.OnReceiveMsg#getMsgsender <em>Msgsender</em>}</li>
 *   <li>{@link it.unibo.xtext.qactor.OnReceiveMsg#getMsgreceiver <em>Msgreceiver</em>}</li>
 *   <li>{@link it.unibo.xtext.qactor.OnReceiveMsg#getMsgcontent <em>Msgcontent</em>}</li>
 *   <li>{@link it.unibo.xtext.qactor.OnReceiveMsg#getMsgseqnum <em>Msgseqnum</em>}</li>
 *   <li>{@link it.unibo.xtext.qactor.OnReceiveMsg#getDuration <em>Duration</em>}</li>
 * </ul>
 * </p>
 *
 * @see it.unibo.xtext.qactor.QactorPackage#getOnReceiveMsg()
 * @model
 * @generated
 */
public interface OnReceiveMsg extends MessageMove
{
  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see it.unibo.xtext.qactor.QactorPackage#getOnReceiveMsg_Name()
   * @model
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link it.unibo.xtext.qactor.OnReceiveMsg#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

  /**
   * Returns the value of the '<em><b>Msgid</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Msgid</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Msgid</em>' containment reference.
   * @see #setMsgid(PHead)
   * @see it.unibo.xtext.qactor.QactorPackage#getOnReceiveMsg_Msgid()
   * @model containment="true"
   * @generated
   */
  PHead getMsgid();

  /**
   * Sets the value of the '{@link it.unibo.xtext.qactor.OnReceiveMsg#getMsgid <em>Msgid</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Msgid</em>' containment reference.
   * @see #getMsgid()
   * @generated
   */
  void setMsgid(PHead value);

  /**
   * Returns the value of the '<em><b>Msgtype</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Msgtype</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Msgtype</em>' containment reference.
   * @see #setMsgtype(PHead)
   * @see it.unibo.xtext.qactor.QactorPackage#getOnReceiveMsg_Msgtype()
   * @model containment="true"
   * @generated
   */
  PHead getMsgtype();

  /**
   * Sets the value of the '{@link it.unibo.xtext.qactor.OnReceiveMsg#getMsgtype <em>Msgtype</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Msgtype</em>' containment reference.
   * @see #getMsgtype()
   * @generated
   */
  void setMsgtype(PHead value);

  /**
   * Returns the value of the '<em><b>Msgsender</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Msgsender</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Msgsender</em>' containment reference.
   * @see #setMsgsender(PHead)
   * @see it.unibo.xtext.qactor.QactorPackage#getOnReceiveMsg_Msgsender()
   * @model containment="true"
   * @generated
   */
  PHead getMsgsender();

  /**
   * Sets the value of the '{@link it.unibo.xtext.qactor.OnReceiveMsg#getMsgsender <em>Msgsender</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Msgsender</em>' containment reference.
   * @see #getMsgsender()
   * @generated
   */
  void setMsgsender(PHead value);

  /**
   * Returns the value of the '<em><b>Msgreceiver</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Msgreceiver</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Msgreceiver</em>' containment reference.
   * @see #setMsgreceiver(PHead)
   * @see it.unibo.xtext.qactor.QactorPackage#getOnReceiveMsg_Msgreceiver()
   * @model containment="true"
   * @generated
   */
  PHead getMsgreceiver();

  /**
   * Sets the value of the '{@link it.unibo.xtext.qactor.OnReceiveMsg#getMsgreceiver <em>Msgreceiver</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Msgreceiver</em>' containment reference.
   * @see #getMsgreceiver()
   * @generated
   */
  void setMsgreceiver(PHead value);

  /**
   * Returns the value of the '<em><b>Msgcontent</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Msgcontent</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Msgcontent</em>' containment reference.
   * @see #setMsgcontent(PHead)
   * @see it.unibo.xtext.qactor.QactorPackage#getOnReceiveMsg_Msgcontent()
   * @model containment="true"
   * @generated
   */
  PHead getMsgcontent();

  /**
   * Sets the value of the '{@link it.unibo.xtext.qactor.OnReceiveMsg#getMsgcontent <em>Msgcontent</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Msgcontent</em>' containment reference.
   * @see #getMsgcontent()
   * @generated
   */
  void setMsgcontent(PHead value);

  /**
   * Returns the value of the '<em><b>Msgseqnum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Msgseqnum</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Msgseqnum</em>' containment reference.
   * @see #setMsgseqnum(PHead)
   * @see it.unibo.xtext.qactor.QactorPackage#getOnReceiveMsg_Msgseqnum()
   * @model containment="true"
   * @generated
   */
  PHead getMsgseqnum();

  /**
   * Sets the value of the '{@link it.unibo.xtext.qactor.OnReceiveMsg#getMsgseqnum <em>Msgseqnum</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Msgseqnum</em>' containment reference.
   * @see #getMsgseqnum()
   * @generated
   */
  void setMsgseqnum(PHead value);

  /**
   * Returns the value of the '<em><b>Duration</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Duration</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Duration</em>' containment reference.
   * @see #setDuration(TimeLimit)
   * @see it.unibo.xtext.qactor.QactorPackage#getOnReceiveMsg_Duration()
   * @model containment="true"
   * @generated
   */
  TimeLimit getDuration();

  /**
   * Sets the value of the '{@link it.unibo.xtext.qactor.OnReceiveMsg#getDuration <em>Duration</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Duration</em>' containment reference.
   * @see #getDuration()
   * @generated
   */
  void setDuration(TimeLimit value);

} // OnReceiveMsg
