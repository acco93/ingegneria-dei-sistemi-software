/**
 */
package it.unibo.xtext.qactor;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Invitation</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see it.unibo.xtext.qactor.QactorPackage#getInvitation()
 * @model
 * @generated
 */
public interface Invitation extends OutInMessage
{
} // Invitation
