/**
 */
package it.unibo.xtext.qactor;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Send Event As Dispatch</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link it.unibo.xtext.qactor.SendEventAsDispatch#getActor <em>Actor</em>}</li>
 *   <li>{@link it.unibo.xtext.qactor.SendEventAsDispatch#getMsgref <em>Msgref</em>}</li>
 * </ul>
 * </p>
 *
 * @see it.unibo.xtext.qactor.QactorPackage#getSendEventAsDispatch()
 * @model
 * @generated
 */
public interface SendEventAsDispatch extends EventHandlerOperation
{
  /**
   * Returns the value of the '<em><b>Actor</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Actor</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Actor</em>' reference.
   * @see #setActor(QActor)
   * @see it.unibo.xtext.qactor.QactorPackage#getSendEventAsDispatch_Actor()
   * @model
   * @generated
   */
  QActor getActor();

  /**
   * Sets the value of the '{@link it.unibo.xtext.qactor.SendEventAsDispatch#getActor <em>Actor</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Actor</em>' reference.
   * @see #getActor()
   * @generated
   */
  void setActor(QActor value);

  /**
   * Returns the value of the '<em><b>Msgref</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Msgref</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Msgref</em>' reference.
   * @see #setMsgref(Message)
   * @see it.unibo.xtext.qactor.QactorPackage#getSendEventAsDispatch_Msgref()
   * @model
   * @generated
   */
  Message getMsgref();

  /**
   * Sets the value of the '{@link it.unibo.xtext.qactor.SendEventAsDispatch#getMsgref <em>Msgref</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Msgref</em>' reference.
   * @see #getMsgref()
   * @generated
   */
  void setMsgref(Message value);

} // SendEventAsDispatch
