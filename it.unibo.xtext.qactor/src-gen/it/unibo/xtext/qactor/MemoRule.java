/**
 */
package it.unibo.xtext.qactor;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Memo Rule</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see it.unibo.xtext.qactor.QactorPackage#getMemoRule()
 * @model
 * @generated
 */
public interface MemoRule extends EObject
{
} // MemoRule
