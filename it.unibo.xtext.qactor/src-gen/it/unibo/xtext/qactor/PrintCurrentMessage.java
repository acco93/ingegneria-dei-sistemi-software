/**
 */
package it.unibo.xtext.qactor;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Print Current Message</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link it.unibo.xtext.qactor.PrintCurrentMessage#isMemo <em>Memo</em>}</li>
 * </ul>
 * </p>
 *
 * @see it.unibo.xtext.qactor.QactorPackage#getPrintCurrentMessage()
 * @model
 * @generated
 */
public interface PrintCurrentMessage extends BasicMove
{
  /**
   * Returns the value of the '<em><b>Memo</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Memo</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Memo</em>' attribute.
   * @see #setMemo(boolean)
   * @see it.unibo.xtext.qactor.QactorPackage#getPrintCurrentMessage_Memo()
   * @model
   * @generated
   */
  boolean isMemo();

  /**
   * Sets the value of the '{@link it.unibo.xtext.qactor.PrintCurrentMessage#isMemo <em>Memo</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Memo</em>' attribute.
   * @see #isMemo()
   * @generated
   */
  void setMemo(boolean value);

} // PrintCurrentMessage
