/**
 */
package it.unibo.xtext.qactor;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Out In Message</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see it.unibo.xtext.qactor.QactorPackage#getOutInMessage()
 * @model
 * @generated
 */
public interface OutInMessage extends Message
{
} // OutInMessage
