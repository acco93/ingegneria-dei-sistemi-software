/**
 */
package it.unibo.xtext.qactor.impl;

import it.unibo.xtext.qactor.ActionMove;
import it.unibo.xtext.qactor.QactorPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Action Move</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class ActionMoveImpl extends MoveImpl implements ActionMove
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ActionMoveImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return QactorPackage.Literals.ACTION_MOVE;
  }

} //ActionMoveImpl
