/**
 */
package it.unibo.xtext.qactor.impl;

import it.unibo.xtext.qactor.Event;
import it.unibo.xtext.qactor.EventSwitch;
import it.unibo.xtext.qactor.Move;
import it.unibo.xtext.qactor.PHead;
import it.unibo.xtext.qactor.QactorPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Event Switch</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link it.unibo.xtext.qactor.impl.EventSwitchImpl#getEvent <em>Event</em>}</li>
 *   <li>{@link it.unibo.xtext.qactor.impl.EventSwitchImpl#getMsg <em>Msg</em>}</li>
 *   <li>{@link it.unibo.xtext.qactor.impl.EventSwitchImpl#getMove <em>Move</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class EventSwitchImpl extends MessageMoveImpl implements EventSwitch
{
  /**
   * The cached value of the '{@link #getEvent() <em>Event</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getEvent()
   * @generated
   * @ordered
   */
  protected Event event;

  /**
   * The cached value of the '{@link #getMsg() <em>Msg</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getMsg()
   * @generated
   * @ordered
   */
  protected PHead msg;

  /**
   * The cached value of the '{@link #getMove() <em>Move</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getMove()
   * @generated
   * @ordered
   */
  protected Move move;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected EventSwitchImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return QactorPackage.Literals.EVENT_SWITCH;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Event getEvent()
  {
    if (event != null && event.eIsProxy())
    {
      InternalEObject oldEvent = (InternalEObject)event;
      event = (Event)eResolveProxy(oldEvent);
      if (event != oldEvent)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, QactorPackage.EVENT_SWITCH__EVENT, oldEvent, event));
      }
    }
    return event;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Event basicGetEvent()
  {
    return event;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setEvent(Event newEvent)
  {
    Event oldEvent = event;
    event = newEvent;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, QactorPackage.EVENT_SWITCH__EVENT, oldEvent, event));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PHead getMsg()
  {
    return msg;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetMsg(PHead newMsg, NotificationChain msgs)
  {
    PHead oldMsg = msg;
    msg = newMsg;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, QactorPackage.EVENT_SWITCH__MSG, oldMsg, newMsg);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setMsg(PHead newMsg)
  {
    if (newMsg != msg)
    {
      NotificationChain msgs = null;
      if (msg != null)
        msgs = ((InternalEObject)msg).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - QactorPackage.EVENT_SWITCH__MSG, null, msgs);
      if (newMsg != null)
        msgs = ((InternalEObject)newMsg).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - QactorPackage.EVENT_SWITCH__MSG, null, msgs);
      msgs = basicSetMsg(newMsg, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, QactorPackage.EVENT_SWITCH__MSG, newMsg, newMsg));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Move getMove()
  {
    return move;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetMove(Move newMove, NotificationChain msgs)
  {
    Move oldMove = move;
    move = newMove;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, QactorPackage.EVENT_SWITCH__MOVE, oldMove, newMove);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setMove(Move newMove)
  {
    if (newMove != move)
    {
      NotificationChain msgs = null;
      if (move != null)
        msgs = ((InternalEObject)move).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - QactorPackage.EVENT_SWITCH__MOVE, null, msgs);
      if (newMove != null)
        msgs = ((InternalEObject)newMove).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - QactorPackage.EVENT_SWITCH__MOVE, null, msgs);
      msgs = basicSetMove(newMove, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, QactorPackage.EVENT_SWITCH__MOVE, newMove, newMove));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case QactorPackage.EVENT_SWITCH__MSG:
        return basicSetMsg(null, msgs);
      case QactorPackage.EVENT_SWITCH__MOVE:
        return basicSetMove(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case QactorPackage.EVENT_SWITCH__EVENT:
        if (resolve) return getEvent();
        return basicGetEvent();
      case QactorPackage.EVENT_SWITCH__MSG:
        return getMsg();
      case QactorPackage.EVENT_SWITCH__MOVE:
        return getMove();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case QactorPackage.EVENT_SWITCH__EVENT:
        setEvent((Event)newValue);
        return;
      case QactorPackage.EVENT_SWITCH__MSG:
        setMsg((PHead)newValue);
        return;
      case QactorPackage.EVENT_SWITCH__MOVE:
        setMove((Move)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case QactorPackage.EVENT_SWITCH__EVENT:
        setEvent((Event)null);
        return;
      case QactorPackage.EVENT_SWITCH__MSG:
        setMsg((PHead)null);
        return;
      case QactorPackage.EVENT_SWITCH__MOVE:
        setMove((Move)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case QactorPackage.EVENT_SWITCH__EVENT:
        return event != null;
      case QactorPackage.EVENT_SWITCH__MSG:
        return msg != null;
      case QactorPackage.EVENT_SWITCH__MOVE:
        return move != null;
    }
    return super.eIsSet(featureID);
  }

} //EventSwitchImpl
