/**
 */
package it.unibo.xtext.qactor.impl;

import it.unibo.xtext.qactor.AnswerEvent;
import it.unibo.xtext.qactor.MoveFile;
import it.unibo.xtext.qactor.QactorPackage;
import it.unibo.xtext.qactor.Sound;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Sound</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link it.unibo.xtext.qactor.impl.SoundImpl#getSrcfile <em>Srcfile</em>}</li>
 *   <li>{@link it.unibo.xtext.qactor.impl.SoundImpl#getAnswerEvent <em>Answer Event</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class SoundImpl extends ExtensionMoveImpl implements Sound
{
  /**
   * The cached value of the '{@link #getSrcfile() <em>Srcfile</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSrcfile()
   * @generated
   * @ordered
   */
  protected MoveFile srcfile;

  /**
   * The cached value of the '{@link #getAnswerEvent() <em>Answer Event</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAnswerEvent()
   * @generated
   * @ordered
   */
  protected AnswerEvent answerEvent;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected SoundImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return QactorPackage.Literals.SOUND;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MoveFile getSrcfile()
  {
    return srcfile;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetSrcfile(MoveFile newSrcfile, NotificationChain msgs)
  {
    MoveFile oldSrcfile = srcfile;
    srcfile = newSrcfile;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, QactorPackage.SOUND__SRCFILE, oldSrcfile, newSrcfile);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSrcfile(MoveFile newSrcfile)
  {
    if (newSrcfile != srcfile)
    {
      NotificationChain msgs = null;
      if (srcfile != null)
        msgs = ((InternalEObject)srcfile).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - QactorPackage.SOUND__SRCFILE, null, msgs);
      if (newSrcfile != null)
        msgs = ((InternalEObject)newSrcfile).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - QactorPackage.SOUND__SRCFILE, null, msgs);
      msgs = basicSetSrcfile(newSrcfile, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, QactorPackage.SOUND__SRCFILE, newSrcfile, newSrcfile));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AnswerEvent getAnswerEvent()
  {
    return answerEvent;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetAnswerEvent(AnswerEvent newAnswerEvent, NotificationChain msgs)
  {
    AnswerEvent oldAnswerEvent = answerEvent;
    answerEvent = newAnswerEvent;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, QactorPackage.SOUND__ANSWER_EVENT, oldAnswerEvent, newAnswerEvent);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setAnswerEvent(AnswerEvent newAnswerEvent)
  {
    if (newAnswerEvent != answerEvent)
    {
      NotificationChain msgs = null;
      if (answerEvent != null)
        msgs = ((InternalEObject)answerEvent).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - QactorPackage.SOUND__ANSWER_EVENT, null, msgs);
      if (newAnswerEvent != null)
        msgs = ((InternalEObject)newAnswerEvent).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - QactorPackage.SOUND__ANSWER_EVENT, null, msgs);
      msgs = basicSetAnswerEvent(newAnswerEvent, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, QactorPackage.SOUND__ANSWER_EVENT, newAnswerEvent, newAnswerEvent));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case QactorPackage.SOUND__SRCFILE:
        return basicSetSrcfile(null, msgs);
      case QactorPackage.SOUND__ANSWER_EVENT:
        return basicSetAnswerEvent(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case QactorPackage.SOUND__SRCFILE:
        return getSrcfile();
      case QactorPackage.SOUND__ANSWER_EVENT:
        return getAnswerEvent();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case QactorPackage.SOUND__SRCFILE:
        setSrcfile((MoveFile)newValue);
        return;
      case QactorPackage.SOUND__ANSWER_EVENT:
        setAnswerEvent((AnswerEvent)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case QactorPackage.SOUND__SRCFILE:
        setSrcfile((MoveFile)null);
        return;
      case QactorPackage.SOUND__ANSWER_EVENT:
        setAnswerEvent((AnswerEvent)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case QactorPackage.SOUND__SRCFILE:
        return srcfile != null;
      case QactorPackage.SOUND__ANSWER_EVENT:
        return answerEvent != null;
    }
    return super.eIsSet(featureID);
  }

} //SoundImpl
