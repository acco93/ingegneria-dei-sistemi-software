/**
 */
package it.unibo.xtext.qactor.impl;

import it.unibo.xtext.qactor.QactorPackage;
import it.unibo.xtext.qactor.ResumePlan;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Resume Plan</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class ResumePlanImpl extends PlanMoveImpl implements ResumePlan
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ResumePlanImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return QactorPackage.Literals.RESUME_PLAN;
  }

} //ResumePlanImpl
