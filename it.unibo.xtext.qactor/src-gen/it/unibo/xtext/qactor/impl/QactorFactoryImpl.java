/**
 */
package it.unibo.xtext.qactor.impl;

import it.unibo.xtext.qactor.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class QactorFactoryImpl extends EFactoryImpl implements QactorFactory
{
  /**
   * Creates the default factory implementation.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static QactorFactory init()
  {
    try
    {
      QactorFactory theQactorFactory = (QactorFactory)EPackage.Registry.INSTANCE.getEFactory(QactorPackage.eNS_URI);
      if (theQactorFactory != null)
      {
        return theQactorFactory;
      }
    }
    catch (Exception exception)
    {
      EcorePlugin.INSTANCE.log(exception);
    }
    return new QactorFactoryImpl();
  }

  /**
   * Creates an instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public QactorFactoryImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EObject create(EClass eClass)
  {
    switch (eClass.getClassifierID())
    {
      case QactorPackage.QACTOR_SYSTEM: return createQActorSystem();
      case QactorPackage.QACTOR_SYSTEM_SPEC: return createQActorSystemSpec();
      case QactorPackage.ROBOT: return createRobot();
      case QactorPackage.MESSAGE: return createMessage();
      case QactorPackage.OUT_ONLY_MESSAGE: return createOutOnlyMessage();
      case QactorPackage.OUT_IN_MESSAGE: return createOutInMessage();
      case QactorPackage.EVENT: return createEvent();
      case QactorPackage.SIGNAL: return createSignal();
      case QactorPackage.TOKEN: return createToken();
      case QactorPackage.DISPATCH: return createDispatch();
      case QactorPackage.REQUEST: return createRequest();
      case QactorPackage.INVITATION: return createInvitation();
      case QactorPackage.CONTEXT: return createContext();
      case QactorPackage.QACTOR: return createQActor();
      case QactorPackage.RULE: return createRule();
      case QactorPackage.PHEAD: return createPHead();
      case QactorPackage.PTERM: return createPTerm();
      case QactorPackage.PATOM: return createPAtom();
      case QactorPackage.PATOM_STRING: return createPAtomString();
      case QactorPackage.PATOMIC: return createPAtomic();
      case QactorPackage.PATOM_NUM: return createPAtomNum();
      case QactorPackage.PSTRUCT: return createPStruct();
      case QactorPackage.PACTOR_CALL: return createPActorCall();
      case QactorPackage.PPREDEF: return createPPredef();
      case QactorPackage.PIS: return createPIs();
      case QactorPackage.PATOM_CUT: return createPAtomCut();
      case QactorPackage.DATA: return createData();
      case QactorPackage.INTEGER_DATA: return createIntegerData();
      case QactorPackage.STRING_DATA: return createStringData();
      case QactorPackage.ACTION: return createAction();
      case QactorPackage.PLAN: return createPlan();
      case QactorPackage.PLAN_ACTION: return createPlanAction();
      case QactorPackage.GUARD: return createGuard();
      case QactorPackage.GUARD_PREDICATE: return createGuardPredicate();
      case QactorPackage.GUARD_PREDICATE_REMOVABLE: return createGuardPredicateRemovable();
      case QactorPackage.GUARD_PREDICATE_STABLE: return createGuardPredicateStable();
      case QactorPackage.MOVE: return createMove();
      case QactorPackage.ACTION_MOVE: return createActionMove();
      case QactorPackage.EXECUTE_ACTION: return createExecuteAction();
      case QactorPackage.SOLVE_GOAL: return createSolveGoal();
      case QactorPackage.DEMO: return createDemo();
      case QactorPackage.ACTOR_OP: return createActorOp();
      case QactorPackage.BASIC_ROBOT_MOVE: return createBasicRobotMove();
      case QactorPackage.BASIC_MOVE: return createBasicMove();
      case QactorPackage.PRINT: return createPrint();
      case QactorPackage.PRINT_CURRENT_EVENT: return createPrintCurrentEvent();
      case QactorPackage.PRINT_CURRENT_MESSAGE: return createPrintCurrentMessage();
      case QactorPackage.MEMO_CURRENT_EVENT: return createMemoCurrentEvent();
      case QactorPackage.MEMO_CURRENT_MESSAGE: return createMemoCurrentMessage();
      case QactorPackage.PLAN_MOVE: return createPlanMove();
      case QactorPackage.GET_ACTIVATION_EVENT: return createGetActivationEvent();
      case QactorPackage.GET_SENSED_EVENT: return createGetSensedEvent();
      case QactorPackage.LOAD_PLAN: return createLoadPlan();
      case QactorPackage.RUN_PLAN: return createRunPlan();
      case QactorPackage.RESUME_PLAN: return createResumePlan();
      case QactorPackage.SUSPEND_PLAN: return createSuspendPlan();
      case QactorPackage.REPEAT_PLAN: return createRepeatPlan();
      case QactorPackage.SWITCH_PLAN: return createSwitchPlan();
      case QactorPackage.END_PLAN: return createEndPlan();
      case QactorPackage.END_ACTOR: return createEndActor();
      case QactorPackage.GUARD_MOVE: return createGuardMove();
      case QactorPackage.ADD_RULE: return createAddRule();
      case QactorPackage.REMOVE_RULE: return createRemoveRule();
      case QactorPackage.MESSAGE_MOVE: return createMessageMove();
      case QactorPackage.SEND_DISPATCH: return createSendDispatch();
      case QactorPackage.SEND_REQUEST: return createSendRequest();
      case QactorPackage.REPLY_TO_CALLER: return createReplyToCaller();
      case QactorPackage.RECEIVE_MSG: return createReceiveMsg();
      case QactorPackage.MSG_SPEC: return createMsgSpec();
      case QactorPackage.ON_RECEIVE_MSG: return createOnReceiveMsg();
      case QactorPackage.MSG_SELECT: return createMsgSelect();
      case QactorPackage.RAISE_EVENT: return createRaiseEvent();
      case QactorPackage.SENSE_EVENT: return createSenseEvent();
      case QactorPackage.MSG_SWITCH: return createMsgSwitch();
      case QactorPackage.EVENT_SWITCH: return createEventSwitch();
      case QactorPackage.CONTINUATION: return createContinuation();
      case QactorPackage.EXTENSION_MOVE: return createExtensionMove();
      case QactorPackage.PHOTO: return createPhoto();
      case QactorPackage.SOUND: return createSound();
      case QactorPackage.VIDEO: return createVideo();
      case QactorPackage.DELAY: return createDelay();
      case QactorPackage.ANSWER_EVENT: return createAnswerEvent();
      case QactorPackage.EVENT_HANDLER: return createEventHandler();
      case QactorPackage.EVENT_HANDLER_BODY: return createEventHandlerBody();
      case QactorPackage.EVENT_HANDLER_OPERATION: return createEventHandlerOperation();
      case QactorPackage.MEMO_OPERATION: return createMemoOperation();
      case QactorPackage.SOLVE_OPERATION: return createSolveOperation();
      case QactorPackage.SEND_EVENT_AS_DISPATCH: return createSendEventAsDispatch();
      case QactorPackage.MEMO_RULE: return createMemoRule();
      case QactorPackage.MEMO_EVENT: return createMemoEvent();
      case QactorPackage.REACTION: return createReaction();
      case QactorPackage.ALARM_EVENT: return createAlarmEvent();
      case QactorPackage.NORMAL_EVENT: return createNormalEvent();
      case QactorPackage.CONTINUE_EVENT: return createContinueEvent();
      case QactorPackage.VAR_OR_QACTOR: return createVarOrQactor();
      case QactorPackage.VAR_OR_ATOMIC: return createVarOrAtomic();
      case QactorPackage.VAR_OR_STRING: return createVarOrString();
      case QactorPackage.VAR_OR_PSTRUCT: return createVarOrPStruct();
      case QactorPackage.VAR_OR_PHEAD: return createVarOrPhead();
      case QactorPackage.VAR_OR_ATOM_OR_PSTRUCT: return createVarOrAtomOrPStruct();
      case QactorPackage.VARIABLE: return createVariable();
      case QactorPackage.TIME_LIMIT: return createTimeLimit();
      case QactorPackage.COMPONENT_IP: return createComponentIP();
      case QactorPackage.MOVE_FILE: return createMoveFile();
      default:
        throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object createFromString(EDataType eDataType, String initialValue)
  {
    switch (eDataType.getClassifierID())
    {
      case QactorPackage.WINDOW_COLOR:
        return createWindowColorFromString(eDataType, initialValue);
      default:
        throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String convertToString(EDataType eDataType, Object instanceValue)
  {
    switch (eDataType.getClassifierID())
    {
      case QactorPackage.WINDOW_COLOR:
        return convertWindowColorToString(eDataType, instanceValue);
      default:
        throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public QActorSystem createQActorSystem()
  {
    QActorSystemImpl qActorSystem = new QActorSystemImpl();
    return qActorSystem;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public QActorSystemSpec createQActorSystemSpec()
  {
    QActorSystemSpecImpl qActorSystemSpec = new QActorSystemSpecImpl();
    return qActorSystemSpec;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Robot createRobot()
  {
    RobotImpl robot = new RobotImpl();
    return robot;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Message createMessage()
  {
    MessageImpl message = new MessageImpl();
    return message;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public OutOnlyMessage createOutOnlyMessage()
  {
    OutOnlyMessageImpl outOnlyMessage = new OutOnlyMessageImpl();
    return outOnlyMessage;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public OutInMessage createOutInMessage()
  {
    OutInMessageImpl outInMessage = new OutInMessageImpl();
    return outInMessage;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Event createEvent()
  {
    EventImpl event = new EventImpl();
    return event;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Signal createSignal()
  {
    SignalImpl signal = new SignalImpl();
    return signal;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Token createToken()
  {
    TokenImpl token = new TokenImpl();
    return token;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Dispatch createDispatch()
  {
    DispatchImpl dispatch = new DispatchImpl();
    return dispatch;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Request createRequest()
  {
    RequestImpl request = new RequestImpl();
    return request;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Invitation createInvitation()
  {
    InvitationImpl invitation = new InvitationImpl();
    return invitation;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Context createContext()
  {
    ContextImpl context = new ContextImpl();
    return context;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public QActor createQActor()
  {
    QActorImpl qActor = new QActorImpl();
    return qActor;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Rule createRule()
  {
    RuleImpl rule = new RuleImpl();
    return rule;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PHead createPHead()
  {
    PHeadImpl pHead = new PHeadImpl();
    return pHead;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PTerm createPTerm()
  {
    PTermImpl pTerm = new PTermImpl();
    return pTerm;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PAtom createPAtom()
  {
    PAtomImpl pAtom = new PAtomImpl();
    return pAtom;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PAtomString createPAtomString()
  {
    PAtomStringImpl pAtomString = new PAtomStringImpl();
    return pAtomString;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PAtomic createPAtomic()
  {
    PAtomicImpl pAtomic = new PAtomicImpl();
    return pAtomic;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PAtomNum createPAtomNum()
  {
    PAtomNumImpl pAtomNum = new PAtomNumImpl();
    return pAtomNum;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PStruct createPStruct()
  {
    PStructImpl pStruct = new PStructImpl();
    return pStruct;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PActorCall createPActorCall()
  {
    PActorCallImpl pActorCall = new PActorCallImpl();
    return pActorCall;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PPredef createPPredef()
  {
    PPredefImpl pPredef = new PPredefImpl();
    return pPredef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PIs createPIs()
  {
    PIsImpl pIs = new PIsImpl();
    return pIs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PAtomCut createPAtomCut()
  {
    PAtomCutImpl pAtomCut = new PAtomCutImpl();
    return pAtomCut;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Data createData()
  {
    DataImpl data = new DataImpl();
    return data;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public IntegerData createIntegerData()
  {
    IntegerDataImpl integerData = new IntegerDataImpl();
    return integerData;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public StringData createStringData()
  {
    StringDataImpl stringData = new StringDataImpl();
    return stringData;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Action createAction()
  {
    ActionImpl action = new ActionImpl();
    return action;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Plan createPlan()
  {
    PlanImpl plan = new PlanImpl();
    return plan;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PlanAction createPlanAction()
  {
    PlanActionImpl planAction = new PlanActionImpl();
    return planAction;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Guard createGuard()
  {
    GuardImpl guard = new GuardImpl();
    return guard;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public GuardPredicate createGuardPredicate()
  {
    GuardPredicateImpl guardPredicate = new GuardPredicateImpl();
    return guardPredicate;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public GuardPredicateRemovable createGuardPredicateRemovable()
  {
    GuardPredicateRemovableImpl guardPredicateRemovable = new GuardPredicateRemovableImpl();
    return guardPredicateRemovable;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public GuardPredicateStable createGuardPredicateStable()
  {
    GuardPredicateStableImpl guardPredicateStable = new GuardPredicateStableImpl();
    return guardPredicateStable;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Move createMove()
  {
    MoveImpl move = new MoveImpl();
    return move;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ActionMove createActionMove()
  {
    ActionMoveImpl actionMove = new ActionMoveImpl();
    return actionMove;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ExecuteAction createExecuteAction()
  {
    ExecuteActionImpl executeAction = new ExecuteActionImpl();
    return executeAction;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SolveGoal createSolveGoal()
  {
    SolveGoalImpl solveGoal = new SolveGoalImpl();
    return solveGoal;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Demo createDemo()
  {
    DemoImpl demo = new DemoImpl();
    return demo;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ActorOp createActorOp()
  {
    ActorOpImpl actorOp = new ActorOpImpl();
    return actorOp;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BasicRobotMove createBasicRobotMove()
  {
    BasicRobotMoveImpl basicRobotMove = new BasicRobotMoveImpl();
    return basicRobotMove;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BasicMove createBasicMove()
  {
    BasicMoveImpl basicMove = new BasicMoveImpl();
    return basicMove;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Print createPrint()
  {
    PrintImpl print = new PrintImpl();
    return print;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PrintCurrentEvent createPrintCurrentEvent()
  {
    PrintCurrentEventImpl printCurrentEvent = new PrintCurrentEventImpl();
    return printCurrentEvent;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PrintCurrentMessage createPrintCurrentMessage()
  {
    PrintCurrentMessageImpl printCurrentMessage = new PrintCurrentMessageImpl();
    return printCurrentMessage;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MemoCurrentEvent createMemoCurrentEvent()
  {
    MemoCurrentEventImpl memoCurrentEvent = new MemoCurrentEventImpl();
    return memoCurrentEvent;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MemoCurrentMessage createMemoCurrentMessage()
  {
    MemoCurrentMessageImpl memoCurrentMessage = new MemoCurrentMessageImpl();
    return memoCurrentMessage;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PlanMove createPlanMove()
  {
    PlanMoveImpl planMove = new PlanMoveImpl();
    return planMove;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public GetActivationEvent createGetActivationEvent()
  {
    GetActivationEventImpl getActivationEvent = new GetActivationEventImpl();
    return getActivationEvent;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public GetSensedEvent createGetSensedEvent()
  {
    GetSensedEventImpl getSensedEvent = new GetSensedEventImpl();
    return getSensedEvent;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LoadPlan createLoadPlan()
  {
    LoadPlanImpl loadPlan = new LoadPlanImpl();
    return loadPlan;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public RunPlan createRunPlan()
  {
    RunPlanImpl runPlan = new RunPlanImpl();
    return runPlan;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ResumePlan createResumePlan()
  {
    ResumePlanImpl resumePlan = new ResumePlanImpl();
    return resumePlan;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SuspendPlan createSuspendPlan()
  {
    SuspendPlanImpl suspendPlan = new SuspendPlanImpl();
    return suspendPlan;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public RepeatPlan createRepeatPlan()
  {
    RepeatPlanImpl repeatPlan = new RepeatPlanImpl();
    return repeatPlan;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SwitchPlan createSwitchPlan()
  {
    SwitchPlanImpl switchPlan = new SwitchPlanImpl();
    return switchPlan;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EndPlan createEndPlan()
  {
    EndPlanImpl endPlan = new EndPlanImpl();
    return endPlan;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EndActor createEndActor()
  {
    EndActorImpl endActor = new EndActorImpl();
    return endActor;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public GuardMove createGuardMove()
  {
    GuardMoveImpl guardMove = new GuardMoveImpl();
    return guardMove;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AddRule createAddRule()
  {
    AddRuleImpl addRule = new AddRuleImpl();
    return addRule;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public RemoveRule createRemoveRule()
  {
    RemoveRuleImpl removeRule = new RemoveRuleImpl();
    return removeRule;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MessageMove createMessageMove()
  {
    MessageMoveImpl messageMove = new MessageMoveImpl();
    return messageMove;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SendDispatch createSendDispatch()
  {
    SendDispatchImpl sendDispatch = new SendDispatchImpl();
    return sendDispatch;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SendRequest createSendRequest()
  {
    SendRequestImpl sendRequest = new SendRequestImpl();
    return sendRequest;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ReplyToCaller createReplyToCaller()
  {
    ReplyToCallerImpl replyToCaller = new ReplyToCallerImpl();
    return replyToCaller;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ReceiveMsg createReceiveMsg()
  {
    ReceiveMsgImpl receiveMsg = new ReceiveMsgImpl();
    return receiveMsg;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MsgSpec createMsgSpec()
  {
    MsgSpecImpl msgSpec = new MsgSpecImpl();
    return msgSpec;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public OnReceiveMsg createOnReceiveMsg()
  {
    OnReceiveMsgImpl onReceiveMsg = new OnReceiveMsgImpl();
    return onReceiveMsg;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MsgSelect createMsgSelect()
  {
    MsgSelectImpl msgSelect = new MsgSelectImpl();
    return msgSelect;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public RaiseEvent createRaiseEvent()
  {
    RaiseEventImpl raiseEvent = new RaiseEventImpl();
    return raiseEvent;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SenseEvent createSenseEvent()
  {
    SenseEventImpl senseEvent = new SenseEventImpl();
    return senseEvent;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MsgSwitch createMsgSwitch()
  {
    MsgSwitchImpl msgSwitch = new MsgSwitchImpl();
    return msgSwitch;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EventSwitch createEventSwitch()
  {
    EventSwitchImpl eventSwitch = new EventSwitchImpl();
    return eventSwitch;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Continuation createContinuation()
  {
    ContinuationImpl continuation = new ContinuationImpl();
    return continuation;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ExtensionMove createExtensionMove()
  {
    ExtensionMoveImpl extensionMove = new ExtensionMoveImpl();
    return extensionMove;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Photo createPhoto()
  {
    PhotoImpl photo = new PhotoImpl();
    return photo;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Sound createSound()
  {
    SoundImpl sound = new SoundImpl();
    return sound;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Video createVideo()
  {
    VideoImpl video = new VideoImpl();
    return video;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Delay createDelay()
  {
    DelayImpl delay = new DelayImpl();
    return delay;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AnswerEvent createAnswerEvent()
  {
    AnswerEventImpl answerEvent = new AnswerEventImpl();
    return answerEvent;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EventHandler createEventHandler()
  {
    EventHandlerImpl eventHandler = new EventHandlerImpl();
    return eventHandler;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EventHandlerBody createEventHandlerBody()
  {
    EventHandlerBodyImpl eventHandlerBody = new EventHandlerBodyImpl();
    return eventHandlerBody;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EventHandlerOperation createEventHandlerOperation()
  {
    EventHandlerOperationImpl eventHandlerOperation = new EventHandlerOperationImpl();
    return eventHandlerOperation;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MemoOperation createMemoOperation()
  {
    MemoOperationImpl memoOperation = new MemoOperationImpl();
    return memoOperation;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SolveOperation createSolveOperation()
  {
    SolveOperationImpl solveOperation = new SolveOperationImpl();
    return solveOperation;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SendEventAsDispatch createSendEventAsDispatch()
  {
    SendEventAsDispatchImpl sendEventAsDispatch = new SendEventAsDispatchImpl();
    return sendEventAsDispatch;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MemoRule createMemoRule()
  {
    MemoRuleImpl memoRule = new MemoRuleImpl();
    return memoRule;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MemoEvent createMemoEvent()
  {
    MemoEventImpl memoEvent = new MemoEventImpl();
    return memoEvent;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Reaction createReaction()
  {
    ReactionImpl reaction = new ReactionImpl();
    return reaction;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AlarmEvent createAlarmEvent()
  {
    AlarmEventImpl alarmEvent = new AlarmEventImpl();
    return alarmEvent;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NormalEvent createNormalEvent()
  {
    NormalEventImpl normalEvent = new NormalEventImpl();
    return normalEvent;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ContinueEvent createContinueEvent()
  {
    ContinueEventImpl continueEvent = new ContinueEventImpl();
    return continueEvent;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public VarOrQactor createVarOrQactor()
  {
    VarOrQactorImpl varOrQactor = new VarOrQactorImpl();
    return varOrQactor;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public VarOrAtomic createVarOrAtomic()
  {
    VarOrAtomicImpl varOrAtomic = new VarOrAtomicImpl();
    return varOrAtomic;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public VarOrString createVarOrString()
  {
    VarOrStringImpl varOrString = new VarOrStringImpl();
    return varOrString;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public VarOrPStruct createVarOrPStruct()
  {
    VarOrPStructImpl varOrPStruct = new VarOrPStructImpl();
    return varOrPStruct;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public VarOrPhead createVarOrPhead()
  {
    VarOrPheadImpl varOrPhead = new VarOrPheadImpl();
    return varOrPhead;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public VarOrAtomOrPStruct createVarOrAtomOrPStruct()
  {
    VarOrAtomOrPStructImpl varOrAtomOrPStruct = new VarOrAtomOrPStructImpl();
    return varOrAtomOrPStruct;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Variable createVariable()
  {
    VariableImpl variable = new VariableImpl();
    return variable;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TimeLimit createTimeLimit()
  {
    TimeLimitImpl timeLimit = new TimeLimitImpl();
    return timeLimit;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ComponentIP createComponentIP()
  {
    ComponentIPImpl componentIP = new ComponentIPImpl();
    return componentIP;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MoveFile createMoveFile()
  {
    MoveFileImpl moveFile = new MoveFileImpl();
    return moveFile;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public WindowColor createWindowColorFromString(EDataType eDataType, String initialValue)
  {
    WindowColor result = WindowColor.get(initialValue);
    if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
    return result;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertWindowColorToString(EDataType eDataType, Object instanceValue)
  {
    return instanceValue == null ? null : instanceValue.toString();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public QactorPackage getQactorPackage()
  {
    return (QactorPackage)getEPackage();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @deprecated
   * @generated
   */
  @Deprecated
  public static QactorPackage getPackage()
  {
    return QactorPackage.eINSTANCE;
  }

} //QactorFactoryImpl
