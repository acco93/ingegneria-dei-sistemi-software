/**
 */
package it.unibo.xtext.qactor.impl;

import it.unibo.xtext.qactor.QactorPackage;
import it.unibo.xtext.qactor.RunPlan;
import it.unibo.xtext.qactor.VarOrAtomic;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Run Plan</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link it.unibo.xtext.qactor.impl.RunPlanImpl#getPlainid <em>Plainid</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class RunPlanImpl extends PlanMoveImpl implements RunPlan
{
  /**
   * The cached value of the '{@link #getPlainid() <em>Plainid</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPlainid()
   * @generated
   * @ordered
   */
  protected VarOrAtomic plainid;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected RunPlanImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return QactorPackage.Literals.RUN_PLAN;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public VarOrAtomic getPlainid()
  {
    return plainid;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetPlainid(VarOrAtomic newPlainid, NotificationChain msgs)
  {
    VarOrAtomic oldPlainid = plainid;
    plainid = newPlainid;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, QactorPackage.RUN_PLAN__PLAINID, oldPlainid, newPlainid);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setPlainid(VarOrAtomic newPlainid)
  {
    if (newPlainid != plainid)
    {
      NotificationChain msgs = null;
      if (plainid != null)
        msgs = ((InternalEObject)plainid).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - QactorPackage.RUN_PLAN__PLAINID, null, msgs);
      if (newPlainid != null)
        msgs = ((InternalEObject)newPlainid).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - QactorPackage.RUN_PLAN__PLAINID, null, msgs);
      msgs = basicSetPlainid(newPlainid, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, QactorPackage.RUN_PLAN__PLAINID, newPlainid, newPlainid));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case QactorPackage.RUN_PLAN__PLAINID:
        return basicSetPlainid(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case QactorPackage.RUN_PLAN__PLAINID:
        return getPlainid();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case QactorPackage.RUN_PLAN__PLAINID:
        setPlainid((VarOrAtomic)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case QactorPackage.RUN_PLAN__PLAINID:
        setPlainid((VarOrAtomic)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case QactorPackage.RUN_PLAN__PLAINID:
        return plainid != null;
    }
    return super.eIsSet(featureID);
  }

} //RunPlanImpl
