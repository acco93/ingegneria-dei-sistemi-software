/**
 */
package it.unibo.xtext.qactor.impl;

import it.unibo.xtext.qactor.QactorPackage;
import it.unibo.xtext.qactor.SuspendPlan;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Suspend Plan</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class SuspendPlanImpl extends PlanMoveImpl implements SuspendPlan
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected SuspendPlanImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return QactorPackage.Literals.SUSPEND_PLAN;
  }

} //SuspendPlanImpl
