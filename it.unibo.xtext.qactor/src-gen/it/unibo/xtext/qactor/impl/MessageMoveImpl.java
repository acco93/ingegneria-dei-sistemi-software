/**
 */
package it.unibo.xtext.qactor.impl;

import it.unibo.xtext.qactor.MessageMove;
import it.unibo.xtext.qactor.QactorPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Message Move</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class MessageMoveImpl extends MoveImpl implements MessageMove
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected MessageMoveImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return QactorPackage.Literals.MESSAGE_MOVE;
  }

} //MessageMoveImpl
