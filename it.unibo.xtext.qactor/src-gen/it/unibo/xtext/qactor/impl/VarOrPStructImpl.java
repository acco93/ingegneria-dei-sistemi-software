/**
 */
package it.unibo.xtext.qactor.impl;

import it.unibo.xtext.qactor.PStruct;
import it.unibo.xtext.qactor.QactorPackage;
import it.unibo.xtext.qactor.VarOrPStruct;
import it.unibo.xtext.qactor.Variable;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Var Or PStruct</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link it.unibo.xtext.qactor.impl.VarOrPStructImpl#getVar <em>Var</em>}</li>
 *   <li>{@link it.unibo.xtext.qactor.impl.VarOrPStructImpl#getPsrtuct <em>Psrtuct</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class VarOrPStructImpl extends MinimalEObjectImpl.Container implements VarOrPStruct
{
  /**
   * The cached value of the '{@link #getVar() <em>Var</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getVar()
   * @generated
   * @ordered
   */
  protected Variable var;

  /**
   * The cached value of the '{@link #getPsrtuct() <em>Psrtuct</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPsrtuct()
   * @generated
   * @ordered
   */
  protected PStruct psrtuct;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected VarOrPStructImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return QactorPackage.Literals.VAR_OR_PSTRUCT;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Variable getVar()
  {
    return var;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetVar(Variable newVar, NotificationChain msgs)
  {
    Variable oldVar = var;
    var = newVar;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, QactorPackage.VAR_OR_PSTRUCT__VAR, oldVar, newVar);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setVar(Variable newVar)
  {
    if (newVar != var)
    {
      NotificationChain msgs = null;
      if (var != null)
        msgs = ((InternalEObject)var).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - QactorPackage.VAR_OR_PSTRUCT__VAR, null, msgs);
      if (newVar != null)
        msgs = ((InternalEObject)newVar).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - QactorPackage.VAR_OR_PSTRUCT__VAR, null, msgs);
      msgs = basicSetVar(newVar, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, QactorPackage.VAR_OR_PSTRUCT__VAR, newVar, newVar));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PStruct getPsrtuct()
  {
    return psrtuct;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetPsrtuct(PStruct newPsrtuct, NotificationChain msgs)
  {
    PStruct oldPsrtuct = psrtuct;
    psrtuct = newPsrtuct;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, QactorPackage.VAR_OR_PSTRUCT__PSRTUCT, oldPsrtuct, newPsrtuct);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setPsrtuct(PStruct newPsrtuct)
  {
    if (newPsrtuct != psrtuct)
    {
      NotificationChain msgs = null;
      if (psrtuct != null)
        msgs = ((InternalEObject)psrtuct).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - QactorPackage.VAR_OR_PSTRUCT__PSRTUCT, null, msgs);
      if (newPsrtuct != null)
        msgs = ((InternalEObject)newPsrtuct).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - QactorPackage.VAR_OR_PSTRUCT__PSRTUCT, null, msgs);
      msgs = basicSetPsrtuct(newPsrtuct, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, QactorPackage.VAR_OR_PSTRUCT__PSRTUCT, newPsrtuct, newPsrtuct));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case QactorPackage.VAR_OR_PSTRUCT__VAR:
        return basicSetVar(null, msgs);
      case QactorPackage.VAR_OR_PSTRUCT__PSRTUCT:
        return basicSetPsrtuct(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case QactorPackage.VAR_OR_PSTRUCT__VAR:
        return getVar();
      case QactorPackage.VAR_OR_PSTRUCT__PSRTUCT:
        return getPsrtuct();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case QactorPackage.VAR_OR_PSTRUCT__VAR:
        setVar((Variable)newValue);
        return;
      case QactorPackage.VAR_OR_PSTRUCT__PSRTUCT:
        setPsrtuct((PStruct)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case QactorPackage.VAR_OR_PSTRUCT__VAR:
        setVar((Variable)null);
        return;
      case QactorPackage.VAR_OR_PSTRUCT__PSRTUCT:
        setPsrtuct((PStruct)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case QactorPackage.VAR_OR_PSTRUCT__VAR:
        return var != null;
      case QactorPackage.VAR_OR_PSTRUCT__PSRTUCT:
        return psrtuct != null;
    }
    return super.eIsSet(featureID);
  }

} //VarOrPStructImpl
