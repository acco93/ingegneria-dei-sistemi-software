/**
 */
package it.unibo.xtext.qactor.impl;

import it.unibo.xtext.qactor.PHead;
import it.unibo.xtext.qactor.QactorPackage;
import it.unibo.xtext.qactor.VarOrPhead;
import it.unibo.xtext.qactor.Variable;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Var Or Phead</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link it.unibo.xtext.qactor.impl.VarOrPheadImpl#getVar <em>Var</em>}</li>
 *   <li>{@link it.unibo.xtext.qactor.impl.VarOrPheadImpl#getPhead <em>Phead</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class VarOrPheadImpl extends MinimalEObjectImpl.Container implements VarOrPhead
{
  /**
   * The cached value of the '{@link #getVar() <em>Var</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getVar()
   * @generated
   * @ordered
   */
  protected Variable var;

  /**
   * The cached value of the '{@link #getPhead() <em>Phead</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPhead()
   * @generated
   * @ordered
   */
  protected PHead phead;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected VarOrPheadImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return QactorPackage.Literals.VAR_OR_PHEAD;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Variable getVar()
  {
    return var;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetVar(Variable newVar, NotificationChain msgs)
  {
    Variable oldVar = var;
    var = newVar;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, QactorPackage.VAR_OR_PHEAD__VAR, oldVar, newVar);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setVar(Variable newVar)
  {
    if (newVar != var)
    {
      NotificationChain msgs = null;
      if (var != null)
        msgs = ((InternalEObject)var).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - QactorPackage.VAR_OR_PHEAD__VAR, null, msgs);
      if (newVar != null)
        msgs = ((InternalEObject)newVar).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - QactorPackage.VAR_OR_PHEAD__VAR, null, msgs);
      msgs = basicSetVar(newVar, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, QactorPackage.VAR_OR_PHEAD__VAR, newVar, newVar));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PHead getPhead()
  {
    return phead;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetPhead(PHead newPhead, NotificationChain msgs)
  {
    PHead oldPhead = phead;
    phead = newPhead;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, QactorPackage.VAR_OR_PHEAD__PHEAD, oldPhead, newPhead);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setPhead(PHead newPhead)
  {
    if (newPhead != phead)
    {
      NotificationChain msgs = null;
      if (phead != null)
        msgs = ((InternalEObject)phead).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - QactorPackage.VAR_OR_PHEAD__PHEAD, null, msgs);
      if (newPhead != null)
        msgs = ((InternalEObject)newPhead).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - QactorPackage.VAR_OR_PHEAD__PHEAD, null, msgs);
      msgs = basicSetPhead(newPhead, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, QactorPackage.VAR_OR_PHEAD__PHEAD, newPhead, newPhead));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case QactorPackage.VAR_OR_PHEAD__VAR:
        return basicSetVar(null, msgs);
      case QactorPackage.VAR_OR_PHEAD__PHEAD:
        return basicSetPhead(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case QactorPackage.VAR_OR_PHEAD__VAR:
        return getVar();
      case QactorPackage.VAR_OR_PHEAD__PHEAD:
        return getPhead();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case QactorPackage.VAR_OR_PHEAD__VAR:
        setVar((Variable)newValue);
        return;
      case QactorPackage.VAR_OR_PHEAD__PHEAD:
        setPhead((PHead)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case QactorPackage.VAR_OR_PHEAD__VAR:
        setVar((Variable)null);
        return;
      case QactorPackage.VAR_OR_PHEAD__PHEAD:
        setPhead((PHead)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case QactorPackage.VAR_OR_PHEAD__VAR:
        return var != null;
      case QactorPackage.VAR_OR_PHEAD__PHEAD:
        return phead != null;
    }
    return super.eIsSet(featureID);
  }

} //VarOrPheadImpl
