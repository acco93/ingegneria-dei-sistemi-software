/**
 */
package it.unibo.xtext.qactor.impl;

import it.unibo.xtext.qactor.Event;
import it.unibo.xtext.qactor.NormalEvent;
import it.unibo.xtext.qactor.Plan;
import it.unibo.xtext.qactor.QactorPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Normal Event</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link it.unibo.xtext.qactor.impl.NormalEventImpl#getEv <em>Ev</em>}</li>
 *   <li>{@link it.unibo.xtext.qactor.impl.NormalEventImpl#getPlanRef <em>Plan Ref</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class NormalEventImpl extends AlarmEventImpl implements NormalEvent
{
  /**
   * The cached value of the '{@link #getEv() <em>Ev</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getEv()
   * @generated
   * @ordered
   */
  protected Event ev;

  /**
   * The cached value of the '{@link #getPlanRef() <em>Plan Ref</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPlanRef()
   * @generated
   * @ordered
   */
  protected Plan planRef;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected NormalEventImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return QactorPackage.Literals.NORMAL_EVENT;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Event getEv()
  {
    if (ev != null && ev.eIsProxy())
    {
      InternalEObject oldEv = (InternalEObject)ev;
      ev = (Event)eResolveProxy(oldEv);
      if (ev != oldEv)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, QactorPackage.NORMAL_EVENT__EV, oldEv, ev));
      }
    }
    return ev;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Event basicGetEv()
  {
    return ev;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setEv(Event newEv)
  {
    Event oldEv = ev;
    ev = newEv;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, QactorPackage.NORMAL_EVENT__EV, oldEv, ev));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Plan getPlanRef()
  {
    if (planRef != null && planRef.eIsProxy())
    {
      InternalEObject oldPlanRef = (InternalEObject)planRef;
      planRef = (Plan)eResolveProxy(oldPlanRef);
      if (planRef != oldPlanRef)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, QactorPackage.NORMAL_EVENT__PLAN_REF, oldPlanRef, planRef));
      }
    }
    return planRef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Plan basicGetPlanRef()
  {
    return planRef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setPlanRef(Plan newPlanRef)
  {
    Plan oldPlanRef = planRef;
    planRef = newPlanRef;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, QactorPackage.NORMAL_EVENT__PLAN_REF, oldPlanRef, planRef));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case QactorPackage.NORMAL_EVENT__EV:
        if (resolve) return getEv();
        return basicGetEv();
      case QactorPackage.NORMAL_EVENT__PLAN_REF:
        if (resolve) return getPlanRef();
        return basicGetPlanRef();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case QactorPackage.NORMAL_EVENT__EV:
        setEv((Event)newValue);
        return;
      case QactorPackage.NORMAL_EVENT__PLAN_REF:
        setPlanRef((Plan)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case QactorPackage.NORMAL_EVENT__EV:
        setEv((Event)null);
        return;
      case QactorPackage.NORMAL_EVENT__PLAN_REF:
        setPlanRef((Plan)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case QactorPackage.NORMAL_EVENT__EV:
        return ev != null;
      case QactorPackage.NORMAL_EVENT__PLAN_REF:
        return planRef != null;
    }
    return super.eIsSet(featureID);
  }

} //NormalEventImpl
