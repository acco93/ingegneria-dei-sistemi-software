/**
 */
package it.unibo.xtext.qactor.impl;

import it.unibo.xtext.qactor.QactorPackage;
import it.unibo.xtext.qactor.Signal;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Signal</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class SignalImpl extends OutOnlyMessageImpl implements Signal
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected SignalImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return QactorPackage.Literals.SIGNAL;
  }

} //SignalImpl
