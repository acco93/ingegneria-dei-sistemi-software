/**
 */
package it.unibo.xtext.qactor.impl;

import it.unibo.xtext.qactor.MemoCurrentEvent;
import it.unibo.xtext.qactor.MemoOperation;
import it.unibo.xtext.qactor.MemoRule;
import it.unibo.xtext.qactor.QActor;
import it.unibo.xtext.qactor.QactorPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Memo Operation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link it.unibo.xtext.qactor.impl.MemoOperationImpl#getRule <em>Rule</em>}</li>
 *   <li>{@link it.unibo.xtext.qactor.impl.MemoOperationImpl#getActor <em>Actor</em>}</li>
 *   <li>{@link it.unibo.xtext.qactor.impl.MemoOperationImpl#getDoMemo <em>Do Memo</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class MemoOperationImpl extends EventHandlerOperationImpl implements MemoOperation
{
  /**
   * The cached value of the '{@link #getRule() <em>Rule</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRule()
   * @generated
   * @ordered
   */
  protected MemoRule rule;

  /**
   * The cached value of the '{@link #getActor() <em>Actor</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getActor()
   * @generated
   * @ordered
   */
  protected QActor actor;

  /**
   * The cached value of the '{@link #getDoMemo() <em>Do Memo</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDoMemo()
   * @generated
   * @ordered
   */
  protected MemoCurrentEvent doMemo;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected MemoOperationImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return QactorPackage.Literals.MEMO_OPERATION;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MemoRule getRule()
  {
    return rule;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetRule(MemoRule newRule, NotificationChain msgs)
  {
    MemoRule oldRule = rule;
    rule = newRule;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, QactorPackage.MEMO_OPERATION__RULE, oldRule, newRule);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setRule(MemoRule newRule)
  {
    if (newRule != rule)
    {
      NotificationChain msgs = null;
      if (rule != null)
        msgs = ((InternalEObject)rule).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - QactorPackage.MEMO_OPERATION__RULE, null, msgs);
      if (newRule != null)
        msgs = ((InternalEObject)newRule).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - QactorPackage.MEMO_OPERATION__RULE, null, msgs);
      msgs = basicSetRule(newRule, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, QactorPackage.MEMO_OPERATION__RULE, newRule, newRule));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public QActor getActor()
  {
    if (actor != null && actor.eIsProxy())
    {
      InternalEObject oldActor = (InternalEObject)actor;
      actor = (QActor)eResolveProxy(oldActor);
      if (actor != oldActor)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, QactorPackage.MEMO_OPERATION__ACTOR, oldActor, actor));
      }
    }
    return actor;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public QActor basicGetActor()
  {
    return actor;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setActor(QActor newActor)
  {
    QActor oldActor = actor;
    actor = newActor;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, QactorPackage.MEMO_OPERATION__ACTOR, oldActor, actor));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MemoCurrentEvent getDoMemo()
  {
    return doMemo;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetDoMemo(MemoCurrentEvent newDoMemo, NotificationChain msgs)
  {
    MemoCurrentEvent oldDoMemo = doMemo;
    doMemo = newDoMemo;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, QactorPackage.MEMO_OPERATION__DO_MEMO, oldDoMemo, newDoMemo);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDoMemo(MemoCurrentEvent newDoMemo)
  {
    if (newDoMemo != doMemo)
    {
      NotificationChain msgs = null;
      if (doMemo != null)
        msgs = ((InternalEObject)doMemo).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - QactorPackage.MEMO_OPERATION__DO_MEMO, null, msgs);
      if (newDoMemo != null)
        msgs = ((InternalEObject)newDoMemo).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - QactorPackage.MEMO_OPERATION__DO_MEMO, null, msgs);
      msgs = basicSetDoMemo(newDoMemo, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, QactorPackage.MEMO_OPERATION__DO_MEMO, newDoMemo, newDoMemo));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case QactorPackage.MEMO_OPERATION__RULE:
        return basicSetRule(null, msgs);
      case QactorPackage.MEMO_OPERATION__DO_MEMO:
        return basicSetDoMemo(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case QactorPackage.MEMO_OPERATION__RULE:
        return getRule();
      case QactorPackage.MEMO_OPERATION__ACTOR:
        if (resolve) return getActor();
        return basicGetActor();
      case QactorPackage.MEMO_OPERATION__DO_MEMO:
        return getDoMemo();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case QactorPackage.MEMO_OPERATION__RULE:
        setRule((MemoRule)newValue);
        return;
      case QactorPackage.MEMO_OPERATION__ACTOR:
        setActor((QActor)newValue);
        return;
      case QactorPackage.MEMO_OPERATION__DO_MEMO:
        setDoMemo((MemoCurrentEvent)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case QactorPackage.MEMO_OPERATION__RULE:
        setRule((MemoRule)null);
        return;
      case QactorPackage.MEMO_OPERATION__ACTOR:
        setActor((QActor)null);
        return;
      case QactorPackage.MEMO_OPERATION__DO_MEMO:
        setDoMemo((MemoCurrentEvent)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case QactorPackage.MEMO_OPERATION__RULE:
        return rule != null;
      case QactorPackage.MEMO_OPERATION__ACTOR:
        return actor != null;
      case QactorPackage.MEMO_OPERATION__DO_MEMO:
        return doMemo != null;
    }
    return super.eIsSet(featureID);
  }

} //MemoOperationImpl
