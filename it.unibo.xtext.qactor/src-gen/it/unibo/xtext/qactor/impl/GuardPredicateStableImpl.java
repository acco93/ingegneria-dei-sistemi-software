/**
 */
package it.unibo.xtext.qactor.impl;

import it.unibo.xtext.qactor.GuardPredicateStable;
import it.unibo.xtext.qactor.QactorPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Guard Predicate Stable</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class GuardPredicateStableImpl extends GuardPredicateImpl implements GuardPredicateStable
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected GuardPredicateStableImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return QactorPackage.Literals.GUARD_PREDICATE_STABLE;
  }

} //GuardPredicateStableImpl
