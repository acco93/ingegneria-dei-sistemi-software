/**
 */
package it.unibo.xtext.qactor.impl;

import it.unibo.xtext.qactor.PHead;
import it.unibo.xtext.qactor.Plan;
import it.unibo.xtext.qactor.QactorPackage;
import it.unibo.xtext.qactor.SolveGoal;
import it.unibo.xtext.qactor.TimeLimit;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Solve Goal</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link it.unibo.xtext.qactor.impl.SolveGoalImpl#getName <em>Name</em>}</li>
 *   <li>{@link it.unibo.xtext.qactor.impl.SolveGoalImpl#getGoal <em>Goal</em>}</li>
 *   <li>{@link it.unibo.xtext.qactor.impl.SolveGoalImpl#getDuration <em>Duration</em>}</li>
 *   <li>{@link it.unibo.xtext.qactor.impl.SolveGoalImpl#getPlan <em>Plan</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class SolveGoalImpl extends ActionMoveImpl implements SolveGoal
{
  /**
   * The default value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected static final String NAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected String name = NAME_EDEFAULT;

  /**
   * The cached value of the '{@link #getGoal() <em>Goal</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getGoal()
   * @generated
   * @ordered
   */
  protected PHead goal;

  /**
   * The cached value of the '{@link #getDuration() <em>Duration</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDuration()
   * @generated
   * @ordered
   */
  protected TimeLimit duration;

  /**
   * The cached value of the '{@link #getPlan() <em>Plan</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPlan()
   * @generated
   * @ordered
   */
  protected Plan plan;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected SolveGoalImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return QactorPackage.Literals.SOLVE_GOAL;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getName()
  {
    return name;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setName(String newName)
  {
    String oldName = name;
    name = newName;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, QactorPackage.SOLVE_GOAL__NAME, oldName, name));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PHead getGoal()
  {
    return goal;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetGoal(PHead newGoal, NotificationChain msgs)
  {
    PHead oldGoal = goal;
    goal = newGoal;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, QactorPackage.SOLVE_GOAL__GOAL, oldGoal, newGoal);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setGoal(PHead newGoal)
  {
    if (newGoal != goal)
    {
      NotificationChain msgs = null;
      if (goal != null)
        msgs = ((InternalEObject)goal).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - QactorPackage.SOLVE_GOAL__GOAL, null, msgs);
      if (newGoal != null)
        msgs = ((InternalEObject)newGoal).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - QactorPackage.SOLVE_GOAL__GOAL, null, msgs);
      msgs = basicSetGoal(newGoal, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, QactorPackage.SOLVE_GOAL__GOAL, newGoal, newGoal));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TimeLimit getDuration()
  {
    return duration;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetDuration(TimeLimit newDuration, NotificationChain msgs)
  {
    TimeLimit oldDuration = duration;
    duration = newDuration;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, QactorPackage.SOLVE_GOAL__DURATION, oldDuration, newDuration);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDuration(TimeLimit newDuration)
  {
    if (newDuration != duration)
    {
      NotificationChain msgs = null;
      if (duration != null)
        msgs = ((InternalEObject)duration).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - QactorPackage.SOLVE_GOAL__DURATION, null, msgs);
      if (newDuration != null)
        msgs = ((InternalEObject)newDuration).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - QactorPackage.SOLVE_GOAL__DURATION, null, msgs);
      msgs = basicSetDuration(newDuration, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, QactorPackage.SOLVE_GOAL__DURATION, newDuration, newDuration));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Plan getPlan()
  {
    if (plan != null && plan.eIsProxy())
    {
      InternalEObject oldPlan = (InternalEObject)plan;
      plan = (Plan)eResolveProxy(oldPlan);
      if (plan != oldPlan)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, QactorPackage.SOLVE_GOAL__PLAN, oldPlan, plan));
      }
    }
    return plan;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Plan basicGetPlan()
  {
    return plan;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setPlan(Plan newPlan)
  {
    Plan oldPlan = plan;
    plan = newPlan;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, QactorPackage.SOLVE_GOAL__PLAN, oldPlan, plan));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case QactorPackage.SOLVE_GOAL__GOAL:
        return basicSetGoal(null, msgs);
      case QactorPackage.SOLVE_GOAL__DURATION:
        return basicSetDuration(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case QactorPackage.SOLVE_GOAL__NAME:
        return getName();
      case QactorPackage.SOLVE_GOAL__GOAL:
        return getGoal();
      case QactorPackage.SOLVE_GOAL__DURATION:
        return getDuration();
      case QactorPackage.SOLVE_GOAL__PLAN:
        if (resolve) return getPlan();
        return basicGetPlan();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case QactorPackage.SOLVE_GOAL__NAME:
        setName((String)newValue);
        return;
      case QactorPackage.SOLVE_GOAL__GOAL:
        setGoal((PHead)newValue);
        return;
      case QactorPackage.SOLVE_GOAL__DURATION:
        setDuration((TimeLimit)newValue);
        return;
      case QactorPackage.SOLVE_GOAL__PLAN:
        setPlan((Plan)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case QactorPackage.SOLVE_GOAL__NAME:
        setName(NAME_EDEFAULT);
        return;
      case QactorPackage.SOLVE_GOAL__GOAL:
        setGoal((PHead)null);
        return;
      case QactorPackage.SOLVE_GOAL__DURATION:
        setDuration((TimeLimit)null);
        return;
      case QactorPackage.SOLVE_GOAL__PLAN:
        setPlan((Plan)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case QactorPackage.SOLVE_GOAL__NAME:
        return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
      case QactorPackage.SOLVE_GOAL__GOAL:
        return goal != null;
      case QactorPackage.SOLVE_GOAL__DURATION:
        return duration != null;
      case QactorPackage.SOLVE_GOAL__PLAN:
        return plan != null;
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (name: ");
    result.append(name);
    result.append(')');
    return result.toString();
  }

} //SolveGoalImpl
