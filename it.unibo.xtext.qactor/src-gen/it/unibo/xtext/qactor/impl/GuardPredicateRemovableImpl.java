/**
 */
package it.unibo.xtext.qactor.impl;

import it.unibo.xtext.qactor.GuardPredicateRemovable;
import it.unibo.xtext.qactor.QactorPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Guard Predicate Removable</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class GuardPredicateRemovableImpl extends GuardPredicateImpl implements GuardPredicateRemovable
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected GuardPredicateRemovableImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return QactorPackage.Literals.GUARD_PREDICATE_REMOVABLE;
  }

} //GuardPredicateRemovableImpl
