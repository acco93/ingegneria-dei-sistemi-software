/**
 */
package it.unibo.xtext.qactor.impl;

import it.unibo.xtext.qactor.AnswerEvent;
import it.unibo.xtext.qactor.MoveFile;
import it.unibo.xtext.qactor.Photo;
import it.unibo.xtext.qactor.QactorPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Photo</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link it.unibo.xtext.qactor.impl.PhotoImpl#getDestfile <em>Destfile</em>}</li>
 *   <li>{@link it.unibo.xtext.qactor.impl.PhotoImpl#getAnswerEvent <em>Answer Event</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class PhotoImpl extends ExtensionMoveImpl implements Photo
{
  /**
   * The cached value of the '{@link #getDestfile() <em>Destfile</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDestfile()
   * @generated
   * @ordered
   */
  protected MoveFile destfile;

  /**
   * The cached value of the '{@link #getAnswerEvent() <em>Answer Event</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAnswerEvent()
   * @generated
   * @ordered
   */
  protected AnswerEvent answerEvent;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected PhotoImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return QactorPackage.Literals.PHOTO;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MoveFile getDestfile()
  {
    return destfile;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetDestfile(MoveFile newDestfile, NotificationChain msgs)
  {
    MoveFile oldDestfile = destfile;
    destfile = newDestfile;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, QactorPackage.PHOTO__DESTFILE, oldDestfile, newDestfile);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDestfile(MoveFile newDestfile)
  {
    if (newDestfile != destfile)
    {
      NotificationChain msgs = null;
      if (destfile != null)
        msgs = ((InternalEObject)destfile).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - QactorPackage.PHOTO__DESTFILE, null, msgs);
      if (newDestfile != null)
        msgs = ((InternalEObject)newDestfile).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - QactorPackage.PHOTO__DESTFILE, null, msgs);
      msgs = basicSetDestfile(newDestfile, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, QactorPackage.PHOTO__DESTFILE, newDestfile, newDestfile));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AnswerEvent getAnswerEvent()
  {
    return answerEvent;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetAnswerEvent(AnswerEvent newAnswerEvent, NotificationChain msgs)
  {
    AnswerEvent oldAnswerEvent = answerEvent;
    answerEvent = newAnswerEvent;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, QactorPackage.PHOTO__ANSWER_EVENT, oldAnswerEvent, newAnswerEvent);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setAnswerEvent(AnswerEvent newAnswerEvent)
  {
    if (newAnswerEvent != answerEvent)
    {
      NotificationChain msgs = null;
      if (answerEvent != null)
        msgs = ((InternalEObject)answerEvent).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - QactorPackage.PHOTO__ANSWER_EVENT, null, msgs);
      if (newAnswerEvent != null)
        msgs = ((InternalEObject)newAnswerEvent).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - QactorPackage.PHOTO__ANSWER_EVENT, null, msgs);
      msgs = basicSetAnswerEvent(newAnswerEvent, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, QactorPackage.PHOTO__ANSWER_EVENT, newAnswerEvent, newAnswerEvent));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case QactorPackage.PHOTO__DESTFILE:
        return basicSetDestfile(null, msgs);
      case QactorPackage.PHOTO__ANSWER_EVENT:
        return basicSetAnswerEvent(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case QactorPackage.PHOTO__DESTFILE:
        return getDestfile();
      case QactorPackage.PHOTO__ANSWER_EVENT:
        return getAnswerEvent();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case QactorPackage.PHOTO__DESTFILE:
        setDestfile((MoveFile)newValue);
        return;
      case QactorPackage.PHOTO__ANSWER_EVENT:
        setAnswerEvent((AnswerEvent)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case QactorPackage.PHOTO__DESTFILE:
        setDestfile((MoveFile)null);
        return;
      case QactorPackage.PHOTO__ANSWER_EVENT:
        setAnswerEvent((AnswerEvent)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case QactorPackage.PHOTO__DESTFILE:
        return destfile != null;
      case QactorPackage.PHOTO__ANSWER_EVENT:
        return answerEvent != null;
    }
    return super.eIsSet(featureID);
  }

} //PhotoImpl
