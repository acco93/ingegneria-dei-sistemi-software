/**
 */
package it.unibo.xtext.qactor.impl;

import it.unibo.xtext.qactor.Plan;
import it.unibo.xtext.qactor.PlanAction;
import it.unibo.xtext.qactor.QactorPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Plan</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link it.unibo.xtext.qactor.impl.PlanImpl#getName <em>Name</em>}</li>
 *   <li>{@link it.unibo.xtext.qactor.impl.PlanImpl#isNormal <em>Normal</em>}</li>
 *   <li>{@link it.unibo.xtext.qactor.impl.PlanImpl#isResume <em>Resume</em>}</li>
 *   <li>{@link it.unibo.xtext.qactor.impl.PlanImpl#getAction <em>Action</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class PlanImpl extends MinimalEObjectImpl.Container implements Plan
{
  /**
   * The default value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected static final String NAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected String name = NAME_EDEFAULT;

  /**
   * The default value of the '{@link #isNormal() <em>Normal</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isNormal()
   * @generated
   * @ordered
   */
  protected static final boolean NORMAL_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isNormal() <em>Normal</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isNormal()
   * @generated
   * @ordered
   */
  protected boolean normal = NORMAL_EDEFAULT;

  /**
   * The default value of the '{@link #isResume() <em>Resume</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isResume()
   * @generated
   * @ordered
   */
  protected static final boolean RESUME_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isResume() <em>Resume</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isResume()
   * @generated
   * @ordered
   */
  protected boolean resume = RESUME_EDEFAULT;

  /**
   * The cached value of the '{@link #getAction() <em>Action</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAction()
   * @generated
   * @ordered
   */
  protected EList<PlanAction> action;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected PlanImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return QactorPackage.Literals.PLAN;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getName()
  {
    return name;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setName(String newName)
  {
    String oldName = name;
    name = newName;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, QactorPackage.PLAN__NAME, oldName, name));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isNormal()
  {
    return normal;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setNormal(boolean newNormal)
  {
    boolean oldNormal = normal;
    normal = newNormal;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, QactorPackage.PLAN__NORMAL, oldNormal, normal));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isResume()
  {
    return resume;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setResume(boolean newResume)
  {
    boolean oldResume = resume;
    resume = newResume;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, QactorPackage.PLAN__RESUME, oldResume, resume));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<PlanAction> getAction()
  {
    if (action == null)
    {
      action = new EObjectContainmentEList<PlanAction>(PlanAction.class, this, QactorPackage.PLAN__ACTION);
    }
    return action;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case QactorPackage.PLAN__ACTION:
        return ((InternalEList<?>)getAction()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case QactorPackage.PLAN__NAME:
        return getName();
      case QactorPackage.PLAN__NORMAL:
        return isNormal();
      case QactorPackage.PLAN__RESUME:
        return isResume();
      case QactorPackage.PLAN__ACTION:
        return getAction();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case QactorPackage.PLAN__NAME:
        setName((String)newValue);
        return;
      case QactorPackage.PLAN__NORMAL:
        setNormal((Boolean)newValue);
        return;
      case QactorPackage.PLAN__RESUME:
        setResume((Boolean)newValue);
        return;
      case QactorPackage.PLAN__ACTION:
        getAction().clear();
        getAction().addAll((Collection<? extends PlanAction>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case QactorPackage.PLAN__NAME:
        setName(NAME_EDEFAULT);
        return;
      case QactorPackage.PLAN__NORMAL:
        setNormal(NORMAL_EDEFAULT);
        return;
      case QactorPackage.PLAN__RESUME:
        setResume(RESUME_EDEFAULT);
        return;
      case QactorPackage.PLAN__ACTION:
        getAction().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case QactorPackage.PLAN__NAME:
        return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
      case QactorPackage.PLAN__NORMAL:
        return normal != NORMAL_EDEFAULT;
      case QactorPackage.PLAN__RESUME:
        return resume != RESUME_EDEFAULT;
      case QactorPackage.PLAN__ACTION:
        return action != null && !action.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (name: ");
    result.append(name);
    result.append(", normal: ");
    result.append(normal);
    result.append(", resume: ");
    result.append(resume);
    result.append(')');
    return result.toString();
  }

} //PlanImpl
