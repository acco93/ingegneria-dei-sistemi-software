/**
 */
package it.unibo.xtext.qactor.impl;

import it.unibo.xtext.qactor.OutInMessage;
import it.unibo.xtext.qactor.QactorPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Out In Message</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class OutInMessageImpl extends MessageImpl implements OutInMessage
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected OutInMessageImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return QactorPackage.Literals.OUT_IN_MESSAGE;
  }

} //OutInMessageImpl
