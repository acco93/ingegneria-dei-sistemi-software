/**
 */
package it.unibo.xtext.qactor;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Solve Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link it.unibo.xtext.qactor.SolveOperation#getGoal <em>Goal</em>}</li>
 *   <li>{@link it.unibo.xtext.qactor.SolveOperation#getActor <em>Actor</em>}</li>
 * </ul>
 * </p>
 *
 * @see it.unibo.xtext.qactor.QactorPackage#getSolveOperation()
 * @model
 * @generated
 */
public interface SolveOperation extends EventHandlerOperation
{
  /**
   * Returns the value of the '<em><b>Goal</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Goal</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Goal</em>' containment reference.
   * @see #setGoal(PTerm)
   * @see it.unibo.xtext.qactor.QactorPackage#getSolveOperation_Goal()
   * @model containment="true"
   * @generated
   */
  PTerm getGoal();

  /**
   * Sets the value of the '{@link it.unibo.xtext.qactor.SolveOperation#getGoal <em>Goal</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Goal</em>' containment reference.
   * @see #getGoal()
   * @generated
   */
  void setGoal(PTerm value);

  /**
   * Returns the value of the '<em><b>Actor</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Actor</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Actor</em>' reference.
   * @see #setActor(QActor)
   * @see it.unibo.xtext.qactor.QactorPackage#getSolveOperation_Actor()
   * @model
   * @generated
   */
  QActor getActor();

  /**
   * Sets the value of the '{@link it.unibo.xtext.qactor.SolveOperation#getActor <em>Actor</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Actor</em>' reference.
   * @see #getActor()
   * @generated
   */
  void setActor(QActor value);

} // SolveOperation
