/**
 */
package it.unibo.xtext.qactor;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Sound</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link it.unibo.xtext.qactor.Sound#getSrcfile <em>Srcfile</em>}</li>
 *   <li>{@link it.unibo.xtext.qactor.Sound#getAnswerEvent <em>Answer Event</em>}</li>
 * </ul>
 * </p>
 *
 * @see it.unibo.xtext.qactor.QactorPackage#getSound()
 * @model
 * @generated
 */
public interface Sound extends ExtensionMove
{
  /**
   * Returns the value of the '<em><b>Srcfile</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Srcfile</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Srcfile</em>' containment reference.
   * @see #setSrcfile(MoveFile)
   * @see it.unibo.xtext.qactor.QactorPackage#getSound_Srcfile()
   * @model containment="true"
   * @generated
   */
  MoveFile getSrcfile();

  /**
   * Sets the value of the '{@link it.unibo.xtext.qactor.Sound#getSrcfile <em>Srcfile</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Srcfile</em>' containment reference.
   * @see #getSrcfile()
   * @generated
   */
  void setSrcfile(MoveFile value);

  /**
   * Returns the value of the '<em><b>Answer Event</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Answer Event</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Answer Event</em>' containment reference.
   * @see #setAnswerEvent(AnswerEvent)
   * @see it.unibo.xtext.qactor.QactorPackage#getSound_AnswerEvent()
   * @model containment="true"
   * @generated
   */
  AnswerEvent getAnswerEvent();

  /**
   * Sets the value of the '{@link it.unibo.xtext.qactor.Sound#getAnswerEvent <em>Answer Event</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Answer Event</em>' containment reference.
   * @see #getAnswerEvent()
   * @generated
   */
  void setAnswerEvent(AnswerEvent value);

} // Sound
