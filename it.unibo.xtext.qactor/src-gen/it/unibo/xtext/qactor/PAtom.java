/**
 */
package it.unibo.xtext.qactor;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>PAtom</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see it.unibo.xtext.qactor.QactorPackage#getPAtom()
 * @model
 * @generated
 */
public interface PAtom extends PHead, PTerm
{
} // PAtom
