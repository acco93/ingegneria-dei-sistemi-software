/**
 */
package it.unibo.xtext.qactor;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Receive Msg</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link it.unibo.xtext.qactor.ReceiveMsg#getName <em>Name</em>}</li>
 *   <li>{@link it.unibo.xtext.qactor.ReceiveMsg#getDuration <em>Duration</em>}</li>
 *   <li>{@link it.unibo.xtext.qactor.ReceiveMsg#getSpec <em>Spec</em>}</li>
 * </ul>
 * </p>
 *
 * @see it.unibo.xtext.qactor.QactorPackage#getReceiveMsg()
 * @model
 * @generated
 */
public interface ReceiveMsg extends MessageMove
{
  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see it.unibo.xtext.qactor.QactorPackage#getReceiveMsg_Name()
   * @model
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link it.unibo.xtext.qactor.ReceiveMsg#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

  /**
   * Returns the value of the '<em><b>Duration</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Duration</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Duration</em>' containment reference.
   * @see #setDuration(TimeLimit)
   * @see it.unibo.xtext.qactor.QactorPackage#getReceiveMsg_Duration()
   * @model containment="true"
   * @generated
   */
  TimeLimit getDuration();

  /**
   * Sets the value of the '{@link it.unibo.xtext.qactor.ReceiveMsg#getDuration <em>Duration</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Duration</em>' containment reference.
   * @see #getDuration()
   * @generated
   */
  void setDuration(TimeLimit value);

  /**
   * Returns the value of the '<em><b>Spec</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Spec</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Spec</em>' containment reference.
   * @see #setSpec(MsgSpec)
   * @see it.unibo.xtext.qactor.QactorPackage#getReceiveMsg_Spec()
   * @model containment="true"
   * @generated
   */
  MsgSpec getSpec();

  /**
   * Sets the value of the '{@link it.unibo.xtext.qactor.ReceiveMsg#getSpec <em>Spec</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Spec</em>' containment reference.
   * @see #getSpec()
   * @generated
   */
  void setSpec(MsgSpec value);

} // ReceiveMsg
