/**
 */
package it.unibo.xtext.qactor;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>PStruct</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link it.unibo.xtext.qactor.PStruct#getName <em>Name</em>}</li>
 *   <li>{@link it.unibo.xtext.qactor.PStruct#getMsgArg <em>Msg Arg</em>}</li>
 * </ul>
 * </p>
 *
 * @see it.unibo.xtext.qactor.QactorPackage#getPStruct()
 * @model
 * @generated
 */
public interface PStruct extends PHead, PTerm
{
  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see it.unibo.xtext.qactor.QactorPackage#getPStruct_Name()
   * @model
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link it.unibo.xtext.qactor.PStruct#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

  /**
   * Returns the value of the '<em><b>Msg Arg</b></em>' containment reference list.
   * The list contents are of type {@link it.unibo.xtext.qactor.PTerm}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Msg Arg</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Msg Arg</em>' containment reference list.
   * @see it.unibo.xtext.qactor.QactorPackage#getPStruct_MsgArg()
   * @model containment="true"
   * @generated
   */
  EList<PTerm> getMsgArg();

} // PStruct
