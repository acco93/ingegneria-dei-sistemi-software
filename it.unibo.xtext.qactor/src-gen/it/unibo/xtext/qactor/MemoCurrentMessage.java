/**
 */
package it.unibo.xtext.qactor;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Memo Current Message</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link it.unibo.xtext.qactor.MemoCurrentMessage#isLastonly <em>Lastonly</em>}</li>
 * </ul>
 * </p>
 *
 * @see it.unibo.xtext.qactor.QactorPackage#getMemoCurrentMessage()
 * @model
 * @generated
 */
public interface MemoCurrentMessage extends BasicMove
{
  /**
   * Returns the value of the '<em><b>Lastonly</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Lastonly</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Lastonly</em>' attribute.
   * @see #setLastonly(boolean)
   * @see it.unibo.xtext.qactor.QactorPackage#getMemoCurrentMessage_Lastonly()
   * @model
   * @generated
   */
  boolean isLastonly();

  /**
   * Sets the value of the '{@link it.unibo.xtext.qactor.MemoCurrentMessage#isLastonly <em>Lastonly</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Lastonly</em>' attribute.
   * @see #isLastonly()
   * @generated
   */
  void setLastonly(boolean value);

} // MemoCurrentMessage
