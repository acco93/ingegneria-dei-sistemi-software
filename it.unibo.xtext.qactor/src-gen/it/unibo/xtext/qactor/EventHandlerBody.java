/**
 */
package it.unibo.xtext.qactor;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Event Handler Body</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link it.unibo.xtext.qactor.EventHandlerBody#getOp <em>Op</em>}</li>
 * </ul>
 * </p>
 *
 * @see it.unibo.xtext.qactor.QactorPackage#getEventHandlerBody()
 * @model
 * @generated
 */
public interface EventHandlerBody extends EObject
{
  /**
   * Returns the value of the '<em><b>Op</b></em>' containment reference list.
   * The list contents are of type {@link it.unibo.xtext.qactor.EventHandlerOperation}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Op</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Op</em>' containment reference list.
   * @see it.unibo.xtext.qactor.QactorPackage#getEventHandlerBody_Op()
   * @model containment="true"
   * @generated
   */
  EList<EventHandlerOperation> getOp();

} // EventHandlerBody
