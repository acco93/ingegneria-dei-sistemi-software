/**
 */
package it.unibo.xtext.qactor;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Event Switch</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link it.unibo.xtext.qactor.EventSwitch#getEvent <em>Event</em>}</li>
 *   <li>{@link it.unibo.xtext.qactor.EventSwitch#getMsg <em>Msg</em>}</li>
 *   <li>{@link it.unibo.xtext.qactor.EventSwitch#getMove <em>Move</em>}</li>
 * </ul>
 * </p>
 *
 * @see it.unibo.xtext.qactor.QactorPackage#getEventSwitch()
 * @model
 * @generated
 */
public interface EventSwitch extends MessageMove
{
  /**
   * Returns the value of the '<em><b>Event</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Event</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Event</em>' reference.
   * @see #setEvent(Event)
   * @see it.unibo.xtext.qactor.QactorPackage#getEventSwitch_Event()
   * @model
   * @generated
   */
  Event getEvent();

  /**
   * Sets the value of the '{@link it.unibo.xtext.qactor.EventSwitch#getEvent <em>Event</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Event</em>' reference.
   * @see #getEvent()
   * @generated
   */
  void setEvent(Event value);

  /**
   * Returns the value of the '<em><b>Msg</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Msg</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Msg</em>' containment reference.
   * @see #setMsg(PHead)
   * @see it.unibo.xtext.qactor.QactorPackage#getEventSwitch_Msg()
   * @model containment="true"
   * @generated
   */
  PHead getMsg();

  /**
   * Sets the value of the '{@link it.unibo.xtext.qactor.EventSwitch#getMsg <em>Msg</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Msg</em>' containment reference.
   * @see #getMsg()
   * @generated
   */
  void setMsg(PHead value);

  /**
   * Returns the value of the '<em><b>Move</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Move</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Move</em>' containment reference.
   * @see #setMove(Move)
   * @see it.unibo.xtext.qactor.QactorPackage#getEventSwitch_Move()
   * @model containment="true"
   * @generated
   */
  Move getMove();

  /**
   * Sets the value of the '{@link it.unibo.xtext.qactor.EventSwitch#getMove <em>Move</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Move</em>' containment reference.
   * @see #getMove()
   * @generated
   */
  void setMove(Move value);

} // EventSwitch
