/**
 */
package it.unibo.xtext.qactor;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Switch Plan</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link it.unibo.xtext.qactor.SwitchPlan#getPlan <em>Plan</em>}</li>
 * </ul>
 * </p>
 *
 * @see it.unibo.xtext.qactor.QactorPackage#getSwitchPlan()
 * @model
 * @generated
 */
public interface SwitchPlan extends PlanMove
{
  /**
   * Returns the value of the '<em><b>Plan</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Plan</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Plan</em>' reference.
   * @see #setPlan(Plan)
   * @see it.unibo.xtext.qactor.QactorPackage#getSwitchPlan_Plan()
   * @model
   * @generated
   */
  Plan getPlan();

  /**
   * Sets the value of the '{@link it.unibo.xtext.qactor.SwitchPlan#getPlan <em>Plan</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Plan</em>' reference.
   * @see #getPlan()
   * @generated
   */
  void setPlan(Plan value);

} // SwitchPlan
