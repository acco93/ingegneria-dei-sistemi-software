/**
 */
package it.unibo.xtext.qactor;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Run Plan</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link it.unibo.xtext.qactor.RunPlan#getPlainid <em>Plainid</em>}</li>
 * </ul>
 * </p>
 *
 * @see it.unibo.xtext.qactor.QactorPackage#getRunPlan()
 * @model
 * @generated
 */
public interface RunPlan extends PlanMove
{
  /**
   * Returns the value of the '<em><b>Plainid</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Plainid</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Plainid</em>' containment reference.
   * @see #setPlainid(VarOrAtomic)
   * @see it.unibo.xtext.qactor.QactorPackage#getRunPlan_Plainid()
   * @model containment="true"
   * @generated
   */
  VarOrAtomic getPlainid();

  /**
   * Sets the value of the '{@link it.unibo.xtext.qactor.RunPlan#getPlainid <em>Plainid</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Plainid</em>' containment reference.
   * @see #getPlainid()
   * @generated
   */
  void setPlainid(VarOrAtomic value);

} // RunPlan
