/**
 */
package it.unibo.xtext.qactor;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Var Or Phead</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link it.unibo.xtext.qactor.VarOrPhead#getVar <em>Var</em>}</li>
 *   <li>{@link it.unibo.xtext.qactor.VarOrPhead#getPhead <em>Phead</em>}</li>
 * </ul>
 * </p>
 *
 * @see it.unibo.xtext.qactor.QactorPackage#getVarOrPhead()
 * @model
 * @generated
 */
public interface VarOrPhead extends EObject
{
  /**
   * Returns the value of the '<em><b>Var</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Var</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Var</em>' containment reference.
   * @see #setVar(Variable)
   * @see it.unibo.xtext.qactor.QactorPackage#getVarOrPhead_Var()
   * @model containment="true"
   * @generated
   */
  Variable getVar();

  /**
   * Sets the value of the '{@link it.unibo.xtext.qactor.VarOrPhead#getVar <em>Var</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Var</em>' containment reference.
   * @see #getVar()
   * @generated
   */
  void setVar(Variable value);

  /**
   * Returns the value of the '<em><b>Phead</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Phead</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Phead</em>' containment reference.
   * @see #setPhead(PHead)
   * @see it.unibo.xtext.qactor.QactorPackage#getVarOrPhead_Phead()
   * @model containment="true"
   * @generated
   */
  PHead getPhead();

  /**
   * Sets the value of the '{@link it.unibo.xtext.qactor.VarOrPhead#getPhead <em>Phead</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Phead</em>' containment reference.
   * @see #getPhead()
   * @generated
   */
  void setPhead(PHead value);

} // VarOrPhead
