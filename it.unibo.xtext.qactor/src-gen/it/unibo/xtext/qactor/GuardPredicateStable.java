/**
 */
package it.unibo.xtext.qactor;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Guard Predicate Stable</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see it.unibo.xtext.qactor.QactorPackage#getGuardPredicateStable()
 * @model
 * @generated
 */
public interface GuardPredicateStable extends GuardPredicate
{
} // GuardPredicateStable
