/**
 */
package it.unibo.xtext.qactor.util;

import it.unibo.xtext.qactor.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see it.unibo.xtext.qactor.QactorPackage
 * @generated
 */
public class QactorAdapterFactory extends AdapterFactoryImpl
{
  /**
   * The cached model package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected static QactorPackage modelPackage;

  /**
   * Creates an instance of the adapter factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public QactorAdapterFactory()
  {
    if (modelPackage == null)
    {
      modelPackage = QactorPackage.eINSTANCE;
    }
  }

  /**
   * Returns whether this factory is applicable for the type of the object.
   * <!-- begin-user-doc -->
   * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
   * <!-- end-user-doc -->
   * @return whether this factory is applicable for the type of the object.
   * @generated
   */
  @Override
  public boolean isFactoryForType(Object object)
  {
    if (object == modelPackage)
    {
      return true;
    }
    if (object instanceof EObject)
    {
      return ((EObject)object).eClass().getEPackage() == modelPackage;
    }
    return false;
  }

  /**
   * The switch that delegates to the <code>createXXX</code> methods.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected QactorSwitch<Adapter> modelSwitch =
    new QactorSwitch<Adapter>()
    {
      @Override
      public Adapter caseQActorSystem(QActorSystem object)
      {
        return createQActorSystemAdapter();
      }
      @Override
      public Adapter caseQActorSystemSpec(QActorSystemSpec object)
      {
        return createQActorSystemSpecAdapter();
      }
      @Override
      public Adapter caseRobot(Robot object)
      {
        return createRobotAdapter();
      }
      @Override
      public Adapter caseMessage(Message object)
      {
        return createMessageAdapter();
      }
      @Override
      public Adapter caseOutOnlyMessage(OutOnlyMessage object)
      {
        return createOutOnlyMessageAdapter();
      }
      @Override
      public Adapter caseOutInMessage(OutInMessage object)
      {
        return createOutInMessageAdapter();
      }
      @Override
      public Adapter caseEvent(Event object)
      {
        return createEventAdapter();
      }
      @Override
      public Adapter caseSignal(Signal object)
      {
        return createSignalAdapter();
      }
      @Override
      public Adapter caseToken(Token object)
      {
        return createTokenAdapter();
      }
      @Override
      public Adapter caseDispatch(Dispatch object)
      {
        return createDispatchAdapter();
      }
      @Override
      public Adapter caseRequest(Request object)
      {
        return createRequestAdapter();
      }
      @Override
      public Adapter caseInvitation(Invitation object)
      {
        return createInvitationAdapter();
      }
      @Override
      public Adapter caseContext(Context object)
      {
        return createContextAdapter();
      }
      @Override
      public Adapter caseQActor(QActor object)
      {
        return createQActorAdapter();
      }
      @Override
      public Adapter caseRule(Rule object)
      {
        return createRuleAdapter();
      }
      @Override
      public Adapter casePHead(PHead object)
      {
        return createPHeadAdapter();
      }
      @Override
      public Adapter casePTerm(PTerm object)
      {
        return createPTermAdapter();
      }
      @Override
      public Adapter casePAtom(PAtom object)
      {
        return createPAtomAdapter();
      }
      @Override
      public Adapter casePAtomString(PAtomString object)
      {
        return createPAtomStringAdapter();
      }
      @Override
      public Adapter casePAtomic(PAtomic object)
      {
        return createPAtomicAdapter();
      }
      @Override
      public Adapter casePAtomNum(PAtomNum object)
      {
        return createPAtomNumAdapter();
      }
      @Override
      public Adapter casePStruct(PStruct object)
      {
        return createPStructAdapter();
      }
      @Override
      public Adapter casePActorCall(PActorCall object)
      {
        return createPActorCallAdapter();
      }
      @Override
      public Adapter casePPredef(PPredef object)
      {
        return createPPredefAdapter();
      }
      @Override
      public Adapter casePIs(PIs object)
      {
        return createPIsAdapter();
      }
      @Override
      public Adapter casePAtomCut(PAtomCut object)
      {
        return createPAtomCutAdapter();
      }
      @Override
      public Adapter caseData(Data object)
      {
        return createDataAdapter();
      }
      @Override
      public Adapter caseIntegerData(IntegerData object)
      {
        return createIntegerDataAdapter();
      }
      @Override
      public Adapter caseStringData(StringData object)
      {
        return createStringDataAdapter();
      }
      @Override
      public Adapter caseAction(Action object)
      {
        return createActionAdapter();
      }
      @Override
      public Adapter casePlan(Plan object)
      {
        return createPlanAdapter();
      }
      @Override
      public Adapter casePlanAction(PlanAction object)
      {
        return createPlanActionAdapter();
      }
      @Override
      public Adapter caseGuard(Guard object)
      {
        return createGuardAdapter();
      }
      @Override
      public Adapter caseGuardPredicate(GuardPredicate object)
      {
        return createGuardPredicateAdapter();
      }
      @Override
      public Adapter caseGuardPredicateRemovable(GuardPredicateRemovable object)
      {
        return createGuardPredicateRemovableAdapter();
      }
      @Override
      public Adapter caseGuardPredicateStable(GuardPredicateStable object)
      {
        return createGuardPredicateStableAdapter();
      }
      @Override
      public Adapter caseMove(Move object)
      {
        return createMoveAdapter();
      }
      @Override
      public Adapter caseActionMove(ActionMove object)
      {
        return createActionMoveAdapter();
      }
      @Override
      public Adapter caseExecuteAction(ExecuteAction object)
      {
        return createExecuteActionAdapter();
      }
      @Override
      public Adapter caseSolveGoal(SolveGoal object)
      {
        return createSolveGoalAdapter();
      }
      @Override
      public Adapter caseDemo(Demo object)
      {
        return createDemoAdapter();
      }
      @Override
      public Adapter caseActorOp(ActorOp object)
      {
        return createActorOpAdapter();
      }
      @Override
      public Adapter caseBasicRobotMove(BasicRobotMove object)
      {
        return createBasicRobotMoveAdapter();
      }
      @Override
      public Adapter caseBasicMove(BasicMove object)
      {
        return createBasicMoveAdapter();
      }
      @Override
      public Adapter casePrint(Print object)
      {
        return createPrintAdapter();
      }
      @Override
      public Adapter casePrintCurrentEvent(PrintCurrentEvent object)
      {
        return createPrintCurrentEventAdapter();
      }
      @Override
      public Adapter casePrintCurrentMessage(PrintCurrentMessage object)
      {
        return createPrintCurrentMessageAdapter();
      }
      @Override
      public Adapter caseMemoCurrentEvent(MemoCurrentEvent object)
      {
        return createMemoCurrentEventAdapter();
      }
      @Override
      public Adapter caseMemoCurrentMessage(MemoCurrentMessage object)
      {
        return createMemoCurrentMessageAdapter();
      }
      @Override
      public Adapter casePlanMove(PlanMove object)
      {
        return createPlanMoveAdapter();
      }
      @Override
      public Adapter caseGetActivationEvent(GetActivationEvent object)
      {
        return createGetActivationEventAdapter();
      }
      @Override
      public Adapter caseGetSensedEvent(GetSensedEvent object)
      {
        return createGetSensedEventAdapter();
      }
      @Override
      public Adapter caseLoadPlan(LoadPlan object)
      {
        return createLoadPlanAdapter();
      }
      @Override
      public Adapter caseRunPlan(RunPlan object)
      {
        return createRunPlanAdapter();
      }
      @Override
      public Adapter caseResumePlan(ResumePlan object)
      {
        return createResumePlanAdapter();
      }
      @Override
      public Adapter caseSuspendPlan(SuspendPlan object)
      {
        return createSuspendPlanAdapter();
      }
      @Override
      public Adapter caseRepeatPlan(RepeatPlan object)
      {
        return createRepeatPlanAdapter();
      }
      @Override
      public Adapter caseSwitchPlan(SwitchPlan object)
      {
        return createSwitchPlanAdapter();
      }
      @Override
      public Adapter caseEndPlan(EndPlan object)
      {
        return createEndPlanAdapter();
      }
      @Override
      public Adapter caseEndActor(EndActor object)
      {
        return createEndActorAdapter();
      }
      @Override
      public Adapter caseGuardMove(GuardMove object)
      {
        return createGuardMoveAdapter();
      }
      @Override
      public Adapter caseAddRule(AddRule object)
      {
        return createAddRuleAdapter();
      }
      @Override
      public Adapter caseRemoveRule(RemoveRule object)
      {
        return createRemoveRuleAdapter();
      }
      @Override
      public Adapter caseMessageMove(MessageMove object)
      {
        return createMessageMoveAdapter();
      }
      @Override
      public Adapter caseSendDispatch(SendDispatch object)
      {
        return createSendDispatchAdapter();
      }
      @Override
      public Adapter caseSendRequest(SendRequest object)
      {
        return createSendRequestAdapter();
      }
      @Override
      public Adapter caseReplyToCaller(ReplyToCaller object)
      {
        return createReplyToCallerAdapter();
      }
      @Override
      public Adapter caseReceiveMsg(ReceiveMsg object)
      {
        return createReceiveMsgAdapter();
      }
      @Override
      public Adapter caseMsgSpec(MsgSpec object)
      {
        return createMsgSpecAdapter();
      }
      @Override
      public Adapter caseOnReceiveMsg(OnReceiveMsg object)
      {
        return createOnReceiveMsgAdapter();
      }
      @Override
      public Adapter caseMsgSelect(MsgSelect object)
      {
        return createMsgSelectAdapter();
      }
      @Override
      public Adapter caseRaiseEvent(RaiseEvent object)
      {
        return createRaiseEventAdapter();
      }
      @Override
      public Adapter caseSenseEvent(SenseEvent object)
      {
        return createSenseEventAdapter();
      }
      @Override
      public Adapter caseMsgSwitch(MsgSwitch object)
      {
        return createMsgSwitchAdapter();
      }
      @Override
      public Adapter caseEventSwitch(EventSwitch object)
      {
        return createEventSwitchAdapter();
      }
      @Override
      public Adapter caseContinuation(Continuation object)
      {
        return createContinuationAdapter();
      }
      @Override
      public Adapter caseExtensionMove(ExtensionMove object)
      {
        return createExtensionMoveAdapter();
      }
      @Override
      public Adapter casePhoto(Photo object)
      {
        return createPhotoAdapter();
      }
      @Override
      public Adapter caseSound(Sound object)
      {
        return createSoundAdapter();
      }
      @Override
      public Adapter caseVideo(Video object)
      {
        return createVideoAdapter();
      }
      @Override
      public Adapter caseDelay(Delay object)
      {
        return createDelayAdapter();
      }
      @Override
      public Adapter caseAnswerEvent(AnswerEvent object)
      {
        return createAnswerEventAdapter();
      }
      @Override
      public Adapter caseEventHandler(EventHandler object)
      {
        return createEventHandlerAdapter();
      }
      @Override
      public Adapter caseEventHandlerBody(EventHandlerBody object)
      {
        return createEventHandlerBodyAdapter();
      }
      @Override
      public Adapter caseEventHandlerOperation(EventHandlerOperation object)
      {
        return createEventHandlerOperationAdapter();
      }
      @Override
      public Adapter caseMemoOperation(MemoOperation object)
      {
        return createMemoOperationAdapter();
      }
      @Override
      public Adapter caseSolveOperation(SolveOperation object)
      {
        return createSolveOperationAdapter();
      }
      @Override
      public Adapter caseSendEventAsDispatch(SendEventAsDispatch object)
      {
        return createSendEventAsDispatchAdapter();
      }
      @Override
      public Adapter caseMemoRule(MemoRule object)
      {
        return createMemoRuleAdapter();
      }
      @Override
      public Adapter caseMemoEvent(MemoEvent object)
      {
        return createMemoEventAdapter();
      }
      @Override
      public Adapter caseReaction(Reaction object)
      {
        return createReactionAdapter();
      }
      @Override
      public Adapter caseAlarmEvent(AlarmEvent object)
      {
        return createAlarmEventAdapter();
      }
      @Override
      public Adapter caseNormalEvent(NormalEvent object)
      {
        return createNormalEventAdapter();
      }
      @Override
      public Adapter caseContinueEvent(ContinueEvent object)
      {
        return createContinueEventAdapter();
      }
      @Override
      public Adapter caseVarOrQactor(VarOrQactor object)
      {
        return createVarOrQactorAdapter();
      }
      @Override
      public Adapter caseVarOrAtomic(VarOrAtomic object)
      {
        return createVarOrAtomicAdapter();
      }
      @Override
      public Adapter caseVarOrString(VarOrString object)
      {
        return createVarOrStringAdapter();
      }
      @Override
      public Adapter caseVarOrPStruct(VarOrPStruct object)
      {
        return createVarOrPStructAdapter();
      }
      @Override
      public Adapter caseVarOrPhead(VarOrPhead object)
      {
        return createVarOrPheadAdapter();
      }
      @Override
      public Adapter caseVarOrAtomOrPStruct(VarOrAtomOrPStruct object)
      {
        return createVarOrAtomOrPStructAdapter();
      }
      @Override
      public Adapter caseVariable(Variable object)
      {
        return createVariableAdapter();
      }
      @Override
      public Adapter caseTimeLimit(TimeLimit object)
      {
        return createTimeLimitAdapter();
      }
      @Override
      public Adapter caseComponentIP(ComponentIP object)
      {
        return createComponentIPAdapter();
      }
      @Override
      public Adapter caseMoveFile(MoveFile object)
      {
        return createMoveFileAdapter();
      }
      @Override
      public Adapter defaultCase(EObject object)
      {
        return createEObjectAdapter();
      }
    };

  /**
   * Creates an adapter for the <code>target</code>.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param target the object to adapt.
   * @return the adapter for the <code>target</code>.
   * @generated
   */
  @Override
  public Adapter createAdapter(Notifier target)
  {
    return modelSwitch.doSwitch((EObject)target);
  }


  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.QActorSystem <em>QActor System</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.QActorSystem
   * @generated
   */
  public Adapter createQActorSystemAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.QActorSystemSpec <em>QActor System Spec</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.QActorSystemSpec
   * @generated
   */
  public Adapter createQActorSystemSpecAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.Robot <em>Robot</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.Robot
   * @generated
   */
  public Adapter createRobotAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.Message <em>Message</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.Message
   * @generated
   */
  public Adapter createMessageAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.OutOnlyMessage <em>Out Only Message</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.OutOnlyMessage
   * @generated
   */
  public Adapter createOutOnlyMessageAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.OutInMessage <em>Out In Message</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.OutInMessage
   * @generated
   */
  public Adapter createOutInMessageAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.Event <em>Event</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.Event
   * @generated
   */
  public Adapter createEventAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.Signal <em>Signal</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.Signal
   * @generated
   */
  public Adapter createSignalAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.Token <em>Token</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.Token
   * @generated
   */
  public Adapter createTokenAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.Dispatch <em>Dispatch</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.Dispatch
   * @generated
   */
  public Adapter createDispatchAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.Request <em>Request</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.Request
   * @generated
   */
  public Adapter createRequestAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.Invitation <em>Invitation</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.Invitation
   * @generated
   */
  public Adapter createInvitationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.Context <em>Context</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.Context
   * @generated
   */
  public Adapter createContextAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.QActor <em>QActor</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.QActor
   * @generated
   */
  public Adapter createQActorAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.Rule <em>Rule</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.Rule
   * @generated
   */
  public Adapter createRuleAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.PHead <em>PHead</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.PHead
   * @generated
   */
  public Adapter createPHeadAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.PTerm <em>PTerm</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.PTerm
   * @generated
   */
  public Adapter createPTermAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.PAtom <em>PAtom</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.PAtom
   * @generated
   */
  public Adapter createPAtomAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.PAtomString <em>PAtom String</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.PAtomString
   * @generated
   */
  public Adapter createPAtomStringAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.PAtomic <em>PAtomic</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.PAtomic
   * @generated
   */
  public Adapter createPAtomicAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.PAtomNum <em>PAtom Num</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.PAtomNum
   * @generated
   */
  public Adapter createPAtomNumAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.PStruct <em>PStruct</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.PStruct
   * @generated
   */
  public Adapter createPStructAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.PActorCall <em>PActor Call</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.PActorCall
   * @generated
   */
  public Adapter createPActorCallAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.PPredef <em>PPredef</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.PPredef
   * @generated
   */
  public Adapter createPPredefAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.PIs <em>PIs</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.PIs
   * @generated
   */
  public Adapter createPIsAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.PAtomCut <em>PAtom Cut</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.PAtomCut
   * @generated
   */
  public Adapter createPAtomCutAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.Data <em>Data</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.Data
   * @generated
   */
  public Adapter createDataAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.IntegerData <em>Integer Data</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.IntegerData
   * @generated
   */
  public Adapter createIntegerDataAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.StringData <em>String Data</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.StringData
   * @generated
   */
  public Adapter createStringDataAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.Action <em>Action</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.Action
   * @generated
   */
  public Adapter createActionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.Plan <em>Plan</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.Plan
   * @generated
   */
  public Adapter createPlanAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.PlanAction <em>Plan Action</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.PlanAction
   * @generated
   */
  public Adapter createPlanActionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.Guard <em>Guard</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.Guard
   * @generated
   */
  public Adapter createGuardAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.GuardPredicate <em>Guard Predicate</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.GuardPredicate
   * @generated
   */
  public Adapter createGuardPredicateAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.GuardPredicateRemovable <em>Guard Predicate Removable</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.GuardPredicateRemovable
   * @generated
   */
  public Adapter createGuardPredicateRemovableAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.GuardPredicateStable <em>Guard Predicate Stable</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.GuardPredicateStable
   * @generated
   */
  public Adapter createGuardPredicateStableAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.Move <em>Move</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.Move
   * @generated
   */
  public Adapter createMoveAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.ActionMove <em>Action Move</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.ActionMove
   * @generated
   */
  public Adapter createActionMoveAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.ExecuteAction <em>Execute Action</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.ExecuteAction
   * @generated
   */
  public Adapter createExecuteActionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.SolveGoal <em>Solve Goal</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.SolveGoal
   * @generated
   */
  public Adapter createSolveGoalAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.Demo <em>Demo</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.Demo
   * @generated
   */
  public Adapter createDemoAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.ActorOp <em>Actor Op</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.ActorOp
   * @generated
   */
  public Adapter createActorOpAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.BasicRobotMove <em>Basic Robot Move</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.BasicRobotMove
   * @generated
   */
  public Adapter createBasicRobotMoveAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.BasicMove <em>Basic Move</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.BasicMove
   * @generated
   */
  public Adapter createBasicMoveAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.Print <em>Print</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.Print
   * @generated
   */
  public Adapter createPrintAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.PrintCurrentEvent <em>Print Current Event</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.PrintCurrentEvent
   * @generated
   */
  public Adapter createPrintCurrentEventAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.PrintCurrentMessage <em>Print Current Message</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.PrintCurrentMessage
   * @generated
   */
  public Adapter createPrintCurrentMessageAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.MemoCurrentEvent <em>Memo Current Event</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.MemoCurrentEvent
   * @generated
   */
  public Adapter createMemoCurrentEventAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.MemoCurrentMessage <em>Memo Current Message</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.MemoCurrentMessage
   * @generated
   */
  public Adapter createMemoCurrentMessageAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.PlanMove <em>Plan Move</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.PlanMove
   * @generated
   */
  public Adapter createPlanMoveAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.GetActivationEvent <em>Get Activation Event</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.GetActivationEvent
   * @generated
   */
  public Adapter createGetActivationEventAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.GetSensedEvent <em>Get Sensed Event</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.GetSensedEvent
   * @generated
   */
  public Adapter createGetSensedEventAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.LoadPlan <em>Load Plan</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.LoadPlan
   * @generated
   */
  public Adapter createLoadPlanAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.RunPlan <em>Run Plan</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.RunPlan
   * @generated
   */
  public Adapter createRunPlanAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.ResumePlan <em>Resume Plan</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.ResumePlan
   * @generated
   */
  public Adapter createResumePlanAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.SuspendPlan <em>Suspend Plan</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.SuspendPlan
   * @generated
   */
  public Adapter createSuspendPlanAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.RepeatPlan <em>Repeat Plan</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.RepeatPlan
   * @generated
   */
  public Adapter createRepeatPlanAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.SwitchPlan <em>Switch Plan</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.SwitchPlan
   * @generated
   */
  public Adapter createSwitchPlanAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.EndPlan <em>End Plan</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.EndPlan
   * @generated
   */
  public Adapter createEndPlanAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.EndActor <em>End Actor</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.EndActor
   * @generated
   */
  public Adapter createEndActorAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.GuardMove <em>Guard Move</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.GuardMove
   * @generated
   */
  public Adapter createGuardMoveAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.AddRule <em>Add Rule</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.AddRule
   * @generated
   */
  public Adapter createAddRuleAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.RemoveRule <em>Remove Rule</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.RemoveRule
   * @generated
   */
  public Adapter createRemoveRuleAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.MessageMove <em>Message Move</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.MessageMove
   * @generated
   */
  public Adapter createMessageMoveAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.SendDispatch <em>Send Dispatch</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.SendDispatch
   * @generated
   */
  public Adapter createSendDispatchAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.SendRequest <em>Send Request</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.SendRequest
   * @generated
   */
  public Adapter createSendRequestAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.ReplyToCaller <em>Reply To Caller</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.ReplyToCaller
   * @generated
   */
  public Adapter createReplyToCallerAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.ReceiveMsg <em>Receive Msg</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.ReceiveMsg
   * @generated
   */
  public Adapter createReceiveMsgAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.MsgSpec <em>Msg Spec</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.MsgSpec
   * @generated
   */
  public Adapter createMsgSpecAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.OnReceiveMsg <em>On Receive Msg</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.OnReceiveMsg
   * @generated
   */
  public Adapter createOnReceiveMsgAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.MsgSelect <em>Msg Select</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.MsgSelect
   * @generated
   */
  public Adapter createMsgSelectAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.RaiseEvent <em>Raise Event</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.RaiseEvent
   * @generated
   */
  public Adapter createRaiseEventAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.SenseEvent <em>Sense Event</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.SenseEvent
   * @generated
   */
  public Adapter createSenseEventAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.MsgSwitch <em>Msg Switch</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.MsgSwitch
   * @generated
   */
  public Adapter createMsgSwitchAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.EventSwitch <em>Event Switch</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.EventSwitch
   * @generated
   */
  public Adapter createEventSwitchAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.Continuation <em>Continuation</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.Continuation
   * @generated
   */
  public Adapter createContinuationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.ExtensionMove <em>Extension Move</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.ExtensionMove
   * @generated
   */
  public Adapter createExtensionMoveAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.Photo <em>Photo</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.Photo
   * @generated
   */
  public Adapter createPhotoAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.Sound <em>Sound</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.Sound
   * @generated
   */
  public Adapter createSoundAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.Video <em>Video</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.Video
   * @generated
   */
  public Adapter createVideoAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.Delay <em>Delay</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.Delay
   * @generated
   */
  public Adapter createDelayAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.AnswerEvent <em>Answer Event</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.AnswerEvent
   * @generated
   */
  public Adapter createAnswerEventAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.EventHandler <em>Event Handler</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.EventHandler
   * @generated
   */
  public Adapter createEventHandlerAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.EventHandlerBody <em>Event Handler Body</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.EventHandlerBody
   * @generated
   */
  public Adapter createEventHandlerBodyAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.EventHandlerOperation <em>Event Handler Operation</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.EventHandlerOperation
   * @generated
   */
  public Adapter createEventHandlerOperationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.MemoOperation <em>Memo Operation</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.MemoOperation
   * @generated
   */
  public Adapter createMemoOperationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.SolveOperation <em>Solve Operation</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.SolveOperation
   * @generated
   */
  public Adapter createSolveOperationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.SendEventAsDispatch <em>Send Event As Dispatch</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.SendEventAsDispatch
   * @generated
   */
  public Adapter createSendEventAsDispatchAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.MemoRule <em>Memo Rule</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.MemoRule
   * @generated
   */
  public Adapter createMemoRuleAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.MemoEvent <em>Memo Event</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.MemoEvent
   * @generated
   */
  public Adapter createMemoEventAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.Reaction <em>Reaction</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.Reaction
   * @generated
   */
  public Adapter createReactionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.AlarmEvent <em>Alarm Event</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.AlarmEvent
   * @generated
   */
  public Adapter createAlarmEventAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.NormalEvent <em>Normal Event</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.NormalEvent
   * @generated
   */
  public Adapter createNormalEventAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.ContinueEvent <em>Continue Event</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.ContinueEvent
   * @generated
   */
  public Adapter createContinueEventAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.VarOrQactor <em>Var Or Qactor</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.VarOrQactor
   * @generated
   */
  public Adapter createVarOrQactorAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.VarOrAtomic <em>Var Or Atomic</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.VarOrAtomic
   * @generated
   */
  public Adapter createVarOrAtomicAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.VarOrString <em>Var Or String</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.VarOrString
   * @generated
   */
  public Adapter createVarOrStringAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.VarOrPStruct <em>Var Or PStruct</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.VarOrPStruct
   * @generated
   */
  public Adapter createVarOrPStructAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.VarOrPhead <em>Var Or Phead</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.VarOrPhead
   * @generated
   */
  public Adapter createVarOrPheadAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.VarOrAtomOrPStruct <em>Var Or Atom Or PStruct</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.VarOrAtomOrPStruct
   * @generated
   */
  public Adapter createVarOrAtomOrPStructAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.Variable <em>Variable</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.Variable
   * @generated
   */
  public Adapter createVariableAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.TimeLimit <em>Time Limit</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.TimeLimit
   * @generated
   */
  public Adapter createTimeLimitAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.ComponentIP <em>Component IP</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.ComponentIP
   * @generated
   */
  public Adapter createComponentIPAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link it.unibo.xtext.qactor.MoveFile <em>Move File</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see it.unibo.xtext.qactor.MoveFile
   * @generated
   */
  public Adapter createMoveFileAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for the default case.
   * <!-- begin-user-doc -->
   * This default implementation returns null.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @generated
   */
  public Adapter createEObjectAdapter()
  {
    return null;
  }

} //QactorAdapterFactory
