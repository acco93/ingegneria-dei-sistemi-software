/**
 */
package it.unibo.xtext.qactor;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Execute Action</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link it.unibo.xtext.qactor.ExecuteAction#getAction <em>Action</em>}</li>
 *   <li>{@link it.unibo.xtext.qactor.ExecuteAction#getArg <em>Arg</em>}</li>
 *   <li>{@link it.unibo.xtext.qactor.ExecuteAction#getSentence <em>Sentence</em>}</li>
 * </ul>
 * </p>
 *
 * @see it.unibo.xtext.qactor.QactorPackage#getExecuteAction()
 * @model
 * @generated
 */
public interface ExecuteAction extends ActionMove
{
  /**
   * Returns the value of the '<em><b>Action</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Action</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Action</em>' reference.
   * @see #setAction(Action)
   * @see it.unibo.xtext.qactor.QactorPackage#getExecuteAction_Action()
   * @model
   * @generated
   */
  Action getAction();

  /**
   * Sets the value of the '{@link it.unibo.xtext.qactor.ExecuteAction#getAction <em>Action</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Action</em>' reference.
   * @see #getAction()
   * @generated
   */
  void setAction(Action value);

  /**
   * Returns the value of the '<em><b>Arg</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Arg</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Arg</em>' containment reference.
   * @see #setArg(PHead)
   * @see it.unibo.xtext.qactor.QactorPackage#getExecuteAction_Arg()
   * @model containment="true"
   * @generated
   */
  PHead getArg();

  /**
   * Sets the value of the '{@link it.unibo.xtext.qactor.ExecuteAction#getArg <em>Arg</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Arg</em>' containment reference.
   * @see #getArg()
   * @generated
   */
  void setArg(PHead value);

  /**
   * Returns the value of the '<em><b>Sentence</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Sentence</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Sentence</em>' containment reference.
   * @see #setSentence(PHead)
   * @see it.unibo.xtext.qactor.QactorPackage#getExecuteAction_Sentence()
   * @model containment="true"
   * @generated
   */
  PHead getSentence();

  /**
   * Sets the value of the '{@link it.unibo.xtext.qactor.ExecuteAction#getSentence <em>Sentence</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Sentence</em>' containment reference.
   * @see #getSentence()
   * @generated
   */
  void setSentence(PHead value);

} // ExecuteAction
