/**
 */
package it.unibo.xtext.qactor;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Guard Predicate Removable</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see it.unibo.xtext.qactor.QactorPackage#getGuardPredicateRemovable()
 * @model
 * @generated
 */
public interface GuardPredicateRemovable extends GuardPredicate
{
} // GuardPredicateRemovable
