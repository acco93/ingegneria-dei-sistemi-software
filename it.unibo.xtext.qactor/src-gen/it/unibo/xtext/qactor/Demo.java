/**
 */
package it.unibo.xtext.qactor;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Demo</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link it.unibo.xtext.qactor.Demo#getName <em>Name</em>}</li>
 *   <li>{@link it.unibo.xtext.qactor.Demo#getGoal <em>Goal</em>}</li>
 *   <li>{@link it.unibo.xtext.qactor.Demo#getPlan <em>Plan</em>}</li>
 * </ul>
 * </p>
 *
 * @see it.unibo.xtext.qactor.QactorPackage#getDemo()
 * @model
 * @generated
 */
public interface Demo extends ActionMove
{
  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see it.unibo.xtext.qactor.QactorPackage#getDemo_Name()
   * @model
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link it.unibo.xtext.qactor.Demo#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

  /**
   * Returns the value of the '<em><b>Goal</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Goal</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Goal</em>' containment reference.
   * @see #setGoal(PHead)
   * @see it.unibo.xtext.qactor.QactorPackage#getDemo_Goal()
   * @model containment="true"
   * @generated
   */
  PHead getGoal();

  /**
   * Sets the value of the '{@link it.unibo.xtext.qactor.Demo#getGoal <em>Goal</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Goal</em>' containment reference.
   * @see #getGoal()
   * @generated
   */
  void setGoal(PHead value);

  /**
   * Returns the value of the '<em><b>Plan</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Plan</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Plan</em>' reference.
   * @see #setPlan(Plan)
   * @see it.unibo.xtext.qactor.QactorPackage#getDemo_Plan()
   * @model
   * @generated
   */
  Plan getPlan();

  /**
   * Sets the value of the '{@link it.unibo.xtext.qactor.Demo#getPlan <em>Plan</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Plan</em>' reference.
   * @see #getPlan()
   * @generated
   */
  void setPlan(Plan value);

} // Demo
