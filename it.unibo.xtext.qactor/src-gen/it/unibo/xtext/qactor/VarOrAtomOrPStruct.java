/**
 */
package it.unibo.xtext.qactor;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Var Or Atom Or PStruct</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link it.unibo.xtext.qactor.VarOrAtomOrPStruct#getVar <em>Var</em>}</li>
 *   <li>{@link it.unibo.xtext.qactor.VarOrAtomOrPStruct#getPsrtuct <em>Psrtuct</em>}</li>
 *   <li>{@link it.unibo.xtext.qactor.VarOrAtomOrPStruct#getAtom <em>Atom</em>}</li>
 * </ul>
 * </p>
 *
 * @see it.unibo.xtext.qactor.QactorPackage#getVarOrAtomOrPStruct()
 * @model
 * @generated
 */
public interface VarOrAtomOrPStruct extends EObject
{
  /**
   * Returns the value of the '<em><b>Var</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Var</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Var</em>' containment reference.
   * @see #setVar(Variable)
   * @see it.unibo.xtext.qactor.QactorPackage#getVarOrAtomOrPStruct_Var()
   * @model containment="true"
   * @generated
   */
  Variable getVar();

  /**
   * Sets the value of the '{@link it.unibo.xtext.qactor.VarOrAtomOrPStruct#getVar <em>Var</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Var</em>' containment reference.
   * @see #getVar()
   * @generated
   */
  void setVar(Variable value);

  /**
   * Returns the value of the '<em><b>Psrtuct</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Psrtuct</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Psrtuct</em>' containment reference.
   * @see #setPsrtuct(PStruct)
   * @see it.unibo.xtext.qactor.QactorPackage#getVarOrAtomOrPStruct_Psrtuct()
   * @model containment="true"
   * @generated
   */
  PStruct getPsrtuct();

  /**
   * Sets the value of the '{@link it.unibo.xtext.qactor.VarOrAtomOrPStruct#getPsrtuct <em>Psrtuct</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Psrtuct</em>' containment reference.
   * @see #getPsrtuct()
   * @generated
   */
  void setPsrtuct(PStruct value);

  /**
   * Returns the value of the '<em><b>Atom</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Atom</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Atom</em>' containment reference.
   * @see #setAtom(PAtomic)
   * @see it.unibo.xtext.qactor.QactorPackage#getVarOrAtomOrPStruct_Atom()
   * @model containment="true"
   * @generated
   */
  PAtomic getAtom();

  /**
   * Sets the value of the '{@link it.unibo.xtext.qactor.VarOrAtomOrPStruct#getAtom <em>Atom</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Atom</em>' containment reference.
   * @see #getAtom()
   * @generated
   */
  void setAtom(PAtomic value);

} // VarOrAtomOrPStruct
