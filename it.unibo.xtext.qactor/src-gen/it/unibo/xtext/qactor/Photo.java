/**
 */
package it.unibo.xtext.qactor;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Photo</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link it.unibo.xtext.qactor.Photo#getDestfile <em>Destfile</em>}</li>
 *   <li>{@link it.unibo.xtext.qactor.Photo#getAnswerEvent <em>Answer Event</em>}</li>
 * </ul>
 * </p>
 *
 * @see it.unibo.xtext.qactor.QactorPackage#getPhoto()
 * @model
 * @generated
 */
public interface Photo extends ExtensionMove
{
  /**
   * Returns the value of the '<em><b>Destfile</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Destfile</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Destfile</em>' containment reference.
   * @see #setDestfile(MoveFile)
   * @see it.unibo.xtext.qactor.QactorPackage#getPhoto_Destfile()
   * @model containment="true"
   * @generated
   */
  MoveFile getDestfile();

  /**
   * Sets the value of the '{@link it.unibo.xtext.qactor.Photo#getDestfile <em>Destfile</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Destfile</em>' containment reference.
   * @see #getDestfile()
   * @generated
   */
  void setDestfile(MoveFile value);

  /**
   * Returns the value of the '<em><b>Answer Event</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Answer Event</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Answer Event</em>' containment reference.
   * @see #setAnswerEvent(AnswerEvent)
   * @see it.unibo.xtext.qactor.QactorPackage#getPhoto_AnswerEvent()
   * @model containment="true"
   * @generated
   */
  AnswerEvent getAnswerEvent();

  /**
   * Sets the value of the '{@link it.unibo.xtext.qactor.Photo#getAnswerEvent <em>Answer Event</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Answer Event</em>' containment reference.
   * @see #getAnswerEvent()
   * @generated
   */
  void setAnswerEvent(AnswerEvent value);

} // Photo
