/**
 */
package it.unibo.xtext.qactor;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Memo Current Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link it.unibo.xtext.qactor.MemoCurrentEvent#isLastonly <em>Lastonly</em>}</li>
 * </ul>
 * </p>
 *
 * @see it.unibo.xtext.qactor.QactorPackage#getMemoCurrentEvent()
 * @model
 * @generated
 */
public interface MemoCurrentEvent extends BasicMove
{
  /**
   * Returns the value of the '<em><b>Lastonly</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Lastonly</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Lastonly</em>' attribute.
   * @see #setLastonly(boolean)
   * @see it.unibo.xtext.qactor.QactorPackage#getMemoCurrentEvent_Lastonly()
   * @model
   * @generated
   */
  boolean isLastonly();

  /**
   * Sets the value of the '{@link it.unibo.xtext.qactor.MemoCurrentEvent#isLastonly <em>Lastonly</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Lastonly</em>' attribute.
   * @see #isLastonly()
   * @generated
   */
  void setLastonly(boolean value);

} // MemoCurrentEvent
