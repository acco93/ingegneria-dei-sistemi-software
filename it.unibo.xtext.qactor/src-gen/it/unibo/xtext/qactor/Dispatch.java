/**
 */
package it.unibo.xtext.qactor;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dispatch</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see it.unibo.xtext.qactor.QactorPackage#getDispatch()
 * @model
 * @generated
 */
public interface Dispatch extends OutOnlyMessage
{
} // Dispatch
