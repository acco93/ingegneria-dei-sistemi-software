/**
 */
package it.unibo.xtext.qactor;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Repeat Plan</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link it.unibo.xtext.qactor.RepeatPlan#getNiter <em>Niter</em>}</li>
 * </ul>
 * </p>
 *
 * @see it.unibo.xtext.qactor.QactorPackage#getRepeatPlan()
 * @model
 * @generated
 */
public interface RepeatPlan extends PlanMove
{
  /**
   * Returns the value of the '<em><b>Niter</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Niter</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Niter</em>' attribute.
   * @see #setNiter(int)
   * @see it.unibo.xtext.qactor.QactorPackage#getRepeatPlan_Niter()
   * @model
   * @generated
   */
  int getNiter();

  /**
   * Sets the value of the '{@link it.unibo.xtext.qactor.RepeatPlan#getNiter <em>Niter</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Niter</em>' attribute.
   * @see #getNiter()
   * @generated
   */
  void setNiter(int value);

} // RepeatPlan
