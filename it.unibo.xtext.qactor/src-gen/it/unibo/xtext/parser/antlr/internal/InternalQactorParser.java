package it.unibo.xtext.parser.antlr.internal; 

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import it.unibo.xtext.services.QactorGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalQactorParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_INT", "RULE_VARID", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'System'", "'-testing'", "'TDDO'", "'Event'", "':'", "'Signal'", "'Token'", "'Dispatch'", "'Request'", "'Invitation'", "'Context'", "'ip'", "'-g'", "'-standalone '", "'-httpserver'", "'QActor'", "'context'", "'{'", "'Rules'", "'}'", "':-'", "','", "'.'", "'('", "')'", "'Actor'", "'<-'", "'is'", "'+'", "'!'", "'int'", "'='", "'String'", "'Action'", "'undoable'", "'maxtime'", "'arg'", "';'", "'Plan'", "'normal'", "'resumeLastPlan'", "'else'", "'['", "'not'", "']'", "'??'", "'!?'", "'execute'", "'with'", "'dosentence'", "'solve'", "'onFailSwitchTo'", "'demo'", "'actorOp'", "'dummyRobotMove'", "'println'", "'printCurrentEvent'", "'-memo'", "'printCurrentMessage'", "'memoCurrentEvent'", "'-lastonly'", "'memoCurrentMessage'", "'getActivationEvent '", "'getSensedEvent '", "'loadPlan'", "'runPlan'", "'suspendLastPlan'", "'repeatPlan'", "'switchToPlan'", "'endPlan'", "'endQActor'", "'addRule'", "'removeRule'", "'forward'", "'-m'", "'demand'", "'answHandle'", "'replyToCaller'", "'receiveMsg'", "'sender'", "'content'", "'receiveTheMsg'", "'m'", "'receiveAndSwitch'", "'->'", "'emit'", "'sense'", "'onMsg'", "'onEvent'", "'continue'", "'photo'", "'sound'", "'video'", "'delay'", "'answerEv'", "'EventHandler'", "'for'", "'-print'", "'memo'", "'forwardEvent'", "'currentEvent'", "'react'", "'or'", "'event'", "'when'", "'$'", "'time'", "'host='", "'port='", "'file'", "'white'", "'gray'", "'blue'", "'green'", "'yellow'", "'cyan'"
    };
    public static final int T__50=50;
    public static final int T__59=59;
    public static final int T__55=55;
    public static final int T__56=56;
    public static final int T__57=57;
    public static final int T__58=58;
    public static final int T__51=51;
    public static final int T__52=52;
    public static final int T__53=53;
    public static final int T__54=54;
    public static final int T__60=60;
    public static final int T__61=61;
    public static final int RULE_ID=4;
    public static final int RULE_INT=6;
    public static final int T__66=66;
    public static final int RULE_ML_COMMENT=8;
    public static final int T__67=67;
    public static final int T__68=68;
    public static final int T__69=69;
    public static final int T__62=62;
    public static final int T__126=126;
    public static final int T__63=63;
    public static final int T__125=125;
    public static final int T__64=64;
    public static final int T__65=65;
    public static final int T__127=127;
    public static final int RULE_VARID=7;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;
    public static final int T__91=91;
    public static final int T__100=100;
    public static final int T__92=92;
    public static final int T__93=93;
    public static final int T__102=102;
    public static final int T__94=94;
    public static final int T__101=101;
    public static final int T__90=90;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__99=99;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int T__95=95;
    public static final int T__96=96;
    public static final int T__97=97;
    public static final int T__98=98;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int T__122=122;
    public static final int T__70=70;
    public static final int T__121=121;
    public static final int T__71=71;
    public static final int T__124=124;
    public static final int T__72=72;
    public static final int T__123=123;
    public static final int T__120=120;
    public static final int RULE_STRING=5;
    public static final int RULE_SL_COMMENT=9;
    public static final int T__77=77;
    public static final int T__119=119;
    public static final int T__78=78;
    public static final int T__118=118;
    public static final int T__79=79;
    public static final int T__73=73;
    public static final int T__115=115;
    public static final int EOF=-1;
    public static final int T__74=74;
    public static final int T__114=114;
    public static final int T__75=75;
    public static final int T__117=117;
    public static final int T__76=76;
    public static final int T__116=116;
    public static final int T__80=80;
    public static final int T__111=111;
    public static final int T__81=81;
    public static final int T__110=110;
    public static final int T__82=82;
    public static final int T__113=113;
    public static final int T__83=83;
    public static final int T__112=112;
    public static final int RULE_WS=10;
    public static final int RULE_ANY_OTHER=11;
    public static final int T__88=88;
    public static final int T__108=108;
    public static final int T__89=89;
    public static final int T__107=107;
    public static final int T__109=109;
    public static final int T__84=84;
    public static final int T__104=104;
    public static final int T__85=85;
    public static final int T__103=103;
    public static final int T__86=86;
    public static final int T__106=106;
    public static final int T__87=87;
    public static final int T__105=105;

    // delegates
    // delegators


        public InternalQactorParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalQactorParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalQactorParser.tokenNames; }
    public String getGrammarFileName() { return "../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g"; }



     	private QactorGrammarAccess grammarAccess;
     	
        public InternalQactorParser(TokenStream input, QactorGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }
        
        @Override
        protected String getFirstRuleName() {
        	return "QActorSystem";	
       	}
       	
       	@Override
       	protected QactorGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}



    // $ANTLR start "entryRuleQActorSystem"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:68:1: entryRuleQActorSystem returns [EObject current=null] : iv_ruleQActorSystem= ruleQActorSystem EOF ;
    public final EObject entryRuleQActorSystem() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleQActorSystem = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:69:2: (iv_ruleQActorSystem= ruleQActorSystem EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:70:2: iv_ruleQActorSystem= ruleQActorSystem EOF
            {
             newCompositeNode(grammarAccess.getQActorSystemRule()); 
            pushFollow(FOLLOW_ruleQActorSystem_in_entryRuleQActorSystem75);
            iv_ruleQActorSystem=ruleQActorSystem();

            state._fsp--;

             current =iv_ruleQActorSystem; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleQActorSystem85); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQActorSystem"


    // $ANTLR start "ruleQActorSystem"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:77:1: ruleQActorSystem returns [EObject current=null] : (otherlv_0= 'System' ( (lv_spec_1_0= ruleQActorSystemSpec ) ) ) ;
    public final EObject ruleQActorSystem() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_spec_1_0 = null;


         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:80:28: ( (otherlv_0= 'System' ( (lv_spec_1_0= ruleQActorSystemSpec ) ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:81:1: (otherlv_0= 'System' ( (lv_spec_1_0= ruleQActorSystemSpec ) ) )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:81:1: (otherlv_0= 'System' ( (lv_spec_1_0= ruleQActorSystemSpec ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:81:3: otherlv_0= 'System' ( (lv_spec_1_0= ruleQActorSystemSpec ) )
            {
            otherlv_0=(Token)match(input,12,FOLLOW_12_in_ruleQActorSystem122); 

                	newLeafNode(otherlv_0, grammarAccess.getQActorSystemAccess().getSystemKeyword_0());
                
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:85:1: ( (lv_spec_1_0= ruleQActorSystemSpec ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:86:1: (lv_spec_1_0= ruleQActorSystemSpec )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:86:1: (lv_spec_1_0= ruleQActorSystemSpec )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:87:3: lv_spec_1_0= ruleQActorSystemSpec
            {
             
            	        newCompositeNode(grammarAccess.getQActorSystemAccess().getSpecQActorSystemSpecParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_ruleQActorSystemSpec_in_ruleQActorSystem143);
            lv_spec_1_0=ruleQActorSystemSpec();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getQActorSystemRule());
            	        }
                   		set(
                   			current, 
                   			"spec",
                    		lv_spec_1_0, 
                    		"QActorSystemSpec");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQActorSystem"


    // $ANTLR start "entryRuleQActorSystemSpec"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:111:1: entryRuleQActorSystemSpec returns [EObject current=null] : iv_ruleQActorSystemSpec= ruleQActorSystemSpec EOF ;
    public final EObject entryRuleQActorSystemSpec() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleQActorSystemSpec = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:112:2: (iv_ruleQActorSystemSpec= ruleQActorSystemSpec EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:113:2: iv_ruleQActorSystemSpec= ruleQActorSystemSpec EOF
            {
             newCompositeNode(grammarAccess.getQActorSystemSpecRule()); 
            pushFollow(FOLLOW_ruleQActorSystemSpec_in_entryRuleQActorSystemSpec179);
            iv_ruleQActorSystemSpec=ruleQActorSystemSpec();

            state._fsp--;

             current =iv_ruleQActorSystemSpec; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleQActorSystemSpec189); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQActorSystemSpec"


    // $ANTLR start "ruleQActorSystemSpec"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:120:1: ruleQActorSystemSpec returns [EObject current=null] : ( ( (lv_name_0_0= RULE_ID ) ) ( (lv_testing_1_0= '-testing' ) )? ( (lv_message_2_0= ruleMessage ) )* ( (lv_context_3_0= ruleContext ) )* ( (lv_actor_4_0= ruleQActor ) )* ( (lv_robot_5_0= ruleRobot ) )? ) ;
    public final EObject ruleQActorSystemSpec() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token lv_testing_1_0=null;
        EObject lv_message_2_0 = null;

        EObject lv_context_3_0 = null;

        EObject lv_actor_4_0 = null;

        EObject lv_robot_5_0 = null;


         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:123:28: ( ( ( (lv_name_0_0= RULE_ID ) ) ( (lv_testing_1_0= '-testing' ) )? ( (lv_message_2_0= ruleMessage ) )* ( (lv_context_3_0= ruleContext ) )* ( (lv_actor_4_0= ruleQActor ) )* ( (lv_robot_5_0= ruleRobot ) )? ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:124:1: ( ( (lv_name_0_0= RULE_ID ) ) ( (lv_testing_1_0= '-testing' ) )? ( (lv_message_2_0= ruleMessage ) )* ( (lv_context_3_0= ruleContext ) )* ( (lv_actor_4_0= ruleQActor ) )* ( (lv_robot_5_0= ruleRobot ) )? )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:124:1: ( ( (lv_name_0_0= RULE_ID ) ) ( (lv_testing_1_0= '-testing' ) )? ( (lv_message_2_0= ruleMessage ) )* ( (lv_context_3_0= ruleContext ) )* ( (lv_actor_4_0= ruleQActor ) )* ( (lv_robot_5_0= ruleRobot ) )? )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:124:2: ( (lv_name_0_0= RULE_ID ) ) ( (lv_testing_1_0= '-testing' ) )? ( (lv_message_2_0= ruleMessage ) )* ( (lv_context_3_0= ruleContext ) )* ( (lv_actor_4_0= ruleQActor ) )* ( (lv_robot_5_0= ruleRobot ) )?
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:124:2: ( (lv_name_0_0= RULE_ID ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:125:1: (lv_name_0_0= RULE_ID )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:125:1: (lv_name_0_0= RULE_ID )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:126:3: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleQActorSystemSpec231); 

            			newLeafNode(lv_name_0_0, grammarAccess.getQActorSystemSpecAccess().getNameIDTerminalRuleCall_0_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getQActorSystemSpecRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_0_0, 
                    		"ID");
            	    

            }


            }

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:142:2: ( (lv_testing_1_0= '-testing' ) )?
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==13) ) {
                alt1=1;
            }
            switch (alt1) {
                case 1 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:143:1: (lv_testing_1_0= '-testing' )
                    {
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:143:1: (lv_testing_1_0= '-testing' )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:144:3: lv_testing_1_0= '-testing'
                    {
                    lv_testing_1_0=(Token)match(input,13,FOLLOW_13_in_ruleQActorSystemSpec254); 

                            newLeafNode(lv_testing_1_0, grammarAccess.getQActorSystemSpecAccess().getTestingTestingKeyword_1_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getQActorSystemSpecRule());
                    	        }
                           		setWithLastConsumed(current, "testing", true, "-testing");
                    	    

                    }


                    }
                    break;

            }

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:157:3: ( (lv_message_2_0= ruleMessage ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==15||(LA2_0>=17 && LA2_0<=21)) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:158:1: (lv_message_2_0= ruleMessage )
            	    {
            	    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:158:1: (lv_message_2_0= ruleMessage )
            	    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:159:3: lv_message_2_0= ruleMessage
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getQActorSystemSpecAccess().getMessageMessageParserRuleCall_2_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleMessage_in_ruleQActorSystemSpec289);
            	    lv_message_2_0=ruleMessage();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getQActorSystemSpecRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"message",
            	            		lv_message_2_0, 
            	            		"Message");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:175:3: ( (lv_context_3_0= ruleContext ) )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==22) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:176:1: (lv_context_3_0= ruleContext )
            	    {
            	    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:176:1: (lv_context_3_0= ruleContext )
            	    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:177:3: lv_context_3_0= ruleContext
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getQActorSystemSpecAccess().getContextContextParserRuleCall_3_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleContext_in_ruleQActorSystemSpec311);
            	    lv_context_3_0=ruleContext();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getQActorSystemSpecRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"context",
            	            		lv_context_3_0, 
            	            		"Context");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:193:3: ( (lv_actor_4_0= ruleQActor ) )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==27) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:194:1: (lv_actor_4_0= ruleQActor )
            	    {
            	    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:194:1: (lv_actor_4_0= ruleQActor )
            	    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:195:3: lv_actor_4_0= ruleQActor
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getQActorSystemSpecAccess().getActorQActorParserRuleCall_4_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleQActor_in_ruleQActorSystemSpec333);
            	    lv_actor_4_0=ruleQActor();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getQActorSystemSpecRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"actor",
            	            		lv_actor_4_0, 
            	            		"QActor");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:211:3: ( (lv_robot_5_0= ruleRobot ) )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==RULE_ID) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:212:1: (lv_robot_5_0= ruleRobot )
                    {
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:212:1: (lv_robot_5_0= ruleRobot )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:213:3: lv_robot_5_0= ruleRobot
                    {
                     
                    	        newCompositeNode(grammarAccess.getQActorSystemSpecAccess().getRobotRobotParserRuleCall_5_0()); 
                    	    
                    pushFollow(FOLLOW_ruleRobot_in_ruleQActorSystemSpec355);
                    lv_robot_5_0=ruleRobot();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getQActorSystemSpecRule());
                    	        }
                           		set(
                           			current, 
                           			"robot",
                            		lv_robot_5_0, 
                            		"Robot");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQActorSystemSpec"


    // $ANTLR start "entryRuleRobot"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:237:1: entryRuleRobot returns [EObject current=null] : iv_ruleRobot= ruleRobot EOF ;
    public final EObject entryRuleRobot() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRobot = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:238:2: (iv_ruleRobot= ruleRobot EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:239:2: iv_ruleRobot= ruleRobot EOF
            {
             newCompositeNode(grammarAccess.getRobotRule()); 
            pushFollow(FOLLOW_ruleRobot_in_entryRuleRobot392);
            iv_ruleRobot=ruleRobot();

            state._fsp--;

             current =iv_ruleRobot; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleRobot402); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRobot"


    // $ANTLR start "ruleRobot"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:246:1: ruleRobot returns [EObject current=null] : ( ( (lv_name_0_0= RULE_ID ) ) ( (lv_actor_1_0= ruleQActor ) ) otherlv_2= 'TDDO' ) ;
    public final EObject ruleRobot() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_2=null;
        EObject lv_actor_1_0 = null;


         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:249:28: ( ( ( (lv_name_0_0= RULE_ID ) ) ( (lv_actor_1_0= ruleQActor ) ) otherlv_2= 'TDDO' ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:250:1: ( ( (lv_name_0_0= RULE_ID ) ) ( (lv_actor_1_0= ruleQActor ) ) otherlv_2= 'TDDO' )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:250:1: ( ( (lv_name_0_0= RULE_ID ) ) ( (lv_actor_1_0= ruleQActor ) ) otherlv_2= 'TDDO' )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:250:2: ( (lv_name_0_0= RULE_ID ) ) ( (lv_actor_1_0= ruleQActor ) ) otherlv_2= 'TDDO'
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:250:2: ( (lv_name_0_0= RULE_ID ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:251:1: (lv_name_0_0= RULE_ID )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:251:1: (lv_name_0_0= RULE_ID )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:252:3: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleRobot444); 

            			newLeafNode(lv_name_0_0, grammarAccess.getRobotAccess().getNameIDTerminalRuleCall_0_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getRobotRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_0_0, 
                    		"ID");
            	    

            }


            }

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:268:2: ( (lv_actor_1_0= ruleQActor ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:269:1: (lv_actor_1_0= ruleQActor )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:269:1: (lv_actor_1_0= ruleQActor )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:270:3: lv_actor_1_0= ruleQActor
            {
             
            	        newCompositeNode(grammarAccess.getRobotAccess().getActorQActorParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_ruleQActor_in_ruleRobot470);
            lv_actor_1_0=ruleQActor();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getRobotRule());
            	        }
                   		set(
                   			current, 
                   			"actor",
                    		lv_actor_1_0, 
                    		"QActor");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_2=(Token)match(input,14,FOLLOW_14_in_ruleRobot482); 

                	newLeafNode(otherlv_2, grammarAccess.getRobotAccess().getTDDOKeyword_2());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRobot"


    // $ANTLR start "entryRuleMessage"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:298:1: entryRuleMessage returns [EObject current=null] : iv_ruleMessage= ruleMessage EOF ;
    public final EObject entryRuleMessage() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMessage = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:299:2: (iv_ruleMessage= ruleMessage EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:300:2: iv_ruleMessage= ruleMessage EOF
            {
             newCompositeNode(grammarAccess.getMessageRule()); 
            pushFollow(FOLLOW_ruleMessage_in_entryRuleMessage518);
            iv_ruleMessage=ruleMessage();

            state._fsp--;

             current =iv_ruleMessage; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleMessage528); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMessage"


    // $ANTLR start "ruleMessage"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:307:1: ruleMessage returns [EObject current=null] : (this_OutOnlyMessage_0= ruleOutOnlyMessage | this_OutInMessage_1= ruleOutInMessage ) ;
    public final EObject ruleMessage() throws RecognitionException {
        EObject current = null;

        EObject this_OutOnlyMessage_0 = null;

        EObject this_OutInMessage_1 = null;


         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:310:28: ( (this_OutOnlyMessage_0= ruleOutOnlyMessage | this_OutInMessage_1= ruleOutInMessage ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:311:1: (this_OutOnlyMessage_0= ruleOutOnlyMessage | this_OutInMessage_1= ruleOutInMessage )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:311:1: (this_OutOnlyMessage_0= ruleOutOnlyMessage | this_OutInMessage_1= ruleOutInMessage )
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==15||(LA6_0>=17 && LA6_0<=19)) ) {
                alt6=1;
            }
            else if ( ((LA6_0>=20 && LA6_0<=21)) ) {
                alt6=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }
            switch (alt6) {
                case 1 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:312:5: this_OutOnlyMessage_0= ruleOutOnlyMessage
                    {
                     
                            newCompositeNode(grammarAccess.getMessageAccess().getOutOnlyMessageParserRuleCall_0()); 
                        
                    pushFollow(FOLLOW_ruleOutOnlyMessage_in_ruleMessage575);
                    this_OutOnlyMessage_0=ruleOutOnlyMessage();

                    state._fsp--;

                     
                            current = this_OutOnlyMessage_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:322:5: this_OutInMessage_1= ruleOutInMessage
                    {
                     
                            newCompositeNode(grammarAccess.getMessageAccess().getOutInMessageParserRuleCall_1()); 
                        
                    pushFollow(FOLLOW_ruleOutInMessage_in_ruleMessage602);
                    this_OutInMessage_1=ruleOutInMessage();

                    state._fsp--;

                     
                            current = this_OutInMessage_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMessage"


    // $ANTLR start "entryRuleOutOnlyMessage"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:338:1: entryRuleOutOnlyMessage returns [EObject current=null] : iv_ruleOutOnlyMessage= ruleOutOnlyMessage EOF ;
    public final EObject entryRuleOutOnlyMessage() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOutOnlyMessage = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:339:2: (iv_ruleOutOnlyMessage= ruleOutOnlyMessage EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:340:2: iv_ruleOutOnlyMessage= ruleOutOnlyMessage EOF
            {
             newCompositeNode(grammarAccess.getOutOnlyMessageRule()); 
            pushFollow(FOLLOW_ruleOutOnlyMessage_in_entryRuleOutOnlyMessage637);
            iv_ruleOutOnlyMessage=ruleOutOnlyMessage();

            state._fsp--;

             current =iv_ruleOutOnlyMessage; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleOutOnlyMessage647); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOutOnlyMessage"


    // $ANTLR start "ruleOutOnlyMessage"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:347:1: ruleOutOnlyMessage returns [EObject current=null] : (this_Dispatch_0= ruleDispatch | this_Event_1= ruleEvent | this_Signal_2= ruleSignal | this_Token_3= ruleToken ) ;
    public final EObject ruleOutOnlyMessage() throws RecognitionException {
        EObject current = null;

        EObject this_Dispatch_0 = null;

        EObject this_Event_1 = null;

        EObject this_Signal_2 = null;

        EObject this_Token_3 = null;


         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:350:28: ( (this_Dispatch_0= ruleDispatch | this_Event_1= ruleEvent | this_Signal_2= ruleSignal | this_Token_3= ruleToken ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:351:1: (this_Dispatch_0= ruleDispatch | this_Event_1= ruleEvent | this_Signal_2= ruleSignal | this_Token_3= ruleToken )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:351:1: (this_Dispatch_0= ruleDispatch | this_Event_1= ruleEvent | this_Signal_2= ruleSignal | this_Token_3= ruleToken )
            int alt7=4;
            switch ( input.LA(1) ) {
            case 19:
                {
                alt7=1;
                }
                break;
            case 15:
                {
                alt7=2;
                }
                break;
            case 17:
                {
                alt7=3;
                }
                break;
            case 18:
                {
                alt7=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;
            }

            switch (alt7) {
                case 1 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:352:5: this_Dispatch_0= ruleDispatch
                    {
                     
                            newCompositeNode(grammarAccess.getOutOnlyMessageAccess().getDispatchParserRuleCall_0()); 
                        
                    pushFollow(FOLLOW_ruleDispatch_in_ruleOutOnlyMessage694);
                    this_Dispatch_0=ruleDispatch();

                    state._fsp--;

                     
                            current = this_Dispatch_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:362:5: this_Event_1= ruleEvent
                    {
                     
                            newCompositeNode(grammarAccess.getOutOnlyMessageAccess().getEventParserRuleCall_1()); 
                        
                    pushFollow(FOLLOW_ruleEvent_in_ruleOutOnlyMessage721);
                    this_Event_1=ruleEvent();

                    state._fsp--;

                     
                            current = this_Event_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 3 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:372:5: this_Signal_2= ruleSignal
                    {
                     
                            newCompositeNode(grammarAccess.getOutOnlyMessageAccess().getSignalParserRuleCall_2()); 
                        
                    pushFollow(FOLLOW_ruleSignal_in_ruleOutOnlyMessage748);
                    this_Signal_2=ruleSignal();

                    state._fsp--;

                     
                            current = this_Signal_2; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 4 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:382:5: this_Token_3= ruleToken
                    {
                     
                            newCompositeNode(grammarAccess.getOutOnlyMessageAccess().getTokenParserRuleCall_3()); 
                        
                    pushFollow(FOLLOW_ruleToken_in_ruleOutOnlyMessage775);
                    this_Token_3=ruleToken();

                    state._fsp--;

                     
                            current = this_Token_3; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOutOnlyMessage"


    // $ANTLR start "entryRuleOutInMessage"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:398:1: entryRuleOutInMessage returns [EObject current=null] : iv_ruleOutInMessage= ruleOutInMessage EOF ;
    public final EObject entryRuleOutInMessage() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOutInMessage = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:399:2: (iv_ruleOutInMessage= ruleOutInMessage EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:400:2: iv_ruleOutInMessage= ruleOutInMessage EOF
            {
             newCompositeNode(grammarAccess.getOutInMessageRule()); 
            pushFollow(FOLLOW_ruleOutInMessage_in_entryRuleOutInMessage810);
            iv_ruleOutInMessage=ruleOutInMessage();

            state._fsp--;

             current =iv_ruleOutInMessage; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleOutInMessage820); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOutInMessage"


    // $ANTLR start "ruleOutInMessage"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:407:1: ruleOutInMessage returns [EObject current=null] : (this_Request_0= ruleRequest | this_Invitation_1= ruleInvitation ) ;
    public final EObject ruleOutInMessage() throws RecognitionException {
        EObject current = null;

        EObject this_Request_0 = null;

        EObject this_Invitation_1 = null;


         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:410:28: ( (this_Request_0= ruleRequest | this_Invitation_1= ruleInvitation ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:411:1: (this_Request_0= ruleRequest | this_Invitation_1= ruleInvitation )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:411:1: (this_Request_0= ruleRequest | this_Invitation_1= ruleInvitation )
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==20) ) {
                alt8=1;
            }
            else if ( (LA8_0==21) ) {
                alt8=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;
            }
            switch (alt8) {
                case 1 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:412:5: this_Request_0= ruleRequest
                    {
                     
                            newCompositeNode(grammarAccess.getOutInMessageAccess().getRequestParserRuleCall_0()); 
                        
                    pushFollow(FOLLOW_ruleRequest_in_ruleOutInMessage867);
                    this_Request_0=ruleRequest();

                    state._fsp--;

                     
                            current = this_Request_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:422:5: this_Invitation_1= ruleInvitation
                    {
                     
                            newCompositeNode(grammarAccess.getOutInMessageAccess().getInvitationParserRuleCall_1()); 
                        
                    pushFollow(FOLLOW_ruleInvitation_in_ruleOutInMessage894);
                    this_Invitation_1=ruleInvitation();

                    state._fsp--;

                     
                            current = this_Invitation_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOutInMessage"


    // $ANTLR start "entryRuleEvent"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:438:1: entryRuleEvent returns [EObject current=null] : iv_ruleEvent= ruleEvent EOF ;
    public final EObject entryRuleEvent() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEvent = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:439:2: (iv_ruleEvent= ruleEvent EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:440:2: iv_ruleEvent= ruleEvent EOF
            {
             newCompositeNode(grammarAccess.getEventRule()); 
            pushFollow(FOLLOW_ruleEvent_in_entryRuleEvent929);
            iv_ruleEvent=ruleEvent();

            state._fsp--;

             current =iv_ruleEvent; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleEvent939); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEvent"


    // $ANTLR start "ruleEvent"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:447:1: ruleEvent returns [EObject current=null] : (otherlv_0= 'Event' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (lv_msg_3_0= rulePHead ) ) ) ;
    public final EObject ruleEvent() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        EObject lv_msg_3_0 = null;


         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:450:28: ( (otherlv_0= 'Event' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (lv_msg_3_0= rulePHead ) ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:451:1: (otherlv_0= 'Event' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (lv_msg_3_0= rulePHead ) ) )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:451:1: (otherlv_0= 'Event' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (lv_msg_3_0= rulePHead ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:451:3: otherlv_0= 'Event' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (lv_msg_3_0= rulePHead ) )
            {
            otherlv_0=(Token)match(input,15,FOLLOW_15_in_ruleEvent976); 

                	newLeafNode(otherlv_0, grammarAccess.getEventAccess().getEventKeyword_0());
                
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:455:1: ( (lv_name_1_0= RULE_ID ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:456:1: (lv_name_1_0= RULE_ID )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:456:1: (lv_name_1_0= RULE_ID )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:457:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleEvent993); 

            			newLeafNode(lv_name_1_0, grammarAccess.getEventAccess().getNameIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getEventRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"ID");
            	    

            }


            }

            otherlv_2=(Token)match(input,16,FOLLOW_16_in_ruleEvent1010); 

                	newLeafNode(otherlv_2, grammarAccess.getEventAccess().getColonKeyword_2());
                
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:477:1: ( (lv_msg_3_0= rulePHead ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:478:1: (lv_msg_3_0= rulePHead )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:478:1: (lv_msg_3_0= rulePHead )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:479:3: lv_msg_3_0= rulePHead
            {
             
            	        newCompositeNode(grammarAccess.getEventAccess().getMsgPHeadParserRuleCall_3_0()); 
            	    
            pushFollow(FOLLOW_rulePHead_in_ruleEvent1031);
            lv_msg_3_0=rulePHead();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getEventRule());
            	        }
                   		set(
                   			current, 
                   			"msg",
                    		lv_msg_3_0, 
                    		"PHead");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEvent"


    // $ANTLR start "entryRuleSignal"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:503:1: entryRuleSignal returns [EObject current=null] : iv_ruleSignal= ruleSignal EOF ;
    public final EObject entryRuleSignal() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSignal = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:504:2: (iv_ruleSignal= ruleSignal EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:505:2: iv_ruleSignal= ruleSignal EOF
            {
             newCompositeNode(grammarAccess.getSignalRule()); 
            pushFollow(FOLLOW_ruleSignal_in_entryRuleSignal1067);
            iv_ruleSignal=ruleSignal();

            state._fsp--;

             current =iv_ruleSignal; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleSignal1077); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSignal"


    // $ANTLR start "ruleSignal"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:512:1: ruleSignal returns [EObject current=null] : (otherlv_0= 'Signal' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (lv_msg_3_0= rulePHead ) ) ) ;
    public final EObject ruleSignal() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        EObject lv_msg_3_0 = null;


         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:515:28: ( (otherlv_0= 'Signal' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (lv_msg_3_0= rulePHead ) ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:516:1: (otherlv_0= 'Signal' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (lv_msg_3_0= rulePHead ) ) )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:516:1: (otherlv_0= 'Signal' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (lv_msg_3_0= rulePHead ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:516:3: otherlv_0= 'Signal' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (lv_msg_3_0= rulePHead ) )
            {
            otherlv_0=(Token)match(input,17,FOLLOW_17_in_ruleSignal1114); 

                	newLeafNode(otherlv_0, grammarAccess.getSignalAccess().getSignalKeyword_0());
                
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:520:1: ( (lv_name_1_0= RULE_ID ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:521:1: (lv_name_1_0= RULE_ID )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:521:1: (lv_name_1_0= RULE_ID )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:522:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleSignal1131); 

            			newLeafNode(lv_name_1_0, grammarAccess.getSignalAccess().getNameIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getSignalRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"ID");
            	    

            }


            }

            otherlv_2=(Token)match(input,16,FOLLOW_16_in_ruleSignal1148); 

                	newLeafNode(otherlv_2, grammarAccess.getSignalAccess().getColonKeyword_2());
                
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:542:1: ( (lv_msg_3_0= rulePHead ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:543:1: (lv_msg_3_0= rulePHead )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:543:1: (lv_msg_3_0= rulePHead )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:544:3: lv_msg_3_0= rulePHead
            {
             
            	        newCompositeNode(grammarAccess.getSignalAccess().getMsgPHeadParserRuleCall_3_0()); 
            	    
            pushFollow(FOLLOW_rulePHead_in_ruleSignal1169);
            lv_msg_3_0=rulePHead();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getSignalRule());
            	        }
                   		set(
                   			current, 
                   			"msg",
                    		lv_msg_3_0, 
                    		"PHead");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSignal"


    // $ANTLR start "entryRuleToken"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:568:1: entryRuleToken returns [EObject current=null] : iv_ruleToken= ruleToken EOF ;
    public final EObject entryRuleToken() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleToken = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:569:2: (iv_ruleToken= ruleToken EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:570:2: iv_ruleToken= ruleToken EOF
            {
             newCompositeNode(grammarAccess.getTokenRule()); 
            pushFollow(FOLLOW_ruleToken_in_entryRuleToken1205);
            iv_ruleToken=ruleToken();

            state._fsp--;

             current =iv_ruleToken; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleToken1215); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleToken"


    // $ANTLR start "ruleToken"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:577:1: ruleToken returns [EObject current=null] : (otherlv_0= 'Token' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (lv_msg_3_0= rulePHead ) ) ) ;
    public final EObject ruleToken() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        EObject lv_msg_3_0 = null;


         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:580:28: ( (otherlv_0= 'Token' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (lv_msg_3_0= rulePHead ) ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:581:1: (otherlv_0= 'Token' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (lv_msg_3_0= rulePHead ) ) )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:581:1: (otherlv_0= 'Token' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (lv_msg_3_0= rulePHead ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:581:3: otherlv_0= 'Token' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (lv_msg_3_0= rulePHead ) )
            {
            otherlv_0=(Token)match(input,18,FOLLOW_18_in_ruleToken1252); 

                	newLeafNode(otherlv_0, grammarAccess.getTokenAccess().getTokenKeyword_0());
                
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:585:1: ( (lv_name_1_0= RULE_ID ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:586:1: (lv_name_1_0= RULE_ID )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:586:1: (lv_name_1_0= RULE_ID )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:587:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleToken1269); 

            			newLeafNode(lv_name_1_0, grammarAccess.getTokenAccess().getNameIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getTokenRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"ID");
            	    

            }


            }

            otherlv_2=(Token)match(input,16,FOLLOW_16_in_ruleToken1286); 

                	newLeafNode(otherlv_2, grammarAccess.getTokenAccess().getColonKeyword_2());
                
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:607:1: ( (lv_msg_3_0= rulePHead ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:608:1: (lv_msg_3_0= rulePHead )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:608:1: (lv_msg_3_0= rulePHead )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:609:3: lv_msg_3_0= rulePHead
            {
             
            	        newCompositeNode(grammarAccess.getTokenAccess().getMsgPHeadParserRuleCall_3_0()); 
            	    
            pushFollow(FOLLOW_rulePHead_in_ruleToken1307);
            lv_msg_3_0=rulePHead();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getTokenRule());
            	        }
                   		set(
                   			current, 
                   			"msg",
                    		lv_msg_3_0, 
                    		"PHead");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleToken"


    // $ANTLR start "entryRuleDispatch"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:633:1: entryRuleDispatch returns [EObject current=null] : iv_ruleDispatch= ruleDispatch EOF ;
    public final EObject entryRuleDispatch() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDispatch = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:634:2: (iv_ruleDispatch= ruleDispatch EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:635:2: iv_ruleDispatch= ruleDispatch EOF
            {
             newCompositeNode(grammarAccess.getDispatchRule()); 
            pushFollow(FOLLOW_ruleDispatch_in_entryRuleDispatch1343);
            iv_ruleDispatch=ruleDispatch();

            state._fsp--;

             current =iv_ruleDispatch; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleDispatch1353); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDispatch"


    // $ANTLR start "ruleDispatch"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:642:1: ruleDispatch returns [EObject current=null] : (otherlv_0= 'Dispatch' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (lv_msg_3_0= rulePHead ) ) ) ;
    public final EObject ruleDispatch() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        EObject lv_msg_3_0 = null;


         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:645:28: ( (otherlv_0= 'Dispatch' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (lv_msg_3_0= rulePHead ) ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:646:1: (otherlv_0= 'Dispatch' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (lv_msg_3_0= rulePHead ) ) )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:646:1: (otherlv_0= 'Dispatch' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (lv_msg_3_0= rulePHead ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:646:3: otherlv_0= 'Dispatch' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (lv_msg_3_0= rulePHead ) )
            {
            otherlv_0=(Token)match(input,19,FOLLOW_19_in_ruleDispatch1390); 

                	newLeafNode(otherlv_0, grammarAccess.getDispatchAccess().getDispatchKeyword_0());
                
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:650:1: ( (lv_name_1_0= RULE_ID ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:651:1: (lv_name_1_0= RULE_ID )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:651:1: (lv_name_1_0= RULE_ID )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:652:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleDispatch1407); 

            			newLeafNode(lv_name_1_0, grammarAccess.getDispatchAccess().getNameIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getDispatchRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"ID");
            	    

            }


            }

            otherlv_2=(Token)match(input,16,FOLLOW_16_in_ruleDispatch1424); 

                	newLeafNode(otherlv_2, grammarAccess.getDispatchAccess().getColonKeyword_2());
                
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:672:1: ( (lv_msg_3_0= rulePHead ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:673:1: (lv_msg_3_0= rulePHead )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:673:1: (lv_msg_3_0= rulePHead )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:674:3: lv_msg_3_0= rulePHead
            {
             
            	        newCompositeNode(grammarAccess.getDispatchAccess().getMsgPHeadParserRuleCall_3_0()); 
            	    
            pushFollow(FOLLOW_rulePHead_in_ruleDispatch1445);
            lv_msg_3_0=rulePHead();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getDispatchRule());
            	        }
                   		set(
                   			current, 
                   			"msg",
                    		lv_msg_3_0, 
                    		"PHead");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDispatch"


    // $ANTLR start "entryRuleRequest"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:698:1: entryRuleRequest returns [EObject current=null] : iv_ruleRequest= ruleRequest EOF ;
    public final EObject entryRuleRequest() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRequest = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:699:2: (iv_ruleRequest= ruleRequest EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:700:2: iv_ruleRequest= ruleRequest EOF
            {
             newCompositeNode(grammarAccess.getRequestRule()); 
            pushFollow(FOLLOW_ruleRequest_in_entryRuleRequest1481);
            iv_ruleRequest=ruleRequest();

            state._fsp--;

             current =iv_ruleRequest; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleRequest1491); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRequest"


    // $ANTLR start "ruleRequest"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:707:1: ruleRequest returns [EObject current=null] : (otherlv_0= 'Request' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (lv_msg_3_0= rulePHead ) ) ) ;
    public final EObject ruleRequest() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        EObject lv_msg_3_0 = null;


         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:710:28: ( (otherlv_0= 'Request' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (lv_msg_3_0= rulePHead ) ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:711:1: (otherlv_0= 'Request' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (lv_msg_3_0= rulePHead ) ) )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:711:1: (otherlv_0= 'Request' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (lv_msg_3_0= rulePHead ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:711:3: otherlv_0= 'Request' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (lv_msg_3_0= rulePHead ) )
            {
            otherlv_0=(Token)match(input,20,FOLLOW_20_in_ruleRequest1528); 

                	newLeafNode(otherlv_0, grammarAccess.getRequestAccess().getRequestKeyword_0());
                
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:715:1: ( (lv_name_1_0= RULE_ID ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:716:1: (lv_name_1_0= RULE_ID )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:716:1: (lv_name_1_0= RULE_ID )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:717:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleRequest1545); 

            			newLeafNode(lv_name_1_0, grammarAccess.getRequestAccess().getNameIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getRequestRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"ID");
            	    

            }


            }

            otherlv_2=(Token)match(input,16,FOLLOW_16_in_ruleRequest1562); 

                	newLeafNode(otherlv_2, grammarAccess.getRequestAccess().getColonKeyword_2());
                
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:737:1: ( (lv_msg_3_0= rulePHead ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:738:1: (lv_msg_3_0= rulePHead )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:738:1: (lv_msg_3_0= rulePHead )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:739:3: lv_msg_3_0= rulePHead
            {
             
            	        newCompositeNode(grammarAccess.getRequestAccess().getMsgPHeadParserRuleCall_3_0()); 
            	    
            pushFollow(FOLLOW_rulePHead_in_ruleRequest1583);
            lv_msg_3_0=rulePHead();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getRequestRule());
            	        }
                   		set(
                   			current, 
                   			"msg",
                    		lv_msg_3_0, 
                    		"PHead");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRequest"


    // $ANTLR start "entryRuleInvitation"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:763:1: entryRuleInvitation returns [EObject current=null] : iv_ruleInvitation= ruleInvitation EOF ;
    public final EObject entryRuleInvitation() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInvitation = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:764:2: (iv_ruleInvitation= ruleInvitation EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:765:2: iv_ruleInvitation= ruleInvitation EOF
            {
             newCompositeNode(grammarAccess.getInvitationRule()); 
            pushFollow(FOLLOW_ruleInvitation_in_entryRuleInvitation1619);
            iv_ruleInvitation=ruleInvitation();

            state._fsp--;

             current =iv_ruleInvitation; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleInvitation1629); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInvitation"


    // $ANTLR start "ruleInvitation"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:772:1: ruleInvitation returns [EObject current=null] : (otherlv_0= 'Invitation' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (lv_msg_3_0= rulePHead ) ) ) ;
    public final EObject ruleInvitation() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        EObject lv_msg_3_0 = null;


         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:775:28: ( (otherlv_0= 'Invitation' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (lv_msg_3_0= rulePHead ) ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:776:1: (otherlv_0= 'Invitation' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (lv_msg_3_0= rulePHead ) ) )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:776:1: (otherlv_0= 'Invitation' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (lv_msg_3_0= rulePHead ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:776:3: otherlv_0= 'Invitation' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (lv_msg_3_0= rulePHead ) )
            {
            otherlv_0=(Token)match(input,21,FOLLOW_21_in_ruleInvitation1666); 

                	newLeafNode(otherlv_0, grammarAccess.getInvitationAccess().getInvitationKeyword_0());
                
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:780:1: ( (lv_name_1_0= RULE_ID ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:781:1: (lv_name_1_0= RULE_ID )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:781:1: (lv_name_1_0= RULE_ID )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:782:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleInvitation1683); 

            			newLeafNode(lv_name_1_0, grammarAccess.getInvitationAccess().getNameIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getInvitationRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"ID");
            	    

            }


            }

            otherlv_2=(Token)match(input,16,FOLLOW_16_in_ruleInvitation1700); 

                	newLeafNode(otherlv_2, grammarAccess.getInvitationAccess().getColonKeyword_2());
                
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:802:1: ( (lv_msg_3_0= rulePHead ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:803:1: (lv_msg_3_0= rulePHead )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:803:1: (lv_msg_3_0= rulePHead )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:804:3: lv_msg_3_0= rulePHead
            {
             
            	        newCompositeNode(grammarAccess.getInvitationAccess().getMsgPHeadParserRuleCall_3_0()); 
            	    
            pushFollow(FOLLOW_rulePHead_in_ruleInvitation1721);
            lv_msg_3_0=rulePHead();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getInvitationRule());
            	        }
                   		set(
                   			current, 
                   			"msg",
                    		lv_msg_3_0, 
                    		"PHead");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInvitation"


    // $ANTLR start "entryRuleContext"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:828:1: entryRuleContext returns [EObject current=null] : iv_ruleContext= ruleContext EOF ;
    public final EObject entryRuleContext() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleContext = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:829:2: (iv_ruleContext= ruleContext EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:830:2: iv_ruleContext= ruleContext EOF
            {
             newCompositeNode(grammarAccess.getContextRule()); 
            pushFollow(FOLLOW_ruleContext_in_entryRuleContext1757);
            iv_ruleContext=ruleContext();

            state._fsp--;

             current =iv_ruleContext; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleContext1767); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleContext"


    // $ANTLR start "ruleContext"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:837:1: ruleContext returns [EObject current=null] : (otherlv_0= 'Context' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= 'ip' ( (lv_ip_3_0= ruleComponentIP ) ) ( ( (lv_env_4_0= '-g' ) ) ( (lv_color_5_0= ruleWindowColor ) ) )? ( (lv_standalone_6_0= '-standalone ' ) )? ( (lv_httpserver_7_0= '-httpserver' ) )? ( (lv_handler_8_0= ruleEventHandler ) )* ) ;
    public final EObject ruleContext() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token lv_env_4_0=null;
        Token lv_standalone_6_0=null;
        Token lv_httpserver_7_0=null;
        EObject lv_ip_3_0 = null;

        Enumerator lv_color_5_0 = null;

        EObject lv_handler_8_0 = null;


         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:840:28: ( (otherlv_0= 'Context' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= 'ip' ( (lv_ip_3_0= ruleComponentIP ) ) ( ( (lv_env_4_0= '-g' ) ) ( (lv_color_5_0= ruleWindowColor ) ) )? ( (lv_standalone_6_0= '-standalone ' ) )? ( (lv_httpserver_7_0= '-httpserver' ) )? ( (lv_handler_8_0= ruleEventHandler ) )* ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:841:1: (otherlv_0= 'Context' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= 'ip' ( (lv_ip_3_0= ruleComponentIP ) ) ( ( (lv_env_4_0= '-g' ) ) ( (lv_color_5_0= ruleWindowColor ) ) )? ( (lv_standalone_6_0= '-standalone ' ) )? ( (lv_httpserver_7_0= '-httpserver' ) )? ( (lv_handler_8_0= ruleEventHandler ) )* )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:841:1: (otherlv_0= 'Context' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= 'ip' ( (lv_ip_3_0= ruleComponentIP ) ) ( ( (lv_env_4_0= '-g' ) ) ( (lv_color_5_0= ruleWindowColor ) ) )? ( (lv_standalone_6_0= '-standalone ' ) )? ( (lv_httpserver_7_0= '-httpserver' ) )? ( (lv_handler_8_0= ruleEventHandler ) )* )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:841:3: otherlv_0= 'Context' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= 'ip' ( (lv_ip_3_0= ruleComponentIP ) ) ( ( (lv_env_4_0= '-g' ) ) ( (lv_color_5_0= ruleWindowColor ) ) )? ( (lv_standalone_6_0= '-standalone ' ) )? ( (lv_httpserver_7_0= '-httpserver' ) )? ( (lv_handler_8_0= ruleEventHandler ) )*
            {
            otherlv_0=(Token)match(input,22,FOLLOW_22_in_ruleContext1804); 

                	newLeafNode(otherlv_0, grammarAccess.getContextAccess().getContextKeyword_0());
                
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:845:1: ( (lv_name_1_0= RULE_ID ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:846:1: (lv_name_1_0= RULE_ID )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:846:1: (lv_name_1_0= RULE_ID )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:847:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleContext1821); 

            			newLeafNode(lv_name_1_0, grammarAccess.getContextAccess().getNameIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getContextRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"ID");
            	    

            }


            }

            otherlv_2=(Token)match(input,23,FOLLOW_23_in_ruleContext1838); 

                	newLeafNode(otherlv_2, grammarAccess.getContextAccess().getIpKeyword_2());
                
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:867:1: ( (lv_ip_3_0= ruleComponentIP ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:868:1: (lv_ip_3_0= ruleComponentIP )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:868:1: (lv_ip_3_0= ruleComponentIP )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:869:3: lv_ip_3_0= ruleComponentIP
            {
             
            	        newCompositeNode(grammarAccess.getContextAccess().getIpComponentIPParserRuleCall_3_0()); 
            	    
            pushFollow(FOLLOW_ruleComponentIP_in_ruleContext1859);
            lv_ip_3_0=ruleComponentIP();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getContextRule());
            	        }
                   		set(
                   			current, 
                   			"ip",
                    		lv_ip_3_0, 
                    		"ComponentIP");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:885:2: ( ( (lv_env_4_0= '-g' ) ) ( (lv_color_5_0= ruleWindowColor ) ) )?
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==24) ) {
                alt9=1;
            }
            switch (alt9) {
                case 1 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:885:3: ( (lv_env_4_0= '-g' ) ) ( (lv_color_5_0= ruleWindowColor ) )
                    {
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:885:3: ( (lv_env_4_0= '-g' ) )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:886:1: (lv_env_4_0= '-g' )
                    {
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:886:1: (lv_env_4_0= '-g' )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:887:3: lv_env_4_0= '-g'
                    {
                    lv_env_4_0=(Token)match(input,24,FOLLOW_24_in_ruleContext1878); 

                            newLeafNode(lv_env_4_0, grammarAccess.getContextAccess().getEnvGKeyword_4_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getContextRule());
                    	        }
                           		setWithLastConsumed(current, "env", true, "-g");
                    	    

                    }


                    }

                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:900:2: ( (lv_color_5_0= ruleWindowColor ) )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:901:1: (lv_color_5_0= ruleWindowColor )
                    {
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:901:1: (lv_color_5_0= ruleWindowColor )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:902:3: lv_color_5_0= ruleWindowColor
                    {
                     
                    	        newCompositeNode(grammarAccess.getContextAccess().getColorWindowColorEnumRuleCall_4_1_0()); 
                    	    
                    pushFollow(FOLLOW_ruleWindowColor_in_ruleContext1912);
                    lv_color_5_0=ruleWindowColor();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getContextRule());
                    	        }
                           		set(
                           			current, 
                           			"color",
                            		lv_color_5_0, 
                            		"WindowColor");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:918:4: ( (lv_standalone_6_0= '-standalone ' ) )?
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==25) ) {
                alt10=1;
            }
            switch (alt10) {
                case 1 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:919:1: (lv_standalone_6_0= '-standalone ' )
                    {
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:919:1: (lv_standalone_6_0= '-standalone ' )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:920:3: lv_standalone_6_0= '-standalone '
                    {
                    lv_standalone_6_0=(Token)match(input,25,FOLLOW_25_in_ruleContext1932); 

                            newLeafNode(lv_standalone_6_0, grammarAccess.getContextAccess().getStandaloneStandaloneKeyword_5_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getContextRule());
                    	        }
                           		setWithLastConsumed(current, "standalone", true, "-standalone ");
                    	    

                    }


                    }
                    break;

            }

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:933:3: ( (lv_httpserver_7_0= '-httpserver' ) )?
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==26) ) {
                alt11=1;
            }
            switch (alt11) {
                case 1 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:934:1: (lv_httpserver_7_0= '-httpserver' )
                    {
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:934:1: (lv_httpserver_7_0= '-httpserver' )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:935:3: lv_httpserver_7_0= '-httpserver'
                    {
                    lv_httpserver_7_0=(Token)match(input,26,FOLLOW_26_in_ruleContext1964); 

                            newLeafNode(lv_httpserver_7_0, grammarAccess.getContextAccess().getHttpserverHttpserverKeyword_6_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getContextRule());
                    	        }
                           		setWithLastConsumed(current, "httpserver", true, "-httpserver");
                    	    

                    }


                    }
                    break;

            }

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:948:3: ( (lv_handler_8_0= ruleEventHandler ) )*
            loop12:
            do {
                int alt12=2;
                int LA12_0 = input.LA(1);

                if ( (LA12_0==107) ) {
                    alt12=1;
                }


                switch (alt12) {
            	case 1 :
            	    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:949:1: (lv_handler_8_0= ruleEventHandler )
            	    {
            	    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:949:1: (lv_handler_8_0= ruleEventHandler )
            	    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:950:3: lv_handler_8_0= ruleEventHandler
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getContextAccess().getHandlerEventHandlerParserRuleCall_7_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleEventHandler_in_ruleContext1999);
            	    lv_handler_8_0=ruleEventHandler();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getContextRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"handler",
            	            		lv_handler_8_0, 
            	            		"EventHandler");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop12;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleContext"


    // $ANTLR start "entryRuleQActor"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:974:1: entryRuleQActor returns [EObject current=null] : iv_ruleQActor= ruleQActor EOF ;
    public final EObject entryRuleQActor() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleQActor = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:975:2: (iv_ruleQActor= ruleQActor EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:976:2: iv_ruleQActor= ruleQActor EOF
            {
             newCompositeNode(grammarAccess.getQActorRule()); 
            pushFollow(FOLLOW_ruleQActor_in_entryRuleQActor2036);
            iv_ruleQActor=ruleQActor();

            state._fsp--;

             current =iv_ruleQActor; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleQActor2046); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQActor"


    // $ANTLR start "ruleQActor"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:983:1: ruleQActor returns [EObject current=null] : (otherlv_0= 'QActor' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= 'context' ( (otherlv_3= RULE_ID ) ) ( ( (lv_env_4_0= '-g' ) ) ( (lv_color_5_0= ruleWindowColor ) ) )? otherlv_6= '{' (otherlv_7= 'Rules' otherlv_8= '{' ( (lv_rules_9_0= ruleRule ) )* otherlv_10= '}' )? ( (lv_data_11_0= ruleData ) )* ( (lv_action_12_0= ruleAction ) )* ( (lv_plans_13_0= rulePlan ) )* otherlv_14= '}' ) ;
    public final EObject ruleQActor() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token lv_env_4_0=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        Token otherlv_14=null;
        Enumerator lv_color_5_0 = null;

        EObject lv_rules_9_0 = null;

        EObject lv_data_11_0 = null;

        EObject lv_action_12_0 = null;

        EObject lv_plans_13_0 = null;


         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:986:28: ( (otherlv_0= 'QActor' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= 'context' ( (otherlv_3= RULE_ID ) ) ( ( (lv_env_4_0= '-g' ) ) ( (lv_color_5_0= ruleWindowColor ) ) )? otherlv_6= '{' (otherlv_7= 'Rules' otherlv_8= '{' ( (lv_rules_9_0= ruleRule ) )* otherlv_10= '}' )? ( (lv_data_11_0= ruleData ) )* ( (lv_action_12_0= ruleAction ) )* ( (lv_plans_13_0= rulePlan ) )* otherlv_14= '}' ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:987:1: (otherlv_0= 'QActor' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= 'context' ( (otherlv_3= RULE_ID ) ) ( ( (lv_env_4_0= '-g' ) ) ( (lv_color_5_0= ruleWindowColor ) ) )? otherlv_6= '{' (otherlv_7= 'Rules' otherlv_8= '{' ( (lv_rules_9_0= ruleRule ) )* otherlv_10= '}' )? ( (lv_data_11_0= ruleData ) )* ( (lv_action_12_0= ruleAction ) )* ( (lv_plans_13_0= rulePlan ) )* otherlv_14= '}' )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:987:1: (otherlv_0= 'QActor' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= 'context' ( (otherlv_3= RULE_ID ) ) ( ( (lv_env_4_0= '-g' ) ) ( (lv_color_5_0= ruleWindowColor ) ) )? otherlv_6= '{' (otherlv_7= 'Rules' otherlv_8= '{' ( (lv_rules_9_0= ruleRule ) )* otherlv_10= '}' )? ( (lv_data_11_0= ruleData ) )* ( (lv_action_12_0= ruleAction ) )* ( (lv_plans_13_0= rulePlan ) )* otherlv_14= '}' )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:987:3: otherlv_0= 'QActor' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= 'context' ( (otherlv_3= RULE_ID ) ) ( ( (lv_env_4_0= '-g' ) ) ( (lv_color_5_0= ruleWindowColor ) ) )? otherlv_6= '{' (otherlv_7= 'Rules' otherlv_8= '{' ( (lv_rules_9_0= ruleRule ) )* otherlv_10= '}' )? ( (lv_data_11_0= ruleData ) )* ( (lv_action_12_0= ruleAction ) )* ( (lv_plans_13_0= rulePlan ) )* otherlv_14= '}'
            {
            otherlv_0=(Token)match(input,27,FOLLOW_27_in_ruleQActor2083); 

                	newLeafNode(otherlv_0, grammarAccess.getQActorAccess().getQActorKeyword_0());
                
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:991:1: ( (lv_name_1_0= RULE_ID ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:992:1: (lv_name_1_0= RULE_ID )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:992:1: (lv_name_1_0= RULE_ID )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:993:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleQActor2100); 

            			newLeafNode(lv_name_1_0, grammarAccess.getQActorAccess().getNameIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getQActorRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"ID");
            	    

            }


            }

            otherlv_2=(Token)match(input,28,FOLLOW_28_in_ruleQActor2117); 

                	newLeafNode(otherlv_2, grammarAccess.getQActorAccess().getContextKeyword_2());
                
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1013:1: ( (otherlv_3= RULE_ID ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1014:1: (otherlv_3= RULE_ID )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1014:1: (otherlv_3= RULE_ID )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1015:3: otherlv_3= RULE_ID
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getQActorRule());
            	        }
                    
            otherlv_3=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleQActor2137); 

            		newLeafNode(otherlv_3, grammarAccess.getQActorAccess().getContextContextCrossReference_3_0()); 
            	

            }


            }

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1026:2: ( ( (lv_env_4_0= '-g' ) ) ( (lv_color_5_0= ruleWindowColor ) ) )?
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==24) ) {
                alt13=1;
            }
            switch (alt13) {
                case 1 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1026:3: ( (lv_env_4_0= '-g' ) ) ( (lv_color_5_0= ruleWindowColor ) )
                    {
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1026:3: ( (lv_env_4_0= '-g' ) )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1027:1: (lv_env_4_0= '-g' )
                    {
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1027:1: (lv_env_4_0= '-g' )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1028:3: lv_env_4_0= '-g'
                    {
                    lv_env_4_0=(Token)match(input,24,FOLLOW_24_in_ruleQActor2156); 

                            newLeafNode(lv_env_4_0, grammarAccess.getQActorAccess().getEnvGKeyword_4_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getQActorRule());
                    	        }
                           		setWithLastConsumed(current, "env", true, "-g");
                    	    

                    }


                    }

                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1041:2: ( (lv_color_5_0= ruleWindowColor ) )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1042:1: (lv_color_5_0= ruleWindowColor )
                    {
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1042:1: (lv_color_5_0= ruleWindowColor )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1043:3: lv_color_5_0= ruleWindowColor
                    {
                     
                    	        newCompositeNode(grammarAccess.getQActorAccess().getColorWindowColorEnumRuleCall_4_1_0()); 
                    	    
                    pushFollow(FOLLOW_ruleWindowColor_in_ruleQActor2190);
                    lv_color_5_0=ruleWindowColor();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getQActorRule());
                    	        }
                           		set(
                           			current, 
                           			"color",
                            		lv_color_5_0, 
                            		"WindowColor");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }

            otherlv_6=(Token)match(input,29,FOLLOW_29_in_ruleQActor2204); 

                	newLeafNode(otherlv_6, grammarAccess.getQActorAccess().getLeftCurlyBracketKeyword_5());
                
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1063:1: (otherlv_7= 'Rules' otherlv_8= '{' ( (lv_rules_9_0= ruleRule ) )* otherlv_10= '}' )?
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0==30) ) {
                alt15=1;
            }
            switch (alt15) {
                case 1 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1063:3: otherlv_7= 'Rules' otherlv_8= '{' ( (lv_rules_9_0= ruleRule ) )* otherlv_10= '}'
                    {
                    otherlv_7=(Token)match(input,30,FOLLOW_30_in_ruleQActor2217); 

                        	newLeafNode(otherlv_7, grammarAccess.getQActorAccess().getRulesKeyword_6_0());
                        
                    otherlv_8=(Token)match(input,29,FOLLOW_29_in_ruleQActor2229); 

                        	newLeafNode(otherlv_8, grammarAccess.getQActorAccess().getLeftCurlyBracketKeyword_6_1());
                        
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1071:1: ( (lv_rules_9_0= ruleRule ) )*
                    loop14:
                    do {
                        int alt14=2;
                        int LA14_0 = input.LA(1);

                        if ( ((LA14_0>=RULE_ID && LA14_0<=RULE_VARID)||LA14_0==117) ) {
                            alt14=1;
                        }


                        switch (alt14) {
                    	case 1 :
                    	    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1072:1: (lv_rules_9_0= ruleRule )
                    	    {
                    	    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1072:1: (lv_rules_9_0= ruleRule )
                    	    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1073:3: lv_rules_9_0= ruleRule
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getQActorAccess().getRulesRuleParserRuleCall_6_2_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_ruleRule_in_ruleQActor2250);
                    	    lv_rules_9_0=ruleRule();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getQActorRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"rules",
                    	            		lv_rules_9_0, 
                    	            		"Rule");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop14;
                        }
                    } while (true);

                    otherlv_10=(Token)match(input,31,FOLLOW_31_in_ruleQActor2263); 

                        	newLeafNode(otherlv_10, grammarAccess.getQActorAccess().getRightCurlyBracketKeyword_6_3());
                        

                    }
                    break;

            }

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1093:3: ( (lv_data_11_0= ruleData ) )*
            loop16:
            do {
                int alt16=2;
                int LA16_0 = input.LA(1);

                if ( (LA16_0==42||LA16_0==44) ) {
                    alt16=1;
                }


                switch (alt16) {
            	case 1 :
            	    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1094:1: (lv_data_11_0= ruleData )
            	    {
            	    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1094:1: (lv_data_11_0= ruleData )
            	    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1095:3: lv_data_11_0= ruleData
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getQActorAccess().getDataDataParserRuleCall_7_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleData_in_ruleQActor2286);
            	    lv_data_11_0=ruleData();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getQActorRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"data",
            	            		lv_data_11_0, 
            	            		"Data");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop16;
                }
            } while (true);

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1111:3: ( (lv_action_12_0= ruleAction ) )*
            loop17:
            do {
                int alt17=2;
                int LA17_0 = input.LA(1);

                if ( (LA17_0==45) ) {
                    alt17=1;
                }


                switch (alt17) {
            	case 1 :
            	    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1112:1: (lv_action_12_0= ruleAction )
            	    {
            	    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1112:1: (lv_action_12_0= ruleAction )
            	    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1113:3: lv_action_12_0= ruleAction
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getQActorAccess().getActionActionParserRuleCall_8_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleAction_in_ruleQActor2308);
            	    lv_action_12_0=ruleAction();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getQActorRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"action",
            	            		lv_action_12_0, 
            	            		"Action");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop17;
                }
            } while (true);

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1129:3: ( (lv_plans_13_0= rulePlan ) )*
            loop18:
            do {
                int alt18=2;
                int LA18_0 = input.LA(1);

                if ( (LA18_0==50) ) {
                    alt18=1;
                }


                switch (alt18) {
            	case 1 :
            	    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1130:1: (lv_plans_13_0= rulePlan )
            	    {
            	    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1130:1: (lv_plans_13_0= rulePlan )
            	    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1131:3: lv_plans_13_0= rulePlan
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getQActorAccess().getPlansPlanParserRuleCall_9_0()); 
            	    	    
            	    pushFollow(FOLLOW_rulePlan_in_ruleQActor2330);
            	    lv_plans_13_0=rulePlan();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getQActorRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"plans",
            	            		lv_plans_13_0, 
            	            		"Plan");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop18;
                }
            } while (true);

            otherlv_14=(Token)match(input,31,FOLLOW_31_in_ruleQActor2343); 

                	newLeafNode(otherlv_14, grammarAccess.getQActorAccess().getRightCurlyBracketKeyword_10());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQActor"


    // $ANTLR start "entryRuleRule"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1159:1: entryRuleRule returns [EObject current=null] : iv_ruleRule= ruleRule EOF ;
    public final EObject entryRuleRule() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRule = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1160:2: (iv_ruleRule= ruleRule EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1161:2: iv_ruleRule= ruleRule EOF
            {
             newCompositeNode(grammarAccess.getRuleRule()); 
            pushFollow(FOLLOW_ruleRule_in_entryRuleRule2379);
            iv_ruleRule=ruleRule();

            state._fsp--;

             current =iv_ruleRule; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleRule2389); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRule"


    // $ANTLR start "ruleRule"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1168:1: ruleRule returns [EObject current=null] : ( ( (lv_head_0_0= rulePHead ) ) (otherlv_1= ':-' ( (lv_body_2_0= rulePTerm ) ) (otherlv_3= ',' ( (lv_body_4_0= rulePTerm ) ) )* )? otherlv_5= '.' ) ;
    public final EObject ruleRule() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        EObject lv_head_0_0 = null;

        EObject lv_body_2_0 = null;

        EObject lv_body_4_0 = null;


         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1171:28: ( ( ( (lv_head_0_0= rulePHead ) ) (otherlv_1= ':-' ( (lv_body_2_0= rulePTerm ) ) (otherlv_3= ',' ( (lv_body_4_0= rulePTerm ) ) )* )? otherlv_5= '.' ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1172:1: ( ( (lv_head_0_0= rulePHead ) ) (otherlv_1= ':-' ( (lv_body_2_0= rulePTerm ) ) (otherlv_3= ',' ( (lv_body_4_0= rulePTerm ) ) )* )? otherlv_5= '.' )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1172:1: ( ( (lv_head_0_0= rulePHead ) ) (otherlv_1= ':-' ( (lv_body_2_0= rulePTerm ) ) (otherlv_3= ',' ( (lv_body_4_0= rulePTerm ) ) )* )? otherlv_5= '.' )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1172:2: ( (lv_head_0_0= rulePHead ) ) (otherlv_1= ':-' ( (lv_body_2_0= rulePTerm ) ) (otherlv_3= ',' ( (lv_body_4_0= rulePTerm ) ) )* )? otherlv_5= '.'
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1172:2: ( (lv_head_0_0= rulePHead ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1173:1: (lv_head_0_0= rulePHead )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1173:1: (lv_head_0_0= rulePHead )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1174:3: lv_head_0_0= rulePHead
            {
             
            	        newCompositeNode(grammarAccess.getRuleAccess().getHeadPHeadParserRuleCall_0_0()); 
            	    
            pushFollow(FOLLOW_rulePHead_in_ruleRule2435);
            lv_head_0_0=rulePHead();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getRuleRule());
            	        }
                   		set(
                   			current, 
                   			"head",
                    		lv_head_0_0, 
                    		"PHead");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1190:2: (otherlv_1= ':-' ( (lv_body_2_0= rulePTerm ) ) (otherlv_3= ',' ( (lv_body_4_0= rulePTerm ) ) )* )?
            int alt20=2;
            int LA20_0 = input.LA(1);

            if ( (LA20_0==32) ) {
                alt20=1;
            }
            switch (alt20) {
                case 1 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1190:4: otherlv_1= ':-' ( (lv_body_2_0= rulePTerm ) ) (otherlv_3= ',' ( (lv_body_4_0= rulePTerm ) ) )*
                    {
                    otherlv_1=(Token)match(input,32,FOLLOW_32_in_ruleRule2448); 

                        	newLeafNode(otherlv_1, grammarAccess.getRuleAccess().getColonHyphenMinusKeyword_1_0());
                        
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1194:1: ( (lv_body_2_0= rulePTerm ) )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1195:1: (lv_body_2_0= rulePTerm )
                    {
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1195:1: (lv_body_2_0= rulePTerm )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1196:3: lv_body_2_0= rulePTerm
                    {
                     
                    	        newCompositeNode(grammarAccess.getRuleAccess().getBodyPTermParserRuleCall_1_1_0()); 
                    	    
                    pushFollow(FOLLOW_rulePTerm_in_ruleRule2469);
                    lv_body_2_0=rulePTerm();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getRuleRule());
                    	        }
                           		add(
                           			current, 
                           			"body",
                            		lv_body_2_0, 
                            		"PTerm");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1212:2: (otherlv_3= ',' ( (lv_body_4_0= rulePTerm ) ) )*
                    loop19:
                    do {
                        int alt19=2;
                        int LA19_0 = input.LA(1);

                        if ( (LA19_0==33) ) {
                            alt19=1;
                        }


                        switch (alt19) {
                    	case 1 :
                    	    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1212:4: otherlv_3= ',' ( (lv_body_4_0= rulePTerm ) )
                    	    {
                    	    otherlv_3=(Token)match(input,33,FOLLOW_33_in_ruleRule2482); 

                    	        	newLeafNode(otherlv_3, grammarAccess.getRuleAccess().getCommaKeyword_1_2_0());
                    	        
                    	    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1216:1: ( (lv_body_4_0= rulePTerm ) )
                    	    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1217:1: (lv_body_4_0= rulePTerm )
                    	    {
                    	    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1217:1: (lv_body_4_0= rulePTerm )
                    	    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1218:3: lv_body_4_0= rulePTerm
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getRuleAccess().getBodyPTermParserRuleCall_1_2_1_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_rulePTerm_in_ruleRule2503);
                    	    lv_body_4_0=rulePTerm();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getRuleRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"body",
                    	            		lv_body_4_0, 
                    	            		"PTerm");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop19;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_5=(Token)match(input,34,FOLLOW_34_in_ruleRule2519); 

                	newLeafNode(otherlv_5, grammarAccess.getRuleAccess().getFullStopKeyword_2());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRule"


    // $ANTLR start "entryRulePHead"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1246:1: entryRulePHead returns [EObject current=null] : iv_rulePHead= rulePHead EOF ;
    public final EObject entryRulePHead() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePHead = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1247:2: (iv_rulePHead= rulePHead EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1248:2: iv_rulePHead= rulePHead EOF
            {
             newCompositeNode(grammarAccess.getPHeadRule()); 
            pushFollow(FOLLOW_rulePHead_in_entryRulePHead2555);
            iv_rulePHead=rulePHead();

            state._fsp--;

             current =iv_rulePHead; 
            match(input,EOF,FOLLOW_EOF_in_entryRulePHead2565); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePHead"


    // $ANTLR start "rulePHead"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1255:1: rulePHead returns [EObject current=null] : (this_PAtom_0= rulePAtom | this_PStruct_1= rulePStruct ) ;
    public final EObject rulePHead() throws RecognitionException {
        EObject current = null;

        EObject this_PAtom_0 = null;

        EObject this_PStruct_1 = null;


         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1258:28: ( (this_PAtom_0= rulePAtom | this_PStruct_1= rulePStruct ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1259:1: (this_PAtom_0= rulePAtom | this_PStruct_1= rulePStruct )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1259:1: (this_PAtom_0= rulePAtom | this_PStruct_1= rulePStruct )
            int alt21=2;
            int LA21_0 = input.LA(1);

            if ( ((LA21_0>=RULE_STRING && LA21_0<=RULE_VARID)||LA21_0==117) ) {
                alt21=1;
            }
            else if ( (LA21_0==RULE_ID) ) {
                int LA21_2 = input.LA(2);

                if ( (LA21_2==EOF||LA21_2==RULE_ID||LA21_2==15||(LA21_2>=17 && LA21_2<=22)||LA21_2==27||(LA21_2>=31 && LA21_2<=34)||LA21_2==36||(LA21_2>=49 && LA21_2<=50)||LA21_2==53||LA21_2==63||LA21_2==88||LA21_2==96||LA21_2==113||LA21_2==118) ) {
                    alt21=1;
                }
                else if ( (LA21_2==35) ) {
                    alt21=2;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 21, 2, input);

                    throw nvae;
                }
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 21, 0, input);

                throw nvae;
            }
            switch (alt21) {
                case 1 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1260:5: this_PAtom_0= rulePAtom
                    {
                     
                            newCompositeNode(grammarAccess.getPHeadAccess().getPAtomParserRuleCall_0()); 
                        
                    pushFollow(FOLLOW_rulePAtom_in_rulePHead2612);
                    this_PAtom_0=rulePAtom();

                    state._fsp--;

                     
                            current = this_PAtom_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1270:5: this_PStruct_1= rulePStruct
                    {
                     
                            newCompositeNode(grammarAccess.getPHeadAccess().getPStructParserRuleCall_1()); 
                        
                    pushFollow(FOLLOW_rulePStruct_in_rulePHead2639);
                    this_PStruct_1=rulePStruct();

                    state._fsp--;

                     
                            current = this_PStruct_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePHead"


    // $ANTLR start "entryRulePTerm"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1286:1: entryRulePTerm returns [EObject current=null] : iv_rulePTerm= rulePTerm EOF ;
    public final EObject entryRulePTerm() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePTerm = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1287:2: (iv_rulePTerm= rulePTerm EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1288:2: iv_rulePTerm= rulePTerm EOF
            {
             newCompositeNode(grammarAccess.getPTermRule()); 
            pushFollow(FOLLOW_rulePTerm_in_entryRulePTerm2674);
            iv_rulePTerm=rulePTerm();

            state._fsp--;

             current =iv_rulePTerm; 
            match(input,EOF,FOLLOW_EOF_in_entryRulePTerm2684); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePTerm"


    // $ANTLR start "rulePTerm"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1295:1: rulePTerm returns [EObject current=null] : (this_PAtom_0= rulePAtom | this_PStruct_1= rulePStruct | this_PPredef_2= rulePPredef | this_PActorCall_3= rulePActorCall ) ;
    public final EObject rulePTerm() throws RecognitionException {
        EObject current = null;

        EObject this_PAtom_0 = null;

        EObject this_PStruct_1 = null;

        EObject this_PPredef_2 = null;

        EObject this_PActorCall_3 = null;


         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1298:28: ( (this_PAtom_0= rulePAtom | this_PStruct_1= rulePStruct | this_PPredef_2= rulePPredef | this_PActorCall_3= rulePActorCall ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1299:1: (this_PAtom_0= rulePAtom | this_PStruct_1= rulePStruct | this_PPredef_2= rulePPredef | this_PActorCall_3= rulePActorCall )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1299:1: (this_PAtom_0= rulePAtom | this_PStruct_1= rulePStruct | this_PPredef_2= rulePPredef | this_PActorCall_3= rulePActorCall )
            int alt22=4;
            switch ( input.LA(1) ) {
            case RULE_STRING:
            case RULE_INT:
                {
                alt22=1;
                }
                break;
            case 117:
                {
                int LA22_2 = input.LA(2);

                if ( (LA22_2==RULE_VARID) ) {
                    int LA22_3 = input.LA(3);

                    if ( (LA22_3==39) ) {
                        alt22=3;
                    }
                    else if ( (LA22_3==EOF||(LA22_3>=33 && LA22_3<=34)||LA22_3==36||LA22_3==56||LA22_3==108) ) {
                        alt22=1;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 22, 3, input);

                        throw nvae;
                    }
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 22, 2, input);

                    throw nvae;
                }
                }
                break;
            case RULE_VARID:
                {
                int LA22_3 = input.LA(2);

                if ( (LA22_3==39) ) {
                    alt22=3;
                }
                else if ( (LA22_3==EOF||(LA22_3>=33 && LA22_3<=34)||LA22_3==36||LA22_3==56||LA22_3==108) ) {
                    alt22=1;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 22, 3, input);

                    throw nvae;
                }
                }
                break;
            case RULE_ID:
                {
                int LA22_4 = input.LA(2);

                if ( (LA22_4==35) ) {
                    alt22=2;
                }
                else if ( (LA22_4==EOF||(LA22_4>=33 && LA22_4<=34)||LA22_4==36||LA22_4==56||LA22_4==108) ) {
                    alt22=1;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 22, 4, input);

                    throw nvae;
                }
                }
                break;
            case 41:
                {
                alt22=3;
                }
                break;
            case 37:
                {
                alt22=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 22, 0, input);

                throw nvae;
            }

            switch (alt22) {
                case 1 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1300:5: this_PAtom_0= rulePAtom
                    {
                     
                            newCompositeNode(grammarAccess.getPTermAccess().getPAtomParserRuleCall_0()); 
                        
                    pushFollow(FOLLOW_rulePAtom_in_rulePTerm2731);
                    this_PAtom_0=rulePAtom();

                    state._fsp--;

                     
                            current = this_PAtom_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1310:5: this_PStruct_1= rulePStruct
                    {
                     
                            newCompositeNode(grammarAccess.getPTermAccess().getPStructParserRuleCall_1()); 
                        
                    pushFollow(FOLLOW_rulePStruct_in_rulePTerm2758);
                    this_PStruct_1=rulePStruct();

                    state._fsp--;

                     
                            current = this_PStruct_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 3 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1320:5: this_PPredef_2= rulePPredef
                    {
                     
                            newCompositeNode(grammarAccess.getPTermAccess().getPPredefParserRuleCall_2()); 
                        
                    pushFollow(FOLLOW_rulePPredef_in_rulePTerm2785);
                    this_PPredef_2=rulePPredef();

                    state._fsp--;

                     
                            current = this_PPredef_2; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 4 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1330:5: this_PActorCall_3= rulePActorCall
                    {
                     
                            newCompositeNode(grammarAccess.getPTermAccess().getPActorCallParserRuleCall_3()); 
                        
                    pushFollow(FOLLOW_rulePActorCall_in_rulePTerm2812);
                    this_PActorCall_3=rulePActorCall();

                    state._fsp--;

                     
                            current = this_PActorCall_3; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePTerm"


    // $ANTLR start "entryRulePAtom"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1346:1: entryRulePAtom returns [EObject current=null] : iv_rulePAtom= rulePAtom EOF ;
    public final EObject entryRulePAtom() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePAtom = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1347:2: (iv_rulePAtom= rulePAtom EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1348:2: iv_rulePAtom= rulePAtom EOF
            {
             newCompositeNode(grammarAccess.getPAtomRule()); 
            pushFollow(FOLLOW_rulePAtom_in_entryRulePAtom2847);
            iv_rulePAtom=rulePAtom();

            state._fsp--;

             current =iv_rulePAtom; 
            match(input,EOF,FOLLOW_EOF_in_entryRulePAtom2857); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePAtom"


    // $ANTLR start "rulePAtom"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1355:1: rulePAtom returns [EObject current=null] : (this_PAtomString_0= rulePAtomString | this_Variable_1= ruleVariable | this_PAtomNum_2= rulePAtomNum | this_PAtomic_3= rulePAtomic ) ;
    public final EObject rulePAtom() throws RecognitionException {
        EObject current = null;

        EObject this_PAtomString_0 = null;

        EObject this_Variable_1 = null;

        EObject this_PAtomNum_2 = null;

        EObject this_PAtomic_3 = null;


         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1358:28: ( (this_PAtomString_0= rulePAtomString | this_Variable_1= ruleVariable | this_PAtomNum_2= rulePAtomNum | this_PAtomic_3= rulePAtomic ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1359:1: (this_PAtomString_0= rulePAtomString | this_Variable_1= ruleVariable | this_PAtomNum_2= rulePAtomNum | this_PAtomic_3= rulePAtomic )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1359:1: (this_PAtomString_0= rulePAtomString | this_Variable_1= ruleVariable | this_PAtomNum_2= rulePAtomNum | this_PAtomic_3= rulePAtomic )
            int alt23=4;
            switch ( input.LA(1) ) {
            case RULE_STRING:
                {
                alt23=1;
                }
                break;
            case RULE_VARID:
            case 117:
                {
                alt23=2;
                }
                break;
            case RULE_INT:
                {
                alt23=3;
                }
                break;
            case RULE_ID:
                {
                alt23=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 23, 0, input);

                throw nvae;
            }

            switch (alt23) {
                case 1 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1360:5: this_PAtomString_0= rulePAtomString
                    {
                     
                            newCompositeNode(grammarAccess.getPAtomAccess().getPAtomStringParserRuleCall_0()); 
                        
                    pushFollow(FOLLOW_rulePAtomString_in_rulePAtom2904);
                    this_PAtomString_0=rulePAtomString();

                    state._fsp--;

                     
                            current = this_PAtomString_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1370:5: this_Variable_1= ruleVariable
                    {
                     
                            newCompositeNode(grammarAccess.getPAtomAccess().getVariableParserRuleCall_1()); 
                        
                    pushFollow(FOLLOW_ruleVariable_in_rulePAtom2931);
                    this_Variable_1=ruleVariable();

                    state._fsp--;

                     
                            current = this_Variable_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 3 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1380:5: this_PAtomNum_2= rulePAtomNum
                    {
                     
                            newCompositeNode(grammarAccess.getPAtomAccess().getPAtomNumParserRuleCall_2()); 
                        
                    pushFollow(FOLLOW_rulePAtomNum_in_rulePAtom2958);
                    this_PAtomNum_2=rulePAtomNum();

                    state._fsp--;

                     
                            current = this_PAtomNum_2; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 4 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1390:5: this_PAtomic_3= rulePAtomic
                    {
                     
                            newCompositeNode(grammarAccess.getPAtomAccess().getPAtomicParserRuleCall_3()); 
                        
                    pushFollow(FOLLOW_rulePAtomic_in_rulePAtom2985);
                    this_PAtomic_3=rulePAtomic();

                    state._fsp--;

                     
                            current = this_PAtomic_3; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePAtom"


    // $ANTLR start "entryRulePAtomString"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1406:1: entryRulePAtomString returns [EObject current=null] : iv_rulePAtomString= rulePAtomString EOF ;
    public final EObject entryRulePAtomString() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePAtomString = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1407:2: (iv_rulePAtomString= rulePAtomString EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1408:2: iv_rulePAtomString= rulePAtomString EOF
            {
             newCompositeNode(grammarAccess.getPAtomStringRule()); 
            pushFollow(FOLLOW_rulePAtomString_in_entryRulePAtomString3020);
            iv_rulePAtomString=rulePAtomString();

            state._fsp--;

             current =iv_rulePAtomString; 
            match(input,EOF,FOLLOW_EOF_in_entryRulePAtomString3030); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePAtomString"


    // $ANTLR start "rulePAtomString"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1415:1: rulePAtomString returns [EObject current=null] : ( (lv_val_0_0= RULE_STRING ) ) ;
    public final EObject rulePAtomString() throws RecognitionException {
        EObject current = null;

        Token lv_val_0_0=null;

         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1418:28: ( ( (lv_val_0_0= RULE_STRING ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1419:1: ( (lv_val_0_0= RULE_STRING ) )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1419:1: ( (lv_val_0_0= RULE_STRING ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1420:1: (lv_val_0_0= RULE_STRING )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1420:1: (lv_val_0_0= RULE_STRING )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1421:3: lv_val_0_0= RULE_STRING
            {
            lv_val_0_0=(Token)match(input,RULE_STRING,FOLLOW_RULE_STRING_in_rulePAtomString3071); 

            			newLeafNode(lv_val_0_0, grammarAccess.getPAtomStringAccess().getValSTRINGTerminalRuleCall_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getPAtomStringRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"val",
                    		lv_val_0_0, 
                    		"STRING");
            	    

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePAtomString"


    // $ANTLR start "entryRulePAtomic"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1445:1: entryRulePAtomic returns [EObject current=null] : iv_rulePAtomic= rulePAtomic EOF ;
    public final EObject entryRulePAtomic() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePAtomic = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1446:2: (iv_rulePAtomic= rulePAtomic EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1447:2: iv_rulePAtomic= rulePAtomic EOF
            {
             newCompositeNode(grammarAccess.getPAtomicRule()); 
            pushFollow(FOLLOW_rulePAtomic_in_entryRulePAtomic3111);
            iv_rulePAtomic=rulePAtomic();

            state._fsp--;

             current =iv_rulePAtomic; 
            match(input,EOF,FOLLOW_EOF_in_entryRulePAtomic3121); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePAtomic"


    // $ANTLR start "rulePAtomic"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1454:1: rulePAtomic returns [EObject current=null] : ( (lv_val_0_0= RULE_ID ) ) ;
    public final EObject rulePAtomic() throws RecognitionException {
        EObject current = null;

        Token lv_val_0_0=null;

         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1457:28: ( ( (lv_val_0_0= RULE_ID ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1458:1: ( (lv_val_0_0= RULE_ID ) )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1458:1: ( (lv_val_0_0= RULE_ID ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1459:1: (lv_val_0_0= RULE_ID )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1459:1: (lv_val_0_0= RULE_ID )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1460:3: lv_val_0_0= RULE_ID
            {
            lv_val_0_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_rulePAtomic3162); 

            			newLeafNode(lv_val_0_0, grammarAccess.getPAtomicAccess().getValIDTerminalRuleCall_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getPAtomicRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"val",
                    		lv_val_0_0, 
                    		"ID");
            	    

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePAtomic"


    // $ANTLR start "entryRulePAtomNum"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1484:1: entryRulePAtomNum returns [EObject current=null] : iv_rulePAtomNum= rulePAtomNum EOF ;
    public final EObject entryRulePAtomNum() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePAtomNum = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1485:2: (iv_rulePAtomNum= rulePAtomNum EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1486:2: iv_rulePAtomNum= rulePAtomNum EOF
            {
             newCompositeNode(grammarAccess.getPAtomNumRule()); 
            pushFollow(FOLLOW_rulePAtomNum_in_entryRulePAtomNum3202);
            iv_rulePAtomNum=rulePAtomNum();

            state._fsp--;

             current =iv_rulePAtomNum; 
            match(input,EOF,FOLLOW_EOF_in_entryRulePAtomNum3212); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePAtomNum"


    // $ANTLR start "rulePAtomNum"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1493:1: rulePAtomNum returns [EObject current=null] : ( (lv_val_0_0= RULE_INT ) ) ;
    public final EObject rulePAtomNum() throws RecognitionException {
        EObject current = null;

        Token lv_val_0_0=null;

         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1496:28: ( ( (lv_val_0_0= RULE_INT ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1497:1: ( (lv_val_0_0= RULE_INT ) )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1497:1: ( (lv_val_0_0= RULE_INT ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1498:1: (lv_val_0_0= RULE_INT )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1498:1: (lv_val_0_0= RULE_INT )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1499:3: lv_val_0_0= RULE_INT
            {
            lv_val_0_0=(Token)match(input,RULE_INT,FOLLOW_RULE_INT_in_rulePAtomNum3253); 

            			newLeafNode(lv_val_0_0, grammarAccess.getPAtomNumAccess().getValINTTerminalRuleCall_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getPAtomNumRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"val",
                    		lv_val_0_0, 
                    		"INT");
            	    

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePAtomNum"


    // $ANTLR start "entryRulePStruct"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1523:1: entryRulePStruct returns [EObject current=null] : iv_rulePStruct= rulePStruct EOF ;
    public final EObject entryRulePStruct() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePStruct = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1524:2: (iv_rulePStruct= rulePStruct EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1525:2: iv_rulePStruct= rulePStruct EOF
            {
             newCompositeNode(grammarAccess.getPStructRule()); 
            pushFollow(FOLLOW_rulePStruct_in_entryRulePStruct3293);
            iv_rulePStruct=rulePStruct();

            state._fsp--;

             current =iv_rulePStruct; 
            match(input,EOF,FOLLOW_EOF_in_entryRulePStruct3303); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePStruct"


    // $ANTLR start "rulePStruct"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1532:1: rulePStruct returns [EObject current=null] : ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= '(' ( (lv_msgArg_2_0= rulePTerm ) )? (otherlv_3= ',' ( (lv_msgArg_4_0= rulePTerm ) ) )* otherlv_5= ')' ) ;
    public final EObject rulePStruct() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        EObject lv_msgArg_2_0 = null;

        EObject lv_msgArg_4_0 = null;


         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1535:28: ( ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= '(' ( (lv_msgArg_2_0= rulePTerm ) )? (otherlv_3= ',' ( (lv_msgArg_4_0= rulePTerm ) ) )* otherlv_5= ')' ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1536:1: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= '(' ( (lv_msgArg_2_0= rulePTerm ) )? (otherlv_3= ',' ( (lv_msgArg_4_0= rulePTerm ) ) )* otherlv_5= ')' )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1536:1: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= '(' ( (lv_msgArg_2_0= rulePTerm ) )? (otherlv_3= ',' ( (lv_msgArg_4_0= rulePTerm ) ) )* otherlv_5= ')' )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1536:2: ( (lv_name_0_0= RULE_ID ) ) otherlv_1= '(' ( (lv_msgArg_2_0= rulePTerm ) )? (otherlv_3= ',' ( (lv_msgArg_4_0= rulePTerm ) ) )* otherlv_5= ')'
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1536:2: ( (lv_name_0_0= RULE_ID ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1537:1: (lv_name_0_0= RULE_ID )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1537:1: (lv_name_0_0= RULE_ID )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1538:3: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_rulePStruct3345); 

            			newLeafNode(lv_name_0_0, grammarAccess.getPStructAccess().getNameIDTerminalRuleCall_0_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getPStructRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_0_0, 
                    		"ID");
            	    

            }


            }

            otherlv_1=(Token)match(input,35,FOLLOW_35_in_rulePStruct3362); 

                	newLeafNode(otherlv_1, grammarAccess.getPStructAccess().getLeftParenthesisKeyword_1());
                
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1558:1: ( (lv_msgArg_2_0= rulePTerm ) )?
            int alt24=2;
            int LA24_0 = input.LA(1);

            if ( ((LA24_0>=RULE_ID && LA24_0<=RULE_VARID)||LA24_0==37||LA24_0==41||LA24_0==117) ) {
                alt24=1;
            }
            switch (alt24) {
                case 1 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1559:1: (lv_msgArg_2_0= rulePTerm )
                    {
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1559:1: (lv_msgArg_2_0= rulePTerm )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1560:3: lv_msgArg_2_0= rulePTerm
                    {
                     
                    	        newCompositeNode(grammarAccess.getPStructAccess().getMsgArgPTermParserRuleCall_2_0()); 
                    	    
                    pushFollow(FOLLOW_rulePTerm_in_rulePStruct3383);
                    lv_msgArg_2_0=rulePTerm();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getPStructRule());
                    	        }
                           		add(
                           			current, 
                           			"msgArg",
                            		lv_msgArg_2_0, 
                            		"PTerm");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }
                    break;

            }

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1576:3: (otherlv_3= ',' ( (lv_msgArg_4_0= rulePTerm ) ) )*
            loop25:
            do {
                int alt25=2;
                int LA25_0 = input.LA(1);

                if ( (LA25_0==33) ) {
                    alt25=1;
                }


                switch (alt25) {
            	case 1 :
            	    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1576:5: otherlv_3= ',' ( (lv_msgArg_4_0= rulePTerm ) )
            	    {
            	    otherlv_3=(Token)match(input,33,FOLLOW_33_in_rulePStruct3397); 

            	        	newLeafNode(otherlv_3, grammarAccess.getPStructAccess().getCommaKeyword_3_0());
            	        
            	    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1580:1: ( (lv_msgArg_4_0= rulePTerm ) )
            	    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1581:1: (lv_msgArg_4_0= rulePTerm )
            	    {
            	    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1581:1: (lv_msgArg_4_0= rulePTerm )
            	    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1582:3: lv_msgArg_4_0= rulePTerm
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getPStructAccess().getMsgArgPTermParserRuleCall_3_1_0()); 
            	    	    
            	    pushFollow(FOLLOW_rulePTerm_in_rulePStruct3418);
            	    lv_msgArg_4_0=rulePTerm();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getPStructRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"msgArg",
            	            		lv_msgArg_4_0, 
            	            		"PTerm");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop25;
                }
            } while (true);

            otherlv_5=(Token)match(input,36,FOLLOW_36_in_rulePStruct3432); 

                	newLeafNode(otherlv_5, grammarAccess.getPStructAccess().getRightParenthesisKeyword_4());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePStruct"


    // $ANTLR start "entryRulePActorCall"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1610:1: entryRulePActorCall returns [EObject current=null] : iv_rulePActorCall= rulePActorCall EOF ;
    public final EObject entryRulePActorCall() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePActorCall = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1611:2: (iv_rulePActorCall= rulePActorCall EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1612:2: iv_rulePActorCall= rulePActorCall EOF
            {
             newCompositeNode(grammarAccess.getPActorCallRule()); 
            pushFollow(FOLLOW_rulePActorCall_in_entryRulePActorCall3468);
            iv_rulePActorCall=rulePActorCall();

            state._fsp--;

             current =iv_rulePActorCall; 
            match(input,EOF,FOLLOW_EOF_in_entryRulePActorCall3478); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePActorCall"


    // $ANTLR start "rulePActorCall"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1619:1: rulePActorCall returns [EObject current=null] : (otherlv_0= 'Actor' otherlv_1= '<-' ( (lv_body_2_0= rulePStruct ) ) ) ;
    public final EObject rulePActorCall() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        EObject lv_body_2_0 = null;


         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1622:28: ( (otherlv_0= 'Actor' otherlv_1= '<-' ( (lv_body_2_0= rulePStruct ) ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1623:1: (otherlv_0= 'Actor' otherlv_1= '<-' ( (lv_body_2_0= rulePStruct ) ) )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1623:1: (otherlv_0= 'Actor' otherlv_1= '<-' ( (lv_body_2_0= rulePStruct ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1623:3: otherlv_0= 'Actor' otherlv_1= '<-' ( (lv_body_2_0= rulePStruct ) )
            {
            otherlv_0=(Token)match(input,37,FOLLOW_37_in_rulePActorCall3515); 

                	newLeafNode(otherlv_0, grammarAccess.getPActorCallAccess().getActorKeyword_0());
                
            otherlv_1=(Token)match(input,38,FOLLOW_38_in_rulePActorCall3527); 

                	newLeafNode(otherlv_1, grammarAccess.getPActorCallAccess().getLessThanSignHyphenMinusKeyword_1());
                
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1631:1: ( (lv_body_2_0= rulePStruct ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1632:1: (lv_body_2_0= rulePStruct )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1632:1: (lv_body_2_0= rulePStruct )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1633:3: lv_body_2_0= rulePStruct
            {
             
            	        newCompositeNode(grammarAccess.getPActorCallAccess().getBodyPStructParserRuleCall_2_0()); 
            	    
            pushFollow(FOLLOW_rulePStruct_in_rulePActorCall3548);
            lv_body_2_0=rulePStruct();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getPActorCallRule());
            	        }
                   		set(
                   			current, 
                   			"body",
                    		lv_body_2_0, 
                    		"PStruct");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePActorCall"


    // $ANTLR start "entryRulePPredef"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1657:1: entryRulePPredef returns [EObject current=null] : iv_rulePPredef= rulePPredef EOF ;
    public final EObject entryRulePPredef() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePPredef = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1658:2: (iv_rulePPredef= rulePPredef EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1659:2: iv_rulePPredef= rulePPredef EOF
            {
             newCompositeNode(grammarAccess.getPPredefRule()); 
            pushFollow(FOLLOW_rulePPredef_in_entryRulePPredef3584);
            iv_rulePPredef=rulePPredef();

            state._fsp--;

             current =iv_rulePPredef; 
            match(input,EOF,FOLLOW_EOF_in_entryRulePPredef3594); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePPredef"


    // $ANTLR start "rulePPredef"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1666:1: rulePPredef returns [EObject current=null] : (this_PAtomCut_0= rulePAtomCut | this_PIs_1= rulePIs ) ;
    public final EObject rulePPredef() throws RecognitionException {
        EObject current = null;

        EObject this_PAtomCut_0 = null;

        EObject this_PIs_1 = null;


         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1669:28: ( (this_PAtomCut_0= rulePAtomCut | this_PIs_1= rulePIs ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1670:1: (this_PAtomCut_0= rulePAtomCut | this_PIs_1= rulePIs )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1670:1: (this_PAtomCut_0= rulePAtomCut | this_PIs_1= rulePIs )
            int alt26=2;
            int LA26_0 = input.LA(1);

            if ( (LA26_0==41) ) {
                alt26=1;
            }
            else if ( (LA26_0==RULE_VARID||LA26_0==117) ) {
                alt26=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 26, 0, input);

                throw nvae;
            }
            switch (alt26) {
                case 1 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1671:5: this_PAtomCut_0= rulePAtomCut
                    {
                     
                            newCompositeNode(grammarAccess.getPPredefAccess().getPAtomCutParserRuleCall_0()); 
                        
                    pushFollow(FOLLOW_rulePAtomCut_in_rulePPredef3641);
                    this_PAtomCut_0=rulePAtomCut();

                    state._fsp--;

                     
                            current = this_PAtomCut_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1681:5: this_PIs_1= rulePIs
                    {
                     
                            newCompositeNode(grammarAccess.getPPredefAccess().getPIsParserRuleCall_1()); 
                        
                    pushFollow(FOLLOW_rulePIs_in_rulePPredef3668);
                    this_PIs_1=rulePIs();

                    state._fsp--;

                     
                            current = this_PIs_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePPredef"


    // $ANTLR start "entryRulePIs"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1697:1: entryRulePIs returns [EObject current=null] : iv_rulePIs= rulePIs EOF ;
    public final EObject entryRulePIs() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePIs = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1698:2: (iv_rulePIs= rulePIs EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1699:2: iv_rulePIs= rulePIs EOF
            {
             newCompositeNode(grammarAccess.getPIsRule()); 
            pushFollow(FOLLOW_rulePIs_in_entryRulePIs3703);
            iv_rulePIs=rulePIs();

            state._fsp--;

             current =iv_rulePIs; 
            match(input,EOF,FOLLOW_EOF_in_entryRulePIs3713); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePIs"


    // $ANTLR start "rulePIs"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1706:1: rulePIs returns [EObject current=null] : ( ( (lv_varout_0_0= ruleVariable ) ) otherlv_1= 'is' ( (lv_varin_2_0= ruleVariable ) ) otherlv_3= '+' ( (lv_num_4_0= rulePAtomNum ) ) ) ;
    public final EObject rulePIs() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_varout_0_0 = null;

        EObject lv_varin_2_0 = null;

        EObject lv_num_4_0 = null;


         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1709:28: ( ( ( (lv_varout_0_0= ruleVariable ) ) otherlv_1= 'is' ( (lv_varin_2_0= ruleVariable ) ) otherlv_3= '+' ( (lv_num_4_0= rulePAtomNum ) ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1710:1: ( ( (lv_varout_0_0= ruleVariable ) ) otherlv_1= 'is' ( (lv_varin_2_0= ruleVariable ) ) otherlv_3= '+' ( (lv_num_4_0= rulePAtomNum ) ) )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1710:1: ( ( (lv_varout_0_0= ruleVariable ) ) otherlv_1= 'is' ( (lv_varin_2_0= ruleVariable ) ) otherlv_3= '+' ( (lv_num_4_0= rulePAtomNum ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1710:2: ( (lv_varout_0_0= ruleVariable ) ) otherlv_1= 'is' ( (lv_varin_2_0= ruleVariable ) ) otherlv_3= '+' ( (lv_num_4_0= rulePAtomNum ) )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1710:2: ( (lv_varout_0_0= ruleVariable ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1711:1: (lv_varout_0_0= ruleVariable )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1711:1: (lv_varout_0_0= ruleVariable )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1712:3: lv_varout_0_0= ruleVariable
            {
             
            	        newCompositeNode(grammarAccess.getPIsAccess().getVaroutVariableParserRuleCall_0_0()); 
            	    
            pushFollow(FOLLOW_ruleVariable_in_rulePIs3759);
            lv_varout_0_0=ruleVariable();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getPIsRule());
            	        }
                   		set(
                   			current, 
                   			"varout",
                    		lv_varout_0_0, 
                    		"Variable");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_1=(Token)match(input,39,FOLLOW_39_in_rulePIs3771); 

                	newLeafNode(otherlv_1, grammarAccess.getPIsAccess().getIsKeyword_1());
                
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1732:1: ( (lv_varin_2_0= ruleVariable ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1733:1: (lv_varin_2_0= ruleVariable )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1733:1: (lv_varin_2_0= ruleVariable )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1734:3: lv_varin_2_0= ruleVariable
            {
             
            	        newCompositeNode(grammarAccess.getPIsAccess().getVarinVariableParserRuleCall_2_0()); 
            	    
            pushFollow(FOLLOW_ruleVariable_in_rulePIs3792);
            lv_varin_2_0=ruleVariable();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getPIsRule());
            	        }
                   		set(
                   			current, 
                   			"varin",
                    		lv_varin_2_0, 
                    		"Variable");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_3=(Token)match(input,40,FOLLOW_40_in_rulePIs3804); 

                	newLeafNode(otherlv_3, grammarAccess.getPIsAccess().getPlusSignKeyword_3());
                
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1754:1: ( (lv_num_4_0= rulePAtomNum ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1755:1: (lv_num_4_0= rulePAtomNum )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1755:1: (lv_num_4_0= rulePAtomNum )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1756:3: lv_num_4_0= rulePAtomNum
            {
             
            	        newCompositeNode(grammarAccess.getPIsAccess().getNumPAtomNumParserRuleCall_4_0()); 
            	    
            pushFollow(FOLLOW_rulePAtomNum_in_rulePIs3825);
            lv_num_4_0=rulePAtomNum();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getPIsRule());
            	        }
                   		set(
                   			current, 
                   			"num",
                    		lv_num_4_0, 
                    		"PAtomNum");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePIs"


    // $ANTLR start "entryRulePAtomCut"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1780:1: entryRulePAtomCut returns [EObject current=null] : iv_rulePAtomCut= rulePAtomCut EOF ;
    public final EObject entryRulePAtomCut() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePAtomCut = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1781:2: (iv_rulePAtomCut= rulePAtomCut EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1782:2: iv_rulePAtomCut= rulePAtomCut EOF
            {
             newCompositeNode(grammarAccess.getPAtomCutRule()); 
            pushFollow(FOLLOW_rulePAtomCut_in_entryRulePAtomCut3861);
            iv_rulePAtomCut=rulePAtomCut();

            state._fsp--;

             current =iv_rulePAtomCut; 
            match(input,EOF,FOLLOW_EOF_in_entryRulePAtomCut3871); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePAtomCut"


    // $ANTLR start "rulePAtomCut"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1789:1: rulePAtomCut returns [EObject current=null] : ( (lv_name_0_0= '!' ) ) ;
    public final EObject rulePAtomCut() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;

         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1792:28: ( ( (lv_name_0_0= '!' ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1793:1: ( (lv_name_0_0= '!' ) )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1793:1: ( (lv_name_0_0= '!' ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1794:1: (lv_name_0_0= '!' )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1794:1: (lv_name_0_0= '!' )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1795:3: lv_name_0_0= '!'
            {
            lv_name_0_0=(Token)match(input,41,FOLLOW_41_in_rulePAtomCut3913); 

                    newLeafNode(lv_name_0_0, grammarAccess.getPAtomCutAccess().getNameExclamationMarkKeyword_0());
                

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getPAtomCutRule());
            	        }
                   		setWithLastConsumed(current, "name", lv_name_0_0, "!");
            	    

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePAtomCut"


    // $ANTLR start "entryRuleData"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1816:1: entryRuleData returns [EObject current=null] : iv_ruleData= ruleData EOF ;
    public final EObject entryRuleData() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleData = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1817:2: (iv_ruleData= ruleData EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1818:2: iv_ruleData= ruleData EOF
            {
             newCompositeNode(grammarAccess.getDataRule()); 
            pushFollow(FOLLOW_ruleData_in_entryRuleData3961);
            iv_ruleData=ruleData();

            state._fsp--;

             current =iv_ruleData; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleData3971); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleData"


    // $ANTLR start "ruleData"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1825:1: ruleData returns [EObject current=null] : (this_IntegerData_0= ruleIntegerData | this_StringData_1= ruleStringData ) ;
    public final EObject ruleData() throws RecognitionException {
        EObject current = null;

        EObject this_IntegerData_0 = null;

        EObject this_StringData_1 = null;


         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1828:28: ( (this_IntegerData_0= ruleIntegerData | this_StringData_1= ruleStringData ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1829:1: (this_IntegerData_0= ruleIntegerData | this_StringData_1= ruleStringData )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1829:1: (this_IntegerData_0= ruleIntegerData | this_StringData_1= ruleStringData )
            int alt27=2;
            int LA27_0 = input.LA(1);

            if ( (LA27_0==42) ) {
                alt27=1;
            }
            else if ( (LA27_0==44) ) {
                alt27=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 27, 0, input);

                throw nvae;
            }
            switch (alt27) {
                case 1 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1830:5: this_IntegerData_0= ruleIntegerData
                    {
                     
                            newCompositeNode(grammarAccess.getDataAccess().getIntegerDataParserRuleCall_0()); 
                        
                    pushFollow(FOLLOW_ruleIntegerData_in_ruleData4018);
                    this_IntegerData_0=ruleIntegerData();

                    state._fsp--;

                     
                            current = this_IntegerData_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1840:5: this_StringData_1= ruleStringData
                    {
                     
                            newCompositeNode(grammarAccess.getDataAccess().getStringDataParserRuleCall_1()); 
                        
                    pushFollow(FOLLOW_ruleStringData_in_ruleData4045);
                    this_StringData_1=ruleStringData();

                    state._fsp--;

                     
                            current = this_StringData_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleData"


    // $ANTLR start "entryRuleIntegerData"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1856:1: entryRuleIntegerData returns [EObject current=null] : iv_ruleIntegerData= ruleIntegerData EOF ;
    public final EObject entryRuleIntegerData() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIntegerData = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1857:2: (iv_ruleIntegerData= ruleIntegerData EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1858:2: iv_ruleIntegerData= ruleIntegerData EOF
            {
             newCompositeNode(grammarAccess.getIntegerDataRule()); 
            pushFollow(FOLLOW_ruleIntegerData_in_entryRuleIntegerData4080);
            iv_ruleIntegerData=ruleIntegerData();

            state._fsp--;

             current =iv_ruleIntegerData; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleIntegerData4090); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIntegerData"


    // $ANTLR start "ruleIntegerData"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1865:1: ruleIntegerData returns [EObject current=null] : (otherlv_0= 'int' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '=' ( (lv_value_3_0= RULE_INT ) ) ) ;
    public final EObject ruleIntegerData() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token lv_value_3_0=null;

         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1868:28: ( (otherlv_0= 'int' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '=' ( (lv_value_3_0= RULE_INT ) ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1869:1: (otherlv_0= 'int' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '=' ( (lv_value_3_0= RULE_INT ) ) )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1869:1: (otherlv_0= 'int' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '=' ( (lv_value_3_0= RULE_INT ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1869:3: otherlv_0= 'int' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '=' ( (lv_value_3_0= RULE_INT ) )
            {
            otherlv_0=(Token)match(input,42,FOLLOW_42_in_ruleIntegerData4127); 

                	newLeafNode(otherlv_0, grammarAccess.getIntegerDataAccess().getIntKeyword_0());
                
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1873:1: ( (lv_name_1_0= RULE_ID ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1874:1: (lv_name_1_0= RULE_ID )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1874:1: (lv_name_1_0= RULE_ID )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1875:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleIntegerData4144); 

            			newLeafNode(lv_name_1_0, grammarAccess.getIntegerDataAccess().getNameIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getIntegerDataRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"ID");
            	    

            }


            }

            otherlv_2=(Token)match(input,43,FOLLOW_43_in_ruleIntegerData4161); 

                	newLeafNode(otherlv_2, grammarAccess.getIntegerDataAccess().getEqualsSignKeyword_2());
                
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1895:1: ( (lv_value_3_0= RULE_INT ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1896:1: (lv_value_3_0= RULE_INT )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1896:1: (lv_value_3_0= RULE_INT )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1897:3: lv_value_3_0= RULE_INT
            {
            lv_value_3_0=(Token)match(input,RULE_INT,FOLLOW_RULE_INT_in_ruleIntegerData4178); 

            			newLeafNode(lv_value_3_0, grammarAccess.getIntegerDataAccess().getValueINTTerminalRuleCall_3_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getIntegerDataRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"value",
                    		lv_value_3_0, 
                    		"INT");
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIntegerData"


    // $ANTLR start "entryRuleStringData"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1921:1: entryRuleStringData returns [EObject current=null] : iv_ruleStringData= ruleStringData EOF ;
    public final EObject entryRuleStringData() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStringData = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1922:2: (iv_ruleStringData= ruleStringData EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1923:2: iv_ruleStringData= ruleStringData EOF
            {
             newCompositeNode(grammarAccess.getStringDataRule()); 
            pushFollow(FOLLOW_ruleStringData_in_entryRuleStringData4219);
            iv_ruleStringData=ruleStringData();

            state._fsp--;

             current =iv_ruleStringData; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleStringData4229); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStringData"


    // $ANTLR start "ruleStringData"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1930:1: ruleStringData returns [EObject current=null] : (otherlv_0= 'String' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '=' ( (lv_value_3_0= RULE_STRING ) ) ) ;
    public final EObject ruleStringData() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token lv_value_3_0=null;

         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1933:28: ( (otherlv_0= 'String' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '=' ( (lv_value_3_0= RULE_STRING ) ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1934:1: (otherlv_0= 'String' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '=' ( (lv_value_3_0= RULE_STRING ) ) )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1934:1: (otherlv_0= 'String' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '=' ( (lv_value_3_0= RULE_STRING ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1934:3: otherlv_0= 'String' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '=' ( (lv_value_3_0= RULE_STRING ) )
            {
            otherlv_0=(Token)match(input,44,FOLLOW_44_in_ruleStringData4266); 

                	newLeafNode(otherlv_0, grammarAccess.getStringDataAccess().getStringKeyword_0());
                
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1938:1: ( (lv_name_1_0= RULE_ID ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1939:1: (lv_name_1_0= RULE_ID )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1939:1: (lv_name_1_0= RULE_ID )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1940:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleStringData4283); 

            			newLeafNode(lv_name_1_0, grammarAccess.getStringDataAccess().getNameIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getStringDataRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"ID");
            	    

            }


            }

            otherlv_2=(Token)match(input,43,FOLLOW_43_in_ruleStringData4300); 

                	newLeafNode(otherlv_2, grammarAccess.getStringDataAccess().getEqualsSignKeyword_2());
                
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1960:1: ( (lv_value_3_0= RULE_STRING ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1961:1: (lv_value_3_0= RULE_STRING )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1961:1: (lv_value_3_0= RULE_STRING )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1962:3: lv_value_3_0= RULE_STRING
            {
            lv_value_3_0=(Token)match(input,RULE_STRING,FOLLOW_RULE_STRING_in_ruleStringData4317); 

            			newLeafNode(lv_value_3_0, grammarAccess.getStringDataAccess().getValueSTRINGTerminalRuleCall_3_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getStringDataRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"value",
                    		lv_value_3_0, 
                    		"STRING");
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStringData"


    // $ANTLR start "entryRuleAction"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1986:1: entryRuleAction returns [EObject current=null] : iv_ruleAction= ruleAction EOF ;
    public final EObject entryRuleAction() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAction = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1987:2: (iv_ruleAction= ruleAction EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1988:2: iv_ruleAction= ruleAction EOF
            {
             newCompositeNode(grammarAccess.getActionRule()); 
            pushFollow(FOLLOW_ruleAction_in_entryRuleAction4358);
            iv_ruleAction=ruleAction();

            state._fsp--;

             current =iv_ruleAction; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleAction4368); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAction"


    // $ANTLR start "ruleAction"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1995:1: ruleAction returns [EObject current=null] : (otherlv_0= 'Action' ( (lv_name_1_0= RULE_ID ) ) ( (lv_undoable_2_0= 'undoable' ) )? (otherlv_3= 'maxtime' otherlv_4= '(' ( (lv_msec_5_0= RULE_INT ) ) otherlv_6= ')' ) (otherlv_7= 'arg' ( (lv_arg_8_0= rulePStruct ) ) )? otherlv_9= ';' ) ;
    public final EObject ruleAction() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token lv_undoable_2_0=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token lv_msec_5_0=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        EObject lv_arg_8_0 = null;


         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1998:28: ( (otherlv_0= 'Action' ( (lv_name_1_0= RULE_ID ) ) ( (lv_undoable_2_0= 'undoable' ) )? (otherlv_3= 'maxtime' otherlv_4= '(' ( (lv_msec_5_0= RULE_INT ) ) otherlv_6= ')' ) (otherlv_7= 'arg' ( (lv_arg_8_0= rulePStruct ) ) )? otherlv_9= ';' ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1999:1: (otherlv_0= 'Action' ( (lv_name_1_0= RULE_ID ) ) ( (lv_undoable_2_0= 'undoable' ) )? (otherlv_3= 'maxtime' otherlv_4= '(' ( (lv_msec_5_0= RULE_INT ) ) otherlv_6= ')' ) (otherlv_7= 'arg' ( (lv_arg_8_0= rulePStruct ) ) )? otherlv_9= ';' )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1999:1: (otherlv_0= 'Action' ( (lv_name_1_0= RULE_ID ) ) ( (lv_undoable_2_0= 'undoable' ) )? (otherlv_3= 'maxtime' otherlv_4= '(' ( (lv_msec_5_0= RULE_INT ) ) otherlv_6= ')' ) (otherlv_7= 'arg' ( (lv_arg_8_0= rulePStruct ) ) )? otherlv_9= ';' )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:1999:3: otherlv_0= 'Action' ( (lv_name_1_0= RULE_ID ) ) ( (lv_undoable_2_0= 'undoable' ) )? (otherlv_3= 'maxtime' otherlv_4= '(' ( (lv_msec_5_0= RULE_INT ) ) otherlv_6= ')' ) (otherlv_7= 'arg' ( (lv_arg_8_0= rulePStruct ) ) )? otherlv_9= ';'
            {
            otherlv_0=(Token)match(input,45,FOLLOW_45_in_ruleAction4405); 

                	newLeafNode(otherlv_0, grammarAccess.getActionAccess().getActionKeyword_0());
                
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2003:1: ( (lv_name_1_0= RULE_ID ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2004:1: (lv_name_1_0= RULE_ID )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2004:1: (lv_name_1_0= RULE_ID )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2005:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleAction4422); 

            			newLeafNode(lv_name_1_0, grammarAccess.getActionAccess().getNameIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getActionRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"ID");
            	    

            }


            }

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2021:2: ( (lv_undoable_2_0= 'undoable' ) )?
            int alt28=2;
            int LA28_0 = input.LA(1);

            if ( (LA28_0==46) ) {
                alt28=1;
            }
            switch (alt28) {
                case 1 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2022:1: (lv_undoable_2_0= 'undoable' )
                    {
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2022:1: (lv_undoable_2_0= 'undoable' )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2023:3: lv_undoable_2_0= 'undoable'
                    {
                    lv_undoable_2_0=(Token)match(input,46,FOLLOW_46_in_ruleAction4445); 

                            newLeafNode(lv_undoable_2_0, grammarAccess.getActionAccess().getUndoableUndoableKeyword_2_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getActionRule());
                    	        }
                           		setWithLastConsumed(current, "undoable", true, "undoable");
                    	    

                    }


                    }
                    break;

            }

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2036:3: (otherlv_3= 'maxtime' otherlv_4= '(' ( (lv_msec_5_0= RULE_INT ) ) otherlv_6= ')' )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2036:5: otherlv_3= 'maxtime' otherlv_4= '(' ( (lv_msec_5_0= RULE_INT ) ) otherlv_6= ')'
            {
            otherlv_3=(Token)match(input,47,FOLLOW_47_in_ruleAction4472); 

                	newLeafNode(otherlv_3, grammarAccess.getActionAccess().getMaxtimeKeyword_3_0());
                
            otherlv_4=(Token)match(input,35,FOLLOW_35_in_ruleAction4484); 

                	newLeafNode(otherlv_4, grammarAccess.getActionAccess().getLeftParenthesisKeyword_3_1());
                
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2044:1: ( (lv_msec_5_0= RULE_INT ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2045:1: (lv_msec_5_0= RULE_INT )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2045:1: (lv_msec_5_0= RULE_INT )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2046:3: lv_msec_5_0= RULE_INT
            {
            lv_msec_5_0=(Token)match(input,RULE_INT,FOLLOW_RULE_INT_in_ruleAction4501); 

            			newLeafNode(lv_msec_5_0, grammarAccess.getActionAccess().getMsecINTTerminalRuleCall_3_2_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getActionRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"msec",
                    		lv_msec_5_0, 
                    		"INT");
            	    

            }


            }

            otherlv_6=(Token)match(input,36,FOLLOW_36_in_ruleAction4518); 

                	newLeafNode(otherlv_6, grammarAccess.getActionAccess().getRightParenthesisKeyword_3_3());
                

            }

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2066:2: (otherlv_7= 'arg' ( (lv_arg_8_0= rulePStruct ) ) )?
            int alt29=2;
            int LA29_0 = input.LA(1);

            if ( (LA29_0==48) ) {
                alt29=1;
            }
            switch (alt29) {
                case 1 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2066:4: otherlv_7= 'arg' ( (lv_arg_8_0= rulePStruct ) )
                    {
                    otherlv_7=(Token)match(input,48,FOLLOW_48_in_ruleAction4532); 

                        	newLeafNode(otherlv_7, grammarAccess.getActionAccess().getArgKeyword_4_0());
                        
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2070:1: ( (lv_arg_8_0= rulePStruct ) )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2071:1: (lv_arg_8_0= rulePStruct )
                    {
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2071:1: (lv_arg_8_0= rulePStruct )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2072:3: lv_arg_8_0= rulePStruct
                    {
                     
                    	        newCompositeNode(grammarAccess.getActionAccess().getArgPStructParserRuleCall_4_1_0()); 
                    	    
                    pushFollow(FOLLOW_rulePStruct_in_ruleAction4553);
                    lv_arg_8_0=rulePStruct();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getActionRule());
                    	        }
                           		set(
                           			current, 
                           			"arg",
                            		lv_arg_8_0, 
                            		"PStruct");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }

            otherlv_9=(Token)match(input,49,FOLLOW_49_in_ruleAction4567); 

                	newLeafNode(otherlv_9, grammarAccess.getActionAccess().getSemicolonKeyword_5());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAction"


    // $ANTLR start "entryRulePlan"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2100:1: entryRulePlan returns [EObject current=null] : iv_rulePlan= rulePlan EOF ;
    public final EObject entryRulePlan() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePlan = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2101:2: (iv_rulePlan= rulePlan EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2102:2: iv_rulePlan= rulePlan EOF
            {
             newCompositeNode(grammarAccess.getPlanRule()); 
            pushFollow(FOLLOW_rulePlan_in_entryRulePlan4603);
            iv_rulePlan=rulePlan();

            state._fsp--;

             current =iv_rulePlan; 
            match(input,EOF,FOLLOW_EOF_in_entryRulePlan4613); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePlan"


    // $ANTLR start "rulePlan"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2109:1: rulePlan returns [EObject current=null] : (otherlv_0= 'Plan' ( (lv_name_1_0= RULE_ID ) ) ( (lv_normal_2_0= 'normal' ) )? ( (lv_resume_3_0= 'resumeLastPlan' ) )? ( (lv_action_4_0= rulePlanAction ) ) (otherlv_5= ';' ( (lv_action_6_0= rulePlanAction ) ) )* ) ;
    public final EObject rulePlan() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token lv_normal_2_0=null;
        Token lv_resume_3_0=null;
        Token otherlv_5=null;
        EObject lv_action_4_0 = null;

        EObject lv_action_6_0 = null;


         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2112:28: ( (otherlv_0= 'Plan' ( (lv_name_1_0= RULE_ID ) ) ( (lv_normal_2_0= 'normal' ) )? ( (lv_resume_3_0= 'resumeLastPlan' ) )? ( (lv_action_4_0= rulePlanAction ) ) (otherlv_5= ';' ( (lv_action_6_0= rulePlanAction ) ) )* ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2113:1: (otherlv_0= 'Plan' ( (lv_name_1_0= RULE_ID ) ) ( (lv_normal_2_0= 'normal' ) )? ( (lv_resume_3_0= 'resumeLastPlan' ) )? ( (lv_action_4_0= rulePlanAction ) ) (otherlv_5= ';' ( (lv_action_6_0= rulePlanAction ) ) )* )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2113:1: (otherlv_0= 'Plan' ( (lv_name_1_0= RULE_ID ) ) ( (lv_normal_2_0= 'normal' ) )? ( (lv_resume_3_0= 'resumeLastPlan' ) )? ( (lv_action_4_0= rulePlanAction ) ) (otherlv_5= ';' ( (lv_action_6_0= rulePlanAction ) ) )* )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2113:3: otherlv_0= 'Plan' ( (lv_name_1_0= RULE_ID ) ) ( (lv_normal_2_0= 'normal' ) )? ( (lv_resume_3_0= 'resumeLastPlan' ) )? ( (lv_action_4_0= rulePlanAction ) ) (otherlv_5= ';' ( (lv_action_6_0= rulePlanAction ) ) )*
            {
            otherlv_0=(Token)match(input,50,FOLLOW_50_in_rulePlan4650); 

                	newLeafNode(otherlv_0, grammarAccess.getPlanAccess().getPlanKeyword_0());
                
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2117:1: ( (lv_name_1_0= RULE_ID ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2118:1: (lv_name_1_0= RULE_ID )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2118:1: (lv_name_1_0= RULE_ID )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2119:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_rulePlan4667); 

            			newLeafNode(lv_name_1_0, grammarAccess.getPlanAccess().getNameIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getPlanRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"ID");
            	    

            }


            }

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2135:2: ( (lv_normal_2_0= 'normal' ) )?
            int alt30=2;
            int LA30_0 = input.LA(1);

            if ( (LA30_0==51) ) {
                alt30=1;
            }
            switch (alt30) {
                case 1 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2136:1: (lv_normal_2_0= 'normal' )
                    {
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2136:1: (lv_normal_2_0= 'normal' )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2137:3: lv_normal_2_0= 'normal'
                    {
                    lv_normal_2_0=(Token)match(input,51,FOLLOW_51_in_rulePlan4690); 

                            newLeafNode(lv_normal_2_0, grammarAccess.getPlanAccess().getNormalNormalKeyword_2_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getPlanRule());
                    	        }
                           		setWithLastConsumed(current, "normal", true, "normal");
                    	    

                    }


                    }
                    break;

            }

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2150:3: ( (lv_resume_3_0= 'resumeLastPlan' ) )?
            int alt31=2;
            int LA31_0 = input.LA(1);

            if ( (LA31_0==52) ) {
                int LA31_1 = input.LA(2);

                if ( (LA31_1==52||LA31_1==54||LA31_1==59||(LA31_1>=61 && LA31_1<=62)||(LA31_1>=64 && LA31_1<=68)||(LA31_1>=70 && LA31_1<=71)||(LA31_1>=73 && LA31_1<=85)||LA31_1==87||(LA31_1>=89 && LA31_1<=90)||LA31_1==93||LA31_1==95||(LA31_1>=97 && LA31_1<=100)||(LA31_1>=102 && LA31_1<=105)) ) {
                    alt31=1;
                }
            }
            switch (alt31) {
                case 1 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2151:1: (lv_resume_3_0= 'resumeLastPlan' )
                    {
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2151:1: (lv_resume_3_0= 'resumeLastPlan' )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2152:3: lv_resume_3_0= 'resumeLastPlan'
                    {
                    lv_resume_3_0=(Token)match(input,52,FOLLOW_52_in_rulePlan4722); 

                            newLeafNode(lv_resume_3_0, grammarAccess.getPlanAccess().getResumeResumeLastPlanKeyword_3_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getPlanRule());
                    	        }
                           		setWithLastConsumed(current, "resume", true, "resumeLastPlan");
                    	    

                    }


                    }
                    break;

            }

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2165:3: ( (lv_action_4_0= rulePlanAction ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2166:1: (lv_action_4_0= rulePlanAction )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2166:1: (lv_action_4_0= rulePlanAction )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2167:3: lv_action_4_0= rulePlanAction
            {
             
            	        newCompositeNode(grammarAccess.getPlanAccess().getActionPlanActionParserRuleCall_4_0()); 
            	    
            pushFollow(FOLLOW_rulePlanAction_in_rulePlan4757);
            lv_action_4_0=rulePlanAction();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getPlanRule());
            	        }
                   		add(
                   			current, 
                   			"action",
                    		lv_action_4_0, 
                    		"PlanAction");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2183:2: (otherlv_5= ';' ( (lv_action_6_0= rulePlanAction ) ) )*
            loop32:
            do {
                int alt32=2;
                int LA32_0 = input.LA(1);

                if ( (LA32_0==49) ) {
                    alt32=1;
                }


                switch (alt32) {
            	case 1 :
            	    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2183:4: otherlv_5= ';' ( (lv_action_6_0= rulePlanAction ) )
            	    {
            	    otherlv_5=(Token)match(input,49,FOLLOW_49_in_rulePlan4770); 

            	        	newLeafNode(otherlv_5, grammarAccess.getPlanAccess().getSemicolonKeyword_5_0());
            	        
            	    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2187:1: ( (lv_action_6_0= rulePlanAction ) )
            	    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2188:1: (lv_action_6_0= rulePlanAction )
            	    {
            	    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2188:1: (lv_action_6_0= rulePlanAction )
            	    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2189:3: lv_action_6_0= rulePlanAction
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getPlanAccess().getActionPlanActionParserRuleCall_5_1_0()); 
            	    	    
            	    pushFollow(FOLLOW_rulePlanAction_in_rulePlan4791);
            	    lv_action_6_0=rulePlanAction();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getPlanRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"action",
            	            		lv_action_6_0, 
            	            		"PlanAction");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop32;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePlan"


    // $ANTLR start "entryRulePlanAction"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2213:1: entryRulePlanAction returns [EObject current=null] : iv_rulePlanAction= rulePlanAction EOF ;
    public final EObject entryRulePlanAction() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePlanAction = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2214:2: (iv_rulePlanAction= rulePlanAction EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2215:2: iv_rulePlanAction= rulePlanAction EOF
            {
             newCompositeNode(grammarAccess.getPlanActionRule()); 
            pushFollow(FOLLOW_rulePlanAction_in_entryRulePlanAction4829);
            iv_rulePlanAction=rulePlanAction();

            state._fsp--;

             current =iv_rulePlanAction; 
            match(input,EOF,FOLLOW_EOF_in_entryRulePlanAction4839); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePlanAction"


    // $ANTLR start "rulePlanAction"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2222:1: rulePlanAction returns [EObject current=null] : ( ( (lv_guard_0_0= ruleGuard ) )? ( (lv_move_1_0= ruleMove ) ) ( (lv_react_2_0= ruleReaction ) )? (otherlv_3= 'else' ( (lv_elsemove_4_0= ruleMove ) ) ( (lv_elsereact_5_0= ruleReaction ) )? )? ) ;
    public final EObject rulePlanAction() throws RecognitionException {
        EObject current = null;

        Token otherlv_3=null;
        EObject lv_guard_0_0 = null;

        EObject lv_move_1_0 = null;

        EObject lv_react_2_0 = null;

        EObject lv_elsemove_4_0 = null;

        EObject lv_elsereact_5_0 = null;


         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2225:28: ( ( ( (lv_guard_0_0= ruleGuard ) )? ( (lv_move_1_0= ruleMove ) ) ( (lv_react_2_0= ruleReaction ) )? (otherlv_3= 'else' ( (lv_elsemove_4_0= ruleMove ) ) ( (lv_elsereact_5_0= ruleReaction ) )? )? ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2226:1: ( ( (lv_guard_0_0= ruleGuard ) )? ( (lv_move_1_0= ruleMove ) ) ( (lv_react_2_0= ruleReaction ) )? (otherlv_3= 'else' ( (lv_elsemove_4_0= ruleMove ) ) ( (lv_elsereact_5_0= ruleReaction ) )? )? )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2226:1: ( ( (lv_guard_0_0= ruleGuard ) )? ( (lv_move_1_0= ruleMove ) ) ( (lv_react_2_0= ruleReaction ) )? (otherlv_3= 'else' ( (lv_elsemove_4_0= ruleMove ) ) ( (lv_elsereact_5_0= ruleReaction ) )? )? )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2226:2: ( (lv_guard_0_0= ruleGuard ) )? ( (lv_move_1_0= ruleMove ) ) ( (lv_react_2_0= ruleReaction ) )? (otherlv_3= 'else' ( (lv_elsemove_4_0= ruleMove ) ) ( (lv_elsereact_5_0= ruleReaction ) )? )?
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2226:2: ( (lv_guard_0_0= ruleGuard ) )?
            int alt33=2;
            int LA33_0 = input.LA(1);

            if ( (LA33_0==54) ) {
                alt33=1;
            }
            switch (alt33) {
                case 1 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2227:1: (lv_guard_0_0= ruleGuard )
                    {
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2227:1: (lv_guard_0_0= ruleGuard )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2228:3: lv_guard_0_0= ruleGuard
                    {
                     
                    	        newCompositeNode(grammarAccess.getPlanActionAccess().getGuardGuardParserRuleCall_0_0()); 
                    	    
                    pushFollow(FOLLOW_ruleGuard_in_rulePlanAction4885);
                    lv_guard_0_0=ruleGuard();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getPlanActionRule());
                    	        }
                           		set(
                           			current, 
                           			"guard",
                            		lv_guard_0_0, 
                            		"Guard");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }
                    break;

            }

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2244:3: ( (lv_move_1_0= ruleMove ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2245:1: (lv_move_1_0= ruleMove )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2245:1: (lv_move_1_0= ruleMove )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2246:3: lv_move_1_0= ruleMove
            {
             
            	        newCompositeNode(grammarAccess.getPlanActionAccess().getMoveMoveParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_ruleMove_in_rulePlanAction4907);
            lv_move_1_0=ruleMove();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getPlanActionRule());
            	        }
                   		set(
                   			current, 
                   			"move",
                    		lv_move_1_0, 
                    		"Move");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2262:2: ( (lv_react_2_0= ruleReaction ) )?
            int alt34=2;
            int LA34_0 = input.LA(1);

            if ( (LA34_0==113) ) {
                alt34=1;
            }
            switch (alt34) {
                case 1 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2263:1: (lv_react_2_0= ruleReaction )
                    {
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2263:1: (lv_react_2_0= ruleReaction )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2264:3: lv_react_2_0= ruleReaction
                    {
                     
                    	        newCompositeNode(grammarAccess.getPlanActionAccess().getReactReactionParserRuleCall_2_0()); 
                    	    
                    pushFollow(FOLLOW_ruleReaction_in_rulePlanAction4928);
                    lv_react_2_0=ruleReaction();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getPlanActionRule());
                    	        }
                           		set(
                           			current, 
                           			"react",
                            		lv_react_2_0, 
                            		"Reaction");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }
                    break;

            }

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2280:3: (otherlv_3= 'else' ( (lv_elsemove_4_0= ruleMove ) ) ( (lv_elsereact_5_0= ruleReaction ) )? )?
            int alt36=2;
            int LA36_0 = input.LA(1);

            if ( (LA36_0==53) ) {
                alt36=1;
            }
            switch (alt36) {
                case 1 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2280:5: otherlv_3= 'else' ( (lv_elsemove_4_0= ruleMove ) ) ( (lv_elsereact_5_0= ruleReaction ) )?
                    {
                    otherlv_3=(Token)match(input,53,FOLLOW_53_in_rulePlanAction4942); 

                        	newLeafNode(otherlv_3, grammarAccess.getPlanActionAccess().getElseKeyword_3_0());
                        
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2284:1: ( (lv_elsemove_4_0= ruleMove ) )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2285:1: (lv_elsemove_4_0= ruleMove )
                    {
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2285:1: (lv_elsemove_4_0= ruleMove )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2286:3: lv_elsemove_4_0= ruleMove
                    {
                     
                    	        newCompositeNode(grammarAccess.getPlanActionAccess().getElsemoveMoveParserRuleCall_3_1_0()); 
                    	    
                    pushFollow(FOLLOW_ruleMove_in_rulePlanAction4963);
                    lv_elsemove_4_0=ruleMove();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getPlanActionRule());
                    	        }
                           		set(
                           			current, 
                           			"elsemove",
                            		lv_elsemove_4_0, 
                            		"Move");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2302:2: ( (lv_elsereact_5_0= ruleReaction ) )?
                    int alt35=2;
                    int LA35_0 = input.LA(1);

                    if ( (LA35_0==113) ) {
                        alt35=1;
                    }
                    switch (alt35) {
                        case 1 :
                            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2303:1: (lv_elsereact_5_0= ruleReaction )
                            {
                            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2303:1: (lv_elsereact_5_0= ruleReaction )
                            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2304:3: lv_elsereact_5_0= ruleReaction
                            {
                             
                            	        newCompositeNode(grammarAccess.getPlanActionAccess().getElsereactReactionParserRuleCall_3_2_0()); 
                            	    
                            pushFollow(FOLLOW_ruleReaction_in_rulePlanAction4984);
                            lv_elsereact_5_0=ruleReaction();

                            state._fsp--;


                            	        if (current==null) {
                            	            current = createModelElementForParent(grammarAccess.getPlanActionRule());
                            	        }
                                   		set(
                                   			current, 
                                   			"elsereact",
                                    		lv_elsereact_5_0, 
                                    		"Reaction");
                            	        afterParserOrEnumRuleCall();
                            	    

                            }


                            }
                            break;

                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePlanAction"


    // $ANTLR start "entryRuleGuard"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2328:1: entryRuleGuard returns [EObject current=null] : iv_ruleGuard= ruleGuard EOF ;
    public final EObject entryRuleGuard() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGuard = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2329:2: (iv_ruleGuard= ruleGuard EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2330:2: iv_ruleGuard= ruleGuard EOF
            {
             newCompositeNode(grammarAccess.getGuardRule()); 
            pushFollow(FOLLOW_ruleGuard_in_entryRuleGuard5023);
            iv_ruleGuard=ruleGuard();

            state._fsp--;

             current =iv_ruleGuard; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleGuard5033); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGuard"


    // $ANTLR start "ruleGuard"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2337:1: ruleGuard returns [EObject current=null] : ( ( (lv_name_0_0= '[' ) ) ( (lv_not_1_0= 'not' ) )? ( (lv_guardspec_2_0= ruleGuardPredicate ) ) otherlv_3= ']' ) ;
    public final EObject ruleGuard() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token lv_not_1_0=null;
        Token otherlv_3=null;
        EObject lv_guardspec_2_0 = null;


         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2340:28: ( ( ( (lv_name_0_0= '[' ) ) ( (lv_not_1_0= 'not' ) )? ( (lv_guardspec_2_0= ruleGuardPredicate ) ) otherlv_3= ']' ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2341:1: ( ( (lv_name_0_0= '[' ) ) ( (lv_not_1_0= 'not' ) )? ( (lv_guardspec_2_0= ruleGuardPredicate ) ) otherlv_3= ']' )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2341:1: ( ( (lv_name_0_0= '[' ) ) ( (lv_not_1_0= 'not' ) )? ( (lv_guardspec_2_0= ruleGuardPredicate ) ) otherlv_3= ']' )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2341:2: ( (lv_name_0_0= '[' ) ) ( (lv_not_1_0= 'not' ) )? ( (lv_guardspec_2_0= ruleGuardPredicate ) ) otherlv_3= ']'
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2341:2: ( (lv_name_0_0= '[' ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2342:1: (lv_name_0_0= '[' )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2342:1: (lv_name_0_0= '[' )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2343:3: lv_name_0_0= '['
            {
            lv_name_0_0=(Token)match(input,54,FOLLOW_54_in_ruleGuard5076); 

                    newLeafNode(lv_name_0_0, grammarAccess.getGuardAccess().getNameLeftSquareBracketKeyword_0_0());
                

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getGuardRule());
            	        }
                   		setWithLastConsumed(current, "name", lv_name_0_0, "[");
            	    

            }


            }

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2356:2: ( (lv_not_1_0= 'not' ) )?
            int alt37=2;
            int LA37_0 = input.LA(1);

            if ( (LA37_0==55) ) {
                alt37=1;
            }
            switch (alt37) {
                case 1 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2357:1: (lv_not_1_0= 'not' )
                    {
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2357:1: (lv_not_1_0= 'not' )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2358:3: lv_not_1_0= 'not'
                    {
                    lv_not_1_0=(Token)match(input,55,FOLLOW_55_in_ruleGuard5107); 

                            newLeafNode(lv_not_1_0, grammarAccess.getGuardAccess().getNotNotKeyword_1_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getGuardRule());
                    	        }
                           		setWithLastConsumed(current, "not", true, "not");
                    	    

                    }


                    }
                    break;

            }

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2371:3: ( (lv_guardspec_2_0= ruleGuardPredicate ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2372:1: (lv_guardspec_2_0= ruleGuardPredicate )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2372:1: (lv_guardspec_2_0= ruleGuardPredicate )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2373:3: lv_guardspec_2_0= ruleGuardPredicate
            {
             
            	        newCompositeNode(grammarAccess.getGuardAccess().getGuardspecGuardPredicateParserRuleCall_2_0()); 
            	    
            pushFollow(FOLLOW_ruleGuardPredicate_in_ruleGuard5142);
            lv_guardspec_2_0=ruleGuardPredicate();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getGuardRule());
            	        }
                   		set(
                   			current, 
                   			"guardspec",
                    		lv_guardspec_2_0, 
                    		"GuardPredicate");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_3=(Token)match(input,56,FOLLOW_56_in_ruleGuard5154); 

                	newLeafNode(otherlv_3, grammarAccess.getGuardAccess().getRightSquareBracketKeyword_3());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGuard"


    // $ANTLR start "entryRuleGuardPredicate"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2401:1: entryRuleGuardPredicate returns [EObject current=null] : iv_ruleGuardPredicate= ruleGuardPredicate EOF ;
    public final EObject entryRuleGuardPredicate() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGuardPredicate = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2402:2: (iv_ruleGuardPredicate= ruleGuardPredicate EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2403:2: iv_ruleGuardPredicate= ruleGuardPredicate EOF
            {
             newCompositeNode(grammarAccess.getGuardPredicateRule()); 
            pushFollow(FOLLOW_ruleGuardPredicate_in_entryRuleGuardPredicate5190);
            iv_ruleGuardPredicate=ruleGuardPredicate();

            state._fsp--;

             current =iv_ruleGuardPredicate; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleGuardPredicate5200); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGuardPredicate"


    // $ANTLR start "ruleGuardPredicate"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2410:1: ruleGuardPredicate returns [EObject current=null] : (this_GuardPredicateStable_0= ruleGuardPredicateStable | this_GuardPredicateRemovable_1= ruleGuardPredicateRemovable ) ;
    public final EObject ruleGuardPredicate() throws RecognitionException {
        EObject current = null;

        EObject this_GuardPredicateStable_0 = null;

        EObject this_GuardPredicateRemovable_1 = null;


         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2413:28: ( (this_GuardPredicateStable_0= ruleGuardPredicateStable | this_GuardPredicateRemovable_1= ruleGuardPredicateRemovable ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2414:1: (this_GuardPredicateStable_0= ruleGuardPredicateStable | this_GuardPredicateRemovable_1= ruleGuardPredicateRemovable )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2414:1: (this_GuardPredicateStable_0= ruleGuardPredicateStable | this_GuardPredicateRemovable_1= ruleGuardPredicateRemovable )
            int alt38=2;
            int LA38_0 = input.LA(1);

            if ( (LA38_0==58) ) {
                alt38=1;
            }
            else if ( (LA38_0==57) ) {
                alt38=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 38, 0, input);

                throw nvae;
            }
            switch (alt38) {
                case 1 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2415:5: this_GuardPredicateStable_0= ruleGuardPredicateStable
                    {
                     
                            newCompositeNode(grammarAccess.getGuardPredicateAccess().getGuardPredicateStableParserRuleCall_0()); 
                        
                    pushFollow(FOLLOW_ruleGuardPredicateStable_in_ruleGuardPredicate5247);
                    this_GuardPredicateStable_0=ruleGuardPredicateStable();

                    state._fsp--;

                     
                            current = this_GuardPredicateStable_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2425:5: this_GuardPredicateRemovable_1= ruleGuardPredicateRemovable
                    {
                     
                            newCompositeNode(grammarAccess.getGuardPredicateAccess().getGuardPredicateRemovableParserRuleCall_1()); 
                        
                    pushFollow(FOLLOW_ruleGuardPredicateRemovable_in_ruleGuardPredicate5274);
                    this_GuardPredicateRemovable_1=ruleGuardPredicateRemovable();

                    state._fsp--;

                     
                            current = this_GuardPredicateRemovable_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGuardPredicate"


    // $ANTLR start "entryRuleGuardPredicateRemovable"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2441:1: entryRuleGuardPredicateRemovable returns [EObject current=null] : iv_ruleGuardPredicateRemovable= ruleGuardPredicateRemovable EOF ;
    public final EObject entryRuleGuardPredicateRemovable() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGuardPredicateRemovable = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2442:2: (iv_ruleGuardPredicateRemovable= ruleGuardPredicateRemovable EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2443:2: iv_ruleGuardPredicateRemovable= ruleGuardPredicateRemovable EOF
            {
             newCompositeNode(grammarAccess.getGuardPredicateRemovableRule()); 
            pushFollow(FOLLOW_ruleGuardPredicateRemovable_in_entryRuleGuardPredicateRemovable5309);
            iv_ruleGuardPredicateRemovable=ruleGuardPredicateRemovable();

            state._fsp--;

             current =iv_ruleGuardPredicateRemovable; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleGuardPredicateRemovable5319); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGuardPredicateRemovable"


    // $ANTLR start "ruleGuardPredicateRemovable"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2450:1: ruleGuardPredicateRemovable returns [EObject current=null] : ( ( (lv_name_0_0= '??' ) ) ( (lv_pred_1_0= rulePTerm ) ) ) ;
    public final EObject ruleGuardPredicateRemovable() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        EObject lv_pred_1_0 = null;


         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2453:28: ( ( ( (lv_name_0_0= '??' ) ) ( (lv_pred_1_0= rulePTerm ) ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2454:1: ( ( (lv_name_0_0= '??' ) ) ( (lv_pred_1_0= rulePTerm ) ) )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2454:1: ( ( (lv_name_0_0= '??' ) ) ( (lv_pred_1_0= rulePTerm ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2454:2: ( (lv_name_0_0= '??' ) ) ( (lv_pred_1_0= rulePTerm ) )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2454:2: ( (lv_name_0_0= '??' ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2455:1: (lv_name_0_0= '??' )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2455:1: (lv_name_0_0= '??' )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2456:3: lv_name_0_0= '??'
            {
            lv_name_0_0=(Token)match(input,57,FOLLOW_57_in_ruleGuardPredicateRemovable5362); 

                    newLeafNode(lv_name_0_0, grammarAccess.getGuardPredicateRemovableAccess().getNameQuestionMarkQuestionMarkKeyword_0_0());
                

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getGuardPredicateRemovableRule());
            	        }
                   		setWithLastConsumed(current, "name", lv_name_0_0, "??");
            	    

            }


            }

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2469:2: ( (lv_pred_1_0= rulePTerm ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2470:1: (lv_pred_1_0= rulePTerm )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2470:1: (lv_pred_1_0= rulePTerm )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2471:3: lv_pred_1_0= rulePTerm
            {
             
            	        newCompositeNode(grammarAccess.getGuardPredicateRemovableAccess().getPredPTermParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_rulePTerm_in_ruleGuardPredicateRemovable5396);
            lv_pred_1_0=rulePTerm();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getGuardPredicateRemovableRule());
            	        }
                   		set(
                   			current, 
                   			"pred",
                    		lv_pred_1_0, 
                    		"PTerm");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGuardPredicateRemovable"


    // $ANTLR start "entryRuleGuardPredicateStable"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2495:1: entryRuleGuardPredicateStable returns [EObject current=null] : iv_ruleGuardPredicateStable= ruleGuardPredicateStable EOF ;
    public final EObject entryRuleGuardPredicateStable() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGuardPredicateStable = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2496:2: (iv_ruleGuardPredicateStable= ruleGuardPredicateStable EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2497:2: iv_ruleGuardPredicateStable= ruleGuardPredicateStable EOF
            {
             newCompositeNode(grammarAccess.getGuardPredicateStableRule()); 
            pushFollow(FOLLOW_ruleGuardPredicateStable_in_entryRuleGuardPredicateStable5432);
            iv_ruleGuardPredicateStable=ruleGuardPredicateStable();

            state._fsp--;

             current =iv_ruleGuardPredicateStable; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleGuardPredicateStable5442); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGuardPredicateStable"


    // $ANTLR start "ruleGuardPredicateStable"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2504:1: ruleGuardPredicateStable returns [EObject current=null] : ( ( (lv_name_0_0= '!?' ) ) ( (lv_pred_1_0= rulePTerm ) ) ) ;
    public final EObject ruleGuardPredicateStable() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        EObject lv_pred_1_0 = null;


         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2507:28: ( ( ( (lv_name_0_0= '!?' ) ) ( (lv_pred_1_0= rulePTerm ) ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2508:1: ( ( (lv_name_0_0= '!?' ) ) ( (lv_pred_1_0= rulePTerm ) ) )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2508:1: ( ( (lv_name_0_0= '!?' ) ) ( (lv_pred_1_0= rulePTerm ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2508:2: ( (lv_name_0_0= '!?' ) ) ( (lv_pred_1_0= rulePTerm ) )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2508:2: ( (lv_name_0_0= '!?' ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2509:1: (lv_name_0_0= '!?' )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2509:1: (lv_name_0_0= '!?' )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2510:3: lv_name_0_0= '!?'
            {
            lv_name_0_0=(Token)match(input,58,FOLLOW_58_in_ruleGuardPredicateStable5485); 

                    newLeafNode(lv_name_0_0, grammarAccess.getGuardPredicateStableAccess().getNameExclamationMarkQuestionMarkKeyword_0_0());
                

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getGuardPredicateStableRule());
            	        }
                   		setWithLastConsumed(current, "name", lv_name_0_0, "!?");
            	    

            }


            }

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2523:2: ( (lv_pred_1_0= rulePTerm ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2524:1: (lv_pred_1_0= rulePTerm )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2524:1: (lv_pred_1_0= rulePTerm )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2525:3: lv_pred_1_0= rulePTerm
            {
             
            	        newCompositeNode(grammarAccess.getGuardPredicateStableAccess().getPredPTermParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_rulePTerm_in_ruleGuardPredicateStable5519);
            lv_pred_1_0=rulePTerm();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getGuardPredicateStableRule());
            	        }
                   		set(
                   			current, 
                   			"pred",
                    		lv_pred_1_0, 
                    		"PTerm");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGuardPredicateStable"


    // $ANTLR start "entryRuleMove"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2549:1: entryRuleMove returns [EObject current=null] : iv_ruleMove= ruleMove EOF ;
    public final EObject entryRuleMove() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMove = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2550:2: (iv_ruleMove= ruleMove EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2551:2: iv_ruleMove= ruleMove EOF
            {
             newCompositeNode(grammarAccess.getMoveRule()); 
            pushFollow(FOLLOW_ruleMove_in_entryRuleMove5555);
            iv_ruleMove=ruleMove();

            state._fsp--;

             current =iv_ruleMove; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleMove5565); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMove"


    // $ANTLR start "ruleMove"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2558:1: ruleMove returns [EObject current=null] : (this_ActionMove_0= ruleActionMove | this_MessageMove_1= ruleMessageMove | this_ExtensionMove_2= ruleExtensionMove | this_BasicMove_3= ruleBasicMove | this_PlanMove_4= rulePlanMove | this_GuardMove_5= ruleGuardMove | this_BasicRobotMove_6= ruleBasicRobotMove ) ;
    public final EObject ruleMove() throws RecognitionException {
        EObject current = null;

        EObject this_ActionMove_0 = null;

        EObject this_MessageMove_1 = null;

        EObject this_ExtensionMove_2 = null;

        EObject this_BasicMove_3 = null;

        EObject this_PlanMove_4 = null;

        EObject this_GuardMove_5 = null;

        EObject this_BasicRobotMove_6 = null;


         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2561:28: ( (this_ActionMove_0= ruleActionMove | this_MessageMove_1= ruleMessageMove | this_ExtensionMove_2= ruleExtensionMove | this_BasicMove_3= ruleBasicMove | this_PlanMove_4= rulePlanMove | this_GuardMove_5= ruleGuardMove | this_BasicRobotMove_6= ruleBasicRobotMove ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2562:1: (this_ActionMove_0= ruleActionMove | this_MessageMove_1= ruleMessageMove | this_ExtensionMove_2= ruleExtensionMove | this_BasicMove_3= ruleBasicMove | this_PlanMove_4= rulePlanMove | this_GuardMove_5= ruleGuardMove | this_BasicRobotMove_6= ruleBasicRobotMove )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2562:1: (this_ActionMove_0= ruleActionMove | this_MessageMove_1= ruleMessageMove | this_ExtensionMove_2= ruleExtensionMove | this_BasicMove_3= ruleBasicMove | this_PlanMove_4= rulePlanMove | this_GuardMove_5= ruleGuardMove | this_BasicRobotMove_6= ruleBasicRobotMove )
            int alt39=7;
            switch ( input.LA(1) ) {
            case 59:
            case 61:
            case 62:
            case 64:
            case 65:
                {
                alt39=1;
                }
                break;
            case 85:
            case 87:
            case 89:
            case 90:
            case 93:
            case 95:
            case 97:
            case 98:
            case 99:
            case 100:
                {
                alt39=2;
                }
                break;
            case 102:
            case 103:
            case 104:
            case 105:
                {
                alt39=3;
                }
                break;
            case 67:
            case 68:
            case 70:
            case 71:
            case 73:
                {
                alt39=4;
                }
                break;
            case 52:
            case 74:
            case 75:
            case 76:
            case 77:
            case 78:
            case 79:
            case 80:
            case 81:
            case 82:
                {
                alt39=5;
                }
                break;
            case 83:
            case 84:
                {
                alt39=6;
                }
                break;
            case 66:
                {
                alt39=7;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 39, 0, input);

                throw nvae;
            }

            switch (alt39) {
                case 1 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2563:5: this_ActionMove_0= ruleActionMove
                    {
                     
                            newCompositeNode(grammarAccess.getMoveAccess().getActionMoveParserRuleCall_0()); 
                        
                    pushFollow(FOLLOW_ruleActionMove_in_ruleMove5612);
                    this_ActionMove_0=ruleActionMove();

                    state._fsp--;

                     
                            current = this_ActionMove_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2573:5: this_MessageMove_1= ruleMessageMove
                    {
                     
                            newCompositeNode(grammarAccess.getMoveAccess().getMessageMoveParserRuleCall_1()); 
                        
                    pushFollow(FOLLOW_ruleMessageMove_in_ruleMove5639);
                    this_MessageMove_1=ruleMessageMove();

                    state._fsp--;

                     
                            current = this_MessageMove_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 3 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2583:5: this_ExtensionMove_2= ruleExtensionMove
                    {
                     
                            newCompositeNode(grammarAccess.getMoveAccess().getExtensionMoveParserRuleCall_2()); 
                        
                    pushFollow(FOLLOW_ruleExtensionMove_in_ruleMove5666);
                    this_ExtensionMove_2=ruleExtensionMove();

                    state._fsp--;

                     
                            current = this_ExtensionMove_2; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 4 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2593:5: this_BasicMove_3= ruleBasicMove
                    {
                     
                            newCompositeNode(grammarAccess.getMoveAccess().getBasicMoveParserRuleCall_3()); 
                        
                    pushFollow(FOLLOW_ruleBasicMove_in_ruleMove5693);
                    this_BasicMove_3=ruleBasicMove();

                    state._fsp--;

                     
                            current = this_BasicMove_3; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 5 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2603:5: this_PlanMove_4= rulePlanMove
                    {
                     
                            newCompositeNode(grammarAccess.getMoveAccess().getPlanMoveParserRuleCall_4()); 
                        
                    pushFollow(FOLLOW_rulePlanMove_in_ruleMove5720);
                    this_PlanMove_4=rulePlanMove();

                    state._fsp--;

                     
                            current = this_PlanMove_4; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 6 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2613:5: this_GuardMove_5= ruleGuardMove
                    {
                     
                            newCompositeNode(grammarAccess.getMoveAccess().getGuardMoveParserRuleCall_5()); 
                        
                    pushFollow(FOLLOW_ruleGuardMove_in_ruleMove5747);
                    this_GuardMove_5=ruleGuardMove();

                    state._fsp--;

                     
                            current = this_GuardMove_5; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 7 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2623:5: this_BasicRobotMove_6= ruleBasicRobotMove
                    {
                     
                            newCompositeNode(grammarAccess.getMoveAccess().getBasicRobotMoveParserRuleCall_6()); 
                        
                    pushFollow(FOLLOW_ruleBasicRobotMove_in_ruleMove5774);
                    this_BasicRobotMove_6=ruleBasicRobotMove();

                    state._fsp--;

                     
                            current = this_BasicRobotMove_6; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMove"


    // $ANTLR start "entryRuleActionMove"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2639:1: entryRuleActionMove returns [EObject current=null] : iv_ruleActionMove= ruleActionMove EOF ;
    public final EObject entryRuleActionMove() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleActionMove = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2640:2: (iv_ruleActionMove= ruleActionMove EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2641:2: iv_ruleActionMove= ruleActionMove EOF
            {
             newCompositeNode(grammarAccess.getActionMoveRule()); 
            pushFollow(FOLLOW_ruleActionMove_in_entryRuleActionMove5809);
            iv_ruleActionMove=ruleActionMove();

            state._fsp--;

             current =iv_ruleActionMove; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleActionMove5819); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleActionMove"


    // $ANTLR start "ruleActionMove"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2648:1: ruleActionMove returns [EObject current=null] : (this_ExecuteAction_0= ruleExecuteAction | this_SolveGoal_1= ruleSolveGoal | this_Demo_2= ruleDemo | this_ActorOp_3= ruleActorOp ) ;
    public final EObject ruleActionMove() throws RecognitionException {
        EObject current = null;

        EObject this_ExecuteAction_0 = null;

        EObject this_SolveGoal_1 = null;

        EObject this_Demo_2 = null;

        EObject this_ActorOp_3 = null;


         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2651:28: ( (this_ExecuteAction_0= ruleExecuteAction | this_SolveGoal_1= ruleSolveGoal | this_Demo_2= ruleDemo | this_ActorOp_3= ruleActorOp ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2652:1: (this_ExecuteAction_0= ruleExecuteAction | this_SolveGoal_1= ruleSolveGoal | this_Demo_2= ruleDemo | this_ActorOp_3= ruleActorOp )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2652:1: (this_ExecuteAction_0= ruleExecuteAction | this_SolveGoal_1= ruleSolveGoal | this_Demo_2= ruleDemo | this_ActorOp_3= ruleActorOp )
            int alt40=4;
            switch ( input.LA(1) ) {
            case 59:
            case 61:
                {
                alt40=1;
                }
                break;
            case 62:
                {
                alt40=2;
                }
                break;
            case 64:
                {
                alt40=3;
                }
                break;
            case 65:
                {
                alt40=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 40, 0, input);

                throw nvae;
            }

            switch (alt40) {
                case 1 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2653:5: this_ExecuteAction_0= ruleExecuteAction
                    {
                     
                            newCompositeNode(grammarAccess.getActionMoveAccess().getExecuteActionParserRuleCall_0()); 
                        
                    pushFollow(FOLLOW_ruleExecuteAction_in_ruleActionMove5866);
                    this_ExecuteAction_0=ruleExecuteAction();

                    state._fsp--;

                     
                            current = this_ExecuteAction_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2663:5: this_SolveGoal_1= ruleSolveGoal
                    {
                     
                            newCompositeNode(grammarAccess.getActionMoveAccess().getSolveGoalParserRuleCall_1()); 
                        
                    pushFollow(FOLLOW_ruleSolveGoal_in_ruleActionMove5893);
                    this_SolveGoal_1=ruleSolveGoal();

                    state._fsp--;

                     
                            current = this_SolveGoal_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 3 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2673:5: this_Demo_2= ruleDemo
                    {
                     
                            newCompositeNode(grammarAccess.getActionMoveAccess().getDemoParserRuleCall_2()); 
                        
                    pushFollow(FOLLOW_ruleDemo_in_ruleActionMove5920);
                    this_Demo_2=ruleDemo();

                    state._fsp--;

                     
                            current = this_Demo_2; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 4 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2683:5: this_ActorOp_3= ruleActorOp
                    {
                     
                            newCompositeNode(grammarAccess.getActionMoveAccess().getActorOpParserRuleCall_3()); 
                        
                    pushFollow(FOLLOW_ruleActorOp_in_ruleActionMove5947);
                    this_ActorOp_3=ruleActorOp();

                    state._fsp--;

                     
                            current = this_ActorOp_3; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleActionMove"


    // $ANTLR start "entryRuleExecuteAction"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2699:1: entryRuleExecuteAction returns [EObject current=null] : iv_ruleExecuteAction= ruleExecuteAction EOF ;
    public final EObject entryRuleExecuteAction() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleExecuteAction = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2700:2: (iv_ruleExecuteAction= ruleExecuteAction EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2701:2: iv_ruleExecuteAction= ruleExecuteAction EOF
            {
             newCompositeNode(grammarAccess.getExecuteActionRule()); 
            pushFollow(FOLLOW_ruleExecuteAction_in_entryRuleExecuteAction5982);
            iv_ruleExecuteAction=ruleExecuteAction();

            state._fsp--;

             current =iv_ruleExecuteAction; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleExecuteAction5992); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExecuteAction"


    // $ANTLR start "ruleExecuteAction"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2708:1: ruleExecuteAction returns [EObject current=null] : ( (otherlv_0= 'execute' ( (otherlv_1= RULE_ID ) ) (otherlv_2= 'with' ( (lv_arg_3_0= rulePHead ) ) )? ) | (otherlv_4= 'dosentence' ( (lv_sentence_5_0= rulePHead ) ) ) ) ;
    public final EObject ruleExecuteAction() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_arg_3_0 = null;

        EObject lv_sentence_5_0 = null;


         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2711:28: ( ( (otherlv_0= 'execute' ( (otherlv_1= RULE_ID ) ) (otherlv_2= 'with' ( (lv_arg_3_0= rulePHead ) ) )? ) | (otherlv_4= 'dosentence' ( (lv_sentence_5_0= rulePHead ) ) ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2712:1: ( (otherlv_0= 'execute' ( (otherlv_1= RULE_ID ) ) (otherlv_2= 'with' ( (lv_arg_3_0= rulePHead ) ) )? ) | (otherlv_4= 'dosentence' ( (lv_sentence_5_0= rulePHead ) ) ) )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2712:1: ( (otherlv_0= 'execute' ( (otherlv_1= RULE_ID ) ) (otherlv_2= 'with' ( (lv_arg_3_0= rulePHead ) ) )? ) | (otherlv_4= 'dosentence' ( (lv_sentence_5_0= rulePHead ) ) ) )
            int alt42=2;
            int LA42_0 = input.LA(1);

            if ( (LA42_0==59) ) {
                alt42=1;
            }
            else if ( (LA42_0==61) ) {
                alt42=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 42, 0, input);

                throw nvae;
            }
            switch (alt42) {
                case 1 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2712:2: (otherlv_0= 'execute' ( (otherlv_1= RULE_ID ) ) (otherlv_2= 'with' ( (lv_arg_3_0= rulePHead ) ) )? )
                    {
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2712:2: (otherlv_0= 'execute' ( (otherlv_1= RULE_ID ) ) (otherlv_2= 'with' ( (lv_arg_3_0= rulePHead ) ) )? )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2712:4: otherlv_0= 'execute' ( (otherlv_1= RULE_ID ) ) (otherlv_2= 'with' ( (lv_arg_3_0= rulePHead ) ) )?
                    {
                    otherlv_0=(Token)match(input,59,FOLLOW_59_in_ruleExecuteAction6030); 

                        	newLeafNode(otherlv_0, grammarAccess.getExecuteActionAccess().getExecuteKeyword_0_0());
                        
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2716:1: ( (otherlv_1= RULE_ID ) )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2717:1: (otherlv_1= RULE_ID )
                    {
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2717:1: (otherlv_1= RULE_ID )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2718:3: otherlv_1= RULE_ID
                    {

                    			if (current==null) {
                    	            current = createModelElement(grammarAccess.getExecuteActionRule());
                    	        }
                            
                    otherlv_1=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleExecuteAction6050); 

                    		newLeafNode(otherlv_1, grammarAccess.getExecuteActionAccess().getActionActionCrossReference_0_1_0()); 
                    	

                    }


                    }

                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2729:2: (otherlv_2= 'with' ( (lv_arg_3_0= rulePHead ) ) )?
                    int alt41=2;
                    int LA41_0 = input.LA(1);

                    if ( (LA41_0==60) ) {
                        alt41=1;
                    }
                    switch (alt41) {
                        case 1 :
                            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2729:4: otherlv_2= 'with' ( (lv_arg_3_0= rulePHead ) )
                            {
                            otherlv_2=(Token)match(input,60,FOLLOW_60_in_ruleExecuteAction6063); 

                                	newLeafNode(otherlv_2, grammarAccess.getExecuteActionAccess().getWithKeyword_0_2_0());
                                
                            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2733:1: ( (lv_arg_3_0= rulePHead ) )
                            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2734:1: (lv_arg_3_0= rulePHead )
                            {
                            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2734:1: (lv_arg_3_0= rulePHead )
                            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2735:3: lv_arg_3_0= rulePHead
                            {
                             
                            	        newCompositeNode(grammarAccess.getExecuteActionAccess().getArgPHeadParserRuleCall_0_2_1_0()); 
                            	    
                            pushFollow(FOLLOW_rulePHead_in_ruleExecuteAction6084);
                            lv_arg_3_0=rulePHead();

                            state._fsp--;


                            	        if (current==null) {
                            	            current = createModelElementForParent(grammarAccess.getExecuteActionRule());
                            	        }
                                   		set(
                                   			current, 
                                   			"arg",
                                    		lv_arg_3_0, 
                                    		"PHead");
                            	        afterParserOrEnumRuleCall();
                            	    

                            }


                            }


                            }
                            break;

                    }


                    }


                    }
                    break;
                case 2 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2752:6: (otherlv_4= 'dosentence' ( (lv_sentence_5_0= rulePHead ) ) )
                    {
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2752:6: (otherlv_4= 'dosentence' ( (lv_sentence_5_0= rulePHead ) ) )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2752:8: otherlv_4= 'dosentence' ( (lv_sentence_5_0= rulePHead ) )
                    {
                    otherlv_4=(Token)match(input,61,FOLLOW_61_in_ruleExecuteAction6106); 

                        	newLeafNode(otherlv_4, grammarAccess.getExecuteActionAccess().getDosentenceKeyword_1_0());
                        
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2756:1: ( (lv_sentence_5_0= rulePHead ) )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2757:1: (lv_sentence_5_0= rulePHead )
                    {
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2757:1: (lv_sentence_5_0= rulePHead )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2758:3: lv_sentence_5_0= rulePHead
                    {
                     
                    	        newCompositeNode(grammarAccess.getExecuteActionAccess().getSentencePHeadParserRuleCall_1_1_0()); 
                    	    
                    pushFollow(FOLLOW_rulePHead_in_ruleExecuteAction6127);
                    lv_sentence_5_0=rulePHead();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getExecuteActionRule());
                    	        }
                           		set(
                           			current, 
                           			"sentence",
                            		lv_sentence_5_0, 
                            		"PHead");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExecuteAction"


    // $ANTLR start "entryRuleSolveGoal"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2782:1: entryRuleSolveGoal returns [EObject current=null] : iv_ruleSolveGoal= ruleSolveGoal EOF ;
    public final EObject entryRuleSolveGoal() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSolveGoal = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2783:2: (iv_ruleSolveGoal= ruleSolveGoal EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2784:2: iv_ruleSolveGoal= ruleSolveGoal EOF
            {
             newCompositeNode(grammarAccess.getSolveGoalRule()); 
            pushFollow(FOLLOW_ruleSolveGoal_in_entryRuleSolveGoal6164);
            iv_ruleSolveGoal=ruleSolveGoal();

            state._fsp--;

             current =iv_ruleSolveGoal; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleSolveGoal6174); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSolveGoal"


    // $ANTLR start "ruleSolveGoal"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2791:1: ruleSolveGoal returns [EObject current=null] : ( ( (lv_name_0_0= 'solve' ) ) ( (lv_goal_1_0= rulePHead ) ) ( (lv_duration_2_0= ruleTimeLimit ) ) (otherlv_3= 'onFailSwitchTo' ( (otherlv_4= RULE_ID ) ) )? ) ;
    public final EObject ruleSolveGoal() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        EObject lv_goal_1_0 = null;

        EObject lv_duration_2_0 = null;


         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2794:28: ( ( ( (lv_name_0_0= 'solve' ) ) ( (lv_goal_1_0= rulePHead ) ) ( (lv_duration_2_0= ruleTimeLimit ) ) (otherlv_3= 'onFailSwitchTo' ( (otherlv_4= RULE_ID ) ) )? ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2795:1: ( ( (lv_name_0_0= 'solve' ) ) ( (lv_goal_1_0= rulePHead ) ) ( (lv_duration_2_0= ruleTimeLimit ) ) (otherlv_3= 'onFailSwitchTo' ( (otherlv_4= RULE_ID ) ) )? )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2795:1: ( ( (lv_name_0_0= 'solve' ) ) ( (lv_goal_1_0= rulePHead ) ) ( (lv_duration_2_0= ruleTimeLimit ) ) (otherlv_3= 'onFailSwitchTo' ( (otherlv_4= RULE_ID ) ) )? )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2795:2: ( (lv_name_0_0= 'solve' ) ) ( (lv_goal_1_0= rulePHead ) ) ( (lv_duration_2_0= ruleTimeLimit ) ) (otherlv_3= 'onFailSwitchTo' ( (otherlv_4= RULE_ID ) ) )?
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2795:2: ( (lv_name_0_0= 'solve' ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2796:1: (lv_name_0_0= 'solve' )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2796:1: (lv_name_0_0= 'solve' )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2797:3: lv_name_0_0= 'solve'
            {
            lv_name_0_0=(Token)match(input,62,FOLLOW_62_in_ruleSolveGoal6217); 

                    newLeafNode(lv_name_0_0, grammarAccess.getSolveGoalAccess().getNameSolveKeyword_0_0());
                

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getSolveGoalRule());
            	        }
                   		setWithLastConsumed(current, "name", lv_name_0_0, "solve");
            	    

            }


            }

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2810:2: ( (lv_goal_1_0= rulePHead ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2811:1: (lv_goal_1_0= rulePHead )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2811:1: (lv_goal_1_0= rulePHead )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2812:3: lv_goal_1_0= rulePHead
            {
             
            	        newCompositeNode(grammarAccess.getSolveGoalAccess().getGoalPHeadParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_rulePHead_in_ruleSolveGoal6251);
            lv_goal_1_0=rulePHead();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getSolveGoalRule());
            	        }
                   		set(
                   			current, 
                   			"goal",
                    		lv_goal_1_0, 
                    		"PHead");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2828:2: ( (lv_duration_2_0= ruleTimeLimit ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2829:1: (lv_duration_2_0= ruleTimeLimit )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2829:1: (lv_duration_2_0= ruleTimeLimit )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2830:3: lv_duration_2_0= ruleTimeLimit
            {
             
            	        newCompositeNode(grammarAccess.getSolveGoalAccess().getDurationTimeLimitParserRuleCall_2_0()); 
            	    
            pushFollow(FOLLOW_ruleTimeLimit_in_ruleSolveGoal6272);
            lv_duration_2_0=ruleTimeLimit();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getSolveGoalRule());
            	        }
                   		set(
                   			current, 
                   			"duration",
                    		lv_duration_2_0, 
                    		"TimeLimit");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2846:2: (otherlv_3= 'onFailSwitchTo' ( (otherlv_4= RULE_ID ) ) )?
            int alt43=2;
            int LA43_0 = input.LA(1);

            if ( (LA43_0==63) ) {
                alt43=1;
            }
            switch (alt43) {
                case 1 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2846:4: otherlv_3= 'onFailSwitchTo' ( (otherlv_4= RULE_ID ) )
                    {
                    otherlv_3=(Token)match(input,63,FOLLOW_63_in_ruleSolveGoal6285); 

                        	newLeafNode(otherlv_3, grammarAccess.getSolveGoalAccess().getOnFailSwitchToKeyword_3_0());
                        
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2850:1: ( (otherlv_4= RULE_ID ) )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2851:1: (otherlv_4= RULE_ID )
                    {
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2851:1: (otherlv_4= RULE_ID )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2852:3: otherlv_4= RULE_ID
                    {

                    			if (current==null) {
                    	            current = createModelElement(grammarAccess.getSolveGoalRule());
                    	        }
                            
                    otherlv_4=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleSolveGoal6305); 

                    		newLeafNode(otherlv_4, grammarAccess.getSolveGoalAccess().getPlanPlanCrossReference_3_1_0()); 
                    	

                    }


                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSolveGoal"


    // $ANTLR start "entryRuleDemo"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2871:1: entryRuleDemo returns [EObject current=null] : iv_ruleDemo= ruleDemo EOF ;
    public final EObject entryRuleDemo() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDemo = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2872:2: (iv_ruleDemo= ruleDemo EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2873:2: iv_ruleDemo= ruleDemo EOF
            {
             newCompositeNode(grammarAccess.getDemoRule()); 
            pushFollow(FOLLOW_ruleDemo_in_entryRuleDemo6343);
            iv_ruleDemo=ruleDemo();

            state._fsp--;

             current =iv_ruleDemo; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleDemo6353); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDemo"


    // $ANTLR start "ruleDemo"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2880:1: ruleDemo returns [EObject current=null] : ( ( (lv_name_0_0= 'demo' ) ) ( (lv_goal_1_0= rulePHead ) ) (otherlv_2= 'onFailSwitchTo' ( (otherlv_3= RULE_ID ) ) )? ) ;
    public final EObject ruleDemo() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        EObject lv_goal_1_0 = null;


         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2883:28: ( ( ( (lv_name_0_0= 'demo' ) ) ( (lv_goal_1_0= rulePHead ) ) (otherlv_2= 'onFailSwitchTo' ( (otherlv_3= RULE_ID ) ) )? ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2884:1: ( ( (lv_name_0_0= 'demo' ) ) ( (lv_goal_1_0= rulePHead ) ) (otherlv_2= 'onFailSwitchTo' ( (otherlv_3= RULE_ID ) ) )? )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2884:1: ( ( (lv_name_0_0= 'demo' ) ) ( (lv_goal_1_0= rulePHead ) ) (otherlv_2= 'onFailSwitchTo' ( (otherlv_3= RULE_ID ) ) )? )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2884:2: ( (lv_name_0_0= 'demo' ) ) ( (lv_goal_1_0= rulePHead ) ) (otherlv_2= 'onFailSwitchTo' ( (otherlv_3= RULE_ID ) ) )?
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2884:2: ( (lv_name_0_0= 'demo' ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2885:1: (lv_name_0_0= 'demo' )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2885:1: (lv_name_0_0= 'demo' )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2886:3: lv_name_0_0= 'demo'
            {
            lv_name_0_0=(Token)match(input,64,FOLLOW_64_in_ruleDemo6396); 

                    newLeafNode(lv_name_0_0, grammarAccess.getDemoAccess().getNameDemoKeyword_0_0());
                

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getDemoRule());
            	        }
                   		setWithLastConsumed(current, "name", lv_name_0_0, "demo");
            	    

            }


            }

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2899:2: ( (lv_goal_1_0= rulePHead ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2900:1: (lv_goal_1_0= rulePHead )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2900:1: (lv_goal_1_0= rulePHead )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2901:3: lv_goal_1_0= rulePHead
            {
             
            	        newCompositeNode(grammarAccess.getDemoAccess().getGoalPHeadParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_rulePHead_in_ruleDemo6430);
            lv_goal_1_0=rulePHead();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getDemoRule());
            	        }
                   		set(
                   			current, 
                   			"goal",
                    		lv_goal_1_0, 
                    		"PHead");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2917:2: (otherlv_2= 'onFailSwitchTo' ( (otherlv_3= RULE_ID ) ) )?
            int alt44=2;
            int LA44_0 = input.LA(1);

            if ( (LA44_0==63) ) {
                alt44=1;
            }
            switch (alt44) {
                case 1 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2917:4: otherlv_2= 'onFailSwitchTo' ( (otherlv_3= RULE_ID ) )
                    {
                    otherlv_2=(Token)match(input,63,FOLLOW_63_in_ruleDemo6443); 

                        	newLeafNode(otherlv_2, grammarAccess.getDemoAccess().getOnFailSwitchToKeyword_2_0());
                        
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2921:1: ( (otherlv_3= RULE_ID ) )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2922:1: (otherlv_3= RULE_ID )
                    {
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2922:1: (otherlv_3= RULE_ID )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2923:3: otherlv_3= RULE_ID
                    {

                    			if (current==null) {
                    	            current = createModelElement(grammarAccess.getDemoRule());
                    	        }
                            
                    otherlv_3=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleDemo6463); 

                    		newLeafNode(otherlv_3, grammarAccess.getDemoAccess().getPlanPlanCrossReference_2_1_0()); 
                    	

                    }


                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDemo"


    // $ANTLR start "entryRuleActorOp"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2942:1: entryRuleActorOp returns [EObject current=null] : iv_ruleActorOp= ruleActorOp EOF ;
    public final EObject entryRuleActorOp() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleActorOp = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2943:2: (iv_ruleActorOp= ruleActorOp EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2944:2: iv_ruleActorOp= ruleActorOp EOF
            {
             newCompositeNode(grammarAccess.getActorOpRule()); 
            pushFollow(FOLLOW_ruleActorOp_in_entryRuleActorOp6501);
            iv_ruleActorOp=ruleActorOp();

            state._fsp--;

             current =iv_ruleActorOp; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleActorOp6511); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleActorOp"


    // $ANTLR start "ruleActorOp"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2951:1: ruleActorOp returns [EObject current=null] : ( ( (lv_name_0_0= 'actorOp' ) ) ( (lv_goal_1_0= rulePHead ) ) (otherlv_2= 'onFailSwitchTo' ( (otherlv_3= RULE_ID ) ) )? ) ;
    public final EObject ruleActorOp() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        EObject lv_goal_1_0 = null;


         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2954:28: ( ( ( (lv_name_0_0= 'actorOp' ) ) ( (lv_goal_1_0= rulePHead ) ) (otherlv_2= 'onFailSwitchTo' ( (otherlv_3= RULE_ID ) ) )? ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2955:1: ( ( (lv_name_0_0= 'actorOp' ) ) ( (lv_goal_1_0= rulePHead ) ) (otherlv_2= 'onFailSwitchTo' ( (otherlv_3= RULE_ID ) ) )? )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2955:1: ( ( (lv_name_0_0= 'actorOp' ) ) ( (lv_goal_1_0= rulePHead ) ) (otherlv_2= 'onFailSwitchTo' ( (otherlv_3= RULE_ID ) ) )? )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2955:2: ( (lv_name_0_0= 'actorOp' ) ) ( (lv_goal_1_0= rulePHead ) ) (otherlv_2= 'onFailSwitchTo' ( (otherlv_3= RULE_ID ) ) )?
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2955:2: ( (lv_name_0_0= 'actorOp' ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2956:1: (lv_name_0_0= 'actorOp' )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2956:1: (lv_name_0_0= 'actorOp' )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2957:3: lv_name_0_0= 'actorOp'
            {
            lv_name_0_0=(Token)match(input,65,FOLLOW_65_in_ruleActorOp6554); 

                    newLeafNode(lv_name_0_0, grammarAccess.getActorOpAccess().getNameActorOpKeyword_0_0());
                

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getActorOpRule());
            	        }
                   		setWithLastConsumed(current, "name", lv_name_0_0, "actorOp");
            	    

            }


            }

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2970:2: ( (lv_goal_1_0= rulePHead ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2971:1: (lv_goal_1_0= rulePHead )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2971:1: (lv_goal_1_0= rulePHead )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2972:3: lv_goal_1_0= rulePHead
            {
             
            	        newCompositeNode(grammarAccess.getActorOpAccess().getGoalPHeadParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_rulePHead_in_ruleActorOp6588);
            lv_goal_1_0=rulePHead();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getActorOpRule());
            	        }
                   		set(
                   			current, 
                   			"goal",
                    		lv_goal_1_0, 
                    		"PHead");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2988:2: (otherlv_2= 'onFailSwitchTo' ( (otherlv_3= RULE_ID ) ) )?
            int alt45=2;
            int LA45_0 = input.LA(1);

            if ( (LA45_0==63) ) {
                alt45=1;
            }
            switch (alt45) {
                case 1 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2988:4: otherlv_2= 'onFailSwitchTo' ( (otherlv_3= RULE_ID ) )
                    {
                    otherlv_2=(Token)match(input,63,FOLLOW_63_in_ruleActorOp6601); 

                        	newLeafNode(otherlv_2, grammarAccess.getActorOpAccess().getOnFailSwitchToKeyword_2_0());
                        
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2992:1: ( (otherlv_3= RULE_ID ) )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2993:1: (otherlv_3= RULE_ID )
                    {
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2993:1: (otherlv_3= RULE_ID )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:2994:3: otherlv_3= RULE_ID
                    {

                    			if (current==null) {
                    	            current = createModelElement(grammarAccess.getActorOpRule());
                    	        }
                            
                    otherlv_3=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleActorOp6621); 

                    		newLeafNode(otherlv_3, grammarAccess.getActorOpAccess().getPlanPlanCrossReference_2_1_0()); 
                    	

                    }


                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleActorOp"


    // $ANTLR start "entryRuleBasicRobotMove"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3013:1: entryRuleBasicRobotMove returns [EObject current=null] : iv_ruleBasicRobotMove= ruleBasicRobotMove EOF ;
    public final EObject entryRuleBasicRobotMove() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBasicRobotMove = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3014:2: (iv_ruleBasicRobotMove= ruleBasicRobotMove EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3015:2: iv_ruleBasicRobotMove= ruleBasicRobotMove EOF
            {
             newCompositeNode(grammarAccess.getBasicRobotMoveRule()); 
            pushFollow(FOLLOW_ruleBasicRobotMove_in_entryRuleBasicRobotMove6659);
            iv_ruleBasicRobotMove=ruleBasicRobotMove();

            state._fsp--;

             current =iv_ruleBasicRobotMove; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleBasicRobotMove6669); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBasicRobotMove"


    // $ANTLR start "ruleBasicRobotMove"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3022:1: ruleBasicRobotMove returns [EObject current=null] : ( (lv_name_0_0= 'dummyRobotMove' ) ) ;
    public final EObject ruleBasicRobotMove() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;

         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3025:28: ( ( (lv_name_0_0= 'dummyRobotMove' ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3026:1: ( (lv_name_0_0= 'dummyRobotMove' ) )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3026:1: ( (lv_name_0_0= 'dummyRobotMove' ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3027:1: (lv_name_0_0= 'dummyRobotMove' )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3027:1: (lv_name_0_0= 'dummyRobotMove' )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3028:3: lv_name_0_0= 'dummyRobotMove'
            {
            lv_name_0_0=(Token)match(input,66,FOLLOW_66_in_ruleBasicRobotMove6711); 

                    newLeafNode(lv_name_0_0, grammarAccess.getBasicRobotMoveAccess().getNameDummyRobotMoveKeyword_0());
                

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getBasicRobotMoveRule());
            	        }
                   		setWithLastConsumed(current, "name", lv_name_0_0, "dummyRobotMove");
            	    

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBasicRobotMove"


    // $ANTLR start "entryRuleBasicMove"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3049:1: entryRuleBasicMove returns [EObject current=null] : iv_ruleBasicMove= ruleBasicMove EOF ;
    public final EObject entryRuleBasicMove() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBasicMove = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3050:2: (iv_ruleBasicMove= ruleBasicMove EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3051:2: iv_ruleBasicMove= ruleBasicMove EOF
            {
             newCompositeNode(grammarAccess.getBasicMoveRule()); 
            pushFollow(FOLLOW_ruleBasicMove_in_entryRuleBasicMove6759);
            iv_ruleBasicMove=ruleBasicMove();

            state._fsp--;

             current =iv_ruleBasicMove; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleBasicMove6769); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBasicMove"


    // $ANTLR start "ruleBasicMove"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3058:1: ruleBasicMove returns [EObject current=null] : (this_Print_0= rulePrint | this_PrintCurrentEvent_1= rulePrintCurrentEvent | this_PrintCurrentMessage_2= rulePrintCurrentMessage | this_MemoCurrentEvent_3= ruleMemoCurrentEvent | this_MemoCurrentMessage_4= ruleMemoCurrentMessage ) ;
    public final EObject ruleBasicMove() throws RecognitionException {
        EObject current = null;

        EObject this_Print_0 = null;

        EObject this_PrintCurrentEvent_1 = null;

        EObject this_PrintCurrentMessage_2 = null;

        EObject this_MemoCurrentEvent_3 = null;

        EObject this_MemoCurrentMessage_4 = null;


         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3061:28: ( (this_Print_0= rulePrint | this_PrintCurrentEvent_1= rulePrintCurrentEvent | this_PrintCurrentMessage_2= rulePrintCurrentMessage | this_MemoCurrentEvent_3= ruleMemoCurrentEvent | this_MemoCurrentMessage_4= ruleMemoCurrentMessage ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3062:1: (this_Print_0= rulePrint | this_PrintCurrentEvent_1= rulePrintCurrentEvent | this_PrintCurrentMessage_2= rulePrintCurrentMessage | this_MemoCurrentEvent_3= ruleMemoCurrentEvent | this_MemoCurrentMessage_4= ruleMemoCurrentMessage )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3062:1: (this_Print_0= rulePrint | this_PrintCurrentEvent_1= rulePrintCurrentEvent | this_PrintCurrentMessage_2= rulePrintCurrentMessage | this_MemoCurrentEvent_3= ruleMemoCurrentEvent | this_MemoCurrentMessage_4= ruleMemoCurrentMessage )
            int alt46=5;
            switch ( input.LA(1) ) {
            case 67:
                {
                alt46=1;
                }
                break;
            case 68:
                {
                alt46=2;
                }
                break;
            case 70:
                {
                alt46=3;
                }
                break;
            case 71:
                {
                alt46=4;
                }
                break;
            case 73:
                {
                alt46=5;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 46, 0, input);

                throw nvae;
            }

            switch (alt46) {
                case 1 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3063:5: this_Print_0= rulePrint
                    {
                     
                            newCompositeNode(grammarAccess.getBasicMoveAccess().getPrintParserRuleCall_0()); 
                        
                    pushFollow(FOLLOW_rulePrint_in_ruleBasicMove6816);
                    this_Print_0=rulePrint();

                    state._fsp--;

                     
                            current = this_Print_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3073:5: this_PrintCurrentEvent_1= rulePrintCurrentEvent
                    {
                     
                            newCompositeNode(grammarAccess.getBasicMoveAccess().getPrintCurrentEventParserRuleCall_1()); 
                        
                    pushFollow(FOLLOW_rulePrintCurrentEvent_in_ruleBasicMove6843);
                    this_PrintCurrentEvent_1=rulePrintCurrentEvent();

                    state._fsp--;

                     
                            current = this_PrintCurrentEvent_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 3 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3083:5: this_PrintCurrentMessage_2= rulePrintCurrentMessage
                    {
                     
                            newCompositeNode(grammarAccess.getBasicMoveAccess().getPrintCurrentMessageParserRuleCall_2()); 
                        
                    pushFollow(FOLLOW_rulePrintCurrentMessage_in_ruleBasicMove6870);
                    this_PrintCurrentMessage_2=rulePrintCurrentMessage();

                    state._fsp--;

                     
                            current = this_PrintCurrentMessage_2; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 4 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3093:5: this_MemoCurrentEvent_3= ruleMemoCurrentEvent
                    {
                     
                            newCompositeNode(grammarAccess.getBasicMoveAccess().getMemoCurrentEventParserRuleCall_3()); 
                        
                    pushFollow(FOLLOW_ruleMemoCurrentEvent_in_ruleBasicMove6897);
                    this_MemoCurrentEvent_3=ruleMemoCurrentEvent();

                    state._fsp--;

                     
                            current = this_MemoCurrentEvent_3; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 5 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3103:5: this_MemoCurrentMessage_4= ruleMemoCurrentMessage
                    {
                     
                            newCompositeNode(grammarAccess.getBasicMoveAccess().getMemoCurrentMessageParserRuleCall_4()); 
                        
                    pushFollow(FOLLOW_ruleMemoCurrentMessage_in_ruleBasicMove6924);
                    this_MemoCurrentMessage_4=ruleMemoCurrentMessage();

                    state._fsp--;

                     
                            current = this_MemoCurrentMessage_4; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBasicMove"


    // $ANTLR start "entryRulePrint"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3119:1: entryRulePrint returns [EObject current=null] : iv_rulePrint= rulePrint EOF ;
    public final EObject entryRulePrint() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePrint = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3120:2: (iv_rulePrint= rulePrint EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3121:2: iv_rulePrint= rulePrint EOF
            {
             newCompositeNode(grammarAccess.getPrintRule()); 
            pushFollow(FOLLOW_rulePrint_in_entryRulePrint6959);
            iv_rulePrint=rulePrint();

            state._fsp--;

             current =iv_rulePrint; 
            match(input,EOF,FOLLOW_EOF_in_entryRulePrint6969); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePrint"


    // $ANTLR start "rulePrint"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3128:1: rulePrint returns [EObject current=null] : ( ( (lv_name_0_0= 'println' ) ) otherlv_1= '(' ( (lv_args_2_0= rulePHead ) ) otherlv_3= ')' ) ;
    public final EObject rulePrint() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_args_2_0 = null;


         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3131:28: ( ( ( (lv_name_0_0= 'println' ) ) otherlv_1= '(' ( (lv_args_2_0= rulePHead ) ) otherlv_3= ')' ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3132:1: ( ( (lv_name_0_0= 'println' ) ) otherlv_1= '(' ( (lv_args_2_0= rulePHead ) ) otherlv_3= ')' )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3132:1: ( ( (lv_name_0_0= 'println' ) ) otherlv_1= '(' ( (lv_args_2_0= rulePHead ) ) otherlv_3= ')' )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3132:2: ( (lv_name_0_0= 'println' ) ) otherlv_1= '(' ( (lv_args_2_0= rulePHead ) ) otherlv_3= ')'
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3132:2: ( (lv_name_0_0= 'println' ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3133:1: (lv_name_0_0= 'println' )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3133:1: (lv_name_0_0= 'println' )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3134:3: lv_name_0_0= 'println'
            {
            lv_name_0_0=(Token)match(input,67,FOLLOW_67_in_rulePrint7012); 

                    newLeafNode(lv_name_0_0, grammarAccess.getPrintAccess().getNamePrintlnKeyword_0_0());
                

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getPrintRule());
            	        }
                   		setWithLastConsumed(current, "name", lv_name_0_0, "println");
            	    

            }


            }

            otherlv_1=(Token)match(input,35,FOLLOW_35_in_rulePrint7037); 

                	newLeafNode(otherlv_1, grammarAccess.getPrintAccess().getLeftParenthesisKeyword_1());
                
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3151:1: ( (lv_args_2_0= rulePHead ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3152:1: (lv_args_2_0= rulePHead )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3152:1: (lv_args_2_0= rulePHead )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3153:3: lv_args_2_0= rulePHead
            {
             
            	        newCompositeNode(grammarAccess.getPrintAccess().getArgsPHeadParserRuleCall_2_0()); 
            	    
            pushFollow(FOLLOW_rulePHead_in_rulePrint7058);
            lv_args_2_0=rulePHead();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getPrintRule());
            	        }
                   		set(
                   			current, 
                   			"args",
                    		lv_args_2_0, 
                    		"PHead");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_3=(Token)match(input,36,FOLLOW_36_in_rulePrint7070); 

                	newLeafNode(otherlv_3, grammarAccess.getPrintAccess().getRightParenthesisKeyword_3());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePrint"


    // $ANTLR start "entryRulePrintCurrentEvent"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3181:1: entryRulePrintCurrentEvent returns [EObject current=null] : iv_rulePrintCurrentEvent= rulePrintCurrentEvent EOF ;
    public final EObject entryRulePrintCurrentEvent() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePrintCurrentEvent = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3182:2: (iv_rulePrintCurrentEvent= rulePrintCurrentEvent EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3183:2: iv_rulePrintCurrentEvent= rulePrintCurrentEvent EOF
            {
             newCompositeNode(grammarAccess.getPrintCurrentEventRule()); 
            pushFollow(FOLLOW_rulePrintCurrentEvent_in_entryRulePrintCurrentEvent7106);
            iv_rulePrintCurrentEvent=rulePrintCurrentEvent();

            state._fsp--;

             current =iv_rulePrintCurrentEvent; 
            match(input,EOF,FOLLOW_EOF_in_entryRulePrintCurrentEvent7116); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePrintCurrentEvent"


    // $ANTLR start "rulePrintCurrentEvent"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3190:1: rulePrintCurrentEvent returns [EObject current=null] : ( ( (lv_name_0_0= 'printCurrentEvent' ) ) ( (lv_memo_1_0= '-memo' ) )? ) ;
    public final EObject rulePrintCurrentEvent() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token lv_memo_1_0=null;

         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3193:28: ( ( ( (lv_name_0_0= 'printCurrentEvent' ) ) ( (lv_memo_1_0= '-memo' ) )? ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3194:1: ( ( (lv_name_0_0= 'printCurrentEvent' ) ) ( (lv_memo_1_0= '-memo' ) )? )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3194:1: ( ( (lv_name_0_0= 'printCurrentEvent' ) ) ( (lv_memo_1_0= '-memo' ) )? )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3194:2: ( (lv_name_0_0= 'printCurrentEvent' ) ) ( (lv_memo_1_0= '-memo' ) )?
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3194:2: ( (lv_name_0_0= 'printCurrentEvent' ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3195:1: (lv_name_0_0= 'printCurrentEvent' )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3195:1: (lv_name_0_0= 'printCurrentEvent' )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3196:3: lv_name_0_0= 'printCurrentEvent'
            {
            lv_name_0_0=(Token)match(input,68,FOLLOW_68_in_rulePrintCurrentEvent7159); 

                    newLeafNode(lv_name_0_0, grammarAccess.getPrintCurrentEventAccess().getNamePrintCurrentEventKeyword_0_0());
                

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getPrintCurrentEventRule());
            	        }
                   		setWithLastConsumed(current, "name", lv_name_0_0, "printCurrentEvent");
            	    

            }


            }

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3209:2: ( (lv_memo_1_0= '-memo' ) )?
            int alt47=2;
            int LA47_0 = input.LA(1);

            if ( (LA47_0==69) ) {
                alt47=1;
            }
            switch (alt47) {
                case 1 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3210:1: (lv_memo_1_0= '-memo' )
                    {
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3210:1: (lv_memo_1_0= '-memo' )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3211:3: lv_memo_1_0= '-memo'
                    {
                    lv_memo_1_0=(Token)match(input,69,FOLLOW_69_in_rulePrintCurrentEvent7190); 

                            newLeafNode(lv_memo_1_0, grammarAccess.getPrintCurrentEventAccess().getMemoMemoKeyword_1_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getPrintCurrentEventRule());
                    	        }
                           		setWithLastConsumed(current, "memo", true, "-memo");
                    	    

                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePrintCurrentEvent"


    // $ANTLR start "entryRulePrintCurrentMessage"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3232:1: entryRulePrintCurrentMessage returns [EObject current=null] : iv_rulePrintCurrentMessage= rulePrintCurrentMessage EOF ;
    public final EObject entryRulePrintCurrentMessage() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePrintCurrentMessage = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3233:2: (iv_rulePrintCurrentMessage= rulePrintCurrentMessage EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3234:2: iv_rulePrintCurrentMessage= rulePrintCurrentMessage EOF
            {
             newCompositeNode(grammarAccess.getPrintCurrentMessageRule()); 
            pushFollow(FOLLOW_rulePrintCurrentMessage_in_entryRulePrintCurrentMessage7240);
            iv_rulePrintCurrentMessage=rulePrintCurrentMessage();

            state._fsp--;

             current =iv_rulePrintCurrentMessage; 
            match(input,EOF,FOLLOW_EOF_in_entryRulePrintCurrentMessage7250); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePrintCurrentMessage"


    // $ANTLR start "rulePrintCurrentMessage"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3241:1: rulePrintCurrentMessage returns [EObject current=null] : ( ( (lv_name_0_0= 'printCurrentMessage' ) ) ( (lv_memo_1_0= '-memo' ) )? ) ;
    public final EObject rulePrintCurrentMessage() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token lv_memo_1_0=null;

         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3244:28: ( ( ( (lv_name_0_0= 'printCurrentMessage' ) ) ( (lv_memo_1_0= '-memo' ) )? ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3245:1: ( ( (lv_name_0_0= 'printCurrentMessage' ) ) ( (lv_memo_1_0= '-memo' ) )? )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3245:1: ( ( (lv_name_0_0= 'printCurrentMessage' ) ) ( (lv_memo_1_0= '-memo' ) )? )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3245:2: ( (lv_name_0_0= 'printCurrentMessage' ) ) ( (lv_memo_1_0= '-memo' ) )?
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3245:2: ( (lv_name_0_0= 'printCurrentMessage' ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3246:1: (lv_name_0_0= 'printCurrentMessage' )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3246:1: (lv_name_0_0= 'printCurrentMessage' )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3247:3: lv_name_0_0= 'printCurrentMessage'
            {
            lv_name_0_0=(Token)match(input,70,FOLLOW_70_in_rulePrintCurrentMessage7293); 

                    newLeafNode(lv_name_0_0, grammarAccess.getPrintCurrentMessageAccess().getNamePrintCurrentMessageKeyword_0_0());
                

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getPrintCurrentMessageRule());
            	        }
                   		setWithLastConsumed(current, "name", lv_name_0_0, "printCurrentMessage");
            	    

            }


            }

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3260:2: ( (lv_memo_1_0= '-memo' ) )?
            int alt48=2;
            int LA48_0 = input.LA(1);

            if ( (LA48_0==69) ) {
                alt48=1;
            }
            switch (alt48) {
                case 1 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3261:1: (lv_memo_1_0= '-memo' )
                    {
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3261:1: (lv_memo_1_0= '-memo' )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3262:3: lv_memo_1_0= '-memo'
                    {
                    lv_memo_1_0=(Token)match(input,69,FOLLOW_69_in_rulePrintCurrentMessage7324); 

                            newLeafNode(lv_memo_1_0, grammarAccess.getPrintCurrentMessageAccess().getMemoMemoKeyword_1_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getPrintCurrentMessageRule());
                    	        }
                           		setWithLastConsumed(current, "memo", true, "-memo");
                    	    

                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePrintCurrentMessage"


    // $ANTLR start "entryRuleMemoCurrentEvent"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3283:1: entryRuleMemoCurrentEvent returns [EObject current=null] : iv_ruleMemoCurrentEvent= ruleMemoCurrentEvent EOF ;
    public final EObject entryRuleMemoCurrentEvent() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMemoCurrentEvent = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3284:2: (iv_ruleMemoCurrentEvent= ruleMemoCurrentEvent EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3285:2: iv_ruleMemoCurrentEvent= ruleMemoCurrentEvent EOF
            {
             newCompositeNode(grammarAccess.getMemoCurrentEventRule()); 
            pushFollow(FOLLOW_ruleMemoCurrentEvent_in_entryRuleMemoCurrentEvent7374);
            iv_ruleMemoCurrentEvent=ruleMemoCurrentEvent();

            state._fsp--;

             current =iv_ruleMemoCurrentEvent; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleMemoCurrentEvent7384); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMemoCurrentEvent"


    // $ANTLR start "ruleMemoCurrentEvent"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3292:1: ruleMemoCurrentEvent returns [EObject current=null] : ( ( (lv_name_0_0= 'memoCurrentEvent' ) ) ( (lv_lastonly_1_0= '-lastonly' ) )? ) ;
    public final EObject ruleMemoCurrentEvent() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token lv_lastonly_1_0=null;

         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3295:28: ( ( ( (lv_name_0_0= 'memoCurrentEvent' ) ) ( (lv_lastonly_1_0= '-lastonly' ) )? ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3296:1: ( ( (lv_name_0_0= 'memoCurrentEvent' ) ) ( (lv_lastonly_1_0= '-lastonly' ) )? )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3296:1: ( ( (lv_name_0_0= 'memoCurrentEvent' ) ) ( (lv_lastonly_1_0= '-lastonly' ) )? )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3296:2: ( (lv_name_0_0= 'memoCurrentEvent' ) ) ( (lv_lastonly_1_0= '-lastonly' ) )?
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3296:2: ( (lv_name_0_0= 'memoCurrentEvent' ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3297:1: (lv_name_0_0= 'memoCurrentEvent' )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3297:1: (lv_name_0_0= 'memoCurrentEvent' )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3298:3: lv_name_0_0= 'memoCurrentEvent'
            {
            lv_name_0_0=(Token)match(input,71,FOLLOW_71_in_ruleMemoCurrentEvent7427); 

                    newLeafNode(lv_name_0_0, grammarAccess.getMemoCurrentEventAccess().getNameMemoCurrentEventKeyword_0_0());
                

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getMemoCurrentEventRule());
            	        }
                   		setWithLastConsumed(current, "name", lv_name_0_0, "memoCurrentEvent");
            	    

            }


            }

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3311:2: ( (lv_lastonly_1_0= '-lastonly' ) )?
            int alt49=2;
            int LA49_0 = input.LA(1);

            if ( (LA49_0==72) ) {
                alt49=1;
            }
            switch (alt49) {
                case 1 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3312:1: (lv_lastonly_1_0= '-lastonly' )
                    {
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3312:1: (lv_lastonly_1_0= '-lastonly' )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3313:3: lv_lastonly_1_0= '-lastonly'
                    {
                    lv_lastonly_1_0=(Token)match(input,72,FOLLOW_72_in_ruleMemoCurrentEvent7458); 

                            newLeafNode(lv_lastonly_1_0, grammarAccess.getMemoCurrentEventAccess().getLastonlyLastonlyKeyword_1_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getMemoCurrentEventRule());
                    	        }
                           		setWithLastConsumed(current, "lastonly", true, "-lastonly");
                    	    

                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMemoCurrentEvent"


    // $ANTLR start "entryRuleMemoCurrentMessage"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3334:1: entryRuleMemoCurrentMessage returns [EObject current=null] : iv_ruleMemoCurrentMessage= ruleMemoCurrentMessage EOF ;
    public final EObject entryRuleMemoCurrentMessage() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMemoCurrentMessage = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3335:2: (iv_ruleMemoCurrentMessage= ruleMemoCurrentMessage EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3336:2: iv_ruleMemoCurrentMessage= ruleMemoCurrentMessage EOF
            {
             newCompositeNode(grammarAccess.getMemoCurrentMessageRule()); 
            pushFollow(FOLLOW_ruleMemoCurrentMessage_in_entryRuleMemoCurrentMessage7508);
            iv_ruleMemoCurrentMessage=ruleMemoCurrentMessage();

            state._fsp--;

             current =iv_ruleMemoCurrentMessage; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleMemoCurrentMessage7518); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMemoCurrentMessage"


    // $ANTLR start "ruleMemoCurrentMessage"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3343:1: ruleMemoCurrentMessage returns [EObject current=null] : ( ( (lv_name_0_0= 'memoCurrentMessage' ) ) ( (lv_lastonly_1_0= '-lastonly' ) )? ) ;
    public final EObject ruleMemoCurrentMessage() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token lv_lastonly_1_0=null;

         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3346:28: ( ( ( (lv_name_0_0= 'memoCurrentMessage' ) ) ( (lv_lastonly_1_0= '-lastonly' ) )? ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3347:1: ( ( (lv_name_0_0= 'memoCurrentMessage' ) ) ( (lv_lastonly_1_0= '-lastonly' ) )? )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3347:1: ( ( (lv_name_0_0= 'memoCurrentMessage' ) ) ( (lv_lastonly_1_0= '-lastonly' ) )? )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3347:2: ( (lv_name_0_0= 'memoCurrentMessage' ) ) ( (lv_lastonly_1_0= '-lastonly' ) )?
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3347:2: ( (lv_name_0_0= 'memoCurrentMessage' ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3348:1: (lv_name_0_0= 'memoCurrentMessage' )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3348:1: (lv_name_0_0= 'memoCurrentMessage' )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3349:3: lv_name_0_0= 'memoCurrentMessage'
            {
            lv_name_0_0=(Token)match(input,73,FOLLOW_73_in_ruleMemoCurrentMessage7561); 

                    newLeafNode(lv_name_0_0, grammarAccess.getMemoCurrentMessageAccess().getNameMemoCurrentMessageKeyword_0_0());
                

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getMemoCurrentMessageRule());
            	        }
                   		setWithLastConsumed(current, "name", lv_name_0_0, "memoCurrentMessage");
            	    

            }


            }

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3362:2: ( (lv_lastonly_1_0= '-lastonly' ) )?
            int alt50=2;
            int LA50_0 = input.LA(1);

            if ( (LA50_0==72) ) {
                alt50=1;
            }
            switch (alt50) {
                case 1 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3363:1: (lv_lastonly_1_0= '-lastonly' )
                    {
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3363:1: (lv_lastonly_1_0= '-lastonly' )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3364:3: lv_lastonly_1_0= '-lastonly'
                    {
                    lv_lastonly_1_0=(Token)match(input,72,FOLLOW_72_in_ruleMemoCurrentMessage7592); 

                            newLeafNode(lv_lastonly_1_0, grammarAccess.getMemoCurrentMessageAccess().getLastonlyLastonlyKeyword_1_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getMemoCurrentMessageRule());
                    	        }
                           		setWithLastConsumed(current, "lastonly", true, "-lastonly");
                    	    

                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMemoCurrentMessage"


    // $ANTLR start "entryRulePlanMove"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3385:1: entryRulePlanMove returns [EObject current=null] : iv_rulePlanMove= rulePlanMove EOF ;
    public final EObject entryRulePlanMove() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePlanMove = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3386:2: (iv_rulePlanMove= rulePlanMove EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3387:2: iv_rulePlanMove= rulePlanMove EOF
            {
             newCompositeNode(grammarAccess.getPlanMoveRule()); 
            pushFollow(FOLLOW_rulePlanMove_in_entryRulePlanMove7642);
            iv_rulePlanMove=rulePlanMove();

            state._fsp--;

             current =iv_rulePlanMove; 
            match(input,EOF,FOLLOW_EOF_in_entryRulePlanMove7652); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePlanMove"


    // $ANTLR start "rulePlanMove"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3394:1: rulePlanMove returns [EObject current=null] : (this_GetActivationEvent_0= ruleGetActivationEvent | this_GetSensedEvent_1= ruleGetSensedEvent | this_LoadPlan_2= ruleLoadPlan | this_RunPlan_3= ruleRunPlan | this_ResumePlan_4= ruleResumePlan | this_RepeatPlan_5= ruleRepeatPlan | this_SwitchPlan_6= ruleSwitchPlan | this_SuspendPlan_7= ruleSuspendPlan | this_EndPlan_8= ruleEndPlan | this_EndActor_9= ruleEndActor ) ;
    public final EObject rulePlanMove() throws RecognitionException {
        EObject current = null;

        EObject this_GetActivationEvent_0 = null;

        EObject this_GetSensedEvent_1 = null;

        EObject this_LoadPlan_2 = null;

        EObject this_RunPlan_3 = null;

        EObject this_ResumePlan_4 = null;

        EObject this_RepeatPlan_5 = null;

        EObject this_SwitchPlan_6 = null;

        EObject this_SuspendPlan_7 = null;

        EObject this_EndPlan_8 = null;

        EObject this_EndActor_9 = null;


         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3397:28: ( (this_GetActivationEvent_0= ruleGetActivationEvent | this_GetSensedEvent_1= ruleGetSensedEvent | this_LoadPlan_2= ruleLoadPlan | this_RunPlan_3= ruleRunPlan | this_ResumePlan_4= ruleResumePlan | this_RepeatPlan_5= ruleRepeatPlan | this_SwitchPlan_6= ruleSwitchPlan | this_SuspendPlan_7= ruleSuspendPlan | this_EndPlan_8= ruleEndPlan | this_EndActor_9= ruleEndActor ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3398:1: (this_GetActivationEvent_0= ruleGetActivationEvent | this_GetSensedEvent_1= ruleGetSensedEvent | this_LoadPlan_2= ruleLoadPlan | this_RunPlan_3= ruleRunPlan | this_ResumePlan_4= ruleResumePlan | this_RepeatPlan_5= ruleRepeatPlan | this_SwitchPlan_6= ruleSwitchPlan | this_SuspendPlan_7= ruleSuspendPlan | this_EndPlan_8= ruleEndPlan | this_EndActor_9= ruleEndActor )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3398:1: (this_GetActivationEvent_0= ruleGetActivationEvent | this_GetSensedEvent_1= ruleGetSensedEvent | this_LoadPlan_2= ruleLoadPlan | this_RunPlan_3= ruleRunPlan | this_ResumePlan_4= ruleResumePlan | this_RepeatPlan_5= ruleRepeatPlan | this_SwitchPlan_6= ruleSwitchPlan | this_SuspendPlan_7= ruleSuspendPlan | this_EndPlan_8= ruleEndPlan | this_EndActor_9= ruleEndActor )
            int alt51=10;
            switch ( input.LA(1) ) {
            case 74:
                {
                alt51=1;
                }
                break;
            case 75:
                {
                alt51=2;
                }
                break;
            case 76:
                {
                alt51=3;
                }
                break;
            case 77:
                {
                alt51=4;
                }
                break;
            case 52:
                {
                alt51=5;
                }
                break;
            case 79:
                {
                alt51=6;
                }
                break;
            case 80:
                {
                alt51=7;
                }
                break;
            case 78:
                {
                alt51=8;
                }
                break;
            case 81:
                {
                alt51=9;
                }
                break;
            case 82:
                {
                alt51=10;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 51, 0, input);

                throw nvae;
            }

            switch (alt51) {
                case 1 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3399:5: this_GetActivationEvent_0= ruleGetActivationEvent
                    {
                     
                            newCompositeNode(grammarAccess.getPlanMoveAccess().getGetActivationEventParserRuleCall_0()); 
                        
                    pushFollow(FOLLOW_ruleGetActivationEvent_in_rulePlanMove7699);
                    this_GetActivationEvent_0=ruleGetActivationEvent();

                    state._fsp--;

                     
                            current = this_GetActivationEvent_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3409:5: this_GetSensedEvent_1= ruleGetSensedEvent
                    {
                     
                            newCompositeNode(grammarAccess.getPlanMoveAccess().getGetSensedEventParserRuleCall_1()); 
                        
                    pushFollow(FOLLOW_ruleGetSensedEvent_in_rulePlanMove7726);
                    this_GetSensedEvent_1=ruleGetSensedEvent();

                    state._fsp--;

                     
                            current = this_GetSensedEvent_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 3 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3419:5: this_LoadPlan_2= ruleLoadPlan
                    {
                     
                            newCompositeNode(grammarAccess.getPlanMoveAccess().getLoadPlanParserRuleCall_2()); 
                        
                    pushFollow(FOLLOW_ruleLoadPlan_in_rulePlanMove7753);
                    this_LoadPlan_2=ruleLoadPlan();

                    state._fsp--;

                     
                            current = this_LoadPlan_2; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 4 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3429:5: this_RunPlan_3= ruleRunPlan
                    {
                     
                            newCompositeNode(grammarAccess.getPlanMoveAccess().getRunPlanParserRuleCall_3()); 
                        
                    pushFollow(FOLLOW_ruleRunPlan_in_rulePlanMove7780);
                    this_RunPlan_3=ruleRunPlan();

                    state._fsp--;

                     
                            current = this_RunPlan_3; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 5 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3439:5: this_ResumePlan_4= ruleResumePlan
                    {
                     
                            newCompositeNode(grammarAccess.getPlanMoveAccess().getResumePlanParserRuleCall_4()); 
                        
                    pushFollow(FOLLOW_ruleResumePlan_in_rulePlanMove7807);
                    this_ResumePlan_4=ruleResumePlan();

                    state._fsp--;

                     
                            current = this_ResumePlan_4; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 6 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3449:5: this_RepeatPlan_5= ruleRepeatPlan
                    {
                     
                            newCompositeNode(grammarAccess.getPlanMoveAccess().getRepeatPlanParserRuleCall_5()); 
                        
                    pushFollow(FOLLOW_ruleRepeatPlan_in_rulePlanMove7834);
                    this_RepeatPlan_5=ruleRepeatPlan();

                    state._fsp--;

                     
                            current = this_RepeatPlan_5; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 7 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3459:5: this_SwitchPlan_6= ruleSwitchPlan
                    {
                     
                            newCompositeNode(grammarAccess.getPlanMoveAccess().getSwitchPlanParserRuleCall_6()); 
                        
                    pushFollow(FOLLOW_ruleSwitchPlan_in_rulePlanMove7861);
                    this_SwitchPlan_6=ruleSwitchPlan();

                    state._fsp--;

                     
                            current = this_SwitchPlan_6; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 8 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3469:5: this_SuspendPlan_7= ruleSuspendPlan
                    {
                     
                            newCompositeNode(grammarAccess.getPlanMoveAccess().getSuspendPlanParserRuleCall_7()); 
                        
                    pushFollow(FOLLOW_ruleSuspendPlan_in_rulePlanMove7888);
                    this_SuspendPlan_7=ruleSuspendPlan();

                    state._fsp--;

                     
                            current = this_SuspendPlan_7; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 9 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3479:5: this_EndPlan_8= ruleEndPlan
                    {
                     
                            newCompositeNode(grammarAccess.getPlanMoveAccess().getEndPlanParserRuleCall_8()); 
                        
                    pushFollow(FOLLOW_ruleEndPlan_in_rulePlanMove7915);
                    this_EndPlan_8=ruleEndPlan();

                    state._fsp--;

                     
                            current = this_EndPlan_8; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 10 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3489:5: this_EndActor_9= ruleEndActor
                    {
                     
                            newCompositeNode(grammarAccess.getPlanMoveAccess().getEndActorParserRuleCall_9()); 
                        
                    pushFollow(FOLLOW_ruleEndActor_in_rulePlanMove7942);
                    this_EndActor_9=ruleEndActor();

                    state._fsp--;

                     
                            current = this_EndActor_9; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePlanMove"


    // $ANTLR start "entryRuleGetActivationEvent"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3505:1: entryRuleGetActivationEvent returns [EObject current=null] : iv_ruleGetActivationEvent= ruleGetActivationEvent EOF ;
    public final EObject entryRuleGetActivationEvent() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGetActivationEvent = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3506:2: (iv_ruleGetActivationEvent= ruleGetActivationEvent EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3507:2: iv_ruleGetActivationEvent= ruleGetActivationEvent EOF
            {
             newCompositeNode(grammarAccess.getGetActivationEventRule()); 
            pushFollow(FOLLOW_ruleGetActivationEvent_in_entryRuleGetActivationEvent7977);
            iv_ruleGetActivationEvent=ruleGetActivationEvent();

            state._fsp--;

             current =iv_ruleGetActivationEvent; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleGetActivationEvent7987); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGetActivationEvent"


    // $ANTLR start "ruleGetActivationEvent"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3514:1: ruleGetActivationEvent returns [EObject current=null] : ( ( (lv_name_0_0= 'getActivationEvent ' ) ) ( (lv_var_1_0= ruleVariable ) ) ) ;
    public final EObject ruleGetActivationEvent() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        EObject lv_var_1_0 = null;


         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3517:28: ( ( ( (lv_name_0_0= 'getActivationEvent ' ) ) ( (lv_var_1_0= ruleVariable ) ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3518:1: ( ( (lv_name_0_0= 'getActivationEvent ' ) ) ( (lv_var_1_0= ruleVariable ) ) )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3518:1: ( ( (lv_name_0_0= 'getActivationEvent ' ) ) ( (lv_var_1_0= ruleVariable ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3518:2: ( (lv_name_0_0= 'getActivationEvent ' ) ) ( (lv_var_1_0= ruleVariable ) )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3518:2: ( (lv_name_0_0= 'getActivationEvent ' ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3519:1: (lv_name_0_0= 'getActivationEvent ' )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3519:1: (lv_name_0_0= 'getActivationEvent ' )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3520:3: lv_name_0_0= 'getActivationEvent '
            {
            lv_name_0_0=(Token)match(input,74,FOLLOW_74_in_ruleGetActivationEvent8030); 

                    newLeafNode(lv_name_0_0, grammarAccess.getGetActivationEventAccess().getNameGetActivationEventKeyword_0_0());
                

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getGetActivationEventRule());
            	        }
                   		setWithLastConsumed(current, "name", lv_name_0_0, "getActivationEvent ");
            	    

            }


            }

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3533:2: ( (lv_var_1_0= ruleVariable ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3534:1: (lv_var_1_0= ruleVariable )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3534:1: (lv_var_1_0= ruleVariable )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3535:3: lv_var_1_0= ruleVariable
            {
             
            	        newCompositeNode(grammarAccess.getGetActivationEventAccess().getVarVariableParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_ruleVariable_in_ruleGetActivationEvent8064);
            lv_var_1_0=ruleVariable();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getGetActivationEventRule());
            	        }
                   		set(
                   			current, 
                   			"var",
                    		lv_var_1_0, 
                    		"Variable");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGetActivationEvent"


    // $ANTLR start "entryRuleGetSensedEvent"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3559:1: entryRuleGetSensedEvent returns [EObject current=null] : iv_ruleGetSensedEvent= ruleGetSensedEvent EOF ;
    public final EObject entryRuleGetSensedEvent() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGetSensedEvent = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3560:2: (iv_ruleGetSensedEvent= ruleGetSensedEvent EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3561:2: iv_ruleGetSensedEvent= ruleGetSensedEvent EOF
            {
             newCompositeNode(grammarAccess.getGetSensedEventRule()); 
            pushFollow(FOLLOW_ruleGetSensedEvent_in_entryRuleGetSensedEvent8100);
            iv_ruleGetSensedEvent=ruleGetSensedEvent();

            state._fsp--;

             current =iv_ruleGetSensedEvent; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleGetSensedEvent8110); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGetSensedEvent"


    // $ANTLR start "ruleGetSensedEvent"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3568:1: ruleGetSensedEvent returns [EObject current=null] : ( ( (lv_name_0_0= 'getSensedEvent ' ) ) ( (lv_var_1_0= ruleVariable ) ) ) ;
    public final EObject ruleGetSensedEvent() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        EObject lv_var_1_0 = null;


         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3571:28: ( ( ( (lv_name_0_0= 'getSensedEvent ' ) ) ( (lv_var_1_0= ruleVariable ) ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3572:1: ( ( (lv_name_0_0= 'getSensedEvent ' ) ) ( (lv_var_1_0= ruleVariable ) ) )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3572:1: ( ( (lv_name_0_0= 'getSensedEvent ' ) ) ( (lv_var_1_0= ruleVariable ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3572:2: ( (lv_name_0_0= 'getSensedEvent ' ) ) ( (lv_var_1_0= ruleVariable ) )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3572:2: ( (lv_name_0_0= 'getSensedEvent ' ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3573:1: (lv_name_0_0= 'getSensedEvent ' )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3573:1: (lv_name_0_0= 'getSensedEvent ' )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3574:3: lv_name_0_0= 'getSensedEvent '
            {
            lv_name_0_0=(Token)match(input,75,FOLLOW_75_in_ruleGetSensedEvent8153); 

                    newLeafNode(lv_name_0_0, grammarAccess.getGetSensedEventAccess().getNameGetSensedEventKeyword_0_0());
                

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getGetSensedEventRule());
            	        }
                   		setWithLastConsumed(current, "name", lv_name_0_0, "getSensedEvent ");
            	    

            }


            }

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3587:2: ( (lv_var_1_0= ruleVariable ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3588:1: (lv_var_1_0= ruleVariable )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3588:1: (lv_var_1_0= ruleVariable )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3589:3: lv_var_1_0= ruleVariable
            {
             
            	        newCompositeNode(grammarAccess.getGetSensedEventAccess().getVarVariableParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_ruleVariable_in_ruleGetSensedEvent8187);
            lv_var_1_0=ruleVariable();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getGetSensedEventRule());
            	        }
                   		set(
                   			current, 
                   			"var",
                    		lv_var_1_0, 
                    		"Variable");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGetSensedEvent"


    // $ANTLR start "entryRuleLoadPlan"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3613:1: entryRuleLoadPlan returns [EObject current=null] : iv_ruleLoadPlan= ruleLoadPlan EOF ;
    public final EObject entryRuleLoadPlan() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLoadPlan = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3614:2: (iv_ruleLoadPlan= ruleLoadPlan EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3615:2: iv_ruleLoadPlan= ruleLoadPlan EOF
            {
             newCompositeNode(grammarAccess.getLoadPlanRule()); 
            pushFollow(FOLLOW_ruleLoadPlan_in_entryRuleLoadPlan8223);
            iv_ruleLoadPlan=ruleLoadPlan();

            state._fsp--;

             current =iv_ruleLoadPlan; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleLoadPlan8233); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLoadPlan"


    // $ANTLR start "ruleLoadPlan"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3622:1: ruleLoadPlan returns [EObject current=null] : ( ( (lv_name_0_0= 'loadPlan' ) ) ( (lv_fname_1_0= ruleVarOrString ) ) ) ;
    public final EObject ruleLoadPlan() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        EObject lv_fname_1_0 = null;


         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3625:28: ( ( ( (lv_name_0_0= 'loadPlan' ) ) ( (lv_fname_1_0= ruleVarOrString ) ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3626:1: ( ( (lv_name_0_0= 'loadPlan' ) ) ( (lv_fname_1_0= ruleVarOrString ) ) )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3626:1: ( ( (lv_name_0_0= 'loadPlan' ) ) ( (lv_fname_1_0= ruleVarOrString ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3626:2: ( (lv_name_0_0= 'loadPlan' ) ) ( (lv_fname_1_0= ruleVarOrString ) )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3626:2: ( (lv_name_0_0= 'loadPlan' ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3627:1: (lv_name_0_0= 'loadPlan' )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3627:1: (lv_name_0_0= 'loadPlan' )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3628:3: lv_name_0_0= 'loadPlan'
            {
            lv_name_0_0=(Token)match(input,76,FOLLOW_76_in_ruleLoadPlan8276); 

                    newLeafNode(lv_name_0_0, grammarAccess.getLoadPlanAccess().getNameLoadPlanKeyword_0_0());
                

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getLoadPlanRule());
            	        }
                   		setWithLastConsumed(current, "name", lv_name_0_0, "loadPlan");
            	    

            }


            }

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3641:2: ( (lv_fname_1_0= ruleVarOrString ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3642:1: (lv_fname_1_0= ruleVarOrString )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3642:1: (lv_fname_1_0= ruleVarOrString )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3643:3: lv_fname_1_0= ruleVarOrString
            {
             
            	        newCompositeNode(grammarAccess.getLoadPlanAccess().getFnameVarOrStringParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_ruleVarOrString_in_ruleLoadPlan8310);
            lv_fname_1_0=ruleVarOrString();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getLoadPlanRule());
            	        }
                   		set(
                   			current, 
                   			"fname",
                    		lv_fname_1_0, 
                    		"VarOrString");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLoadPlan"


    // $ANTLR start "entryRuleRunPlan"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3667:1: entryRuleRunPlan returns [EObject current=null] : iv_ruleRunPlan= ruleRunPlan EOF ;
    public final EObject entryRuleRunPlan() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRunPlan = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3668:2: (iv_ruleRunPlan= ruleRunPlan EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3669:2: iv_ruleRunPlan= ruleRunPlan EOF
            {
             newCompositeNode(grammarAccess.getRunPlanRule()); 
            pushFollow(FOLLOW_ruleRunPlan_in_entryRuleRunPlan8346);
            iv_ruleRunPlan=ruleRunPlan();

            state._fsp--;

             current =iv_ruleRunPlan; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleRunPlan8356); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRunPlan"


    // $ANTLR start "ruleRunPlan"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3676:1: ruleRunPlan returns [EObject current=null] : ( ( (lv_name_0_0= 'runPlan' ) ) ( (lv_plainid_1_0= ruleVarOrAtomic ) ) ) ;
    public final EObject ruleRunPlan() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        EObject lv_plainid_1_0 = null;


         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3679:28: ( ( ( (lv_name_0_0= 'runPlan' ) ) ( (lv_plainid_1_0= ruleVarOrAtomic ) ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3680:1: ( ( (lv_name_0_0= 'runPlan' ) ) ( (lv_plainid_1_0= ruleVarOrAtomic ) ) )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3680:1: ( ( (lv_name_0_0= 'runPlan' ) ) ( (lv_plainid_1_0= ruleVarOrAtomic ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3680:2: ( (lv_name_0_0= 'runPlan' ) ) ( (lv_plainid_1_0= ruleVarOrAtomic ) )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3680:2: ( (lv_name_0_0= 'runPlan' ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3681:1: (lv_name_0_0= 'runPlan' )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3681:1: (lv_name_0_0= 'runPlan' )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3682:3: lv_name_0_0= 'runPlan'
            {
            lv_name_0_0=(Token)match(input,77,FOLLOW_77_in_ruleRunPlan8399); 

                    newLeafNode(lv_name_0_0, grammarAccess.getRunPlanAccess().getNameRunPlanKeyword_0_0());
                

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getRunPlanRule());
            	        }
                   		setWithLastConsumed(current, "name", lv_name_0_0, "runPlan");
            	    

            }


            }

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3695:2: ( (lv_plainid_1_0= ruleVarOrAtomic ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3696:1: (lv_plainid_1_0= ruleVarOrAtomic )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3696:1: (lv_plainid_1_0= ruleVarOrAtomic )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3697:3: lv_plainid_1_0= ruleVarOrAtomic
            {
             
            	        newCompositeNode(grammarAccess.getRunPlanAccess().getPlainidVarOrAtomicParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_ruleVarOrAtomic_in_ruleRunPlan8433);
            lv_plainid_1_0=ruleVarOrAtomic();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getRunPlanRule());
            	        }
                   		set(
                   			current, 
                   			"plainid",
                    		lv_plainid_1_0, 
                    		"VarOrAtomic");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRunPlan"


    // $ANTLR start "entryRuleResumePlan"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3721:1: entryRuleResumePlan returns [EObject current=null] : iv_ruleResumePlan= ruleResumePlan EOF ;
    public final EObject entryRuleResumePlan() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleResumePlan = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3722:2: (iv_ruleResumePlan= ruleResumePlan EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3723:2: iv_ruleResumePlan= ruleResumePlan EOF
            {
             newCompositeNode(grammarAccess.getResumePlanRule()); 
            pushFollow(FOLLOW_ruleResumePlan_in_entryRuleResumePlan8469);
            iv_ruleResumePlan=ruleResumePlan();

            state._fsp--;

             current =iv_ruleResumePlan; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleResumePlan8479); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleResumePlan"


    // $ANTLR start "ruleResumePlan"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3730:1: ruleResumePlan returns [EObject current=null] : ( (lv_name_0_0= 'resumeLastPlan' ) ) ;
    public final EObject ruleResumePlan() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;

         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3733:28: ( ( (lv_name_0_0= 'resumeLastPlan' ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3734:1: ( (lv_name_0_0= 'resumeLastPlan' ) )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3734:1: ( (lv_name_0_0= 'resumeLastPlan' ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3735:1: (lv_name_0_0= 'resumeLastPlan' )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3735:1: (lv_name_0_0= 'resumeLastPlan' )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3736:3: lv_name_0_0= 'resumeLastPlan'
            {
            lv_name_0_0=(Token)match(input,52,FOLLOW_52_in_ruleResumePlan8521); 

                    newLeafNode(lv_name_0_0, grammarAccess.getResumePlanAccess().getNameResumeLastPlanKeyword_0());
                

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getResumePlanRule());
            	        }
                   		setWithLastConsumed(current, "name", lv_name_0_0, "resumeLastPlan");
            	    

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleResumePlan"


    // $ANTLR start "entryRuleSuspendPlan"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3757:1: entryRuleSuspendPlan returns [EObject current=null] : iv_ruleSuspendPlan= ruleSuspendPlan EOF ;
    public final EObject entryRuleSuspendPlan() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSuspendPlan = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3758:2: (iv_ruleSuspendPlan= ruleSuspendPlan EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3759:2: iv_ruleSuspendPlan= ruleSuspendPlan EOF
            {
             newCompositeNode(grammarAccess.getSuspendPlanRule()); 
            pushFollow(FOLLOW_ruleSuspendPlan_in_entryRuleSuspendPlan8569);
            iv_ruleSuspendPlan=ruleSuspendPlan();

            state._fsp--;

             current =iv_ruleSuspendPlan; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleSuspendPlan8579); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSuspendPlan"


    // $ANTLR start "ruleSuspendPlan"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3766:1: ruleSuspendPlan returns [EObject current=null] : ( (lv_name_0_0= 'suspendLastPlan' ) ) ;
    public final EObject ruleSuspendPlan() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;

         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3769:28: ( ( (lv_name_0_0= 'suspendLastPlan' ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3770:1: ( (lv_name_0_0= 'suspendLastPlan' ) )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3770:1: ( (lv_name_0_0= 'suspendLastPlan' ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3771:1: (lv_name_0_0= 'suspendLastPlan' )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3771:1: (lv_name_0_0= 'suspendLastPlan' )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3772:3: lv_name_0_0= 'suspendLastPlan'
            {
            lv_name_0_0=(Token)match(input,78,FOLLOW_78_in_ruleSuspendPlan8621); 

                    newLeafNode(lv_name_0_0, grammarAccess.getSuspendPlanAccess().getNameSuspendLastPlanKeyword_0());
                

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getSuspendPlanRule());
            	        }
                   		setWithLastConsumed(current, "name", lv_name_0_0, "suspendLastPlan");
            	    

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSuspendPlan"


    // $ANTLR start "entryRuleRepeatPlan"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3793:1: entryRuleRepeatPlan returns [EObject current=null] : iv_ruleRepeatPlan= ruleRepeatPlan EOF ;
    public final EObject entryRuleRepeatPlan() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRepeatPlan = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3794:2: (iv_ruleRepeatPlan= ruleRepeatPlan EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3795:2: iv_ruleRepeatPlan= ruleRepeatPlan EOF
            {
             newCompositeNode(grammarAccess.getRepeatPlanRule()); 
            pushFollow(FOLLOW_ruleRepeatPlan_in_entryRuleRepeatPlan8669);
            iv_ruleRepeatPlan=ruleRepeatPlan();

            state._fsp--;

             current =iv_ruleRepeatPlan; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleRepeatPlan8679); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRepeatPlan"


    // $ANTLR start "ruleRepeatPlan"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3802:1: ruleRepeatPlan returns [EObject current=null] : ( ( (lv_name_0_0= 'repeatPlan' ) ) ( (lv_niter_1_0= RULE_INT ) )? ) ;
    public final EObject ruleRepeatPlan() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token lv_niter_1_0=null;

         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3805:28: ( ( ( (lv_name_0_0= 'repeatPlan' ) ) ( (lv_niter_1_0= RULE_INT ) )? ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3806:1: ( ( (lv_name_0_0= 'repeatPlan' ) ) ( (lv_niter_1_0= RULE_INT ) )? )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3806:1: ( ( (lv_name_0_0= 'repeatPlan' ) ) ( (lv_niter_1_0= RULE_INT ) )? )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3806:2: ( (lv_name_0_0= 'repeatPlan' ) ) ( (lv_niter_1_0= RULE_INT ) )?
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3806:2: ( (lv_name_0_0= 'repeatPlan' ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3807:1: (lv_name_0_0= 'repeatPlan' )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3807:1: (lv_name_0_0= 'repeatPlan' )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3808:3: lv_name_0_0= 'repeatPlan'
            {
            lv_name_0_0=(Token)match(input,79,FOLLOW_79_in_ruleRepeatPlan8722); 

                    newLeafNode(lv_name_0_0, grammarAccess.getRepeatPlanAccess().getNameRepeatPlanKeyword_0_0());
                

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getRepeatPlanRule());
            	        }
                   		setWithLastConsumed(current, "name", lv_name_0_0, "repeatPlan");
            	    

            }


            }

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3821:2: ( (lv_niter_1_0= RULE_INT ) )?
            int alt52=2;
            int LA52_0 = input.LA(1);

            if ( (LA52_0==RULE_INT) ) {
                alt52=1;
            }
            switch (alt52) {
                case 1 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3822:1: (lv_niter_1_0= RULE_INT )
                    {
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3822:1: (lv_niter_1_0= RULE_INT )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3823:3: lv_niter_1_0= RULE_INT
                    {
                    lv_niter_1_0=(Token)match(input,RULE_INT,FOLLOW_RULE_INT_in_ruleRepeatPlan8752); 

                    			newLeafNode(lv_niter_1_0, grammarAccess.getRepeatPlanAccess().getNiterINTTerminalRuleCall_1_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getRepeatPlanRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"niter",
                            		lv_niter_1_0, 
                            		"INT");
                    	    

                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRepeatPlan"


    // $ANTLR start "entryRuleSwitchPlan"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3847:1: entryRuleSwitchPlan returns [EObject current=null] : iv_ruleSwitchPlan= ruleSwitchPlan EOF ;
    public final EObject entryRuleSwitchPlan() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSwitchPlan = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3848:2: (iv_ruleSwitchPlan= ruleSwitchPlan EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3849:2: iv_ruleSwitchPlan= ruleSwitchPlan EOF
            {
             newCompositeNode(grammarAccess.getSwitchPlanRule()); 
            pushFollow(FOLLOW_ruleSwitchPlan_in_entryRuleSwitchPlan8794);
            iv_ruleSwitchPlan=ruleSwitchPlan();

            state._fsp--;

             current =iv_ruleSwitchPlan; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleSwitchPlan8804); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSwitchPlan"


    // $ANTLR start "ruleSwitchPlan"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3856:1: ruleSwitchPlan returns [EObject current=null] : ( ( (lv_name_0_0= 'switchToPlan' ) ) ( (otherlv_1= RULE_ID ) ) ) ;
    public final EObject ruleSwitchPlan() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_1=null;

         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3859:28: ( ( ( (lv_name_0_0= 'switchToPlan' ) ) ( (otherlv_1= RULE_ID ) ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3860:1: ( ( (lv_name_0_0= 'switchToPlan' ) ) ( (otherlv_1= RULE_ID ) ) )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3860:1: ( ( (lv_name_0_0= 'switchToPlan' ) ) ( (otherlv_1= RULE_ID ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3860:2: ( (lv_name_0_0= 'switchToPlan' ) ) ( (otherlv_1= RULE_ID ) )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3860:2: ( (lv_name_0_0= 'switchToPlan' ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3861:1: (lv_name_0_0= 'switchToPlan' )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3861:1: (lv_name_0_0= 'switchToPlan' )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3862:3: lv_name_0_0= 'switchToPlan'
            {
            lv_name_0_0=(Token)match(input,80,FOLLOW_80_in_ruleSwitchPlan8847); 

                    newLeafNode(lv_name_0_0, grammarAccess.getSwitchPlanAccess().getNameSwitchToPlanKeyword_0_0());
                

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getSwitchPlanRule());
            	        }
                   		setWithLastConsumed(current, "name", lv_name_0_0, "switchToPlan");
            	    

            }


            }

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3875:2: ( (otherlv_1= RULE_ID ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3876:1: (otherlv_1= RULE_ID )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3876:1: (otherlv_1= RULE_ID )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3877:3: otherlv_1= RULE_ID
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getSwitchPlanRule());
            	        }
                    
            otherlv_1=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleSwitchPlan8880); 

            		newLeafNode(otherlv_1, grammarAccess.getSwitchPlanAccess().getPlanPlanCrossReference_1_0()); 
            	

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSwitchPlan"


    // $ANTLR start "entryRuleEndPlan"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3896:1: entryRuleEndPlan returns [EObject current=null] : iv_ruleEndPlan= ruleEndPlan EOF ;
    public final EObject entryRuleEndPlan() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEndPlan = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3897:2: (iv_ruleEndPlan= ruleEndPlan EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3898:2: iv_ruleEndPlan= ruleEndPlan EOF
            {
             newCompositeNode(grammarAccess.getEndPlanRule()); 
            pushFollow(FOLLOW_ruleEndPlan_in_entryRuleEndPlan8916);
            iv_ruleEndPlan=ruleEndPlan();

            state._fsp--;

             current =iv_ruleEndPlan; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleEndPlan8926); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEndPlan"


    // $ANTLR start "ruleEndPlan"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3905:1: ruleEndPlan returns [EObject current=null] : ( ( (lv_name_0_0= 'endPlan' ) ) ( (lv_msg_1_0= RULE_STRING ) ) ) ;
    public final EObject ruleEndPlan() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token lv_msg_1_0=null;

         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3908:28: ( ( ( (lv_name_0_0= 'endPlan' ) ) ( (lv_msg_1_0= RULE_STRING ) ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3909:1: ( ( (lv_name_0_0= 'endPlan' ) ) ( (lv_msg_1_0= RULE_STRING ) ) )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3909:1: ( ( (lv_name_0_0= 'endPlan' ) ) ( (lv_msg_1_0= RULE_STRING ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3909:2: ( (lv_name_0_0= 'endPlan' ) ) ( (lv_msg_1_0= RULE_STRING ) )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3909:2: ( (lv_name_0_0= 'endPlan' ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3910:1: (lv_name_0_0= 'endPlan' )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3910:1: (lv_name_0_0= 'endPlan' )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3911:3: lv_name_0_0= 'endPlan'
            {
            lv_name_0_0=(Token)match(input,81,FOLLOW_81_in_ruleEndPlan8969); 

                    newLeafNode(lv_name_0_0, grammarAccess.getEndPlanAccess().getNameEndPlanKeyword_0_0());
                

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getEndPlanRule());
            	        }
                   		setWithLastConsumed(current, "name", lv_name_0_0, "endPlan");
            	    

            }


            }

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3924:2: ( (lv_msg_1_0= RULE_STRING ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3925:1: (lv_msg_1_0= RULE_STRING )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3925:1: (lv_msg_1_0= RULE_STRING )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3926:3: lv_msg_1_0= RULE_STRING
            {
            lv_msg_1_0=(Token)match(input,RULE_STRING,FOLLOW_RULE_STRING_in_ruleEndPlan8999); 

            			newLeafNode(lv_msg_1_0, grammarAccess.getEndPlanAccess().getMsgSTRINGTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getEndPlanRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"msg",
                    		lv_msg_1_0, 
                    		"STRING");
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEndPlan"


    // $ANTLR start "entryRuleEndActor"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3950:1: entryRuleEndActor returns [EObject current=null] : iv_ruleEndActor= ruleEndActor EOF ;
    public final EObject entryRuleEndActor() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEndActor = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3951:2: (iv_ruleEndActor= ruleEndActor EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3952:2: iv_ruleEndActor= ruleEndActor EOF
            {
             newCompositeNode(grammarAccess.getEndActorRule()); 
            pushFollow(FOLLOW_ruleEndActor_in_entryRuleEndActor9040);
            iv_ruleEndActor=ruleEndActor();

            state._fsp--;

             current =iv_ruleEndActor; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleEndActor9050); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEndActor"


    // $ANTLR start "ruleEndActor"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3959:1: ruleEndActor returns [EObject current=null] : ( ( (lv_name_0_0= 'endQActor' ) ) ( (lv_msg_1_0= RULE_STRING ) ) ) ;
    public final EObject ruleEndActor() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token lv_msg_1_0=null;

         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3962:28: ( ( ( (lv_name_0_0= 'endQActor' ) ) ( (lv_msg_1_0= RULE_STRING ) ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3963:1: ( ( (lv_name_0_0= 'endQActor' ) ) ( (lv_msg_1_0= RULE_STRING ) ) )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3963:1: ( ( (lv_name_0_0= 'endQActor' ) ) ( (lv_msg_1_0= RULE_STRING ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3963:2: ( (lv_name_0_0= 'endQActor' ) ) ( (lv_msg_1_0= RULE_STRING ) )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3963:2: ( (lv_name_0_0= 'endQActor' ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3964:1: (lv_name_0_0= 'endQActor' )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3964:1: (lv_name_0_0= 'endQActor' )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3965:3: lv_name_0_0= 'endQActor'
            {
            lv_name_0_0=(Token)match(input,82,FOLLOW_82_in_ruleEndActor9093); 

                    newLeafNode(lv_name_0_0, grammarAccess.getEndActorAccess().getNameEndQActorKeyword_0_0());
                

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getEndActorRule());
            	        }
                   		setWithLastConsumed(current, "name", lv_name_0_0, "endQActor");
            	    

            }


            }

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3978:2: ( (lv_msg_1_0= RULE_STRING ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3979:1: (lv_msg_1_0= RULE_STRING )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3979:1: (lv_msg_1_0= RULE_STRING )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:3980:3: lv_msg_1_0= RULE_STRING
            {
            lv_msg_1_0=(Token)match(input,RULE_STRING,FOLLOW_RULE_STRING_in_ruleEndActor9123); 

            			newLeafNode(lv_msg_1_0, grammarAccess.getEndActorAccess().getMsgSTRINGTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getEndActorRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"msg",
                    		lv_msg_1_0, 
                    		"STRING");
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEndActor"


    // $ANTLR start "entryRuleGuardMove"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4004:1: entryRuleGuardMove returns [EObject current=null] : iv_ruleGuardMove= ruleGuardMove EOF ;
    public final EObject entryRuleGuardMove() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGuardMove = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4005:2: (iv_ruleGuardMove= ruleGuardMove EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4006:2: iv_ruleGuardMove= ruleGuardMove EOF
            {
             newCompositeNode(grammarAccess.getGuardMoveRule()); 
            pushFollow(FOLLOW_ruleGuardMove_in_entryRuleGuardMove9164);
            iv_ruleGuardMove=ruleGuardMove();

            state._fsp--;

             current =iv_ruleGuardMove; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleGuardMove9174); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGuardMove"


    // $ANTLR start "ruleGuardMove"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4013:1: ruleGuardMove returns [EObject current=null] : (this_AddRule_0= ruleAddRule | this_RemoveRule_1= ruleRemoveRule ) ;
    public final EObject ruleGuardMove() throws RecognitionException {
        EObject current = null;

        EObject this_AddRule_0 = null;

        EObject this_RemoveRule_1 = null;


         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4016:28: ( (this_AddRule_0= ruleAddRule | this_RemoveRule_1= ruleRemoveRule ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4017:1: (this_AddRule_0= ruleAddRule | this_RemoveRule_1= ruleRemoveRule )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4017:1: (this_AddRule_0= ruleAddRule | this_RemoveRule_1= ruleRemoveRule )
            int alt53=2;
            int LA53_0 = input.LA(1);

            if ( (LA53_0==83) ) {
                alt53=1;
            }
            else if ( (LA53_0==84) ) {
                alt53=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 53, 0, input);

                throw nvae;
            }
            switch (alt53) {
                case 1 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4018:5: this_AddRule_0= ruleAddRule
                    {
                     
                            newCompositeNode(grammarAccess.getGuardMoveAccess().getAddRuleParserRuleCall_0()); 
                        
                    pushFollow(FOLLOW_ruleAddRule_in_ruleGuardMove9221);
                    this_AddRule_0=ruleAddRule();

                    state._fsp--;

                     
                            current = this_AddRule_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4028:5: this_RemoveRule_1= ruleRemoveRule
                    {
                     
                            newCompositeNode(grammarAccess.getGuardMoveAccess().getRemoveRuleParserRuleCall_1()); 
                        
                    pushFollow(FOLLOW_ruleRemoveRule_in_ruleGuardMove9248);
                    this_RemoveRule_1=ruleRemoveRule();

                    state._fsp--;

                     
                            current = this_RemoveRule_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGuardMove"


    // $ANTLR start "entryRuleAddRule"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4044:1: entryRuleAddRule returns [EObject current=null] : iv_ruleAddRule= ruleAddRule EOF ;
    public final EObject entryRuleAddRule() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAddRule = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4045:2: (iv_ruleAddRule= ruleAddRule EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4046:2: iv_ruleAddRule= ruleAddRule EOF
            {
             newCompositeNode(grammarAccess.getAddRuleRule()); 
            pushFollow(FOLLOW_ruleAddRule_in_entryRuleAddRule9283);
            iv_ruleAddRule=ruleAddRule();

            state._fsp--;

             current =iv_ruleAddRule; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleAddRule9293); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAddRule"


    // $ANTLR start "ruleAddRule"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4053:1: ruleAddRule returns [EObject current=null] : ( ( (lv_name_0_0= 'addRule' ) ) ( (lv_rule_1_0= rulePHead ) ) ) ;
    public final EObject ruleAddRule() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        EObject lv_rule_1_0 = null;


         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4056:28: ( ( ( (lv_name_0_0= 'addRule' ) ) ( (lv_rule_1_0= rulePHead ) ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4057:1: ( ( (lv_name_0_0= 'addRule' ) ) ( (lv_rule_1_0= rulePHead ) ) )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4057:1: ( ( (lv_name_0_0= 'addRule' ) ) ( (lv_rule_1_0= rulePHead ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4057:2: ( (lv_name_0_0= 'addRule' ) ) ( (lv_rule_1_0= rulePHead ) )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4057:2: ( (lv_name_0_0= 'addRule' ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4058:1: (lv_name_0_0= 'addRule' )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4058:1: (lv_name_0_0= 'addRule' )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4059:3: lv_name_0_0= 'addRule'
            {
            lv_name_0_0=(Token)match(input,83,FOLLOW_83_in_ruleAddRule9336); 

                    newLeafNode(lv_name_0_0, grammarAccess.getAddRuleAccess().getNameAddRuleKeyword_0_0());
                

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getAddRuleRule());
            	        }
                   		setWithLastConsumed(current, "name", lv_name_0_0, "addRule");
            	    

            }


            }

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4072:2: ( (lv_rule_1_0= rulePHead ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4073:1: (lv_rule_1_0= rulePHead )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4073:1: (lv_rule_1_0= rulePHead )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4074:3: lv_rule_1_0= rulePHead
            {
             
            	        newCompositeNode(grammarAccess.getAddRuleAccess().getRulePHeadParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_rulePHead_in_ruleAddRule9370);
            lv_rule_1_0=rulePHead();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getAddRuleRule());
            	        }
                   		set(
                   			current, 
                   			"rule",
                    		lv_rule_1_0, 
                    		"PHead");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAddRule"


    // $ANTLR start "entryRuleRemoveRule"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4098:1: entryRuleRemoveRule returns [EObject current=null] : iv_ruleRemoveRule= ruleRemoveRule EOF ;
    public final EObject entryRuleRemoveRule() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRemoveRule = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4099:2: (iv_ruleRemoveRule= ruleRemoveRule EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4100:2: iv_ruleRemoveRule= ruleRemoveRule EOF
            {
             newCompositeNode(grammarAccess.getRemoveRuleRule()); 
            pushFollow(FOLLOW_ruleRemoveRule_in_entryRuleRemoveRule9406);
            iv_ruleRemoveRule=ruleRemoveRule();

            state._fsp--;

             current =iv_ruleRemoveRule; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleRemoveRule9416); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRemoveRule"


    // $ANTLR start "ruleRemoveRule"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4107:1: ruleRemoveRule returns [EObject current=null] : ( ( (lv_name_0_0= 'removeRule' ) ) ( (lv_rule_1_0= rulePHead ) ) ) ;
    public final EObject ruleRemoveRule() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        EObject lv_rule_1_0 = null;


         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4110:28: ( ( ( (lv_name_0_0= 'removeRule' ) ) ( (lv_rule_1_0= rulePHead ) ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4111:1: ( ( (lv_name_0_0= 'removeRule' ) ) ( (lv_rule_1_0= rulePHead ) ) )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4111:1: ( ( (lv_name_0_0= 'removeRule' ) ) ( (lv_rule_1_0= rulePHead ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4111:2: ( (lv_name_0_0= 'removeRule' ) ) ( (lv_rule_1_0= rulePHead ) )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4111:2: ( (lv_name_0_0= 'removeRule' ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4112:1: (lv_name_0_0= 'removeRule' )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4112:1: (lv_name_0_0= 'removeRule' )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4113:3: lv_name_0_0= 'removeRule'
            {
            lv_name_0_0=(Token)match(input,84,FOLLOW_84_in_ruleRemoveRule9459); 

                    newLeafNode(lv_name_0_0, grammarAccess.getRemoveRuleAccess().getNameRemoveRuleKeyword_0_0());
                

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getRemoveRuleRule());
            	        }
                   		setWithLastConsumed(current, "name", lv_name_0_0, "removeRule");
            	    

            }


            }

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4126:2: ( (lv_rule_1_0= rulePHead ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4127:1: (lv_rule_1_0= rulePHead )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4127:1: (lv_rule_1_0= rulePHead )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4128:3: lv_rule_1_0= rulePHead
            {
             
            	        newCompositeNode(grammarAccess.getRemoveRuleAccess().getRulePHeadParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_rulePHead_in_ruleRemoveRule9493);
            lv_rule_1_0=rulePHead();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getRemoveRuleRule());
            	        }
                   		set(
                   			current, 
                   			"rule",
                    		lv_rule_1_0, 
                    		"PHead");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRemoveRule"


    // $ANTLR start "entryRuleMessageMove"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4152:1: entryRuleMessageMove returns [EObject current=null] : iv_ruleMessageMove= ruleMessageMove EOF ;
    public final EObject entryRuleMessageMove() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMessageMove = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4153:2: (iv_ruleMessageMove= ruleMessageMove EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4154:2: iv_ruleMessageMove= ruleMessageMove EOF
            {
             newCompositeNode(grammarAccess.getMessageMoveRule()); 
            pushFollow(FOLLOW_ruleMessageMove_in_entryRuleMessageMove9529);
            iv_ruleMessageMove=ruleMessageMove();

            state._fsp--;

             current =iv_ruleMessageMove; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleMessageMove9539); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMessageMove"


    // $ANTLR start "ruleMessageMove"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4161:1: ruleMessageMove returns [EObject current=null] : (this_SendDispatch_0= ruleSendDispatch | this_SendRequest_1= ruleSendRequest | this_ReplyToCaller_2= ruleReplyToCaller | this_ReceiveMsg_3= ruleReceiveMsg | this_OnReceiveMsg_4= ruleOnReceiveMsg | this_MsgSelect_5= ruleMsgSelect | this_RaiseEvent_6= ruleRaiseEvent | this_SenseEvent_7= ruleSenseEvent | this_MsgSwitch_8= ruleMsgSwitch | this_EventSwitch_9= ruleEventSwitch ) ;
    public final EObject ruleMessageMove() throws RecognitionException {
        EObject current = null;

        EObject this_SendDispatch_0 = null;

        EObject this_SendRequest_1 = null;

        EObject this_ReplyToCaller_2 = null;

        EObject this_ReceiveMsg_3 = null;

        EObject this_OnReceiveMsg_4 = null;

        EObject this_MsgSelect_5 = null;

        EObject this_RaiseEvent_6 = null;

        EObject this_SenseEvent_7 = null;

        EObject this_MsgSwitch_8 = null;

        EObject this_EventSwitch_9 = null;


         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4164:28: ( (this_SendDispatch_0= ruleSendDispatch | this_SendRequest_1= ruleSendRequest | this_ReplyToCaller_2= ruleReplyToCaller | this_ReceiveMsg_3= ruleReceiveMsg | this_OnReceiveMsg_4= ruleOnReceiveMsg | this_MsgSelect_5= ruleMsgSelect | this_RaiseEvent_6= ruleRaiseEvent | this_SenseEvent_7= ruleSenseEvent | this_MsgSwitch_8= ruleMsgSwitch | this_EventSwitch_9= ruleEventSwitch ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4165:1: (this_SendDispatch_0= ruleSendDispatch | this_SendRequest_1= ruleSendRequest | this_ReplyToCaller_2= ruleReplyToCaller | this_ReceiveMsg_3= ruleReceiveMsg | this_OnReceiveMsg_4= ruleOnReceiveMsg | this_MsgSelect_5= ruleMsgSelect | this_RaiseEvent_6= ruleRaiseEvent | this_SenseEvent_7= ruleSenseEvent | this_MsgSwitch_8= ruleMsgSwitch | this_EventSwitch_9= ruleEventSwitch )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4165:1: (this_SendDispatch_0= ruleSendDispatch | this_SendRequest_1= ruleSendRequest | this_ReplyToCaller_2= ruleReplyToCaller | this_ReceiveMsg_3= ruleReceiveMsg | this_OnReceiveMsg_4= ruleOnReceiveMsg | this_MsgSelect_5= ruleMsgSelect | this_RaiseEvent_6= ruleRaiseEvent | this_SenseEvent_7= ruleSenseEvent | this_MsgSwitch_8= ruleMsgSwitch | this_EventSwitch_9= ruleEventSwitch )
            int alt54=10;
            switch ( input.LA(1) ) {
            case 85:
                {
                alt54=1;
                }
                break;
            case 87:
                {
                alt54=2;
                }
                break;
            case 89:
                {
                alt54=3;
                }
                break;
            case 90:
                {
                alt54=4;
                }
                break;
            case 93:
                {
                alt54=5;
                }
                break;
            case 95:
                {
                alt54=6;
                }
                break;
            case 97:
                {
                alt54=7;
                }
                break;
            case 98:
                {
                alt54=8;
                }
                break;
            case 99:
                {
                alt54=9;
                }
                break;
            case 100:
                {
                alt54=10;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 54, 0, input);

                throw nvae;
            }

            switch (alt54) {
                case 1 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4166:5: this_SendDispatch_0= ruleSendDispatch
                    {
                     
                            newCompositeNode(grammarAccess.getMessageMoveAccess().getSendDispatchParserRuleCall_0()); 
                        
                    pushFollow(FOLLOW_ruleSendDispatch_in_ruleMessageMove9586);
                    this_SendDispatch_0=ruleSendDispatch();

                    state._fsp--;

                     
                            current = this_SendDispatch_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4176:5: this_SendRequest_1= ruleSendRequest
                    {
                     
                            newCompositeNode(grammarAccess.getMessageMoveAccess().getSendRequestParserRuleCall_1()); 
                        
                    pushFollow(FOLLOW_ruleSendRequest_in_ruleMessageMove9613);
                    this_SendRequest_1=ruleSendRequest();

                    state._fsp--;

                     
                            current = this_SendRequest_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 3 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4186:5: this_ReplyToCaller_2= ruleReplyToCaller
                    {
                     
                            newCompositeNode(grammarAccess.getMessageMoveAccess().getReplyToCallerParserRuleCall_2()); 
                        
                    pushFollow(FOLLOW_ruleReplyToCaller_in_ruleMessageMove9640);
                    this_ReplyToCaller_2=ruleReplyToCaller();

                    state._fsp--;

                     
                            current = this_ReplyToCaller_2; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 4 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4196:5: this_ReceiveMsg_3= ruleReceiveMsg
                    {
                     
                            newCompositeNode(grammarAccess.getMessageMoveAccess().getReceiveMsgParserRuleCall_3()); 
                        
                    pushFollow(FOLLOW_ruleReceiveMsg_in_ruleMessageMove9667);
                    this_ReceiveMsg_3=ruleReceiveMsg();

                    state._fsp--;

                     
                            current = this_ReceiveMsg_3; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 5 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4206:5: this_OnReceiveMsg_4= ruleOnReceiveMsg
                    {
                     
                            newCompositeNode(grammarAccess.getMessageMoveAccess().getOnReceiveMsgParserRuleCall_4()); 
                        
                    pushFollow(FOLLOW_ruleOnReceiveMsg_in_ruleMessageMove9694);
                    this_OnReceiveMsg_4=ruleOnReceiveMsg();

                    state._fsp--;

                     
                            current = this_OnReceiveMsg_4; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 6 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4216:5: this_MsgSelect_5= ruleMsgSelect
                    {
                     
                            newCompositeNode(grammarAccess.getMessageMoveAccess().getMsgSelectParserRuleCall_5()); 
                        
                    pushFollow(FOLLOW_ruleMsgSelect_in_ruleMessageMove9721);
                    this_MsgSelect_5=ruleMsgSelect();

                    state._fsp--;

                     
                            current = this_MsgSelect_5; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 7 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4226:5: this_RaiseEvent_6= ruleRaiseEvent
                    {
                     
                            newCompositeNode(grammarAccess.getMessageMoveAccess().getRaiseEventParserRuleCall_6()); 
                        
                    pushFollow(FOLLOW_ruleRaiseEvent_in_ruleMessageMove9748);
                    this_RaiseEvent_6=ruleRaiseEvent();

                    state._fsp--;

                     
                            current = this_RaiseEvent_6; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 8 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4236:5: this_SenseEvent_7= ruleSenseEvent
                    {
                     
                            newCompositeNode(grammarAccess.getMessageMoveAccess().getSenseEventParserRuleCall_7()); 
                        
                    pushFollow(FOLLOW_ruleSenseEvent_in_ruleMessageMove9775);
                    this_SenseEvent_7=ruleSenseEvent();

                    state._fsp--;

                     
                            current = this_SenseEvent_7; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 9 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4246:5: this_MsgSwitch_8= ruleMsgSwitch
                    {
                     
                            newCompositeNode(grammarAccess.getMessageMoveAccess().getMsgSwitchParserRuleCall_8()); 
                        
                    pushFollow(FOLLOW_ruleMsgSwitch_in_ruleMessageMove9802);
                    this_MsgSwitch_8=ruleMsgSwitch();

                    state._fsp--;

                     
                            current = this_MsgSwitch_8; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 10 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4256:5: this_EventSwitch_9= ruleEventSwitch
                    {
                     
                            newCompositeNode(grammarAccess.getMessageMoveAccess().getEventSwitchParserRuleCall_9()); 
                        
                    pushFollow(FOLLOW_ruleEventSwitch_in_ruleMessageMove9829);
                    this_EventSwitch_9=ruleEventSwitch();

                    state._fsp--;

                     
                            current = this_EventSwitch_9; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMessageMove"


    // $ANTLR start "entryRuleSendDispatch"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4272:1: entryRuleSendDispatch returns [EObject current=null] : iv_ruleSendDispatch= ruleSendDispatch EOF ;
    public final EObject entryRuleSendDispatch() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSendDispatch = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4273:2: (iv_ruleSendDispatch= ruleSendDispatch EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4274:2: iv_ruleSendDispatch= ruleSendDispatch EOF
            {
             newCompositeNode(grammarAccess.getSendDispatchRule()); 
            pushFollow(FOLLOW_ruleSendDispatch_in_entryRuleSendDispatch9864);
            iv_ruleSendDispatch=ruleSendDispatch();

            state._fsp--;

             current =iv_ruleSendDispatch; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleSendDispatch9874); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSendDispatch"


    // $ANTLR start "ruleSendDispatch"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4281:1: ruleSendDispatch returns [EObject current=null] : ( ( (lv_name_0_0= 'forward' ) ) ( (lv_dest_1_0= ruleVarOrQactor ) ) otherlv_2= '-m' ( (otherlv_3= RULE_ID ) ) otherlv_4= ':' ( (lv_val_5_0= rulePHead ) ) ) ;
    public final EObject ruleSendDispatch() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        EObject lv_dest_1_0 = null;

        EObject lv_val_5_0 = null;


         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4284:28: ( ( ( (lv_name_0_0= 'forward' ) ) ( (lv_dest_1_0= ruleVarOrQactor ) ) otherlv_2= '-m' ( (otherlv_3= RULE_ID ) ) otherlv_4= ':' ( (lv_val_5_0= rulePHead ) ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4285:1: ( ( (lv_name_0_0= 'forward' ) ) ( (lv_dest_1_0= ruleVarOrQactor ) ) otherlv_2= '-m' ( (otherlv_3= RULE_ID ) ) otherlv_4= ':' ( (lv_val_5_0= rulePHead ) ) )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4285:1: ( ( (lv_name_0_0= 'forward' ) ) ( (lv_dest_1_0= ruleVarOrQactor ) ) otherlv_2= '-m' ( (otherlv_3= RULE_ID ) ) otherlv_4= ':' ( (lv_val_5_0= rulePHead ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4285:2: ( (lv_name_0_0= 'forward' ) ) ( (lv_dest_1_0= ruleVarOrQactor ) ) otherlv_2= '-m' ( (otherlv_3= RULE_ID ) ) otherlv_4= ':' ( (lv_val_5_0= rulePHead ) )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4285:2: ( (lv_name_0_0= 'forward' ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4286:1: (lv_name_0_0= 'forward' )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4286:1: (lv_name_0_0= 'forward' )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4287:3: lv_name_0_0= 'forward'
            {
            lv_name_0_0=(Token)match(input,85,FOLLOW_85_in_ruleSendDispatch9917); 

                    newLeafNode(lv_name_0_0, grammarAccess.getSendDispatchAccess().getNameForwardKeyword_0_0());
                

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getSendDispatchRule());
            	        }
                   		setWithLastConsumed(current, "name", lv_name_0_0, "forward");
            	    

            }


            }

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4300:2: ( (lv_dest_1_0= ruleVarOrQactor ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4301:1: (lv_dest_1_0= ruleVarOrQactor )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4301:1: (lv_dest_1_0= ruleVarOrQactor )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4302:3: lv_dest_1_0= ruleVarOrQactor
            {
             
            	        newCompositeNode(grammarAccess.getSendDispatchAccess().getDestVarOrQactorParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_ruleVarOrQactor_in_ruleSendDispatch9951);
            lv_dest_1_0=ruleVarOrQactor();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getSendDispatchRule());
            	        }
                   		set(
                   			current, 
                   			"dest",
                    		lv_dest_1_0, 
                    		"VarOrQactor");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_2=(Token)match(input,86,FOLLOW_86_in_ruleSendDispatch9963); 

                	newLeafNode(otherlv_2, grammarAccess.getSendDispatchAccess().getMKeyword_2());
                
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4322:1: ( (otherlv_3= RULE_ID ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4323:1: (otherlv_3= RULE_ID )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4323:1: (otherlv_3= RULE_ID )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4324:3: otherlv_3= RULE_ID
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getSendDispatchRule());
            	        }
                    
            otherlv_3=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleSendDispatch9983); 

            		newLeafNode(otherlv_3, grammarAccess.getSendDispatchAccess().getMsgrefMessageCrossReference_3_0()); 
            	

            }


            }

            otherlv_4=(Token)match(input,16,FOLLOW_16_in_ruleSendDispatch9995); 

                	newLeafNode(otherlv_4, grammarAccess.getSendDispatchAccess().getColonKeyword_4());
                
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4339:1: ( (lv_val_5_0= rulePHead ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4340:1: (lv_val_5_0= rulePHead )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4340:1: (lv_val_5_0= rulePHead )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4341:3: lv_val_5_0= rulePHead
            {
             
            	        newCompositeNode(grammarAccess.getSendDispatchAccess().getValPHeadParserRuleCall_5_0()); 
            	    
            pushFollow(FOLLOW_rulePHead_in_ruleSendDispatch10016);
            lv_val_5_0=rulePHead();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getSendDispatchRule());
            	        }
                   		set(
                   			current, 
                   			"val",
                    		lv_val_5_0, 
                    		"PHead");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSendDispatch"


    // $ANTLR start "entryRuleSendRequest"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4365:1: entryRuleSendRequest returns [EObject current=null] : iv_ruleSendRequest= ruleSendRequest EOF ;
    public final EObject entryRuleSendRequest() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSendRequest = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4366:2: (iv_ruleSendRequest= ruleSendRequest EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4367:2: iv_ruleSendRequest= ruleSendRequest EOF
            {
             newCompositeNode(grammarAccess.getSendRequestRule()); 
            pushFollow(FOLLOW_ruleSendRequest_in_entryRuleSendRequest10052);
            iv_ruleSendRequest=ruleSendRequest();

            state._fsp--;

             current =iv_ruleSendRequest; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleSendRequest10062); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSendRequest"


    // $ANTLR start "ruleSendRequest"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4374:1: ruleSendRequest returns [EObject current=null] : ( ( (lv_name_0_0= 'demand' ) ) ( (lv_dest_1_0= ruleVarOrQactor ) ) otherlv_2= '-m' ( (otherlv_3= RULE_ID ) ) otherlv_4= ':' ( (lv_val_5_0= rulePHead ) ) (otherlv_6= 'answHandle' )? ) ;
    public final EObject ruleSendRequest() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        EObject lv_dest_1_0 = null;

        EObject lv_val_5_0 = null;


         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4377:28: ( ( ( (lv_name_0_0= 'demand' ) ) ( (lv_dest_1_0= ruleVarOrQactor ) ) otherlv_2= '-m' ( (otherlv_3= RULE_ID ) ) otherlv_4= ':' ( (lv_val_5_0= rulePHead ) ) (otherlv_6= 'answHandle' )? ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4378:1: ( ( (lv_name_0_0= 'demand' ) ) ( (lv_dest_1_0= ruleVarOrQactor ) ) otherlv_2= '-m' ( (otherlv_3= RULE_ID ) ) otherlv_4= ':' ( (lv_val_5_0= rulePHead ) ) (otherlv_6= 'answHandle' )? )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4378:1: ( ( (lv_name_0_0= 'demand' ) ) ( (lv_dest_1_0= ruleVarOrQactor ) ) otherlv_2= '-m' ( (otherlv_3= RULE_ID ) ) otherlv_4= ':' ( (lv_val_5_0= rulePHead ) ) (otherlv_6= 'answHandle' )? )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4378:2: ( (lv_name_0_0= 'demand' ) ) ( (lv_dest_1_0= ruleVarOrQactor ) ) otherlv_2= '-m' ( (otherlv_3= RULE_ID ) ) otherlv_4= ':' ( (lv_val_5_0= rulePHead ) ) (otherlv_6= 'answHandle' )?
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4378:2: ( (lv_name_0_0= 'demand' ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4379:1: (lv_name_0_0= 'demand' )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4379:1: (lv_name_0_0= 'demand' )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4380:3: lv_name_0_0= 'demand'
            {
            lv_name_0_0=(Token)match(input,87,FOLLOW_87_in_ruleSendRequest10105); 

                    newLeafNode(lv_name_0_0, grammarAccess.getSendRequestAccess().getNameDemandKeyword_0_0());
                

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getSendRequestRule());
            	        }
                   		setWithLastConsumed(current, "name", lv_name_0_0, "demand");
            	    

            }


            }

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4393:2: ( (lv_dest_1_0= ruleVarOrQactor ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4394:1: (lv_dest_1_0= ruleVarOrQactor )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4394:1: (lv_dest_1_0= ruleVarOrQactor )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4395:3: lv_dest_1_0= ruleVarOrQactor
            {
             
            	        newCompositeNode(grammarAccess.getSendRequestAccess().getDestVarOrQactorParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_ruleVarOrQactor_in_ruleSendRequest10139);
            lv_dest_1_0=ruleVarOrQactor();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getSendRequestRule());
            	        }
                   		set(
                   			current, 
                   			"dest",
                    		lv_dest_1_0, 
                    		"VarOrQactor");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_2=(Token)match(input,86,FOLLOW_86_in_ruleSendRequest10151); 

                	newLeafNode(otherlv_2, grammarAccess.getSendRequestAccess().getMKeyword_2());
                
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4415:1: ( (otherlv_3= RULE_ID ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4416:1: (otherlv_3= RULE_ID )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4416:1: (otherlv_3= RULE_ID )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4417:3: otherlv_3= RULE_ID
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getSendRequestRule());
            	        }
                    
            otherlv_3=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleSendRequest10171); 

            		newLeafNode(otherlv_3, grammarAccess.getSendRequestAccess().getMsgrefMessageCrossReference_3_0()); 
            	

            }


            }

            otherlv_4=(Token)match(input,16,FOLLOW_16_in_ruleSendRequest10183); 

                	newLeafNode(otherlv_4, grammarAccess.getSendRequestAccess().getColonKeyword_4());
                
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4432:1: ( (lv_val_5_0= rulePHead ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4433:1: (lv_val_5_0= rulePHead )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4433:1: (lv_val_5_0= rulePHead )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4434:3: lv_val_5_0= rulePHead
            {
             
            	        newCompositeNode(grammarAccess.getSendRequestAccess().getValPHeadParserRuleCall_5_0()); 
            	    
            pushFollow(FOLLOW_rulePHead_in_ruleSendRequest10204);
            lv_val_5_0=rulePHead();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getSendRequestRule());
            	        }
                   		set(
                   			current, 
                   			"val",
                    		lv_val_5_0, 
                    		"PHead");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4450:2: (otherlv_6= 'answHandle' )?
            int alt55=2;
            int LA55_0 = input.LA(1);

            if ( (LA55_0==88) ) {
                alt55=1;
            }
            switch (alt55) {
                case 1 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4450:4: otherlv_6= 'answHandle'
                    {
                    otherlv_6=(Token)match(input,88,FOLLOW_88_in_ruleSendRequest10217); 

                        	newLeafNode(otherlv_6, grammarAccess.getSendRequestAccess().getAnswHandleKeyword_6());
                        

                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSendRequest"


    // $ANTLR start "entryRuleReplyToCaller"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4462:1: entryRuleReplyToCaller returns [EObject current=null] : iv_ruleReplyToCaller= ruleReplyToCaller EOF ;
    public final EObject entryRuleReplyToCaller() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleReplyToCaller = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4463:2: (iv_ruleReplyToCaller= ruleReplyToCaller EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4464:2: iv_ruleReplyToCaller= ruleReplyToCaller EOF
            {
             newCompositeNode(grammarAccess.getReplyToCallerRule()); 
            pushFollow(FOLLOW_ruleReplyToCaller_in_entryRuleReplyToCaller10255);
            iv_ruleReplyToCaller=ruleReplyToCaller();

            state._fsp--;

             current =iv_ruleReplyToCaller; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleReplyToCaller10265); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleReplyToCaller"


    // $ANTLR start "ruleReplyToCaller"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4471:1: ruleReplyToCaller returns [EObject current=null] : ( ( (lv_name_0_0= 'replyToCaller' ) ) otherlv_1= '-m' ( (otherlv_2= RULE_ID ) ) otherlv_3= ':' ( (lv_val_4_0= rulePHead ) ) ) ;
    public final EObject ruleReplyToCaller() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        EObject lv_val_4_0 = null;


         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4474:28: ( ( ( (lv_name_0_0= 'replyToCaller' ) ) otherlv_1= '-m' ( (otherlv_2= RULE_ID ) ) otherlv_3= ':' ( (lv_val_4_0= rulePHead ) ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4475:1: ( ( (lv_name_0_0= 'replyToCaller' ) ) otherlv_1= '-m' ( (otherlv_2= RULE_ID ) ) otherlv_3= ':' ( (lv_val_4_0= rulePHead ) ) )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4475:1: ( ( (lv_name_0_0= 'replyToCaller' ) ) otherlv_1= '-m' ( (otherlv_2= RULE_ID ) ) otherlv_3= ':' ( (lv_val_4_0= rulePHead ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4475:2: ( (lv_name_0_0= 'replyToCaller' ) ) otherlv_1= '-m' ( (otherlv_2= RULE_ID ) ) otherlv_3= ':' ( (lv_val_4_0= rulePHead ) )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4475:2: ( (lv_name_0_0= 'replyToCaller' ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4476:1: (lv_name_0_0= 'replyToCaller' )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4476:1: (lv_name_0_0= 'replyToCaller' )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4477:3: lv_name_0_0= 'replyToCaller'
            {
            lv_name_0_0=(Token)match(input,89,FOLLOW_89_in_ruleReplyToCaller10308); 

                    newLeafNode(lv_name_0_0, grammarAccess.getReplyToCallerAccess().getNameReplyToCallerKeyword_0_0());
                

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getReplyToCallerRule());
            	        }
                   		setWithLastConsumed(current, "name", lv_name_0_0, "replyToCaller");
            	    

            }


            }

            otherlv_1=(Token)match(input,86,FOLLOW_86_in_ruleReplyToCaller10333); 

                	newLeafNode(otherlv_1, grammarAccess.getReplyToCallerAccess().getMKeyword_1());
                
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4494:1: ( (otherlv_2= RULE_ID ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4495:1: (otherlv_2= RULE_ID )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4495:1: (otherlv_2= RULE_ID )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4496:3: otherlv_2= RULE_ID
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getReplyToCallerRule());
            	        }
                    
            otherlv_2=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleReplyToCaller10353); 

            		newLeafNode(otherlv_2, grammarAccess.getReplyToCallerAccess().getMsgrefMessageCrossReference_2_0()); 
            	

            }


            }

            otherlv_3=(Token)match(input,16,FOLLOW_16_in_ruleReplyToCaller10365); 

                	newLeafNode(otherlv_3, grammarAccess.getReplyToCallerAccess().getColonKeyword_3());
                
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4511:1: ( (lv_val_4_0= rulePHead ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4512:1: (lv_val_4_0= rulePHead )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4512:1: (lv_val_4_0= rulePHead )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4513:3: lv_val_4_0= rulePHead
            {
             
            	        newCompositeNode(grammarAccess.getReplyToCallerAccess().getValPHeadParserRuleCall_4_0()); 
            	    
            pushFollow(FOLLOW_rulePHead_in_ruleReplyToCaller10386);
            lv_val_4_0=rulePHead();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getReplyToCallerRule());
            	        }
                   		set(
                   			current, 
                   			"val",
                    		lv_val_4_0, 
                    		"PHead");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleReplyToCaller"


    // $ANTLR start "entryRuleReceiveMsg"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4537:1: entryRuleReceiveMsg returns [EObject current=null] : iv_ruleReceiveMsg= ruleReceiveMsg EOF ;
    public final EObject entryRuleReceiveMsg() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleReceiveMsg = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4538:2: (iv_ruleReceiveMsg= ruleReceiveMsg EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4539:2: iv_ruleReceiveMsg= ruleReceiveMsg EOF
            {
             newCompositeNode(grammarAccess.getReceiveMsgRule()); 
            pushFollow(FOLLOW_ruleReceiveMsg_in_entryRuleReceiveMsg10422);
            iv_ruleReceiveMsg=ruleReceiveMsg();

            state._fsp--;

             current =iv_ruleReceiveMsg; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleReceiveMsg10432); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleReceiveMsg"


    // $ANTLR start "ruleReceiveMsg"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4546:1: ruleReceiveMsg returns [EObject current=null] : ( ( (lv_name_0_0= 'receiveMsg' ) ) ( (lv_duration_1_0= ruleTimeLimit ) ) ( (lv_spec_2_0= ruleMsgSpec ) )? ) ;
    public final EObject ruleReceiveMsg() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        EObject lv_duration_1_0 = null;

        EObject lv_spec_2_0 = null;


         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4549:28: ( ( ( (lv_name_0_0= 'receiveMsg' ) ) ( (lv_duration_1_0= ruleTimeLimit ) ) ( (lv_spec_2_0= ruleMsgSpec ) )? ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4550:1: ( ( (lv_name_0_0= 'receiveMsg' ) ) ( (lv_duration_1_0= ruleTimeLimit ) ) ( (lv_spec_2_0= ruleMsgSpec ) )? )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4550:1: ( ( (lv_name_0_0= 'receiveMsg' ) ) ( (lv_duration_1_0= ruleTimeLimit ) ) ( (lv_spec_2_0= ruleMsgSpec ) )? )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4550:2: ( (lv_name_0_0= 'receiveMsg' ) ) ( (lv_duration_1_0= ruleTimeLimit ) ) ( (lv_spec_2_0= ruleMsgSpec ) )?
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4550:2: ( (lv_name_0_0= 'receiveMsg' ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4551:1: (lv_name_0_0= 'receiveMsg' )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4551:1: (lv_name_0_0= 'receiveMsg' )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4552:3: lv_name_0_0= 'receiveMsg'
            {
            lv_name_0_0=(Token)match(input,90,FOLLOW_90_in_ruleReceiveMsg10475); 

                    newLeafNode(lv_name_0_0, grammarAccess.getReceiveMsgAccess().getNameReceiveMsgKeyword_0_0());
                

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getReceiveMsgRule());
            	        }
                   		setWithLastConsumed(current, "name", lv_name_0_0, "receiveMsg");
            	    

            }


            }

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4565:2: ( (lv_duration_1_0= ruleTimeLimit ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4566:1: (lv_duration_1_0= ruleTimeLimit )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4566:1: (lv_duration_1_0= ruleTimeLimit )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4567:3: lv_duration_1_0= ruleTimeLimit
            {
             
            	        newCompositeNode(grammarAccess.getReceiveMsgAccess().getDurationTimeLimitParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_ruleTimeLimit_in_ruleReceiveMsg10509);
            lv_duration_1_0=ruleTimeLimit();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getReceiveMsgRule());
            	        }
                   		set(
                   			current, 
                   			"duration",
                    		lv_duration_1_0, 
                    		"TimeLimit");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4583:2: ( (lv_spec_2_0= ruleMsgSpec ) )?
            int alt56=2;
            int LA56_0 = input.LA(1);

            if ( (LA56_0==86) ) {
                alt56=1;
            }
            switch (alt56) {
                case 1 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4584:1: (lv_spec_2_0= ruleMsgSpec )
                    {
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4584:1: (lv_spec_2_0= ruleMsgSpec )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4585:3: lv_spec_2_0= ruleMsgSpec
                    {
                     
                    	        newCompositeNode(grammarAccess.getReceiveMsgAccess().getSpecMsgSpecParserRuleCall_2_0()); 
                    	    
                    pushFollow(FOLLOW_ruleMsgSpec_in_ruleReceiveMsg10530);
                    lv_spec_2_0=ruleMsgSpec();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getReceiveMsgRule());
                    	        }
                           		set(
                           			current, 
                           			"spec",
                            		lv_spec_2_0, 
                            		"MsgSpec");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleReceiveMsg"


    // $ANTLR start "entryRuleMsgSpec"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4609:1: entryRuleMsgSpec returns [EObject current=null] : iv_ruleMsgSpec= ruleMsgSpec EOF ;
    public final EObject entryRuleMsgSpec() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMsgSpec = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4610:2: (iv_ruleMsgSpec= ruleMsgSpec EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4611:2: iv_ruleMsgSpec= ruleMsgSpec EOF
            {
             newCompositeNode(grammarAccess.getMsgSpecRule()); 
            pushFollow(FOLLOW_ruleMsgSpec_in_entryRuleMsgSpec10567);
            iv_ruleMsgSpec=ruleMsgSpec();

            state._fsp--;

             current =iv_ruleMsgSpec; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleMsgSpec10577); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMsgSpec"


    // $ANTLR start "ruleMsgSpec"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4618:1: ruleMsgSpec returns [EObject current=null] : (otherlv_0= '-m' ( (otherlv_1= RULE_ID ) ) otherlv_2= 'sender' ( (lv_sender_3_0= ruleVarOrAtomic ) ) otherlv_4= 'content' ( (lv_content_5_0= rulePHead ) ) ) ;
    public final EObject ruleMsgSpec() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_sender_3_0 = null;

        EObject lv_content_5_0 = null;


         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4621:28: ( (otherlv_0= '-m' ( (otherlv_1= RULE_ID ) ) otherlv_2= 'sender' ( (lv_sender_3_0= ruleVarOrAtomic ) ) otherlv_4= 'content' ( (lv_content_5_0= rulePHead ) ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4622:1: (otherlv_0= '-m' ( (otherlv_1= RULE_ID ) ) otherlv_2= 'sender' ( (lv_sender_3_0= ruleVarOrAtomic ) ) otherlv_4= 'content' ( (lv_content_5_0= rulePHead ) ) )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4622:1: (otherlv_0= '-m' ( (otherlv_1= RULE_ID ) ) otherlv_2= 'sender' ( (lv_sender_3_0= ruleVarOrAtomic ) ) otherlv_4= 'content' ( (lv_content_5_0= rulePHead ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4622:3: otherlv_0= '-m' ( (otherlv_1= RULE_ID ) ) otherlv_2= 'sender' ( (lv_sender_3_0= ruleVarOrAtomic ) ) otherlv_4= 'content' ( (lv_content_5_0= rulePHead ) )
            {
            otherlv_0=(Token)match(input,86,FOLLOW_86_in_ruleMsgSpec10614); 

                	newLeafNode(otherlv_0, grammarAccess.getMsgSpecAccess().getMKeyword_0());
                
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4626:1: ( (otherlv_1= RULE_ID ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4627:1: (otherlv_1= RULE_ID )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4627:1: (otherlv_1= RULE_ID )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4628:3: otherlv_1= RULE_ID
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getMsgSpecRule());
            	        }
                    
            otherlv_1=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleMsgSpec10634); 

            		newLeafNode(otherlv_1, grammarAccess.getMsgSpecAccess().getMsgMessageCrossReference_1_0()); 
            	

            }


            }

            otherlv_2=(Token)match(input,91,FOLLOW_91_in_ruleMsgSpec10646); 

                	newLeafNode(otherlv_2, grammarAccess.getMsgSpecAccess().getSenderKeyword_2());
                
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4643:1: ( (lv_sender_3_0= ruleVarOrAtomic ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4644:1: (lv_sender_3_0= ruleVarOrAtomic )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4644:1: (lv_sender_3_0= ruleVarOrAtomic )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4645:3: lv_sender_3_0= ruleVarOrAtomic
            {
             
            	        newCompositeNode(grammarAccess.getMsgSpecAccess().getSenderVarOrAtomicParserRuleCall_3_0()); 
            	    
            pushFollow(FOLLOW_ruleVarOrAtomic_in_ruleMsgSpec10667);
            lv_sender_3_0=ruleVarOrAtomic();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getMsgSpecRule());
            	        }
                   		set(
                   			current, 
                   			"sender",
                    		lv_sender_3_0, 
                    		"VarOrAtomic");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_4=(Token)match(input,92,FOLLOW_92_in_ruleMsgSpec10679); 

                	newLeafNode(otherlv_4, grammarAccess.getMsgSpecAccess().getContentKeyword_4());
                
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4665:1: ( (lv_content_5_0= rulePHead ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4666:1: (lv_content_5_0= rulePHead )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4666:1: (lv_content_5_0= rulePHead )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4667:3: lv_content_5_0= rulePHead
            {
             
            	        newCompositeNode(grammarAccess.getMsgSpecAccess().getContentPHeadParserRuleCall_5_0()); 
            	    
            pushFollow(FOLLOW_rulePHead_in_ruleMsgSpec10700);
            lv_content_5_0=rulePHead();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getMsgSpecRule());
            	        }
                   		set(
                   			current, 
                   			"content",
                    		lv_content_5_0, 
                    		"PHead");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMsgSpec"


    // $ANTLR start "entryRuleOnReceiveMsg"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4691:1: entryRuleOnReceiveMsg returns [EObject current=null] : iv_ruleOnReceiveMsg= ruleOnReceiveMsg EOF ;
    public final EObject entryRuleOnReceiveMsg() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOnReceiveMsg = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4692:2: (iv_ruleOnReceiveMsg= ruleOnReceiveMsg EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4693:2: iv_ruleOnReceiveMsg= ruleOnReceiveMsg EOF
            {
             newCompositeNode(grammarAccess.getOnReceiveMsgRule()); 
            pushFollow(FOLLOW_ruleOnReceiveMsg_in_entryRuleOnReceiveMsg10736);
            iv_ruleOnReceiveMsg=ruleOnReceiveMsg();

            state._fsp--;

             current =iv_ruleOnReceiveMsg; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleOnReceiveMsg10746); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOnReceiveMsg"


    // $ANTLR start "ruleOnReceiveMsg"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4700:1: ruleOnReceiveMsg returns [EObject current=null] : ( ( (lv_name_0_0= 'receiveTheMsg' ) ) otherlv_1= 'm' otherlv_2= '(' ( (lv_msgid_3_0= rulePHead ) ) otherlv_4= ',' ( (lv_msgtype_5_0= rulePHead ) ) otherlv_6= ',' ( (lv_msgsender_7_0= rulePHead ) ) otherlv_8= ',' ( (lv_msgreceiver_9_0= rulePHead ) ) otherlv_10= ',' ( (lv_msgcontent_11_0= rulePHead ) ) otherlv_12= ',' ( (lv_msgseqnum_13_0= rulePHead ) ) otherlv_14= ')' ( (lv_duration_15_0= ruleTimeLimit ) ) ) ;
    public final EObject ruleOnReceiveMsg() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        Token otherlv_12=null;
        Token otherlv_14=null;
        EObject lv_msgid_3_0 = null;

        EObject lv_msgtype_5_0 = null;

        EObject lv_msgsender_7_0 = null;

        EObject lv_msgreceiver_9_0 = null;

        EObject lv_msgcontent_11_0 = null;

        EObject lv_msgseqnum_13_0 = null;

        EObject lv_duration_15_0 = null;


         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4703:28: ( ( ( (lv_name_0_0= 'receiveTheMsg' ) ) otherlv_1= 'm' otherlv_2= '(' ( (lv_msgid_3_0= rulePHead ) ) otherlv_4= ',' ( (lv_msgtype_5_0= rulePHead ) ) otherlv_6= ',' ( (lv_msgsender_7_0= rulePHead ) ) otherlv_8= ',' ( (lv_msgreceiver_9_0= rulePHead ) ) otherlv_10= ',' ( (lv_msgcontent_11_0= rulePHead ) ) otherlv_12= ',' ( (lv_msgseqnum_13_0= rulePHead ) ) otherlv_14= ')' ( (lv_duration_15_0= ruleTimeLimit ) ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4704:1: ( ( (lv_name_0_0= 'receiveTheMsg' ) ) otherlv_1= 'm' otherlv_2= '(' ( (lv_msgid_3_0= rulePHead ) ) otherlv_4= ',' ( (lv_msgtype_5_0= rulePHead ) ) otherlv_6= ',' ( (lv_msgsender_7_0= rulePHead ) ) otherlv_8= ',' ( (lv_msgreceiver_9_0= rulePHead ) ) otherlv_10= ',' ( (lv_msgcontent_11_0= rulePHead ) ) otherlv_12= ',' ( (lv_msgseqnum_13_0= rulePHead ) ) otherlv_14= ')' ( (lv_duration_15_0= ruleTimeLimit ) ) )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4704:1: ( ( (lv_name_0_0= 'receiveTheMsg' ) ) otherlv_1= 'm' otherlv_2= '(' ( (lv_msgid_3_0= rulePHead ) ) otherlv_4= ',' ( (lv_msgtype_5_0= rulePHead ) ) otherlv_6= ',' ( (lv_msgsender_7_0= rulePHead ) ) otherlv_8= ',' ( (lv_msgreceiver_9_0= rulePHead ) ) otherlv_10= ',' ( (lv_msgcontent_11_0= rulePHead ) ) otherlv_12= ',' ( (lv_msgseqnum_13_0= rulePHead ) ) otherlv_14= ')' ( (lv_duration_15_0= ruleTimeLimit ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4704:2: ( (lv_name_0_0= 'receiveTheMsg' ) ) otherlv_1= 'm' otherlv_2= '(' ( (lv_msgid_3_0= rulePHead ) ) otherlv_4= ',' ( (lv_msgtype_5_0= rulePHead ) ) otherlv_6= ',' ( (lv_msgsender_7_0= rulePHead ) ) otherlv_8= ',' ( (lv_msgreceiver_9_0= rulePHead ) ) otherlv_10= ',' ( (lv_msgcontent_11_0= rulePHead ) ) otherlv_12= ',' ( (lv_msgseqnum_13_0= rulePHead ) ) otherlv_14= ')' ( (lv_duration_15_0= ruleTimeLimit ) )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4704:2: ( (lv_name_0_0= 'receiveTheMsg' ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4705:1: (lv_name_0_0= 'receiveTheMsg' )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4705:1: (lv_name_0_0= 'receiveTheMsg' )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4706:3: lv_name_0_0= 'receiveTheMsg'
            {
            lv_name_0_0=(Token)match(input,93,FOLLOW_93_in_ruleOnReceiveMsg10789); 

                    newLeafNode(lv_name_0_0, grammarAccess.getOnReceiveMsgAccess().getNameReceiveTheMsgKeyword_0_0());
                

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getOnReceiveMsgRule());
            	        }
                   		setWithLastConsumed(current, "name", lv_name_0_0, "receiveTheMsg");
            	    

            }


            }

            otherlv_1=(Token)match(input,94,FOLLOW_94_in_ruleOnReceiveMsg10814); 

                	newLeafNode(otherlv_1, grammarAccess.getOnReceiveMsgAccess().getMKeyword_1());
                
            otherlv_2=(Token)match(input,35,FOLLOW_35_in_ruleOnReceiveMsg10826); 

                	newLeafNode(otherlv_2, grammarAccess.getOnReceiveMsgAccess().getLeftParenthesisKeyword_2());
                
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4727:1: ( (lv_msgid_3_0= rulePHead ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4728:1: (lv_msgid_3_0= rulePHead )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4728:1: (lv_msgid_3_0= rulePHead )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4729:3: lv_msgid_3_0= rulePHead
            {
             
            	        newCompositeNode(grammarAccess.getOnReceiveMsgAccess().getMsgidPHeadParserRuleCall_3_0()); 
            	    
            pushFollow(FOLLOW_rulePHead_in_ruleOnReceiveMsg10847);
            lv_msgid_3_0=rulePHead();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getOnReceiveMsgRule());
            	        }
                   		set(
                   			current, 
                   			"msgid",
                    		lv_msgid_3_0, 
                    		"PHead");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_4=(Token)match(input,33,FOLLOW_33_in_ruleOnReceiveMsg10859); 

                	newLeafNode(otherlv_4, grammarAccess.getOnReceiveMsgAccess().getCommaKeyword_4());
                
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4749:1: ( (lv_msgtype_5_0= rulePHead ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4750:1: (lv_msgtype_5_0= rulePHead )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4750:1: (lv_msgtype_5_0= rulePHead )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4751:3: lv_msgtype_5_0= rulePHead
            {
             
            	        newCompositeNode(grammarAccess.getOnReceiveMsgAccess().getMsgtypePHeadParserRuleCall_5_0()); 
            	    
            pushFollow(FOLLOW_rulePHead_in_ruleOnReceiveMsg10880);
            lv_msgtype_5_0=rulePHead();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getOnReceiveMsgRule());
            	        }
                   		set(
                   			current, 
                   			"msgtype",
                    		lv_msgtype_5_0, 
                    		"PHead");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_6=(Token)match(input,33,FOLLOW_33_in_ruleOnReceiveMsg10892); 

                	newLeafNode(otherlv_6, grammarAccess.getOnReceiveMsgAccess().getCommaKeyword_6());
                
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4771:1: ( (lv_msgsender_7_0= rulePHead ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4772:1: (lv_msgsender_7_0= rulePHead )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4772:1: (lv_msgsender_7_0= rulePHead )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4773:3: lv_msgsender_7_0= rulePHead
            {
             
            	        newCompositeNode(grammarAccess.getOnReceiveMsgAccess().getMsgsenderPHeadParserRuleCall_7_0()); 
            	    
            pushFollow(FOLLOW_rulePHead_in_ruleOnReceiveMsg10913);
            lv_msgsender_7_0=rulePHead();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getOnReceiveMsgRule());
            	        }
                   		set(
                   			current, 
                   			"msgsender",
                    		lv_msgsender_7_0, 
                    		"PHead");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_8=(Token)match(input,33,FOLLOW_33_in_ruleOnReceiveMsg10925); 

                	newLeafNode(otherlv_8, grammarAccess.getOnReceiveMsgAccess().getCommaKeyword_8());
                
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4793:1: ( (lv_msgreceiver_9_0= rulePHead ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4794:1: (lv_msgreceiver_9_0= rulePHead )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4794:1: (lv_msgreceiver_9_0= rulePHead )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4795:3: lv_msgreceiver_9_0= rulePHead
            {
             
            	        newCompositeNode(grammarAccess.getOnReceiveMsgAccess().getMsgreceiverPHeadParserRuleCall_9_0()); 
            	    
            pushFollow(FOLLOW_rulePHead_in_ruleOnReceiveMsg10946);
            lv_msgreceiver_9_0=rulePHead();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getOnReceiveMsgRule());
            	        }
                   		set(
                   			current, 
                   			"msgreceiver",
                    		lv_msgreceiver_9_0, 
                    		"PHead");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_10=(Token)match(input,33,FOLLOW_33_in_ruleOnReceiveMsg10958); 

                	newLeafNode(otherlv_10, grammarAccess.getOnReceiveMsgAccess().getCommaKeyword_10());
                
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4815:1: ( (lv_msgcontent_11_0= rulePHead ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4816:1: (lv_msgcontent_11_0= rulePHead )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4816:1: (lv_msgcontent_11_0= rulePHead )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4817:3: lv_msgcontent_11_0= rulePHead
            {
             
            	        newCompositeNode(grammarAccess.getOnReceiveMsgAccess().getMsgcontentPHeadParserRuleCall_11_0()); 
            	    
            pushFollow(FOLLOW_rulePHead_in_ruleOnReceiveMsg10979);
            lv_msgcontent_11_0=rulePHead();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getOnReceiveMsgRule());
            	        }
                   		set(
                   			current, 
                   			"msgcontent",
                    		lv_msgcontent_11_0, 
                    		"PHead");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_12=(Token)match(input,33,FOLLOW_33_in_ruleOnReceiveMsg10991); 

                	newLeafNode(otherlv_12, grammarAccess.getOnReceiveMsgAccess().getCommaKeyword_12());
                
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4837:1: ( (lv_msgseqnum_13_0= rulePHead ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4838:1: (lv_msgseqnum_13_0= rulePHead )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4838:1: (lv_msgseqnum_13_0= rulePHead )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4839:3: lv_msgseqnum_13_0= rulePHead
            {
             
            	        newCompositeNode(grammarAccess.getOnReceiveMsgAccess().getMsgseqnumPHeadParserRuleCall_13_0()); 
            	    
            pushFollow(FOLLOW_rulePHead_in_ruleOnReceiveMsg11012);
            lv_msgseqnum_13_0=rulePHead();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getOnReceiveMsgRule());
            	        }
                   		set(
                   			current, 
                   			"msgseqnum",
                    		lv_msgseqnum_13_0, 
                    		"PHead");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_14=(Token)match(input,36,FOLLOW_36_in_ruleOnReceiveMsg11024); 

                	newLeafNode(otherlv_14, grammarAccess.getOnReceiveMsgAccess().getRightParenthesisKeyword_14());
                
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4859:1: ( (lv_duration_15_0= ruleTimeLimit ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4860:1: (lv_duration_15_0= ruleTimeLimit )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4860:1: (lv_duration_15_0= ruleTimeLimit )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4861:3: lv_duration_15_0= ruleTimeLimit
            {
             
            	        newCompositeNode(grammarAccess.getOnReceiveMsgAccess().getDurationTimeLimitParserRuleCall_15_0()); 
            	    
            pushFollow(FOLLOW_ruleTimeLimit_in_ruleOnReceiveMsg11045);
            lv_duration_15_0=ruleTimeLimit();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getOnReceiveMsgRule());
            	        }
                   		set(
                   			current, 
                   			"duration",
                    		lv_duration_15_0, 
                    		"TimeLimit");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOnReceiveMsg"


    // $ANTLR start "entryRuleMsgSelect"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4885:1: entryRuleMsgSelect returns [EObject current=null] : iv_ruleMsgSelect= ruleMsgSelect EOF ;
    public final EObject entryRuleMsgSelect() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMsgSelect = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4886:2: (iv_ruleMsgSelect= ruleMsgSelect EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4887:2: iv_ruleMsgSelect= ruleMsgSelect EOF
            {
             newCompositeNode(grammarAccess.getMsgSelectRule()); 
            pushFollow(FOLLOW_ruleMsgSelect_in_entryRuleMsgSelect11081);
            iv_ruleMsgSelect=ruleMsgSelect();

            state._fsp--;

             current =iv_ruleMsgSelect; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleMsgSelect11091); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMsgSelect"


    // $ANTLR start "ruleMsgSelect"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4894:1: ruleMsgSelect returns [EObject current=null] : ( ( (lv_name_0_0= 'receiveAndSwitch' ) ) ( (lv_duration_1_0= ruleTimeLimit ) ) ( (otherlv_2= RULE_ID ) ) (otherlv_3= ',' ( (otherlv_4= RULE_ID ) ) )* otherlv_5= '->' ( (otherlv_6= RULE_ID ) ) (otherlv_7= ',' ( (otherlv_8= RULE_ID ) ) )* ) ;
    public final EObject ruleMsgSelect() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        EObject lv_duration_1_0 = null;


         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4897:28: ( ( ( (lv_name_0_0= 'receiveAndSwitch' ) ) ( (lv_duration_1_0= ruleTimeLimit ) ) ( (otherlv_2= RULE_ID ) ) (otherlv_3= ',' ( (otherlv_4= RULE_ID ) ) )* otherlv_5= '->' ( (otherlv_6= RULE_ID ) ) (otherlv_7= ',' ( (otherlv_8= RULE_ID ) ) )* ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4898:1: ( ( (lv_name_0_0= 'receiveAndSwitch' ) ) ( (lv_duration_1_0= ruleTimeLimit ) ) ( (otherlv_2= RULE_ID ) ) (otherlv_3= ',' ( (otherlv_4= RULE_ID ) ) )* otherlv_5= '->' ( (otherlv_6= RULE_ID ) ) (otherlv_7= ',' ( (otherlv_8= RULE_ID ) ) )* )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4898:1: ( ( (lv_name_0_0= 'receiveAndSwitch' ) ) ( (lv_duration_1_0= ruleTimeLimit ) ) ( (otherlv_2= RULE_ID ) ) (otherlv_3= ',' ( (otherlv_4= RULE_ID ) ) )* otherlv_5= '->' ( (otherlv_6= RULE_ID ) ) (otherlv_7= ',' ( (otherlv_8= RULE_ID ) ) )* )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4898:2: ( (lv_name_0_0= 'receiveAndSwitch' ) ) ( (lv_duration_1_0= ruleTimeLimit ) ) ( (otherlv_2= RULE_ID ) ) (otherlv_3= ',' ( (otherlv_4= RULE_ID ) ) )* otherlv_5= '->' ( (otherlv_6= RULE_ID ) ) (otherlv_7= ',' ( (otherlv_8= RULE_ID ) ) )*
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4898:2: ( (lv_name_0_0= 'receiveAndSwitch' ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4899:1: (lv_name_0_0= 'receiveAndSwitch' )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4899:1: (lv_name_0_0= 'receiveAndSwitch' )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4900:3: lv_name_0_0= 'receiveAndSwitch'
            {
            lv_name_0_0=(Token)match(input,95,FOLLOW_95_in_ruleMsgSelect11134); 

                    newLeafNode(lv_name_0_0, grammarAccess.getMsgSelectAccess().getNameReceiveAndSwitchKeyword_0_0());
                

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getMsgSelectRule());
            	        }
                   		setWithLastConsumed(current, "name", lv_name_0_0, "receiveAndSwitch");
            	    

            }


            }

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4913:2: ( (lv_duration_1_0= ruleTimeLimit ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4914:1: (lv_duration_1_0= ruleTimeLimit )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4914:1: (lv_duration_1_0= ruleTimeLimit )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4915:3: lv_duration_1_0= ruleTimeLimit
            {
             
            	        newCompositeNode(grammarAccess.getMsgSelectAccess().getDurationTimeLimitParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_ruleTimeLimit_in_ruleMsgSelect11168);
            lv_duration_1_0=ruleTimeLimit();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getMsgSelectRule());
            	        }
                   		set(
                   			current, 
                   			"duration",
                    		lv_duration_1_0, 
                    		"TimeLimit");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4931:2: ( (otherlv_2= RULE_ID ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4932:1: (otherlv_2= RULE_ID )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4932:1: (otherlv_2= RULE_ID )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4933:3: otherlv_2= RULE_ID
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getMsgSelectRule());
            	        }
                    
            otherlv_2=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleMsgSelect11188); 

            		newLeafNode(otherlv_2, grammarAccess.getMsgSelectAccess().getMessagesMessageCrossReference_2_0()); 
            	

            }


            }

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4944:2: (otherlv_3= ',' ( (otherlv_4= RULE_ID ) ) )*
            loop57:
            do {
                int alt57=2;
                int LA57_0 = input.LA(1);

                if ( (LA57_0==33) ) {
                    alt57=1;
                }


                switch (alt57) {
            	case 1 :
            	    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4944:4: otherlv_3= ',' ( (otherlv_4= RULE_ID ) )
            	    {
            	    otherlv_3=(Token)match(input,33,FOLLOW_33_in_ruleMsgSelect11201); 

            	        	newLeafNode(otherlv_3, grammarAccess.getMsgSelectAccess().getCommaKeyword_3_0());
            	        
            	    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4948:1: ( (otherlv_4= RULE_ID ) )
            	    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4949:1: (otherlv_4= RULE_ID )
            	    {
            	    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4949:1: (otherlv_4= RULE_ID )
            	    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4950:3: otherlv_4= RULE_ID
            	    {

            	    			if (current==null) {
            	    	            current = createModelElement(grammarAccess.getMsgSelectRule());
            	    	        }
            	            
            	    otherlv_4=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleMsgSelect11221); 

            	    		newLeafNode(otherlv_4, grammarAccess.getMsgSelectAccess().getMessagesMessageCrossReference_3_1_0()); 
            	    	

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop57;
                }
            } while (true);

            otherlv_5=(Token)match(input,96,FOLLOW_96_in_ruleMsgSelect11235); 

                	newLeafNode(otherlv_5, grammarAccess.getMsgSelectAccess().getHyphenMinusGreaterThanSignKeyword_4());
                
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4965:1: ( (otherlv_6= RULE_ID ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4966:1: (otherlv_6= RULE_ID )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4966:1: (otherlv_6= RULE_ID )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4967:3: otherlv_6= RULE_ID
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getMsgSelectRule());
            	        }
                    
            otherlv_6=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleMsgSelect11255); 

            		newLeafNode(otherlv_6, grammarAccess.getMsgSelectAccess().getPlansPlanCrossReference_5_0()); 
            	

            }


            }

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4978:2: (otherlv_7= ',' ( (otherlv_8= RULE_ID ) ) )*
            loop58:
            do {
                int alt58=2;
                int LA58_0 = input.LA(1);

                if ( (LA58_0==33) ) {
                    alt58=1;
                }


                switch (alt58) {
            	case 1 :
            	    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4978:4: otherlv_7= ',' ( (otherlv_8= RULE_ID ) )
            	    {
            	    otherlv_7=(Token)match(input,33,FOLLOW_33_in_ruleMsgSelect11268); 

            	        	newLeafNode(otherlv_7, grammarAccess.getMsgSelectAccess().getCommaKeyword_6_0());
            	        
            	    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4982:1: ( (otherlv_8= RULE_ID ) )
            	    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4983:1: (otherlv_8= RULE_ID )
            	    {
            	    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4983:1: (otherlv_8= RULE_ID )
            	    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:4984:3: otherlv_8= RULE_ID
            	    {

            	    			if (current==null) {
            	    	            current = createModelElement(grammarAccess.getMsgSelectRule());
            	    	        }
            	            
            	    otherlv_8=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleMsgSelect11288); 

            	    		newLeafNode(otherlv_8, grammarAccess.getMsgSelectAccess().getPlansPlanCrossReference_6_1_0()); 
            	    	

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop58;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMsgSelect"


    // $ANTLR start "entryRuleRaiseEvent"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5003:1: entryRuleRaiseEvent returns [EObject current=null] : iv_ruleRaiseEvent= ruleRaiseEvent EOF ;
    public final EObject entryRuleRaiseEvent() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRaiseEvent = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5004:2: (iv_ruleRaiseEvent= ruleRaiseEvent EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5005:2: iv_ruleRaiseEvent= ruleRaiseEvent EOF
            {
             newCompositeNode(grammarAccess.getRaiseEventRule()); 
            pushFollow(FOLLOW_ruleRaiseEvent_in_entryRuleRaiseEvent11326);
            iv_ruleRaiseEvent=ruleRaiseEvent();

            state._fsp--;

             current =iv_ruleRaiseEvent; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleRaiseEvent11336); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRaiseEvent"


    // $ANTLR start "ruleRaiseEvent"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5012:1: ruleRaiseEvent returns [EObject current=null] : ( ( (lv_name_0_0= 'emit' ) ) ( (otherlv_1= RULE_ID ) ) otherlv_2= ':' ( (lv_content_3_0= rulePHead ) ) ) ;
    public final EObject ruleRaiseEvent() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        EObject lv_content_3_0 = null;


         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5015:28: ( ( ( (lv_name_0_0= 'emit' ) ) ( (otherlv_1= RULE_ID ) ) otherlv_2= ':' ( (lv_content_3_0= rulePHead ) ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5016:1: ( ( (lv_name_0_0= 'emit' ) ) ( (otherlv_1= RULE_ID ) ) otherlv_2= ':' ( (lv_content_3_0= rulePHead ) ) )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5016:1: ( ( (lv_name_0_0= 'emit' ) ) ( (otherlv_1= RULE_ID ) ) otherlv_2= ':' ( (lv_content_3_0= rulePHead ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5016:2: ( (lv_name_0_0= 'emit' ) ) ( (otherlv_1= RULE_ID ) ) otherlv_2= ':' ( (lv_content_3_0= rulePHead ) )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5016:2: ( (lv_name_0_0= 'emit' ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5017:1: (lv_name_0_0= 'emit' )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5017:1: (lv_name_0_0= 'emit' )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5018:3: lv_name_0_0= 'emit'
            {
            lv_name_0_0=(Token)match(input,97,FOLLOW_97_in_ruleRaiseEvent11379); 

                    newLeafNode(lv_name_0_0, grammarAccess.getRaiseEventAccess().getNameEmitKeyword_0_0());
                

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getRaiseEventRule());
            	        }
                   		setWithLastConsumed(current, "name", lv_name_0_0, "emit");
            	    

            }


            }

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5031:2: ( (otherlv_1= RULE_ID ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5032:1: (otherlv_1= RULE_ID )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5032:1: (otherlv_1= RULE_ID )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5033:3: otherlv_1= RULE_ID
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getRaiseEventRule());
            	        }
                    
            otherlv_1=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleRaiseEvent11412); 

            		newLeafNode(otherlv_1, grammarAccess.getRaiseEventAccess().getEvEventCrossReference_1_0()); 
            	

            }


            }

            otherlv_2=(Token)match(input,16,FOLLOW_16_in_ruleRaiseEvent11424); 

                	newLeafNode(otherlv_2, grammarAccess.getRaiseEventAccess().getColonKeyword_2());
                
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5048:1: ( (lv_content_3_0= rulePHead ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5049:1: (lv_content_3_0= rulePHead )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5049:1: (lv_content_3_0= rulePHead )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5050:3: lv_content_3_0= rulePHead
            {
             
            	        newCompositeNode(grammarAccess.getRaiseEventAccess().getContentPHeadParserRuleCall_3_0()); 
            	    
            pushFollow(FOLLOW_rulePHead_in_ruleRaiseEvent11445);
            lv_content_3_0=rulePHead();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getRaiseEventRule());
            	        }
                   		set(
                   			current, 
                   			"content",
                    		lv_content_3_0, 
                    		"PHead");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRaiseEvent"


    // $ANTLR start "entryRuleSenseEvent"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5074:1: entryRuleSenseEvent returns [EObject current=null] : iv_ruleSenseEvent= ruleSenseEvent EOF ;
    public final EObject entryRuleSenseEvent() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSenseEvent = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5075:2: (iv_ruleSenseEvent= ruleSenseEvent EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5076:2: iv_ruleSenseEvent= ruleSenseEvent EOF
            {
             newCompositeNode(grammarAccess.getSenseEventRule()); 
            pushFollow(FOLLOW_ruleSenseEvent_in_entryRuleSenseEvent11481);
            iv_ruleSenseEvent=ruleSenseEvent();

            state._fsp--;

             current =iv_ruleSenseEvent; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleSenseEvent11491); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSenseEvent"


    // $ANTLR start "ruleSenseEvent"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5083:1: ruleSenseEvent returns [EObject current=null] : (otherlv_0= 'sense' ( (lv_duration_1_0= ruleTimeLimit ) ) ( (otherlv_2= RULE_ID ) ) (otherlv_3= ',' ( (otherlv_4= RULE_ID ) ) )* otherlv_5= '->' ( (lv_plans_6_0= ruleContinuation ) ) (otherlv_7= ',' ( (lv_plans_8_0= ruleContinuation ) ) )* ) ;
    public final EObject ruleSenseEvent() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        EObject lv_duration_1_0 = null;

        EObject lv_plans_6_0 = null;

        EObject lv_plans_8_0 = null;


         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5086:28: ( (otherlv_0= 'sense' ( (lv_duration_1_0= ruleTimeLimit ) ) ( (otherlv_2= RULE_ID ) ) (otherlv_3= ',' ( (otherlv_4= RULE_ID ) ) )* otherlv_5= '->' ( (lv_plans_6_0= ruleContinuation ) ) (otherlv_7= ',' ( (lv_plans_8_0= ruleContinuation ) ) )* ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5087:1: (otherlv_0= 'sense' ( (lv_duration_1_0= ruleTimeLimit ) ) ( (otherlv_2= RULE_ID ) ) (otherlv_3= ',' ( (otherlv_4= RULE_ID ) ) )* otherlv_5= '->' ( (lv_plans_6_0= ruleContinuation ) ) (otherlv_7= ',' ( (lv_plans_8_0= ruleContinuation ) ) )* )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5087:1: (otherlv_0= 'sense' ( (lv_duration_1_0= ruleTimeLimit ) ) ( (otherlv_2= RULE_ID ) ) (otherlv_3= ',' ( (otherlv_4= RULE_ID ) ) )* otherlv_5= '->' ( (lv_plans_6_0= ruleContinuation ) ) (otherlv_7= ',' ( (lv_plans_8_0= ruleContinuation ) ) )* )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5087:3: otherlv_0= 'sense' ( (lv_duration_1_0= ruleTimeLimit ) ) ( (otherlv_2= RULE_ID ) ) (otherlv_3= ',' ( (otherlv_4= RULE_ID ) ) )* otherlv_5= '->' ( (lv_plans_6_0= ruleContinuation ) ) (otherlv_7= ',' ( (lv_plans_8_0= ruleContinuation ) ) )*
            {
            otherlv_0=(Token)match(input,98,FOLLOW_98_in_ruleSenseEvent11528); 

                	newLeafNode(otherlv_0, grammarAccess.getSenseEventAccess().getSenseKeyword_0());
                
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5091:1: ( (lv_duration_1_0= ruleTimeLimit ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5092:1: (lv_duration_1_0= ruleTimeLimit )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5092:1: (lv_duration_1_0= ruleTimeLimit )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5093:3: lv_duration_1_0= ruleTimeLimit
            {
             
            	        newCompositeNode(grammarAccess.getSenseEventAccess().getDurationTimeLimitParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_ruleTimeLimit_in_ruleSenseEvent11549);
            lv_duration_1_0=ruleTimeLimit();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getSenseEventRule());
            	        }
                   		set(
                   			current, 
                   			"duration",
                    		lv_duration_1_0, 
                    		"TimeLimit");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5109:2: ( (otherlv_2= RULE_ID ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5110:1: (otherlv_2= RULE_ID )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5110:1: (otherlv_2= RULE_ID )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5111:3: otherlv_2= RULE_ID
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getSenseEventRule());
            	        }
                    
            otherlv_2=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleSenseEvent11569); 

            		newLeafNode(otherlv_2, grammarAccess.getSenseEventAccess().getEventsEventCrossReference_2_0()); 
            	

            }


            }

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5122:2: (otherlv_3= ',' ( (otherlv_4= RULE_ID ) ) )*
            loop59:
            do {
                int alt59=2;
                int LA59_0 = input.LA(1);

                if ( (LA59_0==33) ) {
                    alt59=1;
                }


                switch (alt59) {
            	case 1 :
            	    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5122:4: otherlv_3= ',' ( (otherlv_4= RULE_ID ) )
            	    {
            	    otherlv_3=(Token)match(input,33,FOLLOW_33_in_ruleSenseEvent11582); 

            	        	newLeafNode(otherlv_3, grammarAccess.getSenseEventAccess().getCommaKeyword_3_0());
            	        
            	    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5126:1: ( (otherlv_4= RULE_ID ) )
            	    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5127:1: (otherlv_4= RULE_ID )
            	    {
            	    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5127:1: (otherlv_4= RULE_ID )
            	    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5128:3: otherlv_4= RULE_ID
            	    {

            	    			if (current==null) {
            	    	            current = createModelElement(grammarAccess.getSenseEventRule());
            	    	        }
            	            
            	    otherlv_4=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleSenseEvent11602); 

            	    		newLeafNode(otherlv_4, grammarAccess.getSenseEventAccess().getEventsEventCrossReference_3_1_0()); 
            	    	

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop59;
                }
            } while (true);

            otherlv_5=(Token)match(input,96,FOLLOW_96_in_ruleSenseEvent11616); 

                	newLeafNode(otherlv_5, grammarAccess.getSenseEventAccess().getHyphenMinusGreaterThanSignKeyword_4());
                
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5143:1: ( (lv_plans_6_0= ruleContinuation ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5144:1: (lv_plans_6_0= ruleContinuation )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5144:1: (lv_plans_6_0= ruleContinuation )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5145:3: lv_plans_6_0= ruleContinuation
            {
             
            	        newCompositeNode(grammarAccess.getSenseEventAccess().getPlansContinuationParserRuleCall_5_0()); 
            	    
            pushFollow(FOLLOW_ruleContinuation_in_ruleSenseEvent11637);
            lv_plans_6_0=ruleContinuation();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getSenseEventRule());
            	        }
                   		add(
                   			current, 
                   			"plans",
                    		lv_plans_6_0, 
                    		"Continuation");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5161:2: (otherlv_7= ',' ( (lv_plans_8_0= ruleContinuation ) ) )*
            loop60:
            do {
                int alt60=2;
                int LA60_0 = input.LA(1);

                if ( (LA60_0==33) ) {
                    alt60=1;
                }


                switch (alt60) {
            	case 1 :
            	    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5161:4: otherlv_7= ',' ( (lv_plans_8_0= ruleContinuation ) )
            	    {
            	    otherlv_7=(Token)match(input,33,FOLLOW_33_in_ruleSenseEvent11650); 

            	        	newLeafNode(otherlv_7, grammarAccess.getSenseEventAccess().getCommaKeyword_6_0());
            	        
            	    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5165:1: ( (lv_plans_8_0= ruleContinuation ) )
            	    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5166:1: (lv_plans_8_0= ruleContinuation )
            	    {
            	    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5166:1: (lv_plans_8_0= ruleContinuation )
            	    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5167:3: lv_plans_8_0= ruleContinuation
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getSenseEventAccess().getPlansContinuationParserRuleCall_6_1_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleContinuation_in_ruleSenseEvent11671);
            	    lv_plans_8_0=ruleContinuation();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getSenseEventRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"plans",
            	            		lv_plans_8_0, 
            	            		"Continuation");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop60;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSenseEvent"


    // $ANTLR start "entryRuleMsgSwitch"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5191:1: entryRuleMsgSwitch returns [EObject current=null] : iv_ruleMsgSwitch= ruleMsgSwitch EOF ;
    public final EObject entryRuleMsgSwitch() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMsgSwitch = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5192:2: (iv_ruleMsgSwitch= ruleMsgSwitch EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5193:2: iv_ruleMsgSwitch= ruleMsgSwitch EOF
            {
             newCompositeNode(grammarAccess.getMsgSwitchRule()); 
            pushFollow(FOLLOW_ruleMsgSwitch_in_entryRuleMsgSwitch11709);
            iv_ruleMsgSwitch=ruleMsgSwitch();

            state._fsp--;

             current =iv_ruleMsgSwitch; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleMsgSwitch11719); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMsgSwitch"


    // $ANTLR start "ruleMsgSwitch"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5200:1: ruleMsgSwitch returns [EObject current=null] : (otherlv_0= 'onMsg' ( (otherlv_1= RULE_ID ) ) otherlv_2= ':' ( (lv_msg_3_0= rulePHead ) ) otherlv_4= '->' ( (lv_move_5_0= ruleMove ) ) ) ;
    public final EObject ruleMsgSwitch() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_msg_3_0 = null;

        EObject lv_move_5_0 = null;


         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5203:28: ( (otherlv_0= 'onMsg' ( (otherlv_1= RULE_ID ) ) otherlv_2= ':' ( (lv_msg_3_0= rulePHead ) ) otherlv_4= '->' ( (lv_move_5_0= ruleMove ) ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5204:1: (otherlv_0= 'onMsg' ( (otherlv_1= RULE_ID ) ) otherlv_2= ':' ( (lv_msg_3_0= rulePHead ) ) otherlv_4= '->' ( (lv_move_5_0= ruleMove ) ) )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5204:1: (otherlv_0= 'onMsg' ( (otherlv_1= RULE_ID ) ) otherlv_2= ':' ( (lv_msg_3_0= rulePHead ) ) otherlv_4= '->' ( (lv_move_5_0= ruleMove ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5204:3: otherlv_0= 'onMsg' ( (otherlv_1= RULE_ID ) ) otherlv_2= ':' ( (lv_msg_3_0= rulePHead ) ) otherlv_4= '->' ( (lv_move_5_0= ruleMove ) )
            {
            otherlv_0=(Token)match(input,99,FOLLOW_99_in_ruleMsgSwitch11756); 

                	newLeafNode(otherlv_0, grammarAccess.getMsgSwitchAccess().getOnMsgKeyword_0());
                
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5208:1: ( (otherlv_1= RULE_ID ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5209:1: (otherlv_1= RULE_ID )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5209:1: (otherlv_1= RULE_ID )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5210:3: otherlv_1= RULE_ID
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getMsgSwitchRule());
            	        }
                    
            otherlv_1=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleMsgSwitch11776); 

            		newLeafNode(otherlv_1, grammarAccess.getMsgSwitchAccess().getMessageMessageCrossReference_1_0()); 
            	

            }


            }

            otherlv_2=(Token)match(input,16,FOLLOW_16_in_ruleMsgSwitch11788); 

                	newLeafNode(otherlv_2, grammarAccess.getMsgSwitchAccess().getColonKeyword_2());
                
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5225:1: ( (lv_msg_3_0= rulePHead ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5226:1: (lv_msg_3_0= rulePHead )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5226:1: (lv_msg_3_0= rulePHead )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5227:3: lv_msg_3_0= rulePHead
            {
             
            	        newCompositeNode(grammarAccess.getMsgSwitchAccess().getMsgPHeadParserRuleCall_3_0()); 
            	    
            pushFollow(FOLLOW_rulePHead_in_ruleMsgSwitch11809);
            lv_msg_3_0=rulePHead();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getMsgSwitchRule());
            	        }
                   		set(
                   			current, 
                   			"msg",
                    		lv_msg_3_0, 
                    		"PHead");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_4=(Token)match(input,96,FOLLOW_96_in_ruleMsgSwitch11821); 

                	newLeafNode(otherlv_4, grammarAccess.getMsgSwitchAccess().getHyphenMinusGreaterThanSignKeyword_4());
                
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5247:1: ( (lv_move_5_0= ruleMove ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5248:1: (lv_move_5_0= ruleMove )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5248:1: (lv_move_5_0= ruleMove )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5249:3: lv_move_5_0= ruleMove
            {
             
            	        newCompositeNode(grammarAccess.getMsgSwitchAccess().getMoveMoveParserRuleCall_5_0()); 
            	    
            pushFollow(FOLLOW_ruleMove_in_ruleMsgSwitch11842);
            lv_move_5_0=ruleMove();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getMsgSwitchRule());
            	        }
                   		set(
                   			current, 
                   			"move",
                    		lv_move_5_0, 
                    		"Move");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMsgSwitch"


    // $ANTLR start "entryRuleEventSwitch"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5273:1: entryRuleEventSwitch returns [EObject current=null] : iv_ruleEventSwitch= ruleEventSwitch EOF ;
    public final EObject entryRuleEventSwitch() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEventSwitch = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5274:2: (iv_ruleEventSwitch= ruleEventSwitch EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5275:2: iv_ruleEventSwitch= ruleEventSwitch EOF
            {
             newCompositeNode(grammarAccess.getEventSwitchRule()); 
            pushFollow(FOLLOW_ruleEventSwitch_in_entryRuleEventSwitch11878);
            iv_ruleEventSwitch=ruleEventSwitch();

            state._fsp--;

             current =iv_ruleEventSwitch; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleEventSwitch11888); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEventSwitch"


    // $ANTLR start "ruleEventSwitch"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5282:1: ruleEventSwitch returns [EObject current=null] : (otherlv_0= 'onEvent' ( (otherlv_1= RULE_ID ) ) otherlv_2= ':' ( (lv_msg_3_0= rulePHead ) ) otherlv_4= '->' ( (lv_move_5_0= ruleMove ) ) ) ;
    public final EObject ruleEventSwitch() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_msg_3_0 = null;

        EObject lv_move_5_0 = null;


         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5285:28: ( (otherlv_0= 'onEvent' ( (otherlv_1= RULE_ID ) ) otherlv_2= ':' ( (lv_msg_3_0= rulePHead ) ) otherlv_4= '->' ( (lv_move_5_0= ruleMove ) ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5286:1: (otherlv_0= 'onEvent' ( (otherlv_1= RULE_ID ) ) otherlv_2= ':' ( (lv_msg_3_0= rulePHead ) ) otherlv_4= '->' ( (lv_move_5_0= ruleMove ) ) )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5286:1: (otherlv_0= 'onEvent' ( (otherlv_1= RULE_ID ) ) otherlv_2= ':' ( (lv_msg_3_0= rulePHead ) ) otherlv_4= '->' ( (lv_move_5_0= ruleMove ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5286:3: otherlv_0= 'onEvent' ( (otherlv_1= RULE_ID ) ) otherlv_2= ':' ( (lv_msg_3_0= rulePHead ) ) otherlv_4= '->' ( (lv_move_5_0= ruleMove ) )
            {
            otherlv_0=(Token)match(input,100,FOLLOW_100_in_ruleEventSwitch11925); 

                	newLeafNode(otherlv_0, grammarAccess.getEventSwitchAccess().getOnEventKeyword_0());
                
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5290:1: ( (otherlv_1= RULE_ID ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5291:1: (otherlv_1= RULE_ID )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5291:1: (otherlv_1= RULE_ID )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5292:3: otherlv_1= RULE_ID
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getEventSwitchRule());
            	        }
                    
            otherlv_1=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleEventSwitch11945); 

            		newLeafNode(otherlv_1, grammarAccess.getEventSwitchAccess().getEventEventCrossReference_1_0()); 
            	

            }


            }

            otherlv_2=(Token)match(input,16,FOLLOW_16_in_ruleEventSwitch11957); 

                	newLeafNode(otherlv_2, grammarAccess.getEventSwitchAccess().getColonKeyword_2());
                
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5307:1: ( (lv_msg_3_0= rulePHead ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5308:1: (lv_msg_3_0= rulePHead )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5308:1: (lv_msg_3_0= rulePHead )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5309:3: lv_msg_3_0= rulePHead
            {
             
            	        newCompositeNode(grammarAccess.getEventSwitchAccess().getMsgPHeadParserRuleCall_3_0()); 
            	    
            pushFollow(FOLLOW_rulePHead_in_ruleEventSwitch11978);
            lv_msg_3_0=rulePHead();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getEventSwitchRule());
            	        }
                   		set(
                   			current, 
                   			"msg",
                    		lv_msg_3_0, 
                    		"PHead");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_4=(Token)match(input,96,FOLLOW_96_in_ruleEventSwitch11990); 

                	newLeafNode(otherlv_4, grammarAccess.getEventSwitchAccess().getHyphenMinusGreaterThanSignKeyword_4());
                
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5329:1: ( (lv_move_5_0= ruleMove ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5330:1: (lv_move_5_0= ruleMove )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5330:1: (lv_move_5_0= ruleMove )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5331:3: lv_move_5_0= ruleMove
            {
             
            	        newCompositeNode(grammarAccess.getEventSwitchAccess().getMoveMoveParserRuleCall_5_0()); 
            	    
            pushFollow(FOLLOW_ruleMove_in_ruleEventSwitch12011);
            lv_move_5_0=ruleMove();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getEventSwitchRule());
            	        }
                   		set(
                   			current, 
                   			"move",
                    		lv_move_5_0, 
                    		"Move");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEventSwitch"


    // $ANTLR start "entryRuleContinuation"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5355:1: entryRuleContinuation returns [EObject current=null] : iv_ruleContinuation= ruleContinuation EOF ;
    public final EObject entryRuleContinuation() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleContinuation = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5356:2: (iv_ruleContinuation= ruleContinuation EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5357:2: iv_ruleContinuation= ruleContinuation EOF
            {
             newCompositeNode(grammarAccess.getContinuationRule()); 
            pushFollow(FOLLOW_ruleContinuation_in_entryRuleContinuation12047);
            iv_ruleContinuation=ruleContinuation();

            state._fsp--;

             current =iv_ruleContinuation; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleContinuation12057); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleContinuation"


    // $ANTLR start "ruleContinuation"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5364:1: ruleContinuation returns [EObject current=null] : ( ( (otherlv_0= RULE_ID ) ) | ( (lv_nane_1_0= 'continue' ) ) ) ;
    public final EObject ruleContinuation() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_nane_1_0=null;

         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5367:28: ( ( ( (otherlv_0= RULE_ID ) ) | ( (lv_nane_1_0= 'continue' ) ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5368:1: ( ( (otherlv_0= RULE_ID ) ) | ( (lv_nane_1_0= 'continue' ) ) )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5368:1: ( ( (otherlv_0= RULE_ID ) ) | ( (lv_nane_1_0= 'continue' ) ) )
            int alt61=2;
            int LA61_0 = input.LA(1);

            if ( (LA61_0==RULE_ID) ) {
                alt61=1;
            }
            else if ( (LA61_0==101) ) {
                alt61=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 61, 0, input);

                throw nvae;
            }
            switch (alt61) {
                case 1 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5368:2: ( (otherlv_0= RULE_ID ) )
                    {
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5368:2: ( (otherlv_0= RULE_ID ) )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5369:1: (otherlv_0= RULE_ID )
                    {
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5369:1: (otherlv_0= RULE_ID )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5370:3: otherlv_0= RULE_ID
                    {

                    			if (current==null) {
                    	            current = createModelElement(grammarAccess.getContinuationRule());
                    	        }
                            
                    otherlv_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleContinuation12102); 

                    		newLeafNode(otherlv_0, grammarAccess.getContinuationAccess().getPlanPlanCrossReference_0_0()); 
                    	

                    }


                    }


                    }
                    break;
                case 2 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5382:6: ( (lv_nane_1_0= 'continue' ) )
                    {
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5382:6: ( (lv_nane_1_0= 'continue' ) )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5383:1: (lv_nane_1_0= 'continue' )
                    {
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5383:1: (lv_nane_1_0= 'continue' )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5384:3: lv_nane_1_0= 'continue'
                    {
                    lv_nane_1_0=(Token)match(input,101,FOLLOW_101_in_ruleContinuation12126); 

                            newLeafNode(lv_nane_1_0, grammarAccess.getContinuationAccess().getNaneContinueKeyword_1_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getContinuationRule());
                    	        }
                           		setWithLastConsumed(current, "nane", lv_nane_1_0, "continue");
                    	    

                    }


                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleContinuation"


    // $ANTLR start "entryRuleExtensionMove"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5405:1: entryRuleExtensionMove returns [EObject current=null] : iv_ruleExtensionMove= ruleExtensionMove EOF ;
    public final EObject entryRuleExtensionMove() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleExtensionMove = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5406:2: (iv_ruleExtensionMove= ruleExtensionMove EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5407:2: iv_ruleExtensionMove= ruleExtensionMove EOF
            {
             newCompositeNode(grammarAccess.getExtensionMoveRule()); 
            pushFollow(FOLLOW_ruleExtensionMove_in_entryRuleExtensionMove12175);
            iv_ruleExtensionMove=ruleExtensionMove();

            state._fsp--;

             current =iv_ruleExtensionMove; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleExtensionMove12185); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExtensionMove"


    // $ANTLR start "ruleExtensionMove"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5414:1: ruleExtensionMove returns [EObject current=null] : (this_Photo_0= rulePhoto | this_Sound_1= ruleSound | this_Video_2= ruleVideo | this_Delay_3= ruleDelay ) ;
    public final EObject ruleExtensionMove() throws RecognitionException {
        EObject current = null;

        EObject this_Photo_0 = null;

        EObject this_Sound_1 = null;

        EObject this_Video_2 = null;

        EObject this_Delay_3 = null;


         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5417:28: ( (this_Photo_0= rulePhoto | this_Sound_1= ruleSound | this_Video_2= ruleVideo | this_Delay_3= ruleDelay ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5418:1: (this_Photo_0= rulePhoto | this_Sound_1= ruleSound | this_Video_2= ruleVideo | this_Delay_3= ruleDelay )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5418:1: (this_Photo_0= rulePhoto | this_Sound_1= ruleSound | this_Video_2= ruleVideo | this_Delay_3= ruleDelay )
            int alt62=4;
            switch ( input.LA(1) ) {
            case 102:
                {
                alt62=1;
                }
                break;
            case 103:
                {
                alt62=2;
                }
                break;
            case 104:
                {
                alt62=3;
                }
                break;
            case 105:
                {
                alt62=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 62, 0, input);

                throw nvae;
            }

            switch (alt62) {
                case 1 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5419:5: this_Photo_0= rulePhoto
                    {
                     
                            newCompositeNode(grammarAccess.getExtensionMoveAccess().getPhotoParserRuleCall_0()); 
                        
                    pushFollow(FOLLOW_rulePhoto_in_ruleExtensionMove12232);
                    this_Photo_0=rulePhoto();

                    state._fsp--;

                     
                            current = this_Photo_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5429:5: this_Sound_1= ruleSound
                    {
                     
                            newCompositeNode(grammarAccess.getExtensionMoveAccess().getSoundParserRuleCall_1()); 
                        
                    pushFollow(FOLLOW_ruleSound_in_ruleExtensionMove12259);
                    this_Sound_1=ruleSound();

                    state._fsp--;

                     
                            current = this_Sound_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 3 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5439:5: this_Video_2= ruleVideo
                    {
                     
                            newCompositeNode(grammarAccess.getExtensionMoveAccess().getVideoParserRuleCall_2()); 
                        
                    pushFollow(FOLLOW_ruleVideo_in_ruleExtensionMove12286);
                    this_Video_2=ruleVideo();

                    state._fsp--;

                     
                            current = this_Video_2; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 4 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5449:5: this_Delay_3= ruleDelay
                    {
                     
                            newCompositeNode(grammarAccess.getExtensionMoveAccess().getDelayParserRuleCall_3()); 
                        
                    pushFollow(FOLLOW_ruleDelay_in_ruleExtensionMove12313);
                    this_Delay_3=ruleDelay();

                    state._fsp--;

                     
                            current = this_Delay_3; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExtensionMove"


    // $ANTLR start "entryRulePhoto"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5465:1: entryRulePhoto returns [EObject current=null] : iv_rulePhoto= rulePhoto EOF ;
    public final EObject entryRulePhoto() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePhoto = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5466:2: (iv_rulePhoto= rulePhoto EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5467:2: iv_rulePhoto= rulePhoto EOF
            {
             newCompositeNode(grammarAccess.getPhotoRule()); 
            pushFollow(FOLLOW_rulePhoto_in_entryRulePhoto12348);
            iv_rulePhoto=rulePhoto();

            state._fsp--;

             current =iv_rulePhoto; 
            match(input,EOF,FOLLOW_EOF_in_entryRulePhoto12358); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePhoto"


    // $ANTLR start "rulePhoto"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5474:1: rulePhoto returns [EObject current=null] : ( ( (lv_name_0_0= 'photo' ) ) ( (lv_duration_1_0= ruleTimeLimit ) ) ( (lv_destfile_2_0= ruleMoveFile ) ) ( (lv_answerEvent_3_0= ruleAnswerEvent ) )? ) ;
    public final EObject rulePhoto() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        EObject lv_duration_1_0 = null;

        EObject lv_destfile_2_0 = null;

        EObject lv_answerEvent_3_0 = null;


         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5477:28: ( ( ( (lv_name_0_0= 'photo' ) ) ( (lv_duration_1_0= ruleTimeLimit ) ) ( (lv_destfile_2_0= ruleMoveFile ) ) ( (lv_answerEvent_3_0= ruleAnswerEvent ) )? ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5478:1: ( ( (lv_name_0_0= 'photo' ) ) ( (lv_duration_1_0= ruleTimeLimit ) ) ( (lv_destfile_2_0= ruleMoveFile ) ) ( (lv_answerEvent_3_0= ruleAnswerEvent ) )? )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5478:1: ( ( (lv_name_0_0= 'photo' ) ) ( (lv_duration_1_0= ruleTimeLimit ) ) ( (lv_destfile_2_0= ruleMoveFile ) ) ( (lv_answerEvent_3_0= ruleAnswerEvent ) )? )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5478:2: ( (lv_name_0_0= 'photo' ) ) ( (lv_duration_1_0= ruleTimeLimit ) ) ( (lv_destfile_2_0= ruleMoveFile ) ) ( (lv_answerEvent_3_0= ruleAnswerEvent ) )?
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5478:2: ( (lv_name_0_0= 'photo' ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5479:1: (lv_name_0_0= 'photo' )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5479:1: (lv_name_0_0= 'photo' )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5480:3: lv_name_0_0= 'photo'
            {
            lv_name_0_0=(Token)match(input,102,FOLLOW_102_in_rulePhoto12401); 

                    newLeafNode(lv_name_0_0, grammarAccess.getPhotoAccess().getNamePhotoKeyword_0_0());
                

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getPhotoRule());
            	        }
                   		setWithLastConsumed(current, "name", lv_name_0_0, "photo");
            	    

            }


            }

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5493:2: ( (lv_duration_1_0= ruleTimeLimit ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5494:1: (lv_duration_1_0= ruleTimeLimit )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5494:1: (lv_duration_1_0= ruleTimeLimit )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5495:3: lv_duration_1_0= ruleTimeLimit
            {
             
            	        newCompositeNode(grammarAccess.getPhotoAccess().getDurationTimeLimitParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_ruleTimeLimit_in_rulePhoto12435);
            lv_duration_1_0=ruleTimeLimit();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getPhotoRule());
            	        }
                   		set(
                   			current, 
                   			"duration",
                    		lv_duration_1_0, 
                    		"TimeLimit");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5511:2: ( (lv_destfile_2_0= ruleMoveFile ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5512:1: (lv_destfile_2_0= ruleMoveFile )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5512:1: (lv_destfile_2_0= ruleMoveFile )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5513:3: lv_destfile_2_0= ruleMoveFile
            {
             
            	        newCompositeNode(grammarAccess.getPhotoAccess().getDestfileMoveFileParserRuleCall_2_0()); 
            	    
            pushFollow(FOLLOW_ruleMoveFile_in_rulePhoto12456);
            lv_destfile_2_0=ruleMoveFile();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getPhotoRule());
            	        }
                   		set(
                   			current, 
                   			"destfile",
                    		lv_destfile_2_0, 
                    		"MoveFile");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5529:2: ( (lv_answerEvent_3_0= ruleAnswerEvent ) )?
            int alt63=2;
            int LA63_0 = input.LA(1);

            if ( (LA63_0==106) ) {
                alt63=1;
            }
            switch (alt63) {
                case 1 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5530:1: (lv_answerEvent_3_0= ruleAnswerEvent )
                    {
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5530:1: (lv_answerEvent_3_0= ruleAnswerEvent )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5531:3: lv_answerEvent_3_0= ruleAnswerEvent
                    {
                     
                    	        newCompositeNode(grammarAccess.getPhotoAccess().getAnswerEventAnswerEventParserRuleCall_3_0()); 
                    	    
                    pushFollow(FOLLOW_ruleAnswerEvent_in_rulePhoto12477);
                    lv_answerEvent_3_0=ruleAnswerEvent();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getPhotoRule());
                    	        }
                           		set(
                           			current, 
                           			"answerEvent",
                            		lv_answerEvent_3_0, 
                            		"AnswerEvent");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePhoto"


    // $ANTLR start "entryRuleSound"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5555:1: entryRuleSound returns [EObject current=null] : iv_ruleSound= ruleSound EOF ;
    public final EObject entryRuleSound() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSound = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5556:2: (iv_ruleSound= ruleSound EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5557:2: iv_ruleSound= ruleSound EOF
            {
             newCompositeNode(grammarAccess.getSoundRule()); 
            pushFollow(FOLLOW_ruleSound_in_entryRuleSound12514);
            iv_ruleSound=ruleSound();

            state._fsp--;

             current =iv_ruleSound; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleSound12524); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSound"


    // $ANTLR start "ruleSound"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5564:1: ruleSound returns [EObject current=null] : ( ( (lv_name_0_0= 'sound' ) ) ( (lv_duration_1_0= ruleTimeLimit ) ) ( (lv_srcfile_2_0= ruleMoveFile ) ) ( (lv_answerEvent_3_0= ruleAnswerEvent ) )? ) ;
    public final EObject ruleSound() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        EObject lv_duration_1_0 = null;

        EObject lv_srcfile_2_0 = null;

        EObject lv_answerEvent_3_0 = null;


         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5567:28: ( ( ( (lv_name_0_0= 'sound' ) ) ( (lv_duration_1_0= ruleTimeLimit ) ) ( (lv_srcfile_2_0= ruleMoveFile ) ) ( (lv_answerEvent_3_0= ruleAnswerEvent ) )? ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5568:1: ( ( (lv_name_0_0= 'sound' ) ) ( (lv_duration_1_0= ruleTimeLimit ) ) ( (lv_srcfile_2_0= ruleMoveFile ) ) ( (lv_answerEvent_3_0= ruleAnswerEvent ) )? )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5568:1: ( ( (lv_name_0_0= 'sound' ) ) ( (lv_duration_1_0= ruleTimeLimit ) ) ( (lv_srcfile_2_0= ruleMoveFile ) ) ( (lv_answerEvent_3_0= ruleAnswerEvent ) )? )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5568:2: ( (lv_name_0_0= 'sound' ) ) ( (lv_duration_1_0= ruleTimeLimit ) ) ( (lv_srcfile_2_0= ruleMoveFile ) ) ( (lv_answerEvent_3_0= ruleAnswerEvent ) )?
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5568:2: ( (lv_name_0_0= 'sound' ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5569:1: (lv_name_0_0= 'sound' )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5569:1: (lv_name_0_0= 'sound' )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5570:3: lv_name_0_0= 'sound'
            {
            lv_name_0_0=(Token)match(input,103,FOLLOW_103_in_ruleSound12567); 

                    newLeafNode(lv_name_0_0, grammarAccess.getSoundAccess().getNameSoundKeyword_0_0());
                

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getSoundRule());
            	        }
                   		setWithLastConsumed(current, "name", lv_name_0_0, "sound");
            	    

            }


            }

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5583:2: ( (lv_duration_1_0= ruleTimeLimit ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5584:1: (lv_duration_1_0= ruleTimeLimit )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5584:1: (lv_duration_1_0= ruleTimeLimit )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5585:3: lv_duration_1_0= ruleTimeLimit
            {
             
            	        newCompositeNode(grammarAccess.getSoundAccess().getDurationTimeLimitParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_ruleTimeLimit_in_ruleSound12601);
            lv_duration_1_0=ruleTimeLimit();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getSoundRule());
            	        }
                   		set(
                   			current, 
                   			"duration",
                    		lv_duration_1_0, 
                    		"TimeLimit");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5601:2: ( (lv_srcfile_2_0= ruleMoveFile ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5602:1: (lv_srcfile_2_0= ruleMoveFile )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5602:1: (lv_srcfile_2_0= ruleMoveFile )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5603:3: lv_srcfile_2_0= ruleMoveFile
            {
             
            	        newCompositeNode(grammarAccess.getSoundAccess().getSrcfileMoveFileParserRuleCall_2_0()); 
            	    
            pushFollow(FOLLOW_ruleMoveFile_in_ruleSound12622);
            lv_srcfile_2_0=ruleMoveFile();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getSoundRule());
            	        }
                   		set(
                   			current, 
                   			"srcfile",
                    		lv_srcfile_2_0, 
                    		"MoveFile");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5619:2: ( (lv_answerEvent_3_0= ruleAnswerEvent ) )?
            int alt64=2;
            int LA64_0 = input.LA(1);

            if ( (LA64_0==106) ) {
                alt64=1;
            }
            switch (alt64) {
                case 1 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5620:1: (lv_answerEvent_3_0= ruleAnswerEvent )
                    {
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5620:1: (lv_answerEvent_3_0= ruleAnswerEvent )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5621:3: lv_answerEvent_3_0= ruleAnswerEvent
                    {
                     
                    	        newCompositeNode(grammarAccess.getSoundAccess().getAnswerEventAnswerEventParserRuleCall_3_0()); 
                    	    
                    pushFollow(FOLLOW_ruleAnswerEvent_in_ruleSound12643);
                    lv_answerEvent_3_0=ruleAnswerEvent();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getSoundRule());
                    	        }
                           		set(
                           			current, 
                           			"answerEvent",
                            		lv_answerEvent_3_0, 
                            		"AnswerEvent");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSound"


    // $ANTLR start "entryRuleVideo"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5645:1: entryRuleVideo returns [EObject current=null] : iv_ruleVideo= ruleVideo EOF ;
    public final EObject entryRuleVideo() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVideo = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5646:2: (iv_ruleVideo= ruleVideo EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5647:2: iv_ruleVideo= ruleVideo EOF
            {
             newCompositeNode(grammarAccess.getVideoRule()); 
            pushFollow(FOLLOW_ruleVideo_in_entryRuleVideo12680);
            iv_ruleVideo=ruleVideo();

            state._fsp--;

             current =iv_ruleVideo; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleVideo12690); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVideo"


    // $ANTLR start "ruleVideo"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5654:1: ruleVideo returns [EObject current=null] : ( ( (lv_name_0_0= 'video' ) ) ( (lv_duration_1_0= ruleTimeLimit ) ) ( (lv_destfile_2_0= ruleMoveFile ) ) ( (lv_answerEvent_3_0= ruleAnswerEvent ) )? ) ;
    public final EObject ruleVideo() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        EObject lv_duration_1_0 = null;

        EObject lv_destfile_2_0 = null;

        EObject lv_answerEvent_3_0 = null;


         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5657:28: ( ( ( (lv_name_0_0= 'video' ) ) ( (lv_duration_1_0= ruleTimeLimit ) ) ( (lv_destfile_2_0= ruleMoveFile ) ) ( (lv_answerEvent_3_0= ruleAnswerEvent ) )? ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5658:1: ( ( (lv_name_0_0= 'video' ) ) ( (lv_duration_1_0= ruleTimeLimit ) ) ( (lv_destfile_2_0= ruleMoveFile ) ) ( (lv_answerEvent_3_0= ruleAnswerEvent ) )? )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5658:1: ( ( (lv_name_0_0= 'video' ) ) ( (lv_duration_1_0= ruleTimeLimit ) ) ( (lv_destfile_2_0= ruleMoveFile ) ) ( (lv_answerEvent_3_0= ruleAnswerEvent ) )? )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5658:2: ( (lv_name_0_0= 'video' ) ) ( (lv_duration_1_0= ruleTimeLimit ) ) ( (lv_destfile_2_0= ruleMoveFile ) ) ( (lv_answerEvent_3_0= ruleAnswerEvent ) )?
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5658:2: ( (lv_name_0_0= 'video' ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5659:1: (lv_name_0_0= 'video' )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5659:1: (lv_name_0_0= 'video' )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5660:3: lv_name_0_0= 'video'
            {
            lv_name_0_0=(Token)match(input,104,FOLLOW_104_in_ruleVideo12733); 

                    newLeafNode(lv_name_0_0, grammarAccess.getVideoAccess().getNameVideoKeyword_0_0());
                

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getVideoRule());
            	        }
                   		setWithLastConsumed(current, "name", lv_name_0_0, "video");
            	    

            }


            }

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5673:2: ( (lv_duration_1_0= ruleTimeLimit ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5674:1: (lv_duration_1_0= ruleTimeLimit )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5674:1: (lv_duration_1_0= ruleTimeLimit )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5675:3: lv_duration_1_0= ruleTimeLimit
            {
             
            	        newCompositeNode(grammarAccess.getVideoAccess().getDurationTimeLimitParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_ruleTimeLimit_in_ruleVideo12767);
            lv_duration_1_0=ruleTimeLimit();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getVideoRule());
            	        }
                   		set(
                   			current, 
                   			"duration",
                    		lv_duration_1_0, 
                    		"TimeLimit");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5691:2: ( (lv_destfile_2_0= ruleMoveFile ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5692:1: (lv_destfile_2_0= ruleMoveFile )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5692:1: (lv_destfile_2_0= ruleMoveFile )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5693:3: lv_destfile_2_0= ruleMoveFile
            {
             
            	        newCompositeNode(grammarAccess.getVideoAccess().getDestfileMoveFileParserRuleCall_2_0()); 
            	    
            pushFollow(FOLLOW_ruleMoveFile_in_ruleVideo12788);
            lv_destfile_2_0=ruleMoveFile();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getVideoRule());
            	        }
                   		set(
                   			current, 
                   			"destfile",
                    		lv_destfile_2_0, 
                    		"MoveFile");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5709:2: ( (lv_answerEvent_3_0= ruleAnswerEvent ) )?
            int alt65=2;
            int LA65_0 = input.LA(1);

            if ( (LA65_0==106) ) {
                alt65=1;
            }
            switch (alt65) {
                case 1 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5710:1: (lv_answerEvent_3_0= ruleAnswerEvent )
                    {
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5710:1: (lv_answerEvent_3_0= ruleAnswerEvent )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5711:3: lv_answerEvent_3_0= ruleAnswerEvent
                    {
                     
                    	        newCompositeNode(grammarAccess.getVideoAccess().getAnswerEventAnswerEventParserRuleCall_3_0()); 
                    	    
                    pushFollow(FOLLOW_ruleAnswerEvent_in_ruleVideo12809);
                    lv_answerEvent_3_0=ruleAnswerEvent();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getVideoRule());
                    	        }
                           		set(
                           			current, 
                           			"answerEvent",
                            		lv_answerEvent_3_0, 
                            		"AnswerEvent");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVideo"


    // $ANTLR start "entryRuleDelay"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5735:1: entryRuleDelay returns [EObject current=null] : iv_ruleDelay= ruleDelay EOF ;
    public final EObject entryRuleDelay() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDelay = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5736:2: (iv_ruleDelay= ruleDelay EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5737:2: iv_ruleDelay= ruleDelay EOF
            {
             newCompositeNode(grammarAccess.getDelayRule()); 
            pushFollow(FOLLOW_ruleDelay_in_entryRuleDelay12846);
            iv_ruleDelay=ruleDelay();

            state._fsp--;

             current =iv_ruleDelay; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleDelay12856); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDelay"


    // $ANTLR start "ruleDelay"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5744:1: ruleDelay returns [EObject current=null] : ( ( (lv_name_0_0= 'delay' ) ) ( (lv_duration_1_0= ruleTimeLimit ) ) ) ;
    public final EObject ruleDelay() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        EObject lv_duration_1_0 = null;


         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5747:28: ( ( ( (lv_name_0_0= 'delay' ) ) ( (lv_duration_1_0= ruleTimeLimit ) ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5748:1: ( ( (lv_name_0_0= 'delay' ) ) ( (lv_duration_1_0= ruleTimeLimit ) ) )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5748:1: ( ( (lv_name_0_0= 'delay' ) ) ( (lv_duration_1_0= ruleTimeLimit ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5748:2: ( (lv_name_0_0= 'delay' ) ) ( (lv_duration_1_0= ruleTimeLimit ) )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5748:2: ( (lv_name_0_0= 'delay' ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5749:1: (lv_name_0_0= 'delay' )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5749:1: (lv_name_0_0= 'delay' )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5750:3: lv_name_0_0= 'delay'
            {
            lv_name_0_0=(Token)match(input,105,FOLLOW_105_in_ruleDelay12899); 

                    newLeafNode(lv_name_0_0, grammarAccess.getDelayAccess().getNameDelayKeyword_0_0());
                

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getDelayRule());
            	        }
                   		setWithLastConsumed(current, "name", lv_name_0_0, "delay");
            	    

            }


            }

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5763:2: ( (lv_duration_1_0= ruleTimeLimit ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5764:1: (lv_duration_1_0= ruleTimeLimit )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5764:1: (lv_duration_1_0= ruleTimeLimit )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5765:3: lv_duration_1_0= ruleTimeLimit
            {
             
            	        newCompositeNode(grammarAccess.getDelayAccess().getDurationTimeLimitParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_ruleTimeLimit_in_ruleDelay12933);
            lv_duration_1_0=ruleTimeLimit();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getDelayRule());
            	        }
                   		set(
                   			current, 
                   			"duration",
                    		lv_duration_1_0, 
                    		"TimeLimit");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDelay"


    // $ANTLR start "entryRuleAnswerEvent"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5789:1: entryRuleAnswerEvent returns [EObject current=null] : iv_ruleAnswerEvent= ruleAnswerEvent EOF ;
    public final EObject entryRuleAnswerEvent() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAnswerEvent = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5790:2: (iv_ruleAnswerEvent= ruleAnswerEvent EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5791:2: iv_ruleAnswerEvent= ruleAnswerEvent EOF
            {
             newCompositeNode(grammarAccess.getAnswerEventRule()); 
            pushFollow(FOLLOW_ruleAnswerEvent_in_entryRuleAnswerEvent12969);
            iv_ruleAnswerEvent=ruleAnswerEvent();

            state._fsp--;

             current =iv_ruleAnswerEvent; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleAnswerEvent12979); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAnswerEvent"


    // $ANTLR start "ruleAnswerEvent"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5798:1: ruleAnswerEvent returns [EObject current=null] : (otherlv_0= 'answerEv' ( (lv_name_1_0= RULE_ID ) ) ) ;
    public final EObject ruleAnswerEvent() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;

         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5801:28: ( (otherlv_0= 'answerEv' ( (lv_name_1_0= RULE_ID ) ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5802:1: (otherlv_0= 'answerEv' ( (lv_name_1_0= RULE_ID ) ) )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5802:1: (otherlv_0= 'answerEv' ( (lv_name_1_0= RULE_ID ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5802:3: otherlv_0= 'answerEv' ( (lv_name_1_0= RULE_ID ) )
            {
            otherlv_0=(Token)match(input,106,FOLLOW_106_in_ruleAnswerEvent13016); 

                	newLeafNode(otherlv_0, grammarAccess.getAnswerEventAccess().getAnswerEvKeyword_0());
                
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5806:1: ( (lv_name_1_0= RULE_ID ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5807:1: (lv_name_1_0= RULE_ID )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5807:1: (lv_name_1_0= RULE_ID )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5808:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleAnswerEvent13033); 

            			newLeafNode(lv_name_1_0, grammarAccess.getAnswerEventAccess().getNameIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getAnswerEventRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"ID");
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAnswerEvent"


    // $ANTLR start "entryRuleEventHandler"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5832:1: entryRuleEventHandler returns [EObject current=null] : iv_ruleEventHandler= ruleEventHandler EOF ;
    public final EObject entryRuleEventHandler() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEventHandler = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5833:2: (iv_ruleEventHandler= ruleEventHandler EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5834:2: iv_ruleEventHandler= ruleEventHandler EOF
            {
             newCompositeNode(grammarAccess.getEventHandlerRule()); 
            pushFollow(FOLLOW_ruleEventHandler_in_entryRuleEventHandler13074);
            iv_ruleEventHandler=ruleEventHandler();

            state._fsp--;

             current =iv_ruleEventHandler; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleEventHandler13084); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEventHandler"


    // $ANTLR start "ruleEventHandler"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5841:1: ruleEventHandler returns [EObject current=null] : (otherlv_0= 'EventHandler' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= 'for' ( (otherlv_3= RULE_ID ) ) (otherlv_4= ',' ( (otherlv_5= RULE_ID ) ) )* )? ( (lv_print_6_0= '-print' ) )? (otherlv_7= '{' ( (lv_body_8_0= ruleEventHandlerBody ) ) otherlv_9= '}' )? otherlv_10= ';' ) ;
    public final EObject ruleEventHandler() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token lv_print_6_0=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        EObject lv_body_8_0 = null;


         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5844:28: ( (otherlv_0= 'EventHandler' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= 'for' ( (otherlv_3= RULE_ID ) ) (otherlv_4= ',' ( (otherlv_5= RULE_ID ) ) )* )? ( (lv_print_6_0= '-print' ) )? (otherlv_7= '{' ( (lv_body_8_0= ruleEventHandlerBody ) ) otherlv_9= '}' )? otherlv_10= ';' ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5845:1: (otherlv_0= 'EventHandler' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= 'for' ( (otherlv_3= RULE_ID ) ) (otherlv_4= ',' ( (otherlv_5= RULE_ID ) ) )* )? ( (lv_print_6_0= '-print' ) )? (otherlv_7= '{' ( (lv_body_8_0= ruleEventHandlerBody ) ) otherlv_9= '}' )? otherlv_10= ';' )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5845:1: (otherlv_0= 'EventHandler' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= 'for' ( (otherlv_3= RULE_ID ) ) (otherlv_4= ',' ( (otherlv_5= RULE_ID ) ) )* )? ( (lv_print_6_0= '-print' ) )? (otherlv_7= '{' ( (lv_body_8_0= ruleEventHandlerBody ) ) otherlv_9= '}' )? otherlv_10= ';' )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5845:3: otherlv_0= 'EventHandler' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= 'for' ( (otherlv_3= RULE_ID ) ) (otherlv_4= ',' ( (otherlv_5= RULE_ID ) ) )* )? ( (lv_print_6_0= '-print' ) )? (otherlv_7= '{' ( (lv_body_8_0= ruleEventHandlerBody ) ) otherlv_9= '}' )? otherlv_10= ';'
            {
            otherlv_0=(Token)match(input,107,FOLLOW_107_in_ruleEventHandler13121); 

                	newLeafNode(otherlv_0, grammarAccess.getEventHandlerAccess().getEventHandlerKeyword_0());
                
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5849:1: ( (lv_name_1_0= RULE_ID ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5850:1: (lv_name_1_0= RULE_ID )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5850:1: (lv_name_1_0= RULE_ID )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5851:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleEventHandler13138); 

            			newLeafNode(lv_name_1_0, grammarAccess.getEventHandlerAccess().getNameIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getEventHandlerRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"ID");
            	    

            }


            }

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5867:2: (otherlv_2= 'for' ( (otherlv_3= RULE_ID ) ) (otherlv_4= ',' ( (otherlv_5= RULE_ID ) ) )* )?
            int alt67=2;
            int LA67_0 = input.LA(1);

            if ( (LA67_0==108) ) {
                alt67=1;
            }
            switch (alt67) {
                case 1 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5867:4: otherlv_2= 'for' ( (otherlv_3= RULE_ID ) ) (otherlv_4= ',' ( (otherlv_5= RULE_ID ) ) )*
                    {
                    otherlv_2=(Token)match(input,108,FOLLOW_108_in_ruleEventHandler13156); 

                        	newLeafNode(otherlv_2, grammarAccess.getEventHandlerAccess().getForKeyword_2_0());
                        
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5871:1: ( (otherlv_3= RULE_ID ) )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5872:1: (otherlv_3= RULE_ID )
                    {
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5872:1: (otherlv_3= RULE_ID )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5873:3: otherlv_3= RULE_ID
                    {

                    			if (current==null) {
                    	            current = createModelElement(grammarAccess.getEventHandlerRule());
                    	        }
                            
                    otherlv_3=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleEventHandler13176); 

                    		newLeafNode(otherlv_3, grammarAccess.getEventHandlerAccess().getEventsEventCrossReference_2_1_0()); 
                    	

                    }


                    }

                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5884:2: (otherlv_4= ',' ( (otherlv_5= RULE_ID ) ) )*
                    loop66:
                    do {
                        int alt66=2;
                        int LA66_0 = input.LA(1);

                        if ( (LA66_0==33) ) {
                            alt66=1;
                        }


                        switch (alt66) {
                    	case 1 :
                    	    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5884:4: otherlv_4= ',' ( (otherlv_5= RULE_ID ) )
                    	    {
                    	    otherlv_4=(Token)match(input,33,FOLLOW_33_in_ruleEventHandler13189); 

                    	        	newLeafNode(otherlv_4, grammarAccess.getEventHandlerAccess().getCommaKeyword_2_2_0());
                    	        
                    	    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5888:1: ( (otherlv_5= RULE_ID ) )
                    	    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5889:1: (otherlv_5= RULE_ID )
                    	    {
                    	    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5889:1: (otherlv_5= RULE_ID )
                    	    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5890:3: otherlv_5= RULE_ID
                    	    {

                    	    			if (current==null) {
                    	    	            current = createModelElement(grammarAccess.getEventHandlerRule());
                    	    	        }
                    	            
                    	    otherlv_5=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleEventHandler13209); 

                    	    		newLeafNode(otherlv_5, grammarAccess.getEventHandlerAccess().getEventsEventCrossReference_2_2_1_0()); 
                    	    	

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop66;
                        }
                    } while (true);


                    }
                    break;

            }

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5901:6: ( (lv_print_6_0= '-print' ) )?
            int alt68=2;
            int LA68_0 = input.LA(1);

            if ( (LA68_0==109) ) {
                alt68=1;
            }
            switch (alt68) {
                case 1 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5902:1: (lv_print_6_0= '-print' )
                    {
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5902:1: (lv_print_6_0= '-print' )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5903:3: lv_print_6_0= '-print'
                    {
                    lv_print_6_0=(Token)match(input,109,FOLLOW_109_in_ruleEventHandler13231); 

                            newLeafNode(lv_print_6_0, grammarAccess.getEventHandlerAccess().getPrintPrintKeyword_3_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getEventHandlerRule());
                    	        }
                           		setWithLastConsumed(current, "print", true, "-print");
                    	    

                    }


                    }
                    break;

            }

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5916:3: (otherlv_7= '{' ( (lv_body_8_0= ruleEventHandlerBody ) ) otherlv_9= '}' )?
            int alt69=2;
            int LA69_0 = input.LA(1);

            if ( (LA69_0==29) ) {
                alt69=1;
            }
            switch (alt69) {
                case 1 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5916:5: otherlv_7= '{' ( (lv_body_8_0= ruleEventHandlerBody ) ) otherlv_9= '}'
                    {
                    otherlv_7=(Token)match(input,29,FOLLOW_29_in_ruleEventHandler13258); 

                        	newLeafNode(otherlv_7, grammarAccess.getEventHandlerAccess().getLeftCurlyBracketKeyword_4_0());
                        
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5920:1: ( (lv_body_8_0= ruleEventHandlerBody ) )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5921:1: (lv_body_8_0= ruleEventHandlerBody )
                    {
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5921:1: (lv_body_8_0= ruleEventHandlerBody )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5922:3: lv_body_8_0= ruleEventHandlerBody
                    {
                     
                    	        newCompositeNode(grammarAccess.getEventHandlerAccess().getBodyEventHandlerBodyParserRuleCall_4_1_0()); 
                    	    
                    pushFollow(FOLLOW_ruleEventHandlerBody_in_ruleEventHandler13279);
                    lv_body_8_0=ruleEventHandlerBody();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getEventHandlerRule());
                    	        }
                           		set(
                           			current, 
                           			"body",
                            		lv_body_8_0, 
                            		"EventHandlerBody");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    otherlv_9=(Token)match(input,31,FOLLOW_31_in_ruleEventHandler13291); 

                        	newLeafNode(otherlv_9, grammarAccess.getEventHandlerAccess().getRightCurlyBracketKeyword_4_2());
                        

                    }
                    break;

            }

            otherlv_10=(Token)match(input,49,FOLLOW_49_in_ruleEventHandler13305); 

                	newLeafNode(otherlv_10, grammarAccess.getEventHandlerAccess().getSemicolonKeyword_5());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEventHandler"


    // $ANTLR start "entryRuleEventHandlerBody"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5954:1: entryRuleEventHandlerBody returns [EObject current=null] : iv_ruleEventHandlerBody= ruleEventHandlerBody EOF ;
    public final EObject entryRuleEventHandlerBody() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEventHandlerBody = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5955:2: (iv_ruleEventHandlerBody= ruleEventHandlerBody EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5956:2: iv_ruleEventHandlerBody= ruleEventHandlerBody EOF
            {
             newCompositeNode(grammarAccess.getEventHandlerBodyRule()); 
            pushFollow(FOLLOW_ruleEventHandlerBody_in_entryRuleEventHandlerBody13341);
            iv_ruleEventHandlerBody=ruleEventHandlerBody();

            state._fsp--;

             current =iv_ruleEventHandlerBody; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleEventHandlerBody13351); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEventHandlerBody"


    // $ANTLR start "ruleEventHandlerBody"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5963:1: ruleEventHandlerBody returns [EObject current=null] : ( ( (lv_op_0_0= ruleEventHandlerOperation ) ) (otherlv_1= ';' ( (lv_op_2_0= ruleEventHandlerOperation ) ) )* ) ;
    public final EObject ruleEventHandlerBody() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        EObject lv_op_0_0 = null;

        EObject lv_op_2_0 = null;


         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5966:28: ( ( ( (lv_op_0_0= ruleEventHandlerOperation ) ) (otherlv_1= ';' ( (lv_op_2_0= ruleEventHandlerOperation ) ) )* ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5967:1: ( ( (lv_op_0_0= ruleEventHandlerOperation ) ) (otherlv_1= ';' ( (lv_op_2_0= ruleEventHandlerOperation ) ) )* )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5967:1: ( ( (lv_op_0_0= ruleEventHandlerOperation ) ) (otherlv_1= ';' ( (lv_op_2_0= ruleEventHandlerOperation ) ) )* )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5967:2: ( (lv_op_0_0= ruleEventHandlerOperation ) ) (otherlv_1= ';' ( (lv_op_2_0= ruleEventHandlerOperation ) ) )*
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5967:2: ( (lv_op_0_0= ruleEventHandlerOperation ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5968:1: (lv_op_0_0= ruleEventHandlerOperation )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5968:1: (lv_op_0_0= ruleEventHandlerOperation )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5969:3: lv_op_0_0= ruleEventHandlerOperation
            {
             
            	        newCompositeNode(grammarAccess.getEventHandlerBodyAccess().getOpEventHandlerOperationParserRuleCall_0_0()); 
            	    
            pushFollow(FOLLOW_ruleEventHandlerOperation_in_ruleEventHandlerBody13397);
            lv_op_0_0=ruleEventHandlerOperation();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getEventHandlerBodyRule());
            	        }
                   		add(
                   			current, 
                   			"op",
                    		lv_op_0_0, 
                    		"EventHandlerOperation");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5985:2: (otherlv_1= ';' ( (lv_op_2_0= ruleEventHandlerOperation ) ) )*
            loop70:
            do {
                int alt70=2;
                int LA70_0 = input.LA(1);

                if ( (LA70_0==49) ) {
                    alt70=1;
                }


                switch (alt70) {
            	case 1 :
            	    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5985:4: otherlv_1= ';' ( (lv_op_2_0= ruleEventHandlerOperation ) )
            	    {
            	    otherlv_1=(Token)match(input,49,FOLLOW_49_in_ruleEventHandlerBody13410); 

            	        	newLeafNode(otherlv_1, grammarAccess.getEventHandlerBodyAccess().getSemicolonKeyword_1_0());
            	        
            	    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5989:1: ( (lv_op_2_0= ruleEventHandlerOperation ) )
            	    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5990:1: (lv_op_2_0= ruleEventHandlerOperation )
            	    {
            	    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5990:1: (lv_op_2_0= ruleEventHandlerOperation )
            	    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:5991:3: lv_op_2_0= ruleEventHandlerOperation
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getEventHandlerBodyAccess().getOpEventHandlerOperationParserRuleCall_1_1_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleEventHandlerOperation_in_ruleEventHandlerBody13431);
            	    lv_op_2_0=ruleEventHandlerOperation();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getEventHandlerBodyRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"op",
            	            		lv_op_2_0, 
            	            		"EventHandlerOperation");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop70;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEventHandlerBody"


    // $ANTLR start "entryRuleEventHandlerOperation"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6015:1: entryRuleEventHandlerOperation returns [EObject current=null] : iv_ruleEventHandlerOperation= ruleEventHandlerOperation EOF ;
    public final EObject entryRuleEventHandlerOperation() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEventHandlerOperation = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6016:2: (iv_ruleEventHandlerOperation= ruleEventHandlerOperation EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6017:2: iv_ruleEventHandlerOperation= ruleEventHandlerOperation EOF
            {
             newCompositeNode(grammarAccess.getEventHandlerOperationRule()); 
            pushFollow(FOLLOW_ruleEventHandlerOperation_in_entryRuleEventHandlerOperation13469);
            iv_ruleEventHandlerOperation=ruleEventHandlerOperation();

            state._fsp--;

             current =iv_ruleEventHandlerOperation; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleEventHandlerOperation13479); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEventHandlerOperation"


    // $ANTLR start "ruleEventHandlerOperation"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6024:1: ruleEventHandlerOperation returns [EObject current=null] : (this_MemoOperation_0= ruleMemoOperation | this_SolveOperation_1= ruleSolveOperation | this_RaiseEvent_2= ruleRaiseEvent | this_SendEventAsDispatch_3= ruleSendEventAsDispatch ) ;
    public final EObject ruleEventHandlerOperation() throws RecognitionException {
        EObject current = null;

        EObject this_MemoOperation_0 = null;

        EObject this_SolveOperation_1 = null;

        EObject this_RaiseEvent_2 = null;

        EObject this_SendEventAsDispatch_3 = null;


         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6027:28: ( (this_MemoOperation_0= ruleMemoOperation | this_SolveOperation_1= ruleSolveOperation | this_RaiseEvent_2= ruleRaiseEvent | this_SendEventAsDispatch_3= ruleSendEventAsDispatch ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6028:1: (this_MemoOperation_0= ruleMemoOperation | this_SolveOperation_1= ruleSolveOperation | this_RaiseEvent_2= ruleRaiseEvent | this_SendEventAsDispatch_3= ruleSendEventAsDispatch )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6028:1: (this_MemoOperation_0= ruleMemoOperation | this_SolveOperation_1= ruleSolveOperation | this_RaiseEvent_2= ruleRaiseEvent | this_SendEventAsDispatch_3= ruleSendEventAsDispatch )
            int alt71=4;
            switch ( input.LA(1) ) {
            case 71:
            case 110:
                {
                alt71=1;
                }
                break;
            case 62:
                {
                alt71=2;
                }
                break;
            case 97:
                {
                alt71=3;
                }
                break;
            case 111:
                {
                alt71=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 71, 0, input);

                throw nvae;
            }

            switch (alt71) {
                case 1 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6029:5: this_MemoOperation_0= ruleMemoOperation
                    {
                     
                            newCompositeNode(grammarAccess.getEventHandlerOperationAccess().getMemoOperationParserRuleCall_0()); 
                        
                    pushFollow(FOLLOW_ruleMemoOperation_in_ruleEventHandlerOperation13526);
                    this_MemoOperation_0=ruleMemoOperation();

                    state._fsp--;

                     
                            current = this_MemoOperation_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6039:5: this_SolveOperation_1= ruleSolveOperation
                    {
                     
                            newCompositeNode(grammarAccess.getEventHandlerOperationAccess().getSolveOperationParserRuleCall_1()); 
                        
                    pushFollow(FOLLOW_ruleSolveOperation_in_ruleEventHandlerOperation13553);
                    this_SolveOperation_1=ruleSolveOperation();

                    state._fsp--;

                     
                            current = this_SolveOperation_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 3 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6049:5: this_RaiseEvent_2= ruleRaiseEvent
                    {
                     
                            newCompositeNode(grammarAccess.getEventHandlerOperationAccess().getRaiseEventParserRuleCall_2()); 
                        
                    pushFollow(FOLLOW_ruleRaiseEvent_in_ruleEventHandlerOperation13580);
                    this_RaiseEvent_2=ruleRaiseEvent();

                    state._fsp--;

                     
                            current = this_RaiseEvent_2; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 4 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6059:5: this_SendEventAsDispatch_3= ruleSendEventAsDispatch
                    {
                     
                            newCompositeNode(grammarAccess.getEventHandlerOperationAccess().getSendEventAsDispatchParserRuleCall_3()); 
                        
                    pushFollow(FOLLOW_ruleSendEventAsDispatch_in_ruleEventHandlerOperation13607);
                    this_SendEventAsDispatch_3=ruleSendEventAsDispatch();

                    state._fsp--;

                     
                            current = this_SendEventAsDispatch_3; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEventHandlerOperation"


    // $ANTLR start "entryRuleMemoOperation"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6075:1: entryRuleMemoOperation returns [EObject current=null] : iv_ruleMemoOperation= ruleMemoOperation EOF ;
    public final EObject entryRuleMemoOperation() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMemoOperation = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6076:2: (iv_ruleMemoOperation= ruleMemoOperation EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6077:2: iv_ruleMemoOperation= ruleMemoOperation EOF
            {
             newCompositeNode(grammarAccess.getMemoOperationRule()); 
            pushFollow(FOLLOW_ruleMemoOperation_in_entryRuleMemoOperation13642);
            iv_ruleMemoOperation=ruleMemoOperation();

            state._fsp--;

             current =iv_ruleMemoOperation; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleMemoOperation13652); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMemoOperation"


    // $ANTLR start "ruleMemoOperation"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6084:1: ruleMemoOperation returns [EObject current=null] : ( (otherlv_0= 'memo' ( (lv_rule_1_0= ruleMemoRule ) ) otherlv_2= 'for' ( (otherlv_3= RULE_ID ) ) ) | ( ( (lv_doMemo_4_0= ruleMemoCurrentEvent ) ) otherlv_5= 'for' ( (otherlv_6= RULE_ID ) ) ) ) ;
    public final EObject ruleMemoOperation() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        EObject lv_rule_1_0 = null;

        EObject lv_doMemo_4_0 = null;


         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6087:28: ( ( (otherlv_0= 'memo' ( (lv_rule_1_0= ruleMemoRule ) ) otherlv_2= 'for' ( (otherlv_3= RULE_ID ) ) ) | ( ( (lv_doMemo_4_0= ruleMemoCurrentEvent ) ) otherlv_5= 'for' ( (otherlv_6= RULE_ID ) ) ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6088:1: ( (otherlv_0= 'memo' ( (lv_rule_1_0= ruleMemoRule ) ) otherlv_2= 'for' ( (otherlv_3= RULE_ID ) ) ) | ( ( (lv_doMemo_4_0= ruleMemoCurrentEvent ) ) otherlv_5= 'for' ( (otherlv_6= RULE_ID ) ) ) )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6088:1: ( (otherlv_0= 'memo' ( (lv_rule_1_0= ruleMemoRule ) ) otherlv_2= 'for' ( (otherlv_3= RULE_ID ) ) ) | ( ( (lv_doMemo_4_0= ruleMemoCurrentEvent ) ) otherlv_5= 'for' ( (otherlv_6= RULE_ID ) ) ) )
            int alt72=2;
            int LA72_0 = input.LA(1);

            if ( (LA72_0==110) ) {
                alt72=1;
            }
            else if ( (LA72_0==71) ) {
                alt72=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 72, 0, input);

                throw nvae;
            }
            switch (alt72) {
                case 1 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6088:2: (otherlv_0= 'memo' ( (lv_rule_1_0= ruleMemoRule ) ) otherlv_2= 'for' ( (otherlv_3= RULE_ID ) ) )
                    {
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6088:2: (otherlv_0= 'memo' ( (lv_rule_1_0= ruleMemoRule ) ) otherlv_2= 'for' ( (otherlv_3= RULE_ID ) ) )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6088:4: otherlv_0= 'memo' ( (lv_rule_1_0= ruleMemoRule ) ) otherlv_2= 'for' ( (otherlv_3= RULE_ID ) )
                    {
                    otherlv_0=(Token)match(input,110,FOLLOW_110_in_ruleMemoOperation13690); 

                        	newLeafNode(otherlv_0, grammarAccess.getMemoOperationAccess().getMemoKeyword_0_0());
                        
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6092:1: ( (lv_rule_1_0= ruleMemoRule ) )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6093:1: (lv_rule_1_0= ruleMemoRule )
                    {
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6093:1: (lv_rule_1_0= ruleMemoRule )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6094:3: lv_rule_1_0= ruleMemoRule
                    {
                     
                    	        newCompositeNode(grammarAccess.getMemoOperationAccess().getRuleMemoRuleParserRuleCall_0_1_0()); 
                    	    
                    pushFollow(FOLLOW_ruleMemoRule_in_ruleMemoOperation13711);
                    lv_rule_1_0=ruleMemoRule();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getMemoOperationRule());
                    	        }
                           		set(
                           			current, 
                           			"rule",
                            		lv_rule_1_0, 
                            		"MemoRule");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    otherlv_2=(Token)match(input,108,FOLLOW_108_in_ruleMemoOperation13723); 

                        	newLeafNode(otherlv_2, grammarAccess.getMemoOperationAccess().getForKeyword_0_2());
                        
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6114:1: ( (otherlv_3= RULE_ID ) )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6115:1: (otherlv_3= RULE_ID )
                    {
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6115:1: (otherlv_3= RULE_ID )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6116:3: otherlv_3= RULE_ID
                    {

                    			if (current==null) {
                    	            current = createModelElement(grammarAccess.getMemoOperationRule());
                    	        }
                            
                    otherlv_3=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleMemoOperation13743); 

                    		newLeafNode(otherlv_3, grammarAccess.getMemoOperationAccess().getActorQActorCrossReference_0_3_0()); 
                    	

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6128:6: ( ( (lv_doMemo_4_0= ruleMemoCurrentEvent ) ) otherlv_5= 'for' ( (otherlv_6= RULE_ID ) ) )
                    {
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6128:6: ( ( (lv_doMemo_4_0= ruleMemoCurrentEvent ) ) otherlv_5= 'for' ( (otherlv_6= RULE_ID ) ) )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6128:7: ( (lv_doMemo_4_0= ruleMemoCurrentEvent ) ) otherlv_5= 'for' ( (otherlv_6= RULE_ID ) )
                    {
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6128:7: ( (lv_doMemo_4_0= ruleMemoCurrentEvent ) )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6129:1: (lv_doMemo_4_0= ruleMemoCurrentEvent )
                    {
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6129:1: (lv_doMemo_4_0= ruleMemoCurrentEvent )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6130:3: lv_doMemo_4_0= ruleMemoCurrentEvent
                    {
                     
                    	        newCompositeNode(grammarAccess.getMemoOperationAccess().getDoMemoMemoCurrentEventParserRuleCall_1_0_0()); 
                    	    
                    pushFollow(FOLLOW_ruleMemoCurrentEvent_in_ruleMemoOperation13772);
                    lv_doMemo_4_0=ruleMemoCurrentEvent();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getMemoOperationRule());
                    	        }
                           		set(
                           			current, 
                           			"doMemo",
                            		lv_doMemo_4_0, 
                            		"MemoCurrentEvent");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    otherlv_5=(Token)match(input,108,FOLLOW_108_in_ruleMemoOperation13784); 

                        	newLeafNode(otherlv_5, grammarAccess.getMemoOperationAccess().getForKeyword_1_1());
                        
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6150:1: ( (otherlv_6= RULE_ID ) )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6151:1: (otherlv_6= RULE_ID )
                    {
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6151:1: (otherlv_6= RULE_ID )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6152:3: otherlv_6= RULE_ID
                    {

                    			if (current==null) {
                    	            current = createModelElement(grammarAccess.getMemoOperationRule());
                    	        }
                            
                    otherlv_6=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleMemoOperation13804); 

                    		newLeafNode(otherlv_6, grammarAccess.getMemoOperationAccess().getActorQActorCrossReference_1_2_0()); 
                    	

                    }


                    }


                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMemoOperation"


    // $ANTLR start "entryRuleSolveOperation"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6171:1: entryRuleSolveOperation returns [EObject current=null] : iv_ruleSolveOperation= ruleSolveOperation EOF ;
    public final EObject entryRuleSolveOperation() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSolveOperation = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6172:2: (iv_ruleSolveOperation= ruleSolveOperation EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6173:2: iv_ruleSolveOperation= ruleSolveOperation EOF
            {
             newCompositeNode(grammarAccess.getSolveOperationRule()); 
            pushFollow(FOLLOW_ruleSolveOperation_in_entryRuleSolveOperation13841);
            iv_ruleSolveOperation=ruleSolveOperation();

            state._fsp--;

             current =iv_ruleSolveOperation; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleSolveOperation13851); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSolveOperation"


    // $ANTLR start "ruleSolveOperation"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6180:1: ruleSolveOperation returns [EObject current=null] : (otherlv_0= 'solve' ( (lv_goal_1_0= rulePTerm ) ) otherlv_2= 'for' ( (otherlv_3= RULE_ID ) ) ) ;
    public final EObject ruleSolveOperation() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        EObject lv_goal_1_0 = null;


         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6183:28: ( (otherlv_0= 'solve' ( (lv_goal_1_0= rulePTerm ) ) otherlv_2= 'for' ( (otherlv_3= RULE_ID ) ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6184:1: (otherlv_0= 'solve' ( (lv_goal_1_0= rulePTerm ) ) otherlv_2= 'for' ( (otherlv_3= RULE_ID ) ) )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6184:1: (otherlv_0= 'solve' ( (lv_goal_1_0= rulePTerm ) ) otherlv_2= 'for' ( (otherlv_3= RULE_ID ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6184:3: otherlv_0= 'solve' ( (lv_goal_1_0= rulePTerm ) ) otherlv_2= 'for' ( (otherlv_3= RULE_ID ) )
            {
            otherlv_0=(Token)match(input,62,FOLLOW_62_in_ruleSolveOperation13888); 

                	newLeafNode(otherlv_0, grammarAccess.getSolveOperationAccess().getSolveKeyword_0());
                
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6188:1: ( (lv_goal_1_0= rulePTerm ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6189:1: (lv_goal_1_0= rulePTerm )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6189:1: (lv_goal_1_0= rulePTerm )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6190:3: lv_goal_1_0= rulePTerm
            {
             
            	        newCompositeNode(grammarAccess.getSolveOperationAccess().getGoalPTermParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_rulePTerm_in_ruleSolveOperation13909);
            lv_goal_1_0=rulePTerm();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getSolveOperationRule());
            	        }
                   		set(
                   			current, 
                   			"goal",
                    		lv_goal_1_0, 
                    		"PTerm");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_2=(Token)match(input,108,FOLLOW_108_in_ruleSolveOperation13921); 

                	newLeafNode(otherlv_2, grammarAccess.getSolveOperationAccess().getForKeyword_2());
                
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6210:1: ( (otherlv_3= RULE_ID ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6211:1: (otherlv_3= RULE_ID )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6211:1: (otherlv_3= RULE_ID )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6212:3: otherlv_3= RULE_ID
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getSolveOperationRule());
            	        }
                    
            otherlv_3=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleSolveOperation13941); 

            		newLeafNode(otherlv_3, grammarAccess.getSolveOperationAccess().getActorQActorCrossReference_3_0()); 
            	

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSolveOperation"


    // $ANTLR start "entryRuleSendEventAsDispatch"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6231:1: entryRuleSendEventAsDispatch returns [EObject current=null] : iv_ruleSendEventAsDispatch= ruleSendEventAsDispatch EOF ;
    public final EObject entryRuleSendEventAsDispatch() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSendEventAsDispatch = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6232:2: (iv_ruleSendEventAsDispatch= ruleSendEventAsDispatch EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6233:2: iv_ruleSendEventAsDispatch= ruleSendEventAsDispatch EOF
            {
             newCompositeNode(grammarAccess.getSendEventAsDispatchRule()); 
            pushFollow(FOLLOW_ruleSendEventAsDispatch_in_entryRuleSendEventAsDispatch13977);
            iv_ruleSendEventAsDispatch=ruleSendEventAsDispatch();

            state._fsp--;

             current =iv_ruleSendEventAsDispatch; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleSendEventAsDispatch13987); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSendEventAsDispatch"


    // $ANTLR start "ruleSendEventAsDispatch"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6240:1: ruleSendEventAsDispatch returns [EObject current=null] : (otherlv_0= 'forwardEvent' ( (otherlv_1= RULE_ID ) ) otherlv_2= '-m' ( (otherlv_3= RULE_ID ) ) ) ;
    public final EObject ruleSendEventAsDispatch() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;

         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6243:28: ( (otherlv_0= 'forwardEvent' ( (otherlv_1= RULE_ID ) ) otherlv_2= '-m' ( (otherlv_3= RULE_ID ) ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6244:1: (otherlv_0= 'forwardEvent' ( (otherlv_1= RULE_ID ) ) otherlv_2= '-m' ( (otherlv_3= RULE_ID ) ) )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6244:1: (otherlv_0= 'forwardEvent' ( (otherlv_1= RULE_ID ) ) otherlv_2= '-m' ( (otherlv_3= RULE_ID ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6244:3: otherlv_0= 'forwardEvent' ( (otherlv_1= RULE_ID ) ) otherlv_2= '-m' ( (otherlv_3= RULE_ID ) )
            {
            otherlv_0=(Token)match(input,111,FOLLOW_111_in_ruleSendEventAsDispatch14024); 

                	newLeafNode(otherlv_0, grammarAccess.getSendEventAsDispatchAccess().getForwardEventKeyword_0());
                
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6248:1: ( (otherlv_1= RULE_ID ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6249:1: (otherlv_1= RULE_ID )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6249:1: (otherlv_1= RULE_ID )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6250:3: otherlv_1= RULE_ID
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getSendEventAsDispatchRule());
            	        }
                    
            otherlv_1=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleSendEventAsDispatch14044); 

            		newLeafNode(otherlv_1, grammarAccess.getSendEventAsDispatchAccess().getActorQActorCrossReference_1_0()); 
            	

            }


            }

            otherlv_2=(Token)match(input,86,FOLLOW_86_in_ruleSendEventAsDispatch14056); 

                	newLeafNode(otherlv_2, grammarAccess.getSendEventAsDispatchAccess().getMKeyword_2());
                
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6265:1: ( (otherlv_3= RULE_ID ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6266:1: (otherlv_3= RULE_ID )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6266:1: (otherlv_3= RULE_ID )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6267:3: otherlv_3= RULE_ID
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getSendEventAsDispatchRule());
            	        }
                    
            otherlv_3=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleSendEventAsDispatch14076); 

            		newLeafNode(otherlv_3, grammarAccess.getSendEventAsDispatchAccess().getMsgrefMessageCrossReference_3_0()); 
            	

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSendEventAsDispatch"


    // $ANTLR start "entryRuleMemoRule"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6286:1: entryRuleMemoRule returns [EObject current=null] : iv_ruleMemoRule= ruleMemoRule EOF ;
    public final EObject entryRuleMemoRule() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMemoRule = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6287:2: (iv_ruleMemoRule= ruleMemoRule EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6288:2: iv_ruleMemoRule= ruleMemoRule EOF
            {
             newCompositeNode(grammarAccess.getMemoRuleRule()); 
            pushFollow(FOLLOW_ruleMemoRule_in_entryRuleMemoRule14112);
            iv_ruleMemoRule=ruleMemoRule();

            state._fsp--;

             current =iv_ruleMemoRule; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleMemoRule14122); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMemoRule"


    // $ANTLR start "ruleMemoRule"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6295:1: ruleMemoRule returns [EObject current=null] : this_MemoEvent_0= ruleMemoEvent ;
    public final EObject ruleMemoRule() throws RecognitionException {
        EObject current = null;

        EObject this_MemoEvent_0 = null;


         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6298:28: (this_MemoEvent_0= ruleMemoEvent )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6300:5: this_MemoEvent_0= ruleMemoEvent
            {
             
                    newCompositeNode(grammarAccess.getMemoRuleAccess().getMemoEventParserRuleCall()); 
                
            pushFollow(FOLLOW_ruleMemoEvent_in_ruleMemoRule14168);
            this_MemoEvent_0=ruleMemoEvent();

            state._fsp--;

             
                    current = this_MemoEvent_0; 
                    afterParserOrEnumRuleCall();
                

            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMemoRule"


    // $ANTLR start "entryRuleMemoEvent"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6316:1: entryRuleMemoEvent returns [EObject current=null] : iv_ruleMemoEvent= ruleMemoEvent EOF ;
    public final EObject entryRuleMemoEvent() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMemoEvent = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6317:2: (iv_ruleMemoEvent= ruleMemoEvent EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6318:2: iv_ruleMemoEvent= ruleMemoEvent EOF
            {
             newCompositeNode(grammarAccess.getMemoEventRule()); 
            pushFollow(FOLLOW_ruleMemoEvent_in_entryRuleMemoEvent14202);
            iv_ruleMemoEvent=ruleMemoEvent();

            state._fsp--;

             current =iv_ruleMemoEvent; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleMemoEvent14212); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMemoEvent"


    // $ANTLR start "ruleMemoEvent"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6325:1: ruleMemoEvent returns [EObject current=null] : ( (lv_name_0_0= 'currentEvent' ) ) ;
    public final EObject ruleMemoEvent() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;

         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6328:28: ( ( (lv_name_0_0= 'currentEvent' ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6329:1: ( (lv_name_0_0= 'currentEvent' ) )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6329:1: ( (lv_name_0_0= 'currentEvent' ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6330:1: (lv_name_0_0= 'currentEvent' )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6330:1: (lv_name_0_0= 'currentEvent' )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6331:3: lv_name_0_0= 'currentEvent'
            {
            lv_name_0_0=(Token)match(input,112,FOLLOW_112_in_ruleMemoEvent14254); 

                    newLeafNode(lv_name_0_0, grammarAccess.getMemoEventAccess().getNameCurrentEventKeyword_0());
                

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getMemoEventRule());
            	        }
                   		setWithLastConsumed(current, "name", lv_name_0_0, "currentEvent");
            	    

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMemoEvent"


    // $ANTLR start "entryRuleReaction"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6352:1: entryRuleReaction returns [EObject current=null] : iv_ruleReaction= ruleReaction EOF ;
    public final EObject entryRuleReaction() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleReaction = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6353:2: (iv_ruleReaction= ruleReaction EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6354:2: iv_ruleReaction= ruleReaction EOF
            {
             newCompositeNode(grammarAccess.getReactionRule()); 
            pushFollow(FOLLOW_ruleReaction_in_entryRuleReaction14302);
            iv_ruleReaction=ruleReaction();

            state._fsp--;

             current =iv_ruleReaction; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleReaction14312); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleReaction"


    // $ANTLR start "ruleReaction"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6361:1: ruleReaction returns [EObject current=null] : ( ( (lv_name_0_0= 'react' ) ) ( (lv_alarms_1_0= ruleAlarmEvent ) ) (otherlv_2= 'or' ( (lv_alarms_3_0= ruleAlarmEvent ) ) )* ) ;
    public final EObject ruleReaction() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_2=null;
        EObject lv_alarms_1_0 = null;

        EObject lv_alarms_3_0 = null;


         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6364:28: ( ( ( (lv_name_0_0= 'react' ) ) ( (lv_alarms_1_0= ruleAlarmEvent ) ) (otherlv_2= 'or' ( (lv_alarms_3_0= ruleAlarmEvent ) ) )* ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6365:1: ( ( (lv_name_0_0= 'react' ) ) ( (lv_alarms_1_0= ruleAlarmEvent ) ) (otherlv_2= 'or' ( (lv_alarms_3_0= ruleAlarmEvent ) ) )* )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6365:1: ( ( (lv_name_0_0= 'react' ) ) ( (lv_alarms_1_0= ruleAlarmEvent ) ) (otherlv_2= 'or' ( (lv_alarms_3_0= ruleAlarmEvent ) ) )* )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6365:2: ( (lv_name_0_0= 'react' ) ) ( (lv_alarms_1_0= ruleAlarmEvent ) ) (otherlv_2= 'or' ( (lv_alarms_3_0= ruleAlarmEvent ) ) )*
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6365:2: ( (lv_name_0_0= 'react' ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6366:1: (lv_name_0_0= 'react' )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6366:1: (lv_name_0_0= 'react' )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6367:3: lv_name_0_0= 'react'
            {
            lv_name_0_0=(Token)match(input,113,FOLLOW_113_in_ruleReaction14355); 

                    newLeafNode(lv_name_0_0, grammarAccess.getReactionAccess().getNameReactKeyword_0_0());
                

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getReactionRule());
            	        }
                   		setWithLastConsumed(current, "name", lv_name_0_0, "react");
            	    

            }


            }

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6380:2: ( (lv_alarms_1_0= ruleAlarmEvent ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6381:1: (lv_alarms_1_0= ruleAlarmEvent )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6381:1: (lv_alarms_1_0= ruleAlarmEvent )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6382:3: lv_alarms_1_0= ruleAlarmEvent
            {
             
            	        newCompositeNode(grammarAccess.getReactionAccess().getAlarmsAlarmEventParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_ruleAlarmEvent_in_ruleReaction14389);
            lv_alarms_1_0=ruleAlarmEvent();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getReactionRule());
            	        }
                   		add(
                   			current, 
                   			"alarms",
                    		lv_alarms_1_0, 
                    		"AlarmEvent");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6398:2: (otherlv_2= 'or' ( (lv_alarms_3_0= ruleAlarmEvent ) ) )*
            loop73:
            do {
                int alt73=2;
                int LA73_0 = input.LA(1);

                if ( (LA73_0==114) ) {
                    alt73=1;
                }


                switch (alt73) {
            	case 1 :
            	    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6398:4: otherlv_2= 'or' ( (lv_alarms_3_0= ruleAlarmEvent ) )
            	    {
            	    otherlv_2=(Token)match(input,114,FOLLOW_114_in_ruleReaction14402); 

            	        	newLeafNode(otherlv_2, grammarAccess.getReactionAccess().getOrKeyword_2_0());
            	        
            	    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6402:1: ( (lv_alarms_3_0= ruleAlarmEvent ) )
            	    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6403:1: (lv_alarms_3_0= ruleAlarmEvent )
            	    {
            	    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6403:1: (lv_alarms_3_0= ruleAlarmEvent )
            	    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6404:3: lv_alarms_3_0= ruleAlarmEvent
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getReactionAccess().getAlarmsAlarmEventParserRuleCall_2_1_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleAlarmEvent_in_ruleReaction14423);
            	    lv_alarms_3_0=ruleAlarmEvent();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getReactionRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"alarms",
            	            		lv_alarms_3_0, 
            	            		"AlarmEvent");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop73;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleReaction"


    // $ANTLR start "entryRuleAlarmEvent"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6428:1: entryRuleAlarmEvent returns [EObject current=null] : iv_ruleAlarmEvent= ruleAlarmEvent EOF ;
    public final EObject entryRuleAlarmEvent() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAlarmEvent = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6429:2: (iv_ruleAlarmEvent= ruleAlarmEvent EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6430:2: iv_ruleAlarmEvent= ruleAlarmEvent EOF
            {
             newCompositeNode(grammarAccess.getAlarmEventRule()); 
            pushFollow(FOLLOW_ruleAlarmEvent_in_entryRuleAlarmEvent14461);
            iv_ruleAlarmEvent=ruleAlarmEvent();

            state._fsp--;

             current =iv_ruleAlarmEvent; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleAlarmEvent14471); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAlarmEvent"


    // $ANTLR start "ruleAlarmEvent"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6437:1: ruleAlarmEvent returns [EObject current=null] : (this_NormalEvent_0= ruleNormalEvent | this_ContinueEvent_1= ruleContinueEvent ) ;
    public final EObject ruleAlarmEvent() throws RecognitionException {
        EObject current = null;

        EObject this_NormalEvent_0 = null;

        EObject this_ContinueEvent_1 = null;


         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6440:28: ( (this_NormalEvent_0= ruleNormalEvent | this_ContinueEvent_1= ruleContinueEvent ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6441:1: (this_NormalEvent_0= ruleNormalEvent | this_ContinueEvent_1= ruleContinueEvent )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6441:1: (this_NormalEvent_0= ruleNormalEvent | this_ContinueEvent_1= ruleContinueEvent )
            int alt74=2;
            int LA74_0 = input.LA(1);

            if ( (LA74_0==115) ) {
                alt74=1;
            }
            else if ( (LA74_0==116) ) {
                alt74=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 74, 0, input);

                throw nvae;
            }
            switch (alt74) {
                case 1 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6442:5: this_NormalEvent_0= ruleNormalEvent
                    {
                     
                            newCompositeNode(grammarAccess.getAlarmEventAccess().getNormalEventParserRuleCall_0()); 
                        
                    pushFollow(FOLLOW_ruleNormalEvent_in_ruleAlarmEvent14518);
                    this_NormalEvent_0=ruleNormalEvent();

                    state._fsp--;

                     
                            current = this_NormalEvent_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6452:5: this_ContinueEvent_1= ruleContinueEvent
                    {
                     
                            newCompositeNode(grammarAccess.getAlarmEventAccess().getContinueEventParserRuleCall_1()); 
                        
                    pushFollow(FOLLOW_ruleContinueEvent_in_ruleAlarmEvent14545);
                    this_ContinueEvent_1=ruleContinueEvent();

                    state._fsp--;

                     
                            current = this_ContinueEvent_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAlarmEvent"


    // $ANTLR start "entryRuleNormalEvent"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6468:1: entryRuleNormalEvent returns [EObject current=null] : iv_ruleNormalEvent= ruleNormalEvent EOF ;
    public final EObject entryRuleNormalEvent() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNormalEvent = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6469:2: (iv_ruleNormalEvent= ruleNormalEvent EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6470:2: iv_ruleNormalEvent= ruleNormalEvent EOF
            {
             newCompositeNode(grammarAccess.getNormalEventRule()); 
            pushFollow(FOLLOW_ruleNormalEvent_in_entryRuleNormalEvent14580);
            iv_ruleNormalEvent=ruleNormalEvent();

            state._fsp--;

             current =iv_ruleNormalEvent; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleNormalEvent14590); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNormalEvent"


    // $ANTLR start "ruleNormalEvent"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6477:1: ruleNormalEvent returns [EObject current=null] : ( ( (lv_name_0_0= 'event' ) ) ( (otherlv_1= RULE_ID ) ) otherlv_2= '->' ( (otherlv_3= RULE_ID ) ) ) ;
    public final EObject ruleNormalEvent() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;

         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6480:28: ( ( ( (lv_name_0_0= 'event' ) ) ( (otherlv_1= RULE_ID ) ) otherlv_2= '->' ( (otherlv_3= RULE_ID ) ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6481:1: ( ( (lv_name_0_0= 'event' ) ) ( (otherlv_1= RULE_ID ) ) otherlv_2= '->' ( (otherlv_3= RULE_ID ) ) )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6481:1: ( ( (lv_name_0_0= 'event' ) ) ( (otherlv_1= RULE_ID ) ) otherlv_2= '->' ( (otherlv_3= RULE_ID ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6481:2: ( (lv_name_0_0= 'event' ) ) ( (otherlv_1= RULE_ID ) ) otherlv_2= '->' ( (otherlv_3= RULE_ID ) )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6481:2: ( (lv_name_0_0= 'event' ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6482:1: (lv_name_0_0= 'event' )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6482:1: (lv_name_0_0= 'event' )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6483:3: lv_name_0_0= 'event'
            {
            lv_name_0_0=(Token)match(input,115,FOLLOW_115_in_ruleNormalEvent14633); 

                    newLeafNode(lv_name_0_0, grammarAccess.getNormalEventAccess().getNameEventKeyword_0_0());
                

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getNormalEventRule());
            	        }
                   		setWithLastConsumed(current, "name", lv_name_0_0, "event");
            	    

            }


            }

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6496:2: ( (otherlv_1= RULE_ID ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6497:1: (otherlv_1= RULE_ID )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6497:1: (otherlv_1= RULE_ID )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6498:3: otherlv_1= RULE_ID
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getNormalEventRule());
            	        }
                    
            otherlv_1=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleNormalEvent14666); 

            		newLeafNode(otherlv_1, grammarAccess.getNormalEventAccess().getEvEventCrossReference_1_0()); 
            	

            }


            }

            otherlv_2=(Token)match(input,96,FOLLOW_96_in_ruleNormalEvent14678); 

                	newLeafNode(otherlv_2, grammarAccess.getNormalEventAccess().getHyphenMinusGreaterThanSignKeyword_2());
                
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6513:1: ( (otherlv_3= RULE_ID ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6514:1: (otherlv_3= RULE_ID )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6514:1: (otherlv_3= RULE_ID )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6515:3: otherlv_3= RULE_ID
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getNormalEventRule());
            	        }
                    
            otherlv_3=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleNormalEvent14698); 

            		newLeafNode(otherlv_3, grammarAccess.getNormalEventAccess().getPlanRefPlanCrossReference_3_0()); 
            	

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNormalEvent"


    // $ANTLR start "entryRuleContinueEvent"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6534:1: entryRuleContinueEvent returns [EObject current=null] : iv_ruleContinueEvent= ruleContinueEvent EOF ;
    public final EObject entryRuleContinueEvent() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleContinueEvent = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6535:2: (iv_ruleContinueEvent= ruleContinueEvent EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6536:2: iv_ruleContinueEvent= ruleContinueEvent EOF
            {
             newCompositeNode(grammarAccess.getContinueEventRule()); 
            pushFollow(FOLLOW_ruleContinueEvent_in_entryRuleContinueEvent14734);
            iv_ruleContinueEvent=ruleContinueEvent();

            state._fsp--;

             current =iv_ruleContinueEvent; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleContinueEvent14744); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleContinueEvent"


    // $ANTLR start "ruleContinueEvent"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6543:1: ruleContinueEvent returns [EObject current=null] : ( ( (lv_name_0_0= 'when' ) ) ( (otherlv_1= RULE_ID ) ) ) ;
    public final EObject ruleContinueEvent() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_1=null;

         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6546:28: ( ( ( (lv_name_0_0= 'when' ) ) ( (otherlv_1= RULE_ID ) ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6547:1: ( ( (lv_name_0_0= 'when' ) ) ( (otherlv_1= RULE_ID ) ) )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6547:1: ( ( (lv_name_0_0= 'when' ) ) ( (otherlv_1= RULE_ID ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6547:2: ( (lv_name_0_0= 'when' ) ) ( (otherlv_1= RULE_ID ) )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6547:2: ( (lv_name_0_0= 'when' ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6548:1: (lv_name_0_0= 'when' )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6548:1: (lv_name_0_0= 'when' )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6549:3: lv_name_0_0= 'when'
            {
            lv_name_0_0=(Token)match(input,116,FOLLOW_116_in_ruleContinueEvent14787); 

                    newLeafNode(lv_name_0_0, grammarAccess.getContinueEventAccess().getNameWhenKeyword_0_0());
                

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getContinueEventRule());
            	        }
                   		setWithLastConsumed(current, "name", lv_name_0_0, "when");
            	    

            }


            }

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6562:2: ( (otherlv_1= RULE_ID ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6563:1: (otherlv_1= RULE_ID )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6563:1: (otherlv_1= RULE_ID )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6564:3: otherlv_1= RULE_ID
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getContinueEventRule());
            	        }
                    
            otherlv_1=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleContinueEvent14820); 

            		newLeafNode(otherlv_1, grammarAccess.getContinueEventAccess().getEvOccurEventCrossReference_1_0()); 
            	

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleContinueEvent"


    // $ANTLR start "entryRuleVarOrQactor"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6583:1: entryRuleVarOrQactor returns [EObject current=null] : iv_ruleVarOrQactor= ruleVarOrQactor EOF ;
    public final EObject entryRuleVarOrQactor() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVarOrQactor = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6584:2: (iv_ruleVarOrQactor= ruleVarOrQactor EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6585:2: iv_ruleVarOrQactor= ruleVarOrQactor EOF
            {
             newCompositeNode(grammarAccess.getVarOrQactorRule()); 
            pushFollow(FOLLOW_ruleVarOrQactor_in_entryRuleVarOrQactor14856);
            iv_ruleVarOrQactor=ruleVarOrQactor();

            state._fsp--;

             current =iv_ruleVarOrQactor; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleVarOrQactor14866); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVarOrQactor"


    // $ANTLR start "ruleVarOrQactor"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6592:1: ruleVarOrQactor returns [EObject current=null] : ( ( (lv_var_0_0= ruleVariable ) ) | ( (otherlv_1= RULE_ID ) ) ) ;
    public final EObject ruleVarOrQactor() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        EObject lv_var_0_0 = null;


         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6595:28: ( ( ( (lv_var_0_0= ruleVariable ) ) | ( (otherlv_1= RULE_ID ) ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6596:1: ( ( (lv_var_0_0= ruleVariable ) ) | ( (otherlv_1= RULE_ID ) ) )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6596:1: ( ( (lv_var_0_0= ruleVariable ) ) | ( (otherlv_1= RULE_ID ) ) )
            int alt75=2;
            int LA75_0 = input.LA(1);

            if ( (LA75_0==RULE_VARID||LA75_0==117) ) {
                alt75=1;
            }
            else if ( (LA75_0==RULE_ID) ) {
                alt75=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 75, 0, input);

                throw nvae;
            }
            switch (alt75) {
                case 1 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6596:2: ( (lv_var_0_0= ruleVariable ) )
                    {
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6596:2: ( (lv_var_0_0= ruleVariable ) )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6597:1: (lv_var_0_0= ruleVariable )
                    {
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6597:1: (lv_var_0_0= ruleVariable )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6598:3: lv_var_0_0= ruleVariable
                    {
                     
                    	        newCompositeNode(grammarAccess.getVarOrQactorAccess().getVarVariableParserRuleCall_0_0()); 
                    	    
                    pushFollow(FOLLOW_ruleVariable_in_ruleVarOrQactor14912);
                    lv_var_0_0=ruleVariable();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getVarOrQactorRule());
                    	        }
                           		set(
                           			current, 
                           			"var",
                            		lv_var_0_0, 
                            		"Variable");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;
                case 2 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6615:6: ( (otherlv_1= RULE_ID ) )
                    {
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6615:6: ( (otherlv_1= RULE_ID ) )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6616:1: (otherlv_1= RULE_ID )
                    {
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6616:1: (otherlv_1= RULE_ID )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6617:3: otherlv_1= RULE_ID
                    {

                    			if (current==null) {
                    	            current = createModelElement(grammarAccess.getVarOrQactorRule());
                    	        }
                            
                    otherlv_1=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleVarOrQactor14938); 

                    		newLeafNode(otherlv_1, grammarAccess.getVarOrQactorAccess().getDestQActorCrossReference_1_0()); 
                    	

                    }


                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVarOrQactor"


    // $ANTLR start "entryRuleVarOrAtomic"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6636:1: entryRuleVarOrAtomic returns [EObject current=null] : iv_ruleVarOrAtomic= ruleVarOrAtomic EOF ;
    public final EObject entryRuleVarOrAtomic() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVarOrAtomic = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6637:2: (iv_ruleVarOrAtomic= ruleVarOrAtomic EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6638:2: iv_ruleVarOrAtomic= ruleVarOrAtomic EOF
            {
             newCompositeNode(grammarAccess.getVarOrAtomicRule()); 
            pushFollow(FOLLOW_ruleVarOrAtomic_in_entryRuleVarOrAtomic14974);
            iv_ruleVarOrAtomic=ruleVarOrAtomic();

            state._fsp--;

             current =iv_ruleVarOrAtomic; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleVarOrAtomic14984); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVarOrAtomic"


    // $ANTLR start "ruleVarOrAtomic"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6645:1: ruleVarOrAtomic returns [EObject current=null] : ( ( (lv_var_0_0= ruleVariable ) ) | ( (lv_const_1_0= rulePAtomic ) ) ) ;
    public final EObject ruleVarOrAtomic() throws RecognitionException {
        EObject current = null;

        EObject lv_var_0_0 = null;

        EObject lv_const_1_0 = null;


         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6648:28: ( ( ( (lv_var_0_0= ruleVariable ) ) | ( (lv_const_1_0= rulePAtomic ) ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6649:1: ( ( (lv_var_0_0= ruleVariable ) ) | ( (lv_const_1_0= rulePAtomic ) ) )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6649:1: ( ( (lv_var_0_0= ruleVariable ) ) | ( (lv_const_1_0= rulePAtomic ) ) )
            int alt76=2;
            int LA76_0 = input.LA(1);

            if ( (LA76_0==RULE_VARID||LA76_0==117) ) {
                alt76=1;
            }
            else if ( (LA76_0==RULE_ID) ) {
                alt76=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 76, 0, input);

                throw nvae;
            }
            switch (alt76) {
                case 1 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6649:2: ( (lv_var_0_0= ruleVariable ) )
                    {
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6649:2: ( (lv_var_0_0= ruleVariable ) )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6650:1: (lv_var_0_0= ruleVariable )
                    {
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6650:1: (lv_var_0_0= ruleVariable )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6651:3: lv_var_0_0= ruleVariable
                    {
                     
                    	        newCompositeNode(grammarAccess.getVarOrAtomicAccess().getVarVariableParserRuleCall_0_0()); 
                    	    
                    pushFollow(FOLLOW_ruleVariable_in_ruleVarOrAtomic15030);
                    lv_var_0_0=ruleVariable();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getVarOrAtomicRule());
                    	        }
                           		set(
                           			current, 
                           			"var",
                            		lv_var_0_0, 
                            		"Variable");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;
                case 2 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6668:6: ( (lv_const_1_0= rulePAtomic ) )
                    {
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6668:6: ( (lv_const_1_0= rulePAtomic ) )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6669:1: (lv_const_1_0= rulePAtomic )
                    {
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6669:1: (lv_const_1_0= rulePAtomic )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6670:3: lv_const_1_0= rulePAtomic
                    {
                     
                    	        newCompositeNode(grammarAccess.getVarOrAtomicAccess().getConstPAtomicParserRuleCall_1_0()); 
                    	    
                    pushFollow(FOLLOW_rulePAtomic_in_ruleVarOrAtomic15057);
                    lv_const_1_0=rulePAtomic();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getVarOrAtomicRule());
                    	        }
                           		set(
                           			current, 
                           			"const",
                            		lv_const_1_0, 
                            		"PAtomic");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVarOrAtomic"


    // $ANTLR start "entryRuleVarOrString"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6694:1: entryRuleVarOrString returns [EObject current=null] : iv_ruleVarOrString= ruleVarOrString EOF ;
    public final EObject entryRuleVarOrString() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVarOrString = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6695:2: (iv_ruleVarOrString= ruleVarOrString EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6696:2: iv_ruleVarOrString= ruleVarOrString EOF
            {
             newCompositeNode(grammarAccess.getVarOrStringRule()); 
            pushFollow(FOLLOW_ruleVarOrString_in_entryRuleVarOrString15093);
            iv_ruleVarOrString=ruleVarOrString();

            state._fsp--;

             current =iv_ruleVarOrString; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleVarOrString15103); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVarOrString"


    // $ANTLR start "ruleVarOrString"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6703:1: ruleVarOrString returns [EObject current=null] : ( ( (lv_var_0_0= ruleVariable ) ) | ( (lv_const_1_0= RULE_STRING ) ) ) ;
    public final EObject ruleVarOrString() throws RecognitionException {
        EObject current = null;

        Token lv_const_1_0=null;
        EObject lv_var_0_0 = null;


         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6706:28: ( ( ( (lv_var_0_0= ruleVariable ) ) | ( (lv_const_1_0= RULE_STRING ) ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6707:1: ( ( (lv_var_0_0= ruleVariable ) ) | ( (lv_const_1_0= RULE_STRING ) ) )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6707:1: ( ( (lv_var_0_0= ruleVariable ) ) | ( (lv_const_1_0= RULE_STRING ) ) )
            int alt77=2;
            int LA77_0 = input.LA(1);

            if ( (LA77_0==RULE_VARID||LA77_0==117) ) {
                alt77=1;
            }
            else if ( (LA77_0==RULE_STRING) ) {
                alt77=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 77, 0, input);

                throw nvae;
            }
            switch (alt77) {
                case 1 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6707:2: ( (lv_var_0_0= ruleVariable ) )
                    {
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6707:2: ( (lv_var_0_0= ruleVariable ) )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6708:1: (lv_var_0_0= ruleVariable )
                    {
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6708:1: (lv_var_0_0= ruleVariable )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6709:3: lv_var_0_0= ruleVariable
                    {
                     
                    	        newCompositeNode(grammarAccess.getVarOrStringAccess().getVarVariableParserRuleCall_0_0()); 
                    	    
                    pushFollow(FOLLOW_ruleVariable_in_ruleVarOrString15149);
                    lv_var_0_0=ruleVariable();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getVarOrStringRule());
                    	        }
                           		set(
                           			current, 
                           			"var",
                            		lv_var_0_0, 
                            		"Variable");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;
                case 2 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6726:6: ( (lv_const_1_0= RULE_STRING ) )
                    {
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6726:6: ( (lv_const_1_0= RULE_STRING ) )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6727:1: (lv_const_1_0= RULE_STRING )
                    {
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6727:1: (lv_const_1_0= RULE_STRING )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6728:3: lv_const_1_0= RULE_STRING
                    {
                    lv_const_1_0=(Token)match(input,RULE_STRING,FOLLOW_RULE_STRING_in_ruleVarOrString15172); 

                    			newLeafNode(lv_const_1_0, grammarAccess.getVarOrStringAccess().getConstSTRINGTerminalRuleCall_1_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getVarOrStringRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"const",
                            		lv_const_1_0, 
                            		"STRING");
                    	    

                    }


                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVarOrString"


    // $ANTLR start "entryRuleVariable"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6758:1: entryRuleVariable returns [EObject current=null] : iv_ruleVariable= ruleVariable EOF ;
    public final EObject entryRuleVariable() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVariable = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6759:2: (iv_ruleVariable= ruleVariable EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6760:2: iv_ruleVariable= ruleVariable EOF
            {
             newCompositeNode(grammarAccess.getVariableRule()); 
            pushFollow(FOLLOW_ruleVariable_in_entryRuleVariable15219);
            iv_ruleVariable=ruleVariable();

            state._fsp--;

             current =iv_ruleVariable; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleVariable15229); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVariable"


    // $ANTLR start "ruleVariable"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6767:1: ruleVariable returns [EObject current=null] : ( ( (lv_guardvar_0_0= '$' ) )? ( (lv_name_1_0= RULE_VARID ) ) ) ;
    public final EObject ruleVariable() throws RecognitionException {
        EObject current = null;

        Token lv_guardvar_0_0=null;
        Token lv_name_1_0=null;

         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6770:28: ( ( ( (lv_guardvar_0_0= '$' ) )? ( (lv_name_1_0= RULE_VARID ) ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6771:1: ( ( (lv_guardvar_0_0= '$' ) )? ( (lv_name_1_0= RULE_VARID ) ) )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6771:1: ( ( (lv_guardvar_0_0= '$' ) )? ( (lv_name_1_0= RULE_VARID ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6771:2: ( (lv_guardvar_0_0= '$' ) )? ( (lv_name_1_0= RULE_VARID ) )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6771:2: ( (lv_guardvar_0_0= '$' ) )?
            int alt78=2;
            int LA78_0 = input.LA(1);

            if ( (LA78_0==117) ) {
                alt78=1;
            }
            switch (alt78) {
                case 1 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6772:1: (lv_guardvar_0_0= '$' )
                    {
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6772:1: (lv_guardvar_0_0= '$' )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6773:3: lv_guardvar_0_0= '$'
                    {
                    lv_guardvar_0_0=(Token)match(input,117,FOLLOW_117_in_ruleVariable15272); 

                            newLeafNode(lv_guardvar_0_0, grammarAccess.getVariableAccess().getGuardvarDollarSignKeyword_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getVariableRule());
                    	        }
                           		setWithLastConsumed(current, "guardvar", true, "$");
                    	    

                    }


                    }
                    break;

            }

            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6786:3: ( (lv_name_1_0= RULE_VARID ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6787:1: (lv_name_1_0= RULE_VARID )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6787:1: (lv_name_1_0= RULE_VARID )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6788:3: lv_name_1_0= RULE_VARID
            {
            lv_name_1_0=(Token)match(input,RULE_VARID,FOLLOW_RULE_VARID_in_ruleVariable15303); 

            			newLeafNode(lv_name_1_0, grammarAccess.getVariableAccess().getNameVARIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getVariableRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"VARID");
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVariable"


    // $ANTLR start "entryRuleTimeLimit"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6812:1: entryRuleTimeLimit returns [EObject current=null] : iv_ruleTimeLimit= ruleTimeLimit EOF ;
    public final EObject entryRuleTimeLimit() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTimeLimit = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6813:2: (iv_ruleTimeLimit= ruleTimeLimit EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6814:2: iv_ruleTimeLimit= ruleTimeLimit EOF
            {
             newCompositeNode(grammarAccess.getTimeLimitRule()); 
            pushFollow(FOLLOW_ruleTimeLimit_in_entryRuleTimeLimit15344);
            iv_ruleTimeLimit=ruleTimeLimit();

            state._fsp--;

             current =iv_ruleTimeLimit; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleTimeLimit15354); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTimeLimit"


    // $ANTLR start "ruleTimeLimit"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6821:1: ruleTimeLimit returns [EObject current=null] : ( ( (lv_name_0_0= 'time' ) ) otherlv_1= '(' ( ( (lv_msec_2_0= RULE_INT ) ) | ( (lv_var_3_0= ruleVariable ) ) ) otherlv_4= ')' ) ;
    public final EObject ruleTimeLimit() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_1=null;
        Token lv_msec_2_0=null;
        Token otherlv_4=null;
        EObject lv_var_3_0 = null;


         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6824:28: ( ( ( (lv_name_0_0= 'time' ) ) otherlv_1= '(' ( ( (lv_msec_2_0= RULE_INT ) ) | ( (lv_var_3_0= ruleVariable ) ) ) otherlv_4= ')' ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6825:1: ( ( (lv_name_0_0= 'time' ) ) otherlv_1= '(' ( ( (lv_msec_2_0= RULE_INT ) ) | ( (lv_var_3_0= ruleVariable ) ) ) otherlv_4= ')' )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6825:1: ( ( (lv_name_0_0= 'time' ) ) otherlv_1= '(' ( ( (lv_msec_2_0= RULE_INT ) ) | ( (lv_var_3_0= ruleVariable ) ) ) otherlv_4= ')' )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6825:2: ( (lv_name_0_0= 'time' ) ) otherlv_1= '(' ( ( (lv_msec_2_0= RULE_INT ) ) | ( (lv_var_3_0= ruleVariable ) ) ) otherlv_4= ')'
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6825:2: ( (lv_name_0_0= 'time' ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6826:1: (lv_name_0_0= 'time' )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6826:1: (lv_name_0_0= 'time' )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6827:3: lv_name_0_0= 'time'
            {
            lv_name_0_0=(Token)match(input,118,FOLLOW_118_in_ruleTimeLimit15397); 

                    newLeafNode(lv_name_0_0, grammarAccess.getTimeLimitAccess().getNameTimeKeyword_0_0());
                

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getTimeLimitRule());
            	        }
                   		setWithLastConsumed(current, "name", lv_name_0_0, "time");
            	    

            }


            }

            otherlv_1=(Token)match(input,35,FOLLOW_35_in_ruleTimeLimit15422); 

                	newLeafNode(otherlv_1, grammarAccess.getTimeLimitAccess().getLeftParenthesisKeyword_1());
                
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6844:1: ( ( (lv_msec_2_0= RULE_INT ) ) | ( (lv_var_3_0= ruleVariable ) ) )
            int alt79=2;
            int LA79_0 = input.LA(1);

            if ( (LA79_0==RULE_INT) ) {
                alt79=1;
            }
            else if ( (LA79_0==RULE_VARID||LA79_0==117) ) {
                alt79=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 79, 0, input);

                throw nvae;
            }
            switch (alt79) {
                case 1 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6844:2: ( (lv_msec_2_0= RULE_INT ) )
                    {
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6844:2: ( (lv_msec_2_0= RULE_INT ) )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6845:1: (lv_msec_2_0= RULE_INT )
                    {
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6845:1: (lv_msec_2_0= RULE_INT )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6846:3: lv_msec_2_0= RULE_INT
                    {
                    lv_msec_2_0=(Token)match(input,RULE_INT,FOLLOW_RULE_INT_in_ruleTimeLimit15440); 

                    			newLeafNode(lv_msec_2_0, grammarAccess.getTimeLimitAccess().getMsecINTTerminalRuleCall_2_0_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getTimeLimitRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"msec",
                            		lv_msec_2_0, 
                            		"INT");
                    	    

                    }


                    }


                    }
                    break;
                case 2 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6863:6: ( (lv_var_3_0= ruleVariable ) )
                    {
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6863:6: ( (lv_var_3_0= ruleVariable ) )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6864:1: (lv_var_3_0= ruleVariable )
                    {
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6864:1: (lv_var_3_0= ruleVariable )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6865:3: lv_var_3_0= ruleVariable
                    {
                     
                    	        newCompositeNode(grammarAccess.getTimeLimitAccess().getVarVariableParserRuleCall_2_1_0()); 
                    	    
                    pushFollow(FOLLOW_ruleVariable_in_ruleTimeLimit15472);
                    lv_var_3_0=ruleVariable();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getTimeLimitRule());
                    	        }
                           		set(
                           			current, 
                           			"var",
                            		lv_var_3_0, 
                            		"Variable");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }

            otherlv_4=(Token)match(input,36,FOLLOW_36_in_ruleTimeLimit15485); 

                	newLeafNode(otherlv_4, grammarAccess.getTimeLimitAccess().getRightParenthesisKeyword_3());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTimeLimit"


    // $ANTLR start "entryRuleComponentIP"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6893:1: entryRuleComponentIP returns [EObject current=null] : iv_ruleComponentIP= ruleComponentIP EOF ;
    public final EObject entryRuleComponentIP() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleComponentIP = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6894:2: (iv_ruleComponentIP= ruleComponentIP EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6895:2: iv_ruleComponentIP= ruleComponentIP EOF
            {
             newCompositeNode(grammarAccess.getComponentIPRule()); 
            pushFollow(FOLLOW_ruleComponentIP_in_entryRuleComponentIP15521);
            iv_ruleComponentIP=ruleComponentIP();

            state._fsp--;

             current =iv_ruleComponentIP; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleComponentIP15531); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleComponentIP"


    // $ANTLR start "ruleComponentIP"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6902:1: ruleComponentIP returns [EObject current=null] : (otherlv_0= '[' otherlv_1= 'host=' ( (lv_host_2_0= RULE_STRING ) ) otherlv_3= 'port=' ( (lv_port_4_0= RULE_INT ) ) otherlv_5= ']' ) ;
    public final EObject ruleComponentIP() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token lv_host_2_0=null;
        Token otherlv_3=null;
        Token lv_port_4_0=null;
        Token otherlv_5=null;

         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6905:28: ( (otherlv_0= '[' otherlv_1= 'host=' ( (lv_host_2_0= RULE_STRING ) ) otherlv_3= 'port=' ( (lv_port_4_0= RULE_INT ) ) otherlv_5= ']' ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6906:1: (otherlv_0= '[' otherlv_1= 'host=' ( (lv_host_2_0= RULE_STRING ) ) otherlv_3= 'port=' ( (lv_port_4_0= RULE_INT ) ) otherlv_5= ']' )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6906:1: (otherlv_0= '[' otherlv_1= 'host=' ( (lv_host_2_0= RULE_STRING ) ) otherlv_3= 'port=' ( (lv_port_4_0= RULE_INT ) ) otherlv_5= ']' )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6906:3: otherlv_0= '[' otherlv_1= 'host=' ( (lv_host_2_0= RULE_STRING ) ) otherlv_3= 'port=' ( (lv_port_4_0= RULE_INT ) ) otherlv_5= ']'
            {
            otherlv_0=(Token)match(input,54,FOLLOW_54_in_ruleComponentIP15568); 

                	newLeafNode(otherlv_0, grammarAccess.getComponentIPAccess().getLeftSquareBracketKeyword_0());
                
            otherlv_1=(Token)match(input,119,FOLLOW_119_in_ruleComponentIP15580); 

                	newLeafNode(otherlv_1, grammarAccess.getComponentIPAccess().getHostKeyword_1());
                
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6914:1: ( (lv_host_2_0= RULE_STRING ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6915:1: (lv_host_2_0= RULE_STRING )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6915:1: (lv_host_2_0= RULE_STRING )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6916:3: lv_host_2_0= RULE_STRING
            {
            lv_host_2_0=(Token)match(input,RULE_STRING,FOLLOW_RULE_STRING_in_ruleComponentIP15597); 

            			newLeafNode(lv_host_2_0, grammarAccess.getComponentIPAccess().getHostSTRINGTerminalRuleCall_2_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getComponentIPRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"host",
                    		lv_host_2_0, 
                    		"STRING");
            	    

            }


            }

            otherlv_3=(Token)match(input,120,FOLLOW_120_in_ruleComponentIP15614); 

                	newLeafNode(otherlv_3, grammarAccess.getComponentIPAccess().getPortKeyword_3());
                
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6936:1: ( (lv_port_4_0= RULE_INT ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6937:1: (lv_port_4_0= RULE_INT )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6937:1: (lv_port_4_0= RULE_INT )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6938:3: lv_port_4_0= RULE_INT
            {
            lv_port_4_0=(Token)match(input,RULE_INT,FOLLOW_RULE_INT_in_ruleComponentIP15631); 

            			newLeafNode(lv_port_4_0, grammarAccess.getComponentIPAccess().getPortINTTerminalRuleCall_4_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getComponentIPRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"port",
                    		lv_port_4_0, 
                    		"INT");
            	    

            }


            }

            otherlv_5=(Token)match(input,56,FOLLOW_56_in_ruleComponentIP15648); 

                	newLeafNode(otherlv_5, grammarAccess.getComponentIPAccess().getRightSquareBracketKeyword_5());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleComponentIP"


    // $ANTLR start "entryRuleMoveFile"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6966:1: entryRuleMoveFile returns [EObject current=null] : iv_ruleMoveFile= ruleMoveFile EOF ;
    public final EObject entryRuleMoveFile() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMoveFile = null;


        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6967:2: (iv_ruleMoveFile= ruleMoveFile EOF )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6968:2: iv_ruleMoveFile= ruleMoveFile EOF
            {
             newCompositeNode(grammarAccess.getMoveFileRule()); 
            pushFollow(FOLLOW_ruleMoveFile_in_entryRuleMoveFile15684);
            iv_ruleMoveFile=ruleMoveFile();

            state._fsp--;

             current =iv_ruleMoveFile; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleMoveFile15694); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMoveFile"


    // $ANTLR start "ruleMoveFile"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6975:1: ruleMoveFile returns [EObject current=null] : ( ( (lv_name_0_0= 'file' ) ) otherlv_1= '(' ( (lv_fname_2_0= ruleVarOrString ) ) otherlv_3= ')' ) ;
    public final EObject ruleMoveFile() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_fname_2_0 = null;


         enterRule(); 
            
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6978:28: ( ( ( (lv_name_0_0= 'file' ) ) otherlv_1= '(' ( (lv_fname_2_0= ruleVarOrString ) ) otherlv_3= ')' ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6979:1: ( ( (lv_name_0_0= 'file' ) ) otherlv_1= '(' ( (lv_fname_2_0= ruleVarOrString ) ) otherlv_3= ')' )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6979:1: ( ( (lv_name_0_0= 'file' ) ) otherlv_1= '(' ( (lv_fname_2_0= ruleVarOrString ) ) otherlv_3= ')' )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6979:2: ( (lv_name_0_0= 'file' ) ) otherlv_1= '(' ( (lv_fname_2_0= ruleVarOrString ) ) otherlv_3= ')'
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6979:2: ( (lv_name_0_0= 'file' ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6980:1: (lv_name_0_0= 'file' )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6980:1: (lv_name_0_0= 'file' )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6981:3: lv_name_0_0= 'file'
            {
            lv_name_0_0=(Token)match(input,121,FOLLOW_121_in_ruleMoveFile15737); 

                    newLeafNode(lv_name_0_0, grammarAccess.getMoveFileAccess().getNameFileKeyword_0_0());
                

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getMoveFileRule());
            	        }
                   		setWithLastConsumed(current, "name", lv_name_0_0, "file");
            	    

            }


            }

            otherlv_1=(Token)match(input,35,FOLLOW_35_in_ruleMoveFile15762); 

                	newLeafNode(otherlv_1, grammarAccess.getMoveFileAccess().getLeftParenthesisKeyword_1());
                
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6998:1: ( (lv_fname_2_0= ruleVarOrString ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6999:1: (lv_fname_2_0= ruleVarOrString )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:6999:1: (lv_fname_2_0= ruleVarOrString )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:7000:3: lv_fname_2_0= ruleVarOrString
            {
             
            	        newCompositeNode(grammarAccess.getMoveFileAccess().getFnameVarOrStringParserRuleCall_2_0()); 
            	    
            pushFollow(FOLLOW_ruleVarOrString_in_ruleMoveFile15783);
            lv_fname_2_0=ruleVarOrString();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getMoveFileRule());
            	        }
                   		set(
                   			current, 
                   			"fname",
                    		lv_fname_2_0, 
                    		"VarOrString");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_3=(Token)match(input,36,FOLLOW_36_in_ruleMoveFile15795); 

                	newLeafNode(otherlv_3, grammarAccess.getMoveFileAccess().getRightParenthesisKeyword_3());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMoveFile"


    // $ANTLR start "ruleWindowColor"
    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:7028:1: ruleWindowColor returns [Enumerator current=null] : ( (enumLiteral_0= 'white' ) | (enumLiteral_1= 'gray' ) | (enumLiteral_2= 'blue' ) | (enumLiteral_3= 'green' ) | (enumLiteral_4= 'yellow' ) | (enumLiteral_5= 'cyan' ) ) ;
    public final Enumerator ruleWindowColor() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;
        Token enumLiteral_3=null;
        Token enumLiteral_4=null;
        Token enumLiteral_5=null;

         enterRule(); 
        try {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:7030:28: ( ( (enumLiteral_0= 'white' ) | (enumLiteral_1= 'gray' ) | (enumLiteral_2= 'blue' ) | (enumLiteral_3= 'green' ) | (enumLiteral_4= 'yellow' ) | (enumLiteral_5= 'cyan' ) ) )
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:7031:1: ( (enumLiteral_0= 'white' ) | (enumLiteral_1= 'gray' ) | (enumLiteral_2= 'blue' ) | (enumLiteral_3= 'green' ) | (enumLiteral_4= 'yellow' ) | (enumLiteral_5= 'cyan' ) )
            {
            // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:7031:1: ( (enumLiteral_0= 'white' ) | (enumLiteral_1= 'gray' ) | (enumLiteral_2= 'blue' ) | (enumLiteral_3= 'green' ) | (enumLiteral_4= 'yellow' ) | (enumLiteral_5= 'cyan' ) )
            int alt80=6;
            switch ( input.LA(1) ) {
            case 122:
                {
                alt80=1;
                }
                break;
            case 123:
                {
                alt80=2;
                }
                break;
            case 124:
                {
                alt80=3;
                }
                break;
            case 125:
                {
                alt80=4;
                }
                break;
            case 126:
                {
                alt80=5;
                }
                break;
            case 127:
                {
                alt80=6;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 80, 0, input);

                throw nvae;
            }

            switch (alt80) {
                case 1 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:7031:2: (enumLiteral_0= 'white' )
                    {
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:7031:2: (enumLiteral_0= 'white' )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:7031:4: enumLiteral_0= 'white'
                    {
                    enumLiteral_0=(Token)match(input,122,FOLLOW_122_in_ruleWindowColor15845); 

                            current = grammarAccess.getWindowColorAccess().getWhiteEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_0, grammarAccess.getWindowColorAccess().getWhiteEnumLiteralDeclaration_0()); 
                        

                    }


                    }
                    break;
                case 2 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:7037:6: (enumLiteral_1= 'gray' )
                    {
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:7037:6: (enumLiteral_1= 'gray' )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:7037:8: enumLiteral_1= 'gray'
                    {
                    enumLiteral_1=(Token)match(input,123,FOLLOW_123_in_ruleWindowColor15862); 

                            current = grammarAccess.getWindowColorAccess().getGrayEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_1, grammarAccess.getWindowColorAccess().getGrayEnumLiteralDeclaration_1()); 
                        

                    }


                    }
                    break;
                case 3 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:7043:6: (enumLiteral_2= 'blue' )
                    {
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:7043:6: (enumLiteral_2= 'blue' )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:7043:8: enumLiteral_2= 'blue'
                    {
                    enumLiteral_2=(Token)match(input,124,FOLLOW_124_in_ruleWindowColor15879); 

                            current = grammarAccess.getWindowColorAccess().getBlueEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_2, grammarAccess.getWindowColorAccess().getBlueEnumLiteralDeclaration_2()); 
                        

                    }


                    }
                    break;
                case 4 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:7049:6: (enumLiteral_3= 'green' )
                    {
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:7049:6: (enumLiteral_3= 'green' )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:7049:8: enumLiteral_3= 'green'
                    {
                    enumLiteral_3=(Token)match(input,125,FOLLOW_125_in_ruleWindowColor15896); 

                            current = grammarAccess.getWindowColorAccess().getGreenEnumLiteralDeclaration_3().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_3, grammarAccess.getWindowColorAccess().getGreenEnumLiteralDeclaration_3()); 
                        

                    }


                    }
                    break;
                case 5 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:7055:6: (enumLiteral_4= 'yellow' )
                    {
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:7055:6: (enumLiteral_4= 'yellow' )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:7055:8: enumLiteral_4= 'yellow'
                    {
                    enumLiteral_4=(Token)match(input,126,FOLLOW_126_in_ruleWindowColor15913); 

                            current = grammarAccess.getWindowColorAccess().getYellowEnumLiteralDeclaration_4().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_4, grammarAccess.getWindowColorAccess().getYellowEnumLiteralDeclaration_4()); 
                        

                    }


                    }
                    break;
                case 6 :
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:7061:6: (enumLiteral_5= 'cyan' )
                    {
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:7061:6: (enumLiteral_5= 'cyan' )
                    // ../it.unibo.xtext.qactor/src-gen/it/unibo/xtext/parser/antlr/internal/InternalQactor.g:7061:8: enumLiteral_5= 'cyan'
                    {
                    enumLiteral_5=(Token)match(input,127,FOLLOW_127_in_ruleWindowColor15930); 

                            current = grammarAccess.getWindowColorAccess().getCyanEnumLiteralDeclaration_5().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_5, grammarAccess.getWindowColorAccess().getCyanEnumLiteralDeclaration_5()); 
                        

                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleWindowColor"

    // Delegated rules


 

    public static final BitSet FOLLOW_ruleQActorSystem_in_entryRuleQActorSystem75 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleQActorSystem85 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_12_in_ruleQActorSystem122 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_ruleQActorSystemSpec_in_ruleQActorSystem143 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleQActorSystemSpec_in_entryRuleQActorSystemSpec179 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleQActorSystemSpec189 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleQActorSystemSpec231 = new BitSet(new long[]{0x00000000087EA012L});
    public static final BitSet FOLLOW_13_in_ruleQActorSystemSpec254 = new BitSet(new long[]{0x00000000087E8012L});
    public static final BitSet FOLLOW_ruleMessage_in_ruleQActorSystemSpec289 = new BitSet(new long[]{0x00000000087E8012L});
    public static final BitSet FOLLOW_ruleContext_in_ruleQActorSystemSpec311 = new BitSet(new long[]{0x0000000008400012L});
    public static final BitSet FOLLOW_ruleQActor_in_ruleQActorSystemSpec333 = new BitSet(new long[]{0x0000000008000012L});
    public static final BitSet FOLLOW_ruleRobot_in_ruleQActorSystemSpec355 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRobot_in_entryRuleRobot392 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleRobot402 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleRobot444 = new BitSet(new long[]{0x0000000008000000L});
    public static final BitSet FOLLOW_ruleQActor_in_ruleRobot470 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_14_in_ruleRobot482 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleMessage_in_entryRuleMessage518 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleMessage528 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOutOnlyMessage_in_ruleMessage575 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOutInMessage_in_ruleMessage602 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOutOnlyMessage_in_entryRuleOutOnlyMessage637 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleOutOnlyMessage647 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleDispatch_in_ruleOutOnlyMessage694 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleEvent_in_ruleOutOnlyMessage721 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSignal_in_ruleOutOnlyMessage748 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleToken_in_ruleOutOnlyMessage775 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOutInMessage_in_entryRuleOutInMessage810 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleOutInMessage820 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRequest_in_ruleOutInMessage867 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleInvitation_in_ruleOutInMessage894 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleEvent_in_entryRuleEvent929 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleEvent939 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_15_in_ruleEvent976 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleEvent993 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_16_in_ruleEvent1010 = new BitSet(new long[]{0x00000000000000F0L,0x0020000000000000L});
    public static final BitSet FOLLOW_rulePHead_in_ruleEvent1031 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSignal_in_entryRuleSignal1067 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleSignal1077 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_17_in_ruleSignal1114 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleSignal1131 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_16_in_ruleSignal1148 = new BitSet(new long[]{0x00000000000000F0L,0x0020000000000000L});
    public static final BitSet FOLLOW_rulePHead_in_ruleSignal1169 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleToken_in_entryRuleToken1205 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleToken1215 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_18_in_ruleToken1252 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleToken1269 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_16_in_ruleToken1286 = new BitSet(new long[]{0x00000000000000F0L,0x0020000000000000L});
    public static final BitSet FOLLOW_rulePHead_in_ruleToken1307 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleDispatch_in_entryRuleDispatch1343 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleDispatch1353 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_19_in_ruleDispatch1390 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleDispatch1407 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_16_in_ruleDispatch1424 = new BitSet(new long[]{0x00000000000000F0L,0x0020000000000000L});
    public static final BitSet FOLLOW_rulePHead_in_ruleDispatch1445 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRequest_in_entryRuleRequest1481 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleRequest1491 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_20_in_ruleRequest1528 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleRequest1545 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_16_in_ruleRequest1562 = new BitSet(new long[]{0x00000000000000F0L,0x0020000000000000L});
    public static final BitSet FOLLOW_rulePHead_in_ruleRequest1583 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleInvitation_in_entryRuleInvitation1619 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleInvitation1629 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_21_in_ruleInvitation1666 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleInvitation1683 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_16_in_ruleInvitation1700 = new BitSet(new long[]{0x00000000000000F0L,0x0020000000000000L});
    public static final BitSet FOLLOW_rulePHead_in_ruleInvitation1721 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleContext_in_entryRuleContext1757 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleContext1767 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_22_in_ruleContext1804 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleContext1821 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_23_in_ruleContext1838 = new BitSet(new long[]{0x0040000000000000L});
    public static final BitSet FOLLOW_ruleComponentIP_in_ruleContext1859 = new BitSet(new long[]{0x0000000007000002L,0x0000080000000000L});
    public static final BitSet FOLLOW_24_in_ruleContext1878 = new BitSet(new long[]{0x0000000000000000L,0xFC00000000000000L});
    public static final BitSet FOLLOW_ruleWindowColor_in_ruleContext1912 = new BitSet(new long[]{0x0000000006000002L,0x0000080000000000L});
    public static final BitSet FOLLOW_25_in_ruleContext1932 = new BitSet(new long[]{0x0000000004000002L,0x0000080000000000L});
    public static final BitSet FOLLOW_26_in_ruleContext1964 = new BitSet(new long[]{0x0000000000000002L,0x0000080000000000L});
    public static final BitSet FOLLOW_ruleEventHandler_in_ruleContext1999 = new BitSet(new long[]{0x0000000000000002L,0x0000080000000000L});
    public static final BitSet FOLLOW_ruleQActor_in_entryRuleQActor2036 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleQActor2046 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_27_in_ruleQActor2083 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleQActor2100 = new BitSet(new long[]{0x0000000010000000L});
    public static final BitSet FOLLOW_28_in_ruleQActor2117 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleQActor2137 = new BitSet(new long[]{0x0000000021000000L});
    public static final BitSet FOLLOW_24_in_ruleQActor2156 = new BitSet(new long[]{0x0000000000000000L,0xFC00000000000000L});
    public static final BitSet FOLLOW_ruleWindowColor_in_ruleQActor2190 = new BitSet(new long[]{0x0000000020000000L});
    public static final BitSet FOLLOW_29_in_ruleQActor2204 = new BitSet(new long[]{0x00043400C0000000L});
    public static final BitSet FOLLOW_30_in_ruleQActor2217 = new BitSet(new long[]{0x0000000020000000L});
    public static final BitSet FOLLOW_29_in_ruleQActor2229 = new BitSet(new long[]{0x00000000800000F0L,0x0020000000000000L});
    public static final BitSet FOLLOW_ruleRule_in_ruleQActor2250 = new BitSet(new long[]{0x00000000800000F0L,0x0020000000000000L});
    public static final BitSet FOLLOW_31_in_ruleQActor2263 = new BitSet(new long[]{0x0004340080000000L});
    public static final BitSet FOLLOW_ruleData_in_ruleQActor2286 = new BitSet(new long[]{0x0004340080000000L});
    public static final BitSet FOLLOW_ruleAction_in_ruleQActor2308 = new BitSet(new long[]{0x0004200080000000L});
    public static final BitSet FOLLOW_rulePlan_in_ruleQActor2330 = new BitSet(new long[]{0x0004000080000000L});
    public static final BitSet FOLLOW_31_in_ruleQActor2343 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRule_in_entryRuleRule2379 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleRule2389 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePHead_in_ruleRule2435 = new BitSet(new long[]{0x0000000500000000L});
    public static final BitSet FOLLOW_32_in_ruleRule2448 = new BitSet(new long[]{0x00000220000000F0L,0x0020000000000000L});
    public static final BitSet FOLLOW_rulePTerm_in_ruleRule2469 = new BitSet(new long[]{0x0000000600000000L});
    public static final BitSet FOLLOW_33_in_ruleRule2482 = new BitSet(new long[]{0x00000220000000F0L,0x0020000000000000L});
    public static final BitSet FOLLOW_rulePTerm_in_ruleRule2503 = new BitSet(new long[]{0x0000000600000000L});
    public static final BitSet FOLLOW_34_in_ruleRule2519 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePHead_in_entryRulePHead2555 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulePHead2565 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePAtom_in_rulePHead2612 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePStruct_in_rulePHead2639 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePTerm_in_entryRulePTerm2674 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulePTerm2684 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePAtom_in_rulePTerm2731 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePStruct_in_rulePTerm2758 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePPredef_in_rulePTerm2785 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePActorCall_in_rulePTerm2812 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePAtom_in_entryRulePAtom2847 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulePAtom2857 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePAtomString_in_rulePAtom2904 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVariable_in_rulePAtom2931 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePAtomNum_in_rulePAtom2958 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePAtomic_in_rulePAtom2985 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePAtomString_in_entryRulePAtomString3020 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulePAtomString3030 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_STRING_in_rulePAtomString3071 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePAtomic_in_entryRulePAtomic3111 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulePAtomic3121 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rulePAtomic3162 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePAtomNum_in_entryRulePAtomNum3202 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulePAtomNum3212 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_INT_in_rulePAtomNum3253 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePStruct_in_entryRulePStruct3293 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulePStruct3303 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rulePStruct3345 = new BitSet(new long[]{0x0000000800000000L});
    public static final BitSet FOLLOW_35_in_rulePStruct3362 = new BitSet(new long[]{0x00000232000000F0L,0x0020000000000000L});
    public static final BitSet FOLLOW_rulePTerm_in_rulePStruct3383 = new BitSet(new long[]{0x0000001200000000L});
    public static final BitSet FOLLOW_33_in_rulePStruct3397 = new BitSet(new long[]{0x00000220000000F0L,0x0020000000000000L});
    public static final BitSet FOLLOW_rulePTerm_in_rulePStruct3418 = new BitSet(new long[]{0x0000001200000000L});
    public static final BitSet FOLLOW_36_in_rulePStruct3432 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePActorCall_in_entryRulePActorCall3468 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulePActorCall3478 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_37_in_rulePActorCall3515 = new BitSet(new long[]{0x0000004000000000L});
    public static final BitSet FOLLOW_38_in_rulePActorCall3527 = new BitSet(new long[]{0x00000000000000F0L,0x0020000000000000L});
    public static final BitSet FOLLOW_rulePStruct_in_rulePActorCall3548 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePPredef_in_entryRulePPredef3584 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulePPredef3594 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePAtomCut_in_rulePPredef3641 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePIs_in_rulePPredef3668 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePIs_in_entryRulePIs3703 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulePIs3713 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVariable_in_rulePIs3759 = new BitSet(new long[]{0x0000008000000000L});
    public static final BitSet FOLLOW_39_in_rulePIs3771 = new BitSet(new long[]{0x0000000000000080L,0x0020000000000000L});
    public static final BitSet FOLLOW_ruleVariable_in_rulePIs3792 = new BitSet(new long[]{0x0000010000000000L});
    public static final BitSet FOLLOW_40_in_rulePIs3804 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_rulePAtomNum_in_rulePIs3825 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePAtomCut_in_entryRulePAtomCut3861 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulePAtomCut3871 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_41_in_rulePAtomCut3913 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleData_in_entryRuleData3961 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleData3971 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIntegerData_in_ruleData4018 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleStringData_in_ruleData4045 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIntegerData_in_entryRuleIntegerData4080 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleIntegerData4090 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_42_in_ruleIntegerData4127 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleIntegerData4144 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_43_in_ruleIntegerData4161 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_RULE_INT_in_ruleIntegerData4178 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleStringData_in_entryRuleStringData4219 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleStringData4229 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_44_in_ruleStringData4266 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleStringData4283 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_43_in_ruleStringData4300 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_RULE_STRING_in_ruleStringData4317 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAction_in_entryRuleAction4358 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleAction4368 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_45_in_ruleAction4405 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleAction4422 = new BitSet(new long[]{0x0000C00000000000L});
    public static final BitSet FOLLOW_46_in_ruleAction4445 = new BitSet(new long[]{0x0000800000000000L});
    public static final BitSet FOLLOW_47_in_ruleAction4472 = new BitSet(new long[]{0x0000000800000000L});
    public static final BitSet FOLLOW_35_in_ruleAction4484 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_RULE_INT_in_ruleAction4501 = new BitSet(new long[]{0x0000001000000000L});
    public static final BitSet FOLLOW_36_in_ruleAction4518 = new BitSet(new long[]{0x0003000000000000L});
    public static final BitSet FOLLOW_48_in_ruleAction4532 = new BitSet(new long[]{0x00000000000000F0L,0x0020000000000000L});
    public static final BitSet FOLLOW_rulePStruct_in_ruleAction4553 = new BitSet(new long[]{0x0002000000000000L});
    public static final BitSet FOLLOW_49_in_ruleAction4567 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePlan_in_entryRulePlan4603 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulePlan4613 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_50_in_rulePlan4650 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_rulePlan4667 = new BitSet(new long[]{0x6858000000000000L,0x000003DEA6BFFEDFL});
    public static final BitSet FOLLOW_51_in_rulePlan4690 = new BitSet(new long[]{0x6858000000000000L,0x000003DEA6BFFEDFL});
    public static final BitSet FOLLOW_52_in_rulePlan4722 = new BitSet(new long[]{0x6858000000000000L,0x000003DEA6BFFEDFL});
    public static final BitSet FOLLOW_rulePlanAction_in_rulePlan4757 = new BitSet(new long[]{0x0002000000000002L});
    public static final BitSet FOLLOW_49_in_rulePlan4770 = new BitSet(new long[]{0x6858000000000000L,0x000003DEA6BFFEDFL});
    public static final BitSet FOLLOW_rulePlanAction_in_rulePlan4791 = new BitSet(new long[]{0x0002000000000002L});
    public static final BitSet FOLLOW_rulePlanAction_in_entryRulePlanAction4829 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulePlanAction4839 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleGuard_in_rulePlanAction4885 = new BitSet(new long[]{0x6858000000000000L,0x000003DEA6BFFEDFL});
    public static final BitSet FOLLOW_ruleMove_in_rulePlanAction4907 = new BitSet(new long[]{0x0020000000000002L,0x0002000000000000L});
    public static final BitSet FOLLOW_ruleReaction_in_rulePlanAction4928 = new BitSet(new long[]{0x0020000000000002L});
    public static final BitSet FOLLOW_53_in_rulePlanAction4942 = new BitSet(new long[]{0x6858000000000000L,0x000003DEA6BFFEDFL});
    public static final BitSet FOLLOW_ruleMove_in_rulePlanAction4963 = new BitSet(new long[]{0x0000000000000002L,0x0002000000000000L});
    public static final BitSet FOLLOW_ruleReaction_in_rulePlanAction4984 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleGuard_in_entryRuleGuard5023 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleGuard5033 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_54_in_ruleGuard5076 = new BitSet(new long[]{0x0680000000000000L});
    public static final BitSet FOLLOW_55_in_ruleGuard5107 = new BitSet(new long[]{0x0680000000000000L});
    public static final BitSet FOLLOW_ruleGuardPredicate_in_ruleGuard5142 = new BitSet(new long[]{0x0100000000000000L});
    public static final BitSet FOLLOW_56_in_ruleGuard5154 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleGuardPredicate_in_entryRuleGuardPredicate5190 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleGuardPredicate5200 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleGuardPredicateStable_in_ruleGuardPredicate5247 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleGuardPredicateRemovable_in_ruleGuardPredicate5274 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleGuardPredicateRemovable_in_entryRuleGuardPredicateRemovable5309 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleGuardPredicateRemovable5319 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_57_in_ruleGuardPredicateRemovable5362 = new BitSet(new long[]{0x00000220000000F0L,0x0020000000000000L});
    public static final BitSet FOLLOW_rulePTerm_in_ruleGuardPredicateRemovable5396 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleGuardPredicateStable_in_entryRuleGuardPredicateStable5432 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleGuardPredicateStable5442 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_58_in_ruleGuardPredicateStable5485 = new BitSet(new long[]{0x00000220000000F0L,0x0020000000000000L});
    public static final BitSet FOLLOW_rulePTerm_in_ruleGuardPredicateStable5519 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleMove_in_entryRuleMove5555 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleMove5565 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleActionMove_in_ruleMove5612 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleMessageMove_in_ruleMove5639 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleExtensionMove_in_ruleMove5666 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBasicMove_in_ruleMove5693 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePlanMove_in_ruleMove5720 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleGuardMove_in_ruleMove5747 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBasicRobotMove_in_ruleMove5774 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleActionMove_in_entryRuleActionMove5809 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleActionMove5819 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleExecuteAction_in_ruleActionMove5866 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSolveGoal_in_ruleActionMove5893 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleDemo_in_ruleActionMove5920 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleActorOp_in_ruleActionMove5947 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleExecuteAction_in_entryRuleExecuteAction5982 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleExecuteAction5992 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_59_in_ruleExecuteAction6030 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleExecuteAction6050 = new BitSet(new long[]{0x1000000000000002L});
    public static final BitSet FOLLOW_60_in_ruleExecuteAction6063 = new BitSet(new long[]{0x00000000000000F0L,0x0020000000000000L});
    public static final BitSet FOLLOW_rulePHead_in_ruleExecuteAction6084 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_61_in_ruleExecuteAction6106 = new BitSet(new long[]{0x00000000000000F0L,0x0020000000000000L});
    public static final BitSet FOLLOW_rulePHead_in_ruleExecuteAction6127 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSolveGoal_in_entryRuleSolveGoal6164 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleSolveGoal6174 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_62_in_ruleSolveGoal6217 = new BitSet(new long[]{0x00000000000000F0L,0x0020000000000000L});
    public static final BitSet FOLLOW_rulePHead_in_ruleSolveGoal6251 = new BitSet(new long[]{0x0000000000000000L,0x0040000000000000L});
    public static final BitSet FOLLOW_ruleTimeLimit_in_ruleSolveGoal6272 = new BitSet(new long[]{0x8000000000000002L});
    public static final BitSet FOLLOW_63_in_ruleSolveGoal6285 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleSolveGoal6305 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleDemo_in_entryRuleDemo6343 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleDemo6353 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_64_in_ruleDemo6396 = new BitSet(new long[]{0x00000000000000F0L,0x0020000000000000L});
    public static final BitSet FOLLOW_rulePHead_in_ruleDemo6430 = new BitSet(new long[]{0x8000000000000002L});
    public static final BitSet FOLLOW_63_in_ruleDemo6443 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleDemo6463 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleActorOp_in_entryRuleActorOp6501 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleActorOp6511 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_65_in_ruleActorOp6554 = new BitSet(new long[]{0x00000000000000F0L,0x0020000000000000L});
    public static final BitSet FOLLOW_rulePHead_in_ruleActorOp6588 = new BitSet(new long[]{0x8000000000000002L});
    public static final BitSet FOLLOW_63_in_ruleActorOp6601 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleActorOp6621 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBasicRobotMove_in_entryRuleBasicRobotMove6659 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleBasicRobotMove6669 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_66_in_ruleBasicRobotMove6711 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBasicMove_in_entryRuleBasicMove6759 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleBasicMove6769 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePrint_in_ruleBasicMove6816 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePrintCurrentEvent_in_ruleBasicMove6843 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePrintCurrentMessage_in_ruleBasicMove6870 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleMemoCurrentEvent_in_ruleBasicMove6897 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleMemoCurrentMessage_in_ruleBasicMove6924 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePrint_in_entryRulePrint6959 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulePrint6969 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_67_in_rulePrint7012 = new BitSet(new long[]{0x0000000800000000L});
    public static final BitSet FOLLOW_35_in_rulePrint7037 = new BitSet(new long[]{0x00000000000000F0L,0x0020000000000000L});
    public static final BitSet FOLLOW_rulePHead_in_rulePrint7058 = new BitSet(new long[]{0x0000001000000000L});
    public static final BitSet FOLLOW_36_in_rulePrint7070 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePrintCurrentEvent_in_entryRulePrintCurrentEvent7106 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulePrintCurrentEvent7116 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_68_in_rulePrintCurrentEvent7159 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000020L});
    public static final BitSet FOLLOW_69_in_rulePrintCurrentEvent7190 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePrintCurrentMessage_in_entryRulePrintCurrentMessage7240 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulePrintCurrentMessage7250 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_70_in_rulePrintCurrentMessage7293 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000020L});
    public static final BitSet FOLLOW_69_in_rulePrintCurrentMessage7324 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleMemoCurrentEvent_in_entryRuleMemoCurrentEvent7374 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleMemoCurrentEvent7384 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_71_in_ruleMemoCurrentEvent7427 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000100L});
    public static final BitSet FOLLOW_72_in_ruleMemoCurrentEvent7458 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleMemoCurrentMessage_in_entryRuleMemoCurrentMessage7508 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleMemoCurrentMessage7518 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_73_in_ruleMemoCurrentMessage7561 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000100L});
    public static final BitSet FOLLOW_72_in_ruleMemoCurrentMessage7592 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePlanMove_in_entryRulePlanMove7642 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulePlanMove7652 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleGetActivationEvent_in_rulePlanMove7699 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleGetSensedEvent_in_rulePlanMove7726 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLoadPlan_in_rulePlanMove7753 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRunPlan_in_rulePlanMove7780 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleResumePlan_in_rulePlanMove7807 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRepeatPlan_in_rulePlanMove7834 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSwitchPlan_in_rulePlanMove7861 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSuspendPlan_in_rulePlanMove7888 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleEndPlan_in_rulePlanMove7915 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleEndActor_in_rulePlanMove7942 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleGetActivationEvent_in_entryRuleGetActivationEvent7977 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleGetActivationEvent7987 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_74_in_ruleGetActivationEvent8030 = new BitSet(new long[]{0x0000000000000080L,0x0020000000000000L});
    public static final BitSet FOLLOW_ruleVariable_in_ruleGetActivationEvent8064 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleGetSensedEvent_in_entryRuleGetSensedEvent8100 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleGetSensedEvent8110 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_75_in_ruleGetSensedEvent8153 = new BitSet(new long[]{0x0000000000000080L,0x0020000000000000L});
    public static final BitSet FOLLOW_ruleVariable_in_ruleGetSensedEvent8187 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLoadPlan_in_entryRuleLoadPlan8223 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleLoadPlan8233 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_76_in_ruleLoadPlan8276 = new BitSet(new long[]{0x00000000000000A0L,0x0020000000000000L});
    public static final BitSet FOLLOW_ruleVarOrString_in_ruleLoadPlan8310 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRunPlan_in_entryRuleRunPlan8346 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleRunPlan8356 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_77_in_ruleRunPlan8399 = new BitSet(new long[]{0x00000000000000F0L,0x0020000000000000L});
    public static final BitSet FOLLOW_ruleVarOrAtomic_in_ruleRunPlan8433 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleResumePlan_in_entryRuleResumePlan8469 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleResumePlan8479 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_52_in_ruleResumePlan8521 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSuspendPlan_in_entryRuleSuspendPlan8569 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleSuspendPlan8579 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_78_in_ruleSuspendPlan8621 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRepeatPlan_in_entryRuleRepeatPlan8669 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleRepeatPlan8679 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_79_in_ruleRepeatPlan8722 = new BitSet(new long[]{0x0000000000000042L});
    public static final BitSet FOLLOW_RULE_INT_in_ruleRepeatPlan8752 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSwitchPlan_in_entryRuleSwitchPlan8794 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleSwitchPlan8804 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_80_in_ruleSwitchPlan8847 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleSwitchPlan8880 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleEndPlan_in_entryRuleEndPlan8916 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleEndPlan8926 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_81_in_ruleEndPlan8969 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_RULE_STRING_in_ruleEndPlan8999 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleEndActor_in_entryRuleEndActor9040 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleEndActor9050 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_82_in_ruleEndActor9093 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_RULE_STRING_in_ruleEndActor9123 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleGuardMove_in_entryRuleGuardMove9164 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleGuardMove9174 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAddRule_in_ruleGuardMove9221 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRemoveRule_in_ruleGuardMove9248 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAddRule_in_entryRuleAddRule9283 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleAddRule9293 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_83_in_ruleAddRule9336 = new BitSet(new long[]{0x00000000000000F0L,0x0020000000000000L});
    public static final BitSet FOLLOW_rulePHead_in_ruleAddRule9370 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRemoveRule_in_entryRuleRemoveRule9406 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleRemoveRule9416 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_84_in_ruleRemoveRule9459 = new BitSet(new long[]{0x00000000000000F0L,0x0020000000000000L});
    public static final BitSet FOLLOW_rulePHead_in_ruleRemoveRule9493 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleMessageMove_in_entryRuleMessageMove9529 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleMessageMove9539 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSendDispatch_in_ruleMessageMove9586 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSendRequest_in_ruleMessageMove9613 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleReplyToCaller_in_ruleMessageMove9640 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleReceiveMsg_in_ruleMessageMove9667 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOnReceiveMsg_in_ruleMessageMove9694 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleMsgSelect_in_ruleMessageMove9721 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRaiseEvent_in_ruleMessageMove9748 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSenseEvent_in_ruleMessageMove9775 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleMsgSwitch_in_ruleMessageMove9802 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleEventSwitch_in_ruleMessageMove9829 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSendDispatch_in_entryRuleSendDispatch9864 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleSendDispatch9874 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_85_in_ruleSendDispatch9917 = new BitSet(new long[]{0x0000000000000090L,0x0020000000000000L});
    public static final BitSet FOLLOW_ruleVarOrQactor_in_ruleSendDispatch9951 = new BitSet(new long[]{0x0000000000000000L,0x0000000000400000L});
    public static final BitSet FOLLOW_86_in_ruleSendDispatch9963 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleSendDispatch9983 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_16_in_ruleSendDispatch9995 = new BitSet(new long[]{0x00000000000000F0L,0x0020000000000000L});
    public static final BitSet FOLLOW_rulePHead_in_ruleSendDispatch10016 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSendRequest_in_entryRuleSendRequest10052 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleSendRequest10062 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_87_in_ruleSendRequest10105 = new BitSet(new long[]{0x0000000000000090L,0x0020000000000000L});
    public static final BitSet FOLLOW_ruleVarOrQactor_in_ruleSendRequest10139 = new BitSet(new long[]{0x0000000000000000L,0x0000000000400000L});
    public static final BitSet FOLLOW_86_in_ruleSendRequest10151 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleSendRequest10171 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_16_in_ruleSendRequest10183 = new BitSet(new long[]{0x00000000000000F0L,0x0020000000000000L});
    public static final BitSet FOLLOW_rulePHead_in_ruleSendRequest10204 = new BitSet(new long[]{0x0000000000000002L,0x0000000001000000L});
    public static final BitSet FOLLOW_88_in_ruleSendRequest10217 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleReplyToCaller_in_entryRuleReplyToCaller10255 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleReplyToCaller10265 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_89_in_ruleReplyToCaller10308 = new BitSet(new long[]{0x0000000000000000L,0x0000000000400000L});
    public static final BitSet FOLLOW_86_in_ruleReplyToCaller10333 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleReplyToCaller10353 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_16_in_ruleReplyToCaller10365 = new BitSet(new long[]{0x00000000000000F0L,0x0020000000000000L});
    public static final BitSet FOLLOW_rulePHead_in_ruleReplyToCaller10386 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleReceiveMsg_in_entryRuleReceiveMsg10422 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleReceiveMsg10432 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_90_in_ruleReceiveMsg10475 = new BitSet(new long[]{0x0000000000000000L,0x0040000000000000L});
    public static final BitSet FOLLOW_ruleTimeLimit_in_ruleReceiveMsg10509 = new BitSet(new long[]{0x0000000000000002L,0x0000000000400000L});
    public static final BitSet FOLLOW_ruleMsgSpec_in_ruleReceiveMsg10530 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleMsgSpec_in_entryRuleMsgSpec10567 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleMsgSpec10577 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_86_in_ruleMsgSpec10614 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleMsgSpec10634 = new BitSet(new long[]{0x0000000000000000L,0x0000000008000000L});
    public static final BitSet FOLLOW_91_in_ruleMsgSpec10646 = new BitSet(new long[]{0x00000000000000F0L,0x0020000000000000L});
    public static final BitSet FOLLOW_ruleVarOrAtomic_in_ruleMsgSpec10667 = new BitSet(new long[]{0x0000000000000000L,0x0000000010000000L});
    public static final BitSet FOLLOW_92_in_ruleMsgSpec10679 = new BitSet(new long[]{0x00000000000000F0L,0x0020000000000000L});
    public static final BitSet FOLLOW_rulePHead_in_ruleMsgSpec10700 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOnReceiveMsg_in_entryRuleOnReceiveMsg10736 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleOnReceiveMsg10746 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_93_in_ruleOnReceiveMsg10789 = new BitSet(new long[]{0x0000000000000000L,0x0000000040000000L});
    public static final BitSet FOLLOW_94_in_ruleOnReceiveMsg10814 = new BitSet(new long[]{0x0000000800000000L});
    public static final BitSet FOLLOW_35_in_ruleOnReceiveMsg10826 = new BitSet(new long[]{0x00000000000000F0L,0x0020000000000000L});
    public static final BitSet FOLLOW_rulePHead_in_ruleOnReceiveMsg10847 = new BitSet(new long[]{0x0000000200000000L});
    public static final BitSet FOLLOW_33_in_ruleOnReceiveMsg10859 = new BitSet(new long[]{0x00000000000000F0L,0x0020000000000000L});
    public static final BitSet FOLLOW_rulePHead_in_ruleOnReceiveMsg10880 = new BitSet(new long[]{0x0000000200000000L});
    public static final BitSet FOLLOW_33_in_ruleOnReceiveMsg10892 = new BitSet(new long[]{0x00000000000000F0L,0x0020000000000000L});
    public static final BitSet FOLLOW_rulePHead_in_ruleOnReceiveMsg10913 = new BitSet(new long[]{0x0000000200000000L});
    public static final BitSet FOLLOW_33_in_ruleOnReceiveMsg10925 = new BitSet(new long[]{0x00000000000000F0L,0x0020000000000000L});
    public static final BitSet FOLLOW_rulePHead_in_ruleOnReceiveMsg10946 = new BitSet(new long[]{0x0000000200000000L});
    public static final BitSet FOLLOW_33_in_ruleOnReceiveMsg10958 = new BitSet(new long[]{0x00000000000000F0L,0x0020000000000000L});
    public static final BitSet FOLLOW_rulePHead_in_ruleOnReceiveMsg10979 = new BitSet(new long[]{0x0000000200000000L});
    public static final BitSet FOLLOW_33_in_ruleOnReceiveMsg10991 = new BitSet(new long[]{0x00000000000000F0L,0x0020000000000000L});
    public static final BitSet FOLLOW_rulePHead_in_ruleOnReceiveMsg11012 = new BitSet(new long[]{0x0000001000000000L});
    public static final BitSet FOLLOW_36_in_ruleOnReceiveMsg11024 = new BitSet(new long[]{0x0000000000000000L,0x0040000000000000L});
    public static final BitSet FOLLOW_ruleTimeLimit_in_ruleOnReceiveMsg11045 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleMsgSelect_in_entryRuleMsgSelect11081 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleMsgSelect11091 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_95_in_ruleMsgSelect11134 = new BitSet(new long[]{0x0000000000000000L,0x0040000000000000L});
    public static final BitSet FOLLOW_ruleTimeLimit_in_ruleMsgSelect11168 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleMsgSelect11188 = new BitSet(new long[]{0x0000000200000000L,0x0000000100000000L});
    public static final BitSet FOLLOW_33_in_ruleMsgSelect11201 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleMsgSelect11221 = new BitSet(new long[]{0x0000000200000000L,0x0000000100000000L});
    public static final BitSet FOLLOW_96_in_ruleMsgSelect11235 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleMsgSelect11255 = new BitSet(new long[]{0x0000000200000002L});
    public static final BitSet FOLLOW_33_in_ruleMsgSelect11268 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleMsgSelect11288 = new BitSet(new long[]{0x0000000200000002L});
    public static final BitSet FOLLOW_ruleRaiseEvent_in_entryRuleRaiseEvent11326 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleRaiseEvent11336 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_97_in_ruleRaiseEvent11379 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleRaiseEvent11412 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_16_in_ruleRaiseEvent11424 = new BitSet(new long[]{0x00000000000000F0L,0x0020000000000000L});
    public static final BitSet FOLLOW_rulePHead_in_ruleRaiseEvent11445 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSenseEvent_in_entryRuleSenseEvent11481 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleSenseEvent11491 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_98_in_ruleSenseEvent11528 = new BitSet(new long[]{0x0000000000000000L,0x0040000000000000L});
    public static final BitSet FOLLOW_ruleTimeLimit_in_ruleSenseEvent11549 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleSenseEvent11569 = new BitSet(new long[]{0x0000000200000000L,0x0000000100000000L});
    public static final BitSet FOLLOW_33_in_ruleSenseEvent11582 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleSenseEvent11602 = new BitSet(new long[]{0x0000000200000000L,0x0000000100000000L});
    public static final BitSet FOLLOW_96_in_ruleSenseEvent11616 = new BitSet(new long[]{0x0000000000000010L,0x0000002000000000L});
    public static final BitSet FOLLOW_ruleContinuation_in_ruleSenseEvent11637 = new BitSet(new long[]{0x0000000200000002L});
    public static final BitSet FOLLOW_33_in_ruleSenseEvent11650 = new BitSet(new long[]{0x0000000000000010L,0x0000002000000000L});
    public static final BitSet FOLLOW_ruleContinuation_in_ruleSenseEvent11671 = new BitSet(new long[]{0x0000000200000002L});
    public static final BitSet FOLLOW_ruleMsgSwitch_in_entryRuleMsgSwitch11709 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleMsgSwitch11719 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_99_in_ruleMsgSwitch11756 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleMsgSwitch11776 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_16_in_ruleMsgSwitch11788 = new BitSet(new long[]{0x00000000000000F0L,0x0020000000000000L});
    public static final BitSet FOLLOW_rulePHead_in_ruleMsgSwitch11809 = new BitSet(new long[]{0x0000000000000000L,0x0000000100000000L});
    public static final BitSet FOLLOW_96_in_ruleMsgSwitch11821 = new BitSet(new long[]{0x6858000000000000L,0x000003DEA6BFFEDFL});
    public static final BitSet FOLLOW_ruleMove_in_ruleMsgSwitch11842 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleEventSwitch_in_entryRuleEventSwitch11878 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleEventSwitch11888 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_100_in_ruleEventSwitch11925 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleEventSwitch11945 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_16_in_ruleEventSwitch11957 = new BitSet(new long[]{0x00000000000000F0L,0x0020000000000000L});
    public static final BitSet FOLLOW_rulePHead_in_ruleEventSwitch11978 = new BitSet(new long[]{0x0000000000000000L,0x0000000100000000L});
    public static final BitSet FOLLOW_96_in_ruleEventSwitch11990 = new BitSet(new long[]{0x6858000000000000L,0x000003DEA6BFFEDFL});
    public static final BitSet FOLLOW_ruleMove_in_ruleEventSwitch12011 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleContinuation_in_entryRuleContinuation12047 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleContinuation12057 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleContinuation12102 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_101_in_ruleContinuation12126 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleExtensionMove_in_entryRuleExtensionMove12175 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleExtensionMove12185 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePhoto_in_ruleExtensionMove12232 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSound_in_ruleExtensionMove12259 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVideo_in_ruleExtensionMove12286 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleDelay_in_ruleExtensionMove12313 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePhoto_in_entryRulePhoto12348 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulePhoto12358 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_102_in_rulePhoto12401 = new BitSet(new long[]{0x0000000000000000L,0x0040000000000000L});
    public static final BitSet FOLLOW_ruleTimeLimit_in_rulePhoto12435 = new BitSet(new long[]{0x0000000000000000L,0x0200000000000000L});
    public static final BitSet FOLLOW_ruleMoveFile_in_rulePhoto12456 = new BitSet(new long[]{0x0000000000000002L,0x0000040000000000L});
    public static final BitSet FOLLOW_ruleAnswerEvent_in_rulePhoto12477 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSound_in_entryRuleSound12514 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleSound12524 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_103_in_ruleSound12567 = new BitSet(new long[]{0x0000000000000000L,0x0040000000000000L});
    public static final BitSet FOLLOW_ruleTimeLimit_in_ruleSound12601 = new BitSet(new long[]{0x0000000000000000L,0x0200000000000000L});
    public static final BitSet FOLLOW_ruleMoveFile_in_ruleSound12622 = new BitSet(new long[]{0x0000000000000002L,0x0000040000000000L});
    public static final BitSet FOLLOW_ruleAnswerEvent_in_ruleSound12643 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVideo_in_entryRuleVideo12680 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleVideo12690 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_104_in_ruleVideo12733 = new BitSet(new long[]{0x0000000000000000L,0x0040000000000000L});
    public static final BitSet FOLLOW_ruleTimeLimit_in_ruleVideo12767 = new BitSet(new long[]{0x0000000000000000L,0x0200000000000000L});
    public static final BitSet FOLLOW_ruleMoveFile_in_ruleVideo12788 = new BitSet(new long[]{0x0000000000000002L,0x0000040000000000L});
    public static final BitSet FOLLOW_ruleAnswerEvent_in_ruleVideo12809 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleDelay_in_entryRuleDelay12846 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleDelay12856 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_105_in_ruleDelay12899 = new BitSet(new long[]{0x0000000000000000L,0x0040000000000000L});
    public static final BitSet FOLLOW_ruleTimeLimit_in_ruleDelay12933 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAnswerEvent_in_entryRuleAnswerEvent12969 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleAnswerEvent12979 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_106_in_ruleAnswerEvent13016 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleAnswerEvent13033 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleEventHandler_in_entryRuleEventHandler13074 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleEventHandler13084 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_107_in_ruleEventHandler13121 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleEventHandler13138 = new BitSet(new long[]{0x0002000020000000L,0x0000300000000000L});
    public static final BitSet FOLLOW_108_in_ruleEventHandler13156 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleEventHandler13176 = new BitSet(new long[]{0x0002000220000000L,0x0000200000000000L});
    public static final BitSet FOLLOW_33_in_ruleEventHandler13189 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleEventHandler13209 = new BitSet(new long[]{0x0002000220000000L,0x0000200000000000L});
    public static final BitSet FOLLOW_109_in_ruleEventHandler13231 = new BitSet(new long[]{0x0002000020000000L});
    public static final BitSet FOLLOW_29_in_ruleEventHandler13258 = new BitSet(new long[]{0x4000000000000000L,0x0000C00200000080L});
    public static final BitSet FOLLOW_ruleEventHandlerBody_in_ruleEventHandler13279 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_31_in_ruleEventHandler13291 = new BitSet(new long[]{0x0002000000000000L});
    public static final BitSet FOLLOW_49_in_ruleEventHandler13305 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleEventHandlerBody_in_entryRuleEventHandlerBody13341 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleEventHandlerBody13351 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleEventHandlerOperation_in_ruleEventHandlerBody13397 = new BitSet(new long[]{0x0002000000000002L});
    public static final BitSet FOLLOW_49_in_ruleEventHandlerBody13410 = new BitSet(new long[]{0x4000000000000000L,0x0000C00200000080L});
    public static final BitSet FOLLOW_ruleEventHandlerOperation_in_ruleEventHandlerBody13431 = new BitSet(new long[]{0x0002000000000002L});
    public static final BitSet FOLLOW_ruleEventHandlerOperation_in_entryRuleEventHandlerOperation13469 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleEventHandlerOperation13479 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleMemoOperation_in_ruleEventHandlerOperation13526 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSolveOperation_in_ruleEventHandlerOperation13553 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRaiseEvent_in_ruleEventHandlerOperation13580 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSendEventAsDispatch_in_ruleEventHandlerOperation13607 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleMemoOperation_in_entryRuleMemoOperation13642 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleMemoOperation13652 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_110_in_ruleMemoOperation13690 = new BitSet(new long[]{0x0000000000000000L,0x0001000000000000L});
    public static final BitSet FOLLOW_ruleMemoRule_in_ruleMemoOperation13711 = new BitSet(new long[]{0x0000000000000000L,0x0000100000000000L});
    public static final BitSet FOLLOW_108_in_ruleMemoOperation13723 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleMemoOperation13743 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleMemoCurrentEvent_in_ruleMemoOperation13772 = new BitSet(new long[]{0x0000000000000000L,0x0000100000000000L});
    public static final BitSet FOLLOW_108_in_ruleMemoOperation13784 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleMemoOperation13804 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSolveOperation_in_entryRuleSolveOperation13841 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleSolveOperation13851 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_62_in_ruleSolveOperation13888 = new BitSet(new long[]{0x00000220000000F0L,0x0020000000000000L});
    public static final BitSet FOLLOW_rulePTerm_in_ruleSolveOperation13909 = new BitSet(new long[]{0x0000000000000000L,0x0000100000000000L});
    public static final BitSet FOLLOW_108_in_ruleSolveOperation13921 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleSolveOperation13941 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSendEventAsDispatch_in_entryRuleSendEventAsDispatch13977 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleSendEventAsDispatch13987 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_111_in_ruleSendEventAsDispatch14024 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleSendEventAsDispatch14044 = new BitSet(new long[]{0x0000000000000000L,0x0000000000400000L});
    public static final BitSet FOLLOW_86_in_ruleSendEventAsDispatch14056 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleSendEventAsDispatch14076 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleMemoRule_in_entryRuleMemoRule14112 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleMemoRule14122 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleMemoEvent_in_ruleMemoRule14168 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleMemoEvent_in_entryRuleMemoEvent14202 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleMemoEvent14212 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_112_in_ruleMemoEvent14254 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleReaction_in_entryRuleReaction14302 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleReaction14312 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_113_in_ruleReaction14355 = new BitSet(new long[]{0x0000000000000000L,0x0018000000000000L});
    public static final BitSet FOLLOW_ruleAlarmEvent_in_ruleReaction14389 = new BitSet(new long[]{0x0000000000000002L,0x0004000000000000L});
    public static final BitSet FOLLOW_114_in_ruleReaction14402 = new BitSet(new long[]{0x0000000000000000L,0x0018000000000000L});
    public static final BitSet FOLLOW_ruleAlarmEvent_in_ruleReaction14423 = new BitSet(new long[]{0x0000000000000002L,0x0004000000000000L});
    public static final BitSet FOLLOW_ruleAlarmEvent_in_entryRuleAlarmEvent14461 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleAlarmEvent14471 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNormalEvent_in_ruleAlarmEvent14518 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleContinueEvent_in_ruleAlarmEvent14545 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNormalEvent_in_entryRuleNormalEvent14580 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleNormalEvent14590 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_115_in_ruleNormalEvent14633 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleNormalEvent14666 = new BitSet(new long[]{0x0000000000000000L,0x0000000100000000L});
    public static final BitSet FOLLOW_96_in_ruleNormalEvent14678 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleNormalEvent14698 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleContinueEvent_in_entryRuleContinueEvent14734 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleContinueEvent14744 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_116_in_ruleContinueEvent14787 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleContinueEvent14820 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVarOrQactor_in_entryRuleVarOrQactor14856 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleVarOrQactor14866 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVariable_in_ruleVarOrQactor14912 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleVarOrQactor14938 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVarOrAtomic_in_entryRuleVarOrAtomic14974 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleVarOrAtomic14984 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVariable_in_ruleVarOrAtomic15030 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePAtomic_in_ruleVarOrAtomic15057 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVarOrString_in_entryRuleVarOrString15093 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleVarOrString15103 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVariable_in_ruleVarOrString15149 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_STRING_in_ruleVarOrString15172 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVariable_in_entryRuleVariable15219 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleVariable15229 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_117_in_ruleVariable15272 = new BitSet(new long[]{0x0000000000000080L});
    public static final BitSet FOLLOW_RULE_VARID_in_ruleVariable15303 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTimeLimit_in_entryRuleTimeLimit15344 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleTimeLimit15354 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_118_in_ruleTimeLimit15397 = new BitSet(new long[]{0x0000000800000000L});
    public static final BitSet FOLLOW_35_in_ruleTimeLimit15422 = new BitSet(new long[]{0x00000000000000C0L,0x0020000000000000L});
    public static final BitSet FOLLOW_RULE_INT_in_ruleTimeLimit15440 = new BitSet(new long[]{0x0000001000000000L});
    public static final BitSet FOLLOW_ruleVariable_in_ruleTimeLimit15472 = new BitSet(new long[]{0x0000001000000000L});
    public static final BitSet FOLLOW_36_in_ruleTimeLimit15485 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleComponentIP_in_entryRuleComponentIP15521 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleComponentIP15531 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_54_in_ruleComponentIP15568 = new BitSet(new long[]{0x0000000000000000L,0x0080000000000000L});
    public static final BitSet FOLLOW_119_in_ruleComponentIP15580 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_RULE_STRING_in_ruleComponentIP15597 = new BitSet(new long[]{0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_120_in_ruleComponentIP15614 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_RULE_INT_in_ruleComponentIP15631 = new BitSet(new long[]{0x0100000000000000L});
    public static final BitSet FOLLOW_56_in_ruleComponentIP15648 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleMoveFile_in_entryRuleMoveFile15684 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleMoveFile15694 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_121_in_ruleMoveFile15737 = new BitSet(new long[]{0x0000000800000000L});
    public static final BitSet FOLLOW_35_in_ruleMoveFile15762 = new BitSet(new long[]{0x00000000000000A0L,0x0020000000000000L});
    public static final BitSet FOLLOW_ruleVarOrString_in_ruleMoveFile15783 = new BitSet(new long[]{0x0000001000000000L});
    public static final BitSet FOLLOW_36_in_ruleMoveFile15795 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_122_in_ruleWindowColor15845 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_123_in_ruleWindowColor15862 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_124_in_ruleWindowColor15879 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_125_in_ruleWindowColor15896 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_126_in_ruleWindowColor15913 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_127_in_ruleWindowColor15930 = new BitSet(new long[]{0x0000000000000002L});

}