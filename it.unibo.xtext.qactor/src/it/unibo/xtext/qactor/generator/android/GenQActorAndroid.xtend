package it.unibo.xtext.qactor.generator.android 
import it.unibo.xtext.qactor.*
import it.unibo.xtext.qactor.generator.common.GenKb
import it.unibo.xtext.qactor.generator.common.GenUtils
import it.unibo.xtext.qactor.generator.common.IGenQActorAndroid
import it.unibo.xtext.qactor.generator.common.SysKb

/*
 * ====================================================
 * ANDROID SUPPORTS
 * ====================================================
 */
class GenQActorAndroid implements IGenQActorAndroid{ 
protected var String packageName    = ""	 
protected var QActorSystemSpec system   
protected var String sysName        = ""	
protected var String activityClassName  = ""	
protected var String ctxClassName   = ""	 
protected var srcMoreDir = "srcMore";	

	override doGenerate(QActorSystemSpec qasys, Context ctx, GenKb kb) {
  		println(" *** GenQActorAndroid starts for " + qasys.name + " ctx=" + ctx.name )
		packageName      = "it.unibo."+kb.getActorSystemName(); //GenUtils.packageName ;
		system 			 = qasys 
		sysName 	     = kb.getActorSystemName().toLowerCase ;
		ctxClassName     = ctx.name ;
		activityClassName    = system.name.toFirstUpper + "Activity" ; //"Activity"+ctxClassName;
 		//Generate the Android info 
 		GenUtils.genFileDir(  "..", "", "AndroidManifestCustom",   "xml", genAndroidManifest(  ) )
//		if( SysKb.existFile("../res/values/colors.xml") ) return;
		//WARNING: the layout file must be lowercase
   		var fname = system.name.toLowerCase+"layout"
    	var fpath = "../res/layout/"+fname+".xml"
    	if( ! SysKb.existFile( fpath ) ) 	
 			GenUtils.genFileDir(  "../res/layout", "", system.name.toLowerCase+"layout",   "xml", genAndroidLayout(  ) )
 		GenUtils.genFileDir(  "../res/values", "", "colors",   "xml", genColors(  ) )
		GenUtils.genFileDir(  "../res/values", "", "strings",   "xml", genStrings(  ) )
		GenUtils.genFileDir(  "../res/values", "", "styles",   "xml", genStyles(  ) )

   		fname = system.name.toFirstUpper+"Activity"
   		fpath =  GenUtils.srcDirPath +packageName.replace(".","/")+"/"+fname+".java"
   		println(" *** GenQActorAndroid  " + fpath + " exists:" + SysKb.existFile( fpath ) )
   		if( ! SysKb.existFile( fpath ) ) 			 
   			GenUtils.genFileSrcDir(  packageName, fname , "java", genMainActivity(  ) )
	 		//Generate the Java support 
	  		GenUtils.genFileSrcDir(  packageName, "ActionCode",   "java", genActionCode(  ) )
	  		GenUtils.genFileSrcDir(  packageName, "BaseActivity",   "java", genBasectivity(  ) )
	  		GenUtils.genFileSrcDir(  packageName, "MessageHandler",   "java", genMessageHandler(  ) )
	  		GenUtils.genFileSrcDir(  packageName, "OutEnvView",   "java", genOutEnvView(  ) )
	  		GenUtils.genFileSrcDir(  packageName, "OutView",   "java", genOutView(  ) )
	   		GenUtils.genFileSrcDir(  packageName, "AndroSensorListenEvJson",   "java", genAndroSensorListenEvJson(  ) ) 	
  	}

def genStrings()'''
<?xml version="1.0" encoding="utf-8"?>
<resources>

    <string name="app_name">it.unibo.«system.name»</string>
    <string name="action_settings">Settings</string>
    <string name="hello_world">Hello world!</string>
    <string name="button">CLICK</string>
    <string name="input">192.168.43.229</string>
    
</resources>
'''

def genStyles()'''
<resources>

    <!--
        Base application theme, dependent on API level. This theme is replaced
        by AppBaseTheme from res/values-vXX/styles.xml on newer devices.
    -->
    <style name="AppBaseTheme" parent="android:Theme.Light">
        <!--
            Theme customizations available in newer API levels can go in
            res/values-vXX/styles.xml, while customizations related to
            backward-compatibility can go here.
        -->
    </style>

    <!-- Application theme. -->
    <style name="AppTheme" parent="AppBaseTheme">
        <!-- All customizations that are NOT specific to a particular API-level can go here. -->
    </style>

</resources>
'''

 def genColors()'''
<?xml version="1.0" encoding="utf-8"?>
<resources>
    <drawable name="red">#7f00</drawable>
    <drawable name="blue">#770000ff</drawable>
    <drawable name="green">#7700ff00</drawable>
	<drawable name="yellow">#77ffff00</drawable>
	<drawable name="black">#ff000000</drawable>
	<drawable name="white">#99ffffff</drawable>
	
	<drawable name="screen_background_black">#ff000000</drawable>
    <drawable name="translucent_background">#e0000000</drawable>
    <drawable name="transparent_background">#00000000</drawable>

    <color name="solid_red">#f00</color>
    <color name="solid_blue">#0000ff</color>
    <color name="solid_green">#f0f0</color>
    <color name="solid_yellow">#ffffff00</color>
</resources>
 '''
 def genAndroidManifest()'''
 <manifest xmlns:android="http://schemas.android.com/apk/res/android"
    package="«packageName»"
    android:versionCode="1"
    android:versionName="1.0" >

    <uses-sdk
        android:minSdkVersion="8"
        android:targetSdkVersion="21" />
    <uses-permission android:name="android.permission.INTERNET" />  
    <application
        android:allowBackup="true"
         android:theme="@style/AppTheme"  
        android:icon="@drawable/ic_launcher"
        android:label="@string/app_name" >
        <activity
            android:name=".«activityClassName»"
            android:label="@string/app_name" >
            <intent-filter>
                <action android:name="android.intent.action.MAIN" />
                <category android:name="android.intent.category.LAUNCHER" />
            </intent-filter>
        </activity>     
    </application>

</manifest>
 '''
  def genAndroidLayout()'''
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="fill_parent"
    android:layout_height="fill_parent"
    android:orientation="vertical" >
     <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="wrap_content" >
 	<Button
	    android:id="@+id/button"
	    android:layout_width="wrap_content"
	    android:layout_height="wrap_content"
	    android:text="@string/button" 
	    android:textSize="20sp"/>        
     </LinearLayout>
     <ScrollView
        android:id="@+id/scrollView1"
        android:layout_width="match_parent"
        android:layout_height="wrap_content" >

        <LinearLayout
            android:layout_width="match_parent"
            android:layout_height="fill_parent"
             android:orientation="vertical" >
		<TextView
			android:id="@+id/output"
			android:layout_width="fill_parent"
			android:layout_height="wrap_content"
			android:text=""
			android:textStyle="italic"
			android:textSize="6pt"			
			android:background="@drawable/white"
			android:textColor="@drawable/black"
		/>
        </LinearLayout>
    </ScrollView>
</LinearLayout>  
  '''	

	def genActionCode()'''
/* ======================================================
* By AN DISI University of Bologna
* ======================================================-
*/
package  «packageName»;
public class ActionCode {
	public static final int callTCPActionCode = 10;
	public static final int callHTTPActionCode = 11;
}//ActionCode
	''' 
	
		
	def genBasectivity()'''
/* ======================================================
* By AN DISI University of Bologna
* ======================================================-
*/
package  «packageName»;
import java.util.Iterator;
import java.util.List;
//import it.unibo.android.«system.name.toFirstUpper».R;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.is.interfaces.IOutputView;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;
import android.net.Uri;

public class BaseActivity extends Activity{
	
 public MessageHandler myHandler;
 protected static Context androidCtx;
 protected TextView output;
 protected IOutputView outView = null;
 protected IOutputEnvView outEnvView = null;
 protected Bundle myBundle;
 protected boolean verbose = false;
 protected SensorManager sensorManager;
 
 public static Context getAndroidContext(){
	 return androidCtx;
 }
	/* ======================================== 
	 *  ACTIVITY AS A COMPONENT
	 *	Activity cycle  
	 * ========================================
	 */
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		myHandler = new  MessageHandler(this);
		myBundle  = new Bundle();
 		output    = (TextView) findViewById(R.id.output);
 		outView    = new OutView(this);
 		outEnvView = new OutEnvView(this);		
 		androidCtx = this;
 		sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
	}
	protected void onStart() {
		super.onStart();
		traceMsg("onStart -> " + this.getLocalClassName());
	}//onStart
	protected void onResume() {
		super.onResume();
		traceMsg("onResume -> " + this.getLocalClassName());
	}//onResume
	protected void onRestart() {
		super.onRestart();
		traceMsg("onRestart -> " + this.getLocalClassName());
	}//onRestart
	protected void onPause() {
		super.onPause();
		traceMsg("onPause -> " + this.getLocalClassName());
	}//onPause
	protected void onStop() {
		super.onStop();
		traceMsg("onStop -> " + this.getLocalClassName());
	}//onStop
	protected void onDestroy() {
		super.onDestroy();
		traceMsg("onDestroy -> " + this.getLocalClassName());
	}//onDestroy
	/*
	 *  -------------------------------------
	 *   Utilities  
	 *  -------------------------------------
	 */
	public IOutputEnvView getOutputEnvView(){
		return outEnvView;
	}
	public IOutputView getOutputView(){
		return outView;
	}
	public void println(String msg) {
		if (output == null){ 
			output = (TextView) findViewById(R.id.output);
		}
		output.append(msg+"\n");
	}
	public void printMsg(String msg) {
		if (output == null){ 
			output = (TextView) findViewById(R.id.output);
		}
		output.setText( msg );
	}
	public void showMsg(String msg) {
		if (outView != null) 
		outView.addOutput(msg);
	}
	protected void traceMsg(String msg) {
		if (verbose)
			println(msg);
	}
	protected String activityInfo() {	
		return this.getLocalClassName() + " | taskId=" + this.getTaskId();
	}
	
	protected Uri makeUri(String uriStr, String data) {
		return Uri.parse(uriStr + data);
	}
	/*
	 *  -------------------------------------
	 *   Sensors  
	 *  -------------------------------------
	 */

	protected void findSensors(){
  		Toast.makeText(this, "Sys0Activity sensors" , Toast.LENGTH_SHORT).show();
		List<Sensor> availableSensors = sensorManager.getSensorList(Sensor.TYPE_ALL);
		Iterator<Sensor> itsens = availableSensors.iterator();
		while( itsens.hasNext() ){
			Sensor sens = itsens.next();
			println("SENSOR " + sens.getType() + " " + sens.getName());
 		}
	}
	protected void configureSensors(SensorEventListener handler){    
        try {
         	Sensor accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        	Sensor proximity     = sensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
        	Sensor magneto       = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        	Sensor gyro          = sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
        	/*
        	 * Register the  handler (for all the sensors)
        	 */
        	if( accelerometer != null ) sensorManager.registerListener(handler, proximity, SensorManager.SENSOR_DELAY_NORMAL);
        	if( proximity != null )     sensorManager.registerListener(handler, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);	
        	if( magneto != null )       sensorManager.registerListener(handler, magneto, SensorManager.SENSOR_DELAY_NORMAL);	
        	if( gyro != null )          sensorManager.registerListener(handler, gyro, SensorManager.SENSOR_DELAY_NORMAL);	
 		} catch (Exception e) {
			println("ERROR " + e.getMessage() );
 		}
	}

	
	/*
	 *  -------------------------------------
	 *   Android usage support
	 *  -------------------------------------
	 */
	 protected int notifNum = 0;
	 public void sendNotification(  String logo, String msg ) {
		sendNotification(notifNum++,R.drawable.ic_launcher,logo, msg,  null );
	}
	 
	public void sendNotification(int id, int iconId, String notifType, String msg,
			Class notifyClass) {
		NotificationManager notificationManager = 
			(NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		//Create Notification
		Notification notification = 
				new Notification(iconId,notifType, System.currentTimeMillis());
		notification.flags |= Notification.FLAG_AUTO_CANCEL;
		Intent intent;
		if( notifyClass != null )
			 intent = new Intent(this, notifyClass);
		else intent = new Intent( );
		intent.putExtra(notifType, msg);
		PendingIntent pIntent = 
			PendingIntent.getActivity(this, 0, intent,PendingIntent.FLAG_CANCEL_CURRENT);
		notification.setLatestEventInfo(this, notifType, msg, pIntent);
		//Use the Notification Manager
		notificationManager.notify(id, notification);
	}
		
	protected PendingIntent buildPendingIntent(String actionName, int requestCode){
		Intent myIntent = new Intent(actionName);
	    PendingIntent resIntent=
	    		PendingIntent.getBroadcast(this, requestCode, myIntent, PendingIntent.FLAG_ONE_SHOT);
	    return resIntent;
	}
	
	protected boolean checkActivityResult( int resCode, boolean show ){
	String outMsg ="";
	boolean result = false;
		switch( resCode ){
		case Activity.RESULT_OK:
			outMsg = "RESULT_OK";
			result = true;
			break;
		default: result = false;
		}//switch	
		if( show ) println( outMsg );
		return result;
	} 
} 
'''
 
def genMessageHandler()'''
/* ======================================================
* By AN DISI University of Bologna
* ======================================================-
*/
package «packageName»;
import android.os.Handler;
import android.os.Message;

public class MessageHandler extends Handler {
	protected BaseActivity myActivity;
  
	public MessageHandler(BaseActivity myActivity) {
		this.myActivity = myActivity;
	}
 
	public void handleMessage(Message msg) {
	String currentValue;
		try {
			currentValue = msg.getData().getString("addOutputMsg");
			if (currentValue != null) {
				myActivity.println(currentValue); //DO PRINT !!!
 			}
 			else{ 
 				currentValue = msg.getData().getString("setOutputMsg");
				if (currentValue != null) {
					myActivity.printMsg(currentValue); //DO PRINT !!!
				}
			}
 		} catch (Exception e) {
			System.out.println("ERROR" + e);
		}
	}
 }
'''

def genOutEnvView()'''
/* ======================================================
* By AN DISI University of Bologna
* ======================================================-
*/
package «packageName»;
import it.unibo.is.interfaces.IBasicEnvAwt;
import it.unibo.is.interfaces.IOutputEnvView;

public class OutEnvView extends OutView implements IOutputEnvView {

 
	public OutEnvView(BaseActivity myActivity) {
		super( myActivity );
	}

	@Override
	public IBasicEnvAwt getEnv() {
		return null;
	}
	
 	
} 
'''

def genOutView()'''
/* ======================================================
* By AN DISI University of Bologna
* ======================================================-
*/
package «packageName»;
import android.os.Bundle;
import android.os.Message;
import it.unibo.is.interfaces.IOutputView;

public class OutView implements IOutputView {

	protected BaseActivity myActivity;
	protected int nm = 1;
	protected String curVal = "";

	public OutView(BaseActivity myActivity) {
		this.myActivity = myActivity;
	}
	
	public String getCurVal() {
		return curVal;
	}

	public synchronized void addOutput(String msg) {
		curVal = msg ;
 		Message m = myActivity.myHandler.obtainMessage();
 		Bundle data = new Bundle();
 		data.putString("addOutputMsg", msg);
 		m.setData(data);
 		myActivity.myHandler.sendMessage(m);
	}
	
	public synchronized void setOutput(String msg) {
		curVal = msg ;
 		Message m = myActivity.myHandler.obtainMessage();
 		Bundle data = new Bundle();
 		data.putString("setOutputMsg", msg);
 		m.setData(data);
 		myActivity.myHandler.sendMessage(m);
	}
	
} 
'''

def genAndroSensorListenEvJson()'''
/* ======================================================
* By AN DISI University of Bologna
* ======================================================-
*/
package «packageName»;
import it.unibo.android.sensorData.implementation.AndroidSensorData;
import it.unibo.android.sensorData.implementation.motionSensors.AndroidAccelerometerData;
import it.unibo.android.sensorData.implementation.motionSensors.AndroidGyroscopeData;
import it.unibo.android.sensorData.implementation.positionSensors.AndroidMagneticFieldData;
import it.unibo.android.sensorData.implementation.positionSensors.AndroidProximityData;
import it.unibo.contactEvent.interfaces.IContactEventPlatform;
import it.unibo.contactEvent.platform.ContactEventPlatform;
import it.unibo.is.interfaces.IOutputView;
import it.unibo.system.SituatedPlainObject;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;

public class AndroSensorListenEvJson extends SituatedPlainObject implements SensorEventListener{
private static AndroidSensorData lastdata; 
private int nskip = 0;
private int n = 0;
private IContactEventPlatform platform;

	public AndroSensorListenEvJson(IOutputView outView, int nskip)  {
		super(outView);
		this.nskip = nskip;
		setEventPlatform();
 	}
	
	protected void setEventPlatform(){
		try{
			platform = ContactEventPlatform.getPlatform();
		}catch( Exception e ){
			println("WARNING: no event platform available");
		}		
	}
	public static AndroidSensorData getLastsensorData(){
		return lastdata;
	}

	@Override
	public void onAccuracyChanged(Sensor arg0, int arg1) {
 	}

	@Override
	public void onSensorChanged(SensorEvent sensorEvent) {
		if( sensorEvent.sensor.getType() == Sensor.TYPE_PROXIMITY ){
 			AndroidProximityData sensdata = new AndroidProximityData(
					sensorEvent.values,sensorEvent.accuracy,sensorEvent.timestamp);
			lastdata = sensdata;
// 			println(sensdata.getJsonRep());
//			println("prxomity="+sensdata.isPresent());
			raiseAnEvent();
		}
		if( sensorEvent.sensor.getType() == Sensor.TYPE_ACCELEROMETER ){
			if( n++  % nskip == 0 ){
	 			AndroidAccelerometerData sensdata = new AndroidAccelerometerData(
							sensorEvent.values,sensorEvent.accuracy,sensorEvent.timestamp);
	 			lastdata = sensdata;
// 	 	 		println(sensdata.getPrologRep());
//		 		println("g="+sensdata.getAcceleration());
  	 	 		raiseAnEvent();
			}
 		}
		if( sensorEvent.sensor.getType() == Sensor.TYPE_GYROSCOPE ){
			AndroidGyroscopeData sensdata = new AndroidGyroscopeData(
							sensorEvent.values,sensorEvent.accuracy,sensorEvent.timestamp);
	 		lastdata = sensdata;
   	 	 	raiseAnEvent();
 		}
		if( sensorEvent.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD ){
			AndroidMagneticFieldData sensdata = new AndroidMagneticFieldData(
							sensorEvent.values,sensorEvent.accuracy,sensorEvent.timestamp);
	 		lastdata = sensdata;
   	 	 	raiseAnEvent();
		}
 	}
	
	protected void raiseAnEvent(){
		String evntMsg = "sensorData("+lastdata.getPrologRep()+")";
//		println("raiseAnEvent " + evntMsg );
		if( platform == null ) setEventPlatform();
		if( platform != null ) platform.raiseEvent( "android", "androidsensor", evntMsg );
		 
	}	
}
'''

def genMainActivity()'''
«GenUtils.logo»
/*
This code is generated only ONCE
*/
package «packageName»;
import android.os.Bundle;
//import «packageName».«system.name»layout.R;

public class «system.name.toFirstUpper»Activity extends BaseActivity {
 
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.«system.name.toLowerCase»layout);
		completeTheConfiguration();
		launchThesensorSystem();
 		launchTheQaSystem();
	}
	protected void onPause() {
		super.onPause();
		//println("onPause " );
 		it.unibo.«ctxClassName».Main«ctxClassName.toFirstUpper».endQaApplOnAndroid(this);
	}//onPause
 	protected void launchThesensorSystem(){
 	    this.findSensors();
 	    this.configureSensors( new AndroSensorListenEvJson( outView, 10) );
 	}	
	protected void launchTheQaSystem(){
		it.unibo.«ctxClassName».Main«ctxClassName.toFirstUpper».initQaApplOnAndroid(this);	
 	}
	protected void completeTheConfiguration(){
		//TODO by the Application designer	
 	}
	
}'''
}