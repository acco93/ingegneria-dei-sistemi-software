package it.unibo.xtext.qactor.generator.common

import it.unibo.xtext.qactor.QActorSystemSpec

class GenKb {
 var QActorSystemSpec actorSystemModel
 var SysKb sysKb
 
   
	def initdomainModelKb( QActorSystemSpec model ){
  		this.actorSystemModel = model
  		sysKb = SysKb.getSysKb()
   		initComponents( ) 
  	}  	
  	
  	def initComponents( ){
  		sysKb.setDomainModel(actorSystemModel)
   	}

/*
 * Properties
 */
  	
  	def QActorSystemSpec getActorSystem( ){
  		return actorSystemModel;
  	}
  	def String getActorSystemName( ){
  		return actorSystemModel.name
  	}

 }