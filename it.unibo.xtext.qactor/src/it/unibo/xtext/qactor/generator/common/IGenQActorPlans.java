package it.unibo.xtext.qactor.generator.common;

import it.unibo.xtext.qactor.QActor;

public interface IGenQActorPlans {
	public  String doGenerate( QActor actor,  GenKb kb );
	//GRAPH
	public String pauGenerate( QActor actor,  GenKb kb );
	public String getFirstPlan();
	public String getLastsPlan();
	public void clearall();
	
}
