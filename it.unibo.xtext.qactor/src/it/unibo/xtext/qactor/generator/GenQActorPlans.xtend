package it.unibo.xtext.qactor.generator
import it.unibo.xtext.qactor.generator.common.IGenQActorPlans
import it.unibo.xtext.qactor.QActor
import it.unibo.xtext.qactor.generator.common.GenKb
import it.unibo.xtext.qactor.*
import java.util.HashMap
import java.util.ArrayList

class GenQActorPlans implements IGenQActorPlans{
protected Plan curPlan;
protected PlanAction curPlanAction;
protected QActor curAactor;
/*
 * GRAPH
 */
protected HashMap<String,ArrayList<String>> sciabomap = new HashMap<String,ArrayList<String>>();
protected ArrayList<String> sciabolist = new ArrayList<String>();
protected String planpau	// 'actual' plan
protected String initialplanname;
protected int countSwitch = 0;
protected ArrayList<String> finalStates = new ArrayList<String>();
protected ArrayList<String> emptyStates = new ArrayList<String>(); 
protected ArrayList<String> noEntryStates = new ArrayList<String>();
protected  ArrayList<String> allEntryStates = new ArrayList<String>();
protected  ArrayList<String> allPlanList = new ArrayList<String>();

 	override String doGenerate(QActor actor, GenKb kb) {
	var outS = ""
		curAactor = actor;
 		println(" *** GenQActorPlans starts for " + actor.name )
		for( plan : actor.plans ){
			curPlan = plan;
			outS = outS + genPlan( plan )
		} 
  		outS
 	}
/*
 * GRAPH
 */
	override pauGenerate(QActor actor, GenKb kb) {
			//throw new UnsupportedOperationException("TODO: auto-generated method stub")
			var outS = ''
			curAactor = actor;
	 		println(" *** *** pauGenerate starts for " + actor.name )
			for( plan : actor.plans ){
				allPlanList.add(plan.name)
				countSwitch = 0;
				sciabolist.clear();
				sciabomap.clear();
				curPlan = plan;
				println(" *** *** pauGenerate ... " + plan )
				if(plan.normal){
					initialplanname = plan.name;
				}
				outS = outS + pauPlan( plan )
				if(outS.equalsIgnoreCase('\n')){
					outS = ''
				}
			} 
	  		outS
		} 

 def pauPlan(Plan p)'''
 «genPauMoves(p)»
 '''
  	def genPauMoves( Plan p ){
 		planpau=p.name;
		var outS = '';
		for( action : p.action ){
			outS = outS + genPau(action.move);
			if(outS.equalsIgnoreCase('\n')){
				outS = '';
			}
		}
		for (HashMap.Entry<String,ArrayList<String>> entry : sciabomap.entrySet()) {
				allEntryStates.add(entry.key)
				
				outS=outS+'\n'+planpau+' -> '+ entry.key + ' [ label = "';
				for(item : entry.value){
					outS = outS + item + '/';
				}
				outS = outS + '"];';
		}
		if(countSwitch == 0){
			finalStates.add(planpau);
		}
		outS
	}
 
 def dispatch genPau(Move m){
	emptyStates.add(planpau);
	var outS = ''
	outS
}
def dispatch genPau( EventSwitch m){
	removeEmptyStates()
	var outs = '';
	var msgUserTemplate = ""+genPHead(m.event.msg)
	
	outs= outs + genPauWithVars(m.move, msgUserTemplate )
	outs;
	
}

def dispatch genPauWithVars(Move move, String msgUserTemplate ){
	
	sciabolist.add(msgUserTemplate)
	var outs=''
	outs
}
def dispatch genPauWithVars(SwitchPlan move, String msgUserTemplate ){
	var i = 0;
	countSwitch++;
	var outs='';
	for (  i = 0;  i < sciabolist.size(); i++){
            var tempName = sciabolist.get(i);
            if(tempName.equalsIgnoreCase(msgUserTemplate)){
                sciabolist.remove(i);
            }
        }
	i = 0;
	var piano = move.plan.name;
	var cont = 0;
	for (HashMap.Entry<String,ArrayList<String>> entry : sciabomap.entrySet()) {
    	if(piano.equals(entry.key)){
    		cont++;
    		entry.value.add(msgUserTemplate);
    	}
    }
    if (cont == 0){
    	var ArrayList<String> lista = new ArrayList<String>();
    	lista.add(msgUserTemplate);
    	sciabomap.put(piano, lista);
    }
    
    outs;
    
}
def dispatch genPau( MsgSwitch m){
	removeEmptyStates()
	var outs = '';
	var msgUserTemplate = ""+genPHead(m.message.msg)
	outs= outs + genPauWithVars(m.move, msgUserTemplate )
	outs;
}

def dispatch genPau( RepeatPlan m){
	removeEmptyStates();
	var ArrayList<String> sciabodopp = new ArrayList<String>();
	var outs = '\n'+planpau+' -> ';
	outs = outs +planpau + ' [ label = "' ;
	var bool = false;
	
	
	for (item : sciabolist){
		for (HashMap.Entry<String,ArrayList<String>> entry : sciabomap.entrySet()) {
			for(valore : entry.value){
				if(item.equalsIgnoreCase(valore)) bool = true;
			}
		}
		for(elem : sciabodopp){
			if(item.equalsIgnoreCase(elem)){
				bool = true;
			}
		}
		if(!bool){
			outs = outs + item + '/'
			sciabodopp.add(item);
		}
		bool = false;
	
		
	}
	outs = outs + '"];'
	outs;	
}

def dispatch genPau( SwitchPlan m){
	removeEmptyStates()
	var bool = false;
	var ArrayList<String> sciabodopp = new ArrayList<String>();
	countSwitch++;
	var outs ='\n'+planpau+' -> ';
	outs = outs + m.plan.name + ' [ label = "' ;
	for (item : sciabolist){
		for (HashMap.Entry<String,ArrayList<String>> entry : sciabomap.entrySet()) {
			for(valore : entry.value){
				if(item.equalsIgnoreCase(valore)) bool = true;
			}
		}
		for(elem : sciabodopp){
			if(item.equalsIgnoreCase(elem)){
				bool = true;
			}
		}
		if(!bool){
			outs = outs + item + '/'
			sciabodopp.add(item);
		}
		bool = false;
	
	}
	outs = outs + '"];'
	outs
	
}
def removeEmptyStates(){
		var i = 0;
		for (  i = 0;  i < emptyStates.size(); i++){
            var tempName = emptyStates.get(i);
            if(tempName.equalsIgnoreCase(planpau)){
                emptyStates.remove(i);
            }
		}
}
def removeNoEntry(){
	var i = 0;
		for (  i = 0;  i < finalStates.size(); i++){
			var tempName = finalStates.get(i);
            for(item : noEntryStates){
            	if(tempName.equalsIgnoreCase(item)){
                	finalStates.remove(i);
           		}
            }
		}
	
}


def createNoEntry(){
	var bool = false;
	for(item : allPlanList){
		for(elem : allEntryStates){
			if(item.equalsIgnoreCase(elem)){
				bool = true;
			}
		}
		if(!bool){
			noEntryStates.add(item)
		}
		bool = false;
	}
}
	override getFirstPlan() {
		//throw new UnsupportedOperationException("TODO: auto-generated method stub")
		return initialplanname;
	}
	
	override getLastsPlan() {
		//throw new UnsupportedOperationException("TODO: auto-generated method stub")
		var str = '';
		var bool = false;
		createNoEntry();
		removeNoEntry()
		for(item : finalStates){
			
			for (elem : emptyStates){
				if(item.equals(elem)){
					bool = true;
				}
			}
		
			if (!bool){
				str = str + item + '; ';
		}	
		}
		str;
	}
	
	override clearall() {
		//throw new UnsupportedOperationException("TODO: auto-generated method stub")
		initialplanname = ''
		countSwitch = 0
		allPlanList.clear()
		allEntryStates.clear();
		finalStates.clear()
		emptyStates.clear()
		noEntryStates.clear()
	}
	
/*
 * ENDGRAPH
 */
 	
 /*
  * ---------------------------------------------
  * Plan
  * ---------------------------------------------
  */
 def genPlan( Plan p )'''
 	public boolean «p.name»() throws Exception{	//public to allow reflection
 	try{
 		curPlanInExec =  "«p.name»";
 		boolean returnValue = suspendWork;
 	while(true){
 	nPlanIter++;
		«genMoves(p)»
 	break;
 	}//while
 	return returnValue;
 	}catch(Exception e){
 	   //println( getName() + " plan=«p.name» WARNING:" + e.getMessage() );
 	   QActorContext.terminateQActorSystem(this); 
 	   return false;  
 	}
 	}
 '''
 	
	def genMoves( Plan p ){
	var outS = ""
		for( action : p.action ){
			curPlanAction = action
			if( action.guard != null ) outS = outS + genGuard( action.guard ) +"{\n"
			outS = outS + genMove( action.move )	
			if( action.guard != null ) outS = outS + "}\n"				 
			if( action.elsemove != null ) outS = outS + genPlanElseMove(action)
		}
		if( p.resume ){
				outS = outS +'''
returnValue = continueWork;  
''' 
		}
		outS
	}
	
	def genPlanElseMove( PlanAction pa )'''
	else{ « genMove( pa.elsemove )»}'''
/*
 * -----------------------------------------------------------
 * GUARD
 * ASSUMPTION; g  != null
 * -----------------------------------------------------------
 */
def genGuard( Guard g ){ genGuard(g, g.guardspec) }

def genGuard( Guard g, GuardPredicate gs )'''
if( (guardVars = QActorUtils.evalTheGuard(this, «IF(g.not)»"«genGuardPredicate("not ",g.guardspec)»" )) != null )
«ELSE»"«genGuardPredicate("",g.guardspec)»" )) != null )«ENDIF»'''
def dispatch genGuardPredicate(String prefix,GuardPredicate g)'''not here GuardPredicate'''
def dispatch genGuardPredicate(String prefix,GuardPredicateRemovable g)''' «prefix»??«genPTerm(g.pred)»'''
def dispatch genGuardPredicate(String prefix,GuardPredicateStable g)''' «prefix»!?«genPTerm(g.pred)»'''

//def dispatch genGuard( Guard g, GuardSpec gs )'''not here GuardSpec'''
/*
 * Aguzzi
 */
//def dispatch genGuard( Guard g, GuardQuery gs )'''
//guardVars = QActorUtils.evalTheGuard(this, «IF(g.not)»"«genGuardSpec("not ",g.guardspec)»«ELSE»"«genGuardSpec("",g.guardspec)»«ENDIF»" );
//if( guardVars!= null )
//'''
//def dispatch genGuard( Guard g, GuardAtom gs )'''
//guardVars = QActorUtils.evalTheGuard(this, «IF(g.not)»"«genGuardSpec("not ",g.guardspec)»«ELSE»"«genGuardSpec("",g.guardspec)»«ENDIF»" );
//if( guardVars!= null )
//'''

//def dispatch genGuard( Guard g, GuardQuery gs )'''
//if( QActorUtils.evalTheGuard(this, «IF(g.not)»"«genGuardSpec("not ",g.guardspec)»" ) != null )
//«ELSE»"«genGuardSpec("",g.guardspec)»" ) != null )«ENDIF»'''

//def dispatch genGuard( Guard g, GuardWait gs )'''
////execute a ActionDummy that continues when «gs.evId.name»
//aar = execDummyActionForGuardWait( "«gs.evId.name»" );
//'''



//def dispatch genGuardSpec(String prefix, GuardSpec g)'''not here GuardSpec'''
//def dispatch genGuardSpec(String prefix, GuardQuery g)'''«genGuardQuery(prefix,g)»'''
// 
//def dispatch genGuardQuery(String prefix,GuardQuery g)'''not here GuardQuery'''
//def dispatch genGuardQuery(String prefix,GuardAtom g)'''«prefix»«g.atom»«IF(g.stay)»!«ENDIF»'''
//def dispatch genGuardQuery(String prefix,GuardPredicate g)'''not here GuardPredicate'''
//def dispatch genGuardQuery(String prefix,GuardPredicateRemovable g)'''
//«prefix»??«genPTerm(g.pred)»'''
//def dispatch genGuardQuery(String prefix,GuardPredicateStable g)'''
//«prefix»!?«genPTerm(g.pred)»'''
/* 
def genPredicate(Predicate pred)'''
«pred.functor»(«FOR arg: pred.args SEPARATOR ","»«genGuardArg(arg)»«ENDFOR»)'''
def dispatch genGuardSpec(String prefix,GuardWait g)'''
«prefix»!!«g.evId.name»'''
def dispatch genGuardArg(GuardArg arg)'''not here GuardArgs'''
def dispatch genGuardArg(VarArg arg)'''«arg.atom»'''
def dispatch genGuardArg(IntAtonArg arg)'''«arg.atom»'''
def dispatch genGuardArg(StringArg arg)'''"«arg.atom»"'''
*/

/*
 * PStruct
 */
def genPStruct( PStruct ps )'''«ps.name»(«FOR term: ps.msgArg SEPARATOR ','»«genPTerm(term)»«ENDFOR»)'''
def dispatch genPTerm( PTerm pt )'''not here Pterm'''
def dispatch genPTerm( PAtom pt )'''not here PAtom'''
def dispatch genPTerm( PAtomic pt )'''«pt.^val»'''
def dispatch genPTerm( PAtomString pt )'''\"«pt.^val»\"'''	//no blanks
def dispatch genPTerm( Variable pt )'''«pt.name»'''
def dispatch genPTerm( PAtomNum pt )'''«pt.^val»'''
def dispatch genPTerm( PStruct ps )'''«genPStruct(ps)»'''
//def dispatch genPTerm( Data d )'''not here Data'''
//def dispatch genPTerm( IntegerData d )'''«d.value»'''
//def dispatch genPTerm( StringData d )'''"«d.value»"'''
/*
 * Aguzzi
 */
//def dispatch genPTerm (PPredicate pt)'''not here predicate'''
def dispatch genPTerm(PIs pt)'''«genPTerm(pt.varout)» is «genPTerm(pt.varin)» + «genPTerm(pt.num)»'''
def dispatch genPTerm( PAtomCut pt )''' ! '''


//def genGuardAfterWait( GuardWait g)'''
//«IF g.guard != null »«IF( g.not)»«genGuardQuery("not", g.guard)»«ELSE»«genGuardQuery("",g.guard)»«ENDIF»«ELSE»true«ENDIF»'''

	
	def  dispatch genMove( PlanAction  pa){}
	def  dispatch genMove( Move  move)'''//not here genMove Move'''
	def  dispatch genMove( ActionMove  m )'''//not here ActionMove'''
		
	def  dispatch genMove( ExecuteAction  m ){
			if( m.sentence != null ) return genMoveSentence(m.sentence)
			else return genMoveAction(m)
	}

def genMoveSentence(PHead m)'''
temporaryStr = "«genPHead(m)»";
«IF curPlanAction.guard != null » 
temporaryStr = QActorUtils.substituteVars(guardVars,temporaryStr);
«ENDIF»
aar = actionUtils.solveSentence(temporaryStr);
'''
	
def   genMoveAction( ExecuteAction  m )'''
//«m.action.name»;
«IF ! (m.arg == null) »
temporaryStr = "«genPHead(m.arg)»";
  «IF curPlanAction.guard != null » 
  temporaryStr = QActorUtils.substituteVars(guardVars,temporaryStr);
 «ENDIF»
 «ENDIF»
	«IF m.action.msec==0»
actionResult = «m.action.name»(«IF m.action.arg != null»"«genPStruct(m.action.arg)»"«ELSE»""«ENDIF» , «IF m.arg != null»temporaryStr«ELSE»""«ENDIF»);
if( !actionResult ) break;
	«ELSE»
aar = executeActionAsFSM( new it.unibo.qactors.action.ActionApplication( outEnvView, «m.action.msec» , this, 
	    	"«m.action.name»" , «IF m.action.arg != null»"«genPStruct(m.action.arg)»"«ELSE»""«ENDIF» , «IF m.arg != null»temporaryStr«ELSE»""«ENDIF»),
			"«genReacEvents(curPlanAction.react)»" , "«genReactPlans(curPlanAction.react)»",ActionExecMode.synch );
if(  ! aar.getGoon() ) break;
 «ENDIF»
	'''
	def  dispatch genMove( SolveGoal  m )''' 
		parg = "«genPHead(m.goal)»";
		«IF curPlanAction.guard != null » 
		  parg = QActorUtils.substituteVars(guardVars,parg);
		«ENDIF»
		//REGENERATE AKKA
		aar = solveGoalReactive(parg,«genTimeLimit(m.duration)»,"«genReacEvents(curPlanAction.react)»","«genReactPlans(curPlanAction.react)»");
		«genCheckAar(m.name)»		
 	'''
	def  dispatch genMove( Demo  m )''' 
		parg = "«genPHead(m.goal)»";
		«IF curPlanAction.guard != null » 
		  parg = QActorUtils.substituteVars(guardVars,parg);
		«ENDIF»
		//tout=1 day (24 h)
		//aar = solveGoalReactive(parg,86400000,"«genReacEvents(curPlanAction.react)»","«genReactPlans(curPlanAction.react)»");
		//genCheckAar(m.name)»		
		QActorUtils.solveGoal(parg,pengine );
 	'''
	
def dispatch genMove( ActorOp  m )''' 
		parg = "actorOp(«genPHead(m.goal)»)";
		«IF curPlanAction.guard != null »parg = QActorUtils.substituteVars(guardVars,parg);«ENDIF»
		//aar = solveGoalReactive(parg,3600000,"«genReacEvents(curPlanAction.react)»","«genReactPlans(curPlanAction.react)»");
		//genCheckAar(m.name)»
		QActorUtils.solveGoal(parg,pengine );
'''
	
	def genMove( SolveGoal  m, String varRepo )''' 
		{ String parg = "«genPHead(m.goal)»";
		«IF curPlanAction.guard != null » 
		  parg = QActorUtils.substituteVars(guardVars,parg);
		«ENDIF»
		  //aar = solveGoal( parg , «genTimeLimit(m.duration)», "","«genReacEvents(curPlanAction.react)»" , "«genReactPlans(curPlanAction.react)»" );
		  aar = QActorUtils.solveGoal(this,myCtx,pengine,parg,"«genReacEvents(curPlanAction.react)»",outEnvView,«genTimeLimit(m.duration)»);
		«genCheckAar(m.name)»
		if( aar.getResult().equals("failure")){
		«IF m.plan != null»if( ! switchToPlan("«m.plan.name»").getGoon() ) break;«ELSE»if( ! aar.getGoon() ) break;«ENDIF»
		}else if( ! aar.getGoon() ) break;
		}
	'''

	def  dispatch genMove( Print  move)'''
		temporaryStr = "«genPHead(move.args)»";
		  «IF curPlanAction.guard != null » 
		temporaryStr = QActorUtils.substituteVars(guardVars,temporaryStr);
		  «ENDIF»
		println( temporaryStr );  
 	'''
//OLD aar = solveGoal( «IF m.goal.^var!=null»«genVariable( m.goal.^var)»«ELSEIF  m.goal.atom!=null»"«m.goal.atom.^val»"«ELSE»"«genPStruct(m.goal.psrtuct)»"«ENDIF», «genTimeLimit(m.duration)», "","«genReacEvents(curPlanAction.react)»" , "«genReactPlans(curPlanAction.react)»"  );
 
 /*
 * PArgTerm
 */
//def  dispatch genPArgTerm( PArgTerm pt )'''not here PArgTerm'''
//def  dispatch  genPArgTerm( PStruct pt ){genPStruct(pt)}
//def  dispatch  genPArgTerm( PAtom pt ){genPTerm(pt)}  
//def  dispatch  genPArgTerm( PAtomString pt )''' «pt.^val» '''


	
//	def genOtherArgs(MoveArgs args)'''«IF args.vars.length>0»+«ENDIF»«FOR vos:args.vars SEPARATOR '+'»«genVoS(vos)»«ENDFOR»'''
	
	def  dispatch genMove( PrintCurrentEvent  move)'''
		printCurrentEvent(«IF move.memo»true«ELSE»false«ENDIF»);
 	'''
	def  dispatch genMove( PrintCurrentMessage  move)'''
 		printCurrentMessage(«IF move.memo»true«ELSE»false«ENDIF»);
	'''
	def  dispatch genMove( MemoCurrentMessage  move)'''
 		memoCurrentMessage( «move.lastonly» );
	'''
	def  dispatch genMove( MemoCurrentEvent  move)'''
 		memoCurrentEvent( «move.lastonly» );
	'''
	
	
	def  dispatch genMove( SendDispatch move )''' 
 	temporaryStr = QActorUtils.unifyMsgContent(pengine,"«genPHead(move.msgref.msg)»","«genPHead(move.^val)»", guardVars ).toString();
 	sendMsg("«move.msgref.name»",«genVarQactor(move.dest)», QActorContext.dispatch, temporaryStr ); 
	''' 
	
 	
	
	def  dispatch genMove( ReplyToCaller move )''' 
	temporaryStr = "«genPHead(move.^val)»";
	«IF curPlanAction.guard != null » 
	temporaryStr = QActorUtils.substituteVars(guardVars,temporaryStr );
	«ENDIF»
	replyToCaller("«move.msgref.name»", temporaryStr);
	''' 
	def  dispatch genMove( SendRequest move )''' 
 	temporaryStr = QActorUtils.unifyMsgContent(pengine,"«genPHead(move.msgref.msg)»","«genPHead(move.^val)»", guardVars ).toString();
 	sendMsg("«move.msgref.name»",«genVarQactor(move.dest)», QActorContext.request, temporaryStr  );
	''' 
	def  dispatch genMove( ReceiveMsg move ){
		if( move.spec == null)
	'''//ReceiveMsg
 		 aar = planUtils.receiveAMsg(mysupport,«move.duration.msec», "«genReacEvents(curPlanAction.react)»" , "«genReactPlans(curPlanAction.react)»" ); 	//could block
		if( aar.getInterrupted() ){
			curPlanInExec   = "playTheGame";
			if( ! aar.getGoon() ) break;
		} 			
		//if( ! aar.getGoon() ){
			//System.out.println("			WARNING: receiveMsg in " + getName() + " TOUT " + aar.getTimeRemained() + "/" +  «move.duration.msec»);
			//addRule("tout(receive,"+getName()+")");
		//} 		 
		//println(getName() + " received " + aar.getResult() );
''' 
else'''//ReceiveMsg
		 aar  = planUtils.receiveMsg(mysupport,
		 "«move.spec.msg.name»" ,"MSGTYPE", 
		 «genVarAtomicStr(move.spec.sender)»,this.getName(), 
		 "«genPHead(move.spec.content)»","MSGNUM", «move.duration.msec», "«genReacEvents(curPlanAction.react)»" , "«genReactPlans(curPlanAction.react)»");	//could block
		«genCheckAar(move.name)»
		if( aar.getInterrupted() ){
			curPlanInExec   = "playTheGame";
			if( ! aar.getGoon() ) break;
		} 			
		//if( ! aar.getGoon() ||  aar.getResult().contains("receive(timeOut(") ){
			//System.out.println("			WARNING: receiveMsg in " + getName() + " TOUT " + aar.getTimeRemained() + "/" +  «move.duration.msec»);
		//	addRule("tout(receive,"+getName()+")");
		//} 		 
		 //println(getName() + " received " + aar.getResult() );
'''
}
//def  dispatch genMove( OnReceive recMove )'''
//	planUtils.receiveAction( mysupport, «genTimeLimit(recMove.duration)» ); //"«recMove.move.plan.name»" , «genTimeLimit(recMove.duration)»
//'''
 	
	def  dispatch genMove( OnReceiveMsg move )'''
	//OnReceiveMsg 
		 aar  = planUtils.receiveMsg(mysupport,
		 "«genPHead(move.msgid)»" , "«genPHead(move.msgtype)»" , 
		 "«genPHead(move.msgsender)»" , "«genPHead(move.msgreceiver)»" ,
		 "«genPHead(move.msgcontent)»" , "«genPHead(move.msgseqnum)»", «move.duration.msec», "«genReacEvents(curPlanAction.react)»" , "«genReactPlans(curPlanAction.react)»");	//could block
		 «genCheckAar(move.name)»
		//if( ! aar.getGoon() ||  aar.getResult().contains("receive(timeOut(") ){
			//System.out.println(" " + getName() + " no goon " + aar.getTimeRemained() + "/" +  «move.duration.msec»);
			//addRule("tout(receive,"+getName()+")");
		//}
	''' 
	
def  dispatch genMove( RaiseEvent move )'''
 	temporaryStr = QActorUtils.unifyMsgContent(pengine, "«genPHead(move.ev.msg)»","«genPHead(move.content)»", guardVars ).toString();
 	emit( "«move.ev.name»", temporaryStr );
'''

def  dispatch genMove( SenseEvent move )'''
//senseEvent
timeoutval = «genTimeLimit(move.duration)»;
aar = planUtils.senseEvents( timeoutval,"«FOR ev:move.events SEPARATOR ','»«ev.name»«ENDFOR»","«FOR c:move.plans SEPARATOR ','»«genContinuation(c)»«ENDFOR»",
"«genReacEvents(curPlanAction.react)»" , "«genReactPlans(curPlanAction.react)»",ActionExecMode.synch );
if( ! aar.getGoon() || aar.getTimeRemained() <= 0 ){
	//println("			WARNING: sense timeout");
	addRule("tout(senseevent,"+getName()+")");
	//break;
}
'''


def genContinuation( Continuation c ){
	if( c.nane != null ) return '''continue'''
	else '''«c.plan.name»'''
}
def  dispatch genMove( MsgSelect se )'''
//MsgSelect
aar = planUtils.receiveMsgAndSwitch( mysupport,
"msgselect([«FOR ev:se.messages SEPARATOR ','»«ev.name»«ENDFOR»],[«FOR p:se.plans SEPARATOR ','»«p.name»«ENDFOR»])", 
"«genReacEvents(curPlanAction.react)»" , "«genReactPlans(curPlanAction.react)»",
«genTimeLimit(se.duration)»
);
if( aar.getInterrupted() ){
	curPlanInExec   = "«curPlan.name»";
	if( ! aar.getGoon() ) break;
}
//if( ! aar.getGoon() ){
//	System.out.println("			WARNING: receiveMsgAndSwitch in " + getName() + " TOUT " + aar.getTimeRemained() + "/" +  1000);
//	addRule("tout(receive,"+getName()+")");
//} 		 
''' 

//def dispatch genMove( MsgSwitch m)'''
//if( currentMessage.msgId().equals("«m.message.name»")){
////	Term msgtuser  = Term.createTerm(currentMessage.msgContent());
////	Term msgtdef = Term.createTerm("«genPHead(m.msg)»");
////	if( pengine.unify(msgtuser, msgtdef) ){
//	temporaryStr="«genPHead(m.msg)»";
//	temporaryStr = updateVars("«genPHead(m.message.msg)»", currentMessage.msgContent(), temporaryStr);
//	println("*************" + temporaryStr);
//	aar = solveGoal( temporaryStr , 0, "","" , "" );
//		«genMoveWithVars(m.move, temporaryStr, temporaryStr)»	//
////	}
//}
//'''

/*
 * OLD VERSION
if( currentExternalEvent.getEventId().equals("«m.event.name»") ){
	Term contentList = Term.createTerm("[ «genPHead(m.msg)» ]");
	Term planList    = Term.createTerm("[ «FOR c:m.plan SEPARATOR ','»«genContinuation(c)»«ENDFOR» ]");
	aar = eventswitch( contentList, planList);
	if( ! aar.getGoon() ) break;
}
*/
def dispatch genMove( MsgSwitch m){
	var msgUserTemplate = ""+genPHead(m.msg)
	var msgTemplate     = ""+genPHead(m.message.msg)
	return 
'''//onMsg
if( currentMessage.msgId().equals("«m.message.name»") ){
	«genMoveWithVars("currentMessage.msgContent()",m.move, msgUserTemplate, msgTemplate)»
}'''
}
def dispatch genMove( EventSwitch m){	
 	var msgUserTemplate = ""+genPHead(m.msg)
	var msgTemplate     = ""+genPHead(m.event.msg)
	return 
'''//onEvent
if( currentEvent.getEventId().equals("«m.event.name»") ){
 		«genMoveWithVars("currentEvent.getMsg()",m.move, msgUserTemplate, msgTemplate)»
 }
 '''
}
//def genMoveSwtich( Move m, String msgUserTemplate, String msgTemplate )'''
//	Term msgtuser  = Term.createTerm("«msgUserTemplate»");
//	Term msgtdef   = Term.createTerm("«msgTemplate»");
//    «genMoveWithVars( m , msgUserTemplate, msgTemplate »
// '''
def dispatch genMoveWithVars( String curT, Move move, String msgUserTemplate,  String msgTemplate   )'''
	//println("WARNING: variable substitution not yet implmented " ); 
	«genMove(move)»
'''
def dispatch genMoveWithVars( String curT, SwitchPlan move, String msgUserTemplate,  String msgTemplate  )'''
String parg = "";
«IF curPlanAction.guard != null » 	
parg =  updateVars(  Term.createTerm("«msgTemplate»"), Term.createTerm("«msgUserTemplate»"), 
	    		  					Term.createTerm(«curT»), parg);
«ELSE»
/* SwitchPlan */
parg =  updateVars(  Term.createTerm("«msgTemplate»"), Term.createTerm("«msgUserTemplate»"), 
	    		  					Term.createTerm(«curT»), parg);
 «ENDIF»	
	if( parg != null ){
		 if( ! planUtils.switchToPlan("«move.plan.name»").getGoon() ) break; 
	}//else println("guard «curPlanAction.guard» fails");  //parg is null when there is no guard (onEvent)
'''

def dispatch genMoveWithVars( String curT, ActorOp move, String msgUserTemplate,  String msgTemplate  )'''
String parg = "actorOp(«genPHead(move.goal)»)";
«IF curPlanAction.guard != null » 	
parg = updateVars( Term.createTerm("«msgTemplate»"), Term.createTerm("«msgUserTemplate»"), 
	    		  					Term.createTerm(«curT»), parg);
«ELSE»
/* ActorOp */
parg =  updateVars( Term.createTerm("«msgTemplate»"), Term.createTerm("«msgUserTemplate»"), 
	    		  					Term.createTerm(«curT»), parg);
 «ENDIF»	
if( parg != null ){
	aar = solveGoalReactive(parg,3600000,"«genReacEvents(curPlanAction.react)»","«genReactPlans(curPlanAction.react)»");
	«genCheckAar(move.name)»
}
'''

def dispatch genMoveWithVars( String curT, Print move, String msgUserTemplate,  String msgTemplate  )'''
String parg = "«genPHead(move.args)»";
«IF curPlanAction.guard != null » 	
parg = updateVars(  Term.createTerm("«msgTemplate»"), Term.createTerm("«msgUserTemplate»"), 
	    		  					Term.createTerm(«curT»), parg);
«ELSE»
/* Print */
parg =  updateVars( Term.createTerm("«msgTemplate»"), Term.createTerm("«msgUserTemplate»"), 
	    		  					Term.createTerm(«curT»), parg);
 «ENDIF»	
	if( parg != null ) println( parg );  
'''
def dispatch genMoveWithVars( String curT, EndPlan m, String msgUserTemplate,  String msgTemplate  )'''
String parg="«m.msg»";
«IF curPlanAction.guard != null » 
parg = updateVars(Term.createTerm("«msgTemplate»"),  Term.createTerm("«msgUserTemplate»"), 
	    		  					Term.createTerm(«curT»), parg);
«ELSE»
/* EndPlan */
parg =  updateVars( Term.createTerm("«msgTemplate»"),  Term.createTerm("«msgUserTemplate»"), 
	    		  					Term.createTerm(«curT»), parg);
 «ENDIF»
if( parg != null ){
	 println( parg);
	«IF(curPlan.resume)»returnValue = continueWork;«ENDIF»
}					
'''

def dispatch genMoveWithVars( String curT, AddRule m, String msgUserTemplate,  String msgTemplate  )'''
String parg="«genPHead(m.rule)»";
«IF curPlanAction.guard != null »
parg = updateVars( Term.createTerm("«msgTemplate»"),  Term.createTerm("«msgUserTemplate»"), 
	    		  					Term.createTerm(«curT»), parg);
«ELSE»
/* AddRule */
parg = updateVars(Term.createTerm("«msgTemplate»"),  Term.createTerm("«msgUserTemplate»"), 
	    		  					Term.createTerm(«curT»), parg);
«ENDIF»
if( parg != null ) addRule(parg);	    		  					
'''
def dispatch genMoveWithVars( String curT, RemoveRule m, String msgUserTemplate,  String msgTemplate  )'''
String parg="«genPHead(m.rule)»";
«IF curPlanAction.guard != null »
parg = updateVars( Term.createTerm("«msgTemplate»"),  Term.createTerm("«msgUserTemplate»"), 
	    		  					Term.createTerm(«curT»), parg);
«ELSE»
/* RemoveRule */
parg = updateVars( Term.createTerm("«msgTemplate»"),  Term.createTerm("«msgUserTemplate»"), 
	    		  					Term.createTerm(«curT»), parg);
«ENDIF»
if( parg != null ) removeRule(parg);
'''
def dispatch genMoveWithVars( String curT, RaiseEvent m, String msgUserTemplate,  String msgTemplate  )'''
String parg="«genPHead(m.content)»";
«IF curPlanAction.guard != null »
parg = updateVars(Term.createTerm("«msgTemplate»"),  Term.createTerm("«msgUserTemplate»"), 
	    		  					Term.createTerm(«curT»), parg);
«ELSE»
/* RaiseEvent */
parg = updateVars(Term.createTerm("«msgTemplate»"),  Term.createTerm("«msgUserTemplate»"), 
	    		  					Term.createTerm(«curT»), parg);
«ENDIF»
if( parg != null ) emit( "«m.ev.name»", parg );
''' 
def dispatch genMoveWithVars( String curT, SendDispatch m, String msgUserTemplate,  String msgTemplate  )'''
String parg="«genPHead(m.^val)»";
«IF curPlanAction.guard != null »
parg = updateVars( Term.createTerm("«msgTemplate»"),  Term.createTerm("«msgUserTemplate»"), 
	    		  					Term.createTerm(«curT»), parg);
«ELSE»
/* SendDispatch */
parg = updateVars(Term.createTerm("«msgTemplate»"),  Term.createTerm("«msgUserTemplate»"), 
	    		  					Term.createTerm(«curT»), parg);
«ENDIF»
if( parg != null ) sendMsg("«m.msgref.name»",«genVarQactor(m.dest)», QActorContext.dispatch, parg ); 
'''

def  dispatch genMoveWithVars( String curT, ReplyToCaller m, String msgUserTemplate,  String msgTemplate )'''
String parg="«genPHead(m.^val)»";
«IF curPlanAction.guard != null »
parg = updateVars( Term.createTerm("«msgTemplate»"),  Term.createTerm("«msgUserTemplate»"), 
	    		  					Term.createTerm(«curT»), parg);
«ELSE»
/* ReplyToCaller */
parg = updateVars( Term.createTerm("«msgTemplate»"),  Term.createTerm("«msgUserTemplate»"), 
	    		  					Term.createTerm(«curT»), parg);
«ENDIF»
if( parg != null ) replyToCaller("«m.msgref.name»", parg);
'''

def dispatch genMoveWithVars( String curT, SolveGoal m, String msgUserTemplate,  String msgTemplate   ){	 
	genMoveWithVars("demo",curT,m.goal,""+genTimeLimit(m.duration),m.plan,msgUserTemplate, msgTemplate )
}
def dispatch genMoveWithVars( String curT, Demo m, String msgUserTemplate,  String msgTemplate   ){
	genMoveWithVars("demo",curT,m.goal,"86400000",m.plan,msgUserTemplate, msgTemplate )
}
 
def genMoveWithVars( String name, String curT, PHead goal, String duration, Plan plan, String msgUserTemplate,  String msgTemplate   )'''
String parg="«genPHead(goal)»";
«IF curPlanAction.guard != null » 	
parg = updateVars( Term.createTerm("«msgTemplate»"),  Term.createTerm("«msgUserTemplate»"), 
	    		  					Term.createTerm(«curT»), parg);
«ELSE»
/* PHead */
parg =  updateVars( Term.createTerm("«msgTemplate»"), Term.createTerm("«msgUserTemplate»"), 
	    		  					Term.createTerm(«curT»), parg);
«ENDIF»	
	if( parg != null ) {
	    aar = QActorUtils.solveGoal(this,myCtx,pengine,parg,"«genReacEvents(curPlanAction.react)»",outEnvView,«duration»);
		«genCheckAar(name)»
		if( aar.getResult().equals("failure")){
			«IF plan != null»if( ! planUtils.switchToPlan("«plan.name»").getGoon() ) break;«ELSE»if( ! aar.getGoon() ) break;«ENDIF»
		}else if( ! aar.getGoon() ) break;
	}
''' 
/*
 * Action guard
 */
def dispatch genGuardSpec( Guard g, GuardPredicate  gs )'''not here GuardPredicate'''
//def dispatch genGuardSpec( Guard g, GuardAtom  gs )'''«IF g.not»not «ENDIF» «gs.atom»'''
def dispatch genGuardSpec( Guard g, GuardPredicateStable gs )'''«IF g.not»not «ENDIF» «genPTerm(gs.pred)»'''
def dispatch genGuardSpec( Guard g, GuardPredicateRemovable gs )'''- «IF g.not»not «ENDIF» «genPTerm(gs.pred)» '''
//def dispatch genGuardSpec( Guard g, GuardWait gw )'''event,«gw.evId.name»'''



/* 
 * -----------------------------------------------------------
 * PlanMove
 * -----------------------------------------------------------
 */
def  dispatch genMove( PlanMove  move)'''//not here genMove PlanMove''' 
def  dispatch genMove( GetActivationEvent  move)'''
it.unibo.contactEvent.interfaces.IEventItem «move.^var.name»= this.getPlanActivationEvent();
if(  «move.^var.name» != null ) println("activation event= " +  «move.^var.name».getDefaultRep() );
''' 
def  dispatch genMove( GetSensedEvent  move)'''
it.unibo.contactEvent.interfaces.IEventItem «move.^var.name»= this.currentEvent;
if(  «move.^var.name» != null ) println("sensed event= " +  «move.^var.name».getDefaultRep() );
''' 
def  dispatch genMove( ResumePlan  move)'''
returnValue = continueWork; //we must restore nPlanIter and curPlanInExec of the 'interrupted' plan
''' 
def  dispatch genMove( SuspendPlan  move)'''
return suspendWork;  
''' 
def  dispatch genMove( RepeatPlan  move)'''
if( planUtils.repeatPlan(«move.niter»).getGoon() ) continue;
''' 
def  dispatch genMove( SwitchPlan  move)'''
if( ! planUtils.switchToPlan("«move.plan.name»").getGoon() ) break;
'''  
def  dispatch genMove( EndPlan  move)'''
println( "«move.msg»" );
«IF(curPlan.resume)»returnValue = continueWork;«ENDIF»
//QActorContext.terminateQActorSystem(this);
break;
'''
def  dispatch genMove( EndActor  move)'''
println( "«move.msg»" );
//QActorContext.terminateQActorSystem(this); 
'''
def  dispatch genMove( LoadPlan  move)''' 
	   //aar = solveGoal( "consult( \"./«genVorString(move.fname)»\" )", 0, "","" , ""  );	   
	   QActorUtils.consultFromFile( pengine, "«genVorString(move.fname)»" ); 
'''  
def  dispatch genMove( RunPlan  move)'''
	    alice.tuprolog.SolveInfo sss = QActorUtils.solveGoal("runPlan(«genVarAtomic(move.plainid)»)", pengine );
	    if( ! sss.isSuccess()){
	    		println("run plan «genVarAtomic(move.plainid)» failed");
	    		 break;
	    }

'''  
/* 
 * -----------------------------------------------------------
 * GuardMove
 * -----------------------------------------------------------
 */ 

 
 def dispatch genMove( RemoveRule  m )'''
 temporaryStr = "«genPHead(m.rule)»";
 «IF curPlanAction.guard != null » 
 temporaryStr = QActorUtils.substituteVars(guardVars,temporaryStr);
 «ENDIF»
 removeRule( temporaryStr );  
  '''
 def dispatch genMove( AddRule  m )'''
 temporaryStr = "«genPHead(m.rule)»";
 «IF curPlanAction.guard != null » 
 temporaryStr = QActorUtils.substituteVars(guardVars,temporaryStr);
 «ENDIF»
 addRule( temporaryStr );  
 '''
		  


/* 
 * -----------------------------------------------------------
 * ReactMove
 * -----------------------------------------------------------
 */
def genReacEvents( Reaction r ){
	if( r != null ) genReactionEvents(r)
}
def genReactionEvents( Reaction r )'''«FOR alarm : r.alarms SEPARATOR ","»«genAlarmEvents(alarm)»«ENDFOR»'''
def dispatch genAlarmEvents( AlarmEvent a )'''not here AlarmEvent'''
def dispatch genAlarmEvents( NormalEvent a )'''«a.ev.name»'''
def dispatch genAlarmEvents( ContinueEvent a )'''«a.evOccur.name»'''

def genReactPlans( Reaction r ){
	if( r != null ) genReactionPlans(r)
}
def genReactionPlans( Reaction r )'''«FOR alarm : r.alarms SEPARATOR ","»«genAlarmPlans(alarm)»«ENDFOR»'''
def dispatch genAlarmPlans( AlarmEvent a )'''not here AlarmEvent'''
def dispatch genAlarmPlans( NormalEvent a )'''«a.planRef.name»'''
def dispatch genAlarmPlans( ContinueEvent a )'''dummyPlan'''

/* 
 * -----------------------------------------------------------
 * ExtensionMove
 * -----------------------------------------------------------
 */
 //def genAnswerEvent( AnswerEvent ev )'''«IF ev != null »"«ev.name»"«ELSE»""«ENDIF»'''
    
 	def  dispatch genMove( ExtensionMove move )'''//Not yet implemented
 	'''
 	def  dispatch genMove( Sound move )''' 
 //playsound
«IF move.answerEvent == null»
terminationEvId =  QActorUtils.getNewName(IActorAction.endBuiltinEvent);«ELSE»
terminationEvId = "«move.answerEvent.name»";«ENDIF»
 	 	aar = playSound(«genVoS(move.srcfile.fname)»,«IF move.answerEvent == null» ActionExecMode.synch«ELSE»ActionExecMode.asynch«ENDIF», terminationEvId, «genTimeLimit(move.duration)»,"«genReacEvents(curPlanAction.react)»" , "«genReactPlans(curPlanAction.react)»" ); 
		«genCheckAar(move.name)»
  	'''
	def dispatch genMove( Delay m )'''
//delay
aar = delayReactive(«genTimeLimit(m.duration)»,"«genReacEvents(curPlanAction.react)»" , "«genReactPlans(curPlanAction.react)»");
if( aar.getInterrupted() ) curPlanInExec   = "«curPlan.name»";
if( ! aar.getGoon() ) break;
'''
 	
  	def genCheckAar(String actionName)'''
//println(getName() + " plan " + curPlanInExec  +  " interrupted=" + aar.getInterrupted() + " action goon="+aar.getGoon());
if( aar.getInterrupted() ){
	curPlanInExec   = "«curPlan.name»";
	if( ! aar.getGoon() ) break;
} 			
'''	
/*
 * MSG
 */	
 /*  
 	def genPStruct( PStruct msg )'''«msg.name»( «genPArgs(msg)» )'''
	def genPArgs( PStruct msg )'''«FOR pm : msg.msgArg  SEPARATOR "," »«genPArg(pm)»«ENDFOR»'''
 			
 	def dispatch genPArg( PTerm pa )'''//not here genPArg'''
 	def dispatch genPArg( PAtomVAr pa )'''«pa.name»'''
 	def dispatch genPArg( PAtomString pa )''' \"«pa.^val»\"  '''
 	def dispatch genPArg( PAtomNum pa )'''«pa.^val»'''
 */
/*
 * VarOrString
 */
 def genVoS( VarOrString vos )'''«IF vos.^var!=null»«genVariable( vos.^var)»«ELSE»"«vos.const»"«ENDIF»'''
 def genVorString( VarOrString vos )'''«IF vos.^var!=null»«genVariable( vos.^var)»«ELSE»«vos.const»«ENDIF»'''
 def genVoStr( VarOrString vos )'''«IF vos.^var!=null»"«vos.^var.name»"«ELSE»"«vos.const»"«ENDIF»'''
 def genVarPstruct( VarOrPStruct vop )'''«IF vop.^var!=null»«genVariable( vop.^var) »«ELSE»«genPStruct(vop.psrtuct)»«ENDIF»'''
 def genVarAtomic( VarOrAtomic vop )'''«IF vop.^var!=null»«genVariable( vop.^var) »«ELSE»«vop.const.^val»«ENDIF»'''
 def genVarAtomicStr( VarOrAtomic vop )'''«IF vop.^var!=null»«genVariable( vop.^var) »«ELSE»"«vop.const.^val»"«ENDIF»'''
 def genVarQactor( VarOrQactor vop )'''«IF vop.^var!=null»«genVariable( vop.^var) »«ELSE»"«vop.dest.name»"«ENDIF»'''
 
 //def genVS( VarOrString vs )'''«IF vs.^var != null»«genVariable( vs.^var)»«ELSE»"«vs.const»"«ENDIF»'''
 def genVarPstructString( VarOrPStruct vop )'''«IF vop.^var!=null»«genVariable( vop.^var) »«ELSE»"«genPStruct(vop.psrtuct)»"«ENDIF»'''
 def genVariable( Variable  v )'''guardVars.get("«v.name»")'''
 
// def genTimeLimit(TimeLimit tl)'''«IF tl.^var==null»«tl.msec»«ELSE»Integer.parseInt(«genVariable(tl.^var)»)«ENDIF»'''
def genTimeLimit(TimeLimit tl)'''«IF tl.^var==null»«tl.msec»«ELSE»Integer.parseInt(QActorUtils.substituteVars(guardVars,"«tl.^var.name»").replace("'",""))«ENDIF»'''
/*
 * PHead
 */
 def dispatch genPHead( PHead ph )'''not here genPHead'''
 def dispatch genPHead( PAtom ph ){genPTerm(ph)}
 def dispatch genPHead( PStruct ph ){genPTerm(ph)}

}
