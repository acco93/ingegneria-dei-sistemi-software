package it.unibo.xtext.qactor.generator 
import it.unibo.xtext.qactor.*
import it.unibo.xtext.qactor.generator.common.GenKb
import it.unibo.xtext.qactor.generator.common.IGenQActorCtxSystem
import it.unibo.xtext.qactor.generator.common.GenUtils
import it.unibo.xtext.qactor.generator.common.IGenQActorAndroid
import com.google.inject.Inject
import it.unibo.xtext.qactor.generator.common.SysKb

/*
 * ====================================================
 * SYSTEM
 * ====================================================
 */
class GenQActorCtxSystem implements IGenQActorCtxSystem{ 
@Inject IGenQActorAndroid genQActorAndroid
protected var String packageName    = ""	 
protected var QActorSystemSpec system   
protected var String sysName        = ""	
protected var String mainClassName  = ""	
protected var String ctxClassName   = ""	 
protected var srcMoreDir = "srcMore";	
protected var androPackage ="";
	override doGenerate(QActorSystemSpec qasys, Context ctx, GenKb kb) {
  		println(" *** GenQActorCtxSystem starts for " + qasys.name + " ctx=" + ctx.name )
		packageName      = GenUtils.packageName ;
		system 			 = qasys 
		sysName 	     = kb.getActorSystemName().toLowerCase ;
		ctxClassName     = ctx.name.toFirstUpper ;
		mainClassName    = "Main"+ctxClassName;
		androPackage     = "it.unibo."+kb.actorSystemName;
		if( GenUtils.androidEnv() ){
			srcMoreDir="assets"
			genQActorAndroid.doGenerate(qasys,  ctx,  kb)
		}   
		
		//Generate the main theory  
// 		GenUtils.genFileDir(  ".", packageName, mainClassName,   "java", genCtxQActor( mainClassName, ctx  ) )
  		GenUtils.genFile(  packageName, mainClassName,   "java", genCtxQActor( mainClassName, ctx  ) )
  		if( ! SysKb.existFile( "../build_"+ctx.name+".gradle" ) ){
  			GenUtils.genFileDir(  "..", "", "build_"+ctx.name, "gradle",  genBuildGradle(mainClassName,ctx,false ) )  		
  		}
   		genTheHandlers(ctx) 		 
  	}
/*
 * genBuildGradle
 */
def genBuildGradle(String className, Context ctx, boolean ddr)''' 
/*
================================================================================
build_«ctx.name».gradle
GENERATED ONLY ONCE
USAGE:	 
	gradle -b build_«ctx.name».gradle eclipse	//to set the dependency on the library
	gradle -b build_«ctx.name».gradle build
================================================================================
*/
apply plugin: 'java'
apply plugin: 'eclipse'
apply plugin: 'java-library-distribution'

/*
--------------------------------------------------------------
PROPERTIES
--------------------------------------------------------------
*/
version = "1.0"
sourceCompatibility = "1.8"
ext{
	mainClassName = "it.unibo.«ctx.name».«className»"
}
/*
--------------------------------------------------------------
DIRS
--------------------------------------------------------------
*/
sourceSets {
	main {
		java {
			srcDirs = ['src' , 'src-gen'  ]
		}
	}
	test {
		 java {
		   srcDirs = ['test']
		 }
	}
}
/*
--------------------------------------------------------------
DEPENDENCIES
--------------------------------------------------------------
*/
repositories {
      mavenCentral()
}
dependencies {
	  compile fileTree(dir: '../it.unibo.iss.libs/libs/unibo', include: 'uniboInterfaces.jar')
	  compile fileTree(dir: '../it.unibo.iss.libs/libs/unibo', include: 'unibonoawtsupports.jar')
	  compile fileTree(dir: '../it.unibo.iss.libs/libs/unibo', include: 'uniboEnvBaseAwt.jar')
	  compile fileTree(dir: '../it.unibo.iss.libs/libs/unibo', include: '2p301.jar')
	  compile fileTree(dir: '../it.unibo.iss.libs/libs/unibo', include: 'qa18Akka.jar')
	  compile fileTree(dir: '../it.unibo.iss.libs/libs/http',  include: 'nanoHTTPD.jar')
	  // https://mvnrepository.com/artifact/org.eclipse.paho/org.eclipse.paho.client.mqttv3
	  //compile group: 'org.eclipse.paho', name: 'org.eclipse.paho.client.mqttv3', version: '1.1.0'
	  
	  //compile fileTree(dir: '../it.unibo.iss.libs/libs/bth', include: 'bluecove-2.1.1-SNAPSHOT.jar')
	  //compile fileTree(dir: '../it.unibo.iss.libs/libs/bth', include: 'bluecove-gpl-2.1.1-SNAPSHOT.jar')
	  // https://mvnrepository.com/artifact/org.json/json
	  compile group: 'org.json', name: 'json', version: '20160810'
 	  «IF ddr»
	 compile group: 'com.pi4j', name: 'pi4j-core', version: '1.1'
	 compile fileTree(dir: '../it.unibo.iss.libs/libs/pi4j',  include: 'pi4j-core-1.1.jar')
	 compile fileTree(dir: '../it.unibo.iss.libs/libs/unibo', include: 'labbaseRobotSam.jar')
	 compile fileTree(dir: '../it.unibo.iss.libs/libs/unibo', include: 'uniboQactorRobot.jar') 
  	  «ENDIF»	 
  	 //compile fileTree(dir: '../it.unibo.iss.libs/libs/unibo', include: 'blsHL.jar')
     //compile fileTree(dir: '../it.unibo.iss.libs/libs/unibo', include: 'blsGUI.jar')
	 //compile fileTree(dir: '../it.unibo.iss.libs/libs/unibo', include: 'bls17Blink.jar')
	 //compile fileTree(dir: '../it.unibo.iss.libs/libs/unibo', include: 'blsArduino.jar')
	 //compile fileTree(dir: '../it.unibo.iss.libs/libs/unibo', include: 'blsProxy.jar')
      testCompile 'junit:junit:4.12'
      // https://mvnrepository.com/artifact/com.typesafe.akka/akka-actor_2.11
	  compile group: 'com.typesafe.akka', name: 'akka-actor_2.11', version: '2.4.8'
	  compile group: 'com.typesafe.akka', name: 'akka-remote_2.11', version: '2.4.9-RC2'
	// https://mvnrepository.com/artifact/org.eclipse.paho/org.eclipse.paho.client.mqttv3
	compile group: 'org.eclipse.paho', name: 'org.eclipse.paho.client.mqttv3', version: '1.1.0'
}
/*
--------------------------------------------------------------
AFTER TEST
--------------------------------------------------------------
*/
test {
    afterTest { desc, result -> 
        println "Executing test ${desc.name} [${desc.className}] with result: ${result.resultType}"
    }
}
/*
---------------------------------------------------------------------
JAR: incldes src-gen code (excluding Java) in the executable jar
---------------------------------------------------------------------
*/
jar {
   	from( 'src-gen' ){ include '**/*.*'	 exclude '**/*.java'  }
 	manifest {
 		attributes "Class-Path": '.  ' + configurations.compile.collect { "lib/"+it.getName() }.join(' ')
		attributes 'Main-Class': "$mainClassName"
	}
}
distributions {
    main{
        baseName = "$mainClassName"
    }
}
/*
---------------------------------------------------------
PREPARE DISTRIBUTION
---------------------------------------------------------
*/
task copyInfoForDist << {
	copy {
		from 'audio'
		into 'src/dist/audio/'
		include '**/*.*'
 	}
 	«IF ddr»
	copy {
		from 'configuration'
		into 'src/dist/configuration/'
		include '**/*.*'
 	}
 	«ENDIF»
	copy { 
		from 'srcMore'
		into 'src/dist/srcMore/'
		include '**/*.*'
 	}
	copy { 
		from '.'
		into 'src/dist/'
		include '*.pl'
		include '*.html'
		«IF ddr»include 'hardwareConfiguration.properties'«ENDIF»
 	}
	copy {  
		from 'src'
		into 'src/dist/'
		include '*.qa'
		include '*.ddr'
		include '*.baseddr'
   	}
}
task cleanDistDir(type: Delete) {
	  delete 'src/dist'
      
  }
jar.dependsOn cleanDistDir
jar.dependsOn copyInfoForDist
''' 	

/*
 * ACTOR CONTEXT
 */	
def  genCtxQActor( String className, Context ctx )'''
«GenUtils.logo»
package «packageName»;
import it.unibo.qactors.QActorContext;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.system.SituatedSysKb;
public class «className»  {
  
//MAIN
 	«IF GenUtils.androidEnv()»
 	private static QActorContext actx; 
	public static void initQaApplOnAndroid( «androPackage».BaseActivity activity)  {
	try{		 
	 	final InputStream sysKbStream    = activity.getAssets().open("«packageName.replace(".","/")»/«sysName».pl");
		final InputStream sysRulesStream = activity.getAssets().open("«packageName.replace(".","/")»/sysRules.pl");
		actx = new  «className»("«ctx.name»", activity.getOutputEnvView(), sysKbStream, sysRulesStream );
		it.unibo.contactEvent.platform.ContactEventPlatform.getPlatform(actx);
		actx.configure();
	} catch (Exception e) {
		activity.println("initQaApplication ERROR " + e.getMessage());
	}
  	} 	
	public static void endQaApplOnAndroid( «androPackage».BaseActivity activity)  {
	try{		 
	 	actx.terminate();
	} catch (Exception e) {
		activity.println("endQaApplOnAndroid ERROR " + e.getMessage());
	}
	} 	
 	«ELSE»
	public static QActorContext initTheContext() throws Exception{
		IOutputEnvView outEnvView = SituatedSysKb.standardOutEnvView;
		«IF( ctx.env )»«genCtxEnv(ctx)»«ENDIF»
		String webDir = «IF(ctx.httpserver)»"./«srcMoreDir»/«packageName.replace(".","/")»"«ELSE»null«ENDIF»;
		return QActorContext.initQActorSystem(
			"«ctx.name.toLowerCase»", "./«srcMoreDir»/«packageName.replace(".","/")»/«sysName».pl", 
			"./«srcMoreDir»/«packageName.replace(".","/")»/sysRules.pl", outEnvView,webDir,«system.testing»);
	}
	public static void main(String[] args) throws Exception{
		initTheContext();
	} 	
	«ENDIF» 	
}
'''

def genEventList(EventHandler h)'''new String[]{«FOR ev : h.events SEPARATOR ","»"«ev.name»"«ENDFOR»}'''

def genCtxEnv( Context ctx )'''
it.unibo.is.interfaces.IBasicEnvAwt env=new it.unibo.baseEnv.basicFrame.EnvFrame( 
	"Env_«ctx.name»",«genColor(ctx.color)», java.awt.Color.black );
env.init();
outEnvView = env.getOutputEnvView();
'''

def genColor( WindowColor c)'''java.awt.Color.«c» '''

/*
 * --------------------------------------------------
 * HANDLERS
 * --------------------------------------------------
 */
def  genTheHandlers( Context ctx )'''
	«FOR h :  ctx.handler»
		«genEventHandler(h)»
 	«ENDFOR» 
'''

 
 
def genEventHandler(EventHandler h){
	 var hname = h.name.trim().toFirstUpper
      //From 1.2.4 generate the Abstract in src-gen and no more in src
	 GenUtils.genFile(  packageName, "Abstract"+hname , "java", genAbstractEventHandler(h) )
//	 GenUtils.genFileSrcDir(  packageName, "Abstract"+hname , "java", genAbstractEventHandler(h) )
     var fname = GenUtils.srcDirPath+ packageName.replace(".","/")+ "/"+hname+".java"
     println(" *** GenQActorCtxSystem fname " + fname  ) 
      
	 if( ! GenUtils.testing ) 	
	 	GenUtils.genFileSrcDir(  packageName, hname , "java", genUserEventHandler(h) )
	 else if( ! SysKb.existFile( fname ) ) GenUtils.genFile(packageName, hname , "java", genUserEventHandler(h) )
}
def genUserEventHandler(EventHandler h)'''
«GenUtils.logo»
package «GenUtils.packageName»;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.QActorContext;

public class «h.name.toFirstUpper» extends Abstract«h.name.toFirstUpper» { 
	public «h.name.toFirstUpper»(String name, QActorContext myCtx, IOutputEnvView outEnvView, String[] eventIds ) throws Exception {
		super(name, myCtx, outEnvView,eventIds);
  	}
}
'''

def genAbstractEventHandler(EventHandler h)'''
«GenUtils.logo»
package «GenUtils.packageName»;
import alice.tuprolog.Term;
import it.unibo.contactEvent.interfaces.IEventItem;
import it.unibo.qactors.platform.EventHandlerComponent;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.QActorContext;
import it.unibo.qactors.QActorUtils;

public abstract class Abstract«h.name.toFirstUpper» extends EventHandlerComponent { 
protected IEventItem event;
	public Abstract«h.name.toFirstUpper»(String name, QActorContext myCtx, IOutputEnvView outEnvView, String[] eventIds ) throws Exception {
		super(name, myCtx, eventIds, outEnvView);
  	}
	@Override
	public void doJob() throws Exception {	}
	
	public void handleCurrentEvent() throws Exception {
		event = this.currentEvent; //AKKA getEventItem();
		if( event == null ) return;
«IF h.print»
		//showMsg( "---------------------------------------------------------------------" );	
		showMsg( event.getPrologRep()  );				 
		//showMsg( "---------------------------------------------------------------------" );	
«ENDIF»
		«IF h.body != null »«genEvhBody( h.body )» «ENDIF»	
	}//handleCurrentEvent
	
	@Override
	protected void handleQActorEvent(IEventItem ev) {
		super.handleQActorEvent(ev);
 		try {
			handleCurrentEvent();
		} catch (Exception e) {
 			e.printStackTrace();
		}
	}//handleQActorEvent
	
}
'''


def genEvhBody(EventHandlerBody b)'''
«FOR op:b.op»«genEventHandlerOperation(op)»«ENDFOR»
'''
def dispatch genEventHandlerOperation( EventHandlerOperation op )'''not here EventHandlerOperation'''
def dispatch genEventHandlerOperation( MemoOperation op )'''
«IF op.rule != null»QActorUtils.getQActor("«op.actor.name»_ctrl").«genEvhRule( op.rule)»;
«ELSE»
QActorUtils.getQActor("«op.actor.name»_ctrl").memoCurrentEvent( event, «op.doMemo.lastonly»);
«ENDIF»
''' 
def dispatch genEventHandlerOperation( SolveOperation op )'''
QActorUtils.solveGoal(QActorUtils.getQActor("«op.actor.name»_ctrl").getPrologEngine(),"«genPTerm(op.goal)»");
'''
def dispatch genEventHandlerOperation( RaiseEvent op )'''
emit( "«op.ev.name»", «genPHead(op.content)» );
'''
def dispatch genEventHandlerOperation( SendEventAsDispatch op )'''{
Term msgt       = Term.createTerm(event.getMsg());
Term msgPattern = Term.createTerm("«genPHead(op.msgref.msg)»");
		boolean b = this.pengine.unify(msgt, msgPattern);
		if( b ) {
	  		sendMsg("«op.msgref.name»","«op.actor.name»", QActorContext.dispatch, msgt.toString() ); 
		}else{
			println("non unifiable");
		}
}
'''
/*
 * PHead
 */
 def dispatch genPHead( PHead ph )'''not here genPHead'''
 def dispatch genPHead( PAtom ph ){ genPTerm(ph) }
 def dispatch genPHead( PStruct ph ){ genPTerm(ph) }


def dispatch genEvhRule( MemoRule op )'''not here MemoRule'''
def dispatch genEvhRule( MemoEvent op )'''addRule(event.getDefaultRep())'''
def genPStruct( PStruct ps )'''«ps.name»(«FOR term: ps.msgArg SEPARATOR ','»«genPTerm(term)»«ENDFOR»)'''
def dispatch genPTerm( PTerm pt )'''not here Pterm'''
def dispatch genPTerm( PAtom pt )'''not here PAtom'''
def dispatch genPTerm( PAtomic pt )'''«pt.^val»'''
def dispatch genPTerm( PAtomString pt )''' \"«pt.^val»\" '''
def dispatch genPTerm( Variable pt )'''«pt.name»'''
def dispatch genPTerm( PAtomNum pt )'''«pt.^val»''' 
def dispatch genPTerm( PStruct ps )'''«genPStruct(ps)»'''
  
 
 

}