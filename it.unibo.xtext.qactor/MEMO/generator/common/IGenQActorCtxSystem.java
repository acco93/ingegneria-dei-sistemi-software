package it.unibo.xtext.qactor.generator.common;

import it.unibo.xtext.qactor.Context;
import it.unibo.xtext.qactor.QActorSystemSpec;

public interface IGenQActorCtxSystem {
	public void doGenerate( QActorSystemSpec system,  Context actor, GenKb kb );
}
