package it.unibo.xtext.qactor.generator.common;
import it.unibo.xtext.qactor.QActorSystemSpec;

public interface IGenQActorSystem {
	public void doGenerate( QActorSystemSpec system,  GenKb kb );
}
