package it.unibo.xtext.qactor.generator.common;
import it.unibo.xtext.qactor.QActor;

public interface IGenQActor {
	public void doGenerate( QActor actor,  GenKb kb );
}
