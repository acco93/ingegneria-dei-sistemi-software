package it.unibo.xtext.qactor.generator
import it.unibo.xtext.qactor.*
import it.unibo.xtext.qactor.generator.common.GenKb
import it.unibo.xtext.qactor.generator.common.GenUtils
 

class GenQActorPlanProlog  {
private var pc = 0
protected var QActor actor
protected var GenKb kb
protected var String packageName 
protected var String actorClassName = ""	
protected var srcMoreDir ="srcMore";
protected PlanAction curPlanAction;

	def doGen(QActor myactor, GenKb mykb){
  		actor	= myactor 
 		kb		= mykb
 		println(" *** GenQActorPlanProlog starts for " + actor.name )
 		packageName     = GenUtils.packageName 
		actorClassName  = actor.name.toFirstUpper 
  		if( GenUtils.androidEnv() ) srcMoreDir="assets"
     	 
 		GenUtils.genFileDir(   "..",  srcMoreDir+"/"+packageName,  "plans", "txt", genThePlans( actor  ) );
    }

/*
 * ============================================
 * PLANS
 * ============================================
 */
def genThePlans(QActor actor){
var outS = ""
	for( plan : actor.plans ){ 
		pc = 1
		outS = outS + genPlan( plan )
	}
	outS
}

def genPlan( Plan p ){
var outS=""
	for( action : p.action ){
		curPlanAction = action
		outS = outS + genPlanAction(p.name , action)
	} 
	if( ! p.normal ){
		if(  p.resume ) outS = outS + 
		'''plan(«pc++»,«p.name», sentence(true,move(resumeplan),'','' ))
		''' 
		else outS = outS + 
		'''plan(«pc++»,«p.name», sentence(true,move(resumeplan),'','' ))
		''' 
 	}
 	outS
}
def genPlanAction( String pname, PlanAction pa )'''
plan(«pc++»,«pname», sentence(«genGuard(pa.guard)»,«genPlanMove(pa.move)»,'«genReacEvents(pa.react)»','«genReactPlans(pa.react)»' ))
'''
/*
 * Action guard
 */
def genGuard( Guard g )'''«IF g==null»true«ELSE»«genGuardSpec(g, g.guardspec)»«ENDIF»'''
//def dispatch genGuardSpec( Guard g, GuardSpec  gs )'''not here GuardSpec'''
//def dispatch genGuardSpec( Guard g, GuardQuery gs )'''not here GuardQuery'''
def dispatch genGuardSpec( Guard g, GuardPredicate  gs )'''not here GuardPredicate'''
//def dispatch genGuardSpec( Guard g, GuardAtom  gs )'''«IF g.not»not «ENDIF» «gs.atom»'''
def dispatch genGuardSpec( Guard g, GuardPredicateStable gs )'''«IF g.not»not «ENDIF» «genPTerm(gs.pred)»'''
def dispatch genGuardSpec( Guard g, GuardPredicateRemovable gs )'''- «IF g.not»not «ENDIF» «genPTerm(gs.pred)» '''
//def dispatch genGuardSpec( Guard g, GuardWait gw )'''event,«gw.evId.name»'''

/*
 * Move
 */
def dispatch genPlanMove( Move m )'''not here Move'''
/*
 * Action move
 */
def dispatch genPlanMove( ActionMove  m )'''not here ActionMove''' 
def dispatch genPlanMove( ExecuteAction  m ){
	if( m.sentence != null ) return genMoveSentence (m.sentence)
	else return genPlanMoveAction( m )
}
def genMoveSentence(PHead s)'''solve(«genPHead(s)»)'''
//def  genPlanMoveAction( ExecuteAction  m )'''time(«m.action.msec»),cmd(«m.action.name»),«IF m.arg==null»args('')«ELSE»args('«IF m.action.arg != null»«genPStruct(m.action.arg)»«ELSE»""«ENDIF» , «IF m.arg != null»«genPHead(m.arg)»«ELSE»""«ENDIF»')«ENDIF»'''
def  genPlanMoveAction( ExecuteAction  m )'''todo(execute, «m.action.name»)'''
def dispatch genPlanMove( SolveGoal  m )'''move(solve,«genPHead(m.goal)»,«genTimeLimit(m.duration)»,«IF( m.plan != null  )»"«m.plan.name»"«ELSE»""«ENDIF»)'''

/*
 * Message move
 */
def dispatch genPlanMove( MessageMove  m )'''not here MessageMove'''
/*
 * raisevent 
 */
def dispatch genPlanMove( RaiseEvent m )'''raise( «m.ev.name» , «genPHead(m.content)» )')'''
def dispatch genPlanMove( SendDispatch m )'''forward(«genVarQactor(m.dest)», «m.msgref.name»,«genPHead(m.^val)»)''' 
def dispatch genPlanMove( ReplyToCaller m )'''replyToCaller(«m.msgref.name»,«genPHead(m.^val)»)''' 
def genVarQactor( VarOrQactor vop )'''«IF vop.^var!=null»«genVariable( vop.^var) »«ELSE»«vop.dest.name»«ENDIF»'''

def dispatch genPlanMove( SendRequest m )''' request(«genVarQactor(m.dest)»),m.msgref.name»,«genPHead(m.^val)»))''' 
def dispatch genPlanMove( ReceiveMsg m )'''receiveMsg(«genMsgSpec(m.spec)»)'''
def dispatch genPlanMove( OnReceiveMsg m )'''receiveTheMsg(«genPHead(m.msgid)»,«genPHead(m.msgtype)»,«genPHead(m.msgsender)»,«genPHead(m.msgreceiver)»,«genPHead(m.msgcontent)»,«genPHead(m.msgseqnum)» )'''
 
def dispatch genPlanMove( SenseEvent m )'''senseEvent([«FOR ev:m.events SEPARATOR ','»«ev.name»«ENDFOR»],[«FOR c:m.plans SEPARATOR ','»«genContinuation(c)»«ENDFOR»]'''
def dispatch genPlanMove( MsgSelect m )'''  msgselect([«FOR ev:m.messages SEPARATOR ','»«ev.name»«ENDFOR»],[«FOR p:m.plans SEPARATOR ','»«p.name»«ENDFOR»])'''
//def dispatch genPlanMove( MsgSwitch m )'''msgswitch(«m.message.name»,[«FOR content:m.msg SEPARATOR ','»«genPHead(content)»«ENDFOR»],[«FOR c:m.plan SEPARATOR ','»«genContinuation(c)»«ENDFOR»])'''
//def dispatch genPlanMove( EventSwitch m )'''eventswitch(«m.event.name»,[«FOR content:m.msg SEPARATOR ','»«genPHead(content)»«ENDFOR»],[«FOR c:m.plan SEPARATOR ','»«genContinuation(c)»«ENDFOR»])'''

def dispatch genPlanMove( MsgSwitch m )'''msgswitch(«m.message.name», «genPHead(m.msg)» , «genPlanMove(m.move)»)''' 
def dispatch genPlanMove( EventSwitch m )'''eventswitch(«m.event.name», »«genPHead(m.msg)», «genPlanMove(m.move)» )'''

def genMsgSpec(MsgSpec ms)'''«IF(ms!=null)»«genTheMsgSpec(ms)»«ELSE»''«ENDIF»'''
def genTheMsgSpec(MsgSpec ms)'''msg(«ms.msg.name»,«genMessageType(ms.msg)»,«genVarAtomic(ms.sender)»,RECEIVER,«genPHead(ms.content)»SEQNUM)'''
def genVoS( VarOrString vos )'''«IF vos.^var!=null»«vos.^var.name»«ELSE»«vos.const»«ENDIF»'''
def genVoStr( VarOrString vos )'''«IF vos.^var!=null»«vos.^var.name»«ELSE»'«vos.const»'«ENDIF»'''
def genVarPstruct( VarOrPStruct vop )'''«IF vop.^var!=null»«vop.^var.name»«ELSE»«genPStruct(vop.psrtuct)»«ENDIF»'''
def genVarOrAtomOrPStruct( VarOrAtomOrPStruct vop )'''«IF vop.^var!=null»«vop.^var.name»«ELSEIF vop.atom!=null»«vop.atom.^val»«ELSE»«genPStruct(vop.psrtuct)»«ENDIF»'''
def genVarPstructString( VarOrPStruct vop )'''«IF vop.^var!=null»«genVariable(vop.^var)»«ELSE»'«genPStruct(vop.psrtuct)»'«ENDIF»'''
def genVariable( Variable  v )'''«v.name»''' //automatic dynamic variable substutution by the interpreter
def genVarAtomic( VarOrAtomic vop )'''«IF vop.^var!=null»«genVariable( vop.^var) »«ELSE»«vop.const.^val»«ENDIF»'''

def dispatch genMessageType( Message m)'''not here message'''
def dispatch genMessageType( Event m)'''event'''
def dispatch genMessageType( Signal m)'''signal'''
def dispatch genMessageType( Token m)'''token'''
def dispatch genMessageType( Dispatch m)'''dispatch''' 
def dispatch genMessageType( Request m)'''request'''
def dispatch genMessageType( Invitation m)'''invitation'''

def genContinuation( Continuation c ){
	if( c.nane != null ) return '''continue'''
	else '''«c.plan.name»'''
}
/*
 * PHead
 */
 def dispatch genPHead( PHead ph )'''not here genPHead'''
 def dispatch genPHead( PAtom ph ){ genPTerm(ph) }
 def dispatch genPHead( PStruct ph ){ genPTerm(ph) }
/*
 * PTerm
 */
def dispatch genPTerm( PTerm pt )'''not here Pterm'''
def dispatch genPTerm( PAtom pt )'''not here PAtom''' 
def dispatch genPTerm( PAtomic d )'''«d.^val»'''
def dispatch genPTerm( PAtomString pt )'''"«pt.^val»"'''
def dispatch genPTerm( Variable pt )'''«pt.name»'''
def dispatch genPTerm( PAtomNum pt )'''«pt.^val»'''
def dispatch genPTerm( PActorCall pt )'''«actor.name» <- «genPStruct(pt.body)» '''
def dispatch genPTerm( PStruct ps )'''«genPStruct(ps)»'''
//def dispatch genPTerm( Data d )'''not here Data'''
//def dispatch genPTerm( AtomData d )'''«d.name»'''
//def dispatch genPTerm( IntegerData d )'''«d.value»'''
//def dispatch genPTerm( StringData d )'''"«d.value»"'''
def genPStruct( PStruct ps )'''«ps.name»( «FOR term: ps.msgArg SEPARATOR ','»«genPTerm(term)»«ENDFOR»)'''
/*
 * Aguzzi
 */
//def dispatch genPTerm (PPredicate pt)'''not here predicate'''
def dispatch genPTerm(PIs pt)'''«genPTerm(pt.varout)» is «genPTerm(pt.varin)» + «genPTerm(pt.num)»'''
def dispatch genPTerm( PAtomCut pt )''' ! '''


/*
 * Basic move
 */
def dispatch genPlanMove( BasicMove m )'''not here BasicMove'''
def dispatch genPlanMove( Print  m )'''move(print,«genPHead(m.args)»)'''
def dispatch genPlanMove( PrintCurrentEvent  m )'''printCurrentEvent(«IF m.memo»'memo'«ELSE»''«ENDIF»)'''
def dispatch genPlanMove( PrintCurrentMessage  m )'''printCurrentMessage(«IF m.memo»'memo'«ELSE»''«ENDIF»)'''
def dispatch genPlanMove( MemoCurrentEvent  m )'''memoCurrentEvent '''
def dispatch genPlanMove( MemoCurrentMessage  m )''' memoCurrentMessage'''
//def genPrintMoveArgs(MoveArgs args)'''«args.msg»«genOtherArgs(args)»'''
//def genOtherArgs(MoveArgs args)'''«IF args.vars.length>0»«ENDIF»«FOR vos:args.vars»«genVoS(vos)»«ENDFOR»'''
//def genPrintMoveArgs(PArgTerm arg)'''«genPArgTerm(arg)»''' 
 /*
 * PArgTerm
 */
//def dispatch genPArgTerm( PArgTerm pt )'''not here PArgTerm'''
//def dispatch genPArgTerm( PStruct pt ){genPStruct(pt)}
//def dispatch genPArgTerm( PAtom pt ){genPTerm(pt)}    

/*
 *
 * Extension move
 */
def dispatch genPlanMove( ExtensionMove m )'''not here ExtensionMove'''
def dispatch genPlanMove( Sound m )'''move(playsound('«genVoS(m.srcfile.fname)»',«genTimeLimit(m.duration)»,«genAnswerEvent(m.answerEvent)»))'''
def dispatch genPlanMove( Delay m )'''delay( ) '''
//def genTimeLimit(TimeLimit tl)'''«IF tl.^var != null » «tl.^var.name» «ELSE» «tl.msec»«ENDIF»'''
def genTimeLimit(TimeLimit tl)'''«IF tl.^var==null»«tl.msec»«ELSE»Integer.parseInt(substituteVars(guardVars,"«tl.^var.name»"))«ENDIF»'''

def genAnswerEvent( AnswerEvent ev )'''«IF ev != null »«ev.name»«ELSE»''«ENDIF»'''
/*
 * Reaction
 */
def genReacEvents( Reaction r ){
	if( r != null ) genReactionEvents(r)	 
}
def genReactionEvents( Reaction r )'''«FOR alarm : r.alarms SEPARATOR ","»«genAlarmEvents(alarm)»«ENDFOR»'''
def dispatch genAlarmEvents( AlarmEvent a )'''not here AlarmEvent'''
def dispatch genAlarmEvents( NormalEvent a )'''«a.ev.name»'''
def dispatch genAlarmEvents( ContinueEvent a )'''«a.evOccur.name»'''

def genReactPlans( Reaction r ){
	if( r != null ) genReactionPlans(r)
}
def genReactionPlans( Reaction r )'''«FOR alarm : r.alarms SEPARATOR ","»«genAlarmPlans(alarm)»«ENDFOR»'''
def dispatch genAlarmPlans( AlarmEvent a )'''not here AlarmEvent'''
def dispatch genAlarmPlans( NormalEvent a )'''«a.planRef.name»'''
def dispatch genAlarmPlans( ContinueEvent a )'''dummyPlan'''

/*
 * Plan move
 */
def dispatch genPlanMove( PlanMove  m )'''not here PlanMove'''
def dispatch genPlanMove( ResumePlan  m )'''move(resumeplan)'''
def dispatch genPlanMove( RepeatPlan  m )'''move(repeatplan) '''
def dispatch genPlanMove( SwitchPlan  m )'''move( switchplan(«m.plan.name») )'''
def dispatch genPlanMove( EndPlan  m )'''endplan('«m.msg»')'''
def dispatch genPlanMove( LoadPlan  m )'''loadPlan(«genVoStr(m.fname)»)'''
def dispatch genPlanMove( RunPlan  m )'''runPlan(«genVarAtomic(m.plainid)»)'''  //TODO VarOrAtomic

/*
 * ADDED but perhaps outside the scope of plans.txt
 */
def dispatch genPlanMove( GetActivationEvent  m )'''getActivationEvent('«m.^var.name»')'''
def dispatch genPlanMove( GetSensedEvent  m )'''getSensedEvent('«m.^var.name»')'''
/*
 * Guard move
 */
 def dispatch genPlanMove( GuardMove  m )'''not here  GuardMove'''
 
 def dispatch genPlanMove( AddRule  m )'''addrule( «genPHead(m.rule)» )'''
 def dispatch genPlanMove( RemoveRule  m )'''removerule( «genPHead(m.rule)» )'''
}