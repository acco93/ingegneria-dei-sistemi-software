package it.unibo.xtext.qactor.generator 
import it.unibo.xtext.qactor.*
import it.unibo.xtext.qactor.generator.common.GenKb
import it.unibo.xtext.qactor.generator.common.IGenQActorCtxSystem
import it.unibo.xtext.qactor.generator.common.GenUtils
import it.unibo.xtext.qactor.generator.common.IGenQActorAndroid
import com.google.inject.Inject
import it.unibo.xtext.qactor.generator.common.SysKb

/*
 * ====================================================
 * SYSTEM
 * ====================================================
 */
class GenQActorCtxSystem implements IGenQActorCtxSystem{ 
@Inject IGenQActorAndroid genQActorAndroid
protected var String packageName    = ""	 
protected var QActorSystemSpec system   
protected var String sysName        = ""	
protected var String mainClassName  = ""	
protected var String ctxClassName   = ""	 
protected var srcMoreDir = "srcMore";	
protected var androPackage ="";
	override doGenerate(QActorSystemSpec qasys, Context ctx, GenKb kb) {
  		println(" *** GenQActorCtxSystem starts for " + qasys.name + " ctx=" + ctx.name )
		packageName      = GenUtils.packageName ;
		system 			 = qasys 
		sysName 	     = kb.getActorSystemName().toLowerCase ;
		ctxClassName     = ctx.name.toFirstUpper ;
		mainClassName    = "Main"+ctxClassName;
		androPackage     = "it.unibo."+kb.actorSystemName;
		if( GenUtils.androidEnv() ){
			srcMoreDir="assets"
			genQActorAndroid.doGenerate(qasys,  ctx,  kb)
		}   
		
		//Generate the main theory  
// 		GenUtils.genFileDir(  ".", packageName, mainClassName,   "java", genCtxQActor( mainClassName, ctx  ) )
  		GenUtils.genFile(  packageName, mainClassName,   "java", genCtxQActor( mainClassName, ctx  ) )
  		GenUtils.genFileDir(  "..", "", "build_"+ctx.name, "gradle",  genBuildGradle(mainClassName,ctx ) )
   		genTheHandlers(ctx) 		 
  	}
/*
 * genBuildGradle
 */
def genBuildGradle(String className, Context ctx)''' 
/*
================================================================================
build_«ctx.name».gradle
USAGE:	 
	gradle -b build_«ctx.name».gradle eclipse	//to set the dependency on the library
	gradle -b build_«ctx.name».gradle build
================================================================================
*/
apply plugin: 'java'
apply plugin: 'eclipse'
apply plugin: 'java-library-distribution'

/*
--------------------------------------------------------------
PROPERTIES
--------------------------------------------------------------
*/
version = "1.0"
sourceCompatibility = "1.8"
ext{
	mainClassName = "it.unibo.«ctx.name».«className»"
}
/*
--------------------------------------------------------------
DIRS
--------------------------------------------------------------
*/
sourceSets {
	main {
		java {
			srcDirs = ['src' , 'src-gen'  ]
		}
	}
	test {
		 java {
		   srcDirs = ['test']
		 }
	}
}
/*
--------------------------------------------------------------
DEPENDENCIES
--------------------------------------------------------------
*/
dependencies {
	 compile fileTree(dir: './localLibs/unibo', include: '*.jar')
}
/*
--------------------------------------------------------------
AFTER TEST
--------------------------------------------------------------
*/
test {
    afterTest { desc, result -> 
        println "Executing test ${desc.name} [${desc.className}] with result: ${result.resultType}"
    }
}
/*
---------------------------------------------------------------------
JAR: incldes src-gen code (excluding Java) in the executable jar
---------------------------------------------------------------------
*/
jar {
   	from( 'src-gen' ){ include '**/*.*'	 exclude '**/*.java'  }
 	manifest {
 		attributes "Class-Path": '.  ' + configurations.compile.collect { "lib/"+it.getName() }.join(' ')
		attributes 'Main-Class': "$mainClassName"
	}
}
/*
---------------------------------------------------------
PREPARE DISTRIBUTION
---------------------------------------------------------
*/
task copyInfoForDist << {
	copy {
		from 'audio'
		into 'src/dist/audio/'
		include '**/*.*'
 	}
	copy { 
		from 'srcMore'
		into 'src/dist/srcMore/'
		include '**/*.*'
 	}
	copy { 
		from '.'
		into 'src/dist/'
		include '*.pl'
 	}
}
jar.dependsOn copyInfoForDist
''' 	

/*
 * ACTOR CONTEXT
 */	
def  genCtxQActor( String className, Context ctx )'''
«GenUtils.logo»
package «packageName»;
import it.unibo.qactors.ActorContext;
import java.io.InputStream;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.system.SituatedSysKb;
public class «className» extends ActorContext{
//private IBasicEnvAwt env; 
 
	public «className»(String name, IOutputEnvView outEnvView,
			InputStream sysKbStream, InputStream sysRulesStream) throws Exception {
		super(name, outEnvView, sysKbStream, sysRulesStream);
		this.outEnvView = outEnvView;
		env = outEnvView.getEnv();
 	}
	@Override
	public void configure() {
		try {
		SituatedSysKb.init();	//Init the schedulers
 		«IF ctx.httpserver»
		println("Starting the http server ... ");
		new  it.unibo.qactors.web.QActorHttpServer(outEnvView,".",8080).start();
		«ENDIF»
		«IF ctx.handler.size > 0 »println("Starting the handlers .... ");«ENDIF»
		«FOR handler: ctx.handler»
			new «handler.name.toFirstUpper»("«handler.name.toLowerCase»", this, outEnvView,«genEventList(handler)»);
		«ENDFOR»
		«IF system.actor.size > 0 »println("Starting the actors .... ");«ENDIF»	
		«FOR actor: system.actor»
			«IF actor.context.name.equals(ctx.name)»
 				new «GenUtils.getPackageActorName(actor)».«actor.name.toFirstUpper»("«actor.name.toLowerCase»", this, outEnvView);
 			«ENDIF»
		«ENDFOR»
		
 		} catch (Exception e) {
 		  e.printStackTrace();
		} 		
	}
 	
/*
* ----------------------------------------------
* MAIN
* ----------------------------------------------
*/
 	
 	«IF GenUtils.androidEnv()»
 	private static ActorContext actx; 
	public static void initQaApplOnAndroid( «androPackage».BaseActivity activity)  {
	try{		 
	 	final InputStream sysKbStream    = activity.getAssets().open("«packageName.replace(".","/")»/«sysName».pl");
		final InputStream sysRulesStream = activity.getAssets().open("«packageName.replace(".","/")»/sysRules.pl");
		actx = new  «className»("«ctx.name»", activity.getOutputEnvView(), sysKbStream, sysRulesStream );
		it.unibo.contactEvent.platform.ContactEventPlatform.getPlatform(actx);
		actx.configure();
	} catch (Exception e) {
		activity.println("initQaApplication ERROR " + e.getMessage());
	}
  	} 
  	
	public static void endQaApplOnAndroid( «androPackage».BaseActivity activity)  {
	try{		 
	 	actx.terminate();
	} catch (Exception e) {
		activity.println("endQaApplOnAndroid ERROR " + e.getMessage());
	}
	} 	
 	«ELSE»
	public static void main(String[] args) throws Exception{
		IOutputEnvView outEnvView = SituatedSysKb.standardOutEnvView;
		«IF( ctx.env )»«genCtxEnv(ctx)»«ENDIF»
		InputStream sysKbStream    = null;
		InputStream sysRulesStream = null;
		/*
		 * In the final distribution srcMore is a user-visible directory, while the 
		 * application in packaged in a jar
		 */
		try{
			sysKbStream    = //«className».class.getResourceAsStream("«sysName».pl");
				new java.io.FileInputStream("./«srcMoreDir»/«packageName.replace(".","/")»/«sysName».pl");
		}catch(Exception e){
			System.out.println("sysKbStream ERROR=" + e.getMessage() )	;
			sysKbStream = new java.io.FileInputStream(".srcMore/«packageName.replace(".","/")»/«sysName».pl");
		}
		try{
			sysRulesStream = //«className».class.getResourceAsStream("sysRules.pl");
				new java.io.FileInputStream("./«srcMoreDir»/«packageName.replace(".","/")»/sysRules.pl");
		}catch(Exception e){
			System.out.println("sysKbStream ERROR=" + e.getMessage() )	;
			sysRulesStream = new java.io.FileInputStream(".srcMore/«packageName.replace(".","/")»/sysRules.pl");
		}
	/*	
 		InputStream sysKbStream    = //«className».class.getResourceAsStream("«sysName».pl");
 			new java.io.FileInputStream("./«srcMoreDir»/«packageName.replace(".","/")»/«sysName».pl");
		InputStream sysRulesStream = //«className».class.getResourceAsStream("sysRules.pl");
			new java.io.FileInputStream("./«srcMoreDir»/«packageName.replace(".","/")»/sysRules.pl");
	*/
		new «className»("«ctx.name»", outEnvView, sysKbStream, sysRulesStream ).configure();
 	} 	
 	«ENDIF»
}
'''
def genEventList(EventHandler h)'''new String[]{«FOR ev : h.events SEPARATOR ","»"«ev.name»"«ENDFOR»}'''

def genCtxEnv( Context ctx )'''
		it.unibo.is.interfaces.IBasicEnvAwt env = new it.unibo.baseEnv.basicFrame.EnvFrame( "Env_«ctx.name»", «genColor(ctx.color)», java.awt.Color.black );
		env.init();
		outEnvView = env.getOutputEnvView();
'''

def genColor( WindowColor c)'''java.awt.Color.«c» '''

/*
 * --------------------------------------------------
 * HANDLERS
 * --------------------------------------------------
 */
def  genTheHandlers( Context ctx )'''
	«FOR h :  ctx.handler»
		«genEventHandler(h)»
 	«ENDFOR» 
'''

 
 
def genEventHandler(EventHandler h){
	 var hname = h.name.trim().toFirstUpper
      //From 1.2.4 generate the Abstract in src-gen and no more in src
	 GenUtils.genFile(  packageName, "Abstract"+hname , "java", genAbstractEventHandler(h) )
//	 GenUtils.genFileSrcDir(  packageName, "Abstract"+hname , "java", genAbstractEventHandler(h) )
     var fname = "../src/"+ packageName.replace(".","/")+ "/"+hname+".java"
     println(" *** GenQActorCtxSystem fname " + fname ) 
     if( ! SysKb.existFile( fname ) ) 
		GenUtils.genFileSrcDir(  packageName, hname , "java", genUserEventHandler(h) )
}
def genUserEventHandler(EventHandler h)'''
«GenUtils.logo»
package «GenUtils.packageName»;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.ActorContext;

public class «h.name.toFirstUpper» extends Abstract«h.name.toFirstUpper» { 
	public «h.name.toFirstUpper»(String name, ActorContext myCtx, IOutputEnvView outEnvView, String[] eventIds ) throws Exception {
		super(name, myCtx, outEnvView,eventIds);
  	}
}
'''

def genAbstractEventHandler(EventHandler h)'''
«GenUtils.logo»
package «GenUtils.packageName»;
import alice.tuprolog.Term;
import it.unibo.contactEvent.interfaces.IEventItem;
import it.unibo.contactEvent.platform.EventHandlerComponent;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.ActorContext;

public abstract class Abstract«h.name.toFirstUpper» extends EventHandlerComponent { //implements IActionHandler
protected IEventItem event;
	public Abstract«h.name.toFirstUpper»(String name, ActorContext myCtx, IOutputEnvView outEnvView, String[] eventIds ) throws Exception {
		super(name, myCtx, eventIds, outEnvView);
  	}
	@Override
	public void doJob() throws Exception {
		event = getEventItem();
		if( event == null ) return;
«IF h.print»
		showMsg( "---------------------------------------------------------------------" );	
		showMsg( event.getPrologRep()  );				 
		showMsg( "---------------------------------------------------------------------" );	
«ENDIF»
		«IF h.body != null »«genEvhBody( h.body )» «ENDIF»	
	}
}
'''


def genEvhBody(EventHandlerBody b)'''
«FOR op:b.op»«genEventHandlerOperation(op)»«ENDFOR»
'''
def dispatch genEventHandlerOperation( EventHandlerOperation op )'''not here EventHandlerOperation'''
def dispatch genEventHandlerOperation( MemoOperation op )'''
«IF op.rule != null»myCtx.getActor("«op.actor.name»").«genEvhRule( op.rule)»;
«ELSE»
myCtx.getActor("«op.actor.name»").memoCurrentEvent( event, «op.doMemo.lastonly»);
«ENDIF»
''' 
def dispatch genEventHandlerOperation( SolveOperation op )'''
myCtx.getActor("«op.actor.name»").solveGoal("«genPTerm(op.goal)»",0,"","","");
'''
def dispatch genEventHandlerOperation( RaiseEvent op )'''
emit( "«op.ev.name»", «genPHead(op.content)» );
'''
def dispatch genEventHandlerOperation( SendEventAsDispatch op )'''{
Term msgt       = Term.createTerm(event.getMsg());
Term msgPattern = Term.createTerm("«genPHead(op.msgref.msg)»");
		boolean b = this.pengine.unify(msgt, msgPattern);
		if( b ) {
	  		sendMsg("«op.msgref.name»","«op.actor.name»", ActorContext.dispatch, msgt.toString() ); 
		}else{
			println("non unifiable");
		}
}
'''
/*
 * PHead
 */
 def dispatch genPHead( PHead ph )'''not here genPHead'''
 def dispatch genPHead( PAtom ph ){ genPTerm(ph) }
 def dispatch genPHead( PStruct ph ){ genPTerm(ph) }


def dispatch genEvhRule( MemoRule op )'''not here MemoRule'''
def dispatch genEvhRule( MemoEvent op )'''addRule("stored("+event.getEventId()+","+event.getMsg()+")")'''
def genPStruct( PStruct ps )'''«ps.name»(«FOR term: ps.msgArg SEPARATOR ','»«genPTerm(term)»«ENDFOR»)'''
def dispatch genPTerm( PTerm pt )'''not here Pterm'''
def dispatch genPTerm( PAtom pt )'''not here PAtom'''
def dispatch genPTerm( PAtomic pt )'''«pt.^val»'''
def dispatch genPTerm( PAtomString pt )''' \"«pt.^val»\" '''
def dispatch genPTerm( Variable pt )'''«pt.name»'''
def dispatch genPTerm( PAtomNum pt )'''«pt.^val»''' 
def dispatch genPTerm( PStruct ps )'''«genPStruct(ps)»'''
  
 
 

}