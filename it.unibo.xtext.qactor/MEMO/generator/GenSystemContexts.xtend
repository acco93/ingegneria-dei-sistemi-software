package it.unibo.xtext.qactor.generator
import it.unibo.xtext.qactor.generator.common.IGenSystemContexts
import it.unibo.xtext.qactor.QActorSystemSpec
import it.unibo.xtext.qactor.QActor
import it.unibo.xtext.qactor.generator.common.GenKb
import it.unibo.xtext.qactor.generator.common.GenUtils
import it.unibo.xtext.qactor.Context
import it.unibo.xtext.qactor.Robot

/*
 * =================================================
 * CONTEXT
 * =================================================
 */
class GenSystemContexts implements IGenSystemContexts {
var String packageName = ""
var QActorSystemSpec system;
var String descrDir = "../descr";
var Context curCtx ;
var srcMoreDir ="srcMore";
 	override doGenerate( QActorSystemSpec mysystem,  Context myCtx, GenKb kb ){
		system 			= mysystem
		curCtx          = myCtx
 		packageName     = GenUtils.packageName ;  
 		var sysName     = system.name.toLowerCase;
		println(" *** GensystemContexts starts " + sysName +  " " + packageName + " " + curCtx.name )
 		//Generate the sysRules.pl
   		//GenUtils.genFile(  packageName, "sysRules" , "pl", genCtxRules() )		
   		GenUtils.genFileDir( "..",  srcMoreDir+"/"+packageName.replace(".","/"), "sysRules" , "pl", genCtxRules() )
 		if( GenUtils.androidEnv() ){
 			srcMoreDir="assets"
 			GenUtils.genFileDir(  "..", "assets/"+packageName, "sysRules" , "pl", genCtxRules() )					
 		}      	 
 		
	    GenUtils.genFileDir(   "..",  srcMoreDir+"/"+packageName, sysName, "pl", genCtx( system ) )	
 		if( ! it.unibo.xtext.qactor.generator.common.SysKb.existFile( "../" +srcMoreDir +"/"+packageName.replace(".","/")+"/QActorWebUI.html") ){
	  		GenUtils.genFileDir(  "..", srcMoreDir+"/"+packageName, "QActorWebUI", "js",    genQActorWebUIJs() )
	  		GenUtils.genFileDir(  "..", srcMoreDir+"/"+packageName, "QActorWebUI", "html",  genQActorWebUIHtml() )
	  		GenUtils.genFileDir(  "..", srcMoreDir+"/"+packageName, "QActorWebUI", "css",   genQActorWebUICss() )  	
		}
		/*
 		 * Generate only once the html server files in the srcMore context directory
 		 */
  		//		
 		if( ! it.unibo.xtext.qactor.generator.common.SysKb.existFile( descrDir + "/"+sysName+".html") ) 
			GenUtils.genFileDir(  descrDir, "", sysName, "html",  genDescr( sysName) )
   	}

 
  	
/*
 * Description
 */
def genDescr(String sysName)'''
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<link rel="stylesheet" type="text/css" href="../../css/plainStyle.css">
<link rel="stylesheet" type="text/css" href="../../css/QActorTabStyle.css">   
<script type="text/javascript" src="../../css/issStyle.js"></script>
<head>

<title>«sysName»</title></head>
    <body>
    
<div class="container wrapper"> 
<div id="top">
</div> 
     
<div class="wrapper">
<div id="main">
<p>
<h5>«sysName»</h5>
    
<h3 id="h3"><a name="problem"></a>The problem</h3>    
 
    
<h3 id="h3">The approach</h3>

<h3>The code</h3>
<a href="../../../../it.unibo.xtext.QActor.planned/src/«sysName».ddr">«sysName».ddr</a>    
</p>
</div>

<div id="bottom">
By AN  Unibo-DISI    
</div>
</div>
</div>    
</body>
</html>
'''

/*
 * Context rules
 */  	
def genCtxRules()'''
%==============================================
% DEFINED BY THE SYSTEM DESIGNER
% CONTEXT HANDLING UTILTY RULES
%==============================================
getCtxNames(CTXNAMES) :-
	findall( NAME, context( NAME, _, _, _ ), CTXNAMES).
getCtxPortNames(PORTNAMES) :-
	findall( PORT, context( _, _, _, PORT ), PORTNAMES).
getTheContexts(CTXS) :-
	findall( context( CTX, HOST, PROTOCOL, PORT ), context( CTX, HOST, PROTOCOL, PORT ), CTXS).
getTheActors(ACTORS) :-
	findall( qactor( A, CTX ), qactor( A, CTX ), ACTORS).

insertActorOnce( NAME, CTX ) :-
	qactor( NAME , CTX ), !.
insertActorOnce( NAME, CTX ) :-	
 	assert( qactor( NAME , CTX ) ).

removeActor( NAME, CTX ):-
	retract( qactor( NAME , CTX ) ).
removeActor( NAME, CTX ).

getCtxHost( NAME, HOST )  :- context( NAME, HOST, PROTOCOL, PORT ).
getCtxPort( NAME,  PORT ) :- context( NAME, HOST, PROTOCOL, PORT ).
getCtxProtocol( NAME,  PROTOCOL ) :- context( NAME, HOST, PROTOCOL, PORT ).

getMsgId( msg( MSGID, MSGTYPE, SENDER, RECEIVER, CONTENT, SEQNUM ) , MSGID  ).
getMsgType( msg( MSGID, MSGTYPE, SENDER, RECEIVER, CONTENT, SEQNUM ) , MSGTYPE ).
getMsgSenderId( msg( MSGID, MSGTYPE, SENDER, RECEIVER, CONTENT, SEQNUM ) , SENDER ).
getMsgReceiverId( msg( MSGID, MSGTYPE, SENDER, RECEIVER, CONTENT, SEQNUM ) , RECEIVER ).
getMsgContent( msg( MSGID, MSGTYPE, SENDER, RECEIVER, CONTENT, SEQNUM ) , CONTENT ).
getMsgNum( msg( MSGID, MSGTYPE, SENDER, RECEIVER, CONTENT, SEQNUM ) , SEQNUM ).

checkMsg( MSG, MSGLIST, PLANLIST, RES ) :- 
	%%stdout <- println( checkMsg( MSG, MSGLIST, PLANLIST, RES ) ),
	checkTheMsg(MSG, MSGLIST, PLANLIST, RES).	
checkTheMsg( MSG, [], _, dummyPlan ).
checkTheMsg( msg( MSGID, MSGTYPE, SENDER, RECEIVER, CONTENT, SEQNUM ), [ MSGID | _ ], [ PLAN | _ ], PLAN ):-!.
checkTheMsg( MSG, [_|R], [_|P], RES ):- 
	%%stdout <- println( checkMsg( MSG, R, P, RES ) ),
	checkTheMsg(MSG, R, P, RES).

msgContentToPlan( MSG, CONTENTLIST,PLANLIST,RES ):-
	%stdout <- println( msgContentToPlan( MSG, CONTENTLIST,PLANLIST,RES) ),
	msgContentToAPlan( MSG, CONTENTLIST,PLANLIST,RES ).
msgContentToAPlan( MSG, [], _, dummyPlan ).
msgContentToAPlan( msg( MSGID, MSGTYPE, SENDER, RECEIVER, CONTENT, SEQNUM ), [ CONTENT | _ ], [ PLAN | _ ], PLAN ):-!.
msgContentToAPlan( event(  CONTENT  ), [ CONTENT | _ ], [ PLAN | _ ], PLAN ):-!.
msgContentToAPlan( MSG, [_|R], [_|P], RES ):- 
	%stdout <- println( msgContentToAPlan( MSG, R, P, RES ) ),
	msgContentToPlan(MSG, R, P, RES).	


%==============================================
% MEMENTO
%==============================================
%%% :- stdout <- println( hello ).
%%% --------------------------------------------------
% context( NAME, HOST, PROTOCOL, PORT )
% PROTOCOL : "TCP" | "UDP" | "SERIAL" | "PROCESS" | ...
%%% --------------------------------------------------

%%% --------------------------------------------------
% msg( MSGID, MSGTYPE, SENDER, RECEIVER, CONTENT, SEQNUM )
% MSGTYPE : dispatch request answer
%%% --------------------------------------------------

'''

 
/* 
 * Context config file
 */ 
def genCtx(QActorSystemSpec sys)'''
%====================================================================================
% Context «curCtx.name» «IF curCtx.standalone»standalone=«ENDIF» SYSTEM-configuration: file «packageName».«sys.name».pl 
%====================================================================================
«IF curCtx.standalone»«genStandaloneCtx()»
«ELSE»
«FOR  context : system.context »
	«genCtxSpec(context)»
«ENDFOR»
%%% -------------------------------------------
«FOR  qa : system.actor »
	«genCtx(qa)»
«ENDFOR»
%%% -------------------------------------------
«FOR  qr : system.robot »
	«genCtx(qr)»
«ENDFOR»

«ENDIF»
'''

//Generate a standalone Context
def genStandaloneCtx()'''
«genCtxSpec(curCtx)»
%%% -------------------------------------------
«FOR  qa : system.actor »
	«IF qa.context.name.equals( curCtx.name)»«genCtx(qa)»«ENDIF»
«ENDFOR»
'''
def genCtx(QActor qa)'''
qactor( «qa.name.toLowerCase» , «qa.context.name.toLowerCase»  ).
'''
/*
 * ROBOT 
 */
def genCtx(Robot qar)'''
qactor( «qar.actor.name.toLowerCase» , «qar.actor.context.name.toLowerCase»  ).
'''
 
def genCtxSpec(Context ctx)'''
context(«ctx.name.toLowerCase», "«ctx.ip.host»",  "TCP", "«ctx.ip.port»" ).  		 
'''

/*
 * ----------------------------------------------
 * HTML
 * ----------------------------------------------
 */
def genQActorWebUICss()'''
/* QActorWebUI.css */
body {
    font: 100% Lucida Sans;
    margin: 20px;
    line-height: 26px;
}

#h3 {
    background-color: #f6d8f8;
}
#i {
    background-color: #fcf881;
}

#main {
	font: 80% Lucida Sans;
    margin: 0 5px;
}

#bottom {
     padding: 5px;
    font-size: 70%;
    line-height: 14px;
    
}

#robots {
    background-color: #d9f7d6;
    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
    width: 80%;
    border-collapse: collapse;
}

#robots td  {
    font-size: 1em;
    border: 1px solid #98bf21;
    padding: 3px 7px 2px 7px;
}
'''
 
def genQActorWebUIHtml()'''
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css" href="QActorWebUI.css">
<script type="text/javascript" src="QActorWebUI.js"></script>
</head>
<body>
<h3 id="h3">Robot Web UI</h3>

<div> 
<input size=85% type="textArea" id="input" value="[ true ], play('./audio/tada2.wav') , 1500 " />
<input style="height: 30px; width: 90px;" type="button"
 id="ExternalForward" value="RUN" 
 onmousedown="document.getElementById('inputArea').value= document.getElementById('inputArea').value+'\n'+document.getElementById('input').value;
 send('i-'+document.getElementById('input').value); "
<br/>
<textarea id="inputArea" cols="100" rows="10" style="overflow:scroll;">
</textarea>
</div>

<div id="bottom">
<center>
<button onclick="setSpeed('high');">speedHigh</button>
<button onclick="setSpeed('medium');">speedMedium</button>
<button onclick="setSpeed('low');">speedLow</button>
</center>
   
</div>
 	<table id="robots">
		<tr>
			<td>
				<table>
 					<tr>
						<td>Speed</td>
 						<td><input size=30% type="text" id="speeddd" value="low" /></td>
						<td>Connection</td>
						<td><input size=30% type="text" id="connection" value="CONNECTING ..." /></td>
	 				</tr>
					<tr>
						<td>Sending</td>
 						<td><input size=30% type="text" id="sending" value=" ... " /></td>
 						<td>State</td>
 						<td><input size=30% type="text" id="state" value=" ... " /></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>
                <center>
				<table style="background-color: #d9f7f3">
					<tr>
						<td align="center"><input
							style="height: 40px; width: 90px; color:red" type="button"
							id="ExternalAlarm" value="FIRE" onmousedown="send('e(alarm(fire))');" /></td>
						<td align="center"><input
							style="height: 60px; width: 90px;" type="button"
							id="ExternalForward" value="FORWARD" onmousedown="send('w('+curSpeed+')');" /></td>
						<td align="center"><input
							style="height: 60px; width: 90px; color:red " type="button" 
							id="ExternalAlarm" value="OBSTACLE" onmousedown="send('e(alarm(obstacle))');" /></td>
					</tr>
					<tr>
						<td align="center"><input
							style="height: 60px; width: 90px;" type="button"
							id="ExternalLeft" value="LEFT" onmousedown="send('a('+curSpeed+')');"  /></td>
						<td align="center"><input
							style="height: 60px; width: 90px;" type="button"
							id="ExternalStop" value="STOP" onmousedown="send('h('+curSpeed+')');"  /></td>
						<td align="center"><input
							style="height: 60px; width: 90px;" type="button"
							id="ExternalRight" value="RIGHT" onmousedown="send('d('+curSpeed+')');" /></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td align="center"><input
							style="height: 60px; width: 90px;" type="button"
							id="ExternalBackward" value="BACKWARD" onmousedown="send('s('+curSpeed+')');"/></td>
						<td>&nbsp;</td>
					</tr>
				</table>
                </center>
			</td>
		</tr>
	</table>
 </body>
</html>
'''

def genQActorWebUIJs()'''
//QActorWebUI.js
var curSpeed = "low";
	
    console.log("QActorWebUI.js : server IP= "+document.location.host);
 /*
 * WEBSOCKET
 */
    var sock = new WebSocket("ws://"+document.location.host, "protocolOne");
    sock.onopen = function (event) {
         //console.log("QActorWebUI.js : I am connected to server.....");
         document.getElementById("connection").value = 'CONNECTED';
    };
    sock.onmessage = function (event) {
        //console.log("QActorWebUI.js : "+event.data);
        //alert( "onmessage " + event);
        document.getElementById("state").value = ""+event.data;
    }
    sock.onerror = function (error) {
        //console.log('WebSocket Error %0',  error);
        document.getElementById("state").value = ""+error;
    };
    
	function setSpeed(val){
		curSpeed = val;
		document.getElementById("speed").value = curSpeed;
	}
	function send(message) {
		document.getElementById("sending").value = ""+message;
		sock.send(message);
	};
/*	
	document.onkeydown = function(event) {
//	alert("event="+event);
		    event = event || window.event;
		    switch (event.keyCode || event.which) {
		        case 65:
		            send('a-High');
		            break;
		        case 68:
		            send('d-High');
		            break;
		        case 87:
		            send('w-High');
		            break;
		        case 83:
		            send('s-High');
		            break;
		        default:
		            //send('h-High');
		    }
	};
*/
 	//alert("loaded");
'''


 }