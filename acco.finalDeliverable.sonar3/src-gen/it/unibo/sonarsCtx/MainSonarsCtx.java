/* Generated by AN DISI Unibo */ 
package it.unibo.sonarsCtx;
import it.unibo.qactors.QActorContext;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.system.SituatedSysKb;
public class MainSonarsCtx  {
  
//MAIN
public static QActorContext initTheContext() throws Exception{
	IOutputEnvView outEnvView = SituatedSysKb.standardOutEnvView;
	String webDir = null;
	return QActorContext.initQActorSystem(
		"sonarsctx", "./srcMore/it/unibo/sonarsCtx/sonars.pl", 
		"./srcMore/it/unibo/sonarsCtx/sysRules.pl", outEnvView,webDir,false);
}
public static void main(String[] args) throws Exception{
	initTheContext();
} 	
}
