/* Generated by AN DISI Unibo */
/*
This code is generated only ONCE
*/
package it.unibo.sonar2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;

import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.QActorContext;

public class Sonar2 extends AbstractSonar2 {
	private Process p;

	public Sonar2(String actorId, QActorContext myCtx, IOutputEnvView outEnvView) throws Exception {
		super(actorId, myCtx, outEnvView);
	}

	public void initSensor() {
		try {
			p = Runtime.getRuntime().exec("sudo ./SonarAlone3");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void readData() throws IOException {

		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
			String[] split = reader.readLine().split(",");

			System.out.println("data(" + split[0] + "," + split[1] + ")");

			this.addRule("data(" + split[0] + "," + split[1] + ")");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void readDataVirtual() throws IOException {

		int position = 3;
		int distance = 85 + new Random().nextInt(20);
	//	System.out.println("data(" + distance + "," + position + ")");
		this.addRule("data(" + distance + "," + position + ")");

	}
}
