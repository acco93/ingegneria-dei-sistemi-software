%====================================================================================
% Context sonarsCtx  SYSTEM-configuration: file it.unibo.sonarsCtx.sonars.pl 
%====================================================================================
context(pccontext, "192.168.43.183",  "TCP", "8033" ).  		 
context(sonarsctx, "192.168.43.9",  "TCP", "9987" ).  		 
%%% -------------------------------------------
qactor( sonar1 , sonarsctx, "it.unibo.sonar1.MsgHandle_Sonar1"   ). %%store msgs 
qactor( sonar1_ctrl , sonarsctx, "it.unibo.sonar1.Sonar1"   ). %%control-driven 
qactor( sonar2 , sonarsctx, "it.unibo.sonar2.MsgHandle_Sonar2"   ). %%store msgs 
qactor( sonar2_ctrl , sonarsctx, "it.unibo.sonar2.Sonar2"   ). %%control-driven 
%%% -------------------------------------------
%%% -------------------------------------------

