package it.unibo.testing;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Test;

import it.unibo.testing.Properties;
import alice.tuprolog.Prolog;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.QActorContext;
import it.unibo.qactors.QActorUtils;
import it.unibo.qactors.akka.QActor;
import it.unibo.system.SituatedSysKb;
import java.lang.reflect.Method;

import it.unibo.actor.Actor;

public class Testing {
	
	/**
	 * Default standard output
	 */ 
	private IOutputEnvView stdOut = SituatedSysKb.standardOutEnvView;
	
	/**
	 * Prolog engine
	 */ 
	private Prolog pengine;
	
	/**
	 * Contexts definition
	 */ 
	private QActorContext ctx;
	
	/**
	 * QActors definition
	 */ 
	private QActor actor;
	
	/**
	 * Properties object definition
	 */
	 private Properties actorProperties;
	
	/**
	 * Empty constructor
	 */
	public Testing(){
		
	}
	
	
	/**
	 * Set up
	 */
	 @Before
	 public void setUp() {
	 		try {
	 			
	 			// INIT CONTEXTS
	 			// init context ctx =================================
	 			ctx = QActorContext.initQActorSystem(
	 									// context name
	 									"ctx",
	 									// system theory file
	 									"./srcMore/it/unibo/ctx/system.pl",
	 									// system rules file
	 									"./src-gen/it/unibo/ctx/sysRules.pl",
	 									// output
	 									stdOut,
	 									// web dir
	 									null,
	 									// testing
	 									true);
	 									
	 			// checking the correct context initialization
	 			assertTrue("set up ctx", ctx != null);
	 			// end init ctx =====================================
	 			
	 			
	 			// INIT QACTORS
	 		actor = QActorUtils.getQActor("actor");
	 		assertTrue("set up actor", actor != null);
	 		actorProperties = this.getProperties("actor");
	 		
	 			// INIT PROLOG ENGINE
	 			// pengine = qa.getPrologEngine();
	 			// assertTrue("setUp", pengine != null);
	 
	 		
	 		} catch (Exception e) {
	 			System.out.println("setUp ERROR " + e.getMessage());
	 			fail("setUp " + e.getMessage());
	 		}
	 	}
	
	
	@Test
		public void testRun1() throws Exception {
				Thread.sleep(500);
				assertTrue("Check is ready ...", actorProperties.getIsReady()== true);
				
				QActorUtils.raiseEvent(ctx, "ctx", "ev", "ev");
				
				Thread.sleep(1000);
				assertTrue("Check sensed events ...", actorProperties.getSensedEvents()== 1);
				
			
			}		
	@Test
		public void testRun2() throws Exception {
				Thread.sleep(500);
				assertFalse("Another test", actorProperties.getIsReady()== false);
				
			
			}		
	
		
		private Properties getProperties(String actorName) throws Exception {
	
			Properties properties = null;
	
			QActor actor = QActorUtils.getQActor(actorName + "_ctrl");
	
			Method method = Actor.class.getMethod("getTestProperties");
	
			if (method == null) {
	
				fail("A method getTestProperties that returns a Properties object is needed.");
	
			} else {
	
				properties = (Properties) method.invoke((Actor) actor);
	
			}
	
			return properties;
	
		}
	
}




