package it.unibo.testing;

public class Properties {

	private Boolean isReady;
	private Integer sensedEvents;
	
	public Properties () {
		isReady = false;
		sensedEvents = 0;
	}
	
	public synchronized Boolean getIsReady() {
			return isReady;
	}
	
	public synchronized void setIsReady(Boolean isReady) {
		this.isReady = isReady;
	}
	public synchronized Integer getSensedEvents() {
			return sensedEvents;
	}
	
	public synchronized void setSensedEvents(Integer sensedEvents) {
		this.sensedEvents = sensedEvents;
	}
}

