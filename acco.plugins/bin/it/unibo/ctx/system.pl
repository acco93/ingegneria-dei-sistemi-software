%====================================================================================
% Context ctx  SYSTEM-configuration: file it.unibo.ctx.system.pl 
%====================================================================================
context(ctx, "localhost",  "TCP", "8888" ).  		 
%%% -------------------------------------------
qactor( actor , ctx, "it.unibo.actor.MsgHandle_Actor"   ). %%store msgs 
qactor( actor_ctrl , ctx, "it.unibo.actor.Actor"   ). %%control-driven 
%%% -------------------------------------------
%%% -------------------------------------------

